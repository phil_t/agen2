/*******************************************************
 *
 * aligned malloc
 *
 *******************************************************/
#ifndef __AGEN_MEMALLOC_H__
#define __AGEN_MEMALLOC_H__

#include <SDL_stdinc.h>

#ifdef __cplusplus
extern "C" {
#endif

void* agen_malloc(uint32_t align, size_t size);
void agen_free(void* ptr);

#ifdef __cplusplus
};
#endif
#endif /* __AGEN_MEMALLOC_H__ */
