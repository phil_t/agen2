#include "queue.h"
#include <SDL_assert.h>
#include <SDL_log.h>
#include <SDL_timer.h>
#include <SDL_thread.h>
#include <SDL_cpuinfo.h>
#include "memalloc.h"

#include <liblfds711.h>
#ifdef __APPLE__
#include <dispatch/dispatch.h>
#endif

typedef struct lfds711_queue_bmm_state lfds711_queue_bmm_state;
typedef struct lfds711_queue_bmm_element lfds711_queue_bmm_element;

struct agen_queue_t {
    lfds711_queue_bmm_state q;
#ifdef __APPLE__
	/*
	 * on OSX/iOS SDL2 emulates semaphores via condvar/mutex pair
	 * due to the lack of sem_value. We don't care about sem_value.
	 */
	dispatch_semaphore_t sem;
#else
	SDL_sem* sem;
#endif
};

agen_queue_t* agen_queue_create(uint32_t size) {
  /*
   * The number of elements given to the bounded/many/many 
   * queue must be a positive integer power of 2
   */
	SDL_assert(size > 0);
	SDL_assert((size % 2) == 0);
 
	agen_queue_t* ret = SDL_static_cast(agen_queue_t*, agen_malloc(SDL_CACHELINE_SIZE, sizeof(agen_queue_t)));
	if (ret == NULL) {
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "out of memory");
		return NULL;
	}

	lfds711_queue_bmm_element* elems = SDL_calloc(size, sizeof(lfds711_queue_bmm_element));
	if (elems == NULL) {
		agen_free(ret);
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "out of memory");
		return NULL;
	}
	
	lfds711_queue_bmm_init_valid_on_current_logical_core(&ret->q, elems, size, NULL);
	
#ifdef __APPLE__
	ret->sem = dispatch_semaphore_create(0);
#else
	ret->sem = SDL_CreateSemaphore(0);
#endif

	if (ret->sem == NULL) {
		SDL_free(elems);
		agen_free(ret);
#ifdef __APPLE__
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Cannot create dispatch semaphore:out of memory");
#endif
		ret = NULL;
	}
	
	return ret;
}
/*
 * add a node at tail->next and update tail
 */
int32_t agen_queue_push(agen_queue_t* q, void* entry) {
	SDL_assert(q);
	
	LFDS711_MISC_MAKE_VALID_ON_CURRENT_LOGICAL_CORE_INITS_COMPLETED_BEFORE_NOW_ON_ANY_OTHER_LOGICAL_CORE;
	/*
	 * atomically add item
	 */
	if (lfds711_queue_bmm_enqueue(&q->q, NULL, entry) == 0) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "queue overflow, will resize");
		return -1;
	}
	/*
	 * notify consumers
	 */
#ifdef __APPLE__
	dispatch_semaphore_signal(q->sem);
#else
	SDL_SemPost(q->sem);
#endif
	return 0;
}

void* agen_queue_pop(agen_queue_t* q, uint32_t timeout) {
#ifdef __APPLE__
    long err = 0;

    switch (timeout) {
      case 0:
        err = dispatch_semaphore_wait(q->sem, DISPATCH_TIME_NOW);
        break;

      case SDL_MUTEX_MAXWAIT:
        err = dispatch_semaphore_wait(q->sem, DISPATCH_TIME_FOREVER);
        break;

      default:
        err = dispatch_semaphore_wait(q->sem,
            dispatch_time(DISPATCH_TIME_NOW, timeout * 1000000ULL));
        break;
    }
    
    if (err) {
        /* expired, back to sleep */
        return NULL;
    }
#else
	int err = SDL_SemWaitTimeout(q->sem, timeout);
	if (err < 0) {
		/* failed, will retry */
		SDL_Delay(100);
		return NULL;
	} 
  
  if (err == SDL_MUTEX_TIMEDOUT) {
		/* timeout expired, back to sleep */
		return NULL;
	}
#endif
	LFDS711_MISC_MAKE_VALID_ON_CURRENT_LOGICAL_CORE_INITS_COMPLETED_BEFORE_NOW_ON_ANY_OTHER_LOGICAL_CORE;

	void* ret = NULL;
	if (lfds711_queue_bmm_dequeue(&q->q, NULL, &ret) == 0) {
		/* empty, back to sleep */
		return NULL;
	}
	return ret;
}

void agen_queue_wake(agen_queue_t* q) {
	SDL_assert(q);
#ifdef __APPLE__
	dispatch_semaphore_signal(q->sem);
#else
	SDL_SemPost(q->sem);
#endif
}
/*
 * block current thread until queue is drained
 */
void agen_queue_wait(agen_queue_t* q) {
	SDL_assert(q);

	while (agen_queue_num_items(q) > 0) {
		SDL_Delay(1);
	}
}
/*
 * get number of items in queue
 */
uint32_t agen_queue_num_items(agen_queue_t* q) {
	SDL_assert(q);
	LFDS711_MISC_MAKE_VALID_ON_CURRENT_LOGICAL_CORE_INITS_COMPLETED_BEFORE_NOW_ON_ANY_OTHER_LOGICAL_CORE;
	
	lfds711_pal_uint_t count = 0;
	lfds711_queue_bmm_query(&q->q, LFDS711_QUEUE_BMM_QUERY_GET_POTENTIALLY_INACCURATE_COUNT, NULL, &count);
	return SDL_static_cast(uint32_t, count);
}

void agen_queue_destroy(agen_queue_t* q) {
	SDL_assert(q);
	
	LFDS711_MISC_MAKE_VALID_ON_CURRENT_LOGICAL_CORE_INITS_COMPLETED_BEFORE_NOW_ON_ANY_OTHER_LOGICAL_CORE;
	lfds711_queue_bmm_cleanup(&q->q, NULL);
	SDL_free(q->q.element_array);
#ifdef __APPLE__
	dispatch_release(q->sem);
#else
	SDL_DestroySemaphore(q->sem);
#endif
	agen_free(q);
}
