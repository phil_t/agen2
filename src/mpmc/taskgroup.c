#include "taskgroup.h"
#include "task.h"
#include "memalloc.h"
#include "queue.h"

#include <SDL_log.h>
#include <SDL_assert.h>
#include <SDL_atomic.h>
#include <SDL_timer.h>
#include <SDL_events.h>

#ifdef _MSC_VER
__declspec(dllimport) Uint32 agen_notify_eventID(void);
#else
extern Uint32 agen_notify_eventID(void);
#endif

struct agen_task_group_t {
	SDL_atomic_t    count;
	agen_complete_f on_complete;
	void*           arg;
};

static void agen_group_task_complete(void* arg) {
	SDL_assert(arg);

	agen_task_group_t* group = SDL_static_cast(agen_task_group_t*, arg);
	SDL_AtomicDecRef(&group->count);
    /*
     * Note: cannot determine if the task group is complete here,
     * it's possible each agen_task_group_add completes before the 
     * next one and count is back to 0.
     */
}
/*
 * suspend current thread until the task group is complete
 */
static void agen_task_group_status_wait(void* arg) {
	SDL_assert(arg);
	agen_task_group_t* group = SDL_static_cast(agen_task_group_t*, arg);
	
	while (SDL_AtomicGet(&group->count) > 0) {
		SDL_Delay(1);
	}
}
/*
 * spinlock current thread until the task group is complete
 */
static void agen_task_group_status_spin_lock(void* arg) {
	SDL_assert(arg);
	agen_task_group_t* group = SDL_static_cast(agen_task_group_t*, arg);
	
	while (SDL_AtomicGet(&group->count) > 0);
}

static void agen_task_group_complete_notify(void* arg) {
	SDL_assert(arg);
	agen_task_group_t* group = SDL_static_cast(agen_task_group_t*, arg);
	
	SDL_Event notify;
	notify.type         = agen_notify_eventID();
	notify.user.code    = 1;
	notify.user.data1   = group->on_complete;
	notify.user.data2   = group->arg;
	SDL_PushEvent(&notify);
}

agen_task_group_t* agen_task_group_create(void) {
	agen_task_group_t* group = agen_malloc(sizeof(void*), sizeof(agen_task_group_t));
	if (group == NULL) {
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "out of memory");
		return NULL;
	}
	
	SDL_AtomicSet(&group->count, 0);
	group->on_complete = NULL;
	group->arg = NULL;

	return group;
}

int32_t agen_task_group_add(agen_task_group_t* group, agen_queue_t* q, const char* symbol, void* data) {
	SDL_assert(group);
	SDL_assert(symbol);
	SDL_assert(q);

	/* increment ASAP */
	SDL_AtomicIncRef(&group->count);

	agen_task_t* task = agen_task_create_lua(symbol, data, &agen_group_task_complete, group);
	if (task == NULL) {
		SDL_AtomicDecRef(&group->count);
		return -1;
	}

	if (agen_queue_push(q, task)) {
		SDL_AtomicDecRef(&group->count);
		agen_task_destroy(task);
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "add to queue failed");
		return -2;
	}

	return 0;
}
/*
 * wait until all tasks in the group are complete
 */
void agen_task_group_wait(agen_task_group_t* group) {
	SDL_assert(group);
	agen_task_group_status_wait(group);
}

void agen_task_group_spin_lock(agen_task_group_t* group) {
	SDL_assert(group);
	agen_task_group_status_spin_lock(group);
}
/*
 * Push SDL event when task group is empty (all tasks complete).
 * Caller is responsible for keeping notification arg pointer alive.
 */
int32_t
agen_task_group_notify(agen_task_group_t* group, agen_queue_t* q, agen_complete_f f, void* arg) {
	SDL_assert(group);
	SDL_assert(q);
	SDL_assert(f);
    
	group->on_complete = f;
	group->arg = arg;
	/* don't let compiler re-order writes */
	SDL_CompilerBarrier();
	
	/* WARNING: check can run before _add incrents count */
	if (SDL_AtomicGet(&group->count) == 0) {
		/* tasks already complete, execute immediately on caller thread */
		agen_task_group_complete_notify(group);
		return 0;
	}
    
	/* add C task waiting on group->count */
	agen_task_t* barrier =
		agen_task_create_c(&agen_task_group_status_wait, group, &agen_task_group_complete_notify, group);
	
	if (barrier == NULL) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "out of memory");
		return -1;
	}
	
	if (agen_queue_push(q, barrier)) {
		agen_task_destroy(barrier);
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "add to queue failed");
		return -2;
	}

	return 0;
}
/*
 * Wait for currently queued tasks to complete before executing new ones.
 */
int32_t agen_task_group_barrier(agen_task_group_t* group, agen_queue_t* q) {
	SDL_assert(group);
	SDL_assert(q);
    
	/* not implemented */
	SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "agen_task_group_barrier: not implemented");
	return -1;
}

void agen_task_group_destroy(agen_task_group_t* group) {
	SDL_assert(group);
	agen_task_group_wait(group);
	agen_free(group);
}
