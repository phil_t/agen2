/********************************************************************
 *
 * Holds a function symbol from worker.lua and its generic argument
 *
 ********************************************************************/
#ifndef __AGEN_TASK_H__
#define __AGEN_TASK_H__

#include "mpmc.h"

#ifdef __cplusplus
extern "C" {
#endif
    
#define LUAJIT_ENABLE_LUA52COMPAT
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

typedef struct agen_task_t agen_task_t;

agen_task_t* agen_task_create_lua(const char* symbol, void* data, agen_complete_f f, void* arg);
agen_task_t* agen_task_create_c(agen_task_f t, void* data, agen_complete_f f, void* arg);
void agen_task_run(agen_task_t* task, lua_State* L, int error_handler);
void agen_task_destroy(agen_task_t* task);
    
#ifdef __cplusplus
};
#endif

#endif /* __AGEN_TASK_H__ */
