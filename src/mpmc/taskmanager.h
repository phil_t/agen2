/********************************************************************
 *
 * Singleton thread pool of lua threads that can consume a queue of tasks.
 * Caller is the producer pushing tasks to the pool's queue.
 *
 ********************************************************************/
#ifndef __AGEN_TASKMANAGER_H__
#define __AGEN_TASKMANAGER_H__

#include "mpmc.h"
#include <SDL_thread.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ref/unref singleton pool object */
MPMC_API agen_task_manager_t* agen_taskman_ref(void);
MPMC_API void agen_taskman_unref(agen_task_manager_t* aman);
/* start/stop execution */
MPMC_API int32_t agen_taskman_start(agen_task_manager_t* aman, uint32_t max_threads);
MPMC_API int32_t agen_taskman_stop(agen_task_manager_t* aman);

MPMC_API uint32_t agen_taskman_num_threads(agen_task_manager_t* aman, void* null);
MPMC_API uint32_t agen_taskman_is_running(agen_task_manager_t* aman);
/* access on of the 3 priority level communication channels */
MPMC_API agen_queue_t* agen_get_queue(agen_task_manager_t* aman, SDL_ThreadPriority p);

#ifdef __cplusplus
};
#endif

#endif /* __AGEN_TASKMANAGER_H__ */
