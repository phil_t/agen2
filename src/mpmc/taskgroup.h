/*************************************************************************
 *
 * A group of possibly parallel tasks
 *
 *************************************************************************/
#ifndef __AGEN_TASKGROUP_H__
#define __AGEN_TASKGROUP_H__

#include "mpmc.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct agen_task_group_t agen_task_group_t;
MPMC_API agen_task_group_t* agen_task_group_create(void);
/* Note: will block util tasks are complete */
MPMC_API void agen_task_group_destroy(agen_task_group_t* group);

/* add new task to task group */
MPMC_API int32_t agen_task_group_add(agen_task_group_t* group, agen_queue_t* q, const char* symbol, void* data);
/* barrier: don't start executing new tasks in group until previous ones are complete */
MPMC_API int32_t agen_task_group_barrier(agen_task_group_t* group, agen_queue_t* q);

/* suspend calling thread until all tasks are complete */
MPMC_API void agen_task_group_wait(agen_task_group_t* group);
/* spinlock calling thread until all tasks are complete */
MPMC_API void agen_task_group_spin_lock(agen_task_group_t* group);

/* notify SDL events processing thread when all tasks are complete */
MPMC_API int32_t agen_task_group_notify(agen_task_group_t* group, agen_queue_t* q, agen_complete_f f, void* arg);

#ifdef __cplusplus
};
#endif

#endif /* __AGEN_TASKGROUP_H__ */
