/*************************************************************************
 *
 * Thread safe multiple producers / multiple consumers queue.
 *
 *************************************************************************/
#ifndef __AGEN_QUEUE_H__
#define __AGEN_QUEUE_H__

#include "mpmc.h"

#ifdef __cplusplus
extern "C" {
#endif
// task queue api
MPMC_API agen_queue_t* agen_queue_create(uint32_t size);
MPMC_API int32_t agen_queue_push(agen_queue_t* q, void* entry);
MPMC_API void* agen_queue_pop(agen_queue_t* q, uint32_t timeout);
MPMC_API void agen_queue_wait(agen_queue_t* q);
MPMC_API uint32_t agen_queue_num_items(agen_queue_t* q);
void agen_queue_wake(agen_queue_t* q);
MPMC_API void agen_queue_destroy(agen_queue_t* q);

#ifdef __cplusplus
};
#endif
#endif /* __AGEN_QUEUE_H__ */
