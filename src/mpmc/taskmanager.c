#include "taskmanager.h"
#include "queue.h"
#include "task.h"
#include "memalloc.h"

#include <SDL_log.h>
#include <SDL_assert.h>
#include <SDL_atomic.h>
#include <SDL_cpuinfo.h>
#include <SDL_timer.h>

struct agen_task_manager_t {
    SDL_atomic_t   ref;
    SDL_atomic_t   running;
    uint32_t       num_threads;
    SDL_Thread**   threads;
    agen_queue_t*  queue[3];
};

static agen_task_manager_t* manager = NULL;

static uint32_t next_pot(uint32_t x) {
	--x;
	x |= x >>  1;
	x |= x >>  2;
	x |= x >>  4;
	x |= x >>  8;
	x |= x >> 16;
	return ++x;
}

static lua_State* mpmc_create_lua_state(void) {
  lua_State* L = luaL_newstate();

  if (L == NULL) {
    return L;
  }

  lua_gc(L, LUA_GCSTOP, 0);  /* stop gc during initialization */
  luaL_openlibs(L);
  lua_gc(L, LUA_GCRESTART, -1); /* start gc again */

  return L;
}

static int mpmc_lua_error(lua_State* S) {
    const char* message = lua_tostring(S, -1);
    lua_Debug ar;
    // start from level 1, thus skipping the error handler at the current level
    int level = 1;

    if (message == NULL) message = "nil";
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", message);

    if (lua_getstack(S, level, &ar)) {

        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Stack trace:");
        do {
            if (lua_getinfo(S, "Snl", &ar)) {
                SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "%d. call in line %d from %s %s '%s' defined in %s:%d",
                    level,
                    ar.currentline,
                    ar.what     ? ar.what : "unknown",
                    ar.namewhat ? ar.namewhat : "",
                    ar.name     ? ar.name : "noname",
                    ar.short_src,
                    ar.linedefined);
            }

            level++;
        } while (lua_getstack(S, level, &ar));
    }
    return 1;
}

/*
 * Worker thread
 * Will setup a private lua state and require('worker')
 * Lua state will be reused for new tasks. May lead to leaks if tasks leave globals.
 */
static int SDLCALL agen_thread_func(void* data) {
  agen_task_manager_t* aman = SDL_static_cast(agen_task_manager_t*, data);
  agen_task_t* task   = NULL;
  lua_State* S = mpmc_create_lua_state();
  int error_handler = 0;
  int ret = 1;
 
  if (S == NULL) {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "thread pool failed to create thread owned lua state");
    return ret;
  }

  SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "thread pool: worker thread starts");

  lua_pushcfunction(S, &mpmc_lua_error);
  error_handler = lua_gettop(S);
  /*
	 * require('worker')
	 */
  lua_getglobal(S, "require");
  lua_pushliteral(S, "worker");
  /*
   * and run worker globals
   */
  if (lua_pcall(S, 1, LUA_MULTRET, error_handler)) {
    goto WORKER_THREAD_FAILED;
  }
	/*
	 * No GC while processing a task, will force full collection on task switch.
	 */
	lua_gc(S, LUA_GCSTOP, 0);

  while (SDL_AtomicGet(&aman->running) != 0) {
    task = agen_queue_pop(aman->queue[SDL_THREAD_PRIORITY_HIGH], 0);
    if (task != NULL) {
      SDL_SetThreadPriority(SDL_THREAD_PRIORITY_HIGH);
      goto EXEC_TASK;
    }
        
    task = agen_queue_pop(aman->queue[SDL_THREAD_PRIORITY_NORMAL], 0);
    if (task != NULL) {
      SDL_SetThreadPriority(SDL_THREAD_PRIORITY_NORMAL);
      goto EXEC_TASK;
    }

    task = agen_queue_pop(aman->queue[SDL_THREAD_PRIORITY_LOW], 0);
    if (task != NULL) {
      SDL_SetThreadPriority(SDL_THREAD_PRIORITY_LOW);
      goto EXEC_TASK;
    }
    /*
		 * don't spinlock
		 */
    SDL_Delay(1);
    continue;
        
  EXEC_TASK:
		/*
		 * A new task was scheduled, execute it
		 */
    agen_task_run(task, S, error_handler);
    /*
     * clean up lua state for reuse
     */
    lua_settop(S, error_handler);
    agen_task_destroy(task);
    lua_gc(S, LUA_GCCOLLECT, 0);
    /*
		 * restore thread priority to default
		 */
    SDL_SetThreadPriority(SDL_THREAD_PRIORITY_NORMAL);
  }

  ret = 0;
    
WORKER_THREAD_FAILED:
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "thread pool: worker thread quits");
    lua_close(S);
    
    return ret;
}

agen_task_manager_t* agen_taskman_ref(void) {
    if (manager == NULL) {
        manager = agen_malloc(sizeof(void*), sizeof(agen_task_manager_t));
        if (manager == NULL) {
            SDL_LogError(SDL_LOG_CATEGORY_ERROR, "thread pool: out of memory");
            return NULL;
        }
        
        SDL_AtomicSet(&manager->ref, 0);
        SDL_AtomicSet(&manager->running, 0);
        manager->num_threads = 0;
        manager->threads = NULL;
        manager->queue[SDL_THREAD_PRIORITY_LOW] = NULL;
        manager->queue[SDL_THREAD_PRIORITY_NORMAL] = NULL;
        manager->queue[SDL_THREAD_PRIORITY_HIGH] = NULL;
    }
    
    SDL_AtomicIncRef(&manager->ref);
    return manager;
}

int32_t agen_taskman_start(agen_task_manager_t* aman, uint32_t max_threads) {
    const Uint8 name[32];
	Uint32 i = 0;

    SDL_assert(aman);
    
    if (SDL_AtomicGet(&aman->running)) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "thread pool: already running");
        return 0;
    }
    /*
		 * Set running flag
		 */
    SDL_AtomicSet(&aman->running, 1);
    
    aman->num_threads = max_threads;
    if (aman->num_threads == 0) {
        aman->num_threads = SDL_GetCPUCount();
        SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "thread pool: max threads set to [%d]", aman->num_threads);
    }
    /*
     * pre-alloc mem for thread list
     */
    aman->threads = SDL_calloc(aman->num_threads, sizeof(SDL_Thread*));
    if (aman->threads == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "thread pool: out of memory.");
        return -2;
    }
    /*
		 * prepare a separate queue for each priority level
		 */
    aman->queue[SDL_THREAD_PRIORITY_LOW]    = agen_queue_create(next_pot(aman->num_threads * 32));
    aman->queue[SDL_THREAD_PRIORITY_NORMAL] = agen_queue_create(next_pot(aman->num_threads *  8));
    aman->queue[SDL_THREAD_PRIORITY_HIGH]   = agen_queue_create(next_pot(aman->num_threads *  1));
    /*
     * start a worker thread on each logical cpu core
     */
    for (i = 0; i < aman->num_threads; i++) {
        SDL_snprintf((char*)name, 32, "agen_thread_%d", i + 1);
        aman->threads[i] = SDL_CreateThread(&agen_thread_func, (const char*)name, aman);
        if (aman->threads[i] == NULL) {
            SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "thread pool: failed to create worker thread: %s.", SDL_GetError());
        }
    }
    
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "thread pool: started [%d] worker threads.", aman->num_threads);
    
	return 0;
}

int32_t agen_taskman_stop(agen_task_manager_t* aman) {
    int threadExitCode = 0;
    Uint32 i = 0;

    SDL_assert(aman);
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "thread pool: waiting for worker threads to exit");
    /*
     * set flag for threads to quit
     */
    SDL_AtomicSet(&aman->running, 0);
    /*
     * wake threads waiting forever on semaphore to check 'running'
     *
    for (i = 0; i < aman->num_threads; i++) {
        agen_queue_wake(aman->queue);
    }
     */
    /*
     * wait for worker threads to complete
     */
    for (i = 0; i < aman->num_threads; i++) {
        SDL_WaitThread(aman->threads[i], &threadExitCode);
        if (threadExitCode) {
            SDL_LogError(SDL_LOG_CATEGORY_ERROR, "thread pool: worker thread exited with code [%d].", threadExitCode);
        }
    }
    
    agen_queue_destroy(aman->queue[SDL_THREAD_PRIORITY_LOW]);
    agen_queue_destroy(aman->queue[SDL_THREAD_PRIORITY_NORMAL]);
    agen_queue_destroy(aman->queue[SDL_THREAD_PRIORITY_HIGH]);

    SDL_free(aman->threads);
    
    return 0;
}

uint32_t agen_taskman_num_threads(agen_task_manager_t* aman, void* null) {
    SDL_assert(aman != NULL);
    return aman->num_threads;
}

agen_queue_t* agen_get_queue(agen_task_manager_t* aman, SDL_ThreadPriority p) {
    SDL_assert(aman);
    SDL_assert(aman->queue);
    return aman->queue[p];
}

uint32_t agen_taskman_is_running(agen_task_manager_t* aman) {
    SDL_assert(aman);
    return SDL_AtomicGet(&aman->running);
}

void agen_taskman_unref(agen_task_manager_t* aman) {
	SDL_assert(aman);
	
	if (SDL_AtomicDecRef(&aman->ref) != SDL_TRUE) {
		/* value is non-0 */
		return;
	}

	if (SDL_AtomicGet(&aman->running) != 0) {
		SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Stopping thread pool...");
		agen_taskman_stop(aman);
	}
	
	SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Releasing thread pool...");
	agen_free(aman);
}
