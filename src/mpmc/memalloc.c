#include "memalloc.h"

#if defined(_WIN32)
# include <malloc.h>
#else
# include <stdlib.h>
#endif

void* agen_malloc(uint32_t align, size_t size) {
#ifdef _MSC_VER
    return _aligned_malloc(size, align);
#else
    void* ret = NULL;
    return posix_memalign(&ret, align, size) ? NULL : ret;
#endif
}

void agen_free(void* ptr) {
#ifdef _MSC_VER
    _aligned_free(ptr);
#else
    free(ptr);
#endif
}
