#ifndef __MPMCCONF_H__
#define __MPMCCONF_H__

#include <SDL_stdinc.h>

#if defined(_MSC_VER)
# ifdef mpmc_EXPORTS
#   define MPMC_API			__declspec(dllexport)
# else
#   define MPMC_API			__declspec(dllimport)
# endif
# define MPMC_ALIGN(x)		__declspec(align(x))
#elif defined(__GNUC__) || defined(__clang__)
# define MPMC_API			__attribute__((visibility("default")))
# define MPMC_ALIGN(bytes)	__attribute__((aligned(bytes)))
#else
# define MPMC_API extern
#endif

typedef void (*agen_complete_f)(void* arg);
typedef void (*agen_task_f)(void* arg);

typedef struct agen_queue_t agen_queue_t;
typedef struct lua_State lua_State;
typedef struct agen_task_manager_t agen_task_manager_t;

#endif /* __MPMCCONF_H__ */
