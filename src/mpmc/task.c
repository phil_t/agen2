#include <SDL_log.h>
#include <SDL_assert.h>

#include "task.h"

typedef enum {
    AGEN_TASK_NONE = 0,
    AGEN_TASK_C,
    AGEN_TASK_LUA,
} agen_task_type_t;

struct agen_task_t {
    agen_task_type_t type;
    union {
        struct {
            const char*	symbol;
            void*       data;
        } lua;
        struct {
            agen_task_f f;
            void* data;
        } c;
    } args;
	agen_complete_f on_complete;
	void*       arg;
};

agen_task_t* agen_task_create_lua(const char* symbol, void* data, agen_complete_f f, void* arg) {
	SDL_assert(symbol);
	SDL_assert(f);
	
	agen_task_t* ret = SDL_malloc(sizeof(agen_task_t));
	if (ret == NULL) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Out of memory");
		return NULL;
	}

	ret->type            = AGEN_TASK_LUA;
	ret->args.lua.symbol = symbol;
	ret->args.lua.data   = data;
	ret->on_complete     = f;
	ret->arg             = arg;
	return ret;
}

agen_task_t* agen_task_create_c(agen_task_f task_func, void* data, agen_complete_f notify_func, void* arg) {
    SDL_assert(task_func);
    SDL_assert(notify_func);
    
    agen_task_t* ret = SDL_malloc(sizeof(agen_task_t));
    if (ret == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Out of memory");
        return NULL;
    }
    
    ret->type        = AGEN_TASK_C;
    ret->args.c.f    = task_func;
    ret->args.c.data = data;
    ret->on_complete = notify_func;
    ret->arg         = arg;
    return ret;
}

void agen_task_run(agen_task_t* task, lua_State* L, int errHandler) {
    SDL_assert(task);
    
    switch (task->type) {
        case AGEN_TASK_LUA: {
            SDL_assert(L);

            lua_getglobal(L, task->args.lua.symbol);
            if (lua_isnil(L, -1)) {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "worker thread: global symbol %s not found", task->args.lua.symbol);
                break;
                
            }
						else if (!lua_isfunction(L, -1)) {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "worker thread: global symbol %s not a lua function", task->args.lua.symbol);
                break;
            }
            /*
             * arg can be cdata only
             */
            lua_pushlightuserdata(L, task->args.lua.data);
            if (lua_pcall(L, 1, 0, errHandler)) {
                break;
            }
            
            break;
        }
        
        case AGEN_TASK_C:
            (*task->args.c.f)(task->args.c.data);
            break;
            
        default:
            /* shouldn't happen */
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "FIXME: invalid task type");
            break;
    }
    /*
     * always run on_complete handler
     */
    (*task->on_complete)(task->arg);
}

void agen_task_destroy(agen_task_t* task) {
    SDL_assert(task);
    SDL_free(task);
}

