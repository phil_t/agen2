use std::env;
extern crate sdl2;
use sdl2::sys::{SDL_LogSetAllPriority, SDL_LogPriority::SDL_LOG_PRIORITY_VERBOSE};
mod agen;
 
fn main() {
    let args = env::args()
        .collect();

    unsafe {
        SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);
    };

    let res = agen::init(args);
    match res {
        Ok(engine) => {
            let res = engine.run();
            match res {
                Ok(_) => return (),
                Err(err) => {
                    sdl2::log::log(&err.to_string());
                }
            }
        },
        Err(err) => {
            sdl2::log::log(&err);
        }
    }
}
