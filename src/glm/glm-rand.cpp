#include "glm.hpp"
#include <glm/vec3.hpp>
#include <glm/gtc/random.hpp>

GLM_API double random_gaussian(double mean, double dev) {
	return glm::gaussRand(mean, dev);
}

GLM_API double random_linear(double min, double max) {
	return glm::linearRand(min, max);
}

GLM_API void random_spherical(glm::vec3* const v, double radius) {
	*v = glm::sphericalRand(radius);
}
