#include "glm.hpp"

#if __x86_64__ || __i386__  // TODO: does it work on msvc?
#if defined(_MSC_VER)
#include <malloc.h>
#else
#include <stdlib.h>
#endif
#include <string.h>

#define GLM_FORCE_SSE42
#define FUNCNAME(func)		GLM_FUNCNAME(mat4, func, sse2)
#include "glm-mat3D.hpp"
#endif
