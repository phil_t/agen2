#include "glm.hpp"

#if defined(_MSC_VER)
#include <stdint.h>
#endif
#include <string.h>

#define GLM_FORCE_PURE
#define FUNCNAME(func)		GLM_FUNCNAME(mat4, func, pure)
#include "glm-mat3D.hpp"
