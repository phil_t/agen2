#ifndef __AGEN_GLM_HPP__
#define __AGEN_GLM_HPP__

#if defined(__GNUC__) || defined(__clang__)
# define GLM_API extern "C" __attribute__((visibility("default")))
#elif defined(_MSC_VER)
# define GLM_API extern "C" __declspec(dllexport)
#else
# define GLM_API extern "C"
#endif

#define GLM_FUNCNAME(prefix, func, suffix)		prefix ## _ ## func ## _ ## suffix

#define GLM_PRECISION_HIGHP_FLOAT	// match shader precision
#define GLM_PRECISION_HIGHP_INT		// match shader precision
#ifndef __linux__
#define GLM_FORCE_INLINE			// force inlining
#else
//#pragma message "GLM_FORCE_INLINE not used"
#endif
#define GLM_FORCE_EXPLICIT_CTOR		// don't cast implicitly
#define GLM_FORCE_SIZE_T_LENGTH		// type.size() returns size_t instead of int
#define GLM_ENABLE_EXPERIMENTAL
// Enabling swizzle operators will massively increase the
// size of compiled files and the compilation time.
//#define GLM_FORCE_SWIZZLE

#endif // __AGEN_GLM_HPP__
