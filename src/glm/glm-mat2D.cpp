#include "glm.hpp"
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

GLM_API void mat2d_identity(glm::mat3* const self) {
	*self = glm::mat3(1.0f);
}

GLM_API void mat2d_translate(glm::mat3* const self, float x, float y) {
	*self = glm::translate(*self, glm::vec2(x, y));
}

GLM_API void mat2d_rotate(glm::mat3* const self, float rad) {
	*self = glm::rotate(*self, rad);
}

GLM_API void mat2d_scale(glm::mat3* const self, float x, float y) {
	*self = glm::scale(*self, glm::vec2(x, y));
}

GLM_API void mat2d_shearX(glm::mat3* const self, float factor) {
	*self = glm::shearX(*self, factor);
}

GLM_API void mat2d_shearY(glm::mat3* const self, float factor) {
	*self = glm::shearY(*self, factor);
}

GLM_API void mat2d_multiply(glm::mat3* const self, const glm::mat3* left) {
	*self = (*self) * (*left);
}

GLM_API float* mat2d_value_ptr(glm::mat3* const self) {
	return glm::value_ptr(*self);
}

GLM_API size_t mat2d_tostring(const glm::mat3* const mat, char* dest, size_t len) {
	std::string str = glm::to_string(*mat);
	size_t needs = str.size() + 1;
	if (needs < len) return needs;
#ifdef _MSC_VER
	strncpy_s(dest, len, str.c_str(), needs);
#elif defined __linux__
	strncpy(dest, str.c_str(), len);
#else
	strlcpy(dest, str.c_str(), len);
#endif
	return 0;
}
