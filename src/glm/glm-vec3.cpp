#include "glm.hpp"
#include <glm/vec3.hpp>
#include <glm/gtx/string_cast.hpp>

#include <stdint.h>
#include <string.h>
#include <exception>
/////////////////////////////////////////////////////////////////////////////////
//
// vec3
//
/////////////////////////////////////////////////////////////////////////////////
GLM_API glm::vec3* vec3_create(float x, float y, float z) {
  try {
	  return new glm::vec3(x, y, z);
  }
  catch (std::exception& e) {
    // let the caller handle memory allocation failures
    return nullptr;
  };
}

GLM_API size_t vec3_tostring(glm::vec3* v, uint8_t* data, size_t len) {
	std::string str = glm::to_string(*v);
	size_t needs = str.size();
	if (needs < len) return needs;
#ifdef _MSC_VER
	strncpy_s((char*)data, len, str.c_str(), needs);
#elif defined __linux__
	strncpy((char*)data, str.c_str(), len);
#else
	strlcpy((char*)data, str.c_str(), len);
#endif
	return 0;
}

GLM_API bool vec3_equal(glm::vec3* self, const glm::vec3* v) {
	return (*self) == (*v);
}

GLM_API void vec3_free(glm::vec3* v) {
	delete v;
}
