#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

GLM_API void FUNCNAME(identity)(glm::mat4* const mat) {
	*mat = glm::mat4(1.0f);
}
///////////////////////////////////////////////////////////////////////
//
// projections
//
///////////////////////////////////////////////////////////////////////
GLM_API void FUNCNAME(ortho)(glm::mat4* const mat, float left, float right, float bottom, float top, float zNear, float zFar) {
	assert(zFar > zNear);
	*mat = glm::ortho(left, right, bottom, top, zNear, zFar);
}

GLM_API void FUNCNAME(perspective)(glm::mat4* const mat, float FOV, float width, float height, float zNear, float zFar) {
	assert(zFar > zNear);
	*mat = glm::perspectiveFov(FOV, width, height, zNear, zFar);
}

GLM_API void FUNCNAME(frustum)(glm::mat4* const mat, float left, float right, float bottom, float top, float zNear, float zFar) {
	assert(zFar > zNear);
	*mat = glm::frustum(left, right, bottom, top, zNear, zFar);
}

GLM_API void FUNCNAME(lookAt)(glm::mat4* const mat, const glm::vec3* eye, const glm::vec3* center, const glm::vec3* up) {
	*mat = glm::lookAt(*eye, *center, *up);
}

GLM_API void FUNCNAME(multiply)(glm::mat4* const mat, const glm::mat4* right) {
	*mat = (*mat) * (*right);
}

GLM_API void FUNCNAME(model_view_proj)(glm::mat4* const mat, const glm::mat4* model, const glm::mat4* view, const glm::mat4* proj) {
	*mat = (*proj) * (*view) * (*model);
}

GLM_API float* FUNCNAME(ptr)(glm::mat4* const mat) {
	return glm::value_ptr(*mat);
}
//////////////////////////////////////////////////////////////////////
//
// transformations
//
//////////////////////////////////////////////////////////////////////
GLM_API void FUNCNAME(translate)(glm::mat4* const mat, const glm::vec3* const v) {
	*mat = glm::translate(*mat, *v);
}

GLM_API void FUNCNAME(rotate)(glm::mat4* const mat, float rad, const glm::vec3* const axis) {
	*mat = glm::rotate(*mat, rad, *axis);
}

GLM_API void FUNCNAME(scale)(glm::mat4* const mat, const glm::vec3* const factor) {
	*mat = glm::scale(*mat, *factor);
}

GLM_API void FUNCNAME(transpose)(glm::mat4* const mat) {
	*mat = glm::transpose(*mat);
}

GLM_API void FUNCNAME(inverse)(glm::mat4* const mat) {
	*mat = glm::inverse(*mat);
}

GLM_API void FUNCNAME(inverse_transpose)(glm::mat4* const mat) {
	*mat = glm::inverseTranspose(*mat);
}

GLM_API bool FUNCNAME(compare)(const glm::mat4* const mat1, const glm::mat4* const mat2) {
  return *mat1 == *mat2;
}
///////////////////////////////////////////////////////////////
//
// to string
//
///////////////////////////////////////////////////////////////
GLM_API size_t FUNCNAME(tostring)(const glm::mat4* const mat, char* dest, size_t len) {
	std::string str = glm::to_string(*mat);
	size_t needs = str.size() + 1;
	if (needs < len) return needs;
#ifdef _MSC_VER
	strncpy_s(dest, len, str.c_str(), needs);
#elif defined __linux__
	strncpy(dest, str.c_str(), len);
#else
	strlcpy(dest, str.c_str(), len);
#endif
	return 0;
}
