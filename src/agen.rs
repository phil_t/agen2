use std::env;
use std::vec::Vec;
use std::os::raw::c_int;

extern crate sdl2;
use sdl2::{Sdl, EventPump};
use sdl2::event::EventType;
use sdl2::video::Window;
use sdl2::sys::{SDL_EventState, SDL_WindowFlags, SDL_DISABLE, SDL_EventType::SDL_SYSWMEVENT, Uint32};

extern crate mlua;
use mlua::prelude::*;
use mlua::Error::RuntimeError;

static mut AGEN_EVENT_TYPE_TASK_NOTIFY_ID: u32 = 0;
static mut AGEN_EVENT_TYPE_TIMER_FIRED_ID: u32 = 0;
pub struct Engine {
    lua_state:  Lua,
    sdl_window: Window,
}

impl Engine {
    //
    // require('boot'), call _agen_init(cmdargs) and creare a SDL window
    // from the returned config (lua table)
    //
    fn new(sdl_context: &Sdl, args: &Vec<String>) -> Result<Self, LuaError> {
        let lua = unsafe { Lua::unsafe_new() };
        let window: Window = {
            let globals = lua
                .globals();

            // compile boot.lua
            globals
                .get::<&str, LuaFunction>("require")?
                .call::<&str, ()>("boot")?;

            // create lua table from cmdline args
            let args_table = lua
                .create_table()?;
            for (i, arg) in args.iter().skip(1).enumerate() {
                sdl2::log::log(&format!("arg [{}] {}", (i + 1), &arg));
                args_table
                    .raw_seti::<&str>(i + 1, &arg)?;
            }
            //
            // call _agen_init with the cmdline args as a table
            //
            let config = globals
                .get::<&str, LuaFunction>("_agen_init")?
                .call::<LuaTable, LuaTable>(args_table)?;
            //
            // find config.video
            //
            let video = config
                .get::<&str, LuaTable>("video")?;
            //
            // Create SDL window from config
            //
            Self::_create_window(sdl_context, video)?
        };
        //
        // 3. create The Engine
        //
        Ok(Self {
            lua_state: lua,
            sdl_window: window,
        })
    }

//   pub fn window(&self) -> *mut SDL_Window {
//       self.sdl_window::raw();
//   }

    fn _create_window(sdl_context: &Sdl, video: LuaTable) -> Result<Window, LuaError> {
        // sdl2: init video
        let res = sdl_context
            .video();
        let video_subsystem = match res {
            Ok(v) => v,
            Err(msg) => {
                return Err(RuntimeError(msg))
            }
        };
        sdl2::log::log(&format!("Initialized video driver {}", video_subsystem.current_video_driver()));

        let mut flags = SDL_WindowFlags::SDL_WINDOW_RESIZABLE     as u32 |
                        SDL_WindowFlags::SDL_WINDOW_ALLOW_HIGHDPI as u32;
        //
        // find config.video.driver
        //
        let driver = video
            .get::<&str, String>("driver")?;
        match driver.to_lowercase().as_str() {
            "gl"     => {
                flags |= SDL_WindowFlags::SDL_WINDOW_OPENGL as u32;
                sdl2::log::log("requested GL-capable window");
            },
            "vulkan" => {
                flags |= SDL_WindowFlags::SDL_WINDOW_VULKAN as u32;
                sdl2::log::log("requested Vulkan-capable window");
            },
            "metal"  => {
                flags |= SDL_WindowFlags::SDL_WINDOW_METAL  as u32;
                sdl2::log::log("requested Metal-capable window");
            },
            &_       => {},
        };

        // config.video.width
        let w = video
            .get::<&str, i32>("width")
            .unwrap_or(1024);
        // config.video.height
        let h = video
            .get::<&str, i32>("height")
            .unwrap_or(768);
        // config.video.windowed
        let windowed = video
            .get::<&str, bool>("windowed")
            .unwrap_or(true);
        if !windowed {
            flags |= SDL_WindowFlags::SDL_WINDOW_FULLSCREEN_DESKTOP as u32;
        }

        let res = video_subsystem
            .window("Agen 2", w as u32, h as u32)
            .set_window_flags(flags)
            .build();
        let window = match res {
            Ok(w) => w,
            Err(err) => {
                return Err(RuntimeError(err.to_string()))
            },
        };
        // if using desktop res update width / height
        if (window.window_flags() & (SDL_WindowFlags::SDL_WINDOW_FULLSCREEN_DESKTOP as u32)) != 0 {
            let res = window
                .display_mode();
            match res {
                Ok(mode) => {
                    video
                        .raw_set::<&str, i32>("width", mode.w)?;
                    video
                        .raw_set::<&str, i32>("height", mode.h)?;
                    video
                        .raw_set::<&str, i32>("refresh", mode.refresh_rate)?;
                },
                Err(msg) => {
                    sdl2::log::log(&msg);
                }
            }
        }
        Ok(window)
    }

    pub fn run(&self) -> mlua::Result<()> {
        // call _agen_main
        self
            .lua_state
            .globals()
            .get::<&str, LuaFunction>("_agen_main")?
            .call::<u32, ()>(self.sdl_window.id())
    }
}

impl Drop for Engine {
    fn drop(&mut self) {
        sdl2::log::log("calling _agen_quit...");
        let res = self
            .lua_state
            .globals()
            .get::<&str, LuaFunction>("_agen_quit");
        match res {
            Ok(func) => {
                let res = func.call::<(), ()>(());
                match res {
                    Ok(_) => return,
                    Err(err) => {
                        sdl2::log::log(&format!("error calling _agen_quit: {}", &err.to_string()));
                        return ()
                    }
                }
            },
            Err(err) => {
                sdl2::log::log(&format!("error calling _agen_quit: {}", &err.to_string()));
                return ()
            }
        }
    }
}

pub fn init(args: Vec<String>) -> Result<Engine, String> {
    // init SDL2
    let init_res = sdl2::init();
    let sdl_context = match init_res {
        Ok(sdl_context) => sdl_context,
        Err(error) => return Err(error),
    };

    let events_res = _init_events(&sdl_context);
    match events_res {
        Ok(_) => (),
        Err(msg) => return Err(msg),
    }

    _set_hints();

    let platform = sdl2::get_platform();
    sdl2::log::log(&format!("running on {}", platform));

    // setup lua env
    let bp_res = sdl2::filesystem::base_path();
    match bp_res {
        Ok(base_path) => {
            let cpath = base_path.clone() + _lua_cpath();
            sdl2::log::log(&format!("LUA_CPATH set to {}", &cpath));
            env::set_var("LUA_CPATH", cpath);

            let lpath = base_path.clone() + &"?.lua;" +
                       &base_path.clone() + &"lua/?.lua;" +
                       &base_path.clone() + &"../share/agen/?.lua;" +
                       &base_path.clone() + &"../share/agen/lua/?.lua;";
            sdl2::log::log(&format!("LUA_PATH set to {}", &lpath));
            env::set_var("LUA_PATH", lpath);
        },
        Err(_) => {
            sdl2::log::log("no base path found, using lua pre-defined values")
        }
    };

    let res = Engine::new(&sdl_context, &args);
    match res {
        Ok(engine) => return Ok(engine),
        // TODO: stack trace
        Err(err) => return Err(format!("lua error: {}", &err.to_string())),
    }
}

fn _init_events(sdl_context: &Sdl) -> Result<EventPump, String> {
    // init SDL2 events
    let init_event_res = sdl_context
        .event();
    let event_subsystem = match init_event_res {
        Ok(event_sys) => event_sys,
        Err(error) => {
            return Err(error)
        }
    };
    //
    // register custom events for SDL timer firing and mpmc
    // task complete (callbacks on another thread)
    //
    unsafe {
        let res = event_subsystem
            .register_event();
        match res {
            Ok(eid) => AGEN_EVENT_TYPE_TASK_NOTIFY_ID = eid,
            Err(msg) => {
                return Err(msg)
            }
        }

        let res = event_subsystem
            .register_event();
        match res {
            Ok(eid) => AGEN_EVENT_TYPE_TIMER_FIRED_ID = eid,
            Err(msg) => {
                return Err(msg)
            }
        }
    };

    let res = sdl_context
        .event_pump();
    match res {
        Ok(mut event_pump) => {
            unsafe {
                SDL_EventState(SDL_SYSWMEVENT as u32, SDL_DISABLE as c_int);
            };
            // most events are disabled by default and need to be enabled from lua
            let disabled_events = [
                // EventType::SysWmEvent,   // not in sdl2 yet
                EventType::KeyDown,
                EventType::KeyUp,
                EventType::TextEditing,
                EventType::TextInput,
                EventType::MouseButtonDown,
                EventType::MouseButtonUp,
                EventType::MouseMotion,
                EventType::MouseWheel,
                EventType::FingerDown,
                EventType::FingerUp,
                EventType::FingerMotion,
                EventType::DollarGesture,
                EventType::DollarRecord,
                EventType::MultiGesture,
            ];

            for e in disabled_events {
                event_pump
                    .disable_event(e);
            }
            Ok(event_pump)
        },
        Err(msg) => {
            sdl2::log::log(&msg);
            Err(msg)
        }
    }
}


fn _set_hints() {
   // set SDL2 hints
    let hints = [
        // This is specially useful if you build SDL against a non glibc libc library (such as musl) 
        // which provides a relatively small default thread stack size (a few kilobytes versus the 
        // default 8 MB glibc uses).
        ("SDL_HINT_THREAD_STACK_SIZE", "0"), // in bytes
        // macos
        ("SDL_HINT_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK", "1"),
        ("SDL_HINT_MAC_BACKGROUND_APP", "0"),
        ("SDL_HINT_VIDEO_HIGHDPI_DISABLED", "0"),
        ("SDL_HINT_MAC_OPENGL_ASYNC_DISPATCH", "1"),
        // windows
        ("SDL_HINT_TIMER_RESOLUTION", "1"),
        ("SDL_HINT_XINPUT_ENABLED", "1"),
        ("SDL_HINT_WINDOWS_ENABLE_MESSAGELOOP", "1"),
        ("SDL_HINT_WINDOWS_NO_CLOSE_ON_ALT_F4", "0"),
        // linux
        ("SDL_HINT_VIDEO_X11_XRANDR", "1"),
        ("SDL_HINT_X11_WINDOW_TYPE", "_NET_WM_WINDOW_TYPE"),
        ("SDL_HINT_VIDEO_WAYLAND_PREFER_LIBDECOR", "1"),
    ];

    for h in hints {
        if sdl2::hint::set(h.0, h.1) != true {
            sdl2::log::log(&format!("failed to set hint {}", h.0));
        }
    }
}

#[cfg(target_os = "macos")]
fn _lua_cpath() -> &'static str {
    "lib/?.dylib"
}

#[cfg(target_os = "windows")]
fn _lua_cpath() -> &'static str {
    "lib\\?.dll"
}

#[cfg(target_os = "linux")]
fn _lua_cpath() -> &'static str {
    "lib/?.so"
}
//
// C ABI exports
//
#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn agen_notify_eventID() -> Uint32 {
    return unsafe { AGEN_EVENT_TYPE_TASK_NOTIFY_ID as Uint32 }
}

#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn agen_timer_eventID() -> Uint32 {
    return unsafe { AGEN_EVENT_TYPE_TIMER_FIRED_ID as Uint32 }
}
/*
#[no_mangle]
pub extern "C" pub fn agen_sdl_timer_callback(interval: Uint32, param: *mut c_void) -> Uint32 {
    let event = sdl2::event::Event::User {
        timestamp: 0,
        window_id: 0,
        type_: custom_event_type_id,
        code: 1,    // AGEN_LUA_NOTIFY
        data1: param,
        data2: 0x00000000 as *mut c_void,
    };

    ev.push_event(event);
	return interval
}
*/
