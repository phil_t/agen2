-----------------------------------------------------------------------
--
-- worker.lua - loaded by each worker thread in a private state.
-- Every function can be called in parallel from the main thread.
--
-- To report results back to the main thread:
--  * pass shared C pointer as arg and cast it appropriately
--  * push a user SDL event and handle it in main.lua
--  * communicate to main thread via private agen queue
-- Lua data structures can be serialized/deserialized with MessagePack
--
-- Tips on thread priorities:
-- * if the main thread is waiting for results from this sub per frame
-- make it highest priority
-- * if it's a multi-frame operation like disk I/O use low priority.
--
------------------------------------------------------------------------
local ffi = require('ffi')
local sdl = require('sdl2.lib')
local queue = require('agen.util.queue')
-------------------------------------------------------------------
--
-- Note: This is a long living thread, global changes will persist.
--
-------------------------------------------------------------------
--- Generate greeting message and return it to caller
-- @param arg lightuserdata ptr to queue
function hello_world(arg)
  --
  -- Create a GC'ed Lua cdata object around the void* arg. arg is
  -- owned by the calling thread and should not be freed in this thread.
  -- queue:create adds a finalizer, queue:cast doesn't.
  --
  local q = queue:cast(arg)
  --
  -- Don't push finalized cdata objects in the queue since no copy is made 
  -- and pointer can be invalidated by the GC in this thread before or while 
  -- being accessed from the caller.
  -- Push copies or non-GC'ed cdata.
  -- To return a lua object serialize it.
  --
  local ret = sdl.SDL_malloc(14)
  ffi.copy(ret, "Hello, world!", 14)
  --
  -- Store message in queue, caller will fetch, process and free.
  --
  q:push(ret)
end
--- Load image data from file
-- @param arg image file
function load_image(arg)
  -- TODO
end
