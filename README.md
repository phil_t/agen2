# Agen2 - video games framework in lua

Agen2 is the second attempt at designing a games framework. Lessons learned from Agen1:

* Interaction with the OS should be handled by a middleware. Agen1 implements and maintains window handling, event handling and timers for each platform. Agen2 uses SDL2 for that.
* Inspecting the source code, fixing bugs and adding new features is now much easier. Agen1 is a C++ application and includes a custom lua 5.1 build. Agen2 is implemented in LuaJIT + FFI and existing C ABI and LuaJIT libraries can be used directly.
* Agen1 provides a high level rendering API which is convinient for fast prototyping, but does not help with complex models. Agen2 exports a lower level of rendering API leaving the higher level implementation to the game developer.
* The rendering loop is in lua and can be customized and optimized per game.

Supported platforms
Agen2 works on Windows7 and up, MacOSX 10.7 and up and linux. Not tested: iOS, Android.

* Design of each subsystem
- Video subsystem
## Lowest level - simple 3D rendering abstraction supporting different backends
* DX11
* OpenGL 3.3+
* Incomplete: Metal
* Incomplete: DX12 
* Incomplete: Vulkan

* Video Device
  Resources factory
    - Buffer  - static data or host -> GPU or GPU -> host data transfer buffer.
    - Texture - pixel data.
    - Sampler - texture sampling rules.
    - Shader  - vertex, fragment and geometry shaders. TODO: add tesellation shaders support.

* Render State
  Encapsulates rendering pipeline state and serves as a Render and Compute Pass factory.
    - shaders
    - buffer format descriptors for the shaders
    - blending state
    - rendering state
    - render and compute passes (can run in parallel)

* Render Pass
  Encapsulates a command buffer with rendering commands
  that draw into its designated Render Target.
  References its framebuffer, vertex/index buffers, textures and samplers.

* Compute pass
  Encapsulates a command buffer with compute commands.
  References its buffers and textures.

* Render Target
  Backbuffer or texture(s) owned by Render Pass. Can have multiple color and one depth and stencil attachments.

* Logic
  - Device object singleton is instantiated first. It's used as Resources and Render States factory. Each Render State encapsulates specific rendering pipeline state incl. shaders with corresponding vertex buffers description and various
  state settings. It also creates one default Render Pass used to issue rendering commands to the GPU. Render Passes can
  issue rendering commands in parallel or serially.

## Middle level - scene graph traversing with state change tracking and draw calls grouping
* Scene
  - init with data or file
* Scene node with properties
  - geometry
  - light
  - camera
  - material
  - ...
* SceneRenderer
  - init with device
  - present scene

## High level - high level drawing API - borrowed. Multiple high level APIs can co-exist
* love2d-0.10.0

## Others
* Rendering backends
  - DirectX 11
  - OpenGL 3.3+
  - Metal      - incomplete
  - DirectX 12 - incomplete
  - Vulkan     - incomplete

- Audio subsystem
## Lowest level - simple 3D audio abstraction supporting different backends
* OpenAL
* XAudio2

- Compute subsystem
## Lowest level
* Compute backends
  - DirectCompute
  - OpenGL
  - Vulkan
  - Metal

* 3rd party libs used
  - SDL2     - OS interaction       libsdl.org
  - assimp   - model loader         assimp.org
  - liblfds  - non-locking queue    liblfds.org
  - Box2D    - 2D physics           box2d.org
  - SDL2_image
  ** libpng   - png decoder
  ** libjpeg  - jpeg decoder
  ** zlib
  - SDL2_ttf
  ** freetype - font rendering      freetype.org

* TODO: included 3rd party luajit bindings
* TODO: usage example
* TODO: build instructions
