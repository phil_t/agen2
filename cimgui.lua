local ffi   = require('ffi')
local bit   = require('bit')
local math3D   = require('agen.util.math')
local sdl      = require('sdl2')
local keyboard = require('sdl2.input.keyboard')
local mouse    = require('sdl2.input.mouse')
local cimgui   = require('sdl2.cimgui')
local rStateFactory = require('agen.renderState.factory')

local bgcolor = ffi.new('float[4]', {0x2e / 0xff, 0x2e / 0xff, 0x2e / 0xff, 1})

function agen.update(dt)
  --
  -- track window size and input changes
  --
  cimgui.update()
end

function agen.release()
  agen.log.debug('application', "release!")
  --[[
  profiler.stop()
  --
  -- print results
  --
  --
  local n = 1
  for funcName, nCalls, execTime, codeLine in profiler.query("time", 10) do
    agen.log.debug("application", "%d.'%s'x%d time:%d (%s)", n, funcName, nCalls, execTime, codeLine)
    n = n + 1
  end
  profiler.reset()
  ]]
  cimgui.shutdown()
end
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets = {
    shaders = {
      vertex = {},
      pixel  = {},
    },
    buffers = {
      vertex = {},
      index = {},
      uniform = {},
    },
	}
  --
  -- Load shaders
  --
  local dir = "assets/shaders/" .. device:getName()
  assets.shaders.vertex[1] = agen.io.contents(dir .. "/cimgui.vert")
  assets.shaders.pixel[1]  = agen.io.contents(dir .. "/cimgui.frag")
  --
  -- cimgui buffers
  --
  assets.buffers.vertex[1] = device:createBuffer({
    purpose = 'vertex',
    usage   = 'dynamic',
    size    = 16 * 1024 * ffi.sizeof('ImDrawVert'),
    ctype   = 'ImDrawVert',
  })

  assets.buffers.index[1] = device:createBuffer({
    purpose = 'index',
    usage   = 'dynamic',
    size    = 32 * 1024 * ffi.sizeof('ImDrawIdx'),
    ctype   = 'ImDrawIdx',
  })

  assets.buffers.uniform[1] = device:createBuffer({
    purpose = 'uniform',
    usage   = 'dynamic',
    size    = ffi.sizeof('float[16]'),
    ctype   = 'float',
  })
  return assets
end
---------------------------------------------------------------------
--
-- per-frame vars
--
---------------------------------------------------------------------
local axisX = math3D.vec3.create(1, 0, 0)
local axisY = math3D.vec3.create(0, 1, 0)
local axisZ = math3D.vec3.create(0, 0, 1)
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0
--
----------------------------------------------------------------------
local open = ffi.new('bool[1]', {[0] = 1})
function agen.render(device)
  --
  -- render gui
  --
  do
    local rState = device:getRenderState('imgui')
    local pass = rState:getRenderPass('default')
    local io = cimgui.igGetIO()
    --
    -- Avoid rendering when minimized
    --
    if io.DisplaySize.x == 0 or io.DisplaySize.y == 0 then
      return
    end
    --
    -- generate vertices
    --
    cimgui.igNewFrame()
    --cimgui.igShowUserGuide()
    --cimgui.igShowStyleEditor(nil)
    cimgui.igShowDemoWindow(open)
    --cimgui.igShowMetricsWindow(nil)
    cimgui.igRender()
    --
    -- render
    --
    local drawData = cimgui.igGetDrawData()
    assert(drawData ~= nil, "Call after Render(), but before NewFrame()")
    assert(drawData.Valid, "Call after Render(), but before NewFrame()")
    --
    -- scale to framebuffer res (hi DPI)
    --
    --local fb_width  = math.floor(io.DisplaySize.x * io.DisplayFramebufferScale.x)
    --local fb_height = math.floor(io.DisplaySize.y * io.DisplayFramebufferScale.y)
    drawData:scaleClipRects(io.DisplayFramebufferScale)
    --
    -- update buffers
    --
    drawData:updateBuffers(pass:getVertexBuffer(0), pass:getIndexBuffer())
    --
    -- render
    --
    rState:setActive()
    pass:begin()
    pass:clear()

    local vOffset, iOffset = 0, 0
    local cmdList, drawCmd

    for i = 0, drawData.CmdListsCount - 1, 1 do
      cmdList = drawData.CmdLists[i]
      local iDrawOffset = iOffset
      for j = 0, cmdList:getCmdSize() - 1, 1 do
        drawCmd = cmdList:getCmdPtr(j)
        if drawCmd.UserCallback ~= nil then
          drawCmd.UserCallback(cmdList, drawCmd)
        else
          if drawCmd.ClipRect ~= nil and
             drawCmd.ClipRect.z > 0 and 
             drawCmd.ClipRect.w > 0 then
            -- x1,y1, x2,y2
            pass:setScissor(
              drawCmd.ClipRect.x, 
              drawCmd.ClipRect.y,
              (drawCmd.ClipRect.z - drawCmd.ClipRect.x) * 2, -- TODO: HIDPI
              (drawCmd.ClipRect.w - drawCmd.ClipRect.y) * 2)
              --agen.log.verbose('application', 'clip <%f:%f> <%f:%f>', drawCmd.ClipRect.x, drawCmd.ClipRect.y, drawCmd.ClipRect.z, drawCmd.ClipRect.w)
          end
          pass:drawIndexedPrimitive(vOffset, drawCmd.ElemCount, iDrawOffset)
        end
        iDrawOffset = iDrawOffset + drawCmd.ElemCount
      end
      vOffset = vOffset + cmdList:getVtxBufferSize()
      iOffset = iOffset + cmdList:getIdxBufferSize()
    end
    pass:submit()
  end
end
-------------------------------------------------------------------
--
-- TODO:
-- 1. Create agScene from file with current video device.
-- 2. Initialize referenced resources.
-- 3. Setup render states with passes.
--
-- Don't just block until everything is done.
--
-------------------------------------------------------------------
function agen.init()
  local device = agen.video.getDevice()
  local assets = loadAssets(device)
  local rTarget = device:getDefaultRenderTarget()

  rTarget:setClearColor(0, bgcolor)
  --
  -- GUI rendering
  --
  do
    agen.log.info('application', "using ImGui version %s", cimgui.igGetVersion())

    local window = agen.window
    local io = cimgui.init(window)
    -- use default font
    cimgui.ImFontAtlas_AddFontDefault(io.Fonts, nil)
    -- add custom font
    --cimgui.ImFontAtlas_AddFontFromFileTTF(io.Fonts, 'assets/fonts/Vera.ttf', 14, nil, nil)
    --cimgui.ImFontAtlas_AddFontFromFileTTF(io.Fonts, 'assets/fonts/NotoSans-Bold.ttf', 22, nil, nil)
    local pix, w, h, bpp = io.Fonts:getTexDataA8()
    local fontTexture = device:createTexture({
      width  = w,
      height = h,
      format = sdl.PIXELFORMAT_A8,
      bytesPerPixel = bpp,
      pitch   = bpp * w,
      levels  = 1,
      samples = 1,
      purpose = 'color',
    })
    fontTexture:updateRect(0, nil, pix, w * bpp * h, w * bpp)

    local rState = rStateFactory.createForImGUI(device, {
      -- use window coords
      size = {
        window:getSize()
      },
      shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
      },
      topology = 'triangles',
      vertexBuffers = {
        [0] = assets.buffers.vertex[1],
      },
      indexBuffer = assets.buffers.index[1],
      uniformBuffer = assets.buffers.uniform[1],
      textures = {
        [0] = fontTexture,
      },
    })

	  local tex = rState:getProgram():getSamplerIndex('Texture')
	  rState:getRenderPass('default'):setTexture(tex, 0, 0)
  end
  --[[
  profiler.hookall('C')
  -- FIXME: allow sdl.SDL_GetTicks as arg
  profiler.setclock(function() return sdl.SDL_GetTicks() end)
  profiler.start()
  ]]
end

