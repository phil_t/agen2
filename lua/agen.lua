------------------------------------------------------------
--
-- Agen application object
--
------------------------------------------------------------
require('utils.strict')
strict_declare_globals('agen', '_agen_init', '_agen_main', '_agen_quit')

local ffi    = require('ffi')
local debug  = require('debug')
local string = require('string')
local sdl    = require('sdl2.lib')
local SDLwindow = require('sdl2.window')

if ffi.os == 'Windows' then
  ffi.cdef([[
int32_t _splitpath_s(const char* path, char* drive, size_t driveNumberOfElements,
                     char* dir,  size_t dirNumberOfElements,
                     char* fname,size_t nameNumberOfElements,
                     char* ext,  size_t extNumberOfElements);
  ]])
else
  ffi.cdef([[
char* dirname(const char *path);
char* basename(const char *path);
int chdir(const char *path);
  ]])
end

agen = {}
agen.log    = require('lua.log')
-- open a log file
agen.log.file("log.txt")

agen.conf   = require('conf')
agen.window = nil
agen.io     = require('agen.io')
agen.event  = require('lua.sdl2.event')
agen.video  = require('agen.video')
agen.audio  = require('agen.audio')
agen.nodraw = function() sdl.SDL_Delay(16) end
agen.draw   = function() agen.video.getDevice():renderFrame() end

local _done = false
------------------------------------------------------------
--
-- Caller implements function agen.render(device).
-- Called every iteration of the main loop.
--
-- nodraw is called when window is not visible / minimized and
-- should not make any render calls. Default implementation 
-- will suspend the current thread for 16 ms
--
-- draw is called to render a frame. If vsync is enabled video 
-- backends will block in draw().
--
-------------------------------------------------------------
local _draw  = agen.nodraw

-- default event handlers (internal)
local function _activate()
  _draw = agen.draw
end

local function _deactivate()
  _draw = agen.nodraw
end

--- Performs a full garbage collection cycle
local function _collect()
  collectgarbage('collect')
end

--- Scales the back buffer
local function _scaleBackBuffer(e)
  -- update device
  agen.video:getDevice():backBufferScale(e:window())
end

--- Resizes the backbuffer
local function _resizeBackBuffer(e)
  agen.video:getDevice():backBufferResize(e:window())
end

--- Calls a function, raising an error with a traceback
-- @param func Function address
-- @param ... arguments
local function _xpcall_debug(func, ...)
  assert(type(func) == 'function', 'try block not callable')
  -- todo: figure out how to change the "level" of debug.traceback
  -- so we don't get "xpcallback" in the error message
  -- todo: multiple return arguments on success
  local r, e = xpcall(func, debug.traceback, ...)
  if r == false then
    error(e)
  end
  return e
end

local function _xpcall_release(func, ...)
  return func(...)
end

local _xpcall = _xpcall_release
--- Initializes all sub-systems
local function _preLoop()
  -- not immediate
  -- always preceding SDL_WINDOWEVENT_RESIZED
  agen.event.setCallback(sdl.WINDOWEVENT_SIZE_CHANGED, _scaleBackBuffer)
  agen.event.setCallback(sdl.WINDOWEVENT_MAXIMIZED, _resizeBackBuffer)
  agen.event.setCallback(sdl.WINDOWEVENT_HIDDEN, _deactivate)
  agen.event.setCallback(sdl.WINDOWEVENT_MINIMIZED, _deactivate)
  agen.event.setCallback(sdl.WINDOWEVENT_RESTORED, _activate)
  agen.event.setCallback(sdl.WINDOWEVENT_SHOWN, _activate)
  -- immediate: caller blocks until callback returns
  agen.event.setCallback(sdl.APP_LOWMEMORY, _collect, true)

  --agen.event.setFilter(true)

  -- initialize video with the default window created by the agen binary
  agen.video.init(agen.window, agen.conf.video)
  -- initialize audio
  agen.audio.init(agen.window, agen.conf.audio)
end

--- Releases all sub-systems
local function _postLoop()
  agen.audio.release()
  agen.video.release()
  agen.event.setFilter(false)
  _collect()
end
--- Get private directory where application is allowed to write
-- @param org Company name (string)
-- @param app App name (sintrg)
-- @return path (lua string)
function agen.getPrivatePath(org, app)
  return sdl.GetPrivatePath(org, arg)
end
---------------------------------------------------------------------------------
--
-- default implementations - can be overwritten by client code
--
---------------------------------------------------------------------------------
function agen.init()
  -- at least one RenderState created
end
--- Default runloop implementation
function agen.run()
  -- really bad and imprecise loop
  local _lastupdate = 0
  local dt = 0
  local now = sdl.SDL_GetTicks()
  -- GC: manual override
  _collect()
  collectgarbage('stop')
  -- enter event loop
  while not _done do
    -- dispatch pending events
    if not agen.event.process() then
      -- quit requested
      break
    end
    -- update
    now = sdl.SDL_GetTicks()
    dt = now - _lastupdate
    _lastupdate = now
    agen.update(dt / 1000)
    -- redraw
    _draw()
    -- and run a GC step
    collectgarbage('step')
  end
  collectgarbage('restart')
end
--- Dummy implementation
function agen.render(videoDev)
end

--- Stops the main loop and quits
function agen.close()
  _done = true
end
---------------------------------------------------------------------------------
--
-- entry points called from C
--
---------------------------------------------------------------------------------
--- setup globals and enter main loop
function _agen_main(windowID)
  agen.log.verbose('application', 'in _agen_main')

  agen.window = SDLwindow.fromID(windowID)
  if agen.window == nil then
    error("failed to get the SDL Window")
  end

  if agen.conf.devmode then
    _xpcall = _xpcall_debug
  end
  -- run file or default to main.lua
  local script
  for _, arg in pairs(agen.arg) do
    -- skip args that start with - or / (options)
    if not string.match(arg, '^[-/]') then
      script = arg
      break
    end
  end

  if script then
    if ffi.os == 'Windows' then
      --[[
      -- not needed for now
      local win32 = require('agen.system.win32')
      local fullpath = ffi.new('char[4096]')
      -- TODO: move to win32
      local len = ffi.C.GetFullPathNameA(script, 4096, fullpath, nil)
      if len ~= 0 then
        local wd = ffi.new('char[256]')
        local name = ffi.new('char[256]')
        ffi.C._splitpath_s(fullpath, nil, 0, wd, 255, name, 255, nil, 0)
        agen.log.debug('application', 'working directory [%s], file [%s]', wd, name)
        win32.SetCurrentDirectory(wd)
        script = ffi.string(name)
      end
      ]]
    else
      local dr = ffi.C.dirname(script)
      agen.log.debug('application', 'working directory [%s]', dr)
      ffi.C.chdir(dr)
      script = ffi.string(ffi.C.basename(script))
    end
    agen.log.debug('application', 'running [%s]', script)
    dofile(script)
  else
    script = 'main'
    require(script)
  end
	-- init devices
  _xpcall(_preLoop)
  -- run script
  _xpcall(function()
    -- app-specific init
    agen.init()
    -- loop
    agen.run()
  end)
  -- app-specific cleanup
  _xpcall(agen.release)
end
--- Release event
-- there'll be no more lua calls after that one
function _agen_quit()
  agen.log.verbose('application', 'in _agen_quit')
  -- release devices
  _xpcall(_postLoop)
end

return agen
