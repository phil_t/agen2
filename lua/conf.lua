return {
  video = {
    driver = 'SDL',
    -- DX debugging requires Windows SDK
    -- GL debugging requires GL_ARB_debug_output
    debug = false,
    -- fulscreen: mode resolution. 0x0 will match the current desktop resolution.
    -- windowed:  window size in logical units.
    width  = 1024,
    height = 768,
    windowed = true,
    -- multisampling
    samples = 0,
    -- DX11: vsync with every <vsync> frame
    -- set to 0 for perfromance benchmarking
    vsync = 1,
    -- min required color buffer bits
    color_buffer = 32,
    -- min required depth buffer bits
    depth_buffer = 24,
    -- TODO: min required stencil buffer bits?
  },
  audio = {
    driver = 'SDL',
    debug = false,
    channels = 'stereo',
    rate = 44100,
    bits = 16
  },
  -- default log level
  logLevel = 'warn',
  -- dump stacktrace on lua error?
  devmode = true,
}
