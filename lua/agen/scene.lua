----------------------------------------------------------------------
--
-- Scene object must implement:
-- createSceneFromPath
-- createScene
-- createRenderer
--
----------------------------------------------------------------------
return function(driver)
  return require('agen.scene.' .. driver)
end
