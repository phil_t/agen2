local ffi = require('ffi')
local ole32 = ffi.load('ole32')

ffi.cdef([[
typedef enum _tagCOINIT {
  MULTITHREADED		  = 0x00000000,
	APARTMENTTHREADED	= 0x00000002,
	DISABLE_OLE1DDE		= 0x00000004,
	SPEED_OVER_MEMORY	= 0x00000008
} COINIT;

typedef enum _tagCLSCTX { 
  INPROC_SERVER           = 0x00000001,
  INPROC_HANDLER          = 0x00000002,
  LOCAL_SERVER            = 0x00000004,
  INPROC_SERVER16         = 0x00000008,
  REMOTE_SERVER           = 0x00000010,
  INPROC_HANDLER16        = 0x00000020,
  RESERVED1               = 0x00000040,
  RESERVED2               = 0x00000080,
  RESERVED3               = 0x00000100,
  RESERVED4               = 0x00000200,
  NO_CODE_DOWNLOAD        = 0x00000400,
  RESERVED5               = 0x00000800,
  NO_CUSTOM_MARSHAL       = 0x00001000,
  ENABLE_CODE_DOWNLOAD    = 0x00002000,
  NO_FAILURE_LOG          = 0x00004000,
  DISABLE_AAA             = 0x00008000,
  ENABLE_AAA              = 0x00010000,
  FROM_DEFAULT_CONTEXT    = 0x00020000,
  ACTIVATE_32_BIT_SERVER  = 0x00040000,
  ACTIVATE_64_BIT_SERVER  = 0x00080000,
  ENABLE_CLOAKING         = 0x00100000,
  APPCONTAINER            = 0x00400000,
  ACTIVATE_AAA_AS_IU      = 0x00800000,
  PS_DLL                  = 0x80000000
} CLSCTX;

HRESULT CoInitializeEx(LPVOID pvReserved, DWORD dwCoInit);
HRESULT CoCreateInstance(GUID* rclsid, 
  IUnknown* pUnkOuter, 
  CLSCTX dwClsContext, 
  GUID* riid, 
  void** ppv);
void CoUninitialize(void);

HRESULT CLSIDFromString(const char* lpsz, GUID* pclsid);
]])

return ole32
