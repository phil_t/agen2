local ffi = require('ffi')
local bit = require('bit')
local string = require('string')
local iconv  = require('sdl2.iconv')

ffi.cdef([[
typedef uint32_t	HRESULT;    // 32-bit unsigned
typedef uint32_t  UINT;       // 32-bit unsigned
typedef int32_t		INT;        // 32-bit signed
typedef uint32_t 	UINT32;     // 32-bit unsigned
typedef int		    BOOL;       // 32-bit
typedef uint32_t 	DWORD;      // 32-bit unsigned
typedef uint16_t	WORD;       // 16-bit unsigned
typedef uint32_t 	ULONG;      // 32-bit unsigned
typedef int32_t   LONG;       // 32-bit signed
typedef void* 		HWND;
typedef void* 		HANDLE;
typedef void* 	  HMONITOR;
typedef void* 		LUID;
typedef __int64		LARGE_INTEGER;
typedef void*     LPVOID;
typedef const void* LPCVOID;
typedef uint64_t  UINT64;     // FFI doesn't know about __uint64
typedef void*		  HMODULE;
typedef wchar_t   WCHAR;
typedef char*	    LPSTR;
typedef const char* LPCSTR;
typedef wchar_t* LPWSTR;
typedef const wchar_t* LPCWSTR;
typedef void*			HDC;
typedef uint8_t   UINT8;
typedef size_t    SIZE_T;
typedef int8_t    BYTE;
typedef float     FLOAT;

static const int LOGPIXELSX = 88;   /* Logical pixels/inch in X                 */
static const int LOGPIXELSY = 90;   /* Logical pixels/inch in Y                */

typedef struct _GUID {
  uint32_t Data1;
  uint16_t Data2;
  uint16_t Data3;
  uint8_t  Data4[8];
} GUID, *REFGUID, *REFIID;

typedef struct _POINT {
	float x, y;
} POINT;

typedef struct _RECT {
  LONG left, top;
  LONG right, bottom;
} RECT;

typedef struct _OSVERSIONINFO {
	DWORD dwOSVersionInfoSize;
	DWORD dwMajorVersion;
	DWORD dwMinorVersion;
	DWORD dwBuildNumber;
	DWORD dwPlatformId;
	uint8_t szCSDVersion[128];
} OSVERSIONINFO;

typedef struct tagMONITORINFOEX {
	DWORD cbSize;
	RECT  rcMonitor;
	RECT  rcWork;
	DWORD dwFlags;
} MONITORINFO;

typedef enum _FORMAT_MESSAGE_FLAGS {
  ALLOCATE_BUFFER = 0x00000100,
  IGNORE_INSERTS  = 0x00000200,
  FROM_STRING     = 0x00000400,
  FROM_HMODULE    = 0x00000800,
  FROM_SYSTEM     = 0x00001000,
  ARGUMENT_ARRAY  = 0x00002000,
} FORMAT_MESSAGE_FLAGS;

DWORD __stdcall FormatMessageA(FORMAT_MESSAGE_FLAGS dwFlags, const void* lpSource, DWORD dwMessageId, DWORD dwLanguageId, char** lpBuffer, DWORD nSize, va_list* args);
BOOL  __stdcall GetVersionExA(OSVERSIONINFO* lpVersionInfo);
DWORD __stdcall GetLastError(void);
void* __stdcall LocalFree(void* hMem);

typedef enum _tagPROCESS_DPI_AWARENESS { 
  Process_DPI_Unaware            = 0x00000000,
  Process_System_DPI_Aware       = 1,
  Process_Per_Monitor_DPI_Aware  = 2
} PROCESS_DPI_AWARENESS;

DWORD __stdcall GetFullPathNameA(LPCSTR lpFileName, DWORD nBufferLength, LPSTR lpBuffer, LPSTR* lpFilePart);

BOOL GetMonitorInfoA(HMONITOR hMonitor, MONITORINFO* lpmi);
HDC GetDC(HWND hWnd);
BOOL IsWindow(HWND hWnd);
int GetDeviceCaps(HDC hdc, int flag);
int ReleaseDC(HWND hWnd, HDC hDC);
// 8.1+
HRESULT __stdcall SetProcessDpiAwarenessInternal(PROCESS_DPI_AWARENESS value);
//HRESULT __stdcall GetProcessDpiAwareness(HANDLE hprocess, PROCESS_DPI_AWARENESS *value);
// Vista+
BOOL    __stdcall SetProcessDPIAware(void);

typedef struct _IUnknownVtbl {
  HRESULT (__stdcall *QueryInterface)(void* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(void* This);
  ULONG (__stdcall *Release)(void* This);
} IUnknownVtbl;

typedef struct _IUnknown {
  const IUnknownVtbl* lpVtbl;
} IUnknown;

typedef struct _SECURITY_ATTRIBUTES {
  DWORD  nLength;
  LPVOID lpSecurityDescriptor;
  BOOL   bInheritHandle;
} SECURITY_ATTRIBUTES;

BOOL __stdcall SetCurrentDirectoryW(wchar_t* lpPathName);
]])

local win32 = {}

win32.ffi = ffi
----------------------------------------------------------------------
--
-- Get OS version. Deprecated since Win8.1+
--
----------------------------------------------------------------------
function win32.getWindowsVersion()
	local ret = 0
	local version = ffi.new('OSVERSIONINFO', {
		dwOSVersionInfoSize = ffi.sizeof('OSVERSIONINFO')
	})
	
	if ffi.C.GetVersionExA(version) then
	  -- get major and minor digits
	  local major = version.dwMajorVersion
	  local minor = version.dwMinorVersion
	  local dmajor = tonumber(major)
	  local dminor = tonumber(minor)
	  ret = dmajor + (dminor / 10)
	  agen.log.verbose("video", "Windows %.1f detected", ret)
	else
    local err = ffi.C.GetLastError()
    agen.log.verbose("video", "GetVersionEx failed: [%s]", win32.GetErrorString(err))
	end
	return ret
end

function win32.GetErrorString(code)
  assert(code, 'invalid argument #1, error code cannot be nil')
  local message = ffi.new('char*[1]')
  local chars = ffi.C.FormatMessageA(bit.bor(ffi.C.ALLOCATE_BUFFER, ffi.C.FROM_SYSTEM, ffi.C.IGNORE_INSERTS), nil, code, 0, message, 128, nil)
  if chars == 0 then
    local err = ffi.C.GetLastError()
    agen.log.debug('application', "Failed to get error message from [0x%x]: %s", code, win32.GetErrorString(err))
    return string.format("0x%x", tonumber(code))
  end
  local ret = ffi.string(message[0])
  ffi.C.LocalFree(message[0])
  return ret
end

function win32.SetProcessDpiAwareness(value)
  if ffi.C.SetProcessDpiAwarenessInternal ~= nil then
    local hres = ffi.C.SetProcessDpiAwarenessInternal(value)
    if hres ~= 0 then
      agen.log.error('application', "Failed to set DPI awareness: %s", win32.GetErrorString(hres))
    end
  elseif ffi.C.SetProcessDPIAware ~= nil and value ~= 0 then
    if ffi.C.SetProcessDPIAware() == 0 then
      agen.log.error('application', "Failed to set DPI awareness: %s", win32.GetErrorString(ffi.C.GetLastError()))
    end
  else
    agen.log.debug('application', "Setting process DPI awareness not supported on this windows version")
  end
end

function win32.GetProcessDpiAwareness()
  if ffi.C.GetProcessDpiAwareness ~= nil then
    local aware = ffi.new('PROCESS_DPI_AWARENESS[1]')
    local hres = ffi.C.GetProcessDpiAwareness(nil, aware)
    if hres ~= 0 then
      agen.log.error('application', "Failed to get DPI awareness: %s", win32.GetErrorString(hres))
    end
    return aware[0] > 0
  end
  return 0
end

function win32.GetScreenLogicalDPI()
  local hdc = ffi.C.GetDC(nil)
  local scaleX = ffi.C.GetDeviceCaps(hdc, ffi.C.LOGPIXELSX) / 96
  local scaleY = ffi.C.GetDeviceCaps(hdc, ffi.C.LOGPIXELSY) / 96
  ffi.C.ReleaseDC(nil, hdc)
  return scaleX, scaleY
end

function win32.GetMonitorInfo(hmon)
	local info = ffi.new('MONITORINFO', {
		cbSize = ffi.sizeof('MONITORINFO')
	})
	local ret = ffi.C.GetMonitorInfoA(hmon, info)
	if ret == 0 then
    local code = ffi.C.GetLastError()
		error(string.format("Failed to get monitor info: %s", win32.GetErrorString(code)))
	end
	return info
end

function win32.finalizerCOM(obj)
  assert(obj ~= nil, "invalid argument #1, managed object cannot be nil")
  local objStr = tostring(obj) or 'Unknown'
  local ref = obj:release()
  agen.log.verbose('application', "[%s] released, remaining refs [%d]", objStr, ref)
end

function win32.IsWindow(hwnd)
  assert(hwnd ~= nil, "invalid argument #1, hwnd cannot be nil")
  assert(ffi.istype('HWND', hwnd), "invalid argument #1, type not HWND")
  return ffi.C.IsWindow(hwnd)
end

function win32.SetCurrentDirectory(path)
  assert(path ~= nil, 'invalid argument #1, path cannot be nil')
  local res = ffi.C.SetCurrentDirectoryW(iconv.UTF8_TO_UTF16(path))
  if res == 0 then
    local err = ffi.C.GetLastError()
    error(string.format('Failed to set current directory: %s', win32.GetErrorString(err)))
  end
end
-------------------------------------------------------------------------------------------
--
-- IUnknown
--
-------------------------------------------------------------------------------------------
local IUnknown = {}
function IUnknown:queryInterface(ctype, guid)
  assert(ctype, "invalid argument #1, C-type cannot be nil")
  assert(guid ~= nil, "invalid argument #2, GUID cannot be nil")
  assert(ffi.istype('GUID', guid), 'Invalid argument #2: not a GUID')
  
  local ret  = ffi.new('void*[1]')
  local hres = self.lpVtbl.QueryInterface(self, guid, ret)
  if hres ~= 0 then
    error(string.format("QueryInterface failed: %s", win32.GetErrorString(hres)))
  end
  local obj = ffi.cast(ctype, ret[0])
  return ffi.gc(obj, win32.finalizerCOM)
end

function IUnknown:ref()
  return self.lpVtbl.AddRef(self)
end

function IUnknown:release()
  return self.lpVtbl.Release(self)
end

win32.IUnknownMT = {__index = IUnknown}
ffi.metatype('IUnknown', win32.IUnknownMT)

return win32
