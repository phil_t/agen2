local math3D = require('agen.util.math')
local loveMath = {}
local math = require('math')

function loveMath.setRandomSeed(low, high)
  if high then
    -- 2 x 32-bit numbers
    error('not implemented')
  else
    -- 1 x 53-bit number
    math.randomseed(low)
  end
end

function loveMath.random(min, max)
  if max then
    -- min-max
    assert(min < max, 'random limits valid')
    return math3D.linear(min, max)
  elseif min then
    assert(min < 1, 'random limits valid')
    -- 1-min
    return math3D.linear(1, min)
  else
    -- 0-1
    return math3D.linear(0, 1)
  end
end

return loveMath
