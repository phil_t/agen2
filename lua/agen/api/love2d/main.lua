local ffi = require('ffi')
local rStateFactory = require('agen.renderState.factory')
strict_declare_globals( 'love' )
-------------------------------------------------------------------
--
-- setup love2d compatible environment and load love2d/main.lua
--
-------------------------------------------------------------------
function agen.init()
  local device = agen.video.getDevice()
  local dir = "../../../../assets/shaders/" .. device:getName()
  --
  -- setup love2d globals
  --
  love = require('agen.api.love2d.love')
  --
  -- setup love2d-compatible render state
  --
  local rState = rStateFactory.create2D(device, {
    label   = 'default',
    center  = 'topleft',
    size    = {agen.window:getSize()},
    -- will force using vertex2D
    depthTest = false,
    shaders = {
      vertex = agen.io.contents(dir .. "/love2d.vert"),
      pixel  = agen.io.contents(dir .. "/love2d.frag"),
    },
    topology = 'strip',
    vertexBuffers = {
      [0] = device:createBuffer({
        purpose = 'vertex',
        usage   = 'dynamic',
        size    = 8 * 1024 * ffi.sizeof('vertex2D'),
        ctype   = 'vertex2D',
      }),
    },
    indexBuffer = device:createBuffer({
      purpose = 'index',
      usage   = 'dynamic',
      size    = 8 * 1024 * ffi.sizeof('uint16_t'),
      ctype   = 'uint16_t',
    }),
    uniformBuffer = device:createBuffer({
      purpose = 'uniform',
      usage   = 'dynamic',
      -- mat4x4 + color4
      size    = ffi.sizeof('xform_t'),
      ctype   = 'xform_t',
    }),
    bgcolor = {0, 0, 0, 1},
  })
  --
  -- enter love2d
  --
  dofile('love2d/main.lua')
  --
  -- call love.load
  --
  love.load()
end

function agen.update(dt)
  love.update(dt)
end

function agen.render(videoDev)
  local rState = videoDev:getRenderState('default')
  rState:setActive()

  love.graphics._enter(rState)

  local pass = rState:getRenderPass('default')
  pass:begin()
  pass:clear()
  love.draw()
  pass:submit()
end
--------------------------------------------------------------------
--
-- cleanup
--
--------------------------------------------------------------------
function agen.release()
  love = nil
end
