local ffi   = require('ffi')
local table = require('table')
local mat4  = require('agen.util.math.mat4')

local graphics = {}

local _graphicsState = {
  vbOffset    = 0,
  activeState = nil,
  xform       = ffi.new('xform_t'),
  xformStack  = {},
}

local function _setColor(colorArray, r, g, b, a)
  assert(r >= 0 and r < 256, 'red color value in range')
  assert(g >= 0 and g < 256, 'green color value in range')
  assert(b >= 0 and b < 256, 'blue color value in range')
  assert(a >= 0 and a < 256, 'alpha value in range')
  colorArray[0] = r / 255
  colorArray[1] = g / 255
  colorArray[2] = b / 255
  colorArray[3] = a / 255
end
----------------------------------------------------------------------------------
--
-- state management
--
----------------------------------------------------------------------------------
function graphics.setColor(red, green, blue, alpha)
  if type(red) == 'table' then
    -- setColor({rgba}) available in 0.7.0+
    assert(#red == 3 or #red == 4, 'setColor(table): 3 or 4 elements provided')
    _setColor(_graphicsState.xform.color, red[1], red[2], red[3], red[4] or 255)
  elseif type(red)  == 'number' and type(green) == 'number' and 
         type(blue) == 'number' and (alpha and type(alpha) == 'number' or true) then
    _setColor(_graphicsState.xform.color, red, green, blue, alpha or 255)
  else
    -- TODO: match l2d behavior
    error('Invalid argument to setColor')
  end
end

function graphics.getColor()
  local c = _graphicsState.xform.color
  return c[0] * 255, c[1] * 255, c[2] * 255, c[3] * 255
end

function graphics.getBackgroundColor()
  local device = agen.video.getDevice()
  local c = device:getDefaultRenderTarget():getClearColor(0)
  -- float[4] to lua numbers
  return c[0] * 255, c[1] * 255, c[2] * 255, c[3] * 255
end

function graphics.setBackgroundColor(r, g, b, a)
  local c = ffi.new('float[4]')
  _setColor(c, r, g, b, a or 255)
  local device = agen.video.getDevice()
  device:getDefaultRenderTarget():setClearColor(0, c)
end
--- push the default xform matrix onto the transformation stack
function graphics.origin()
end
--- The graphics module is inactive if a window is not open 
--- or if the app is in the background on iOS. 
function graphics.isActive()
  -- TODO: monitor window events
  return true
end
--- Clears the screen to transparent black (0, 0, 0, 0) in LÖVE 0.10.0+
function graphics.clear(...)
  local arg = {...}
  local pass = graphics.getActiveRenderState():getRenderPass('default')
  if #arg == 0 then
    -- default is transparent black (0, 0, 0, 0)
    pass:clear()
  elseif #arg == 1 then
    -- {r, g, b, a}
    pass:clear()
  elseif #arg == 3 then
    -- r, g, b
    pass:clear()
  elseif #arg == 4 then
    -- r, g, b, a
    pass:clear()
  else
    error('graphics.clear(): incorrect number of parameters')
  end
end
--------------------------------------------------------------------------------
--
-- not love2d API
--
--------------------------------------------------------------------------------
function graphics._enter(rState)
  _graphicsState.activeState = rState
  -- reset VB offset
  _graphicsState.vbOffset = 0
  -- reset xform
  ffi.copy(_graphicsState.xform.modelViewProj, 
           rState:getModelViewProj():ptr(), 
           ffi.sizeof('float[16]'))
end

function graphics.getActiveRenderState()
  return _graphicsState.activeState
end
---------------------------------------------------------------------------------
--
-- vector graphics
--
---------------------------------------------------------------------------------
function graphics.rectangle(mode, x, y, w, h)
  assert(mode == 'fill', 'supported rectangle rendering mode')
  local rect = ffi.new('vertex2D[4]', {
    {x,     y,     0, 0},
    {x + w, y,     0, 0},
    {x,     y + h, 0, 0},
    {x + w, y + h, 0, 0},
  })
  local size = ffi.sizeof('vertex2D') * 4
  local pass = graphics.getActiveRenderState():getRenderPass('default')
  -- update vertex buffer
  local vBuffer = pass:getVertexBuffer(0)
  vBuffer:update(_graphicsState.vbOffset, rect, size)
  -- update uniform buffer
  local cBuffer = pass:getUniformBuffer('xform')
  cBuffer:update(0, _graphicsState.xform, ffi.sizeof('xform_t'))
  -- render
  pass:drawPrimitive(_graphicsState.vbOffset, 4)
  -- update offset for subsequent draw calls
  _graphicsState.vbOffset = _graphicsState.vbOffset + size
end
---------------------------------------------------------------------------------
--
-- device methods
--
---------------------------------------------------------------------------------
function graphics.present()
  -- no-op, called from agen.videoDevice:renderFrame
end

return graphics
