local love = {}
local os = require('os')

-- Drawing of shapes and images, management of screen geometry.    
love.graphics = require('agen.api.love2d.graphics')
-- Provides system-independent mathematical functions. Added since 0.9.0 
love.math     = require('agen.api.love2d.math')
-- Manages events, like keypresses.  Added since 0.6.0 
love.event    = require('agen.api.love2d.event')
--[[
love.timer      Provides high-resolution timing functionality.    
love.joystick   Provides an interface to connected joysticks.   
love.keyboard   Provides an interface to the user's keyboard.   
love.mouse      Provides an interface to the user's mouse.    
love.filesystem Provides an interface to the user's filesystem.   
love.image      Provides an interface to decode encoded image data.   
love.sound      This module is responsible for decoding sound files.    
love.audio      Provides an interface to output sound to the user's speakers.   

love.physics    Can simulate 2D rigid body physics in a realistic manner. Added since 0.4.0 
love.font       Allows you to work with fonts.  Added since 0.7.0 
love.thread     Allows you to work with threads.  Added since 0.7.0 
love.system     Provides access to information about the user's system. Added since 0.9.0 
love.window     Provides an interface for the program's window. Added since 0.9.0 
love.touch      Provides an interface to touch-screen presses.  Added since 0.10.0  
love.video      This module is responsible for decoding and streaming video files.  Added since 0.10.0  
]]
------------------------------------------------------------------------------------
--
-- default placeholders for these callbacks, which 
-- you can override inside your own code by creating your own 
-- function with the same name as the callback:
--
------------------------------------------------------------------------------------
function love.load()
end

function love.update(dt) 
end

function love.draw()
end
--[[
function love.run()
	if love.math then
		love.math.setRandomSeed(os.time())
	end
 
	if love.load then love.load(arg) end
 
	-- We don't want the first frame's dt to include time taken by love.load.
	if love.timer then love.timer.step() end
 
	local dt = 0
 
	-- Main loop time.
	while true do
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end
 
		-- Update dt, as we'll be passing it to update
		if love.timer then
			love.timer.step()
			dt = love.timer.getDelta()
		end
 
		-- Call update and draw
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
 
		if love.graphics and love.graphics.isActive() then
			love.graphics.clear(love.graphics.getBackgroundColor())
			love.graphics.origin()
			if love.draw then love.draw() end
			love.graphics.present()
		end
 
		if love.timer then love.timer.sleep(0.001) end
	end
end
]]
return love
