local canvas = love.graphics.newCanvas()
local clear
 
function love.update()
  -- Use an anonymous function to draw lines on our canvas.
  canvas:renderTo(function()
    if clear then
      love.graphics.clear() -- Clear the canvas before drawing lines.
    end
                                           
    -- Draw lines from the screen's origin to a random x and y coordinate.
    local rx, ry = love.math.random(0, love.graphics.getWidth()), love.math.random(0, love.graphics.getHeight())
    love.graphics.setColor(love.math.random(255), 0, 0)
    love.graphics.line(0, 0, rx, ry)
    love.graphics.setColor(255, 255, 255)
  end)
end
                                                                                     
function love.draw()
  love.graphics.draw(canvas)
end
                                                                                         
function love.keypressed(key)
  if key == "c" then
    clear = not clear
  end
end
