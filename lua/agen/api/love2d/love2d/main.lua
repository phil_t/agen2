-------------------------------------------------------------
--
-- automatically executed when running
-- agen2 lua/agen/api/love2d/main.lua
--
-------------------------------------------------------------
local x, y, w, h

function love.load(arg)
  x, y, w, h = 20, 20, 60, 20
  love.graphics.setBackgroundColor(195, 195, 195)
end
     
-- Increase the size of the rectangle every frame.
function love.update(dt)
  w = w + 1
  h = h + 1
end
             
-- Draw a colored rectangle.
function love.draw()
  love.graphics.setColor(0, 100, 100)
  love.graphics.rectangle("fill", x, y, w, h)
end
