----------------------------------------------------------------------------------------
--
-- Video instance interface used as a video device factory
--
-- Usage:
--	* Create device object and use it as vertex buffer / texture / render state factory.
--  * Implementation of agen.draw
--
----------------------------------------------------------------------------------------
local IVideoDevice = require('agen.interface.video.device')
local _deviceObj = nil
local video = {}

function video.init(window, config)
	_deviceObj = IVideoDevice:new(window, config)
  -- Create default render state and default render target
  _deviceObj:init(config)
end

--- Get the current thread's device object
function video.getDevice()
	return _deviceObj
end

function video.release()
  -- was is initialized successfully?
  if _deviceObj ~= nil then
    -- enforce specific finalization order if needed
    _deviceObj:finalize()
    _deviceObj = nil
  end
end

return video
