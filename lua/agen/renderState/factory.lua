---------------------------------------------------------------------------
--
-- Factory for pre-defined render state objects
--
---------------------------------------------------------------------------
local ffi  = require('ffi')
local math = require('math')
local math3D = require('agen.util.math')
--
-- pre-defined vertex formats
--
ffi.cdef([[
/** vertex without depth component **/
typedef struct _vertex2D {
  float x, y;
  float u, v;
} vertex2D;

/** vertex with depth component **/
typedef struct _vertex3D {
  float x, y, z;
  float u, v;
} vertex3D;

/** 3-component vector */
typedef struct _f3Vertex3D {
  float x, y, z;
} vertex3f;

typedef struct _xform {
  float modelViewProj[16];
  float color[4];
} xform_t;
]])

local rStateFactory = {}
--- 2D render state with customizable ortho projection
-- @param agen video device
-- @param opts table of options
-- @return agen2 render state object
function rStateFactory.create2D(device, opts)
  assert(opts, "invalid argument #1, lua table expected, got nil")
  assert(opts.center, "required option <center> not set")
  assert(opts.size, "required option <size> not set")
  assert(opts.shaders, "required option <shaders> not set")
  assert(opts.vertexBuffers, "required option <vertexBuffers> not set")
  assert(opts.uniformBuffer, "required option <uniformBuffer> not set")
  assert(opts.topology, "required option <topology> not set")

  local vctype = opts.depthTest and 'vertex3D' or 'vertex2D'
  local vtype  = opts.depthTest and 'float3'   or 'float2'
  --
  -- 1. Create new render state
  --
  local rState = device:createRenderState({
    label = opts.label or 'default',
    shaders = opts.shaders,
    topology = opts.topology,
    -- interleaved vertex / texture data
	  vertexBuffersAttr = opts.vertexBuffersAttr or {
      {index = 0, name = 'position', offset = ffi.offsetof(vctype, 'x'), type = vtype,    usage = 'position', stride = ffi.sizeof(vctype)},
      {index = 0, name = 'texcoord', offset = ffi.offsetof(vctype, 'u'), type = 'float2', usage = 'texcoord', stride = ffi.sizeof(vctype)},
	  },
  })
  --
  -- 2. Setup ortho projection
  --
  local w, h = opts.size[1], opts.size[2]
  agen.log.debug('application', 'setting up 2D ortho projection with size [%dx%d]', w, h)

  rState.xform.model = math3D.mat4.identity()
  rState.xform.view = math3D.mat4.lookAt(
	  math3D.vec3.create(0, 0, 1),
	  math3D.vec3.create(0, 0, 0),
	  math3D.vec3.create(0, 1, 0))
  if type(opts.center) == 'table' then
    -- custom projection
    agen.log.verbose('application', "setting up custom ortho projection...")
    rState.xform.projection = math3D.mat4.createFromData(ffi.new('float[16]', opts.center))
  elseif opts.center == 'middle' then
    agen.log.verbose('application', "setting up projection with 0,0 at the middle...")
    -- left, right, bottom, top, near, far
    rState.xform.projection = math3D.mat4.ortho(-w / 2, w / 2, -h / 2, h / 2, 0, 1)
  elseif opts.center == 'topleft' then
    agen.log.verbose('application', "setting up projection with 0,0 at top left...")
    -- left, right, bottom, top, near, far
    rState.xform.projection = math3D.mat4.ortho(0, w, h, 0, 0, 1)
  else
    error("Required option opts.center not set")
  end
  local cbuffer = opts.uniformBuffer
  local modelViewProj = cbuffer:map('write_discard')
  ffi.copy(modelViewProj, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
  cbuffer:unmap()
  --
  -- defaults are:
  --  no depth checking - need to render back to front
  --  alpha blending
  --  no culling - vertex order doesn't matter
  --
  rState:setDepthFunc(opts.depthTest and 'less' or 'off')
  rState:setBlendFunc('alpha')
  rState:setCullFunc('off')
  --
  -- 3. Setup default render pass
  --
  local pass = rState:getRenderPass('default'):init({
    vertexBuffers = opts.vertexBuffers,
    indexBuffer = opts.indexBuffer,
    uniforms = {
      vertex = {
        [0] = {name = 'xform', data = opts.uniformBuffer},
      },
    },
    samplers = opts.samplers or {
      [0] = device:createSampler(),
    },
    textures = opts.textures,
  })
  --
  -- 4. Set clear color (optional)
  --
  if opts.bgcolor then
    local c = ffi.new('float[4]', opts.bgcolor)
    device:getDefaultRenderTarget():setClearColor(0, c)
  end
  return rState
end
--- ImGUI compatible 2D render state with ortho projection
-- @param agen video device
-- @param opts table of options
-- @return agen2 render state object
function rStateFactory.createForImGUI(device, opts)
  assert(opts, "invalid argument #1, lua table expected, got nil")
  opts.label = 'imgui'
  opts.depthTest = false
  opts.vertexBuffersAttr = {
    {index = 0, name = 'position', offset = ffi.offsetof('ImDrawVert', 'pos'), type = 'float2',   usage = 'position', stride = ffi.sizeof('ImDrawVert')},
    {index = 0, name = 'texcoord', offset = ffi.offsetof('ImDrawVert', 'uv'),  type = 'float2',   usage = 'texcoord', stride = ffi.sizeof('ImDrawVert')},
    {index = 0, name = 'color',    offset = ffi.offsetof('ImDrawVert', 'col'), type = 'ubyte4',   usage = 'color',    stride = ffi.sizeof('ImDrawVert'), norm = true},
  }
  opts.center = 'topleft'
  return rStateFactory.create2D(device, opts)
end
--- create a render state for 3D rendering
-- @param agen video device
-- @param opts table of inputs
-- @return renderState object
function rStateFactory.create3D(device, opts)
  assert(opts, "invalid argument #1, lua table expected, got nil")
  assert(opts.shaders, 'missing required option <shaders>')
  assert(opts.uniformBuffers, 'missing required option <uniformBuffers>')
  assert(opts.indexBuffer, 'missing required option <indexBuffer>')
  assert(opts.topology, 'missing required option <topology>')

  local rTarget = device:getDefaultRenderTarget()
 	local rState = device:createRenderState({
    label = opts.label or 'default',
    shaders = opts.shaders,
    topology = opts.topology,
    vertexBuffersAttr = opts.vertexBuffersAttr or {
      {index = 0, name = 'position', offset = 0, type = 'float3', usage = 'position', stride = ffi.sizeof('vertex3f')},
      {index = 1, name = 'normal',   offset = 0, type = 'float3', usage = 'normal',   stride = ffi.sizeof('vertex3f'), norm = true},
      {index = 2, name = 'texcoord', offset = 0, type = 'float2', usage = 'texcoord', stride = ffi.sizeof('vertex3f')},
    },
  })

  local zNear  = 0.1
  local zFar   = 100
  local yViewAngle = 45

  local w, h
  if opts.size then
    w, h = opts.size[1], opts.size[2]
  else
    w, h = rTarget:getViewVolume(0)
  end
  agen.log.debug('application', 'setting up 3D perspective projection with size [%dx%d]', w, h)
  --
  -- Setup 3D projection
  --
  rState.xform.projection = math3D.mat4.perspective(
    math.rad(yViewAngle), 
    w, h, 
    zNear, zFar)
  -- view (camera)
  rState.xform.view = math3D.mat4.lookAt(
    -- camera location
    math3D.vec3.create(0, 1, -10),
    -- looking at point
    math3D.vec3.create(0, 1, 0),
    -- normalized camera "up" vector
    math3D.vec3.create(0, 1, 0))
  -- model
  rState.xform.model = math3D.mat4.identity()
  --
  -- Setup default renderPass
  --
  rState:getRenderPass('default'):init({
    vertexBuffers = opts.vertexBuffers,
    uniforms = opts.uniformBuffers,
    indexBuffer = opts.indexBuffer,
    samplers = opts.samplers or {
      [0] = device:createSampler(),
    },
    textures = opts.textures,
  })
  --
  -- Set clear color (optional)
  --
  if opts.bgcolor then
    local c = ffi.new('float[4]', opts.bgcolor)
    rTarget:setClearColor(0, c)
  end
  --
  -- Set depth/cull/blend states
  --
  rState:setDepthFunc('less')
  rState:setCullFunc('back')
  rState:setBlendFunc('off')

  return rState
end

return rStateFactory
