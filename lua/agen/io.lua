local io  = require('io')
local ffi = require('ffi')
local rwops = require('sdl2.rwops')
-- local dds   = require('agen.util.dds')
--- Reads the contents of a file,
--- With binary files, use mode "rb"
-- @param fn filename
function io.contents(fn)
  agen.log.debug('application', 'slurping file [%s]...', fn)
	local f = io.open(fn, "r")
  if f then
    local a = f:read("*all")
    f:close()
    return a
  end
end

function io.createStream(path)
  assert(path, 'invalid argument #1, file path expected, got nil')
  local ops = rwops.createFromPath(path, 'rb')
  local fourcc = ffi.new('uint8_t[4]')
  if ops:read(fourcc, 4, 1) ~= 1 then
    error("Failed to read from file")
  end
  agen.log.debug('application', "4CC [%c][%c][%c][%d]", fourcc[0], fourcc[1], fourcc[2], fourcc[3])
  -- if dds.isDDS(fourcc) then
  -- end
  return ops
end

return io
