local string = require('string')
local math   = require('math')

local lib = require('agen.scene.assimp.lib')
local fileIO = require('agen.scene.assimp.fileio')
local mesh = require('agen.scene.assimp.mesh')
local ffi = lib.ffi

ffi.cdef([[
typedef enum _aiPostProcessSteps {
	CalcTangentSpace			= 0x00000001,
	// requires index buffer
	JoinIdenticalVertices = 0x00000002,
	// dx9 friendly
	MakeLeftHanded				= 0x00000004,
	// Line and point primitives are *not* modified!
	Triangulate						= 0x00000008,
	RemoveComponent				= 0x00000010,
	GenNormals						= 0x00000020,
	GenSmoothNormals			= 0x00000040,
	// useful for real-time rendering
	SplitLargeMeshes			= 0x00000080,
	// intended for applications without a scenegraph
	PreTransformVertices  = 0x00000100,
	// If you intend to perform the skinning in hardware
	LimitBoneWeights      = 0x00000200,
	ValidateDataStructure = 0x00000400,
	// tries to improve the average post-transform vertex cache miss ratio for all meshes
	ImproveCacheLocality	= 0x00000800,
	RemoveRedundantMaterials = 0x00001000,
	FixInfacingNormals		= 0x00002000,
	// splits meshes with more than one primitive type in homogeneous sub-meshes
	SortByPType						= 0x00008000,
	FindDegenerates				= 0x00010000,
	// intended to get rid of some common exporter errors
	FindInvalidData				= 0x00020000,
	// converts non-UV mappings (such as spherical or cylindrical mapping) to 
	// proper texture coordinate channels
	GenUVCoords						= 0x00040000,
	TransformUVCoords			= 0x00080000,
	FindInstances					= 0x00100000,
	// reduce the number of meshes.
	OptimizeMeshes				= 0x00200000,
	OptimizeGraph					= 0x00400000,
	FlipUVs								= 0x00800000,
	FlipWindingOrder			= 0x01000000,
	SplitByBoneCount			= 0x02000000,
	Debone								= 0x04000000,
} aiPostProcessSteps;

typedef enum _aiSceneFlags {
	INCOMPLETE				 = 0x00000001,
	VALIDATED					 = 0x00000002,
	VALIDATION_WARNING = 0x00000004,
	NON_VERBOSE_FORMAT = 0x00000008,
	TERRAIN						 = 0x00000010,
} aiSceneFlags;

typedef struct _aiMemoryInfo {
  /** Storage allocated for texture data */
	unsigned int textures;
	/** Storage allocated for material data  */
	unsigned int materials;
	/** Storage allocated for mesh data */
	unsigned int meshes;
	/** Storage allocated for node data */
	unsigned int nodes;
	/** Storage allocated for animation data */
	unsigned int animations;
	/** Storage allocated for camera data */
	unsigned int cameras;
	/** Storage allocated for light data */
	unsigned int lights;
	/** Total storage allocated for the full import. */
	unsigned int total;
} aiMemoryInfo;

typedef struct _aiAnimation aiAnimation;
typedef struct _aiSkeleton aiSkeleton;

typedef struct _aiScene {
	unsigned int mFlags;
	aiNode*      mRootNode;
	unsigned int mNumMeshes;
/** The array of meshes. 
	* Use the indices given in the aiNode structure to access 
	* this array. The array is mNumMeshes in size. If the
	* AI_SCENE_FLAGS_INCOMPLETE flag is not set there will always 
	* be at least ONE material.
	*/
	aiMesh**		 mMeshes;
	unsigned int mNumMaterials;
/** The array of materials. 
  * Use the index given in each aiMesh structure to access this
  * array. The array is mNumMaterials in size. If the
  * AI_SCENE_FLAGS_INCOMPLETE flag is not set there will always 
  * be at least ONE material.
  */
	aiMaterial** mMaterials;
	unsigned int mNumAnimations;
	aiAnimation** mAnimations;
	unsigned int mNumTextures;
	aiTexture**  mTextures;
	unsigned int mNumLights;
	aiLight**		 mLights;
	unsigned int mNumCameras;
	aiCamera**	 mCameras;
	aiMetadata* mMetaData;
	aiString mName;
	unsigned int mNumSkeletons;
	aiSkeleton **mSkeletons;
	char*			   mPrivate;
} aiScene;

typedef struct _aiPropertyStore aiPropertyStore;
aiPropertyStore* aiCreatePropertyStore(void);
void aiReleasePropertyStore(aiPropertyStore* p);
void aiSetImportPropertyInteger(aiPropertyStore* store, const char* szName, int value);
void aiSetImportPropertyFloat(aiPropertyStore* store, const char* szName, float value);
void aiSetImportPropertyString(aiPropertyStore* store, const char* szName, const aiString* st);
void aiSetImportPropertyMatrix(aiPropertyStore* store, const char* szName, const aiMatrix4x4* mat);

const aiScene* aiImportFileExWithProperties(const char* pFile, unsigned int pFlags, aiFileIO* pFS, const aiPropertyStore* pProps);
const aiScene* aiImportFileFromMemoryWithProperties(const char* pBuffer, unsigned int pLength,
    unsigned int pFlags, const char* pHint, const aiPropertyStore* pProps);
const aiScene* aiApplyPostProcessing(const aiScene* pScene, unsigned int pFlags);
void aiReleaseImport(const aiScene* pScene);
void aiGetMemoryRequirements(const aiScene* pIn, aiMemoryInfo* in);
// supported file formats by extension
void aiGetExtensionList(aiString* szOut);
]])
---------------------------------------------------------------
--
-- aiPropertyStore
--
---------------------------------------------------------------
local aiPropertyStore = {}
local aiPropertyStoreMT = {
  __index = aiPropertyStore,
  __newindex = function(self, name, value)
    assert(value ~= nil, "property value is nil")
    local t = type(value)
    if t == 'string' then
      local str = ffi.new('aiString')
      str.length = string.len(value)
      ffi.copy(str.data, value, string.len(value))
      lib.aiSetImportPropertyString(self, name, str)
    elseif t == 'boolean' then
      if value then
        lib.aiSetImportPropertyInteger(self, name, 1)
      else
        lib.aiSetImportPropertyInteger(self, name, 0)
      end
    elseif t == 'number' then
      if math.floor(value) == value then
        lib.aiSetImportPropertyInteger(self, name, value)
      else
        lib.aiSetImportPropertyFloat(self, name, value)
      end
    elseif t == 'cdata' then
      if ffi.istype('aiString', value) then
        lib.aiSetImportPropertyString(self, name, value)
      elseif ffi.istype('aiMatrix4x4', value) then
        lib.aiSetImportPropertyMatrix(self, name, value)
      else
        error("Cannot set " .. tostring(value))
      end
    else
      error("Cannot set " .. tostring(value))
    end
  end,
}

function aiPropertyStore.new(tab)
  local self = ffi.gc(lib.aiCreatePropertyStore(), lib.aiReleasePropertyStore)
  for name, value in pairs(tab) do
    self[name] = value
  end
  return self
end
---------------------------------------------------------------
--
-- aiScene
--
---------------------------------------------------------------
local aiScene = {}
local aiSceneMT = {__index = aiScene}

function aiScene.newFromFile(file, flags)
	return aiScene.newFromFileExWithProps(file, flags, nil, {})
end

function aiScene.newFromFileExWithProps(file, flags, iofuncs, props)
  agen.log.verbose('application', "asset [%s]", file)
  local sceneProps = aiPropertyStore.new(props)
  local path, filename = string.match(file, '(.+)/(.+)')
  agen.log.debug('application', "path to asset [%s]", path)
	local self = lib.aiImportFileExWithProperties(file, flags, iofuncs, sceneProps)
	if self == nil then
		error(string.format("Failed to load [%s]: %s", file, lib.GetErrorString()))
	end
	return ffi.gc(self, lib.aiReleaseImport)
end

local vp_nff = [[
nff
version 2.0

// The following two lines are optional.
viewpos 0.0 0.0 0.0   // Viewpoint is at the origin
viewdir 0.0 0.0 1.0   // and looking straight forward.

// polygon
Rect1
4 // num vertices
-1 -1 0
1 -1 0
1 1 0
-1 1 0
1 // num polygons
4 0 1 2 3 0xfff
]]

function aiScene.new(flags, props)
  local sceneProps = aiPropertyStore.new(props)
  local self = lib.aiImportFileFromMemoryWithProperties(
    vp_nff, string.len(vp_nff), flags, "nff", sceneProps)
	if self == nil then
		error(string.format("Failed to load scene: %s", lib.GetErrorString()))
	end
	return ffi.gc(self, lib.aiReleaseImport)
end

function aiScene:getAABB()
	local xform = ffi.new('aiMatrix4x4')
	xform:IdentityMatrix4()

	local min = ffi.new('aiVector3D', {
		x = 1e10,
		y = 1e10,
		z = 1e10
	})

	local max = ffi.new('aiVector3D', {
		x = -1e10,
		y = -1e10,
		z = -1e10
	})
	--
  -- recursively scan children
  --
	self.mRootNode:getAABB(min, max, xform)
  return min, max
end
--- Meshes iterator
function aiScene:iterMeshes()
  local i = -1
  local num = self.mNumMeshes
  return function()
    i = i + 1
    if i < num then return i, self.mMeshes[i] end
  end
end
--- Materials iterator
function aiScene:iterMaterials()
  local i = -1
  local num = self.mNumMaterials
  return function()
    i = i + 1
    if i < num then return i, self.mMaterials[i] end
  end
end
--- Textures iterator. These are embedded into the scene. Usually
-- textures are part of the material
-- @return textures iterator
function aiScene:iterTextures()
  local i = -1
  local num = self.mNumTextures
  return function()
    i = i + 1
    if i < num then return i, self.mTextures[i] end
  end
end
--- Get vertex and index buffer size of all scene meshes in vertices
function aiScene:getBuffersSize()
  local vLen = 0
  local iLen = 0
  for _, mesh in self:iterMeshes() do
    vLen = vLen + mesh.mNumVertices
    iLen = iLen + mesh:getNumIndices()
  end
  return vLen, iLen
end

function aiScene:getRootXForm()
  return self.mRootNode:getXForm()
end

aiScene.PropertyStore = ffi.metatype('aiPropertyStore', aiPropertyStoreMT)

return ffi.metatype('aiScene', aiSceneMT) 
