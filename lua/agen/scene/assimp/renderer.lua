---------------------------------------------------------------------
--
-- Scene renderer
--
---------------------------------------------------------------------
local ffi    = require('ffi')
local math   = require('math')

local assimp = require('agen.scene.assimp.lib')
local math3D = require('agen.util.math')

local dds    = require('agen.util.dds')
local sdl    = require('sdl2')
local image  = require('sdl2.image')
local rwops  = require('sdl2.rwops')

local SceneRenderer = {}
local SceneRendererMT = {__index = SceneRenderer}

ffi.cdef([[
typedef struct _Material3D {
  aiColor4D rgbaDiffuse;
  aiColor4D rgbaSpecular;
  aiColor4D rgbaEmissive;
  aiColor4D rgbaAmbient;
} Material3D;
]])

function SceneRenderer.create(scene, baseDir)
  local self = {
    video = agen.video.getDevice(),
    audio = agen.audio.getDevice(),
    scene = scene,
    buffers = {
      vertex = nil,
      normal = nil,
      index  = nil,
      texcoord = nil,
      uniform = {
        vertex = nil,
        frag   = nil,
      },
    },
    ranges = {
      vertex = {},
      index  = {},
    },
    textures = {},
    materials = nil,
    baseDir = baseDir,
  }
  setmetatable(self, SceneRendererMT)
  self:init()
  return self
end
--- Explicitly preload asynchronously an array of objects:
-- scene, node, mesh or material
function SceneRenderer:preloadObjects(objs, onComplete)

end

function SceneRenderer:init(opts)
  self:initBuffers(opts)
  self:initMaterials(opts)
end

function SceneRenderer:initBuffers(opts)
  local aiScene = self.scene
  -- setup buffers
  local vSize, iSize = aiScene:getBuffersSize()
  agen.log.debug('application', "Scene: [%u] verts, [%u] indices", vSize, iSize)
  -- vertex
  self.buffers.vertex = self.video:createBuffer({
    name    = 'scene_vertices',
    purpose = 'vertex',
    usage   = 'static',
    size    = vSize * ffi.sizeof('aiVector3D'),
    ctype   = 'aiVector3D',
  })
  -- index
  self.buffers.index = self.video:createBuffer({
    name    = 'scene_indices',
    purpose = 'index',
    usage   = 'static',
    size    = iSize * ffi.sizeof('uint32_t'),
    ctype   = 'uint32_t',
  })
  -- texcoords
  self.buffers.texcoord = self.video:createBuffer({
    name    = 'scene_texcoords',
    purpose = 'vertex',
    usage   = 'static',
    size    = vSize * ffi.sizeof('aiVector3D'),
    ctype   = 'aiVector3D',
  })
  -- normals
  self.buffers.normal = self.video:createBuffer({
    name    = 'scene_normals',
    purpose = 'vertex',
    usage   = 'static',
    size    = vSize * ffi.sizeof('aiVector3D'),
    ctype   = 'aiVector3D',
  })
  -- uniform: materials and transformations
  self.buffers.uniform.vertex = self.video:createBuffer({
    name    = 'xform',
    purpose = 'uniform',
    usage   = 'dynamic',
    size    = 16 * ffi.sizeof('float'),
    ctype   = 'float',
  })

  self.buffers.uniform.frag = self.video:createBuffer({
    name    = 'material',
    purpose = 'uniform',
    usage   = 'dynamic',
    size    = ffi.sizeof('Material3D'),
    ctype   = 'float',
  })

  agen.log.debug('application', "Loading meshes...")
  local vOffset, vNum  = 0, 0
  local iOffset, iNum  = 0, 0
  local vBytes, iBytes = 0, 0

  for i, mesh in aiScene:iterMeshes() do
    -- assert(mesh.mPrimitiveTypes == assimp.PTYPE_TRIANGLE, "Only triangle primitive type supported, got " .. tostring(mesh.mPrimitiveTypes))
    -- update buffers
    -- vertex, normals, texture coords
    vBytes = mesh:getBytesVertices()
    self.buffers.vertex:update(vOffset, mesh.mVertices, vBytes)
    if mesh.mNormals ~= nil then
      self.buffers.normal:update(vOffset, mesh.mNormals, vBytes)
    end
    if mesh.mTextureCoords[0] ~= nil then
      self.buffers.texcoord:update(vOffset, mesh.mTextureCoords[0], vBytes)
    end
    self.ranges.vertex[i] = {
      offset = vNum,
      length = mesh.mNumVertices,
    }
    vOffset = vOffset + vBytes
    vNum    = vNum    + mesh.mNumVertices
    -- and index
    local numIndices = mesh:getNumIndices()
    local ptr = mesh:getIndexBuffer('uint32_t')

    iBytes = numIndices * ffi.sizeof('uint32_t')
    self.buffers.index:update(iOffset, ptr, iBytes)
    self.ranges.index[i] = {
      offset = iNum,
      length = numIndices,
    }
    iOffset = iOffset + iBytes
    iNum    = iNum    + numIndices
  end
end

function SceneRenderer:initMaterials(opts)
  local aiScene = self.scene
  local numMaterials = aiScene.mNumMaterials

  if numMaterials == 0 then
    return
  end

  local white = ffi.new('aiColor4D', {1, 1, 1, 1})
  local black = ffi.new('aiColor4D', {0, 0, 0, 1})
  self.materials = ffi.new('Material3D[?]', numMaterials)
  agen.log.debug('application', "Creating materials...")
  for i, mat in aiScene:iterMaterials() do
--[[
    agen.log.debug('application', "found material [%s]", mat:GetName())
    for pname, prop in pairs(mat) do
      agen.log.debug('application', "\tmaterial property [%s] = [%s]", pname, prop:value(mat))
    end
]]
    if not mat:GetPropColor('$clr.diffuse', self.materials[i].rgbaDiffuse) then
      ffi.copy(self.materials[i].rgbaDiffuse, white, ffi.sizeof('aiColor4D'))
    end
    -- only used with lighting on
    if not mat:GetPropColor('$clr.emissive', self.materials[i].rgbaEmissive) then
      ffi.copy(self.materials[i].rgbaEmissive, black, ffi.sizeof('aiColor4D'))
    end

    if not mat:GetPropColor('$clr.specular', self.materials[i].rgbaSpecular) then
      ffi.copy(self.materials[i].rgbaSpecular, black, ffi.sizeof('aiColor4D'))
    end

    if not mat:GetPropColor('$clr.ambient', self.materials[i].rgbaAmbient) then
      ffi.copy(self.materials[i].rgbaAmbient,  black, ffi.sizeof('aiColor4D'))
    end

    local numTexDiffuse = mat:GetMaterialTextureCount(assimp.TextureType_DIFFUSE)
    agen.log.debug('application', 'diffuse textures: [%d]', numTexDiffuse)

    if numTexDiffuse > 0 then
      for j = 0, numTexDiffuse - 1, 1 do
        local tex = mat:GetPropTexture(j, assimp.TextureType_DIFFUSE)
        if tex and tex ~= nil then
          -- FIXME: one texture per material
          self.textures[i] = self:loadImage(self.baseDir .. '/' .. tex)
        else
          agen.log.debug('application', "No texture at index [%d]", j)
        end
      end
    end
  end
end
--- Render scene
-- @param renderState to use
function SceneRenderer:present(rState)
  -- will re-calc ModelViewProj
	local mvp = rState:getModelViewProj()
  rState:setActive()

  local pass = rState:getRenderPass('default')
  pass:begin()
  pass:clear()
  pass:clearDepth()
  --
  -- update xform buffer
  --
  local cbuffer = pass:getUniformBuffer('xform')
  local modelViewProj = cbuffer:map('write_discard')
  ffi.copy(modelViewProj, mvp:ptr(), 16 * ffi.sizeof('float'))
  cbuffer:unmap()

  -- FIXME: not here
  local tex = nil
  if self.textures ~= nil and #self.textures > 0 then
    tex = pass.owner:getProgram():getSamplerIndex('tex')
  end
  
  local matbuff = pass:getUniformBuffer('material')
  --
  -- render all meshes
  --
  for i = 0, self.scene.mNumMeshes - 1, 1 do
    local matIndex = self.scene.mMeshes[i].mMaterialIndex
    --
    -- update material buffer
    --
    local ptr = matbuff:map('write_discard')
    ffi.copy(ptr, self.materials[matIndex], ffi.sizeof('Material3D'))
    matbuff:unmap()

    if self.textures[matIndex] then
      pass:setTexture(tex, 0, matIndex)
    end

    pass:drawIndexedPrimitive(self.ranges.vertex[i].offset, 
      self.ranges.index[i].length, 
      self.ranges.index[i].offset)
  end
  pass:submit()
end
--[[
function SceneRenderer:traverse(pass, nodeIndex, node)
  assert(pass, "Invalid argument #1: render pass expected, got nil")
  assert(nodeIndex >= 0, "Invalid argument #2: node index not a positive")
  assert(node, "Invalid argument #3, scene node expected, got nil")
  assert(ffi.istype('aiNode', node), "Invalid argument #3, scene node expected")

  agen.log.verbose('video', "processing node [%d.%s] with %d children", 
    nodeIndex, 
    node:getName(), 
    node.mNumChildren)

  pass:drawIndexedPrimitive(self.ranges.vertex[nodeIndex].offset, 
    self.ranges.index[nodeIndex].length, 
    self.ranges.index[nodeIndex].offset)

  for i, n in node:iterChildren() do
    self:traverse(pass, nodeIndex + i, n)
  end
end
]]
function SceneRenderer:loadImage(filename)
  agen.log.debug('application', "loading image [%s]", filename)
	local fourcc = ffi.new('uint8_t[4]')
	local tex
	local ops = rwops.createFromPath(filename, 'rb')
	if ops:read(fourcc, 4, 1) ~= 1 then
	  error("Failed to read from file")
	end
	agen.log.debug('application', "[%c][%c][%c][%c]", fourcc[0], fourcc[1], fourcc[2], fourcc[3])
  agen.log.debug('application', 'loading texture by type...');
	if dds.isDDS(fourcc) then
	  local details = dds.getDetails(ops)
	  tex = self.video:createTexture(details)
	  -- fill with data
	  local destRect = ffi.new('SDL_Rect', {
		  x = 0,
		  y = 0,
		  w = details.width,
		  h = details.height})
	  local buffer = ffi.new('uint8_t[?]', details.size)
	  for level = 1, details.levels, 1 do
		  local len, pitch = dds.readMipLevel(ops, details, level - 1, buffer, details.size)
		  tex:updateRect(level - 1, destRect, buffer, len, pitch)
		  destRect.w = math.max(math.floor(destRect.w / 2), 1)
		  destRect.h = math.max(math.floor(destRect.h / 2), 1)
	  end
	else
	  if ops:seek(0, 0) ~= 0 then
		  error("Failed to reset stream")
	  end
	  local img = image.createFromRWops(ops, filename)
	  tex = self.video:createTextureFromSurface(img)
	end
	return tex
end

return SceneRenderer
