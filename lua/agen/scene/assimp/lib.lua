local ffi = require('ffi')
local string = require('string')

local lib = ffi.load('assimp')

ffi.cdef([[
typedef enum _aiComponent {
  /** Normal vectors */
  Component_NORMALS = 0x2,
  /** Tangents and bitangents go always together ... */
  Component_TANGENTS_AND_BITANGENTS = 0x4,
  /** ALL color sets
  * Use aiComponent_COLORn(N) to specify the N'th set */
  Component_COLORS = 0x8,
  /** ALL texture UV sets
  * aiComponent_TEXCOORDn(N) to specify the N'th set  */
  Component_TEXCOORDS = 0x10,
  /** Removes all bone weights from all meshes.
  * The scenegraph nodes corresponding to the bones are NOT removed.
  * use the #aiProcess_OptimizeGraph step to do this */
  Component_BONEWEIGHTS = 0x20,
  /** Removes all node animations (aiScene::mAnimations).
  * The corresponding scenegraph nodes are NOT removed.
  * use the #aiProcess_OptimizeGraph step to do this */
  Component_ANIMATIONS = 0x40,
  /** Removes all embedded textures (aiScene::mTextures) */
  Component_TEXTURES = 0x80,
  /** Removes all light sources (aiScene::mLights).
  * The corresponding scenegraph nodes are NOT removed.
  * use the #aiProcess_OptimizeGraph step to do this */
  Component_LIGHTS = 0x100,
  /** Removes all cameras (aiScene::mCameras).
  * The corresponding scenegraph nodes are NOT removed.
  * use the #aiProcess_OptimizeGraph step to do this */
  Component_CAMERAS = 0x200,
  /** Removes all meshes (aiScene::mMeshes). */
  Component_MESHES = 0x400,
  /** Removes all materials. One default material will
  * be generated, so aiScene::mNumMaterials will be 1. */
  Component_MATERIALS = 0x800,
  /** This value is not used. It is just there to force the
  *  compiler to map this enum to a 32 Bit integer. */
  Component_Force32Bit = 0x9fffffff
} aiComponent;

typedef int aiBool;
typedef enum _aiReturn {
	/** Indicates that a function was successful */
	SUCCESS = 0x0,
	/** Indicates that a function failed */
	FAILURE = -0x1,
	/** Indicates that not enough memory was available
	 * to perform the requested operation 
	 */
	OUTOFMEMORY = -0x3,
	/** @cond never 
		*  Force 32-bit size enum
		*/
	_AI_ENFORCE_ENUM_SIZE = 0x7fffffff
} aiReturn;

typedef void (__cdecl *aiLogStreamCallback)(const char*, char*);
typedef struct _aiLogStream {
	/** callback to be called */
	aiLogStreamCallback callback;
	/** user data to be passed to the callback */
	char* user;
} aiLogStream;
void aiAttachLogStream(const aiLogStream* stream);
void aiEnableVerboseLogging(aiBool d);
aiReturn aiDetachLogStream(const aiLogStream* stream);
void aiDetachAllLogStreams(void);

const char* aiGetErrorString();

unsigned int aiGetVersionMinor();
unsigned int aiGetVersionMajor();
unsigned int aiGetVersionRevision();

typedef struct _aiString {
  // Binary length of the string excluding the terminal 0
	uint32_t length;
	char data[1024];
} aiString;

typedef struct _aiColor4D {
  float r, g, b, a;
} aiColor4D;

#pragma pack(1)
typedef struct _aiColor3D {
  float r, g, b;
} aiColor3D;

typedef struct _aiVector3D {
	float x, y, z;
} aiVector3D;
#pragma pack()

typedef struct _aiVector2D {
  float x, y;
} aiVector2D;

typedef struct _aiMatrix4x4 {
	float m[16];
} aiMatrix4x4;

typedef struct _aiMaterialProperty aiMaterialProperty;
typedef struct _aiMaterial aiMaterial;

typedef struct _aiTexel {
  unsigned char b, g, r, a;
} aiTexel;

typedef struct _aiTexture {
  /** Width of the texture, in pixels
   *
   * If mHeight is zero the texture is compressed in a format
   * like JPEG. In this case mWidth specifies the size of the
   * memory area pcData is pointing to, in bytes.
   */
  unsigned int mWidth;
  /** Height of the texture, in pixels
   *
   * If this value is zero, pcData points to an compressed texture
   * in any format (e.g. JPEG).
   */
  unsigned int mHeight;
  char achFormatHint[9];
  aiTexel* pcData;
  aiString mFilename;
} aiTexture;

typedef struct _aiNode aiNode;
typedef struct _aiMetadata aiMetadata;
typedef struct _aiLight aiLight;
typedef struct _aiCamera aiCamera;
]])
-----------------------------------------------------------------------
--
-- aiString
--
-----------------------------------------------------------------------
local aiStringMT = {
  __len      = function(self) return self.length end,
  __tostring = function(self)
    assert(self.length >= 0 and self.length < 1024, "Invalid aiString: length " .. tostring(self.length) .. " too big")
    if self.length == 0 then return 'noname' end
    return ffi.string(self.data, self.length)
  end,
}
ffi.metatype('aiString', aiStringMT)
------------------------------------------------------------------------
--
-- aiLogStream
--
------------------------------------------------------------------------
local aiLogStreamMT = {
  __index = {},
  __gc    = function(self)
		lib.aiDetachLogStream(self)
    if self.callback then
		  self.callback:free()
    end
  end,
}

aiLogStreamMT.__index.attach = function(self)
	lib.aiAttachLogStream(self)
end

aiLogStreamMT.__index.detach = function(self)
	lib.aiDetachLogStream(self)
end

aiLogStreamMT.__index.setFunc = function(self, func)
	self.callback:set(func)
end

ffi.metatype('aiLogStream', aiLogStreamMT)

local defaultLogStream = ffi.new('aiLogStream', {
  callback = ffi.cast('aiLogStreamCallback', function(msg, user) end),
  user = nil,
})
-----------------------------------------------------------------------
--
-- accessor object
--
-----------------------------------------------------------------------
local assimp = {}

assimp.ffi = ffi

function assimp.GetErrorString()
	return ffi.string(lib.aiGetErrorString())
end

function assimp.EnableLogging(logfunc, verbose)
	defaultLogStream:setFunc(logfunc)
	defaultLogStream:attach()
	lib.aiEnableVerboseLogging(verbose)
end

function assimp.GetVersion()
  return string.format("%d.%d.%d",
    lib.aiGetVersionMajor(),
    lib.aiGetVersionMinor(),
    lib.aiGetVersionRevision())
end

local assimpMT = {__index = lib}
return setmetatable(assimp, assimpMT)
