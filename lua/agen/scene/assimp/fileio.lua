local sdl = require('sdl2.lib')
local rwops = require('sdl2.rwops')
local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
typedef enum _aiOrigin {
    /** Beginning of the file */
    aiOrigin_SET = 0x0,
    /** Current position of the file pointer */
    aiOrigin_CUR = 0x1,
    /** End of the file, offsets must be negative */
    aiOrigin_END = 0x2,
    /**  @cond never
     *   Force 32-bit size enum
     */
    _AI_ORIGIN_ENFORCE_ENUM_SIZE = 0x7fffffff
    /// @endcond
} aiOrigin;

typedef struct _aiFile aiFile;
typedef struct _aiFileIO aiFileIO;

// aiFile callbacks
typedef size_t   (*aiFileWriteProc)(aiFile*, const char*, size_t, size_t);
typedef size_t   (*aiFileReadProc) (aiFile*, char*, size_t,size_t);
typedef size_t   (*aiFileTellProc) (aiFile*);
typedef void     (*aiFileFlushProc)(aiFile*);
typedef aiReturn (*aiFileSeek)(aiFile*, size_t, aiOrigin);

// aiFileIO callbacks
typedef aiFile* (*aiFileOpenProc) (aiFileIO*, const char*, const char*);
typedef void    (*aiFileCloseProc)(aiFileIO*, aiFile*);

// Represents user-defined data
typedef char* aiUserData;

// ----------------------------------------------------------------------------------
/** @brief C-API: File system callbacks
 *
 *  Provided are functions to open and close files. Supply a custom structure to
 *  the import function. If you don't, a default implementation is used. Use custom
 *  file systems to enable reading from other sources, such as ZIPs
 *  or memory locations. */
struct aiFileIO {
    /** Function used to open a new file
     */
    aiFileOpenProc OpenProc;
    /** Function used to close an existing file
     */
    aiFileCloseProc CloseProc;
    /** User-defined, opaque data */
    aiUserData UserData;
};

// ----------------------------------------------------------------------------------
/** @brief C-API: File callbacks
 *
 *  Actually, it's a data structure to wrap a set of fXXXX (e.g fopen)
 *  replacement functions.
 *
 *  The default implementation of the functions utilizes the fXXX functions from
 *  the CRT. However, you can supply a custom implementation to Assimp by
 *  delivering a custom aiFileIO. Use this to enable reading from other sources,
 *  such as ZIP archives or memory locations. */
struct aiFile {
    /** Callback to read from a file */
    aiFileReadProc ReadProc;
    /** Callback to write to a file */
    aiFileWriteProc WriteProc;
    /** Callback to retrieve the current position of
     *  the file cursor (ftell())
     */
    aiFileTellProc TellProc;
    /** Callback to retrieve the size of the file,
     *  in bytes
     */
    aiFileTellProc FileSizeProc;
    /** Callback to set the current position
     * of the file cursor (fseek())
     */
    aiFileSeek SeekProc;
    /** Callback to flush the file contents
     */
    aiFileFlushProc FlushProc;
    /** User-defined, opaque data
     */
    aiUserData UserData;
};
]])

local aiFileIO = {}

local function aiFileRead(file, ptr, len, sz)
  local ops = ffi.cast('SDL_RWops*', file.UserData)
end

local function aiFileTell(file)
  local ops = ffi.cast('SDL_RWops*', file.UserData)
end

local function aiFileSize(file)
  local ops = ffi.cast('SDL_RWops*', file.UserData)
end

local function aiFileSeek(file, sz, offset)
  local ops = ffi.cast('SDL_RWops*', file.UserData)
end

local function aiFileFlush(file)
  local ops = ffi.cast('SDL_RWops*', file.UserData)
end

local function aiFileClose(fileIO)
end

local function aiFileOpen(fileIO, path, mode)
  assert(fileIO, "Invalid argument #1, cannot be nil")
  assert(ffi.istype('aiFileIO', fileIO), "Invalid argument #1, aiFileIO expected")
  assert(path, "Invalid argument #2, path cannot be nil")

  agen.log.debug('application', "Loading asset [%s] in [%s] mode", path, mode)
  local ops = rwops.createFromPath(path, mode)

  local file = ffi.new('aiFile')
  file.ReadProc  = ffi.cast('aiFileReadProc', aiFileRead)
  file.WriteProc = nil
  file.TellProc  = ffi.cast('aiFileTellProc', aiFileTell)
  file.FileSizeProc = ffi.cast('aiFileTellProc', aiFileSize)
  file.SeekProc  = ffi.cast('aiFileSeekProc', aiFileSeek)
  file.FlushProc = ffi.cast('aiFileFlushProc', aiFileFlush)
  file.UserData  = ffi.cast('aiUserData*', ops)

  fileIO.UserData = sdl.SDL_malloc(string.len(path) + 1)
  ffi.fill(fileIO.UserData, string.len(path) + 1)
  ffi.copy(fileIO.UserData, path, string.len(path))

  return ffi.gc(file, function(file)
    -- keep ops ref'ed
    ops = nil
  end)
end

local function aiFileIOFinalizer(fileIO)
  fileIO.openProc:free()
  fileIO.openProc = nil

  fileIO.closeProc:free()
  fileIO.closeProc = nil

  if fileIO.UserData then
    sdl.SDL_free(fileIO.UserData)
    fileIO.UserData  = nil
  end
end

function aiFileIO.create()
  local fileIO = ffi.new('aiFileIO')
  fileIO.openProc  = ffi.cast('aiFileOpenProc', aiFileOpen)
  fileIO.closeProc = ffi.cast('aiFileCloseProc', aiFileClose)
  fileIO.UserData  = nil
  return ffi.gc(fileIO, aiFileIOFinalizer)
end

return aiFileIO
