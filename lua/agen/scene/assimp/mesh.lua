local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
static const uint32_t MAX_NUMBER_OF_COLOR_SETS = 8;
static const uint32_t MAX_NUMBER_OF_TEXTURECOORDS = 8;

typedef enum _aiPrimitiveType {
	PTYPE_POINT		 = 0x00000001,
	PTYPE_LINE		 = 0x00000002,
	PTYPE_TRIANGLE = 0x00000004,
	PTYPE_POLYGON  = 0x00000008,
  PTYPE_NGONEncodingFlag = 0x00000010,
} aiPrimitiveType;

typedef struct _aiFace {
	unsigned int mNumIndices;
	unsigned int* mIndices;
} aiFace;

typedef struct _aiBone aiBone;
typedef struct _aiAnimMesh aiAnimMesh;

typedef struct _aiAABB {
  aiVector3D mMin;
  aiVector3D mMax;
} aiAABB;

typedef struct _aiMesh {
	unsigned int mPrimitiveTypes;
	unsigned int mNumVertices;
	unsigned int mNumFaces;
	aiVector3D* mVertices;
	aiVector3D* mNormals;
	aiVector3D* mTangents;
	aiVector3D* mBitangents;
	aiColor4D* mColors[MAX_NUMBER_OF_COLOR_SETS];
  /** Vertex texture coords, also known as UV channels.
   * A mesh may contain 0 to MAX_NUMBER_OF_TEXTURECOORDS per
   * vertex. NULL if not present. The array is mNumVertices in size.
   */
	aiVector3D* mTextureCoords[MAX_NUMBER_OF_TEXTURECOORDS];
	unsigned int mNumUVComponents[MAX_NUMBER_OF_TEXTURECOORDS];
	aiFace* mFaces;
	unsigned int mNumBones;
	aiBone** mBones;
/* index in aiScene.mMaterials[n] */
	unsigned int mMaterialIndex;
	aiString mName;
	unsigned int mNumAnimMeshes;
	aiAnimMesh** mAnimMeshes;
  unsigned int mMethod;
  aiAABB mAABB;
  aiString **mTextureCoordsNames;
} aiMesh;
]])
----------------------------------------------------------------
--
-- aiFace
--
----------------------------------------------------------------
local aiFaceMT = {
  __index  = {},
  __len    = function(self) return self.mNumIndices end,
  __ipairs = nil,
}

aiFaceMT.__ipairs = function(self)
  local index = -1
  local num   = self.mNumIndices
  return function()
    index = index + 1
    if index < num then return index, self.mIndices[index] end
  end
end

aiFaceMT.__index.to_array = function(self, ctype)
  local sz = self.mNumIndices
  local ret = ffi.new(ctype .. '[?]', sz)
  for i = 0, sz - 1, 1 do
    ret[i] = ffi.cast(ctype, self.mIndices[i])
  end
  return ret
end

ffi.metatype('aiFace', aiFaceMT)
----------------------------------------------------------------
--
-- aiMesh
--
----------------------------------------------------------------
local aiMesh = {}

function aiMesh:getNumColorChannels()
	local n = 0
	while n < lib.MAX_NUMBER_OF_COLOR_SETS and self.mColors[n] do
		n = n + 1
	end
	return n
end

function aiMesh:hasBones()
	return self.mBones ~= nil and self.mNumBones > 0
end

function aiMesh:getNumUVChannels()
	local n = 0
	while n < lib.MAX_NUMBER_OF_TEXTURECOORDS and self.mTextureCoords[n] do
		n = n + 1
	end
	return n
end

function aiMesh:getName()
	return tostring(self.mName)
end

function aiMesh:getBytesVertices()
  return self.mNumVertices * ffi.sizeof('aiVector3D')
end

function aiMesh:getNumIndices()
  local numIndices = 0
  for f = 0, self.mNumFaces - 1, 1 do
    numIndices = numIndices + self.mFaces[f].mNumIndices
  end
  return numIndices
end

--- Pack all indices into a C array. Needed for static index buffers (cannot be memory mapped).
-- @param ctype C type of the indices, 32 or 16 bit.
function aiMesh:getIndexBuffer(ctype)
  local numIndices = self:getNumIndices()
  local offset = 0
  local face
  local ret = ffi.new(ctype .. '[?]', numIndices)
  
  if ctype == nil or (ffi.sizeof(ctype) == ffi.sizeof('unsigned int')) then
    --
    -- as-is
    --
    for f = 0, self.mNumFaces - 1, 1 do
      face = self.mFaces[f]
      ffi.copy(ret + offset, face.mIndices, face.mNumIndices * ffi.sizeof(ctype))
      offset = offset + face.mNumIndices
    end
    return ret
  end

  if ffi.sizeof(ctype) == ffi.sizeof('uint16_t') then
    --
    -- downcast to 16-bit
    --
    local array
    for f = 0, self.mNumFaces - 1, 1 do
      face = self.mFaces[f]
      array = face:to_array('uint16_t')
      ffi.copy(ret + offset, array, ffi.sizeof(array))
      offset = offset + face.mNumIndices
    end
    return ret  
  end

  error("Invalid index buffer type [" .. ctype .. ']')
end

local aiMeshMT = {__index = aiMesh}
return ffi.metatype('aiMesh', aiMeshMT)
