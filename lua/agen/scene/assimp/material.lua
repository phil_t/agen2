local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
typedef enum _aiPropertyTypeInfo {
  // 32-bit float(s)
  PTI_Float   = 0x1,
  // aiString
  PTI_String  = 0x3,
  // 32-bit int(s)
  PTI_Integer = 0x4,
  // binary
  PTI_Buffer  = 0x5,
  PTI_Force32Bit = 0xffffffff
} aiPropertyTypeInfo;

typedef enum _aiTextureType {
  TextureType_NONE = 0x0,
  TextureType_DIFFUSE = 0x1,
  TextureType_SPECULAR = 0x2,
  TextureType_AMBIENT = 0x3,
  TextureType_EMISSIVE = 0x4,
  TextureType_HEIGHT = 0x5,
  TextureType_NORMALS = 0x6,
  TextureType_SHININESS = 0x7,
  TextureType_OPACITY = 0x8,
  TextureType_DISPLACEMENT = 0x9,
  TextureType_LIGHTMAP = 0xA,
  TextureType_REFLECTION = 0xB,
  TextureType_UNKNOWN = 0xC,
  TextureType_Force32Bit = 0xffffffff
} aiTextureType;

/** @brief Defines how the mapping coords for a texture are generated.
 *
 *  Real-time applications typically require full UV coordinates, so the use of
 *  the aiProcess_GenUVCoords step is highly recommended. It generates proper
 *  UV channels for non-UV mapped objects, as long as an accurate description
 *  how the mapping should look like (e.g spherical) is given.
 *  See the #AI_MATKEY_MAPPING property for more details.
 */
typedef enum _aiTextureMapping {
  /** The mapping coordinates are taken from an UV channel.
  *
  *  The #AI_MATKEY_UVWSRC key specifies from which UV channel
  *  the texture coordinates are to be taken from (remember,
  *  meshes can have more than one UV channel).
  */
  TextureMapping_UV = 0x0,
  /** Spherical mapping */
  TextureMapping_SPHERE = 0x1,
  /** Cylindrical mapping */
  TextureMapping_CYLINDER = 0x2,
  /** Cubic mapping */
  TextureMapping_BOX = 0x3,
  /** Planar mapping */
  TextureMapping_PLANE = 0x4,
  /** Undefined mapping. Have fun. */
  TextureMapping_OTHER = 0x5,
  TextureMapping_Force32Bit = 0xffffffff,
} aiTextureMapping;

/** @brief Defines how UV coordinates outside the [0...1] range are handled.
 *
 *  Commonly refered to as 'wrapping mode'.
 */
typedef enum _aiTextureMapMode {
  /** A texture coordinate u|v is translated to u%1|v%1
   */
  TextureMapMode_Wrap = 0x00000000,
  /** Texture coordinates outside [0...1]
   *  are clamped to the nearest valid value.
   */
  TextureMapMode_Clamp = 0x00000001,
  /** A texture coordinate u|v becomes u%1|v%1 if (u-(u%1))%2 is zero and
   *  1-(u%1)|1-(v%1) otherwise
   */
  TextureMapMode_Mirror = 0x00000002,
  /** If the texture coordinates for a pixel are outside [0...1]
   *  the texture is not applied to that pixel
   */
  TextureMapMode_Decal = 0x00000003,
  TextureMapMode_Force32Bit = 0xffffffff
} aiTextureMapMode;

/** @brief Defines how the Nth texture of a specific type is combined with
 *  the result of all previous layers.
 *
 *  Example (left: key, right: value): <br>
 *  @code
 *  DiffColor0     - gray
 *  DiffTextureOp0 - aiTextureOpMultiply
 *  DiffTexture0   - tex1.png
 *  DiffTextureOp0 - aiTextureOpAdd
 *  DiffTexture1   - tex2.png
 *  @endcode
 *  Written as equation, the final diffuse term for a specific pixel would be:
 *  @code
 *  diffFinal = DiffColor0 * sampleTex(DiffTexture0,UV0) +
 *     sampleTex(DiffTexture1,UV0) * diffContrib;
 *  @endcode
 *  where 'diffContrib' is the intensity of the incoming light for that pixel.
 */
typedef enum _aiTextureOp {
  /** T = T1 * T2 */
  TextureOp_Multiply = 0x0,
  /** T = T1 + T2 */
  TextureOp_Add = 0x1,
  /** T = T1 - T2 */
  TextureOp_Subtract = 0x2,
  /** T = T1 / T2 */
  TextureOp_Divide = 0x3,
  /** T = (T1 + T2) - (T1 * T2) */
  TextureOp_SmoothAdd = 0x4,
  /** T = T1 + (T2-0.5) */
  TextureOp_SignedAdd = 0x5,
  TextureOp_Force32Bit = 0xffffffff
} aiTextureOp;

typedef enum _aiTextureFlags {
    /** The texture's color values have to be inverted (component-wise 1-n)
     */
    aiTextureFlags_Invert = 0x1,
    /** Explicit request to the application to process the alpha channel
     *  of the texture.
     *
     *  Mutually exclusive with #aiTextureFlags_IgnoreAlpha. These
     *  flags are set if the library can say for sure that the alpha
     *  channel is used/is not used. If the model format does not
     *  define this, it is left to the application to decide whether
     *  the texture alpha channel - if any - is evaluated or not.
     */
    aiTextureFlags_UseAlpha = 0x2,
    /** Explicit request to the application to ignore the alpha channel
     *  of the texture.
     *
     *  Mutually exclusive with #aiTextureFlags_UseAlpha.
     */
    aiTextureFlags_IgnoreAlpha = 0x4,
    _aiTextureFlags_Force32Bit = 0xffffffff
} aiTextureFlags;

#pragma pack(1)
/** @brief Defines how an UV channel is transformed.
 *
 *  This is just a helper structure for the #AI_MATKEY_UVTRANSFORM key.
 *  See its documentation for more details.
 *
 *  Typically you'll want to build a matrix of this information. However,
 *  we keep separate scaling/translation/rotation values to make it
 *  easier to process and optimize UV transformations internally.
 */
typedef struct _aiUVTransform {
  /** Translation on the u and v axes.
   *
   *  The default value is (0|0).
   */
  aiVector2D mTranslation;
  /** Scaling on the u and v axes.
   *
   *  The default value is (1|1).
   */
  aiVector2D mScaling;
  /** Rotation - in counter-clockwise direction.
   *
   *  The rotation angle is specified in radians. The
   *  rotation center is 0.5f|0.5f. The default value
   *  0.f.
   */
  float mRotation;
} aiUVTransform;
#pragma pack()

struct _aiMaterialProperty {
  aiString mKey;
  unsigned int mSemantic;
  unsigned int mIndex;
  unsigned int mDataLength;
  aiPropertyTypeInfo mType;
  char* mData;
};

struct _aiMaterial {
  /** List of all material properties loaded. */
  aiMaterialProperty** mProperties;  
  /** Number of properties in the data base */
  unsigned int mNumProperties;
  /** Storage allocated */
  unsigned int mNumAllocated;
};

aiReturn aiGetMaterialProperty(const aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, const aiMaterialProperty** pPropOut);
aiReturn aiGetMaterialFloatArray(const aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, float* pOut, unsigned int* pMax);
aiReturn aiGetMaterialIntegerArray(const aiMaterial* pMat, const char* pKey, unsigned int  type, unsigned int  index, int* pOut, unsigned int* pMax);
aiReturn aiGetMaterialColor(const aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, aiColor4D* pOut);
aiReturn aiGetMaterialUVTransform(const aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, aiUVTransform* pOut);
aiReturn aiGetMaterialString(const aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, aiString* pOut);
unsigned int aiGetMaterialTextureCount(const aiMaterial* pMat, aiTextureType type);
aiReturn aiGetMaterialTexture(const aiMaterial* mat,
  aiTextureType type,
  unsigned int  index,
  aiString* path,
  aiTextureMapping* mapping  /*= NULL*/,
  unsigned int* uvindex       /*= NULL*/,
  float* blend            /*= NULL*/,
  aiTextureOp* op        /*= NULL*/,
  aiTextureMapMode* mapmode  /*= NULL*/,
  unsigned int* flags  /*= NULL*/);
]])
-----------------------------------------------------------------------
--
-- aiMaterialProperty
--
-----------------------------------------------------------------------
local aiMaterialProperty = {}
local aiMaterialPropertyMT = {__index = aiMaterialProperty}

function aiMaterialProperty:name()
  return tostring(self.mKey)
end

function aiMaterialProperty:type()
  return tonumber(self.mType)
end

function aiMaterialProperty:value(mat)
  if     self.mType == lib.PTI_Float   then
    return 'float'
  elseif self.mType == lib.PTI_String  then
    if self.mSemantic == 0 then
      return mat:GetPropString(self:name()) or 'nil'
    else
      -- texture property
      return mat:GetPropTexture(self.mIndex, self.mSemantic)
    end 
  elseif self.mType == lib.PTI_Integer then
    return 'int'
  elseif self.mType == lib.PTI_Buffer  then
    return 'buffer'
  else
    error("Unsupported material type [" .. tonumber(self.mType) .. "]")
  end
end

ffi.metatype('aiMaterialProperty', aiMaterialPropertyMT)
-----------------------------------------------------------------------
--
-- aiMaterial
--
-----------------------------------------------------------------------
local aiMaterial = {}
local aiMaterialMT = {
  __index = aiMaterial,
  __len   = function(self) 
    return self.mNumProperties 
  end,
  __ipairs = function(self)
    local i = -1
    local num = self.mNumProperties
    return function()
      i = i + 1
      if i < num then 
        return i, self.mProperties[i] 
      end
    end
  end,
  __pairs = function(self)
    local i = -1
    local num = self.mNumProperties
    return function()
      i = i + 1
      if i < num then 
        local prop = self.mProperties[i]
        return prop:name(), prop
      end
    end
  end,
}

function aiMaterial:GetProp(name, type, index)
  assert(name, "Invalid parameter #1, property name required")
  assert(type, "Invalid parameter #2, property type required")
  -- pointer to the database value, owned by material
  local prop = ffi.new('aiMaterialProperty*[1]')
  local err = lib.aiGetMaterialProperty(self, name, type, index or 0, prop)
  if err ~= lib.SUCCESS then
    -- not found?
    return nil
  end
  return prop[0]
end
--- Get material property value as lua type
function aiMaterial:GetPropValue(prop)
  assert(prop, "Invalid parameter #1, property cannot be nil")
  assert(ffi.istype('aiMaterialProperty', prop), 
    "Invalid parameter #1, aiMaterialProperty expected")
  return prop:value(self)
end

function aiMaterial:GetPropString(name)
  assert(name, "Invalid argument #1, property name required")
  local str = ffi.new('aiString')
  local err = lib.aiGetMaterialString(self, name, 0, 0, str)
  if err ~= lib.SUCCESS then
    -- not set or error?
    return nil 
  end
  return tostring(str)
end

function aiMaterial:GetPropColor(name, color)
  assert(name, "Invalid argument #1, property name required")
  assert(color, "Invalid argument #2, destination color required")
  local err   = lib.aiGetMaterialColor(self, name, 0, 0, color)
  if err ~= lib.SUCCESS then
    return nil
  end
  return color
end

function aiMaterial:GetMaterialTextureCount(type)
  assert(type, "Invalid argument #1, type is required")
  return lib.aiGetMaterialTextureCount(self, type)
end

function aiMaterial:GetPropTexture(index, type)
  if self:GetMaterialTextureCount(type) < index then
    return nil
  end
  local path = ffi.new('aiString')
  local mapping = ffi.new('aiTextureMapping[1]')
  -- param[out] mapmode Receives the mapping modes to be used for the texture.
  --            Pass NULL if you're not interested in this information. Otherwise,
  --            pass a pointer to an array of two aiTextureMapMode's (one for each
  --            axis, UV order).
  local mapmode = ffi.new('aiTextureMapMode[2]')
  local flags = ffi.new('unsigned int[1]')
  local err = lib.aiGetMaterialTexture(self, type, index, path, mapping, nil, nil, nil, mapmode, flags)
  if err ~= lib.SUCCESS then
    return nil
  end

  if mapping[0] ~= lib.TextureMapping_UV then
    error("Unsupported texture mapping [" .. mapping[0] .. "]")
  end

  agen.log.verbose('application', "mapping mode [%dx%d], flags [%d]", mapmode[0], mapmode[1], flags[0])
  return tostring(path), mapmode, flags[0]
end
--- Get material name
-- @return material name as lua string
function aiMaterial:GetName()
  return self:GetPropString("?mat.name") or 'noname'
end

return ffi.metatype('aiMaterial', aiMaterialMT) 
