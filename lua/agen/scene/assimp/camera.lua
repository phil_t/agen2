local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
struct _aiCamera {
  aiString mName;
  aiVector3D mPosition;
  aiVector3D mUp;
  aiVector3D mLookAt;
  float mHorizontalFOV;
  float mClipPlaneNear;
  float mClipPlaneFar;
  float mAspect;
};
]])

local aiCamera = {}
local aiCameraMT = {__index = aiCamera}

function aiCamera:getName()
  return tostring(self.mName)
end

return ffi.metatype('aiCamera', aiCameraMT)
