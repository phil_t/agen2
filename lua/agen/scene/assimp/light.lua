local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
typedef enum _aiLightSourceType {
  aiLightSource_UNDEFINED   = 0x0,
  //! A directional light source has a well-defined direction
  //! but is infinitely far away. That's quite a good
  //! approximation for sun light.
  aiLightSource_DIRECTIONAL = 0x1,
  //! A point light source has a well-defined position
  //! in space but no direction - it emits light in all
  //! directions. A normal bulb is a point light.
  aiLightSource_POINT      = 0x2,
  //! A spot light source emits light in a specific
  //! angle. It has a position and a direction it is pointing to.
  //! A good example for a spot light is a light spot in
  //! sport arenas.
  aiLightSource_SPOT       = 0x3,
  //! The generic light level of the world, including the bounces
  //! of all other lightsources.
  //! Typically, there's at most one ambient light in a scene.
  //! This light type doesn't have a valid position, direction, or
  //! other properties, just a color.
  aiLightSource_AMBIENT    = 0x4,
  aiLightSource_Force32Bit = 0xffffffff
} aiLightSourceType;

struct _aiLight {
  aiString mName;
  aiLightSourceType mType;
  aiVector3D mPosition;
  aiVector3D mDirection;
  aiVector3D mUp;
  float mAttenuationConstant;
  float mAttenuationLinear;
  float mAttenuationQuadratic;
  aiColor3D mColorDiffuse;
  aiColor3D mColorSpecular;
  aiColor3D mColorAmbient;
  float mAngleInnerCone;
  float mAngleOuterCone;
  aiVector2D mSize;
};
]])

local aiLight = {}
local aiLightMT = {__index = aiLight}

function aiLight:getName()
  return tostring(self.mName)
end

return ffi.metatype('aiLight', aiLightMT)
