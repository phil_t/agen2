local lib = require('agen.scene.assimp.lib')
local ffi = lib.ffi

ffi.cdef([[
struct _aiNode {
	aiString      mName;  
	aiMatrix4x4   mTransformation;
	aiNode*       mParent;
	unsigned int  mNumChildren;
	aiNode**      mChildren;
	unsigned int  mNumMeshes;
	unsigned int* mMeshes;
	aiMetadata*   mMetaData;
};
]])

local aiNode = {}
local aiNodeMT = {__index = aiNode}

function aiNode:getName()
  return tostring(self.mName)
end

function aiNode:getAABB(min, max, xform)
  for i = 0, self.mNumChildren - 1, 1 do
    self.mChildren[i]:getAABB(min, max, xform)
  end
end

function aiNode:iterChildren()
  local i = -1
  local num = self.mNumChildren
  return function()
    i = i + 1
    if i < num then return i, self.mChildren[i] end
  end
end

function aiNode:getParent()
  return self.mParent
end

function aiNode:getXForm()
  return self.mTransformation.m
end

return ffi.metatype('aiNode', aiNodeMT)
