local assimp = require('agen.scene.assimp.lib')
local ffi = assimp.ffi
local bit = require('bit')

assimp.mesh  = require('agen.scene.assimp.mesh')
assimp.node  = require('agen.scene.assimp.node')
assimp.scene = require('agen.scene.assimp.scene')
assimp.material = require('agen.scene.assimp.material')
assimp.camera   = require('agen.scene.assimp.camera')
assimp.light    = require('agen.scene.assimp.light')
assimp.renderer = require('agen.scene.assimp.renderer')

do
  -- redirect assimp log to agen log
  agen.log.debug('application', "Using assimp %s", assimp.GetVersion())
	assimp.EnableLogging(function(msg, user)
    local luastring = ffi.string(msg)
    local cat, text = luastring:match("([^,]+),%s*([^,]+)")
    if cat ~= nil and text ~= nil then
      agen.log.debug(cat, "%s", text)
    else
      agen.log.debug('warn', "%s", msg)
    end  
	end, 0)
end
----------------------------------------------------------------------
--
-- Scene object
--
----------------------------------------------------------------------
local defaultProps = {
  PP_RVC_FLAGS = bit.bor(assimp.Component_TANGENTS_AND_BITANGENTS, assimp.Component_COLORS),
  -- HW limits per draw call
  -- TODO: from device
  PP_SLM_VERTEX_LIMIT   = 1000000,
  PP_SLM_TRIANGLE_LIMIT = 1000000,
  PP_LBW_MAX_WEIGHTS    = 4,
  -- calc scene AABB and normalize scene
  PP_PTV_NORMALIZE = false,
  -- hw-specific vertex cache
  PP_ICL_PTCACHE_SIZE = 12,
  -- exclude all primitives except triangles
  PP_SBP_REMOVE = bit.bor(assimp.PTYPE_POINT, assimp.PTYPE_LINE, assimp.PTYPE_POLYGON),
}

function assimp.createScene(opts)
  -- TODO: from opts
  local flags = bit.bor(
    assimp.Triangulate,
		--assimp.GenNormals, assimp.FixInfacingNormals,
    assimp.OptimizeGraph
  )

  local scene = assimp.scene.new(flags, defaultProps)
  if bit.band(scene.mFlags, assimp.INCOMPLETE) ~= 0 then
    error('scene incomplete')
  end

	agen.log.debug('application', "scene has [%d] meshes, [%d] embedded textures and [%d] cameras", 
    scene.mNumMeshes, 
    scene.mNumTextures, 
    scene.mNumCameras)
  return scene
end

function assimp.createSceneFromPath(path, opts)
  -- TODO: from opts
  local flags = bit.bor(
	  -- index vertices (will create index buffer)
		assimp.JoinIdenticalVertices,
		-- convert to triangles
		assimp.Triangulate,
    -- remove unused data for example per-vertex color breaking indexing
    -- use PP_RVC_FLAGS to specify which
    assimp.RemoveComponent,
    -- generate normals
		assimp.GenNormals, assimp.FixInfacingNormals,
    -- limit mesh sizes to avoid exceeding device's limits
    assimp.SplitLargeMeshes,
    -- limit per-vertex weights to avoid exceeding device's limits
    assimp.LimitBoneWeights,
		assimp.ImproveCacheLocality,
		assimp.SortByPType,
    assimp.ValidateDataStructure,
    -- default is CCW, make CW
    -- assimp.FlipWindingOrder,
    -- only UV texture mappings
		assimp.GenUVCoords, 
    -- flips all UV coordinates along the y-axis and adjusts material settings and bitangents accordingly
    assimp.FlipUVs,    
    -- aiProcess_OptimizeGraph and #aiProcess_PreTransformVertices are incompatible
    assimp.OptimizeGraph,
		assimp.OptimizeMeshes)

  local scene = assimp.scene.newFromFileExWithProps(path, flags, nil, defaultProps)
  if bit.band(scene.mFlags, assimp.INCOMPLETE) ~= 0 then
    error('scene incomplete')
  end

	agen.log.debug('application', "scene has [%d] meshes, [%d] embedded textures and [%d] cameras", 
    scene.mNumMeshes, 
    scene.mNumTextures, 
    scene.mNumCameras)
  return scene
end

function assimp.createRenderer(scene, baseDir)
  return assimp.renderer.create(scene, baseDir)
end

return assimp
