local al = require('agen.audio.AL.device')

return {
  createDevice = function(window, config)
    return al.create(window, config)
  end,
}
