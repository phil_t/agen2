local sdl = require('sdl2')
local device = require('agen.audio.SDL.device')

return {
  createDevice = function(window, config)
    if sdl.SDL_InitSubSystem(sdl.INIT_AUDIO) ~= 0 then
      error("Init audio failed: " .. sdl.GetError())
    end

    agen.log.info('audio', "Initialized audio driver [%s]", 
      sdl.SDL_GetCurrentAudioDriver())
    return device.create(window, config)
  end,
}
