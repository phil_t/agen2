local ffi = require('ffi')
local sdl = require('sdl2')
local bit = require('bit')
local mix = ffi.load('SDL2_mixer')

ffi.cdef([[
typedef enum _MIX_InitFlags {
    INIT_FLAC        = 0x00000001,
    INIT_MOD         = 0x00000002,
    INIT_MODPLUG     = 0x00000004,
    INIT_MP3         = 0x00000008,
    INIT_OGG         = 0x00000010,
    INIT_FLUIDSYNTH  = 0x00000020
} MIX_InitFlags;

int Mix_Init(MIX_InitFlags flags);
void Mix_Quit(void);

/* Mix_OpenAudio uses legacy SDL_OpenAudio() and can only act on Device ID #1. */
int Mix_OpenAudio(int frequency, Uint16 format, int channels, int chunksize);
void Mix_CloseAudio(void);
]])

do
  if sdl.SDL_InitSubSystem(sdl.INIT_AUDIO) ~= 0 then
    error("Init audio failed: " .. sdl.GetError())
  else
    agen.log.info('audio', "Initialized audio driver [%s]", sdl.SDL_GetCurrentAudioDriver())
  end
  --
  -- returns initialized flags or 0 on error
  --
	local err = mix.Mix_Init(bit.bor(mix.INIT_OGG, 0))
	if err == 0 then
		agen.log.error('audio', "failed to load audio plugin [%s]", sdl.GetError())
  else
    --sdl.SDL_LogInfo(sdl.LOG_CATEGORY_AUDIO, "Loaded audio driver [%s]", sdl.SDL_GetCurrentAudioDriver())
	end
  if bit.band(err, mix.INIT_OGG) ~= mix.INIT_OGG then
    agen.log.info('audio', "OGG will not be supported")
  end
end

local SDLmixer = {}

SDLmixer.createDevice = function(window, config)
  --agen.log.info('audio', "Initialized audio driver [%s]", sdl.SDL_GetCurrentAudioDriver())
  return nil
end

return SDLmixer
