local sdl = require('sdl2')
local bit = require('bit')
local string = require('string')
local ffi = sdl.ffi

ffi.cdef([[
typedef Uint32 SDL_AudioDeviceID;

typedef enum _SDL_AudioStatus {
  AUDIO_STOPPED = 0,
  AUDIO_PLAYING,
  AUDIO_PAUSED
} SDL_AudioStatus;

typedef enum _SDL_AudioChangeFlags {
  AUDIO_ALLOW_FREQUENCY_CHANGE = 0x00000001,
  AUDIO_ALLOW_FORMAT_CHANGE    = 0x00000002,
  AUDIO_ALLOW_CHANNELS_CHANGE  = 0x00000004,
} SDL_AudioChangeFlags;

typedef struct _SDL_AudioDevice {
  SDL_AudioDeviceID id;
  SDL_AudioSpec spec;
} SDL_AudioDevice;

SDL_AudioDeviceID SDL_OpenAudioDevice(const char* device,  
									  int 				   iscapture,
									  const SDL_AudioSpec* desired,
									  SDL_AudioSpec*       obtained,
									  SDL_AudioChangeFlags allowed_changes);
void SDL_CloseAudioDevice(SDL_AudioDeviceID dev);
SDL_AudioStatus SDL_GetAudioDeviceStatus(SDL_AudioDeviceID dev);
void SDL_PauseAudioDevice(SDL_AudioDeviceID dev, int pause_on);
]])

local mapChannels = {
  mono      = 1,
  stereo    = 2,
  quadro    = 4,
  five_one  = 6,
  seven_one = 8,
}

local function sdlAudioFormat(mode)
  -- least 8 bits is sample size
  local format = mode.bits

  if mode.bits == 32 then
    -- signed float
    format = bit.bor(format, bit.lshift(1, 15), bit.lshift(1, 8))
  elseif mode.bits == 24 then
    error("24-bit audio formats not implemented")
  elseif mode.bits == 16 then
    -- signed short
    format = bit.bor(format, bit.lshift(1, 15))
  elseif mode.bits == 8 then
    -- unsigned char
    error("8-bit audio formats not implemented")
  else
    error(string.format("Unsupported audio sample size [%d]", mode.bits))
  end

  return ffi.cast('SDL_AudioFormat', format)
end

local SDL_AudioDevice = {}
local SDL_AudioDeviceMT = {
  __index = SDL_AudioDevice,
  __tostring = function(self)
    return ffi.string(sdl.SDL_GetAudioDeviceName(0, 0))
  end,
}
--- Create default audio device
-- @param window SDL window
-- @param config audio config table 
-- @return SDL_AudioDevice
function SDL_AudioDevice.create(window, config)
  local spec = ffi.new('SDL_AudioSpec', {
    freq     = config.rate,
    format   = sdlAudioFormat(config),
    channels = mapChannels[config.channels],
    --
    -- SDL will run callback in a separate thread and we can't use ffi callback here.
    --
    callback = nil,
  })

  local self = ffi.new('SDL_AudioDevice')
  self.id = sdl.SDL_OpenAudioDevice(nil, 0, spec, self.spec, 
    bit.bor(sdl.AUDIO_ALLOW_FREQUENCY_CHANGE, sdl.AUDIO_ALLOW_FORMAT_CHANGE, sdl.AUDIO_ALLOW_CHANNELS_CHANGE))

  if self.id == 0 then
    error(string.format("Failed to open audio device [%s]", sdl.GetError()))
  end
  --
  -- created "paused", unpause
  --
  sdl.SDL_PauseAudioDevice(self.id, 0)

  agen.log.info('audio', "Initialized audio device [%s]", tostring(self))

  return ffi.gc(self, function(self)
    sdl.SDL_CloseAudioDevice(self.id)
  end)
end

function SDL_AudioDevice:createBuffer(opts)
  return nil
end

function SDL_AudioDevice:createSource()
  return nil
end

function SDL_AudioDevice:finalize()
end

return ffi.metatype('SDL_AudioDevice', SDL_AudioDeviceMT)
