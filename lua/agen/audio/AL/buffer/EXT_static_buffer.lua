local al = require('agen.audio.AL.lib')
local ffi = al.ffi

ffi.cdef([[
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	AL_EXT_STATIC_BUFFER
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
typedef ALvoid (*PFNALBUFFERDATASTATICPROC)(ALint bid, ALenum format, const ALvoid* data, ALsizei size, ALsizei freq);

]])

local ALStaticBuffer = {}
ALStaticBuffer.symtab = {
  BufferDataStatic = 'PFNALBUFFERDATASTATICPROC',
}

function ALStaticBuffer:update(opts)
  assert(opts.format,   "Missing required option format")
  assert(opts.channels, "Missing required option channels")
  assert(opts.data,     "Missing required option data")
  assert(opts.len and opts.len > 0,    
                        "Missing or invalid required option len")
  assert(opts.freq,     "Missing required options freq")

  al.BufferDataStatic(self:getID(), self:SDLFormat2ALFormat(opts.channels, opts.format), 
    opts.data, opts.len, opts.freq)
  local err = al.alGetError()
  if err ~= 0 then
    error(string.format("Failed to update AL buffer %d: %s", 
      self:getID(), 
      al.GetString(err)))
  end
end

return ALStaticBuffer
