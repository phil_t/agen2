local al = require('agen.audio.AL.lib')
local ffi = al.ffi

ffi.cdef([[
static const uint32_t FORMAT_MONO_FLOAT32   = 0x10010;
static const uint32_t FORMAT_STEREO_FLOAT32 = 0x10011;
]])

local ExtFloat32 = {}

return ExtFloat32
