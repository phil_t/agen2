local ffi = require('ffi')

ffi.cdef([[
static const uint16_t FORMAT_QUAD8   = 0x1204;
static const uint16_t FORMAT_QUAD16  = 0x1205;
static const uint16_t FORMAT_QUAD32  = 0x1206;
static const uint16_t FORMAT_REAR8   = 0x1207;
static const uint16_t FORMAT_REAR16  = 0x1208;
static const uint16_t FORMAT_REAR32  = 0x1209;
static const uint16_t FORMAT_51CHN8  = 0x120A;
static const uint16_t FORMAT_51CHN16 = 0x120B;
static const uint16_t FORMAT_51CHN32 = 0x120C;
static const uint16_t FORMAT_61CHN8  = 0x120D;
static const uint16_t FORMAT_61CHN16 = 0x120E;
static const uint16_t FORMAT_61CHN32 = 0x120F;
static const uint16_t FORMAT_71CHN8  = 0x1210;
static const uint16_t FORMAT_71CHN16 = 0x1211;
static const uint16_t FORMAT_71CHN32 = 0x1212;
]])

return {}
