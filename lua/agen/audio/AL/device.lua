local string = require('string')
local al     = require('agen.audio.AL.lib')
local alc    = require('agen.audio.AL.context')
local buffer = require('agen.audio.AL.buffer')
local src    = require('agen.audio.AL.source')
local ffi    = al.ffi

ffi.cdef([[
typedef struct _ALDevice {
  ALCdevice* device;
  ALCcontext* ctx;
} ALDevice;

ALCdevice* alcOpenDevice(const ALCchar *devicename);
ALCboolean alcCloseDevice(ALCdevice *device);
ALCenum    alcGetError(ALCdevice *device);
ALCboolean alcIsExtensionPresent(ALCdevice *device, const ALCchar *extname);
void*      alcGetProcAddress(ALCdevice *device, const ALCchar *funcname);
ALCenum    alcGetEnumValue(ALCdevice *device, const ALCchar *enumname);
const ALCchar* alcGetString(ALCdevice *device, ALCenum param);
void       alcGetIntegerv(ALCdevice *device, ALCenum param, ALCsizei size, ALCint *data);
]])

local ALDevice = {}
local ALDeviceMT = {
  __index = ALDevice,
  __newindex = function(self, k, v)
    local sym = ffi.cast(v, al.alcGetProcAddress(self, 'al' .. k))
		if sym then
			rawset(ALDevice, k, sym)
		else
			error("Undefined symbol [" .. v .. ' ' .. k .. "]")
		end
  end,
  __tostring = function(self)
    local deviceName = al.alcGetString(self.device, al.ALC_DEVICE_SPECIFIER)
    if deviceName then
      return ffi.string(deviceName)
    end
    return 'unknown'
  end,
  __gc = function(self)
    self.ctx = nil
    al.alcCloseDevice(self.device)
  end,
}

function ALDevice.create(window, config)
  local device = al.alcOpenDevice(nil)
  if device == nil then
    local err = al.alGetError()
    error(string.format("Failed to open default device: %s", al.GetString(err)))
  end
  -- finalizer now valid, call new()
  local self  = ffi.new('ALDevice')
  self.device = device
  self.ctx    = self:createContext(config)
  agen.log.info('audio', "Initialized audio device [%s]", tostring(self))
  -- setup extensions
  buffer.setup()
  src.setup()
  return self
end
--- Each thread needs its own context
function ALDevice:createContext(config)
  local attribs = ffi.new('ALCint[11]', {
    al.ALC_FREQUENCY,      config.rate or 44100, -- openAL mixer's rate
    al.ALC_MONO_SOURCES,  64,
    al.ALC_STEREO_SOURCES, 2,
    al.ALC_REFRESH,       60, -- update rate of context processing
    al.ALC_SYNC,           0,
    0
  })

  local ctx = alc.create(self.device, attribs)
  ctx:makeCurrent()
  return ctx
end

function ALDevice:getString(enum)
  return ffi.string(al.alcGetString(self.device, enum))
end

function ALDevice:getErrorString()
  return self:getString(al.alcGetError(self.device))
end
--------------------------------------------------------------
--
-- Audio buffers
--
--------------------------------------------------------------
function ALDevice:createBuffer(opts)
  return buffer.create(opts)
end
--------------------------------------------------------------
--
-- Audio sources: points in space producing audio (players)
--
--------------------------------------------------------------
function ALDevice:createSource()
  return src.create()
end

function ALDevice:finalize()
end

return ffi.metatype('ALDevice', ALDeviceMT)
