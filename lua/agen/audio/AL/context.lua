local ffi = require('ffi')
local lib = require('agen.audio.AL.lib')

ffi.cdef([[
typedef char ALCboolean;
typedef char ALCchar;
typedef char ALCbyte;
typedef unsigned char ALCubyte;
typedef short ALCshort;
typedef unsigned short ALCushort;
typedef int ALCint;
typedef unsigned int ALCuint;
typedef int ALCsizei;
typedef float ALCfloat;
typedef double ALCdouble;
typedef void ALCvoid;

typedef enum _ALCenum {
  ALC_NO_ERROR        = 0x0000,

  ALC_DEFAULT_DEVICE_SPECIFIER = 0x1004,
  ALC_DEVICE_SPECIFIER         = 0x1005,
  ALC_EXTENSIONS               = 0x1006,
  ALC_FREQUENCY           = 0x1007,
  ALC_REFRESH             = 0x1008,
  ALC_SYNC                = 0x1009,
  ALC_MONO_SOURCES        = 0x1010,
  ALC_STEREO_SOURCES      = 0x1011,

  ALC_INVALID_DEVICE  = 0xA001,
  ALC_INVALID_CONTEXT = 0xA002,
  ALC_INVALID_ENUM    = 0xA003,
  ALC_INVALID_VALUE   = 0xA004,
  ALC_OUT_OF_MEMORY   = 0xA005,
} ALCenum;

ALCcontext* alcCreateContext(ALCdevice *device, const ALCint* attrlist);
ALCboolean  alcMakeContextCurrent(ALCcontext* context);
void        alcProcessContext(ALCcontext* context);
void        alcSuspendContext(ALCcontext* context);
void        alcDestroyContext(ALCcontext* context);
ALCcontext* alcGetCurrentContext(void);
ALCdevice*  alcGetContextsDevice(ALCcontext* context);
]])

local ALContext = {}
local ALContextMT = {
  __index = ALContext,
  __newindex = function(self, k, v)
  end,
  __tostring = "AL context",
}

function ALContext.create(device, attribs)
  local self = lib.alcCreateContext(device, attribs)
  if not self then
    error(device:getErrorString())
  end
  return ffi.gc(self, function(ctx)
    if ctx then lib.alcDestroyContext(ctx) end
  end)
end

function ALContext.getCurrent()
  return lib.alcGetCurrentContext()
end

function ALContext:makeCurrent()
  local ret = lib.alcMakeContextCurrent(self)
  if ret == 0 then
    error("Failed to make context current")
  end
end

function ALContext:getDevice()
  return lib.alcGetContextsDevice(self)
end

function ALContext:suspend()
  lib.alcSuspendContext(self)
end

function ALContext:process()
  lib.alcProcessContext(self)
end

return ffi.metatype('ALCcontext', ALContextMT)
