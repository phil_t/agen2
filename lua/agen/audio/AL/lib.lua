local ffi = require('ffi')
local alLib = {
	OSX = '/System/Library/Frameworks/OpenAL.framework/OpenAL',
	Windows = 'openal32',
	Linux = 'openal',
}

ffi.cdef([[
typedef char ALboolean;
typedef char ALchar;
typedef char ALbyte;
typedef unsigned char ALubyte;
typedef short ALshort;
typedef unsigned short ALushort;
typedef int ALint;
typedef unsigned int ALuint;
typedef int ALsizei;
typedef float ALfloat;
typedef double ALdouble;
typedef void ALvoid;

typedef struct ALCdevice_struct ALCdevice;
typedef struct ALCcontext_struct ALCcontext;

typedef enum _ALenum {
  AL_NO_ERROR          = 0x0000,
  // buffer properties
  AL_FREQUENCY         = 0x2001,  // in Hz
  AL_BITS              = 0x2002,
  AL_CHANNELS          = 0x2003,
  AL_SIZE              = 0x2004,  // in bytes

  AL_INVALID_NAME      = 0xA001,
  AL_INVALID_ENUM      = 0xA002,
  AL_INVALID_VALUE     = 0xA003,
  AL_INVALID_OPERATION = 0xA004,
  AL_OUT_OF_MEMORY     = 0xA005,

  AL_VENDOR            = 0xB001,
  AL_VERSION           = 0xB002,
  AL_RENDERER          = 0xB003,

  AL_ENUM_UINT32       = 0xffffffff
} ALenum;

ALenum alGetError(void);
ALboolean alIsExtensionPresent( const ALchar* extname);
void* alGetProcAddress(const ALchar* fname);
ALenum alGetEnumValue(const ALchar* ename);

const ALchar* alGetString(ALenum param);
void alGetBooleanv(ALenum param, ALboolean* data);
void alGetIntegerv(ALenum param, ALint* data);
void alGetFloatv(ALenum param, ALfloat* data);
void alGetDoublev(ALenum param, ALdouble* data);
ALboolean alGetBoolean(ALenum param);
ALint alGetInteger(ALenum param);
ALfloat alGetFloat(ALenum param);
ALdouble alGetDouble(ALenum param);

void alEnable(ALenum capability);
void alDisable(ALenum capability); 
ALboolean alIsEnabled(ALenum capability); 
]])

local al = {}
al.ffi = ffi

function al.GetString(code)
  local var = al.alGetString(code)
  if var ~= nil then
    return ffi.string(var)
  end
  return "unknown"
end

local alLibMT = {
  __index = ffi.load(alLib[ffi.os]),
  __newindex = function(self, k, v)
    local sym = ffi.cast(v, self.alGetProcAddress('al' .. k))
		if sym then
			rawset(al, k, sym)
		else
			error("Undefined symbol [" .. v .. ' ' .. k .. "]")
		end
  end,
  __tostring = "Agen2 OpenAL LuaJIT binding",
}
setmetatable(al, alLibMT)

do
  agen.log.info('audio', "AL_VENDOR:%s",   al.GetString(al.AL_VENDOR))
  agen.log.info('audio', "AL_VERSION:%s",  al.GetString(al.AL_VERSION))
  agen.log.info('audio', "AL_RENDERER:%s", al.GetString(al.AL_RENDERER))
end

return al
