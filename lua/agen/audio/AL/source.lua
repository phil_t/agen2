local al  = require('agen.audio.AL.lib')
local ctx = require('agen.audio.AL.context')
local ffi = al.ffi

ffi.cdef([[
/**
 * SOURCE
 * Sources represent individual sound objects in 3D-space.
 * Sources take the PCM data provided in the specified Buffer,
 * apply Source-specific modifications, and then
 * submit them to be mixed according to spatial arrangement etc.
 * 
 * Properties include: -
 *
 * Gain                              AL_GAIN                 ALfloat
 * Min Gain                          AL_MIN_GAIN             ALfloat
 * Max Gain                          AL_MAX_GAIN             ALfloat
 * Position                          AL_POSITION             ALfloat[3]
 * Velocity                          AL_VELOCITY             ALfloat[3]
 * Direction                         AL_DIRECTION            ALfloat[3]
 * Head Relative Mode                AL_SOURCE_RELATIVE      ALint (AL_TRUE or AL_FALSE)
 * Reference Distance                AL_REFERENCE_DISTANCE   ALfloat
 * Max Distance                      AL_MAX_DISTANCE         ALfloat
 * RollOff Factor                    AL_ROLLOFF_FACTOR       ALfloat
 * Inner Angle                       AL_CONE_INNER_ANGLE     ALint or ALfloat
 * Outer Angle                       AL_CONE_OUTER_ANGLE     ALint or ALfloat
 * Cone Outer Gain                   AL_CONE_OUTER_GAIN      ALint or ALfloat
 * Pitch                             AL_PITCH                ALfloat
 * Looping                           AL_LOOPING              ALint (AL_TRUE or AL_FALSE)
 * MS Offset                         AL_MSEC_OFFSET          ALint or ALfloat
 * Byte Offset                       AL_BYTE_OFFSET          ALint or ALfloat
 * Sample Offset                     AL_SAMPLE_OFFSET        ALint or ALfloat
 * Attached Buffer                   AL_BUFFER               ALint
 * State (Query only)                AL_SOURCE_STATE         ALint
 * Buffers Queued (Query only)       AL_BUFFERS_QUEUED       ALint
 * Buffers Processed (Query only)    AL_BUFFERS_PROCESSED    ALint
 */

typedef struct _alSource {
  ALuint id[1];
} alSource;

/* Create Source objects */
typedef void (*PFNALGENSOURCESPROC)(ALsizei n, ALuint* sources); 
/* Delete Source objects */
typedef void (*PFNALDELETESOURCESPROC)(ALsizei n, const ALuint* sources);
/* Verify a handle is a valid Source */ 
typedef ALboolean (*PFNALISSOURCEPROC)(ALuint sid); 
/*
 * Set Source parameters
 */
typedef void (*PFNALSOURCEFPROC)(ALuint sid, ALenum param, ALfloat value); 
typedef void (*PFNALSOURCE3FPROC)(ALuint sid, ALenum param, ALfloat value1, ALfloat value2, ALfloat value3);
typedef void (*PFNALSOURCEFVPROC)(ALuint sid, ALenum param, const ALfloat* values); 
typedef void (*PFNALSOURCEIPROC)(ALuint sid, ALenum param, ALint value); 
typedef void (*PFNALSOURCE3IPROC)(ALuint sid, ALenum param, ALint value1, ALint value2, ALint value3);
typedef void (*PFNALSOURCEIVPROC)(ALuint sid, ALenum param, const ALint* values);
/*
 * Get Source parameters
 */
typedef void (*PFNALGETSOURCEFPROC)(ALuint sid, ALenum param, ALfloat* value);
typedef void (*PFNALGETSOURCE3FPROC)(ALuint sid, ALenum param, ALfloat* value1, ALfloat* value2, ALfloat* value3);
typedef void (*PFNALGETSOURCEFVPROC)(ALuint sid, ALenum param, ALfloat* values);
typedef void (*PFNALGETSOURCEIPROC)(ALuint sid, ALenum param, ALint* value);
typedef void (*PFNALGETSOURCE3IPROC)(ALuint sid, ALenum param, ALint* value1, ALint* value2, ALint* value3);
typedef void (*PFNALGETSOURCEIVPROC)(ALuint sid,  ALenum param, ALint* values);
/*
 * Source vector based playback calls
 */
/* Play, replay, or resume (if paused) a list of Sources */
typedef void (*PFNALSOURCEPLAYVPROC)(ALsizei ns, const ALuint *sids);
/* Stop a list of Sources */
typedef void (*PFNALSOURCESTOPVPROC)(ALsizei ns, const ALuint *sids);
/* Rewind a list of Sources */
typedef void (*PFNALSOURCEREWINDVPROC)(ALsizei ns, const ALuint *sids);
/* Pause a list of Sources */
typedef void (*PFNALSOURCEPAUSEVPROC)(ALsizei ns, const ALuint *sids);
/*
 * Source based playback calls
 */
/* Play, replay, or resume a Source */
typedef void (*PFNALSOURCEPLAYPROC)(ALuint sid);
/* Stop a Source */
typedef void (*PFNALSOURCESTOPPROC)(ALuint sid);
/* Rewind a Source (set playback postiton to beginning) */
typedef void (*PFNALSOURCEREWINDPROC)(ALuint sid);
/* Pause a Source */
typedef void (*PFNALSOURCEPAUSEPROC)(ALuint sid);
/*
 * Source Queuing 
 */
typedef void (*PFNALSOURCEQUEUEBUFFERSPROC)(ALuint sid, ALsizei numEntries, const ALuint *bids);
typedef void (*PFNALSOURCEUNQUEUEBUFFERSPROC)(ALuint sid, ALsizei numEntries, ALuint *bids);
]])

local ALSource = {}
local ALSourceMT = {
  __index = ALSource,
  __gc = function(self) 
    if al.IsSource(self.id[0]) then
      al.alDeleteSources(1, self.id) 
    end
  end,
  __tostring = function(self) 
    return string.format("openAL source <%d>", self.id[0]) 
  end,
  __eq = function(self, other)
    return self.id[0] == other.id[0]
  end,
}

local alSource = ffi.typeof('alSource')
local symtab = {
  GenSources = 'PFNALGENSOURCESPROC',
  DeleteSources = 'PFNALDELETESOURCESPROC',
  IsSource = 'PFNALISSOURCEPROC',
  Sourcef = 'PFNALSOURCEFPROC',
  Source3f = 'PFNALSOURCE3FPROC',
  Sourcefv = 'PFNALSOURCEFVPROC',
  Sourcei = 'PFNALSOURCEIPROC',
  Source3i = 'PFNALSOURCE3IPROC',
  Sourceiv = 'PFNALSOURCEIVPROC',
  GetSourcef = 'PFNALGETSOURCEFPROC',
  GetSource3f = 'PFNALGETSOURCE3FPROC',
  GetSourcefv = 'PFNALGETSOURCEFVPROC',
  GetSourcei = 'PFNALGETSOURCEIPROC',
  GetSource3i = 'PFNALGETSOURCE3IPROC',
  GetSourceiv = 'PFNALGETSOURCEIVPROC',
  SourcePlayv = 'PFNALSOURCEPLAYVPROC',
  SourceStopv = 'PFNALSOURCESTOPVPROC',
  SourceRewindv = 'PFNALSOURCEREWINDVPROC',
  SourcePausev  = 'PFNALSOURCEPAUSEVPROC',
  SourcePlay = 'PFNALSOURCEPLAYPROC',
  SourceStop = 'PFNALSOURCESTOPPROC',
  SourceRewind = 'PFNALSOURCEREWINDPROC',
  SourcePause = 'PFNALSOURCEPAUSEPROC',
  SourceQueueBuffers = 'PFNALSOURCEQUEUEBUFFERSPROC',
  SourceUnqueueBuffers = 'PFNALSOURCEUNQUEUEBUFFERSPROC',
}
--------------------------------------------------------------
--
-- One-time init, needs valid context.
--
--------------------------------------------------------------
function ALSource.setup()
  assert(ctx.getCurrent(), 'Cannot setup Audio Source interface: no valid context')
  for k, v in pairs(symtab) do
    al[k] = v
  end
end

function ALSource.create(opts)
  local self = alSource()
  al.alGenSources(1, self.id)
  return self
end

return ffi.metatype('alSource', ALSourceMT)
