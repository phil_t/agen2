local string = require('string')
local sdl = require('sdl2.lib')
local al  = require('agen.audio.AL.lib')
local ctx = require('agen.audio.AL.context')
local ffi = al.ffi

ffi.cdef([[
static const uint16_t FORMAT_MONO8    = 0x1100;
static const uint16_t FORMAT_MONO16   = 0x1101;
static const uint16_t FORMAT_STEREO8  = 0x1102;
static const uint16_t FORMAT_STEREO16 = 0x1103;
/**
 * BUFFER
 * Buffer objects are storage space for sample data.
 * Buffers are referred to by Sources. One Buffer can be used
 * by multiple Sources.
 *
 * Properties include: -
 *
 * Frequency (Query only)    AL_FREQUENCY      ALint
 * Size (Query only)         AL_SIZE           ALint
 * Bits (Query only)         AL_BITS           ALint
 * Channels (Query only)     AL_CHANNELS       ALint
 */

typedef struct _alBuffer {
  ALuint id[1];
} alBuffer;

/* Create Buffer objects */
typedef void (*PFNALGENBUFFERSPROC)(ALsizei n, ALuint* buffers);
/* Delete Buffer objects */
typedef void (*PFNALDELETEBUFFERSPROC)(ALsizei n, const ALuint* buffers);
/* Verify a handle is a valid Buffer */
typedef ALboolean (*PFNALISBUFFERPROC)(ALuint bid);
/* Specify the data to be copied into a buffer */
typedef void (*PFNALBUFFERDATAPROC)(ALuint bid, ALenum format, const ALvoid* data, ALsizei size, ALsizei freq);
/*
 * Set Buffer parameters
 */
typedef void (*PFNALBUFFERFPROC)(ALuint bid, ALenum param, ALfloat value);
typedef void (*PFNALBUFFER3FPROC)(ALuint bid, ALenum param, ALfloat value1, ALfloat value2, ALfloat value3);
typedef void (*PFNALBUFFERFVPROC)(ALuint bid, ALenum param, const ALfloat* values);
typedef void (*PFNALBUFFERIPROC)(ALuint bid, ALenum param, ALint value);
typedef void (*PFNALBUFFER3IPROC)(ALuint bid, ALenum param, ALint value1, ALint value2, ALint value3);
typedef void (*PFNALBUFFERIVPROC)(ALuint bid, ALenum param, const ALint* values);
/*
 * Get Buffer parameters
 */
typedef void (*PFNALGETBUFFERFPROC)(ALuint bid, ALenum param, ALfloat* value);
typedef void (*PFNALGETBUFFER3FPROC)(ALuint bid, ALenum param, ALfloat* value1, ALfloat* value2, ALfloat* value3);
typedef void (*PFNALGETBUFFERFVPROC)(ALuint bid, ALenum param, ALfloat* values);
typedef void (*PFNALGETBUFFERIPROC)(ALuint bid, ALenum param, ALint* value);
typedef void (*PFNALGETBUFFER3IPROC)(ALuint bid, ALenum param, ALint* value1, ALint* value2, ALint* value3);
typedef void (*PFNALGETBUFFERIVPROC)(ALuint bid, ALenum param, ALint* values);
]])

local ALBuffer = {}
local ALBufferMT = {
  __index = ALBuffer,
  __newindex = function(buf, k, v)
    error("cannot modify buffer object")
  end,
  __gc = function(buf)
    if al.IsBuffer(buf.id[0]) then
      al.DeleteBuffers(1, buf.id)
    end
  end,
  __len = function(buf)
    local size = ffi.new('ALint[1]')
    al.GetBufferi(buf.id[0], al.AL_SIZE, size)
    return size[0]
  end,
  __tostring = function(buf)
    return string.format("openAL buffer <%d>", buf.id[0])
  end,
  __eq = function(buf, other)
    return buf.id[0] == other.id[0]
  end,
}

local alBuffer = ffi.typeof('alBuffer')
local symtab = {
  GenBuffers    = 'PFNALGENBUFFERSPROC',
  DeleteBuffers = 'PFNALDELETEBUFFERSPROC',
  IsBuffer      = 'PFNALISBUFFERPROC',
  BufferData    = 'PFNALBUFFERDATAPROC',
  Bufferf       = 'PFNALBUFFERFPROC',
  Buffer3f      = 'PFNALBUFFER3FPROC',
  Bufferfv      = 'PFNALBUFFERFVPROC',
  Bufferi       = 'PFNALBUFFERIPROC',
  Buffer3i      = 'PFNALBUFFER3IPROC',
  Bufferiv      = 'PFNALBUFFERIVPROC',
  GetBufferf    = 'PFNALGETBUFFERFPROC',
  GetBuffer3f   = 'PFNALGETBUFFER3FPROC',
  GetBufferfv   = 'PFNALGETBUFFERFVPROC',
  GetBufferi    = 'PFNALGETBUFFERIPROC',
  GetBuffer3i   = 'PFNALGETBUFFER3IPROC',
  GetBufferiv   = 'PFNALGETBUFFERIVPROC',
}
--------------------------------------------------------------
--
-- One-time init, needs valid context.
--
--------------------------------------------------------------
function ALBuffer.setup()
  assert(ctx.getCurrent(), 'Cannot setup Audio Buffer interface: no valid context')
  for k, v in pairs(symtab) do
    al[k] = v
  end
end

function ALBuffer.create(opts)
  local self = alBuffer()
  local err = al.alGetError()
  al.GenBuffers(1, self.id)
  err = al.alGetError()
  if err ~= 0 then
    error(string.format("Failed to create AL buffer: %s", al.GetString(err)))
  end

  if opts then
    if opts.data then
      self:update(opts)
    else
      -- TODO: set individual props
    end
  end
  return self
end

function ALBuffer:getID()
  return self.id[0]
end

function ALBuffer:getSize()
  -- call __len
  return #self
end

function ALBuffer:update(opts)
  assert(opts.format,   "Missing required option format")
  assert(opts.channels, "Missing required option channels")
  assert(opts.data,     "Missing required option data")
  assert(opts.len and opts.len > 0,    
                        "Missing or invalid required option len")
  assert(opts.freq,     "Missing required options freq")

  al.BufferData(self:getID(),
                self:SDLFormat2ALFormat(opts.channels, opts.format),
                opts.data, opts.len, opts.freq)
  local err = al.alGetError()
  if err ~= 0 then
    error(string.format("Failed to update AL buffer %d: %s",
      self:getID(),
      al.GetString(err)))
  end
end

ALBuffer.mapSDLFormat2ALFormat = {
  [tonumber(sdl.AUDIO_U8)]     = "FORMAT_%s8",
  [tonumber(sdl.AUDIO_S16LSB)] = "FORMAT_%s16",
}
--- vanilla openAL supports unsigned 8 bit mono or stereo
-- and signed 16 bit mono or stereo little endian pcm data
function ALBuffer:SDLFormat2ALFormat(channels, sdlFormat)
  if channels > 2 then
    -- TODO: downsample to stereo
    error("downsampling to stereo not implemented")
  end

  local key = ALBuffer.mapSDLFormat2ALFormat[sdlFormat]
  if key == nil then
    error("Unsupported format [" .. sdlFormat .. "]");
  end

  if channels == 2 then
    return al[string.format(key, 'STEREO')];
  elseif channels == 1 then
    return al[string.format(key, 'MONO')];
  else
    error("Invalid channels value [" .. channels .. "]")
  end
end

return ffi.metatype('alBuffer', ALBufferMT)
