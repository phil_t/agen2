--------------------------------------------------------------
--
-- Determine what version of XAudio2 is available on the host.
-- Note: DX SDK installs all versions.
--
--------------------------------------------------------------
local ffi = require('ffi')
do
  --------------------------------------------------------------
  -- XAudio2 version 2.9 ships as part of Windows 10,
  -- XAUDIO2_9.DLL, alongside XAudio2.8 to support
  -- older applications, and does not require redistribution.
  --------------------------------------------------------------
  local success, dll = pcall(ffi.load, 'XAudio2_9')
  if success then
    return require('agen.audio.XA2.XA2_9')(dll)
  end
  --------------------------------------------------------------
  -- XAudio2 version 2.8 ships today as a  system component in
  -- Windows 8, XAUDIO2_8.DLL. It is available inbox and does
  -- not require redistribution with an app.
  --------------------------------------------------------------
  success, dll = pcall(ffi.load, 'XAudio2_8')
  if success then
    return require('agen.audio.XA2.XA2_8')(dll)
  end
  --------------------------------------------------------------
  -- XAudio 2.7 and earlier (Windows 7)
  -- All previous versions of XAudio2 for use in apps have been
  -- provided as redistributable DLLs in the DirectX SDK.
  --------------------------------------------------------------
  success, dll = pcall(ffi.load, 'XAudio2_7')
  if success then
    return require('agen.audio.XA2.XA2_7')(dll)
  end

  error("XAudio2 dll not found")
end
