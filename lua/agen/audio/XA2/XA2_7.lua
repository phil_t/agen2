local bit    = require('bit')
local string = require('string')

local lib   = require('agen.audio.XA2.lib')
local ole32 = require('agen.system.win32.ole32')

local iconv = require('sdl2.iconv')
local ffi = lib.ffi

ffi.cdef([[
typedef enum _XAUDIO2_DEVICE_ROLE {
  NotDefaultDevice            = 0x00000000,
  DefaultConsoleDevice        = 0x00000001,
  DefaultMultimediaDevice     = 0x00000002,
  DefaultCommunicationsDevice = 0x00000004,
  DefaultGameDevice           = 0x00000008,
  GlobalDefaultDevice         = 0x0000000f,
  InvalidDeviceRole           = ~GlobalDefaultDevice,
} XAUDIO2_DEVICE_ROLE;

typedef struct _XAUDIO2_DEVICE_DETAILS {
  wchar_t DeviceID[256];              // String identifier for the audio device.
  wchar_t DisplayName[256];           // Friendly name suitable for display to a human.
  XAUDIO2_DEVICE_ROLE Role;           // Roles that the device should be used for.
  WAVEFORMATEXTENSIBLE OutputFormat;  // The device's native PCM audio output format.
} XAUDIO2_DEVICE_DETAILS;

struct _IXAudio2MasteringVoiceVtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2MasteringVoice* this, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2MasteringVoice* this);
  ULONG (__stdcall *Release)(IXAudio2MasteringVoice* this);
  void (__stdcall *GetVoiceDetails)(IXAudio2MasteringVoice* self, XAUDIO2_VOICE_DETAILS* pVoiceDetails);
  HRESULT (__stdcall *SetOutputVoices)(IXAudio2MasteringVoice* self, const XAUDIO2_VOICE_SENDS* pSendList);
  HRESULT (__stdcall *SetEffectChain)(IXAudio2MasteringVoice* self, const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *EnableEffect)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  HRESULT (__stdcall *DisableEffect)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  void (__stdcall *GetEffectState)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, BOOL* pEnabled);
  HRESULT (__stdcall *SetEffectParameters)(IXAudio2MasteringVoice* self, UINT32 EffectIndex,
                                  const void* pParameters,
                                  UINT32 ParametersByteSize,
                                  UINT32 OperationSet);
  HRESULT (__stdcall *GetEffectParameters)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, void* pParameters, UINT32 ParametersByteSize);
  HRESULT (__stdcall *SetFilterParameters)(IXAudio2MasteringVoice* self, const XAUDIO2_FILTER_PARAMETERS* pParameters, UINT32 OperationSet);
  void (__stdcall *GetFilterParameters)(IXAudio2MasteringVoice* self, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetOutputFilterParameters)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                                        const XAUDIO2_FILTER_PARAMETERS* pParameters,
                                        UINT32 OperationSet);
  void (__stdcall *GetOutputFilterParameters)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetVolume)(IXAudio2MasteringVoice* self, float Volume, UINT32 OperationSet);
  void (__stdcall *GetVolume)(IXAudio2MasteringVoice* self, float* pVolume);
  HRESULT (__stdcall *SetChannelVolumes)(IXAudio2MasteringVoice* self, UINT32 Channels, const float* pVolumes, UINT32 OperationSet);
  void (__stdcall *GetChannelVolumes)(IXAudio2MasteringVoice* self, UINT32 Channels, float* pVolumes);
  HRESULT (__stdcall *SetOutputMatrix)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                              UINT32 SourceChannels, UINT32 DestinationChannels,
                              const float* pLevelMatrix,
                              UINT32 OperationSet);
  void (__stdcall *GetOutputMatrix)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                                     UINT32 SourceChannels, UINT32 DestinationChannels,
                                     float* pLevelMatrix);
  void (__stdcall *DestroyVoice)(IXAudio2MasteringVoice* self);
};

struct _IXAudio2Vtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2* this, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2* this);
  ULONG (__stdcall *Release)(IXAudio2* this);
  HRESULT (__stdcall *GetDeviceCount)(IXAudio2* this, UINT32* pCount);
  HRESULT (__stdcall *GetDeviceDetails)(IXAudio2* this, UINT32 Index, XAUDIO2_DEVICE_DETAILS* pDeviceDetails);
  HRESULT (__stdcall *Initialize)(IXAudio2* this, UINT32 Flags, XAUDIO2_PROCESSOR XAudio2Processor);
  HRESULT (__stdcall *RegisterForCallbacks)(IXAudio2EngineCallback* pCallback);
  void (__stdcall *UnregisterForCallbacks)(IXAudio2EngineCallback* pCallback);
  HRESULT (__stdcall *CreateSourceVoice)(IXAudio2* this, IXAudio2Voice** ppSourceVoice,
    const WAVEFORMATEX* pSourceFormat,
    UINT32 Flags,
    float MaxFrequencyRatio,
    IXAudio2VoiceCallback* pCallback,
    const XAUDIO2_VOICE_SENDS* pSendList,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *CreateSubmixVoice)(IXAudio2* this, IXAudio2Voice** ppSubmixVoice,
    UINT32 InputChannels, UINT32 InputSampleRate,
    UINT32 Flags, UINT32 ProcessingStage,
    const XAUDIO2_VOICE_SENDS* pSendList,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *CreateMasteringVoice)(IXAudio2* this, IXAudio2MasteringVoice** ppMasteringVoice,
    UINT32 InputChannels,
    UINT32 InputSampleRate,
    UINT32 Flags, 
    UINT32 DeviceIndex,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *StartEngine)(IXAudio2* this);
  void (__stdcall *StopEngine)(IXAudio2* this);
  HRESULT (__stdcall *CommitChanges)(IXAudio2* this, UINT32 OperationSet);
  void (__stdcall *GetPerformanceData)(IXAudio2* this, XAUDIO2_PERFORMANCE_DATA* pPerfData);
  void (__stdcall *SetDebugConfiguration)(IXAudio2* this, const XAUDIO2_DEBUG_CONFIGURATION* pDebugConfiguration, void* pReserved);
};
]])

local IXAudio2 = {}
setmetatable(IXAudio2, lib.IUnknownMT)

function IXAudio2.create(Flags, XAudio2Processor)
  local hres = ole32.CoInitializeEx(nil, ole32.MULTITHREADED)
  if hres ~= 0 then
    error(string.format("Failed to initialize COM [%s]", lib.GetErrorString(hres)))
  end
  
  local pXAudio2 = ffi.new('IXAudio2*[1]')
  local CLSID
  if bit.band(Flags, lib.DEBUG_ENGINE) == 0 then
    CLSID = ffi.new('GUID', {
      0x5a508685, 0xa254, 0x4fba,
      {0x9b, 0x82, 0x9a, 0x24, 0xb0, 0x03, 0x06, 0xaf}
    })
  else
    CLSID = ffi.new('GUID', {
      0xdb05ea35, 0x0329, 0x4d4b,
      {0xa5, 0x3a, 0x6d, 0xea, 0xd0, 0x3d, 0x38, 0x52}
    })
  end

  local IID_IXAudio2 = ffi.new('GUID', {
    0x8bcf1f58, 0x9fe7, 0x4583,
    {0x8a, 0xc6, 0xe2, 0xad, 0xc4, 0x65, 0xc8, 0xbb}
  })
  
  hres = ole32.CoCreateInstance(CLSID, nil, ole32.INPROC_SERVER, IID_IXAudio2, ffi.cast('void**', pXAudio2))
  if hres ~= 0 then
    error(string.format("Failed to create XA2 instance, dx9 redist probably not installed: [%s]", lib.GetErrorString(hres)))
  end
  
  local ret = ffi.gc(pXAudio2[0], function(obj)
    obj:release()
    ole32.CoUninitialize()
  end)
  
  hres = ret.lpVtbl.Initialize(ret, Flags, XAudio2Processor)
  if hres ~= 0 then
    error(string.format("Failed to initialize XA2 instance [%s]", lib.GetErrorString(hres)))
  end
  return ret
end

function IXAudio2:getDeviceByRole(role)
  local numDevices = ffi.new('UINT32[1]')
  local hres = self.lpVtbl.GetDeviceCount(self, numDevices)
  if hres ~= 0 then
    error(string.format("Failed to get number of audio devices [%s]", lib.GetErrorString(hres)))
  end
  
  local details = ffi.new('XAUDIO2_DEVICE_DETAILS')  
  for i = 1, tonumber(numDevices[0]) do
     hres = self.lpVtbl.GetDeviceDetails(self, i - 1, details)
     if hres == 0 and bit.band(tonumber(details.Role), role) == role then
      -- DisplayName is UTF16
      agen.log.info('audio', "Audio device: [%s]", iconv.UTF16_TO_UTF8(details.DisplayName))
      return i - 1
     end
  end
  error("Failed to find suitable audio device")
end

function IXAudio2:createMasteringVoice(id, channels, rate)
  local voice = ffi.new('IXAudio2MasteringVoice*[1]')
  local hres  = self.lpVtbl.CreateMasteringVoice(self, voice, channels, rate, 0, id, nil)
  if hres ~= 0 then
    error(string.format("Failed to create Xaudio2 master voice [%s]", lib.GetErrorString(hres)))
  end
  -- destroying the mastering voice crashes for some reason
  --return ffi.gc(voice[0], voice[0].destroy)
  return voice[0]
end

function IXAudio2:start()
  local hres = self.lpVtbl.StartEngine(self)
  if hres ~= 0 then
    error(string.format("Failed to start XAudio2 engine:[%s]", lib.GetErrorString(hres)))
  end
end

function IXAudio2:stop()
  self.lpVtbl.StopEngine(self)
end

local IXAudio2MT = {__index = IXAudio2}
ffi.metatype('IXAudio2', IXAudio2MT)
-----------------------------------------------------------------------------
--
-- device factory object
--
-----------------------------------------------------------------------------
local xa2 = {}

function xa2.createDevice(win, config)
  local flags = 0
  if config.debug then
    flags = bit.bor(flags, lib.DEBUG_ENGINE)
  end
  --
  -- init interface
  --
  local xaudio = IXAudio2.create(flags, lib.DEFAULT_PROCESSOR)
  --
  -- find default device
  --
  local id = xaudio:getDeviceByRole(lib.DefaultGameDevice)
  --
  -- create device
  --
  local XA2Device = require('agen.audio.XA2.device')
  return XA2Device.create(xaudio, id, win, config)
end
------------------------------------------------------------------------------------------
--
-- return function
--
------------------------------------------------------------------------------------------  
return function(dll)
  -- lookup unknown symbols in xaudio2 dll
  setmetatable(lib, {__index = dll})
  return xa2
end
