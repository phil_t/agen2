local win32 = require('agen.system.win32')
local ffi = win32.ffi
  
ffi.cdef([[
typedef enum XAUDIO2_WINDOWS_PROCESSOR_SPECIFIER {
  Processor1  = 0x00000001,
  // ...
  ANY_PROCESSOR = 0xffffffff,
  DEFAULT_PROCESSOR = 0x00000001,
} XAUDIO2_PROCESSOR;

typedef enum _XAUDIO2_FLAGS {
  DEBUG_ENGINE            = 0x00000001,        // Used in XAudio2Create on Windows only
  VOICE_NOPITCH           = 0x00000002,        // Used in IXAudio2::CreateSourceVoice
  VOICE_NOSRC             = 0x00000004,        // Used in IXAudio2::CreateSourceVoice
  VOICE_USEFILTER         = 0x00000008,        // Used in IXAudio2::CreateSource/SubmixVoice
  VOICE_MUSIC             = 0x00000010,        // Used in IXAudio2::CreateSourceVoice
  PLAY_TAILS              = 0x00000020,        // Used in IXAudio2SourceVoice::Stop
  END_OF_STREAM           = 0x00000040,        // Used in XAUDIO2_BUFFER.Flags
  SEND_USEFILTER          = 0x00000080,        // Used in XAUDIO2_SEND_DESCRIPTOR.Flags
} XAUDIO2_FLAGS;

// Values for the TraceMask and BreakMask bitmaps.  Only ERRORS and WARNINGS
// are valid in BreakMask.  WARNINGS implies ERRORS, DETAIL implies INFO, and
// FUNC_CALLS implies API_CALLS.  By default, TraceMask is ERRORS and WARNINGS
// and all the other settings are zero.
typedef enum _LOG_MASK {
  LOG_ERRORS     = 0x0001,   // For handled errors with serious effects.
  LOG_WARNINGS   = 0x0002,   // For handled errors that may be recoverable.
  LOG_INFO       = 0x0004,   // Informational chit-chat (e.g. state changes).
  LOG_DETAIL     = 0x0008,   // More detailed chit-chat.
  LOG_API_CALLS  = 0x0010,   // Public API function entries and exits.
  LOG_FUNC_CALLS = 0x0020,   // Internal function entries and exits.
  LOG_TIMING     = 0x0040,   // Delays detected and other timing data.
  LOG_LOCKS      = 0x0080,   // Usage of critical sections and mutexes.
  LOG_MEMORY     = 0x0100,   // Memory heap usage information.
  LOG_STREAMING  = 0x1000,   // Audio streaming information.
} LOG_MASK;

typedef struct _WAVEFORMATEX {
  WORD  wFormatTag;
  WORD  nChannels;
  DWORD nSamplesPerSec;
  DWORD nAvgBytesPerSec;
  WORD  nBlockAlign;
  WORD  wBitsPerSample;
  WORD  cbSize;
} WAVEFORMATEX;

typedef struct _WAVEFORMATEXTENSIBLE {
  WAVEFORMATEX Format;
  union {
    WORD wValidBitsPerSample;
    WORD wSamplesPerBlock;
    WORD wReserved;
  } Samples;
  DWORD        dwChannelMask;
  GUID         SubFormat;
} WAVEFORMATEXTENSIBLE;

typedef struct _XAUDIO2_VOICE_STATE {
  void   *pCurrentBufferContext;
  UINT32 BuffersQueued;
  UINT64 SamplesPlayed;
} XAUDIO2_VOICE_STATE;

typedef struct _XAUDIO2_BUFFER {
  UINT32     Flags;
  UINT32     AudioBytes;
  const int8_t* pAudioData;
  UINT32     PlayBegin;
  UINT32     PlayLength;
  UINT32     LoopBegin;
  UINT32     LoopLength;
  UINT32     LoopCount;
  void*      pContext;
} XAUDIO2_BUFFER;

typedef struct _XAUDIO2_BUFFER_WMA {
  const UINT32* pDecodedPacketCumulativeBytes;
  UINT32        PacketCount;
} XAUDIO2_BUFFER_WMA;

typedef struct _IXAudio2EngineCallback IXAudio2EngineCallback;
struct _IXAudio2EngineCallback {
  void (__stdcall *OnProcessingPassStart)(IXAudio2EngineCallback* this);
  void (__stdcall *OnProcessingPassEnd)(IXAudio2EngineCallback* this);
  void (__stdcall *OnCriticalError)(IXAudio2EngineCallback* this, HRESULT Error);
};

typedef struct _IXAudio2VoiceCallback IXAudio2VoiceCallback;
typedef struct _IXAudio2Voice IXAudio2Voice;

// Used in XAUDIO2_VOICE_SENDS below
typedef struct _XAUDIO2_SEND_DESCRIPTOR {
    UINT32 Flags;                       // Either 0 or XAUDIO2_SEND_USEFILTER.
    IXAudio2Voice* pOutputVoice;        // This send's destination voice.
} XAUDIO2_SEND_DESCRIPTOR;

typedef struct _XAUDIO2_VOICE_SENDS {
    UINT32 SendCount;                   // Number of sends from this voice.
    XAUDIO2_SEND_DESCRIPTOR* pSends;    // Array of SendCount send descriptors.
} XAUDIO2_VOICE_SENDS;

// Used in XAUDIO2_EFFECT_CHAIN below
typedef struct XAUDIO2_EFFECT_DESCRIPTOR {
    IUnknown* pEffect;                  // Pointer to the effect object's IUnknown interface.
    BOOL InitialState;                  // TRUE if the effect should begin in the enabled state.
    UINT32 OutputChannels;              // How many output channels the effect should produce.
} XAUDIO2_EFFECT_DESCRIPTOR;

// Used in the voice creation functions and in IXAudio2Voice::SetEffectChain
typedef struct XAUDIO2_EFFECT_CHAIN {
    UINT32 EffectCount;                 // Number of effects in this voice's effect chain.
    XAUDIO2_EFFECT_DESCRIPTOR* pEffectDescriptors; // Array of effect descriptors.
} XAUDIO2_EFFECT_CHAIN;

typedef struct _XAUDIO2_PERFORMANCE_DATA {
    // CPU usage information
    UINT64 AudioCyclesSinceLastQuery;   // CPU cycles spent on audio processing since the
                                        //  last call to StartEngine or GetPerformanceData.
    UINT64 TotalCyclesSinceLastQuery;   // Total CPU cycles elapsed since the last call
                                        //  (only counts the CPU XAudio2 is running on).
    UINT32 MinimumCyclesPerQuantum;     // Fewest CPU cycles spent processing any one
                                        //  audio quantum since the last call.
    UINT32 MaximumCyclesPerQuantum;     // Most CPU cycles spent processing any one
                                        //  audio quantum since the last call.

    // Memory usage information
    UINT32 MemoryUsageInBytes;          // Total heap space currently in use.

    // Audio latency and glitching information
    UINT32 CurrentLatencyInSamples;     // Minimum delay from when a sample is read from a
                                        //  source buffer to when it reaches the speakers.
    UINT32 GlitchesSinceEngineStarted;  // Audio dropouts since the engine was started.

    // Data about XAudio2's current workload
    UINT32 ActiveSourceVoiceCount;      // Source voices currently playing.
    UINT32 TotalSourceVoiceCount;       // Source voices currently existing.
    UINT32 ActiveSubmixVoiceCount;      // Submix voices currently playing/existing.

    UINT32 ActiveResamplerCount;        // Resample xAPOs currently active.
    UINT32 ActiveMatrixMixCount;        // MatrixMix xAPOs currently active.

    // Usage of the hardware XMA decoder (Xbox 360 only)
    UINT32 ActiveXmaSourceVoices;       // Number of source voices decoding XMA data.
    UINT32 ActiveXmaStreams;            // A voice can use more than one XMA stream.
} XAUDIO2_PERFORMANCE_DATA;

// Used in IXAudio2::SetDebugConfiguration
typedef struct _XAUDIO2_DEBUG_CONFIGURATION {
    UINT32 TraceMask;                   // Bitmap of enabled debug message types.
    UINT32 BreakMask;                   // Message types that will break into the debugger.
    BOOL LogThreadID;                   // Whether to log the thread ID with each message.
    BOOL LogFileline;                   // Whether to log the source file and line number.
    BOOL LogFunctionName;               // Whether to log the function name.
    BOOL LogTiming;                     // Whether to log message timestamps.
} XAUDIO2_DEBUG_CONFIGURATION;

typedef enum _AUDIO_STREAM_CATEGORY { 
  AudioCategory_Other                   = 0,
  AudioCategory_ForegroundOnlyMedia,
  AudioCategory_BackgroundCapableMedia,
  AudioCategory_Communications,
  AudioCategory_Alerts,
  AudioCategory_SoundEffects,
  AudioCategory_GameEffects,
  AudioCategory_GameMedia,
  AudioCategory_GameChat,
  AudioCategory_Speech,
  AudioCategory_Movie,
  AudioCategory_Media
} AUDIO_STREAM_CATEGORY;

typedef struct _IXAudio2Vtbl IXAudio2Vtbl;
typedef struct _IXAudio2 {
  const IXAudio2Vtbl* lpVtbl;
} IXAudio2;

// Returned by IXAudio2Voice::GetVoiceDetails
typedef struct _XAUDIO2_VOICE_DETAILS {
    UINT32 CreationFlags;               // Flags the voice was created with.
    UINT32 InputChannels;               // Channels in the voice's input audio.
    UINT32 InputSampleRate;             // Sample rate of the voice's input audio.
} XAUDIO2_VOICE_DETAILS;

// Used in XAUDIO2_FILTER_PARAMETERS below
typedef enum _tagXAUDIO2_FILTER_TYPE {
    LowPassFilter,                      // Attenuates frequencies above the cutoff frequency.
    BandPassFilter,                     // Attenuates frequencies outside a given range.
    HighPassFilter,                     // Attenuates frequencies below the cutoff frequency.
    NotchFilter                         // Attenuates frequencies inside a given range.
} XAUDIO2_FILTER_TYPE;

// Used in IXAudio2Voice::Set/GetFilterParameters and Set/GetOutputFilterParameters
typedef struct _XAUDIO2_FILTER_PARAMETERS {
    XAUDIO2_FILTER_TYPE Type;           // Low-pass, band-pass or high-pass.
    float Frequency;                    // Radian frequency (2 * sin(pi*CutoffFrequency/SampleRate));
                                        //  must be >= 0 and <= XAUDIO2_MAX_FILTER_FREQUENCY
                                        //  (giving a maximum CutoffFrequency of SampleRate/6).
    float OneOverQ;                     // Reciprocal of the filter's quality factor Q;
                                        //  must be > 0 and <= XAUDIO2_MAX_FILTER_ONEOVERQ.
} XAUDIO2_FILTER_PARAMETERS;

typedef struct _IXAudio2VoiceVtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2Voice* self, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2Voice* self);
  ULONG (__stdcall *Release)(IXAudio2Voice* self);
  /* NAME: IXAudio2Voice::GetVoiceDetails
   * DESCRIPTION: Returns the basic characteristics of this voice.
   *
   * ARGUMENTS:
   *  pVoiceDetails - Returns the voice's details.
   */
  void (__stdcall *GetVoiceDetails)(IXAudio2Voice* self, XAUDIO2_VOICE_DETAILS* pVoiceDetails);
  /* NAME: IXAudio2Voice::SetOutputVoices
  // DESCRIPTION: Replaces the set of submix/mastering voices that receive
  //              this voice's output.
  //
  // ARGUMENTS:
  //  pSendList - Optional list of voices this voice should send audio to.
  */
  HRESULT (__stdcall *SetOutputVoices)(IXAudio2Voice* self, const XAUDIO2_VOICE_SENDS* pSendList);
  /* NAME: IXAudio2Voice::SetEffectChain
  // DESCRIPTION: Replaces this voice's current effect chain with a new one.
  //
  // ARGUMENTS:
  //  pEffectChain - Structure describing the new effect chain to be used.
  */
  HRESULT (__stdcall *SetEffectChain)(IXAudio2Voice* self, const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  /* NAME: IXAudio2Voice::EnableEffect
  // DESCRIPTION: Enables an effect in this voice's effect chain.
  //
  // ARGUMENTS:
  //  EffectIndex - Index of an effect within this voice's effect chain.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *EnableEffect)(IXAudio2Voice* self, UINT32 EffectIndex, UINT32 OperationSet);
  /* NAME: IXAudio2Voice::DisableEffect
  // DESCRIPTION: Disables an effect in this voice's effect chain.
  //
  // ARGUMENTS:
  //  EffectIndex - Index of an effect within this voice's effect chain.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *DisableEffect)(IXAudio2Voice* self, UINT32 EffectIndex, UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetEffectState
  // DESCRIPTION: Returns the running state of an effect.
  //
  // ARGUMENTS:
  //  EffectIndex - Index of an effect within this voice's effect chain.
  //  pEnabled - Returns the enabled/disabled state of the given effect.
  */
  void (__stdcall *GetEffectState)(IXAudio2Voice* self, UINT32 EffectIndex, BOOL* pEnabled);
  /* NAME: IXAudio2Voice::SetEffectParameters
  // DESCRIPTION: Sets effect-specific parameters.
  //
  // REMARKS: Unlike IXAPOParameters::SetParameters, this method may
  //          be called from any thread.  XAudio2 implements
  //          appropriate synchronization to copy the parameters to the
  //          realtime audio processing thread.
  //
  // ARGUMENTS:
  //  EffectIndex - Index of an effect within this voice's effect chain.
  //  pParameters - Pointer to an effect-specific parameters block.
  //  ParametersByteSize - Size of the pParameters array  in bytes.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetEffectParameters)(IXAudio2Voice* self, UINT32 EffectIndex,
                                  const void* pParameters,
                                  UINT32 ParametersByteSize,
                                  UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetEffectParameters
  // DESCRIPTION: Obtains the current effect-specific parameters.
  //
  // ARGUMENTS:
  //  EffectIndex - Index of an effect within this voice's effect chain.
  //  pParameters - Returns the current values of the effect-specific parameters.
  //  ParametersByteSize - Size of the pParameters array in bytes.
  */
  HRESULT (__stdcall *GetEffectParameters)(IXAudio2Voice* self, UINT32 EffectIndex, void* pParameters, UINT32 ParametersByteSize);
  /* NAME: IXAudio2Voice::SetFilterParameters
  // DESCRIPTION: Sets this voice's filter parameters.
  //
  // ARGUMENTS:
  //  pParameters - Pointer to the filter's parameter structure.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetFilterParameters)(IXAudio2Voice* self, const XAUDIO2_FILTER_PARAMETERS* pParameters, UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetFilterParameters
  // DESCRIPTION: Returns this voice's current filter parameters.
  //
  // ARGUMENTS:
  //  pParameters - Returns the filter parameters.
  */
  void (__stdcall *GetFilterParameters)(IXAudio2Voice* self, XAUDIO2_FILTER_PARAMETERS* pParameters);
  /* NAME: IXAudio2Voice::SetOutputFilterParameters
  // DESCRIPTION: Sets the filter parameters on one of this voice's sends.
  //
  // ARGUMENTS:
  //  pDestinationVoice - Destination voice of the send whose filter parameters will be set.
  //  pParameters - Pointer to the filter's parameter structure.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetOutputFilterParameters)(IXAudio2Voice* self, IXAudio2Voice* pDestinationVoice,
                                        const XAUDIO2_FILTER_PARAMETERS* pParameters,
                                        UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetOutputFilterParameters
  // DESCRIPTION: Returns the filter parameters from one of this voice's sends.
  //
  // ARGUMENTS:
  //  pDestinationVoice - Destination voice of the send whose filter parameters will be read.
  //  pParameters - Returns the filter parameters.
  */
  void (__stdcall *GetOutputFilterParameters)(IXAudio2Voice* self, IXAudio2Voice* pDestinationVoice, XAUDIO2_FILTER_PARAMETERS* pParameters);
  /* NAME: IXAudio2Voice::SetVolume
  // DESCRIPTION: Sets this voice's overall volume level.
  //
  // ARGUMENTS:
  //  Volume - New overall volume level to be used, as an amplitude factor.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetVolume) (IXAudio2Voice* self, float Volume, UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetVolume
  // DESCRIPTION: Obtains this voice's current overall volume level.
  //
  // ARGUMENTS:
  //  pVolume: Returns the voice's current overall volume level.
  */
  void (__stdcall *GetVolume)(IXAudio2Voice* self, float* pVolume);
  /* NAME: IXAudio2Voice::SetChannelVolumes
  // DESCRIPTION: Sets this voice's per-channel volume levels.
  //
  // ARGUMENTS:
  //  Channels - Used to confirm the voice's channel count.
  //  pVolumes - Array of per-channel volume levels to be used.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetChannelVolumes)(IXAudio2Voice* self, UINT32 Channels, const float* pVolumes, UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetChannelVolumes
  // DESCRIPTION: Returns this voice's current per-channel volume levels.
  //
  // ARGUMENTS:
  //  Channels - Used to confirm the voice's channel count.
  //  pVolumes - Returns an array of the current per-channel volume levels.
  */
  void (__stdcall *GetChannelVolumes)(IXAudio2Voice* self, UINT32 Channels, float* pVolumes);
  /* NAME: IXAudio2Voice::SetOutputMatrix
  // DESCRIPTION: Sets the volume levels used to mix from each channel of this
  //              voice's output audio to each channel of a given destination
  //              voice's input audio.
  //
  // ARGUMENTS:
  //  pDestinationVoice - The destination voice whose mix matrix to change.
  //  SourceChannels - Used to confirm this voice's output channel count
  //   (the number of channels produced by the last effect in the chain).
  //  DestinationChannels - Confirms the destination voice's input channels.
  //  pLevelMatrix - Array of [SourceChannels * DestinationChannels] send
  //   levels.  The level used to send from source channel S to destination
  //   channel D should be in pLevelMatrix[S + SourceChannels * D].
  //  OperationSet - Used to identify this call as part of a deferred batch.
  */
  HRESULT (__stdcall *SetOutputMatrix)(IXAudio2Voice* self, IXAudio2Voice* pDestinationVoice,
                              UINT32 SourceChannels, UINT32 DestinationChannels,
                              const float* pLevelMatrix,
                              UINT32 OperationSet);
  /* NAME: IXAudio2Voice::GetOutputMatrix
  // DESCRIPTION: Obtains the volume levels used to send each channel of this
  //              voice's output audio to each channel of a given destination
  //              voice's input audio.
  //
  // ARGUMENTS:
  //  pDestinationVoice - The destination voice whose mix matrix to obtain.
  //  SourceChannels - Used to confirm this voice's output channel count
  //   (the number of channels produced by the last effect in the chain).
  //  DestinationChannels - Confirms the destination voice's input channels.
  //  pLevelMatrix - Array of send levels, as above.
  */
  void (__stdcall *GetOutputMatrix)(IXAudio2Voice* self, IXAudio2Voice* pDestinationVoice,
                                     UINT32 SourceChannels, UINT32 DestinationChannels,
                                     float* pLevelMatrix);
  /* NAME: IXAudio2Voice::DestroyVoice
  // DESCRIPTION: Destroys this voice, stopping it if necessary and removing
  //              it from the XAudio2 graph.
  */
  void (__stdcall *DestroyVoice)(IXAudio2Voice* self);
} IXAudio2VoiceVtbl;

struct IXAudio2Voice {
  const IXAudio2VoiceVtbl* lpVtbl;
};

typedef struct _IXAudio2MasteringVoiceVtbl IXAudio2MasteringVoiceVtbl;
typedef struct _IXAudio2MasteringVoice {
  const IXAudio2MasteringVoiceVtbl* lpVtbl;
} IXAudio2MasteringVoice;
/**************************************************************************************************
 *
 * IXAudio2SourceVoice
 *
 **************************************************************************************************/
typedef struct _IXAudio2SourceVoice IXAudio2SourceVoice;
typedef struct _IXAudio2SourceVoiceVtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2SourceVoice* self, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2SourceVoice* self);
  ULONG (__stdcall *Release)(IXAudio2SourceVoice* self);
  void (__stdcall *GetVoiceDetails)(IXAudio2SourceVoice* self, XAUDIO2_VOICE_DETAILS* pVoiceDetails);
  HRESULT (__stdcall *SetOutputVoices)(IXAudio2SourceVoice* self, const XAUDIO2_VOICE_SENDS* pSendList);
  HRESULT (__stdcall *SetEffectChain)(IXAudio2SourceVoice* self, const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *EnableEffect)(IXAudio2SourceVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  HRESULT (__stdcall *DisableEffect)(IXAudio2SourceVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  void (__stdcall *GetEffectState)(IXAudio2SourceVoice* self, UINT32 EffectIndex, BOOL* pEnabled);
  HRESULT (__stdcall *SetEffectParameters)(IXAudio2SourceVoice* self, UINT32 EffectIndex,
                                  const void* pParameters,
                                  UINT32 ParametersByteSize,
                                  UINT32 OperationSet);
  HRESULT (__stdcall *GetEffectParameters)(IXAudio2SourceVoice* self, UINT32 EffectIndex, void* pParameters, UINT32 ParametersByteSize);
  HRESULT (__stdcall *SetFilterParameters)(IXAudio2SourceVoice* self, const XAUDIO2_FILTER_PARAMETERS* pParameters, UINT32 OperationSet);
  void (__stdcall *GetFilterParameters)(IXAudio2SourceVoice* self, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetOutputFilterParameters)(IXAudio2SourceVoice* self, IXAudio2SourceVoice* pDestinationVoice,
                                        const XAUDIO2_FILTER_PARAMETERS* pParameters,
                                        UINT32 OperationSet);
  void (__stdcall *GetOutputFilterParameters)(IXAudio2SourceVoice* self, IXAudio2SourceVoice* pDestinationVoice, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetVolume) (IXAudio2SourceVoice* self, float Volume, UINT32 OperationSet);
  void (__stdcall *GetVolume)(IXAudio2SourceVoice* self, float* pVolume);
  HRESULT (__stdcall *SetChannelVolumes)(IXAudio2SourceVoice* self, UINT32 Channels, const float* pVolumes, UINT32 OperationSet);
  void (__stdcall *GetChannelVolumes)(IXAudio2SourceVoice* self, UINT32 Channels, float* pVolumes);
  HRESULT (__stdcall *SetOutputMatrix)(IXAudio2SourceVoice* self, IXAudio2SourceVoice* pDestinationVoice,
                              UINT32 SourceChannels, UINT32 DestinationChannels,
                              const float* pLevelMatrix,
                              UINT32 OperationSet);
  void (__stdcall *GetOutputMatrix)(IXAudio2SourceVoice* self, IXAudio2SourceVoice* pDestinationVoice,
                                     UINT32 SourceChannels, UINT32 DestinationChannels,
                                     float* pLevelMatrix);
  void (__stdcall *DestroyVoice)(IXAudio2SourceVoice* self);
  // NAME: IXAudio2SourceVoice::Start
  // DESCRIPTION: Makes this voice start consuming and processing audio.
  //
  // ARGUMENTS:
  //  Flags - Flags controlling how the voice should be started.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  //
  HRESULT (__stdcall *Start)(IXAudio2SourceVoice* self, UINT32 Flags, UINT32 OperationSet);
  // NAME: IXAudio2SourceVoice::Stop
  // DESCRIPTION: Makes this voice stop consuming audio.
  //
  // ARGUMENTS:
  //  Flags - Flags controlling how the voice should be stopped.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  //
  HRESULT (__stdcall *Stop)(IXAudio2SourceVoice* self, UINT32 Flags, UINT32 OperationSet);
  // NAME: IXAudio2SourceVoice::SubmitSourceBuffer
  // DESCRIPTION: Adds a new audio buffer to this voice's input queue.
  //
  // ARGUMENTS:
  //  pBuffer - Pointer to the buffer structure to be queued.
  //  pBufferWMA - Additional structure used only when submitting XWMA data.
  //
  HRESULT (__stdcall *SubmitSourceBuffer)(IXAudio2SourceVoice* self, const XAUDIO2_BUFFER* pBuffer, const XAUDIO2_BUFFER_WMA* pBufferWMA);
  // NAME: IXAudio2SourceVoice::FlushSourceBuffers
  // DESCRIPTION: Removes all pending audio buffers from this voice's queue.
  //
  HRESULT (__stdcall *FlushSourceBuffers)(IXAudio2SourceVoice* self);
  // NAME: IXAudio2SourceVoice::Discontinuity
  // DESCRIPTION: Notifies the voice of an intentional break in the stream of
  //              audio buffers (e.g. the end of a sound), to prevent XAudio2
  //              from interpreting an empty buffer queue as a glitch.
  //
  HRESULT (__stdcall *Discontinuity)(IXAudio2SourceVoice* self);
  // NAME: IXAudio2SourceVoice::ExitLoop
  // DESCRIPTION: Breaks out of the current loop when its end is reached.
  //
  // ARGUMENTS:
  //  OperationSet - Used to identify this call as part of a deferred batch.
  //
  HRESULT (__stdcall *ExitLoop)(IXAudio2SourceVoice* self, UINT32 OperationSet);
  // NAME: IXAudio2SourceVoice::GetState
  // DESCRIPTION: Returns the number of buffers currently queued on this voice,
  //              the pContext value associated with the currently processing
  //              buffer (if any), and other voice state information.
  //
  // ARGUMENTS:
  //  pVoiceState - Returns the state information.
  //
  void (__stdcall *GetState)(IXAudio2SourceVoice* self, XAUDIO2_VOICE_STATE* pVoiceState);
  // NAME: IXAudio2SourceVoice::SetFrequencyRatio
  // DESCRIPTION: Sets this voice's frequency adjustment, i.e. its pitch.
  //
  // ARGUMENTS:
  //  Ratio - Frequency change, expressed as source frequency / target frequency.
  //  OperationSet - Used to identify this call as part of a deferred batch.
  //
  HRESULT (__stdcall *SetFrequencyRatio)(IXAudio2SourceVoice* self, float Ratio, UINT32 OperationSet);
  // NAME: IXAudio2SourceVoice::GetFrequencyRatio
  // DESCRIPTION: Returns this voice's current frequency adjustment ratio.
  //
  // ARGUMENTS:
  //  pRatio - Returns the frequency adjustment.
  //
  void (__stdcall *GetFrequencyRatio)(IXAudio2SourceVoice* self, float* pRatio);
  // NAME: IXAudio2SourceVoice::SetSourceSampleRate
  // DESCRIPTION: Reconfigures this voice to treat its source data as being
  //              at a different sample rate than the original one specified
  //              in CreateSourceVoice's pSourceFormat argument.
  //
  // ARGUMENTS:
  //  UINT32 - The intended sample rate of further submitted source data.
  //
  HRESULT (__stdcall *SetSourceSampleRate)(IXAudio2SourceVoice* self, UINT32 NewSourceSampleRate);
} IXAudio2SourceVoiceVtbl;

struct _IXAudio2SourceVoice {
  const IXAudio2SourceVoiceVtbl* lpVtbl;
};
]])

local lib = {}
lib.ffi = ffi
-----------------------------------------------------------------------------------
--
-- shortcut to win32
--
-----------------------------------------------------------------------------------
lib.IUnknownMT     = win32.IUnknownMT
lib.GetErrorString = win32.GetErrorString
lib.FinalizerCOM   = win32.finalizerCOM
-----------------------------------------------------------------------------------
--
-- IXAudio2Voice
--
-----------------------------------------------------------------------------------
local IXAudio2Voice = {}
setmetatable(IXAudio2Voice, win32.IUnknownMT)

function IXAudio2Voice:getDetails()
  local ret = ffi.new('XAUDIO2_VOICE_DETAILS')
  self.lpVtbl.GetVoiceDetails(self, ret)
  return ret
end

function IXAudio2Voice:getVolume()
  local vol = ffi.new('float[1]')
  self.lpVtbl.GetVolume(self, vol)
  return vol[0]
end

function IXAudio2Voice:setVolume(vol, id)
  local hres = self.lpVtbl.SetVolume(self, vol, id)
  if hres ~= 0 then
    agen.log.warn('audio', "Failed to set player [%s]'s volume", tostring(self))
  end
end
----------------------------------------------------------------------------------------------
-- If any other voice is currently sending audio to this voice, the method fails.
-- DestroyVoice waits for the audio processing thread to be idle, so it can take 
-- a little while (typically no more than a couple of milliseconds). This is necessary 
-- to guarantee that the voice will no longer make any callbacks or read any audio data, 
-- so the application can safely free up these resources as soon as the call returns.
-- To avoid title thread interruptions from a blocking DestroyVoice call, the application 
-- can destroy voices on a separate non-critical thread, or the application can use voice 
-- pooling strategies to reuse voices rather than destroying them. Note that voices can only 
-- be reused with audio that has the same data format and the same number of channels the 
-- voice was created with. A voice can play audio data with different sample rates than that 
-- of the voice by calling IXAudio2SourceVoice::SetFrequencyRatio with an appropriate ratio 
-- parameter.
-- It is invalid to call DestroyVoice from within a callback (that is, IXAudio2EngineCallback 
-- or IXAudio2VoiceCallback).
----------------------------------------------------------------------------------------------
function IXAudio2Voice:destroy()
  self.lpVtbl.DestroyVoice(self)
end

local IXAudio2VoiceMT = {__index = IXAudio2Voice}
ffi.metatype('IXAudio2Voice', IXAudio2VoiceMT)
--------------------------------------------------------------------------------------
--
-- IXAudio2MasteringVoice
--
--------------------------------------------------------------------------------------
local IXAudio2MasteringVoice = {}
setmetatable(IXAudio2MasteringVoice, IXAudio2VoiceMT)

function IXAudio2MasteringVoice:getVoiceDetails()
  local details = ffi.new('XAUDIO2_VOICE_DETAILS[1]')
  self.lpVtbl.GetVoiceDetails(self, details)
  return details[0]
end

local IXAudio2MasteringVoiceMT = {__index = IXAudio2MasteringVoice}
ffi.metatype('IXAudio2MasteringVoice', IXAudio2MasteringVoiceMT)
----------------------------------------------------------------------------------------
--
-- IXAudio2SourceVoice
--
----------------------------------------------------------------------------------------
local IXAudio2SourceVoice = {}
setmetatable(IXAudio2SourceVoice, IXAudio2VoiceMT)

function IXAudio2SourceVoice:start(flags, set)
  local hres = self.lpVtbl.Start(self, flags, set)
  if hres ~= 0 then
    error(string.format("Failed to start source voice: %s", lib.GetErrorString(hres)))
  end
end

function IXAudio2SourceVoice:stop(flags, set)
  local hres = self.lpVtbl.Stop(self, flags, set)
  if hres ~= 0 then
    error(string.format("Failed to stop source voice: %s", lib.GetErrorString(hres)))
  end
end

function IXAudio2SourceVoice:setSampleRate(rate)
  local hres = self.lpVtbl.SetSampleRate(self, rate)
  if hres ~= 0 then
    error(string.format("Failed to resample source voice: %s", lib.GetErrorString(hres)))
  end
end

local IXAudio2SourceVoiceMT = {__index = IXAudio2SourceVoice}
ffi.metatype('IXAudio2SourceVoice', IXAudio2SourceVoiceMT)

return lib
