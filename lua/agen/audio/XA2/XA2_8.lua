--------------------------------------------------------------------------
--
-- XAudio 2.8 (ships with Win8(.1) version-specific virtual function tables
--
--------------------------------------------------------------------------
local string = require('string')
local bit    = require('bit')

local lib = require('agen.audio.XA2.lib')
local ffi = lib.ffi

ffi.cdef([[
struct _IXAudio2MasteringVoiceVtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2MasteringVoice* this, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2MasteringVoice* this);
  ULONG (__stdcall *Release)(IXAudio2MasteringVoice* this);
  void (__stdcall *GetVoiceDetails)(IXAudio2MasteringVoice* self, XAUDIO2_VOICE_DETAILS* pVoiceDetails);
  HRESULT (__stdcall *SetOutputVoices)(IXAudio2MasteringVoice* self, const XAUDIO2_VOICE_SENDS* pSendList);
  HRESULT (__stdcall *SetEffectChain)(IXAudio2MasteringVoice* self, const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *EnableEffect)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  HRESULT (__stdcall *DisableEffect)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, UINT32 OperationSet);
  void (__stdcall *GetEffectState)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, BOOL* pEnabled);
  HRESULT (__stdcall *SetEffectParameters)(IXAudio2MasteringVoice* self, UINT32 EffectIndex,
                                  const void* pParameters,
                                  UINT32 ParametersByteSize,
                                  UINT32 OperationSet);
  HRESULT (__stdcall *GetEffectParameters)(IXAudio2MasteringVoice* self, UINT32 EffectIndex, void* pParameters, UINT32 ParametersByteSize);
  HRESULT (__stdcall *SetFilterParameters)(IXAudio2MasteringVoice* self, const XAUDIO2_FILTER_PARAMETERS* pParameters, UINT32 OperationSet);
  void (__stdcall *GetFilterParameters)(IXAudio2MasteringVoice* self, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetOutputFilterParameters)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                                        const XAUDIO2_FILTER_PARAMETERS* pParameters,
                                        UINT32 OperationSet);
  void (__stdcall *GetOutputFilterParameters)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice, XAUDIO2_FILTER_PARAMETERS* pParameters);
  HRESULT (__stdcall *SetVolume) (IXAudio2MasteringVoice* self, float Volume, UINT32 OperationSet);
  void (__stdcall *GetVolume)(IXAudio2MasteringVoice* self, float* pVolume);
  HRESULT (__stdcall *SetChannelVolumes)(IXAudio2MasteringVoice* self, UINT32 Channels, const float* pVolumes, UINT32 OperationSet);
  void (__stdcall *GetChannelVolumes)(IXAudio2MasteringVoice* self, UINT32 Channels, float* pVolumes);
  HRESULT (__stdcall *SetOutputMatrix)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                              UINT32 SourceChannels, UINT32 DestinationChannels,
                              const float* pLevelMatrix,
                              UINT32 OperationSet);
  void (__stdcall *GetOutputMatrix)(IXAudio2MasteringVoice* self, IXAudio2Voice* pDestinationVoice,
                                     UINT32 SourceChannels, UINT32 DestinationChannels,
                                     float* pLevelMatrix);
  void (__stdcall *DestroyVoice)(IXAudio2MasteringVoice* self);
  void (__stdcall *GetChannelMask)(IXAudio2MasteringVoice* This, DWORD *pChannelMask);
};

struct _IXAudio2Vtbl {
  HRESULT (__stdcall *QueryInterface)(IXAudio2* this, REFIID riid, void** ppvInterface);
  ULONG (__stdcall *AddRef)(IXAudio2* this);
  ULONG (__stdcall *Release)(IXAudio2* this);

  HRESULT (__stdcall *RegisterForCallbacks)(IXAudio2EngineCallback* pCallback);
  void (__stdcall *UnregisterForCallbacks)(IXAudio2EngineCallback* pCallback);
  HRESULT (__stdcall *CreateSourceVoice)(IXAudio2* this, IXAudio2Voice** ppSourceVoice,
    const WAVEFORMATEX* pSourceFormat,
    UINT32 Flags,
    float MaxFrequencyRatio,
    IXAudio2VoiceCallback* pCallback,
    const XAUDIO2_VOICE_SENDS* pSendList,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *CreateSubmixVoice)(IXAudio2* this, IXAudio2Voice** ppSubmixVoice,
    UINT32 InputChannels, UINT32 InputSampleRate,
    UINT32 Flags, UINT32 ProcessingStage,
    const XAUDIO2_VOICE_SENDS* pSendList,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain);
  HRESULT (__stdcall *CreateMasteringVoice)(IXAudio2* this, IXAudio2MasteringVoice** ppMasteringVoice,
    UINT32 InputChannels,
    UINT32 InputSampleRate,
    UINT32 Flags, 
    const wchar_t* DeviceID,
    const XAUDIO2_EFFECT_CHAIN* pEffectChain,
    AUDIO_STREAM_CATEGORY streamCategory);
  HRESULT (__stdcall *StartEngine)(IXAudio2* this);
  void (__stdcall *StopEngine)(IXAudio2* this);
  HRESULT (__stdcall *CommitChanges)(IXAudio2* this, UINT32 OperationSet);
  void (__stdcall *GetPerformanceData)(IXAudio2* this, XAUDIO2_PERFORMANCE_DATA* pPerfData);
  void (__stdcall *SetDebugConfiguration)(IXAudio2* this, const XAUDIO2_DEBUG_CONFIGURATION* pDebugConfiguration, void* pReserved);
};
// inline in xaudio2 2.7
HRESULT XAudio2Create(
  IXAudio2**        ppXAudio2,
  UINT32            Flags,
  XAUDIO2_PROCESSOR XAudio2Processor
);
]])

local IXAudio2 = {}
setmetatable(IXAudio2, lib.IUnknownMT)

function IXAudio2.create(XAudio2Processor)
  local pXAudio2 = ffi.new('IXAudio2*[1]')
  local hres = lib.XAudio2Create(pXAudio2, 0, XAudio2Processor)
  if hres ~= 0 then
    error(string.format("Failed to create XA2 instance [%s]", lib.GetErrorString(hres)))
  end
  return ffi.gc(pXAudio2[0], lib.FinalizerCOM)
end

function IXAudio2:createMasteringVoice(id, channels, rate)
  local voice = ffi.new('IXAudio2MasteringVoice*[1]')
  local hres  = self.lpVtbl.CreateMasteringVoice(self, voice, channels, rate, 0, id, nil, lib.AudioCategory_GameEffects)
  if hres ~= 0 then
    error(string.format("Failed to create Xaudio2 master voice [%s]", lib.GetErrorString(hres)))
  end
  return ffi.gc(voice[0], voice[0].destroy)
end

function IXAudio2:start()
  local hres = self.lpVtbl.StartEngine(self)
  if hres ~= 0 then
    error(string.format("Failed to start XAudio2 engine:[%s]", lib.GetErrorString(hres)))
  end
end

function IXAudio2:stop()
  self.lpVtbl.StopEngine(self)
end

local IXAudio2MT = {__index = IXAudio2}
ffi.metatype('IXAudio2', IXAudio2MT)
------------------------------------------------------------------------------------------------
--
-- Device factory
--
------------------------------------------------------------------------------------------------
local xa2 = {}

function xa2.createDevice(win, config)
  -- The DirectX SDK versions of XAUDIO2 supported a flag XAUDIO2_DEBUG_ENGINE to select 
  -- between the release and 'checked' version. This flag is not supported or defined in 
  -- the Windows 8 version of XAUDIO2. 
  local xaudio = IXAudio2.create(lib.DEFAULT_PROCESSOR)
  --
  -- create device
  --
  local XA2Device = require('agen.audio.XA2.device')
  return XA2Device.create(xaudio, nil, win, config)
end
------------------------------------------------------------------------------------------
--
-- return function
--
------------------------------------------------------------------------------------------  
return function(dll)
  -- lookup unknown symbols in xaudio2 dll
  setmetatable(lib, {__index = dll})
  return xa2
end
