local string = require('string')
local bit = require('bit')
local xa2 = require('agen.audio.XA2.lib')
local ffi = xa2.ffi

ffi.cdef([[
typedef struct _XA2Device {
  IXAudio2* owner;
  IXAudio2MasteringVoice* masterVoice;
} XA2Device;
]])

local mapChannels = {
  mono     = 1,
  stereo   = 2,
  twoone   = 3,
  quadro   = 5,
  fiveone  = 6,
  sevenone = 8,
}

local XA2Device = {}
local XA2DeviceMT = {
  __index = XA2Device,
  __newindex = function(self, k, v)
	error("Cannot modify XA2 device object")
  end,
  __gc = function(self)
    self.masterVoice:release()
    self.masterVoice = nil
    self.owner:release()
    self.owner = nil
  end,
}

function XA2Device.create(xaudio, id, window, config)  
  local voice = xaudio:createMasteringVoice(id, mapChannels[config.channels], config.rate)
  local self = ffi.new('XA2Device')
  self.owner = xaudio
  self.owner:ref()
  self.masterVoice = voice
  self.masterVoice:ref()

  if config.debug then
    local debugConfig = ffi.new('XAUDIO2_DEBUG_CONFIGURATION')
    debugConfig.TraceMask = bit.bor(xa2.LOG_API_CALLS, xa2.LOG_DETAIL, xa2.LOG_STREAMING)
    debugConfig.LogThreadID = 1
    self.owner.lpVtbl.SetDebugConfiguration(self.owner, debugConfig, nil)
  end
  self.owner:start()
  return self
end

function XA2Device:createBuffer(opts)
  return nil
end

function XA2Device:createSource(opts)
  return nil
end
--- NOTE: called before self is collected
function XA2Device:finalize()
  self.owner:stop()
end

return ffi.metatype('XA2Device', XA2DeviceMT)
