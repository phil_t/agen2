--------------------------------------------------------------------------
--
-- XAudio 2.9 (ships with Win10) version-specific virtual function tables
--
--------------------------------------------------------------------------
return function(dll)
  return require('agen.audio.XA2.XA2_8')(dll)
end
