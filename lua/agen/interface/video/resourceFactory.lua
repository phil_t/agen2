local ffi = require('ffi')
local sdl = require('sdl2.lib')
--
-- interfaces are just plain tables that get added to a metatable
--
local IResFactory = {}

-- TODO: should be a C ptr instead of lua tbl to pass around threads
local _renderStates = {}

function IResFactory:included(klass)
  agen.log.debug('application', 'object implements IResourceFactory interface')
end

function IResFactory:_finalize_resources()
  agen.log.debug('application', 'IResourceFactory: finalize')
  -- released before the device
  _renderStates = nil
end
-------------------------------------------------------------------------------------------
--
-- buffer resource
--
-------------------------------------------------------------------------------------------
local bufferUsage = {
  constant = 'GPU buffer initialized once at instantiation',
  static   = 'GPU buffer for vertex data. Can be copied into or from',
  dynamic  = 'HOST buffer that can be updated per frame',
  staging  = 'HOST buffer to copy static buffers from or into',
}

function IResFactory:createBuffer(bufferDesc)
  assert(bufferDesc  ~= nil and
    type(bufferDesc) == 'table',        'Invalid buffer description, lua table expected')
  assert(bufferDesc.size,               "Missing required option `size'")
  assert(bufferDesc.size > 0,           "Invalid value for option `size'")
  assert(bufferDesc.purpose,            "Missing required option `purpose'")
  assert(bufferDesc.usage,              "Missing required option `usage'")
  assert(bufferUsage[bufferDesc.usage], "Invalid value [" .. bufferDesc.usage .. "] for option `usage'")
  assert(bufferDesc.ctype,              "Missing required option `ctype'")

  if bufferDesc.usage == 'constant' then
    assert(bufferDesc.data, "Cannot create constant buffer with no data")
  end

  local buffer = self._driver:createBuffer(bufferDesc)

  if bufferDesc.data then
    if bufferDesc.usage == 'dynamic' or bufferDesc.usage == 'staging' then
      local ptr = buffer:map('write')
      ffi.memcpy(ptr, bufferDesc.data, bufferDesc.size)
      buffer:unmap()
    else
      -- copy data to buffer
      buffer:update(0, bufferDesc.data, bufferDesc.size)
    end
  end

  return buffer
end
-------------------------------------------------------------------------------------------
--
-- texture resource
--
-------------------------------------------------------------------------------------------
function IResFactory:createTexture(textureDesc)
  assert(textureDesc  ~= nil and
    type(textureDesc) == 'table', 'invalid texture description, lua table expected, got nil')
  assert(textureDesc.format, "Missing required parameter format")
  assert(textureDesc.width,  "Missing required parameter width")
  assert(textureDesc.height, "Missing required parameter height")
  assert(textureDesc.pitch,  "pitch not specified")

  local tex = self._driver:createTexture(textureDesc)
  --
  -- transfer data from system RAM to texture
  --
  if textureDesc.data ~= nil then
    tex:updateRect(0, nil, textureDesc.data, nil, textureDesc.pitch)
  end

  return tex
end
--- Create a texture from SDL_surface optionally specifying the texture format
-- @param sdl surface
-- @return texture object
function IResFactory:createTextureFromSurface(surface)
  assert(surface ~= nil and
         ffi.istype('SDL_Surface', surface), "invalid argument #1, SDL surface expected, got nil")
  agen.log.debug('video', "Source surface size [%dx%d], bytes per pixel [%d], sdl format [0x%x]",
    surface.w,
    surface.h,
    surface.format.BytesPerPixel, -- how data is laid out in memory, not all bytes may be in use
    surface.format.format)
  --
  -- find suitable texture format for the current surface format
  --
  local destFormat = self._driver:getBestTextureFormat(surface.format.format)
  if destFormat ~= surface.format.format then
    --
    -- conversion required
    --
    surface = surface:createFromSurface(destFormat)
    if surface == nil or surface.format.format ~= destFormat then
      error(string.format("Failed to convert SDL surface format: %s", sdl.GetError()))
    end
  end
  --
  -- Make the surface data host-visible.
  -- no-op for system RAM surfaces, we're not using video RAM surfaces (textures)
  --
  surface:lock()
  -- unlock even on exception
  local ok, ret = pcall(self.createTexture, self, {
    width         = surface.w,
    height        = surface.h,
    format        = surface.format.format,
    sdlformat     = surface.format,
    purpose       = 'color',
    bytesPerPixel = surface.format.BytesPerPixel,
    pitch         = surface.pitch,
    samples       = 1,  -- TODO: multisampling support
    levels        = 1,  -- SDL_Surface has only one level
    data          = surface.pixels,
  })
  surface:unlock()
  if not ok then
    error('createTexture failed:' .. ret)
  end
  return ret
end
---------------------------------------------------------------------------------------------
--
-- sampler resource
-- TODO: create in renderPass from lua table
--
---------------------------------------------------------------------------------------------
function IResFactory:createSampler(samplerDesc)
  assert(samplerDesc == nil or  -- nill ok
    type(samplerDesc) == 'table', 'invalid sampler description, lua table expected, got nil')
  return self._driver:createSampler(samplerDesc)
end
---------------------------------------------------------------------------------------------
--
-- renderState resource
--
---------------------------------------------------------------------------------------------
local topology = {
  triangles = 1,
  strip     = 2,
  lines     = 3,
  linestrip = 4,
  points    = 5,
}

function IResFactory:createRenderState(stateDesc)
  assert(stateDesc ~= nil and
    type(stateDesc) == 'table', 'invalid render state description, lua table expected')
  assert(stateDesc.label  and
         stateDesc.label ~= '',      'invalid render state description, required option <label> not set')
  assert(_renderStates[stateDesc.label] == nil, 'render state with that name already exists')
  assert(stateDesc.shaders and
    type(stateDesc.shaders) == 'table', 'invalid shaders option, non-empty lua table expected')
  assert(stateDesc.vertexBuffersAttr and
    type(stateDesc.vertexBuffersAttr) == 'table', 'invalid vertex layout descriptor, lua table expected')
  assert(stateDesc.topology, 'invalid render state description, required option <topology> not set')
  _renderStates[stateDesc.label] = self._driver:createRenderState(stateDesc)
  return _renderStates[stateDesc.label]
end

-- TODO: render states manager, should be a C ptr to share between threads
function IResFactory:getRenderState(name)
  return _renderStates[name]
end
---------------------------------------------------------------------------------------------
--
-- renderTarget resource
--
---------------------------------------------------------------------------------------------
function IResFactory:createRenderTarget(renderTargetDesc)
  assert(renderTargetDesc  ~= nil and
    type(renderTargetDesc) == 'table', 'invalid render target description, lua table expected, got nil')
  return self._driver:createRenderTarget(renderTargetDesc)
end
---------------------------------------------------------------------------------------------
--
-- fence resource
--
---------------------------------------------------------------------------------------------
function IResFactory:createFence(fenceDesc)
  assert(fenceDesc  ~= nil and
    type(fenceDesc) == 'table', 'invalid fence description, lua table expected, got nil')
  return self._driver:createFence(fenceDesc)
end

return IResFactory
