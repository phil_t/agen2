local bit    = require('bit')
local math   = require('math')
local string = require('string')
--
-- Texture trait
-- A texture must be cdata so it can be accessed from any thread.
--
local ITexture = {}
--- Get texture width and height for a specific mipmap level, default is 0
-- @param level - texture level to query (optional)
-- @returns width, height
function ITexture:getSize(level)
  local w, h = self:_getSize()
  if level == nil or level == 0 then
    return w, h
  end
  return math.max(bit.rshift(w, level), 1),
         math.max(bit.rshift(h, level), 1)
end
--- Update part of texture on a specific level
-- @param level - texture level to update
-- @param destRect - destinaton SDL_Rect or nil for whole dest texture
-- @param data
-- @param dataLen length
-- @param dataPitch pitch
-- @returns 0 on success, throws error on failure
function ITexture:updateRect(level, destRect, data, dataLen, dataPitch)
  assert(level >= 0, "Texture level invalid")
  assert(data, "Cannot update texture region with no data")

  local err, msg = self:_updateRect(level, destRect, data, dataLen, dataPitch)
  if err ~= 0 then
    error(string.format("failed to update texture: [%s]", msg))
  end
  return 0
end

return ITexture
