--------------------------------------------------------------------------------
--
-- "polling" renderer suitable when agen "drives" the main loop as opposed to
-- "event-based" renderer where the OS graphics loop send us an event to "draw"
-- (usually on vblank).
--
--------------------------------------------------------------------------------
local IRenderer = {}

function IRenderer:included(klass)
  agen.log.debug('application', 'object implements IRenderer interface')
end

function IRenderer:getDefaultRenderTarget()
  return self._driver:getDefaultRenderTarget()
end

function IRenderer:renderFrame()
  agen.render(self)
  self._driver:present()
end

function IRenderer:reset(window)
  self._driver:reset(window)
end

function IRenderer:backBufferScale(window)
  self._driver:backBufferScale(window)
end

function IRenderer:backBufferResize(window)
  self._driver:backBufferResize(window)
end

return IRenderer
