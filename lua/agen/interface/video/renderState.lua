--------------------------------------------------------------------
--
-- renderState aka Pipeline interface
--
--------------------------------------------------------------------
local IRenderState = {}

function IRenderState:init()
  self.rpass = {}
  self:createRenderPass('default')
end
------------------------------------------------------
--
-- accessors
--
------------------------------------------------------
function IRenderState:getDevice()
  return self.owner
end

function IRenderState:getModelViewProj()
  local xf = self.xform
  return xf.mvp:mvp(xf.model, xf.view, xf.projection)
end

function IRenderState:getVertexDeclaration()
  return self.vdecl
end

function IRenderState:getProgram()
  return self.program
end
------------------------------------------------------
--
-- render pass factory
--
------------------------------------------------------
function IRenderState:createRenderPass(name)
  assert(name, "Render Pass name cannot be nil")
  assert(self.rpass[name] == nil, "Render Pass already exists")
  local rpass = self:_createRenderPass()
  self.rpass[name] = rpass
  return rpass
end

function IRenderState:getRenderPass(name)
  assert(name, 'render Pass name cannot be nil')
  local rpass = self.rpass[name]
  assert(rpass, "non-existing Render Pass")
  return rpass
end
------------------------------------------------------
--
-- dynamic state
--
------------------------------------------------------
function IRenderState:setWireFrame(flag)
  self:_setWireFrame(flag)
end

function IRenderState:setScissorTest(flag)
  self:_setScissorTest(flag)
end

function IRenderState:setDepthFunc(name)
  assert(name, "Invalid depth func, cannot be nil")
  self:_setDepthFunc(name)
end

function IRenderState:setBlendFunc(name)
  assert(name, "Invalid depth func, cannot be nil")
  self:_setBlendFunc(name)
end
-- 'off', 'front', 'back'
function IRenderState:setCullFunc(name)
  assert(name, "Invalid cull func, cannot be nil")
  self:_setCullFunc(name)
end

return IRenderState
