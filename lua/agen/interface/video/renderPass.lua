local IRenderPass = {}

function IRenderPass:init(opts)
    assert(opts and type(opts) == 'table', "invalid argument: lua table expected")
    assert(opts.vertexBuffers, "No vertex buffers specified")
    self:_init(opts)
    return self
end

function IRenderPass:getUniformBuffer(name)
    assert(name and type(name) == 'string' and name ~= '', "invalid uniform buffer name")
    return self:_getUniformBuffer(name)
end

function IRenderPass:setTexture(slotIndex, samplerIndex, textureIndex)
    assert(slotIndex and slotIndex >= 0, 'invalid argument #1, slot index must be int [0, ... )')
    assert(samplerIndex and samplerIndex >= 0, 'invalid argument #2, sampler index must be int [0, ... )')
    assert(textureIndex and textureIndex >= 0, 'invalid argument #3, texture index must be int [0, ... )')
    return self:_setTexture(slotIndex, samplerIndex, textureIndex)
end
---------------------------------------------------------------------------
--
-- drawing API
--
---------------------------------------------------------------------------
function IRenderPass:begin()
    self:_begin()
end

function IRenderPass:clear()
    self:_clear()
end

function IRenderPass:clearDepth()
    self:_clearDepth()
end

function IRenderPass:clearDepthStencil()
    self:_clearDepthStencil()
end

--- Issue drawing command from buffer starting at index.
-- @param <offset> offset in vertices from the beginning of vertex buffer
-- @param <size> length of data in vertices
function IRenderPass:drawPrimitive(offset, size)
    assert(offset and offset >= 0, "Invalid offset value")
    assert(size and size > 0, "Invalid size value")
    self:_drawPrimitive(offset, size)
end
--- Issue drawing command from indexed buffer
-- @param <offset> offset in vertices from the beginning of vertex buffer
-- @param <size> number of indices to render
-- @param <ioffset> offset in indices from the beginning of index buffer
function IRenderPass:drawIndexedPrimitive(offset, size, ioffset)
    assert(offset and offset >= 0, "Invalid offset value")
    assert(size and size > 0, "Invalid size value")
    assert(ioffset and ioffset >= 0, "Invalid ioffset value")
    self:_drawIndexedPrimitive(offset, size, ioffset)
end
--- Issue instanced geometry drawing command
-- @param <offset> offset in vertices from the beginning of the buffer
-- @param <size> number of elements to render
-- @param <ioffset> offset in indices from the beginning of index buffer
-- @param <count> number of instances to render
function IRenderPass:drawInstancedPrimitive(offset, size, ioffset, count)
    assert(offset and offset >= 0, "Invalid offset value")
    assert(size and size > 0, "Invalid size value")
    assert(ioffset and ioffset >= 0, "Invalid ioffset value")
    assert(count and count > 0, "Invalid count value")
    self:_drawInstancedPrimitive(offset, size, ioffset, count)
end
--- Drawing commands from a buffer
function IRenderPass:drawPrimitiveIndirect(bufferIndex, offset)
    assert(bufferIndex and bufferIndex >= 0, "Invalid bufferIndex value")
    assert(offset and offset >= 0, "Invalid offset value")
    self:_drawPrimitiveIndirect(bufferIndex, offset)
end

function IRenderPass:drawIndexedPrimitiveIndirect(bufferIndex, offset)
    assert(bufferIndex and bufferIndex >= 0, "Invalid bufferIndex value")
    assert(offset and offset >= 0, "Invalid offset value")
    self:_drawIndexedPrimitiveIndirect(bufferIndex, offset)
end
--- submit commands list to device for execution
function IRenderPass:submit()
    self:_submit()
end

return IRenderPass
