local ffi = require('ffi')
----------------------------------------------------------------
--
-- Buffer trait
-- A buffer must be cdata so it can be accessed from any thread.
-- There are 2 types of buffers: accessible from the host or not 
-- accessible from the host. The former can be mapped into the
-- current process address space and modified directly and the
-- later can be copied from/to another host-accessible buffer
-- (aka staging buffer).
--
-----------------------------------------------------------------
local bNum = 0

local IBuffer = {}
--- Asynchronously update buffer
-- @param offset from the begining in bytes
-- @param data   new data pointer
-- @param size   data size in bytes
function IBuffer:update(offset, data, size)
  assert(offset ~= nil, "Missing argument #1, offset cannot be nil")
  assert(offset >= 0 and offset < #self, "Invalid argument #1, offset out of bounds")
  assert(data ~= nil, "Missing argument #2, data cannot be nil")
  assert(size ~= nil, "Missing argument #3, size cannot be nil")
  assert(size > 0 and offset + size <= #self, "Invalid argument #3, size out of bounds")

  bNum = bNum + 1
  --
  -- Buffer may not be host visible, update via staging buffer
  --
  local stageBuff = self:getDevice():createBuffer({
    name    = 'stage' .. bNum,
    purpose = 'data',
    usage   = 'staging',
    size    = size,
    ctype   = 'uint8_t',
  })

  local ptr = stageBuff:map('write')
  ffi.copy(ptr, data, size)
  stageBuff:unmap()
  --
  -- 3. upload to video buffer at offset <offset>
  --
  self:updateFromBuffer(offset, stageBuff)
end

function IBuffer:map(flags)
  assert(flags, 'Invalid argument #1, flags cannot be nil')
  return self:_map(flags)
end

function IBuffer:unmap()
  self:_unmap()
end

return IBuffer
