local ffi          = require('ffi')
local class        = require('middleclass')
local IDevice      = require('agen.interface.device')
local IResFactory  = require('agen.interface.video.resourceFactory')
local IRenderer    = require('agen.interface.video.renderer')
local term         = require('agen.util.terminal')
--------------------------------------------------------------------
--
-- Umbrella interface compositing together the
-- device, resource factory and renderer interfaces
--
--------------------------------------------------------------------
local IVideoDevice = class('IVideoDevice')
IVideoDevice:include(IDevice)
IVideoDevice:include(IResFactory)
IVideoDevice:include(IRenderer)

local _sdlWindow
--- initialize self._driver - called from inside middleclass' :new method
-- @param arg SDL_Window or ffi cptr
-- @param config table
-- @return nil
function IVideoDevice:initialize(arg, config)
  if arg == nil then
    error('missing required argument #1, SDL_Window or ffi cptr expected')
  elseif type(arg) == 'cdata' and ffi.istype('SDL_Window', arg) then
    --
    -- initialize from SDL window with driver
    --
    assert(config  ~= nil and
      type(config) == 'table', 'invalid argument #2, lua table expected, got nil')
    assert(config.driver,      'invalid argument #2, missing required key .driver')
    _sdlWindow = arg
    -- Load specific video backend
    local driver = require('agen.video.' .. config.driver)
    -- Create video device for the first time
    self._driver = driver.createDevice(arg, config)
  elseif type(arg) == 'cdata' then
    --
    -- initialize from C pointer (valid device ptr on another thread)
    --
    -- TODO: ffi.cast(arg, deviceMT)
    self._driver = arg
  else
    error('unexpected argument #1, cdata expected')
  end
end

function IVideoDevice:getWindow()
  return _sdlWindow
end

function IVideoDevice:init(config)
  term.init(self)
  self._driver:init(config)
end

function IVideoDevice:terminalWrite(msg)
  term.write(self, msg)
end

return IVideoDevice
