local IDevice = {}

function IDevice:getName()
  return self._driver:getName()
end

function IDevice:finalize()
  self._driver:finalize()
  collectgarbage('collect')
end

return IDevice
