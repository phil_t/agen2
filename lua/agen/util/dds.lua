-----------------------------------------------------------------
--
-- DirectDraw surface loader. Integers are little endian
--
-----------------------------------------------------------------
local ffi = require('ffi')
local bit = require('bit')
local string = require('string')
local math   = require('math')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _DDS_FLAGS {
  DDSD_CAPS	        = 0x00000001,
  DDSD_HEIGHT       = 0x00000002,
  DDSD_WIDTH	      = 0x00000004,
  DDSD_PITCH	      = 0x00000008,
  DDSD_PIXELFORMAT  = 0x00001000,
  DDSD_MIPMAPCOUNT  = 0x00020000,
  DDSD_LINEARSIZE   = 0x00080000,
  DDSD_DEPTH	      = 0x00800000,
  DDSD_UINT32       = 0xffffffff
} DDS_FLAGS;

typedef enum _DDS_CAPS {
  DDSCAPS_COMPLEX = 0x00000008,  /* Optional; must be used on any file that contains more than one surface (a mipmap, a cubic environment map, or mipmapped volume texture). */
  DDSCAPS_TEXTURE = 0x00001000, /* Required  0x1000 */
  DDSCAPS_MIPMAP  = 0x00400000, /* Optional; should be used for a mipmap.  0x400000 */
  DDSCAPS_UNIT32  = 0xffffffff
} DDS_CAPS;

typedef enum _DDS_CAPS2 {
  DDSCAPS2_CUBEMAP = 0x00000200,  /* Required for a cube map */
  DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400, /* Required when these surfaces are stored in a cube map */
  DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800, /* Required when these surfaces are stored in a cube map */
  DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000, /* Required when these surfaces are stored in a cube map */
  DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000, /* Required when these surfaces are stored in a cube map */
  DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000, /* Required when these surfaces are stored in a cube map */
  DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000, /*  Required when these surfaces are stored in a cube map */
  DDSCAPS2_VOLUME = 0x00200000, /* Required for a volume texture */
  DDSCAPS2_UNIT32 = 0xffffffff
} DDS_CAPS2;

typedef enum _DDPF_FLAGS {
  /* Texture contains alpha data; dwRGBAlphaBitMask contains valid data */
  DDPF_ALPHAPIXELS = 0x00000001, 
  /* Used in some older DDS files for alpha channel only uncompressed data (dwRGBBitCount contains the alpha channel bitcount; dwABitMask contains valid data) */
  DDPF_ALPHA = 0x00000002, 
  /* Texture contains compressed RGB data; dwFourCC contains valid data */
  DDPF_FOURCC = 0x00000004, 
  /* Texture contains uncompressed RGB data; dwRGBBitCount and the RGB masks (dwRBitMask, dwGBitMask, dwBBitMask) contain valid data */
  DDPF_RGB = 0x00000040,
  /* Used in some older DDS files for YUV uncompressed data (dwRGBBitCount contains the YUV bit count; dwRBitMask contains the Y mask, dwGBitMask contains the U mask, dwBBitMask contains the V mask) */
  DDPF_YUV = 0x00000200,
  /* Used in some older DDS files for single channel color uncompressed data (dwRGBBitCount contains the luminance channel bit count; dwRBitMask contains the channel mask). Can be combined with DDPF_ALPHAPIXELS for a two channel DDS file */
  DDPF_LUMINANCE = 0x00020000,
} DDPF_FLAGS;

typedef struct _DDS_PIXELFORMAT {
  uint32_t dwSize;   /* Structure size; set to 32 (bytes). */
  DDPF_FLAGS  dwFlags;  /* Values which indicate what type of data is in the surface. */
  /* Four-character codes for specifying compressed or custom formats. Possible values include: DXT1, DXT2, DXT3, DXT4, or DXT5. A FourCC of DX10 indicates the prescense of the DDS_HEADER_DXT10 extended header, and the dxgiFormat member of that structure indicates the true format. When using a four-character code, dwFlags must include DDPF_FOURCC. */
  uint32_t dwFourCC; 
  /* Number of bits in an RGB (possibly including alpha) format. Valid when dwFlags includes DDPF_RGB, DDPF_LUMINANCE, or DDPF_YUV. */
  uint32_t dwRGBBitCount;  
  /* Red (or luminance or Y) mask for reading color data. For instance, given the A8R8G8B8 format, the red mask would be 0x00ff0000. */
  uint32_t dwRBitMask; 
  /* Green (or U) mask for reading color data. For instance, given the A8R8G8B8 format, the green mask would be 0x0000ff00. */
  uint32_t dwGBitMask; 
  /* Blue (or V) mask for reading color data. For instance, given the A8R8G8B8 format, the blue mask would be 0x000000ff. */
  uint32_t dwBBitMask; 
  /* Alpha mask for reading alpha data. dwFlags must include DDPF_ALPHAPIXELS or DDPF_ALPHA. For instance, given the A8R8G8B8 format, the alpha mask would be 0xff000000. */
  uint32_t dwABitMask; 
} DDS_PIXELFORMAT;

typedef struct _DDS_HEADER {
  uint32_t        dwSize;   /* Size of structure. This member must be set to 124. */
  DDS_FLAGS       dwFlags;  /* Flags to indicate which members contain valid data. */
  uint32_t        dwHeight; /* Surface height (in pixels). */
  uint32_t        dwWidth;  /* Surface width (in pixels).  */
 /* The pitch or number of bytes per scan line in an uncompressed texture; the total number of 
  * bytes in the top level texture for a compressed texture. For information about how to compute
  * the pitch, see the DDS File Layout section of the Programming Guide for DDS.
  */
  uint32_t        dwPitchOrLinearSize;
  uint32_t        dwDepth;  /* Depth of a volume texture (in pixels), otherwise unused. */
  uint32_t        dwMipMapCount;  /* Number of mipmap levels, otherwise unused. */
  uint32_t        dwReserved1[11];
  DDS_PIXELFORMAT ddspf;
  DDS_CAPS        dwCaps;
  DDS_CAPS2       dwCaps2;
  uint32_t        dwCaps3;
  uint32_t        dwCaps4;
  uint32_t        dwReserved2;
} DDS_HEADER;

typedef enum _D3D10_RESOURCE_DIMENSION {	
  D3D10_RESOURCE_DIMENSION_UNKNOWN	  = 0x00000000,
  D3D10_RESOURCE_DIMENSION_TEXTURE1D	= 0x00000002,
  D3D10_RESOURCE_DIMENSION_TEXTURE2D	= 0x00000003,
  D3D10_RESOURCE_DIMENSION_TEXTURE3D	= 0x00000004
} D3D10_RESOURCE_DIMENSION;

typedef struct _DDS_HEADER_DXT10 {
  uint32_t  dxgiFormat;
  D3D10_RESOURCE_DIMENSION resourceDimension;
  uint32_t  miscFlag;
  uint32_t  arraySize;
  uint32_t  miscFlags2;
} DDS_HEADER_DXT10;
]])

local dds = {}
--- Calculate mip level pitch and size for block compressed formats (DXTn)
--
-- For block-compressed formats, compute the pitch as:
-- max( 1, ((width+3)/4) ) * block-size
--
local function bcSize(w, h, block_size)
  local pitch = math.max(1, math.floor((w + 3) / 4)) * block_size
  return pitch * math.max(1, math.floor((h + 3) / 4)), pitch
end
---
-- For other formats, compute the pitch as:
-- ( width * bits-per-pixel + 7 ) / 8
--
-- You divide by 8 for byte alignment.
--
local function rgbSize(w, h, bpp)
  local pitch = math.floor((w * bpp + 7) / 8)
  return pitch * h, pitch
end

local function fourcc2string(fcc)
  return string.char(bit.band(bit.rshift(fcc,  0), 0xff)) ..
         string.char(bit.band(bit.rshift(fcc,  8), 0xff)) ..
         string.char(bit.band(bit.rshift(fcc, 16), 0xff)) ..
         string.char(bit.band(bit.rshift(fcc, 24), 0xff))
end

function dds.isDDS(fourcc)
  return fourcc[0] == string.byte('D') and
         fourcc[1] == string.byte('D') and
         fourcc[2] == string.byte('S') and
         fourcc[3] == string.byte(' ')
end
--- Parse DDS header and position stream at the image data location
-- @param stream object used to read bytes
-- @return table with all information needed to create texture
function dds.getDetails(stream)
  local ret = {}
  local hdr = ffi.new('DDS_HEADER')
  if stream:read(hdr, ffi.sizeof('DDS_HEADER'), 1) ~= 1 then
    error("Unexpected DDS file, too short")
  end

  -- sanity checks
  if hdr.dwSize ~= 124 then
    error("Unexpected DDS file, size not 124 bytes")
  end

  local requiredFlags = bit.bor(
    ffi.C.DDSD_CAPS,
    ffi.C.DDSD_HEIGHT,
    ffi.C.DDSD_WIDTH,
    ffi.C.DDSD_PIXELFORMAT)
  if bit.band(hdr.dwFlags, requiredFlags) ~= requiredFlags then
    error("Unsupported DDS file, height, height or pixel format not specified")
  end

  if bit.band(hdr.dwCaps, ffi.C.DDSCAPS_TEXTURE) == 0 then
    error("Unsupported DDS file, not a texture")
  end

  if hdr.ddspf.dwSize ~= 32 then
    error("Unsupported DDS file pixel format size [" .. hdr.ddspf.dwSize .. "]")
  end

  ret.purpose = 'color'
  -- TODO: LE to native
  ret.height  = hdr.dwHeight
  ret.width   = hdr.dwWidth
  ret.levels  = 1
  ret.size    = 0
  ret.pitch   = 0

  if bit.band(hdr.dwFlags, ffi.C.DDSD_MIPMAPCOUNT) == ffi.C.DDSD_MIPMAPCOUNT and 
    hdr.dwMipMapCount > 0 then
    ret.levels = hdr.dwMipMapCount
  end
  -- data format
  if bit.band(hdr.ddspf.dwFlags, ffi.C.DDPF_FOURCC) ~= 0 then
    -- DXT1/2/3/4/5/10
    ret.format = hdr.ddspf.dwFourCC
    --
    -- The block-size is 8 bytes for DXT1, BC1, and BC4 (ATI1) formats 
    -- and 16 bytes for other block-compressed formats.
    --
    if bit.band(bit.rshift(ret.format, 24), 0xf) == 1 then
      ret.block_size = 8
    else
      ret.block_size = 16
    end
    -- total number of bytes in the top level texture
    if bit.band(hdr.dwCaps, ffi.C.DDSD_LINEARSIZE) ~= 0 then
      ret.size = hdr.dwPitchOrLinearSize
      agen.log.debug('application', "DDS level 0 linear size [%d] bytes, pitch [%d] bytes", ret.size, ret.pitch)
      _, ret.pitch = bcSize(ret.width, ret.height, ret.block_size)
    end

    if ret.pitch == 0 then
      ret.size, ret.pitch = bcSize(ret.width, ret.height, ret.block_size)
      agen.log.debug('application', "DDS level 0 calculated size [%d] bytes, pitch [%d] bytes", ret.size, ret.pitch)
    end
    --
    -- if dwFourCC is set to "DX10" an additional DDS_HEADER_DXT10 structure will be present
    --
    if fourcc2string(ret.format) == 'DX10' then
      agen.log.debug('application', "DX10 format detected")
      local hdr10 = ffi.new('DDS_HEADER_DXT10')
      if stream:read(hdr10, ffi.sizeof('DDS_HEADER_DXT10'), 1) ~= 1 then
        error("Unexpected DDS file, too short")
      end
      ret.format = hdr10.dxgiFormat
      agen.log.debug('application', "DDS texture [%dx%d], DXGI format [%d], levels [%d], size [%d] bytes",
        ret.width, ret.height, ret.format, ret.levels, ret.size)
    else
      agen.log.debug('application', "DDS texture [%dx%d], 4CC format [%s], levels [%d], size [%d] bytes",
        ret.width, ret.height, fourcc2string(ret.format), ret.levels, ret.size)
    end
  elseif bit.band(hdr.ddspf.dwFlags, ffi.C.DDPF_RGB) ~= 0 then
    --
    -- uncompressed RGB data; dwRGBBitCount and the RGB masks 
    -- (dwRBitMask, dwGBitMask, dwBBitMask) contain valid data
    -- Convert to SDL format:
    --
    ret.format = sdl.SDL_MasksToPixelFormatEnum(
      hdr.ddspf.dwRGBBitCount, 
      hdr.ddspf.dwRBitMask,
      hdr.ddspf.dwGBitMask,
      hdr.ddspf.dwBBitMask,
      hdr.ddspf.dwABitMask)

    -- number of bytes per scan line
    if bit.band(hdr.dwCaps, ffi.C.DDSD_PITCH) ~= 0 then
      ret.pitch = hdr.dwPitchOrLinearSize
      ret.size = ret.pitch * ret.height
    end

    if ret.pitch == 0 then
      ret.size, ret.pitch = rgbSize(ret.width, ret.height, hdr.ddspf.dwRGBBitCount)
    end
    ret.bgra = 1
    agen.log.debug('application', "DDS texture [%dx%d], RGB format [0x%x], levels [%d], size [%d] bytes",
      ret.width, ret.height, ret.format, ret.levels, ret.size)
  else
    error("Unsupported DDS file data format, DXT or RGB expected")
  end

  ret.bytesPerPixel = math.floor(ret.pitch / ret.width)

  return ret
end
--- Copy mip level <level> data to destrPtr
function dds.readMipLevel(stream, details, level, destPtr, destPtrSize)
  assert(stream, "Invalid parameter #1, rwops expected, got nil")
  assert(ffi.istype('SDL_RWops', stream), "Invalid parameter #1, rwops expected")
  assert(details, "Invalid parameter #2, DDS details table expected, got nil")
  assert(details.levels, "Missing required DDS property levels")
  assert(level >= 0, "Invalid parameter #3, positive integer or zero expected")
  assert(details.levels > level, "Invalid parameter #3")
  assert(destPtr, "Invalid parameter #4, destination cannot be nil")
  assert(destPtrSize > 0, "Invalid parameter #5, positive integer expected")

  local readSize  = 0
  local readPitch = 0

  if level == 0 then
    readSize  = details.size
    readPitch = details.pitch
  elseif bit.band(bit.rshift(details.format, 28), 0xff) == 1 then
    -- RGB with SDL format
    local w = math.max(bit.rshift(details.width, level), 1)
    local h = math.max(bit.rshift(details.height, level), 1)
    readSize, readPitch = rgbSize(w, h, bit.band(bit.rshift(details.format, 8), 0xff))
  else
    local w = math.max(bit.rshift(details.width, level), 1)
    local h = math.max(bit.rshift(details.height, level), 1)
    readSize, readPitch = bcSize(w, h, details.block_size)
  end

  assert(destPtrSize >= readSize, "Insufficient destination size")
  if stream:read(destPtr, readSize, 1) ~= 1 then
    error("Failed to read from DDS stream")
  end
  return readSize, readPitch
end

return dds
