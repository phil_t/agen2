local ffi = require('ffi')
local lib = require('agen.util.mpmc')

ffi.cdef([[
typedef struct agen_task_group_t agen_task_group_t;

agen_task_group_t* agen_task_group_create(void);
int32_t agen_task_group_add(agen_task_group_t* group, agen_queue_t* q, const char* symbol, void* data);
int32_t agen_task_group_notify(agen_task_group_t* group, agen_queue_t* q, agen_complete_f func, void* data);
void agen_task_group_wait(agen_task_group_t* group);
void agen_task_group_destroy(agen_task_group_t* group);
]])

-- @class Task group
-- @description Group of tasks that are performed in parallel.
local group = {}

--- Create a task group.
-- @return Task group object
function group:create()
  -- Note: finalizer will call wait internally and may block.
  return ffi.gc(lib.agen_task_group_create(), lib.agen_task_group_destroy)
end

--- Adds a new task to the group. 
-- @param q Thread pool queue
-- @param f Function name
-- @param d Data (optional)
-- @return 0 on success or error code
function group:add(q, f, d)
  return lib.agen_task_group_add(self, q, f, d)
end
--- Associate completion notifier with task group
-- @param q Thread pool queue
function group:notify(q, f, d)
  return lib.agen_task_group_notify(self, q, f, d)
end

--- Blocks caller thread until all tasks in the group are complete.
function group:wait()
  lib.agen_task_group_wait(self)
end

local gmt = { __index = group }
return ffi.metatype('agen_task_group_t', gmt)
