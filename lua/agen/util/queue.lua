local ffi = require('ffi')
local bit = require('bit')
local lib = require('agen.util.mpmc')

ffi.cdef([[
agen_queue_t* agen_queue_create(uint32_t size);
int32_t agen_queue_push(agen_queue_t* q, void* entry);
void* agen_queue_pop(agen_queue_t* q, uint32_t timeout);
void agen_queue_wait(agen_queue_t* q);
uint32_t agen_queue_num_items(agen_queue_t* q);
void agen_queue_destroy(agen_queue_t* q);
]])

local function next_pot(x)
  x = x - 1
  x = bit.bor(x, bit.rshift(x,  1))
  x = bit.bor(x, bit.rshift(x,  2))
  x = bit.bor(x, bit.rshift(x,  4))
  x = bit.bor(x, bit.rshift(x,  8))
  x = bit.bor(x, bit.rshift(x, 16))
  return x + 1
end
-- @class Thread-safe queue
-- @description Queue of messages, used for communication between threads.
local queue = {}
--- Casts a void pointer to a queue object
-- @param cdata object
-- @return Queue
function queue:cast(arg)
  return ffi.cast('agen_queue_t*', arg)
end
--- Creates a lua managed queue given a pre-set size.
-- @param n Total number of items. Exceeiding it will cause an error.
-- @return Queue object
function queue:create(n)
  assert(n, "queue size is nil")
  assert(n > 0, "queue size must be positive integer")
  n = next_pot(n)
  return ffi.gc(lib.agen_queue_create(n), lib.agen_queue_destroy)
end
--- Adds an item to the end of the queue.
-- Items are not cdata require either serializtion to cdata or a deep copy.
-- @param e cdata object
-- @return True if the item was added to the queue
function queue:push(e)
  assert(type(e) == 'cdata', "cdata expected")
  return lib.agen_queue_push(self, e) == 0
end
--- Removes an item from the front of the queue.
-- @return An item from the queue or nil if the queue is empty
function queue:pop()
  return lib.agen_queue_pop(self, 0)
end
--- Removes an item from the queue. Will block until one is available.
-- @return An item from the queue
function queue:pop_wait()
  return lib.agen_queue_pop(self, 0xffffffff)
end
--- Suspends the calling thread until the queue is empty effectively waiting for the consumer threads to drain the queue.
function queue:wait()
  lib.agen_queue_wait(self)
end
--- Returns the number of items in the queue. Not guaranteed to be correct.
-- @return Number of items in the queue.
function queue:len()
  return lib.agen_queue_num_items(self)
end

local qmt = { 
  __index = queue, 
  -- luajit passes more than self to __len
  __len = function(self) return lib.agen_queue_num_items(self) end
}
return ffi.metatype('agen_queue_t', qmt)
