----------------------------------------------------------------------
--
-- Built-in terminal for debugging purposes.
--
-- TODO:
--  1. More efficient rendering via index and instancing.
--  2. Use own framebuffer.
--  3. Animate framebuffer show / hide.
--  4. Text scrolling.
--
----------------------------------------------------------------------
local ffi = require('ffi')
local string = require('string')

local math3D = require('agen.util.math')
local sdl    = require('sdl2')
local image  = require('sdl2.image')
local rStateFactory = require('agen.renderState.factory')

local terminal = {
  debugFont = {},
}

local renderStateName = 'debugFont'
local numVertices = 0
-- Each glyph requires 2 triangles.
local vertPerGlyph = 6

ffi.cdef([[
typedef struct _font_cbuffer_t {
  float mvp[16];
  float color[4];
} font_cbuffer_t;
]])
-- triangle list
-- get a "triangle pair" for the character (6 vertices)
local function getTriangles(w, h, sx, sy, sw, sh, tw, th)
  local ux, uy   = sx / tw,      sy / th
  local ux2, uy2 = sw / tw + ux, sh / th + uy
  --
  -- FIXME: don't use pixel coordinates, but mm
  -- NOTE: vertices should go CCW or disable culling
  --
  return ffi.new('vertex2D[?]', vertPerGlyph, {
    {0,  0,  ux,  uy},
    {0,  h,  ux, uy2},
    {w,  0, ux2,  uy},

    {w,  0, ux2,  uy},
    {0,  h,  ux, uy2},
    {w,  h, ux2, uy2}
  })
end

local function loadFont(device, filename, cols, rows)
  local font = {}

  -- Load texture
  local img = image.createFromPath(filename)
  if img == nil then
    error("Failed to load texture")
  end
  agen.log.debug('application', "Created %s", tostring(img))
  --
  -- force conversion to 8-bit alpha as expected by the pixel shader
  --
  if img.format.format ~= sdl.PIXELFORMAT_A8 then
    img = img:createFromSurface(sdl.PIXELFORMAT_A8)
  end

  font.texture = device:createTextureFromSurface(img)
  --
  -- TODO: since only fixed fonts are supported use instancing.
  --
  font.quads = {}
  local tw, th = font.texture:getSize()
  local cw, ch = tw/cols, th/rows
  for i = 0, cols * rows do
    local x, y = i % cols, math.floor(i/rows)
    font.quads[i] = getTriangles(cw, ch, x*cw, y*ch, cw, ch, tw, th)
  end
  --
  -- store the character width/height
  --
  font.cw = cw
  font.ch = ch

  -- will use 1 dynamic buffer for 4k glyphs
  font.vbuffer = device:createBuffer({
    purpose = 'vertex',
    usage   = 'dynamic',
    size    = 4 * 1024 * ffi.sizeof('vertex2D[?]', vertPerGlyph),
    ctype   = 'vertex2D',
  })

  font.cbuffer = device:createBuffer({
    purpose = 'uniform',
    usage   = 'dynamic',
    size    = ffi.sizeof('font_cbuffer_t'),
    ctype   = 'font_cbuffer_t',
  })

  local w, h = device:getWindow():getSize()
  agen.log.debug('video', "terminal screen [%dx%d]", w, h)
  local h4 = h / 4

  font.tbuffer = device:createBuffer({
    purpose = 'vertex',
    usage   = 'static',
    size    = 4 * ffi.sizeof('vertex2D'),
    ctype   = 'vertex2D',
    data    = ffi.new('vertex2D[4]', {
			{-w, -h4, 0, 1},
			{ w, -h4, 1, 1},
			{-w,  h4, 0, 0},
      { w,  h4, 1, 0},
    })
  })

  -- white
	font.color = ffi.new('float[4]', {1, 1, 1, 1})

  return font
end
--- Generate vertices for string sz starting at ox, oy 
-- in viewport coordinates with 0,0 being top left and
-- w,h being down right.
local buffer = nil
local xCor, yCor = 0, 0
local q = ffi.new('vertex2D[?]', vertPerGlyph)
local function generateText(device, font, sz, ox, oy)
  ox, oy = ox or 0, oy or 0
  --
  -- get the triangle list for each character
  --
  local x, y = ox, oy
  --
  -- will only write, ok to discard existing buffer contents. We need 6 vertices per glyph
  --
  buffer = ffi.cast('vertex2D*', font.vbuffer:map('write_discard'))
  if not buffer then
    error("Failed to map vertex buffer")
  end
  
  for i = 0, string.len(sz) - 1 do
    local b = string.byte(sz, i + 1)
    -- new line and carriage return
    if b == 10 or b == 13 then
      x = ox
      y = y + font.ch
      --agen.log.debug('application', "line @ [%dx%d]", x, y)
    else
      -- FIXME: don't skip non-printable characters
      if b < 32 or b > 126 then
        b = 8
      else
        ffi.copy(ffi.cast('uint8_t*', q), 
                 ffi.cast('uint8_t*', font.quads[b]), 
                 ffi.sizeof('vertex2D') * vertPerGlyph)
        --
        -- translate
        --
        for j = 0, vertPerGlyph - 1 do
          q[j].x = q[j].x + x + xCor
          q[j].y = q[j].y + y + yCor
        end
        --
        -- copy quad vertices
        --
        ffi.copy(ffi.cast('uint8_t*', buffer[i * vertPerGlyph]), 
                 ffi.cast('uint8_t*', q), 
                 ffi.sizeof('vertex2D') * vertPerGlyph)
        --
        -- advance x
        --
        x = x + font.cw
      end
    end
  end
  font.vbuffer:unmap()

  numVertices = string.len(sz) * vertPerGlyph
end

local black = ffi.new('float[4]', {0, 0, 0, 1})
function terminal.write(device, sz, x, y)
	generateText(device, terminal.debugFont, sz, x or 0, y or 0)

  local rState = device:getRenderState(renderStateName)
	rState:setActive()

  local pass = rState:getRenderPass('default')
  pass:begin()
  --pass:clear(black)
  pass:drawPrimitive(0, numVertices)
  pass:submit()
end

function terminal.init(device)
  assert(device, "Invalid argument #1, video device expected, got nil")
  terminal.debugFont = loadFont(device, "assets/fonts/font.png", 16, 16)
  -----------------------------------------------------------------------
  --
  -- 2D rendering pipeline with ortho projection with 0, 0 at top left
  --
  -----------------------------------------------------------------------
  local dir = "assets/shaders/" .. device:getName()
  local debugFont = terminal.debugFont

	local rState = rStateFactory.create2D(device, {
    label = renderStateName,
    center = 'topleft',
    -- window coords
    size = {agen.window:getSize()},
    depthTest = false,
    shaders = {
      vertex = agen.io.contents(dir .. "/debugFont.vert"),
      pixel  = agen.io.contents(dir .. "/debugFont.frag"),
    },
    topology = 'triangles',
    vertexBuffers = {
      [0] = debugFont.vbuffer,
    },
    uniformBuffer = debugFont.cbuffer,
	  samplers = {
      [0] = device:createSampler({
        min = 'POINT',
        mag = 'POINT',
        mip = 'POINT',
        address = 'clamp',
      }),
    },
    textures = {
      [0] = debugFont.texture,
    },
  })
  --
  -- populate uniform and vertex buffer
  --
  local ptr = ffi.cast('font_cbuffer_t*', debugFont.cbuffer:map('write_discard'))
  ffi.copy(ptr.mvp, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
  ffi.copy(ptr.color, debugFont.color, ffi.sizeof('float[4]'))
  debugFont.cbuffer:unmap()
  -------------------------------------------------------------------------
  --
  -- Associate texture and sampler with active texture unit in pixel shader
  --
  -------------------------------------------------------------------------
  local pass = rState:getRenderPass('default')
  local tex0 = rState:getProgram():getSamplerIndex('tex0')
  pass:setTexture(tex0, 0, 0)
end

return terminal
