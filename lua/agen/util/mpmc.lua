local ffi = require('ffi')
local lib = ffi.load('mpmc')

ffi.cdef([[
typedef enum {
	NONE = 0x00,
	LUA_NOTIFY
} agen_userevent_code_t;
typedef struct agen_task_manager_t agen_task_manager_t;
typedef struct agen_queue_t agen_queue_t;
typedef void (__cdecl *agen_complete_f)(void* arg);
Uint32 agen_notify_eventID(void);
Uint32 agen_timer_eventID(void);
Uint32 agen_sdl_timer_callback(Uint32 interval, void* param);
/*
 * task manager
 */
agen_task_manager_t* agen_taskman_ref(void);
int32_t agen_taskman_start(agen_task_manager_t* aman, uint32_t max_threads);
uint32_t agen_taskman_is_running(agen_task_manager_t* aman);
uint32_t agen_taskman_num_threads(agen_task_manager_t* aman, void* null);
agen_queue_t* agen_get_queue(agen_task_manager_t* aman, SDL_ThreadPriority p);
int32_t agen_taskman_stop(agen_task_manager_t* aman);
void agen_taskman_unref(agen_task_manager_t* aman);
]])

return lib
