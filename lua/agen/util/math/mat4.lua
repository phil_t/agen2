local ffi  = require('ffi')
local sdl  = require('sdl2.lib')
local glm  = require('agen.util.math.glm')

ffi.cdef([[
void mat4_identity_pure(mat4* self);
void mat4_multiply_pure(mat4* self, const mat4* r);
void mat4_model_view_proj_pure(mat4* mat, const mat4* model, const mat4* view, const mat4* proj);
void mat4_ortho_pure(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_perspective_pure(mat4* self, float FOV, float width, float height, float zNear, float zFar);
void mat4_frustum_pure(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_lookAt_pure(mat4* self, const vec3* eye, const vec3* center, const vec3* up);
float* mat4_ptr_pure(mat4* self);
void mat4_rotate_pure(mat4* self, float rads, const vec3 *axis);
void mat4_translate_pure(mat4* self, const vec3* v);
void mat4_transpose_pure(mat4* self);
void mat4_inverse_pure(mat4* self);
void mat4_inverse_transpose_pure(mat4* self);
void mat4_scale_pure(mat4* self, const vec3* scale);
size_t mat4_tostring_pure(const mat4* self, char* buffer, size_t len);
bool mat4_compare_pure(const mat4* const self, const mat4* const r);
// SSE2
void mat4_identity_sse2(mat4* self);
void mat4_multiply_sse2(mat4* self, const mat4* r);
void mat4_ortho_sse2(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_perspective_sse2(mat4* self, float FOV, float width, float height, float zNear, float zFar);
void mat4_frustum_sse2(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_lookAt_sse2(mat4* self, const vec3* eye, const vec3* center, const vec3* up);
void mat4_model_view_proj_sse2(mat4* mat, const mat4* model, const mat4* view, const mat4* proj);
float* mat4_ptr_sse2(mat4* self);
void mat4_rotate_sse2(mat4* self, float rads, vec3* axis);
void mat4_translate_sse2(mat4* self, const vec3* v);
void mat4_transpose_sse2(mat4* self);
void mat4_inverse_sse2(mat4* self);
void mat4_inverse_transpose_sse2(mat4* self);
void mat4_scale_sse2(mat4* self, const vec3* v);
size_t mat4_tostring_sse2(const mat4* self, char* buffer, size_t len);
bool mat4_compare_sse2(const mat4* const self, const mat4* const r);
// AVX
void mat4_identity_avx(mat4* self);
void mat4_multiply_avx(mat4* self, const mat4* r);
void mat4_ortho_avx(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_perspective_avx(mat4* self, float FOV, float width, float height, float zNear, float zFar);
void mat4_frustum_avx(mat4* self, float left, float right, float bottom, float top, float zNear, float zFar);
void mat4_lookAt_avx(mat4* self, const vec3* eye, const vec3* center, const vec3* up);
void mat4_model_view_proj_avx(mat4* mat, const mat4* model, const mat4* view, const mat4* proj);
float* mat4_ptr_avx(mat4* self);
void mat4_rotate_avx(mat4* self, float rads, vec3* axis);
void mat4_translate_avx(mat4* self, const vec3* v);
void mat4_transpose_avx(mat4* self);
void mat4_inverse_avx(mat4* self);
void mat4_inverse_transpose_avx(mat4* self);
void mat4_scale_avx(mat4* self, const vec3* v);
size_t mat4_tostring_avx(const mat4* self, char* buffer, size_t len);
bool mat4_compare_avx(const mat4* const self, const mat4* const r);
]])

local Matrix4x4MT = {
	__index = {},
	__mul   = nil,
  __eq    = nil,
	__tostring = nil,
}

do
	if sdl.SDL_HasAVX() == sdl.TRUE then
    -- create new uninitialized matrix
		Matrix4x4MT.__index.create = function()
      return ffi.new('mat4 __attribute__((aligned(32)))')
		end
    -- create new matrix and initialize from data
		Matrix4x4MT.__index.createFromData = function(data)
      return ffi.new('mat4 __attribute__((aligned(32)))', data);
		end
    -- create new matrix initialized with identity
		Matrix4x4MT.__index.identity = function()
      local self = ffi.new('mat4 __attribute__((aligned(32)))')
      glm.mat4_identity_avx(self)
      return self
		end
    -- create new ortho matrix
		Matrix4x4MT.__index.ortho = function(left, right, bottom, top, near, far)
      local ret = ffi.new('mat4 __attribute__((aligned(32)))')
      glm.mat4_ortho_avx(ret, left, right, bottom, top, near, far)
			return ret
		end
    -- create new perspective matrix
		Matrix4x4MT.__index.perspective = function(fov, w, h, near, far)
			local ret = ffi.new('mat4 __attribute__((aligned(32)))')
      glm.mat4_perspective_avx(ret, fov, w, h, near, far)
      return ret
		end
    -- create new perspective matrix
		Matrix4x4MT.__index.lookAt = function(eye, target, up)
			local ret = ffi.new('mat4 __attribute__((aligned(32)))')
      glm.mat4_lookAt_avx(ret, eye, target, up)
      return ret
		end
    -- access matrix data pointer
		Matrix4x4MT.__index.ptr       = glm.mat4_ptr_avx
		Matrix4x4MT.__index.multiply  = function(self, mat)
      glm.mat4_multiply_avx(self, mat)
      return self
    end
    -- translate matrix by vector
		Matrix4x4MT.__index.translate = function(self, vec)
      glm.mat4_translate_avx(self, vec)
      return self
    end
		Matrix4x4MT.__index.rotate = function(self, rad, axis)
      glm.mat4_rotate_avx(self, rad, axis)
      return self
    end
		Matrix4x4MT.__index.scale = function(self, vec)
      glm.mat4_scale_avx(self, vec)
      return self
    end
		Matrix4x4MT.__index.setIdentity = function(self)
      glm.mat4_identity_avx(self)
      return self
		end
		Matrix4x4MT.__index.mvp = function(self, model, view, proj)
      glm.mat4_model_view_proj_avx(self, model, view, proj)
      return self
    end
    Matrix4x4MT.__mul = function(self, op)
			local ret = ffi.new('mat4 __attribute__((aligned(32)))', self.m)
      glm.mat4_multiply_avx(ret, op)
      return ret
		end
    Matrix4x4MT.__eq = function(self, op)
      return glm.mat4_multiply_avx(self, op)
    end
		Matrix4x4MT.__tostring = function(self)
      local len = 256
      local buffer = ffi.new('uint8_t[?]', len)
      local needs = glm.mat4_tostring_avx(self, buffer, len)
      if needs > 0 then
        buffer = ffi.new('uint8_t[?]', needs)
        needs = glm.mat4_tostring_avx(self, buffer, needs)
      end
      return ffi.string(buffer)
		end
		Matrix4x4MT.__index.transpose = function(self)
      glm.mat4_transpose_avx(self)
      return self
    end
		Matrix4x4MT.__index.inverse = function(self)
      glm.mat4_inverse_avx(self)
      return self
    end
		Matrix4x4MT.__index.inverse_transpose = function(self)
      glm.mat4_inverse_transpose_avx(self)
      return self
    end
    Matrix4x4MT.__index.frustum = function(l, r, b, t, n, f)
			local ret = ffi.new('mat4 __attribute__((aligned(32)))')
      glm.mat4_frustum_avx(ret, l, r, b, t, n, f)
      return ret
    end
	--
	-- Note: x64 implies SSE2
	--
  elseif sdl.SDL_HasSSE2() == sdl.TRUE then
    -- create new uninitialized matrix
		Matrix4x4MT.__index.create = function()
      return ffi.new('mat4 __attribute__((aligned(16)))')
		end
    -- create new matrix and initialize from data
		Matrix4x4MT.__index.createFromData = function(data)
      return ffi.new('mat4 __attribute__((aligned(16)))', data);
		end
    -- create new matrix initialized with identity
		Matrix4x4MT.__index.identity = function()
      local self = ffi.new('mat4 __attribute__((aligned(16)))')
      glm.mat4_identity_sse2(self)
      return self
		end
    -- create new ortho matrix
		Matrix4x4MT.__index.ortho = function(left, right, bottom, top, near, far)
      local ret = ffi.new('mat4 __attribute__((aligned(16)))')
      glm.mat4_ortho_sse2(ret, left, right, bottom, top, near, far)
			return ret
		end
    -- create new perspective matrix
		Matrix4x4MT.__index.perspective = function(fov, w, h, near, far)
			local ret = ffi.new('mat4 __attribute__((aligned(16)))')
      glm.mat4_perspective_sse2(ret, fov, w, h, near, far)
      return ret
		end
    -- create new perspective matrix
		Matrix4x4MT.__index.lookAt = function(eye, target, up)
			local ret = ffi.new('mat4 __attribute__((aligned(16)))')
      glm.mat4_lookAt_sse2(ret, eye, target, up)
      return ret
		end
    -- access matrix data pointer
		Matrix4x4MT.__index.ptr = glm.mat4_ptr_sse2
		Matrix4x4MT.__index.multiply = function(self, mat)
      glm.mat4_multiply_sse2(self, mat)
      return self
    end
    -- translate matrix by vector
		Matrix4x4MT.__index.translate = function(self, vec)
      glm.mat4_translate_sse2(self, vec)
      return self
    end
		Matrix4x4MT.__index.rotate    = function(self, rad, axis)
      glm.mat4_rotate_sse2(self, rad, axis)
      return self
    end
		Matrix4x4MT.__index.scale     = function(self, vec)
      glm.mat4_scale_sse2(self, vec)
      return self
    end
		Matrix4x4MT.__index.setIdentity = function(self)
      glm.mat4_identity_sse2(self)
      return self
		end
		Matrix4x4MT.__index.mvp       = function(self, model, view, proj)
      glm.mat4_model_view_proj_sse2(self, model, view, proj)
      return self
    end
    Matrix4x4MT.__mul = function(self, op)
			local ret = ffi.new('mat4 __attribute__((aligned(16)))', self.m)
      glm.mat4_multiply_sse2(ret, op)
      return ret
		end
    Matrix4x4MT.__eq = function(self, op)
      return glm.mat4_multiply_sse2(self, op)
    end
		Matrix4x4MT.__tostring = function(self)
      local len = 256
      local buffer = ffi.new('uint8_t[?]', len)
      local needs = glm.mat4_tostring_sse2(self, buffer, len)
      if needs > 0 then
        buffer = ffi.new('uint8_t[?]', needs)
        needs = glm.mat4_tostring_sse2(self, buffer, needs)
      end
      return ffi.string(buffer)
		end
		Matrix4x4MT.__index.transpose = function(self)
      glm.mat4_transpose_sse2(self)
      return self
    end
		Matrix4x4MT.__index.inverse = function(self)
      glm.mat4_inverse_sse2(self)
      return self
    end
		Matrix4x4MT.__index.inverse_transpose = function(self)
      glm.mat4_inverse_transpose_sse2(self)
      return self
    end
    Matrix4x4MT.__index.frustum = function(l, r, b, t, n, f)
			local ret = ffi.new('mat4 __attribute__((aligned(16)))')
      glm.mat4_frustum_sse2(ret, l, r, b, t, n, f)
      return ret
    end
	else
		Matrix4x4MT.__index.create = function()
			return ffi.new('mat4')
		end
		Matrix4x4MT.__index.createFromData = function(data)
			return ffi.new('mat4', data)
		end
		Matrix4x4MT.__index.identity = function()
			local self = ffi.new('mat4')
      glm.mat4_identity_pure(self)
      return self
		end
		Matrix4x4MT.__index.ortho = function(left, right, bottom, top, near, far)
			local ret = ffi.new('mat4')
      glm.mat4_ortho_pure(ret, left, right, bottom, top, near, far)
      return ret
		end
		Matrix4x4MT.__index.perspective = function(fov, w, h, near, far)
			local ret = ffi.new('mat4')
      glm.mat4_perspective_pure(ret, fov, w, h, near, far)
      return ret
		end
		Matrix4x4MT.__index.lookAt = function(eye, target, up)
			local ret = ffi.new('mat4')
      glm.mat4_lookAt_pure(ret, eye, target, up)
      return ret
		end
		Matrix4x4MT.__index.ptr       = glm.mat4_ptr_pure
		Matrix4x4MT.__index.multiply  = function(self, mat)
      glm.mat4_multiply_pure(self, mat)
      return self
    end
		Matrix4x4MT.__index.translate = function(self, vec)
      glm.mat4_translate_pure(self, vec)
      return self
    end
		Matrix4x4MT.__index.rotate    = function(self, rad, axis)
      glm.mat4_rotate_pure(self, rad, axis)
      return self
    end
		Matrix4x4MT.__index.scale     = function(self, vec)
      glm.mat4_scale_pure(self, vec)
      return self
    end
		Matrix4x4MT.__index.setIdentity = function(self)
      glm.mat4_identity_pure(self)
      return self
		end
		Matrix4x4MT.__index.mvp       = function(self, model, view, proj)
      glm.mat4_model_view_proj_pure(self, model, view, proj)
      return self
    end
		Matrix4x4MT.__index.transpose = function(self)
      glm.mat4_transpose_pure(self)
      return self
    end
		Matrix4x4MT.__index.inverse = function(self)
      glm.mat4_inverse_pure(self)
      return self
    end
		Matrix4x4MT.__index.inverse_transpose = function(self)
      glm.mat4_inverse_transpose_pure(self)
      return self
    end
		Matrix4x4MT.__mul = function(self, op)
			local ret = ffi.new('mat4', self.m)
      glm.mat4_op_multiply_pure(self, op)
      return ret
		end
    Matrix4x4MT.__eq = function(self, op)
      return glm.mat4_multiply_pure(self, op)
    end
		Matrix4x4MT.__tostring = function(self)
      local len = 256
      local buffer = ffi.new('uint8_t[?]', len)
      local needs = glm.mat4_tostring_pure(self, buffer, len)
      if needs > 0 then
        buffer = ffi.new('uint8_t[?]', needs)      
        glm.mat4_tostring_pure(self, buffer, needs)
      end
      return ffi.string(buffer)
		end
    Matrix4x4MT.__index.frustum = function(l, r, b, t, n, f)
			local ret = ffi.new('mat4')
      glm.mat4_frustum_pure(ret, l, r, b, t, n, f)
      return ret
    end
	end
end

return ffi.metatype('mat4', Matrix4x4MT)
