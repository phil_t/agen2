local ffi = require('ffi')
local glm = ffi.load('glm')

ffi.cdef([[
typedef struct _vec2 {float x, y;} vec2;
typedef struct _vec3 {float x, y, z;} vec3;
typedef struct _vec4 {float x, y, z, w;} vec4;
typedef struct _mat3 {float m[9];} mat2d;
typedef struct _mat4 {float m[16];} mat4;
]])

return glm
