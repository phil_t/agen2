local ffi = require('ffi')
local glm = require('agen.util.math.glm')

ffi.cdef([[
void mat2D_identity(mat2d* self);
void mat2d_translate(mat2d* self, float x, float y);
void mat2d_rotate(mat2d* self, float rad);
void mat2d_scale(mat2d* self, float x, float y);
void mat2d_shearX(mat2d* self, float factor);
void mat2d_shearY(mat2d* self, float factor);
void mat2d_multiply(mat2d* self, const mat2d* left);
float* mat2d_ptr(mat2d* self);
]])

local Matrix3x3MT = {
  __index = {},
  __mul   = nil,
}

Matrix3x3MT.__index.create = function()
  return ffi.new('mat2d')
end
Matrix3x3MT.__index.translate = function(self, x, y)
  glm.mat2d_translate(self, x, y)
  return self
end
Matrix3x3MT.__index.rotate    = function(self, rad)
  glm.mat2d_rotate(self, rad)
  return self
end
Matrix3x3MT.__index.scale     = function(self, x, y)
  glm.mat2d_scale(self, x, y)
  return self
end
Matrix3x3MT.__index.shearX    = function(self, x)
  glm.mat2d_shearX(self, x)
  return self
end
Matrix3x3MT.__index.shearY    = function(self, y)
  glm.mat2d_shearY(self, y)
  return self
end
Matrix3x3MT.__index.multiply  = function(self, mat)
  glm.mat2d_multiply(self, mat)
  return self
end
Matrix3x3MT.__index.ptr       = glm.mat2d_ptr

Matrix3x3MT.__mul = function(self, mat)
  local ret = ffi.new('mat2d', self.m)
  glm.mat2d_multiply(ret, mat)
  return ret
end

return ffi.metatype('mat2d', Matrix3x3MT)
