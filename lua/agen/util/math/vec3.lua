local ffi = require('ffi')
local glm = require('agen.util.math.glm')

ffi.cdef([[
size_t vec3_tostring(vec3* v, uint8_t* data, size_t len);
]])

local Vec3MT = {
	__index = {},
	__tostring = nil,
}

Vec3MT.__index.create = function(x, y, z)
  return ffi.new('vec3', {x, y, z})
end

Vec3MT.__tostring = function(self)
  local data = ffi.new('uint8_t[256]')
  local needs = glm.vec3_tostring(self, data, 256)
  if needs > 0 then
    data = ffi.new('uint8_t[?]', needs)
    glm.vec3_tostring(self, data, needs)
  end
	return ffi.string(data)
end

return ffi.metatype('vec3', Vec3MT)
