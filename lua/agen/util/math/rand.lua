local ffi  = require('ffi')
local glm  = require('agen.util.math.glm')

ffi.cdef([[
double random_gaussian(double mean, double dev);
double random_linear(double min, double max);
void random_spherical(vec3* v, double radius);
]])

return {
	gaus	 = glm.random_gaussian,
	linear = glm.random_linear,
	sphere = glm.random_spherical,
}
