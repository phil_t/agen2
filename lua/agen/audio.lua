----------------------------------------------------------------------------------------
--
-- Audio device interface abstracting different 3D APIs.
-- 
-- Usage:
--	* Create device object and use it as audio buffer / player factory.
--
-- Notes:
--  * Accessible from any thread
--
----------------------------------------------------------------------------------------
local _deviceObj = nil
local audio = {}

function audio.init(window, config)
  assert(window ~= nil,      'invalid argument #1, app window cannot be nil');
  assert(config  ~= nil and
    type(config) == 'table', 'invalid argument #2, lua table expected, got nil')
  assert(config.driver,      'invalid argument #2, missing required key .driver')
  assert(config.driver, "Invalid argument #2, missing required option `driver'")
	local _driver = require('agen.audio.' .. config.driver)
	-- create device now
	_deviceObj = _driver.createDevice(window, config)
end

function audio.getDevice()
	return _deviceObj
end

function audio.release()
  -- was is initialized successfully?
  if _deviceObj ~= nil then
    -- enforce specific finalization order
    _deviceObj:finalize()
    _deviceObj = nil
  end
end

return audio
