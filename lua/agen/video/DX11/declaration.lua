local string = require('string')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_INPUT_CLASSIFICATION { 
	INPUT_PER_VERTEX_DATA    = 0x00000000,
	INPUT_PER_INSTANCE_DATA  = 0x00000001
} D3D11_INPUT_CLASSIFICATION;

typedef struct _D3D11_INPUT_ELEMENT_DESC {
	const char*                SemanticName;
	UINT                       SemanticIndex;
	DXGI_FORMAT                Format;
	UINT                       InputSlot;
	UINT                       AlignedByteOffset;
	D3D11_INPUT_CLASSIFICATION InputSlotClass;
	UINT                       InstanceDataStepRate;
} D3D11_INPUT_ELEMENT_DESC;

typedef struct _ID3D11InputLayout ID3D11InputLayout;
typedef struct _ID3D11InputLayoutVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11InputLayout* This, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11InputLayout* This);
  ULONG (__stdcall *Release)(ID3D11InputLayout* This);
  void (__stdcall *GetDevice)(ID3D11InputLayout* This, ID3D11Device **ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11InputLayout* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11InputLayout* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11InputLayout* This, REFGUID guid, const IUnknown *pData);
} ID3D11InputLayoutVtbl;

struct _ID3D11InputLayout {
	const ID3D11InputLayoutVtbl* lpVtbl;
};
]])

local ID3D11InputLayout = {}
ID3D11InputLayout.GUID_InputElementDesc = ffi.new('GUID', {
  0x1b0f2e7b,
  0xf77e, 0x4805,
  {0xa0, 0xa6, 0x17, 0x5d, 0x48, 0x31, 0x47, 0x9f}
})
ffi.metatype('ID3D11InputLayout', D3D.ID3D11DeviceChildMT)
---------------------------------------------------------------------------------
--
-- D3D11VertexDecl
--
---------------------------------------------------------------------------------
local _MAX_ATTRIB_SLOTS  = 8
local declType = {
  float1 = D3D.FORMAT_R32_FLOAT,
  float2 = D3D.FORMAT_R32G32_FLOAT,
  float3 = D3D.FORMAT_R32G32B32_FLOAT,
  float4 = D3D.FORMAT_R32G32B32A32_FLOAT,
  d3dcolor = D3D.FORMAT_R32_UINT,
  ubyte4 = D3D.FORMAT_R8G8B8A8_UNORM,
  none	 = D3D.FORMAT_UNKNOWN,
}

local D3D11VertexDecl = {}
local D3D11VertexDeclMT = {__index = D3D11VertexDecl}

function D3D11VertexDecl.create(device, layout, program)
  assert(device, "Invalid argument #1, ID3D11Device device expected, got nil")
  assert(ffi.istype('ID3D11Device', device), "Invalid argument #1, ID3D11Device expected")

	local self = {
		layout  = nil,
    strides = ffi.new('UINT[?]', _MAX_ATTRIB_SLOTS),
	}
	setmetatable(self, D3D11VertexDeclMT)

  local inputDesc = ffi.new('D3D11_INPUT_ELEMENT_DESC[?]', #layout)
  local semanticIndex = {}
  local numSlots = 0

	for i = 1, #layout do
    local index = layout[i].index
    local t = layout[i].type
    local u = layout[i].usage
    if not semanticIndex[u] then
      semanticIndex[u] = 0
    end

		inputDesc[i - 1].SemanticName         = layout[i].name
		inputDesc[i - 1].SemanticIndex        = semanticIndex[u]
		inputDesc[i - 1].Format               = declType[t]
		inputDesc[i - 1].InputSlot            = index
		inputDesc[i - 1].AlignedByteOffset    = layout[i].offset
    if layout[i].instanced then
      inputDesc[i - 1].InputSlotClass       = D3D.INPUT_PER_INSTANCE_DATA
      inputDesc[i - 1].InstanceDataStepRate = 1
    else
      inputDesc[i - 1].InputSlotClass       = D3D.INPUT_PER_VERTEX_DATA
      inputDesc[i - 1].InstanceDataStepRate = 0
    end

    agen.log.debug('video', "name [%s] index [%d] format [0x%x] slot [%d] offset [%d], step [%d], stride [%d]",
      inputDesc[i - 1].SemanticName,
      inputDesc[i - 1].SemanticIndex,
      inputDesc[i - 1].Format,
      inputDesc[i - 1].InputSlot,
      inputDesc[i - 1].AlignedByteOffset,
	    inputDesc[i - 1].InstanceDataStepRate,
      layout[i].stride)

    semanticIndex[u] = semanticIndex[u] + 1

    if index + 1 > numSlots then
      numSlots = index + 1
    end

    self.strides[index] = layout[i].stride
	end

  self.layout = device:createInputLayout(inputDesc, #layout, program:getFirstShaderCode())

	return self
end

function D3D11VertexDecl:setActive(ctx)
  ctx:setLayout(self.layout)
end

return D3D11VertexDecl
