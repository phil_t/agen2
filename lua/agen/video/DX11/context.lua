---------------------------------------------------------------------------------
--
-- Context:
--  created for each thread.
--
---------------------------------------------------------------------------------
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
/************************************************************************
 *
 * rendering
 *
 ************************************************************************/
typedef enum _D3D11_CLEAR_FLAG { 
  CLEAR_DEPTH    = 0x1L,
  CLEAR_STENCIL  = 0x2L
} D3D11_CLEAR_FLAG;
/************************************************************************
 *
 * Depth/Stencil buffer
 *
 ************************************************************************/
typedef enum _D3D11_DEPTH_WRITE_MASK { 
  DEPTH_WRITE_MASK_ZERO  = 0,
  DEPTH_WRITE_MASK_ALL   = 1
} D3D11_DEPTH_WRITE_MASK;

typedef enum _D3D11_STENCIL_OP { 
  STENCIL_OP_KEEP      = 1,
  STENCIL_OP_ZERO      = 2,
  STENCIL_OP_REPLACE   = 3,
  STENCIL_OP_INCR_SAT  = 4,
  STENCIL_OP_DECR_SAT  = 5,
  STENCIL_OP_INVERT    = 6,
  STENCIL_OP_INCR      = 7,
  STENCIL_OP_DECR      = 8
} D3D11_STENCIL_OP;

typedef enum _D3D11_DSV_DIMENSION { 
  DSV_DIMENSION_UNKNOWN           = 0,
  DSV_DIMENSION_TEXTURE1D         = 1,
  DSV_DIMENSION_TEXTURE1DARRAY    = 2,
  DSV_DIMENSION_TEXTURE2D         = 3,
  DSV_DIMENSION_TEXTURE2DARRAY    = 4,
  DSV_DIMENSION_TEXTURE2DMS       = 5,
  DSV_DIMENSION_TEXTURE2DMSARRAY  = 6
} D3D11_DSV_DIMENSION;

typedef struct _D3D11_TEX1D_DSV {
  UINT MipSlice;
} D3D11_TEX1D_DSV;

typedef struct _D3D11_TEX1D_ARRAY_DSV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX1D_ARRAY_DSV;

typedef struct _D3D11_TEX2D_DSV {
  UINT MipSlice;
} D3D11_TEX2D_DSV;

typedef struct _D3D11_TEX2D_ARRAY_DSV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2D_ARRAY_DSV;

typedef struct _D3D11_TEX2DMS_DSV {
  UINT UnusedField_NothingToDefine;
} D3D11_TEX2DMS_DSV;

typedef struct _D3D11_TEX2DMS_ARRAY_DSV {
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2DMS_ARRAY_DSV;

typedef struct _D3D11_DEPTH_STENCILOP_DESC {
  D3D11_STENCIL_OP      StencilFailOp;
  D3D11_STENCIL_OP      StencilDepthFailOp;
  D3D11_STENCIL_OP      StencilPassOp;
  D3D11_COMPARISON_FUNC StencilFunc;
} D3D11_DEPTH_STENCILOP_DESC;

typedef struct _D3D11_DEPTH_STENCIL_DESC {
  BOOL                       DepthEnable;
  D3D11_DEPTH_WRITE_MASK     DepthWriteMask;
  D3D11_COMPARISON_FUNC      DepthFunc;
  BOOL                       StencilEnable;
  UINT8                      StencilReadMask;
  UINT8                      StencilWriteMask;
  D3D11_DEPTH_STENCILOP_DESC FrontFace; 
  D3D11_DEPTH_STENCILOP_DESC BackFace;
} D3D11_DEPTH_STENCIL_DESC;

typedef struct _D3D11_DEPTH_STENCIL_VIEW_DESC {
  DXGI_FORMAT         Format;
  D3D11_DSV_DIMENSION ViewDimension;
  UINT                Flags;
  union {
    D3D11_TEX1D_DSV         Texture1D;
    D3D11_TEX1D_ARRAY_DSV   Texture1DArray;
    D3D11_TEX2D_DSV         Texture2D;
    D3D11_TEX2D_ARRAY_DSV   Texture2DArray;
    D3D11_TEX2DMS_DSV       Texture2DMS;
    D3D11_TEX2DMS_ARRAY_DSV Texture2DMSArray;
  };
} D3D11_DEPTH_STENCIL_VIEW_DESC;

typedef struct _D3D11_RENDER_TARGET_VIEW_DESC {
  DXGI_FORMAT         Format;
  D3D11_RTV_DIMENSION ViewDimension;
  union {
    D3D11_BUFFER_RTV        Buffer;
    D3D11_TEX1D_RTV         Texture1D;
    D3D11_TEX1D_ARRAY_RTV   Texture1DArray;
    D3D11_TEX2D_RTV         Texture2D;
    D3D11_TEX2D_ARRAY_RTV   Texture2DArray;
    D3D11_TEX2DMS_RTV       Texture2DMS;
    D3D11_TEX2DMS_ARRAY_RTV Texture2DMSArray;
    D3D11_TEX3D_RTV         Texture3D;
  };
} D3D11_RENDER_TARGET_VIEW_DESC;
/************************************************************************************
 *
 * Alpha blending
 *
 ************************************************************************************/
typedef enum _D3D11_BLEND { 
  BLEND_ZERO              = 1,
  BLEND_ONE               = 2,
  BLEND_SRC_COLOR         = 3,
  BLEND_INV_SRC_COLOR     = 4,
  BLEND_SRC_ALPHA         = 5,
  BLEND_INV_SRC_ALPHA     = 6,
  BLEND_DEST_ALPHA        = 7,
  BLEND_INV_DEST_ALPHA    = 8,
  BLEND_DEST_COLOR        = 9,
  BLEND_INV_DEST_COLOR    = 10,
  BLEND_SRC_ALPHA_SAT     = 11,
  BLEND_BLEND_FACTOR      = 14,
  BLEND_INV_BLEND_FACTOR  = 15,
  BLEND_SRC1_COLOR        = 16,
  BLEND_INV_SRC1_COLOR    = 17,
  BLEND_SRC1_ALPHA        = 18,
  BLEND_INV_SRC1_ALPHA    = 19
} D3D11_BLEND;

typedef enum _D3D11_BLEND_OP { 
  BLEND_OP_ADD           = 1,
  BLEND_OP_SUBTRACT      = 2,
  BLEND_OP_REV_SUBTRACT  = 3,
  BLEND_OP_MIN           = 4,
  BLEND_OP_MAX           = 5
} D3D11_BLEND_OP;

typedef enum _D3D11_COLOR_WRITE_ENABLE { 
  COLOR_WRITE_ENABLE_RED    = 1,
  COLOR_WRITE_ENABLE_GREEN  = 2,
  COLOR_WRITE_ENABLE_BLUE   = 4,
  COLOR_WRITE_ENABLE_ALPHA  = 8,
  COLOR_WRITE_ENABLE_ALL    = 0x0000000f
} D3D11_COLOR_WRITE_ENABLE;

typedef struct _D3D11_RENDER_TARGET_BLEND_DESC {
  BOOL           BlendEnable;
  D3D11_BLEND    SrcBlend;
  D3D11_BLEND    DestBlend;
  D3D11_BLEND_OP BlendOp;
  D3D11_BLEND    SrcBlendAlpha;
  D3D11_BLEND    DestBlendAlpha;
  D3D11_BLEND_OP BlendOpAlpha;
  UINT8          RenderTargetWriteMask;
} D3D11_RENDER_TARGET_BLEND_DESC;

typedef struct _D3D11_BLEND_DESC {
  BOOL                           AlphaToCoverageEnable;
  BOOL                           IndependentBlendEnable;
  D3D11_RENDER_TARGET_BLEND_DESC RenderTarget[8];
} D3D11_BLEND_DESC;

typedef struct _ID3D11ClassInstance ID3D11ClassInstance;
/**************************************************************
 *
 * ID3D11CommandList
 *
 **************************************************************/
typedef struct _ID3D11CommandList   ID3D11CommandList;
typedef struct _ID3D11CommandListVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11CommandList* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11CommandList* This);
  ULONG (__stdcall *Release)(ID3D11CommandList* This);
  void (__stdcall *GetDevice)(ID3D11CommandList* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11CommandList* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11CommandList* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11CommandList* This, REFGUID guid, const IUnknown *pData);
  UINT (__stdcall *GetContextFlags)(ID3D11CommandList* This);
} ID3D11CommandListVtbl;

struct _ID3D11CommandList {
  const ID3D11CommandListVtbl* lpVtbl;
};
/*************************************************************************************
 *
 * Depth/Stencil View
 *
 *************************************************************************************/
typedef struct _ID3D11DepthStencilView ID3D11DepthStencilView;
typedef struct _ID3D11DepthStencilViewVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11DepthStencilView* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11DepthStencilView* This);
  ULONG (__stdcall *Release)(ID3D11DepthStencilView* This);
  void (__stdcall *GetDevice)(ID3D11DepthStencilView* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11DepthStencilView* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11DepthStencilView* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11DepthStencilView* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetResource)(ID3D11DepthStencilView* This, void** ppResource);
  void (__stdcall *GetDesc)(ID3D11DepthStencilView* This, D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc);
} ID3D11DepthStencilViewVtbl;

struct _ID3D11DepthStencilView {
  const ID3D11DepthStencilViewVtbl* lpVtbl;
};

typedef struct _ID3D11DepthStencilState ID3D11DepthStencilState;
typedef struct _ID3D11DepthStencilStateVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11DepthStencilState* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11DepthStencilState* This);
  ULONG (__stdcall *Release)(ID3D11DepthStencilState* This);
  void (__stdcall *GetDevice)(ID3D11DepthStencilState* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11DepthStencilState* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11DepthStencilState* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11DepthStencilState* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetDesc)(ID3D11DepthStencilState* This, D3D11_DEPTH_STENCIL_DESC *pDesc);
} ID3D11DepthStencilStateVtbl;

struct _ID3D11DepthStencilState {
  const ID3D11DepthStencilStateVtbl* lpVtbl;
};
/***************************************************************************************
 *
 * Rasterizer State
 *
 ***************************************************************************************/
typedef struct _ID3D11RasterizerState ID3D11RasterizerState;
typedef struct _ID3D11RasterizerStateVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11RasterizerState* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11RasterizerState* This);
  ULONG (__stdcall *Release)(ID3D11RasterizerState* This);
  void (__stdcall *GetDevice)(ID3D11RasterizerState* This,ID3D11Device **ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11RasterizerState* This, REFGUID guid, UINT* pDataSize, void* pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11RasterizerState* This, REFGUID guid, UINT DataSize, const void* pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11RasterizerState* This, REFGUID guid, const IUnknown* pData);
  void (__stdcall *GetDesc)(ID3D11RasterizerState* This, D3D11_RASTERIZER_DESC* pDesc);
} ID3D11RasterizerStateVtbl;

struct _ID3D11RasterizerState {
  const ID3D11RasterizerStateVtbl* lpVtbl;
};

typedef struct _ID3D11BlendState ID3D11BlendState;
typedef struct _ID3D11BlendStateVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11BlendState* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11BlendState* This);
  ULONG (__stdcall *Release)(ID3D11BlendState* This);
  void (__stdcall *GetDevice)(ID3D11BlendState* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11BlendState* This, REFGUID guid, UINT* pDataSize, void* pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11BlendState* This, REFGUID guid, UINT DataSize, const void* pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11BlendState* This, REFGUID guid, const IUnknown* pData);
  void (__stdcall *GetDesc)(ID3D11BlendState* This, D3D11_BLEND_DESC* pDesc);
} ID3D11BlendStateVtbl;

struct _ID3D11BlendState {
  const ID3D11BlendStateVtbl* lpVtbl;
};

typedef struct _D3D11_BOX {
  UINT left,   top;
  UINT front,  right;
  UINT bottom, back;
} D3D11_BOX;

typedef struct _ID3D11RenderTargetView ID3D11RenderTargetView;
typedef struct _ID3D11RenderTargetViewVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11RenderTargetView* This, REFIID riid, void **ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11RenderTargetView* This);
  ULONG (__stdcall *Release)(ID3D11RenderTargetView* This);
  void (__stdcall *GetDevice)(ID3D11RenderTargetView* This, ID3D11Device **ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11RenderTargetView* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData )(ID3D11RenderTargetView* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11RenderTargetView* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetResource)(ID3D11RenderTargetView* This, void** ppResource);
  void (__stdcall *GetDesc)(ID3D11RenderTargetView* This, D3D11_RENDER_TARGET_VIEW_DESC *pDesc);
} ID3D11RenderTargetViewVtbl;

struct _ID3D11RenderTargetView {
  const ID3D11RenderTargetViewVtbl* lpVtbl;
};

typedef struct _ID3D11DeviceContext ID3D11DeviceContext;
typedef struct _ID3D11DeviceContextVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11DeviceContext* This, REFIID riid, void **ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11DeviceContext* This);
  ULONG (__stdcall *Release)(ID3D11DeviceContext* This);
  void (__stdcall *GetDevice)(ID3D11DeviceContext* This,ID3D11Device **ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11DeviceContext* This, REFGUID guid, UINT *pDataSize,void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11DeviceContext* This, REFGUID guid, UINT DataSize,const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11DeviceContext* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *VSSetConstantBuffers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *PSSetShaderResources)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *PSSetShader)(ID3D11DeviceContext* This, ID3D11Shader *pPixelShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances);
  void (__stdcall *PSSetSamplers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *VSSetShader)(ID3D11DeviceContext* This, ID3D11Shader *pVertexShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances);
  void (__stdcall *DrawIndexed)(ID3D11DeviceContext* This, UINT IndexCount, UINT StartIndexLocation, INT BaseVertexLocation);
  void (__stdcall *Draw)(ID3D11DeviceContext* This, UINT VertexCount, UINT StartVertexLocation);
  HRESULT (__stdcall *Map)(ID3D11DeviceContext* This, void *pResource, UINT Subresource, D3D11_MAP MapType, D3D11_MAP_FLAG MapFlags, D3D11_MAPPED_SUBRESOURCE* pMappedResource);
  void (__stdcall *Unmap)(ID3D11DeviceContext* This, void *pResource, UINT Subresource);
  void (__stdcall *PSSetConstantBuffers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *IASetInputLayout)(ID3D11DeviceContext* This, ID3D11InputLayout *pInputLayout);
  void (__stdcall *IASetVertexBuffers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppVertexBuffers, const UINT *pStrides, const UINT *pOffsets);
  void (__stdcall *IASetIndexBuffer)(ID3D11DeviceContext* This, ID3D11Buffer *pIndexBuffer, DXGI_FORMAT Format, UINT Offset);
  void (__stdcall *DrawIndexedInstanced)(ID3D11DeviceContext* This, UINT IndexCountPerInstance, UINT InstanceCount, UINT StartIndexLocation, INT BaseVertexLocation, UINT StartInstanceLocation);
  void (__stdcall *DrawInstanced)(ID3D11DeviceContext* This, UINT VertexCountPerInstance, UINT InstanceCount, UINT StartVertexLocation, UINT StartInstanceLocation);
  void (__stdcall *GSSetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *GSSetShader)(ID3D11DeviceContext* This, ID3D11Shader *pShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances);
  void (__stdcall *IASetPrimitiveTopology)(ID3D11DeviceContext* This,D3D11_PRIMITIVE_TOPOLOGY Topology);
  void (__stdcall *VSSetShaderResource)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *VSSetSamplers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *Begin)(ID3D11DeviceContext* This, ID3D11Asynchronous *pAsync);
  void (__stdcall *End)(ID3D11DeviceContext* This, ID3D11Asynchronous *pAsync);
  HRESULT (__stdcall *GetData)(ID3D11DeviceContext* This, ID3D11Asynchronous *pAsync, void *pData, UINT DataSize, UINT GetDataFlags);
  void (__stdcall *SetPredication)(ID3D11DeviceContext* This, ID3D11Predicate *pPredicate, BOOL PredicateValue);
  void (__stdcall *GSSetShaderResources)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *GSSetSamplers)(ID3D11DeviceContext* This, UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *OMSetRenderTargets)(ID3D11DeviceContext* This, UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, ID3D11DepthStencilView *pDepthStencilView);
  void (__stdcall *OMSetRenderTargetsAndUnorderedAccessViews)(ID3D11DeviceContext* This, UINT NumRTVs, ID3D11RenderTargetView *const *ppRenderTargetViews,
    ID3D11DepthStencilView *pDepthStencilView, UINT UAVStartSlot, UINT NumUAVs, ID3D11UnorderedAccessView *const *ppUnorderedAccessViews, const UINT *pUAVInitialCounts);
  void (__stdcall *OMSetBlendState)(ID3D11DeviceContext* This, ID3D11BlendState *pBlendState, const float BlendFactor[4], UINT SampleMask);
  void (__stdcall *OMSetDepthStencilState)(ID3D11DeviceContext* This, ID3D11DepthStencilState *pDepthStencilState, UINT StencilRef);
  void (__stdcall *SOSetTargets)(ID3D11DeviceContext* This, UINT NumBuffers, ID3D11Buffer *const *ppSOTargets, const UINT *pOffsets);
  void (__stdcall *DrawAuto)(ID3D11DeviceContext* This);
  void (__stdcall *DrawIndexedInstancedIndirect)(ID3D11DeviceContext* This, ID3D11Buffer *pBufferForArgs, UINT AlignedByteOffsetForArgs);
  void (__stdcall *DrawInstancedIndirect)(ID3D11DeviceContext* This, ID3D11Buffer *pBufferForArgs, UINT AlignedByteOffsetForArgs);
  void (__stdcall *Dispatch)(ID3D11DeviceContext* This, UINT ThreadGroupCountX, UINT ThreadGroupCountY, UINT ThreadGroupCountZ);
  void (__stdcall *DispatchIndirect)(ID3D11DeviceContext* This, ID3D11Buffer *pBufferForArgs, UINT AlignedByteOffsetForArgs);
  void (__stdcall *RSSetState)(ID3D11DeviceContext* This, ID3D11RasterizerState *pRasterizerState);
  void (__stdcall *RSSetViewports)(ID3D11DeviceContext* This, UINT NumViewports, const D3D11_VIEWPORT *pViewports);
  void (__stdcall *RSSetScissorRects)(ID3D11DeviceContext* This, UINT NumRects, const D3D11_RECT *pRects);
  // On the GPU, async
  void (__stdcall *CopySubresourceRegion)(ID3D11DeviceContext* This, void /*ID3D11Resource*/ *pDstResource, UINT DstSubresource, UINT DstX, UINT DstY, UINT DstZ, void /*ID3D11Resource*/ *pSrcResource, UINT SrcSubresource, const D3D11_BOX *pSrcBox);
  void (__stdcall *CopyResource)(ID3D11DeviceContext* This, void /*ID3D11Resource*/ *pDstResource, void /*ID3D11Resource*/ *pSrcResource);
  // On the CPU, sync
  void (__stdcall *UpdateSubresource)(ID3D11DeviceContext* This, void /*ID3D11Resource*/ *pDstResource, UINT DstSubresource, const D3D11_BOX *pDstBox, const void *pSrcData, UINT SrcRowPitch, UINT SrcDepthPitch);
  void (__stdcall *CopyStructureCount)(ID3D11DeviceContext* This,ID3D11Buffer *pDstBuffer,UINT DstAlignedByteOffset,ID3D11UnorderedAccessView *pSrcView);
  void (__stdcall *ClearRenderTargetView)(ID3D11DeviceContext* This,ID3D11RenderTargetView *pRenderTargetView,const float* ColorRGBA); // 4 x float
  void (__stdcall *ClearUnorderedAccessViewUint)(ID3D11DeviceContext* This,ID3D11UnorderedAccessView *pUnorderedAccessView,const UINT Values[4]);
  void (__stdcall *ClearUnorderedAccessViewFloat)(ID3D11DeviceContext* This,ID3D11UnorderedAccessView *pUnorderedAccessView,const float Values[4]);
  void (__stdcall *ClearDepthStencilView)(ID3D11DeviceContext* This,ID3D11DepthStencilView *pDepthStencilView,UINT ClearFlags,float Depth,uint8_t Stencil);
  void (__stdcall *GenerateMips)(ID3D11DeviceContext* This,ID3D11ShaderResourceView *pShaderResourceView);
  void (__stdcall *SetResourceMinLOD)(ID3D11DeviceContext* This,ID3D11Resource *pResource,float MinLOD);
  float (__stdcall *GetResourceMinLOD)(ID3D11DeviceContext* This,ID3D11Resource *pResource);
  void (__stdcall *ResolveSubresource)(ID3D11DeviceContext* This,ID3D11Resource *pDstResource,UINT DstSubresource,ID3D11Resource *pSrcResource,UINT SrcSubresource,DXGI_FORMAT Format);
  void (__stdcall *ExecuteCommandList)(ID3D11DeviceContext* This,ID3D11CommandList *pCommandList,BOOL RestoreContextState);
  void (__stdcall *HSSetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *HSSetShader)(ID3D11DeviceContext* This,ID3D11Shader *pHullShader, ID3D11ClassInstance *const *ppClassInstances,UINT NumClassInstances);
  void (__stdcall *HSSetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *HSSetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *DSSetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *DSSetShader )(ID3D11DeviceContext* This,ID3D11Shader *pDomainShader,ID3D11ClassInstance *const *ppClassInstances,UINT NumClassInstances);
  void (__stdcall *DSSetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *DSSetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *CSSetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView *const *ppShaderResourceViews);
  void (__stdcall *CSSetUnorderedAccessViews)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumUAVs,ID3D11UnorderedAccessView *const *ppUnorderedAccessViews,const UINT *pUAVInitialCounts);
  void (__stdcall *CSSetShader)(ID3D11DeviceContext* This,ID3D11Shader *pComputeShader,ID3D11ClassInstance *const *ppClassInstances,UINT NumClassInstances);
  void (__stdcall *CSSetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState *const *ppSamplers);
  void (__stdcall *CSSetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer *const *ppConstantBuffers);
  void (__stdcall *VSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *PSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);
  void (__stdcall *PSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppPixelShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);
  void (__stdcall *PSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);
  void (__stdcall *VSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppVertexShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);
  void (__stdcall *PSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *IAGetInputLayout)(ID3D11DeviceContext* This,ID3D11InputLayout **ppInputLayout);
  void (__stdcall *IAGetVertexBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppVertexBuffers,UINT *pStrides,UINT *pOffsets);
  void (__stdcall *IAGetIndexBuffer)(ID3D11DeviceContext* This,ID3D11Buffer **pIndexBuffer,DXGI_FORMAT *Format,UINT *Offset);
  void (__stdcall *GSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *GSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppGeometryShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);
  void (__stdcall *IAGetPrimitiveTopology)(ID3D11DeviceContext* This,D3D11_PRIMITIVE_TOPOLOGY *pTopology);
  void (__stdcall *VSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);
  void (__stdcall *VSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);
  void (__stdcall *GetPredication)(ID3D11DeviceContext* This,ID3D11Predicate **ppPredicate,BOOL *pPredicateValue);
  void (__stdcall *GSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);
  void (__stdcall *GSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);
  void (__stdcall *OMGetRenderTargets)(ID3D11DeviceContext* This,UINT NumViews,ID3D11RenderTargetView **ppRenderTargetViews,ID3D11DepthStencilView **ppDepthStencilView);
  void (__stdcall *OMGetRenderTargetsAndUnorderedAccessViews)(ID3D11DeviceContext* This,UINT NumRTVs,ID3D11RenderTargetView **ppRenderTargetViews,ID3D11DepthStencilView **ppDepthStencilView, UINT UAVStartSlot,UINT NumUAVs,ID3D11UnorderedAccessView **ppUnorderedAccessViews);
  void (__stdcall *OMGetBlendState)(ID3D11DeviceContext* This,ID3D11BlendState **ppBlendState,float BlendFactor[4],UINT *pSampleMask);
  void (__stdcall *OMGetDepthStencilState)(ID3D11DeviceContext* This,ID3D11DepthStencilState **ppDepthStencilState,UINT *pStencilRef);
  void (__stdcall *SOGetTargets)(ID3D11DeviceContext* This,UINT NumBuffers,ID3D11Buffer **ppSOTargets);
  void (__stdcall *RSGetState)(ID3D11DeviceContext* This,ID3D11RasterizerState **ppRasterizerState);
  void (__stdcall *RSGetViewports)(ID3D11DeviceContext* This,UINT *pNumViewports,D3D11_VIEWPORT *pViewports);
  void (__stdcall *RSGetScissorRects)(ID3D11DeviceContext* This,UINT *pNumRects,D3D11_RECT *pRects);
  void (__stdcall *HSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);
  void (__stdcall *HSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppHullShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);
  void (__stdcall *HSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);
  void (__stdcall *HSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *DSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);
  void (__stdcall *DSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppDomainShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);
  void (__stdcall *DSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);
  void (__stdcall *DSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *CSGetShaderResources)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumViews,ID3D11ShaderResourceView **ppShaderResourceViews);       
  void (__stdcall *CSGetUnorderedAccessViews)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumUAVs,ID3D11UnorderedAccessView **ppUnorderedAccessViews);       
  void (__stdcall *CSGetShader)(ID3D11DeviceContext* This,ID3D11Shader **ppComputeShader,ID3D11ClassInstance **ppClassInstances,UINT *pNumClassInstances);       
  void (__stdcall *CSGetSamplers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumSamplers,ID3D11SamplerState **ppSamplers);      
  void (__stdcall *CSGetConstantBuffers)(ID3D11DeviceContext* This,UINT StartSlot,UINT NumBuffers,ID3D11Buffer **ppConstantBuffers);
  void (__stdcall *ClearState)(ID3D11DeviceContext* This);
  void (__stdcall *Flush)(ID3D11DeviceContext* This);
  D3D11_DEVICE_CONTEXT_TYPE (__stdcall *GetType)(ID3D11DeviceContext* This);
  UINT (__stdcall *GetContextFlags)(ID3D11DeviceContext* This);
  HRESULT (__stdcall *FinishCommandList)(ID3D11DeviceContext* This,BOOL RestoreDeferredContextState,ID3D11CommandList **ppCommandList);
} ID3D11DeviceContextVtbl;

struct _ID3D11DeviceContext {
  const ID3D11DeviceContextVtbl* lpVtbl;
};
]])
--------------------------------------------------------------------------------
--
-- ID3D11RenderTargetView
--
--------------------------------------------------------------------------------
local ID3D11RenderTargetView = {}
setmetatable(ID3D11RenderTargetView, D3D.ID3D11ViewMT)

function ID3D11RenderTargetView:getDesc()
  local viewDesc = ffi.new('D3D11_RENDER_TARGET_VIEW_DESC')
  self.lpVtbl.GetDesc(self, viewDesc)
  return viewDesc
end

local ID3D11RenderTargetViewMT = {__index = ID3D11RenderTargetView}
ffi.metatype('ID3D11RenderTargetView', ID3D11RenderTargetViewMT)
--------------------------------------------------------------------------------
--
-- ID3D11DepthStencilView
--
--------------------------------------------------------------------------------
local ID3D11DepthStencilView = {}
setmetatable(ID3D11DepthStencilView, D3D.ID3D11ViewMT)

function ID3D11DepthStencilView:getDesc()
  local viewDesc = ffi.new('D3D11_DEPTH_STENCIL_VIEW_DESC')
  self.lpVtbl.GetDesc(self, viewDesc)
  return viewDesc
end

local ID3D11DepthStencilViewMT = {__index = ID3D11DepthStencilView}
ffi.metatype('ID3D11DepthStencilView', ID3D11DepthStencilViewMT)
---------------------------------------------------------------------------------
--
-- ID3D11RasterizerState
--
---------------------------------------------------------------------------------
local ID3D11RasterizerState = {}
setmetatable(ID3D11RasterizerState, D3D.ID3D11DeviceChildMT)

function ID3D11RasterizerState:getDesc()
  local rasterDesc = ffi.new('D3D11_RASTERIZER_DESC')
  self.lpVtbl.GetDesc(self, rasterDesc)
  return rasterDesc
end

local ID3D11RasterizerStateMT = {__index = ID3D11RasterizerState}
ffi.metatype('ID3D11RasterizerState', ID3D11RasterizerStateMT)
---------------------------------------------------------------------------------
--
-- ID3D11DepthStencilState
--
---------------------------------------------------------------------------------
local ID3D11DepthStencilState = {}
setmetatable(ID3D11DepthStencilState, D3D.ID3D11DeviceChildMT)

function ID3D11DepthStencilState:getDesc()
  local desc = ffi.new('D3D11_DEPTH_STENCIL_DESC')
  self.lpVtbl.GetDesc(self, desc)
  return desc
end

local ID3D11DepthStencilStateMT = {__index = ID3D11DepthStencilState}
ffi.metatype('ID3D11DepthStencilState', ID3D11DepthStencilStateMT)
----------------------------------------------------------------------------------
--
-- ID3D11BlendState
--
----------------------------------------------------------------------------------
local ID3D11BlendState = {}
setmetatable(ID3D11BlendState, D3D.ID3D11DeviceChildMT)

function ID3D11BlendState:getDesc()
  local ret = ffi.new('D3D11_BLEND_DESC')
  self.lpVtbl.GetDesc(self, ret)
  return ret
end

local ID3D11BlendStateMT = {__index = ID3D11BlendState}
ffi.metatype('ID3D11BlendState', ID3D11BlendStateMT)
---------------------------------------------------------------------------------
--
-- ID3D11CommandList
--
---------------------------------------------------------------------------------
local ID3D11CommandList = {}
setmetatable(ID3D11CommandList, D3D.ID3D11DeviceChildMT)

function ID3D11CommandList:getContextFlags()
  return self.lpVtbl.GetContextFlags(self)
end

function ID3D11CommandList:execute(ctx)
  ctx.lpVtbl.ExecuteCommandList(ctx, self, false)
end

local ID3D11CommandListMT = {__index = ID3D11CommandList}
ffi.metatype('ID3D11CommandList',  ID3D11CommandListMT)
---------------------------------------------------------------------------------
--
-- ID3D11DeviceContext
--
---------------------------------------------------------------------------------
local ID3D11DeviceContext = {}
setmetatable(ID3D11DeviceContext, D3D.ID3D11DeviceChildMT)

ID3D11DeviceContext.GUID = ffi.new('GUID', {
  0xc0bfa96c, 0xe089, 0x44fb,
  {0x8e, 0xaf, 0x26, 0xf8, 0x79, 0x61, 0x90, 0xda}
})
--- Get context type
-- @return immediate or deferred. There's only one immediate context
function ID3D11DeviceContext:getType()
  if self.lpVtbl.GetType(self) == D3D.DEVICE_CONTEXT_IMMEDIATE then
    return 'immediate'
  end
  -- DEVICE_CONTEXT_DEFERRED
  return 'deferred'
end
--- Get context's rasterizer state as a lua-managed object
function ID3D11DeviceContext:getRasterizerState()
  local rasterState = ffi.new('ID3D11RasterizerState*[1]')
  self.lpVtbl.RSGetState(self, rasterState)
  if rasterState[0] == nil then
    return nil
  end
  return ffi.gc(rasterState[0], D3D.Finalize)
end

function ID3D11DeviceContext:setRasterizerState(state)
  self.lpVtbl.RSSetState(self, state)
end

function ID3D11DeviceContext:getDepthStencilState()
  local depthState = ffi.new('ID3D11DepthStencilState[1]')
  local stencilRef = ffi.new('UINT[1]')
  self.lpVtbl.OMGetDepthStencilState(self, depthState, stencilRef)
  if depthState[0] == nil then
    return nil
  end
  return ffi.gc(depthState[0], D3D.Finalize)
end

function ID3D11DeviceContext:setDepthStencilState(states, num)
  self.lpVtbl.OMSetDepthStencilState(self, states, num)
end
  
function ID3D11DeviceContext:setBlenderState(state, factor, mask)
  self.lpVtbl.OMSetBlendState(self, state, factor, mask or 0xffffffff)
end

function ID3D11DeviceContext:unmap(resource, subresource)
  assert(resource ~= nil, "Cannot unmap nil resource")
  self.lpVtbl.Unmap(self, resource, subresource)
end

function ID3D11DeviceContext:setViewports(num, viewports)
  self.lpVtbl.RSSetViewports(self, num, viewports)
end

function ID3D11DeviceContext:setScissors(num, rects)
  self.lpVtbl.RSSetScissorRects(self, num, rects)
end

function ID3D11DeviceContext:getFlags()
  return self.lpVtbl.GetContextFlags(self)
end

function ID3D11DeviceContext:clearState()
  self.lpVtbl.ClearState(self)
end

function ID3D11DeviceContext:setLayout(layout)
  assert(layout, "required parameter cannot be nil")
  -- The method will hold a reference to the interfaces passed in. 
  -- This differs from the device state behavior in Direct3D 10. 
  self.lpVtbl.IASetInputLayout(self, layout)
end

-----------------------------------------------------------------------------
--
-- store all issued commands into a command list
--
-----------------------------------------------------------------------------
function ID3D11DeviceContext:finishCommandList()
  local cmdList = ffi.new('ID3D11CommandList*[1]')
  local hres = self.lpVtbl.FinishCommandList(self, false, cmdList)
  if hres ~= 0 then
    error(string.format("Failed to create counter: %s", D3D.GetError(hres)))
  end
  return ffi.gc(cmdList[0], D3D.Finalize)
end

local ID3D11DeviceContextMT = {__index = ID3D11DeviceContext}
return ffi.metatype('ID3D11DeviceContext', ID3D11DeviceContextMT)
