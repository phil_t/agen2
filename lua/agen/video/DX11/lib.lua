local win32 = require('agen.system.win32')
local ffi = win32.ffi
local lib = ffi.load('d3d11')

ffi.cdef([[
static const UINT SDK_VERSION = 7;

typedef enum _DXGI_FORMAT {
	FORMAT_UNKNOWN             = 0,

	FORMAT_R32G32B32A32_FLOAT	 = 2,
	FORMAT_R32G32B32_FLOAT		 = 6,
	FORMAT_R32G32_FLOAT				 = 16,
  FORMAT_D32_FLOAT_S8X24_UINT = 20,

  FORMAT_R8G8B8A8_UNORM      = 28,
  FORMAT_R8G8B8A8_UNORM_SRGB = 29,
  FORMAT_R8G8B8A8_UINT       = 30,

  FORMAT_R32_TYPELESS        = 39,
  FORMAT_D32_FLOAT           = 40,
	FORMAT_R32_FLOAT					 = 41,
	FORMAT_R32_UINT            = 42,

  FORMAT_R24G8_TYPELESS      = 44,
	FORMAT_D24_UNORM_S8_UINT   = 45,
  FORMAT_R24_UNORM_X8_TYPLESS= 46,
  FORMAT_X24_TYPELESS_G8_UINT= 47,
  
  FORMAT_R16_TYPELESS        = 53,
  FORMAT_R16_FLOAT           = 54,
	FORMAT_D16_UNORM           = 55,
  FORMAT_R16_UNORM           = 56,
  FORMAT_R16_UINT            = 57,

  FORMAT_R8_UINT             = 62,
  FORMAT_A8_UNORM            = 65,
  // DXTn compressed formats
  FORMAT_BC1_TYPELESS        = 70,
  FORMAT_BC1_UNORM           = 71,
  FORMAT_BC1_UNORM_SRGB      = 72,
  FORMAT_BC2_TYPELESS        = 73,
  FORMAT_BC2_UNORM           = 74,
  FORMAT_BC2_UNORM_SRGB      = 75,
  FORMAT_BC3_UNORM_TYPELESS  = 76,
  FORMAT_BC3_UNORM           = 77,
  FORMAT_BC3_UNORM_SRGB      = 78,
  // ATIn compressed formats
  FORMAT_BC4_TYPELESS        = 79,
  FORMAT_BC4_UNORM           = 80,
  FORMAT_BC4_SNORM           = 81,
  FORMAT_BC5_TYPELESS        = 82,
  FORMAT_BC5_UNORM           = 83,
  FORMAT_BC5_SNORM           = 84,

	FORMAT_B5G6R5_UNORM        = 85,
	FORMAT_B5G5R5A1_UNORM      = 86,
	FORMAT_B8G8R8A8_UNORM      = 87,
	FORMAT_B8G8R8X8_UNORM      = 88,

  FORMAT_B8G8R8A8_UNORM_SRGB = 91,
  FORMAT_B8G8R8X8_UNORM_SRGB = 93,
  // BPTC
  FORMAT_BC6H_TYPELESS       = 94,
  FORMAT_BC6H_UF16           = 95,
  FORMAT_BC6H_SF16           = 96,
  FORMAT_BC7_TYPELESS        = 97,
  FORMAT_BC7_UNORM           = 98,
  FORMAT_BC7_UNORM_SRGB      = 99,

	FORMAT_P8                  = 113,

	FORMAT_B4G4R4A4_UNORM      = 115,

  FORMAT_FORCE_UINT = 0xffffffffUL
} DXGI_FORMAT;

typedef enum _D3D11_FORMAT_SUPPORT { 
  FORMAT_SUPPORT_BUFFER                       = 0x1,
  FORMAT_SUPPORT_IA_VERTEX_BUFFER             = 0x2,
  FORMAT_SUPPORT_IA_INDEX_BUFFER              = 0x4,
  FORMAT_SUPPORT_SO_BUFFER                    = 0x8,
  FORMAT_SUPPORT_TEXTURE1D                    = 0x10,
  FORMAT_SUPPORT_TEXTURE2D                    = 0x20,
  FORMAT_SUPPORT_TEXTURE3D                    = 0x40,
  FORMAT_SUPPORT_TEXTURECUBE                  = 0x80,
  FORMAT_SUPPORT_SHADER_LOAD                  = 0x100,
  FORMAT_SUPPORT_SHADER_SAMPLE                = 0x200,
  FORMAT_SUPPORT_SHADER_SAMPLE_COMPARISON     = 0x400,
  FORMAT_SUPPORT_SHADER_SAMPLE_MONO_TEXT      = 0x800,
  FORMAT_SUPPORT_MIP                          = 0x1000,
  FORMAT_SUPPORT_MIP_AUTOGEN                  = 0x2000,
  FORMAT_SUPPORT_RENDER_TARGET                = 0x4000,
  FORMAT_SUPPORT_BLENDABLE                    = 0x8000,
  FORMAT_SUPPORT_DEPTH_STENCIL                = 0x10000,
  FORMAT_SUPPORT_CPU_LOCKABLE                 = 0x20000,
  FORMAT_SUPPORT_MULTISAMPLE_RESOLVE          = 0x40000,
  FORMAT_SUPPORT_DISPLAY                      = 0x80000,
  FORMAT_SUPPORT_CAST_WITHIN_BIT_LAYOUT       = 0x100000,
  FORMAT_SUPPORT_MULTISAMPLE_RENDERTARGET     = 0x200000,
  FORMAT_SUPPORT_MULTISAMPLE_LOAD             = 0x400000,
  FORMAT_SUPPORT_SHADER_GATHER                = 0x800000,
  FORMAT_SUPPORT_BACK_BUFFER_CAST             = 0x1000000,
  FORMAT_SUPPORT_TYPED_UNORDERED_ACCESS_VIEW  = 0x2000000,
  FORMAT_SUPPORT_SHADER_GATHER_COMPARISON     = 0x4000000,
  FORMAT_SUPPORT_DECODER_OUTPUT               = 0x8000000,
  FORMAT_SUPPORT_VIDEO_PROCESSOR_OUTPUT       = 0x10000000,
  FORMAT_SUPPORT_VIDEO_PROCESSOR_INPUT        = 0x20000000,
  FORMAT_SUPPORT_VIDEO_ENCODER                = 0x40000000
} D3D11_FORMAT_SUPPORT;

typedef enum _DXGI_USAGE {
	CPU_ACCESS_NONE 		= 0x00000000,
	CPU_ACCESS_DYNAMIC		= 0x00000001,
	CPU_ACCESS_READ_WRITE 	= 0x00000002,
	CPU_ACCESS_SCRATCH		= 0x00000003,
	CPU_ACCESS_FIELD        = 0x0000000F,
	USAGE_SHADER_INPUT      = 0x00000010,
	USAGE_RENDER_TARGET_OUTPUT	= 0x00000020,
	USAGE_BACK_BUFFER       = 0x00000040,
	USAGE_SHARED            = 0x00000080,
	USAGE_READ_ONLY         = 0x00000100,
	USAGE_DISCARD_ON_PRESENT= 0x00000200,
	USAGE_UNORDERED_ACCESS  = 0x00000400,
	USAGE_FORCE_UINT		= 0xffffffffUL
} DXGI_USAGE;

typedef enum _DXGI_PRESENT {
  PRESENT_DEFAULT             = 0x00000000,
  PRESENT_TEST                = 0x00000001,
  PRESENT_DO_NOT_SEQUENCE     = 0x00000002,
  PRESENT_RESTART             = 0x00000004,
  PRESENT_DO_NOT_WAIT         = 0x00000008,
  PRESENT_RESTRICT_TO_OUTPUT  = 0x00000010,
  PRESENT_STEREO_PREFER_RIGHT = 0x00000020,
  PRESENT_STEREO_TEMPORARY_MONO = 0x00000040,
  PRESENT_USE_DURATION        = 0x00000100,
} DXGI_PRESENT;

typedef enum _D3D11_RTV_DIMENSION { 
  RTV_DIMENSION_UNKNOWN           = 0,
  RTV_DIMENSION_BUFFER            = 1,
  RTV_DIMENSION_TEXTURE1D         = 2,
  RTV_DIMENSION_TEXTURE1DARRAY    = 3,
  RTV_DIMENSION_TEXTURE2D         = 4,
  RTV_DIMENSION_TEXTURE2DARRAY    = 5,
  RTV_DIMENSION_TEXTURE2DMS       = 6,
  RTV_DIMENSION_TEXTURE2DMSARRAY  = 7,
  RTV_DIMENSION_TEXTURE3D         = 8
} D3D11_RTV_DIMENSION;

typedef struct _DXGI_RATIONAL {
	UINT Numerator;
	UINT Denominator;
} DXGI_RATIONAL;

typedef struct _DXGI_SAMPLE_DESC {
  UINT Count;
  UINT Quality;
} DXGI_SAMPLE_DESC;

typedef enum _D3D11_MAP {	
  MAP_READ	             = 0x00000001,
  MAP_WRITE	             = 0x00000002,
  MAP_READ_WRITE	       = 0x00000003,
  MAP_WRITE_DISCARD	     = 0x00000004,
  MAP_WRITE_NO_OVERWRITE = 0x00000005
} D3D11_MAP;

typedef enum _D3D11_MAP_FLAG { 
  MAP_FLAG_DO_NOT_WAIT  = 0x00100000
} D3D11_MAP_FLAG;

typedef enum _D3D11_BIND_FLAG {	
  BIND_VERTEX_BUFFER	  = 0x00000001,
  BIND_INDEX_BUFFER	    = 0x00000002,
  BIND_CONSTANT_BUFFER	= 0x00000004,
  BIND_SHADER_RESOURCE	= 0x00000008,
  BIND_STREAM_OUTPUT	  = 0x00000010,
  BIND_RENDER_TARGET	  = 0x00000020,
  BIND_DEPTH_STENCIL	  = 0x00000040,
  BIND_UNORDERED_ACCESS	= 0x00000080
} D3D11_BIND_FLAG;

typedef enum _D3D11_DEVICE_CONTEXT_TYPE {	
  DEVICE_CONTEXT_IMMEDIATE	= 0x00000000,
  DEVICE_CONTEXT_DEFERRED	  = 1, 
} D3D11_DEVICE_CONTEXT_TYPE;
	
typedef enum _D3D11_FILL_MODE { 
  FILL_WIREFRAME  = 0x00000002,
  FILL_SOLID      = 0x00000003
} D3D11_FILL_MODE;

typedef enum _D3D11_CULL_MODE { 
  CULL_NONE   = 0x00000001,
  CULL_FRONT  = 0x00000002,
  CULL_BACK   = 0x00000003
} D3D11_CULL_MODE;

typedef enum _D3D11_COMPARISON_FUNC { 
  COMPARISON_NEVER          = 1,
  COMPARISON_LESS           = 2,
  COMPARISON_EQUAL          = 3,
  COMPARISON_LESS_EQUAL     = 4,
  COMPARISON_GREATER        = 5,
  COMPARISON_NOT_EQUAL      = 6,
  COMPARISON_GREATER_EQUAL  = 7,
  COMPARISON_ALWAYS         = 8
} D3D11_COMPARISON_FUNC;

typedef enum _D3D11_PRIMITIVE_TOPOLOGY {	
  PRIMITIVE_TOPOLOGY_UNDEFINED	= 0x00000000,
  PRIMITIVE_TOPOLOGY_POINTLIST	= 1,
  PRIMITIVE_TOPOLOGY_LINELIST	= 2,
  PRIMITIVE_TOPOLOGY_LINESTRIP	= 3,
  PRIMITIVE_TOPOLOGY_TRIANGLELIST	= 4,
  PRIMITIVE_TOPOLOGY_TRIANGLESTRIP	= 5,
  PRIMITIVE_TOPOLOGY_LINELIST_ADJ	= 10,
  PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ	= 11,
  PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ	= 12,
  PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ	= 13,
  PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST	= 33,
	// ...
  PRIMITIVE_TOPOLOGY_32_CONTROL_POINT_PATCHLIST	= 64
} D3D11_PRIMITIVE_TOPOLOGY;

typedef enum _D3D11_RESOURCE_DIMENSION {	
  RESOURCE_DIMENSION_UNKNOWN	= 0x00000000,
  RESOURCE_DIMENSION_BUFFER		= 1,
  RESOURCE_DIMENSION_TEXTURE1D	= 2,
  RESOURCE_DIMENSION_TEXTURE2D	= 3,
  RESOURCE_DIMENSION_TEXTURE3D	= 4
} D3D11_RESOURCE_DIMENSION;

typedef enum _D3D_FEATURE_LEVEL { 
  FEATURE_LEVEL_9_1   = 0x9100,
  FEATURE_LEVEL_9_2   = 0x9200,
  FEATURE_LEVEL_9_3   = 0x9300,
  FEATURE_LEVEL_10_0  = 0xa000,
  FEATURE_LEVEL_10_1  = 0xa100,
  FEATURE_LEVEL_11_0  = 0xb000,
  FEATURE_LEVEL_11_1  = 0xb100,
  FEATURE_LEVEL_DWORD = 0x0fffffff
} D3D_FEATURE_LEVEL;

typedef struct _D3D11_RECT {
  LONG left;
  LONG top;
  LONG right;
  LONG bottom;
} D3D11_RECT;

typedef struct _D3D11_VIEWPORT {
  float TopLeftX;
  float TopLeftY;
  float Width;
  float Height;
  float MinDepth;
  float MaxDepth;
} D3D11_VIEWPORT;

typedef struct _D3D11_MAPPED_SUBRESOURCE {
  uint8_t* pData;
  UINT RowPitch;
  UINT DepthPitch;
} D3D11_MAPPED_SUBRESOURCE;

typedef struct _D3D11_RASTERIZER_DESC {
  D3D11_FILL_MODE FillMode;
  D3D11_CULL_MODE CullMode;
  BOOL            FrontCounterClockwise;
  INT             DepthBias;
  float           DepthBiasClamp;
  float           SlopeScaledDepthBias;
  BOOL            DepthClipEnable;
  BOOL            ScissorEnable;
  BOOL            MultisampleEnable;
  BOOL            AntialiasedLineEnable;
} D3D11_RASTERIZER_DESC;

typedef struct _D3D11_BUFFER_RTV {
  union {
    UINT FirstElement;
    UINT ElementOffset;
  };
  union {
    UINT NumElements;
    UINT ElementWidth;
  };
} D3D11_BUFFER_RTV;

typedef struct _D3D11_TEX1D_RTV {
  UINT MipSlice;
} D3D11_TEX1D_RTV;

typedef struct _D3D11_TEX1D_ARRAY_RTV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX1D_ARRAY_RTV;

typedef struct _D3D11_TEX2D_RTV {
  UINT MipSlice;
} D3D11_TEX2D_RTV;

typedef struct _D3D11_TEX2D_ARRAY_RTV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2D_ARRAY_RTV;

typedef struct _D3D11_TEX2DMS_ARRAY_RTV {
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2DMS_ARRAY_RTV;

typedef struct _D3D11_TEX2DMS_RTV {
  UINT UnusedField_NothingToDefine;
} D3D11_TEX2DMS_RTV;

typedef struct _D3D11_TEX3D_RTV {
  UINT MipSlice;
  UINT FirstWSlice;
  UINT WSize;
} D3D11_TEX3D_RTV;

typedef struct _D3D11_SUBRESOURCE_DATA {
  const void *pSysMem;
  UINT       SysMemPitch;
  UINT       SysMemSlicePitch;
} D3D11_SUBRESOURCE_DATA;

typedef struct _ID3D11Device ID3D11Device;
/*
 * ID3D11DeviceChild interface
 */
typedef struct _ID3D11DeviceChildVtbl {
  HRESULT (__stdcall *QueryInterface)(void* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(void* This);
  ULONG (__stdcall *Release)(void* This);
  void (__stdcall *GetDevice)(void* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(void* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(void* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(void* This, REFGUID guid, const IUnknown *pData);
} ID3D11DeviceChildVtbl;

typedef struct _ID3D11DeviceChild {
  const ID3D11DeviceChildVtbl* lpVtbl;
} ID3D11DeviceChild;
/*
 * ID3D11Resource interface
 */
typedef struct _ID3D11ResourceVtbl {
  HRESULT (__stdcall *QueryInterface)(void* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(void* This);
  ULONG (__stdcall *Release)(void* This);
  
  void (__stdcall *GetDevice)(void* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(void* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(void* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(void* This, REFGUID guid, const IUnknown *pData);
  
  void (__stdcall *GetType)(void* This, D3D11_RESOURCE_DIMENSION* pResourceDimension);
  void (__stdcall *SetEvictionPriority)(void* This, UINT EvictionPriority);
  UINT (__stdcall *GetEvictionPriority)(void* This);
} ID3D11ResourceVtbl;

typedef struct _ID3D11Resource {
  const ID3D11ResourceVtbl* lpVtbl;
} ID3D11Resource;
/*
 * ID3D11View interface
 */
typedef struct _ID3D11ViewVtbl {
  HRESULT (__stdcall *QueryInterface)(void* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(void* This);
  ULONG (__stdcall *Release)(void* This);
  void (__stdcall *GetDevice)(void* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(void* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(void* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(void* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetResource)(void* This, void** ppResource);
} ID3D11ViewVtbl;

typedef struct _ID3D11View {
  const ID3D11ViewVtbl* lpVtbl;
} ID3D11View;
/**************************************************************
 *
 * ID3D11Asynchronous
 *
 **************************************************************/
typedef struct _ID3D11Asynchronous  ID3D11Asynchronous;
typedef struct _ID3D11AsynchronousVtbl {
  HRESULT (__stdcall *QueryInterface)(void* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(void* This);
  ULONG (__stdcall *Release)(void* This);
  void (__stdcall *GetDevice)(ID3D11Asynchronous* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Asynchronous* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Asynchronous* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Asynchronous* This, REFGUID guid, const IUnknown *pData);
  UINT (__stdcall *GetDataSize)(ID3D11Asynchronous* This);
} ID3D11AsynchronousVtbl;

struct _ID3D11Asynchronous {
  const ID3D11AsynchronousVtbl* lpVtbl;
};
]])

local d3d11 = {}
d3d11.ffi = ffi
d3d11.GetError = win32.GetErrorString
d3d11.Finalize = win32.finalizerCOM
d3d11.IsWindow = win32.IsWindow
---------------------------------------------------------------------------------------
--
-- shared metatables
--
---------------------------------------------------------------------------------------
d3d11.IUnknownMT = win32.IUnknownMT
d3d11.GUID_DebugObjectName = ffi.new('GUID', {
  0x429b8c22, 0x9188, 0x4b0c,
  {0x87, 0x42, 0xac, 0xb0, 0xbf, 0x85, 0xc2, 0x00},
})
---------------------------------------------------------------------------------------
--
-- ID3D11DeviceChildMT
--
---------------------------------------------------------------------------------------
local ID3D11DeviceChild = {}
setmetatable(ID3D11DeviceChild, win32.IUnknownMT)
--- Get ID3D11Device interface from ID3D11DeviceChild object
-- @return lua managed ID3D11Device object
function ID3D11DeviceChild:getDevice()
  local device = ffi.new('ID3D11Device*[1]')
  self.lpVtbl.GetDevice(self, device)
  assert(device[0] ~= nil, "ID3D11DeviceChild parent device is nil!")
  return ffi.gc(device[0], d3d11.Finalize)
end
--- Get immediate ID3D11Context interface from ID3D11DeviceChild's device object.
-- @return lua managed ID3D11Context object
function ID3D11DeviceChild:getImmediateContext()
  local device = ffi.new('ID3D11Device*[1]')
  self.lpVtbl.GetDevice(self, device)
  assert(device[0] ~= nil, "ID3D11DeviceChild parent device is nil!")
  local ctx = device[0]:getImmediateContext()
  -- context keeps ref
  device[0]:release()
  return ctx
end
--- Associate any pointer with COM object
-- @param GUID of data
-- @param size data size in bytes
-- @param data pointer
function ID3D11DeviceChild:setPrivateData(guid, size, data)
  assert(guid ~= nil, "GUI not set")
  assert(ffi.istype('GUID', guid), "Invalid argument #2, GUID expected")
  assert(size > 0, "Size value invalid")
  assert(data ~= nil, "Data cannot be nil")

  local hres = self.lpVtbl.SetPrivateData(self, guid, size, data)
  if hres ~= 0 then
    error(string.format("Failed to set private data: [%s]", d3d11.GetError(hres)))
  end
end
--- Associate COM object with this COM object
-- @param GUID of object
-- @param object ptr
function ID3D11DeviceChild:setPrivateDataInterface(guid, data)
  assert(guid ~= nil, "GUI not set")
  assert(ffi.istype('GUID', guid), "Invalid argument #2, GUID expected")
  assert(data ~= nil, "Data cannot be nil")
  assert(ffi.istype('IUnknown', data), "Invalid argument #3, IUnknown expected")

  local hres = self.lpVtbl.SetPrivateDataInterface(self, guid, data)
  if hres ~= 0 then
    error(string.format("Failed to set private interface data: [%s]", d3d11.GetError(hres)))
  end
end
--- get private data stored in the object. Will copy valueSize bytes to value.
-- @param GUID data GUID
-- @param valueSize size of data in bytes
-- @param value data destination pointer
function ID3D11DeviceChild:getPrivateData(guid, valueSize, value)
  assert(guid ~= nil, "GUI not set")
  assert(ffi.istype('GUID', guid), "Invalid argument #2, GUID expected")
  assert(valueSize ~= nil and valueSize[0] > 0, "Size value invalid")
  assert(value ~= nil, "value cannot be nil")
  
  local hres  = self.lpVtbl.GetPrivateData(self, guid, valueSize, value)
  -- ok not to exist
  if hres ~= 0 and hres ~= 0x887a0002 then
    error(string.format("Failed to get private data: [%s]", d3d11.GetError(hres)))
  end
end

d3d11.ID3D11DeviceChildMT = {__index = ID3D11DeviceChild}
ffi.metatype('ID3D11DeviceChild', d3d11.ID3D11DeviceChildMT)
--------------------------------------------------------------------------------------------
--
-- ID3D11ResourceMT
--
--------------------------------------------------------------------------------------------
local ID3D11Resource = {}
setmetatable(ID3D11Resource, d3d11.ID3D11DeviceChildMT)

function ID3D11Resource:getType()
  local dim = ffi.new('D3D11_RESOURCE_DIMENSION')
  self.lpVtbl.GetType(self, dim)
  return dim
end

d3d11.ID3D11ResourceMT = {__index = ID3D11Resource}
ffi.metatype('ID3D11Resource', d3d11.ID3D11ResourceMT)
--------------------------------------------------------------------------------------------
--
-- ID3D11ViewMT
--
--------------------------------------------------------------------------------------------
local ID3D11View = {}
setmetatable(ID3D11View, d3d11.ID3D11DeviceChildMT)

--- Get view's underlying resource object (lua managed)
function ID3D11View:getResource()
  local res = ffi.new('void*[1]')
  self.lpVtbl.GetResource(self, res)
  if res[0] == nil then
    error("Failed to get resource from view")
  end
  return ffi.gc(res[0], d3d11.Finalize)
end

d3d11.ID3D11ViewMT = {__index = ID3D11View}
ffi.metatype('ID3D11View', d3d11.ID3D11ViewMT)
--------------------------------------------------------------------------------------------
--
-- ID3D11Asynchronous
--
--------------------------------------------------------------------------------------------
local ID3D11Asynchronous = {}
setmetatable(ID3D11Asynchronous, d3d11.ID3D11DeviceChildMT)

function ID3D11Asynchronous:getDataSize()
  return self.lpVtbl.GetDataSize(self)
end

d3d11.ID3D11AsynchronousMT = {__index = ID3D11Asynchronous}
ffi.metatype('ID3D11Asynchronous', d3d11.ID3D11AsynchronousMT)
--------------------------------------------------------------------------------------------
--
-- D3D11 lib
--
--------------------------------------------------------------------------------------------
local d3d11MT = {__index = lib}
return setmetatable(d3d11, d3d11MT)
