local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_COUNTER { 
  COUNTER_DEVICE_DEPENDENT_0  = 0x40000000
} D3D11_COUNTER;

typedef enum _D3D11_COUNTER_TYPE { 
  COUNTER_TYPE_FLOAT32  = 0,
  COUNTER_TYPE_UINT16   = 1,
  COUNTER_TYPE_UINT32   = 2,
  COUNTER_TYPE_UINT64   = 3
} D3D11_COUNTER_TYPE;

typedef enum _D3D11_QUERY { 
  // Determines whether or not the GPU is finished processing commands. When the GPU is finished processing 
  // commands ID3D11DeviceContext::GetData will return S_OK, and pData will point to a BOOL with a value of 
  // TRUE. When using this type of query, ID3D11DeviceContext::Begin is disabled.
  QUERY_EVENT                          = 0,
  QUERY_OCCLUSION                      = 1,
  QUERY_TIMESTAMP                      = 2,
  QUERY_TIMESTAMP_DISJOINT             = 3,
  QUERY_PIPELINE_STATISTICS            = 4,
  QUERY_OCCLUSION_PREDICATE            = 5,
  QUERY_SO_STATISTICS                  = 6,
  QUERY_SO_OVERFLOW_PREDICATE          = 7,
  QUERY_SO_STATISTICS_STREAM0          = 8, 
  QUERY_SO_OVERFLOW_PREDICATE_STREAM0  = 9,
  QUERY_SO_STATISTICS_STREAM1          = 10,
  QUERY_SO_OVERFLOW_PREDICATE_STREAM1  = 11,
  QUERY_SO_STATISTICS_STREAM2          = 12,
  QUERY_SO_OVERFLOW_PREDICATE_STREAM2  = 13,
  QUERY_SO_STATISTICS_STREAM3          = 14,
  QUERY_SO_OVERFLOW_PREDICATE_STREAM3  = 15
} D3D11_QUERY;

typedef enum _D3D11_QUERY_MISC_FLAG { 
  QUERY_MISC_PREDICATEHINT  = 0x1
} D3D11_QUERY_MISC_FLAG;

typedef struct _D3D11_COUNTER_INFO {
  D3D11_COUNTER LastDeviceDependentCounter;
  UINT          NumSimultaneousCounters;
  UINT8         NumDetectableParallelUnits;
} D3D11_COUNTER_INFO;

typedef struct _D3D11_COUNTER_DESC {
  D3D11_COUNTER Counter;
  UINT          MiscFlags;
} D3D11_COUNTER_DESC;

typedef struct _D3D11_QUERY_DESC {
  D3D11_QUERY Query;
  UINT        MiscFlags;
} D3D11_QUERY_DESC;
/*
 * Query results
 */
typedef struct _D3D11_QUERY_DATA_PIPELINE_STATISTICS {
  UINT64 IAVertices;
  UINT64 IAPrimitives;
  UINT64 VSInvocations;
  UINT64 GSInvocations;
  UINT64 GSPrimitives;
  UINT64 CInvocations;
  UINT64 CPrimitives;
  UINT64 PSInvocations;
  UINT64 HSInvocations;
  UINT64 DSInvocations;
  UINT64 CSInvocations;
} D3D11_QUERY_DATA_PIPELINE_STATISTICS;

typedef struct _D3D11_QUERY_DATA_SO_STATISTICS {
  UINT64 NumPrimitivesWritten;
  UINT64 PrimitivesStorageNeeded;
} D3D11_QUERY_DATA_SO_STATISTICS;

typedef struct _ID3D11Counter ID3D11Counter;
typedef struct _ID3D11CounterVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11Counter* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Counter* This);
  ULONG (__stdcall *Release)(ID3D11Counter* This);
  void (__stdcall *GetDevice)(ID3D11Counter* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Counter* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Counter* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Counter* This, REFGUID guid, const IUnknown *pData);
  UINT (__stdcall *GetDataSize)(ID3D11Counter* This);
  void (__stdcall *GetDesc)(ID3D11Counter* This, D3D11_COUNTER_DESC *pDesc);
} ID3D11CounterVtbl;

struct _ID3D11Counter {
  const ID3D11CounterVtbl* lpVtbl;
};
/*
 * A query interface queries information from the GPU.
 */
typedef struct _ID3D11Query ID3D11Query;
typedef struct _ID3D11QueryVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11Query* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Query* This);
  ULONG (__stdcall *Release)(ID3D11Query* This);
  void (__stdcall *GetDevice)(ID3D11Query* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Query* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Query* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Query* This, REFGUID guid, const IUnknown *pData);
  UINT (__stdcall *GetDataSize)(ID3D11Query* This);
  void (__stdcall *GetDesc)(ID3D11Query* This, D3D11_QUERY_DESC* pDesc);
} ID3D11QueryVtbl;

struct _ID3D11Query {
  const ID3D11QueryVtbl* lpVtbl;
};

typedef struct _ID3D11Predicate ID3D11Predicate;
]])
--------------------------------------------------------------
--
-- ID3D11Counter
--
--------------------------------------------------------------
local ID3D11Counter = {}
setmetatable(ID3D11Counter, D3D.ID3D11AsynchronousMT)

ID3D11Counter.GUID = ffi.new('GUID', {
  0x6e8c49fb,
  0xa371, 0x4770,
  {0xb4, 0x40, 0x29, 0x08, 0x60, 0x22, 0xb7, 0x41}
})

function ID3D11Counter:getDesc()
  local desc = ffi.new('D3D11_COUNTER_DESC')
  self.lpVtbl.GetDesc(self, desc)
  return desc
end

local ID3D11CounterMT = {__index = ID3D11Counter}
ffi.metatype('ID3D11Counter', ID3D11CounterMT)
---------------------------------------------------------------
--
-- ID3D11Query
--
---------------------------------------------------------------
local ID3D11Query = {}
setmetatable(ID3D11Query, D3D.ID3D11AsynchronousMT)

function ID3D11Query:getDesc()
  local desc = ffi.new('D3D11_QUERY_DESC')
  self.lpVtbl.GetDesc(self, desc)
  return desc
end

local ID3D11QueryMT = {__index = ID3D11Query}
return ffi.metatype('ID3D11Query', ID3D11QueryMT)
