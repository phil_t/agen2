local bit = require('bit')
local string = require('string')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_RLDO_FLAGS { 
  RLDO_SUMMARY          = 0x1,
  RLDO_DETAIL           = 0x2,
  RLDO_IGNORE_INTERNAL  = 0x4
} D3D11_RLDO_FLAGS;

typedef enum _D3D11_MESSAGE_CATEGORY {	
  MESSAGE_CATEGORY_APPLICATION_DEFINED	= 0,
	MESSAGE_CATEGORY_MISCELLANEOUS	= 1,
	MESSAGE_CATEGORY_INITIALIZATION	= 2,
	MESSAGE_CATEGORY_CLEANUP	= 3,
	MESSAGE_CATEGORY_COMPILATION	= 4,
	MESSAGE_CATEGORY_STATE_CREATION	= 5,
	MESSAGE_CATEGORY_STATE_SETTING	= 6,
	MESSAGE_CATEGORY_STATE_GETTING	= 7,
	MESSAGE_CATEGORY_RESOURCE_MANIPULATION	= 8,
	MESSAGE_CATEGORY_EXECUTION	= 9 
} D3D11_MESSAGE_CATEGORY;

typedef enum _D3D11_MESSAGE_SEVERITY {	
  MESSAGE_SEVERITY_CORRUPTION	= 0,
	MESSAGE_SEVERITY_ERROR	    = 1,
	MESSAGE_SEVERITY_WARNING	  = 2,
	MESSAGE_SEVERITY_INFO	      = 3 
} D3D11_MESSAGE_SEVERITY;

typedef enum _D3D11_MESSAGE_ID {	
  MESSAGE_ID_UNKNOWN	= 0,
} D3D11_MESSAGE_ID;

typedef struct _D3D11_MESSAGE {
  D3D11_MESSAGE_CATEGORY Category;
  D3D11_MESSAGE_SEVERITY Severity;
  D3D11_MESSAGE_ID ID;
  const char *pDescription;
  SIZE_T DescriptionByteLength;
} D3D11_MESSAGE;

typedef struct _D3D11_INFO_QUEUE_FILTER_DESC {
  UINT NumCategories;
  D3D11_MESSAGE_CATEGORY *pCategoryList;
  UINT NumSeverities;
  D3D11_MESSAGE_SEVERITY *pSeverityList;
  UINT NumIDs;
  D3D11_MESSAGE_ID *pIDList;
} D3D11_INFO_QUEUE_FILTER_DESC;

typedef struct _D3D11_INFO_QUEUE_FILTER {
  D3D11_INFO_QUEUE_FILTER_DESC AllowList;
  D3D11_INFO_QUEUE_FILTER_DESC DenyList;
} D3D11_INFO_QUEUE_FILTER;
    
typedef struct _ID3D11Debug ID3D11Debug;
typedef struct _ID3D11DebugVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11Debug* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Debug* This);
  ULONG (__stdcall *Release)(ID3D11Debug* This);
  HRESULT (__stdcall *SetFeatureMask)(ID3D11Debug* This, UINT Mask);
  UINT (__stdcall *GetFeatureMask)(ID3D11Debug* This);
  HRESULT (__stdcall *SetPresentPerRenderOpDelay)(ID3D11Debug* This, UINT Milliseconds);
  UINT (__stdcall *GetPresentPerRenderOpDelay)(ID3D11Debug* This);
  HRESULT (__stdcall *SetSwapChain)(ID3D11Debug* This, IDXGISwapChain *pSwapChain);
  HRESULT (__stdcall *GetSwapChain)(ID3D11Debug* This, IDXGISwapChain **ppSwapChain);
  HRESULT (__stdcall *ValidateContext)(ID3D11Debug* This, ID3D11DeviceContext *pContext);
  HRESULT (__stdcall *ReportLiveDeviceObjects)(ID3D11Debug* This, D3D11_RLDO_FLAGS Flags);
  HRESULT (__stdcall *ValidateContextForDispatch)(ID3D11Debug* This, ID3D11DeviceContext *pContext);
} ID3D11DebugVtbl;

struct _ID3D11Debug {
  const ID3D11DebugVtbl* lpVtbl;
};

typedef struct _ID3D11InfoQueue ID3D11InfoQueue;
typedef struct ID3D11InfoQueueVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11InfoQueue* This, REFIID riid, void **ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11InfoQueue* This);
  ULONG (__stdcall *Release)(ID3D11InfoQueue* This); 
  HRESULT (__stdcall *SetMessageCountLimit)(ID3D11InfoQueue* This, UINT64 MessageCountLimit);
  void (__stdcall *ClearStoredMessages)(ID3D11InfoQueue* This); 
  HRESULT (__stdcall *GetMessage)(ID3D11InfoQueue* This, UINT64 MessageIndex, D3D11_MESSAGE *pMessage, SIZE_T *pMessageByteLength); 
  UINT64 (__stdcall *GetNumMessagesAllowedByStorageFilter)(ID3D11InfoQueue* This); 
  UINT64 (__stdcall *GetNumMessagesDeniedByStorageFilter)(ID3D11InfoQueue* This); 
  UINT64 (__stdcall *GetNumStoredMessages)(ID3D11InfoQueue* This); 
  UINT64 (__stdcall *GetNumStoredMessagesAllowedByRetrievalFilter)(ID3D11InfoQueue* This); 
  UINT64 (__stdcall *GetNumMessagesDiscardedByMessageCountLimit)(ID3D11InfoQueue* This);  
  UINT64 (__stdcall *GetMessageCountLimit)(ID3D11InfoQueue* This);  
  HRESULT (__stdcall *AddStorageFilterEntries)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter); 
  HRESULT (__stdcall *GetStorageFilter)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter, SIZE_T *pFilterByteLength);  
  void (__stdcall *ClearStorageFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushEmptyStorageFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushCopyOfStorageFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushStorageFilter)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter);
  void (__stdcall *PopStorageFilter)(ID3D11InfoQueue* This);
  UINT (__stdcall *GetStorageFilterStackSize)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *AddRetrievalFilterEntries)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter);
  HRESULT (__stdcall *GetRetrievalFilter)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter, SIZE_T *pFilterByteLength);
  void (__stdcall *ClearRetrievalFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushEmptyRetrievalFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushCopyOfRetrievalFilter)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *PushRetrievalFilter)(ID3D11InfoQueue* This, D3D11_INFO_QUEUE_FILTER *pFilter);
  void (__stdcall *PopRetrievalFilter)(ID3D11InfoQueue* This);
  UINT (__stdcall *GetRetrievalFilterStackSize)(ID3D11InfoQueue* This);
  HRESULT (__stdcall *AddMessage)(ID3D11InfoQueue* This, D3D11_MESSAGE_CATEGORY Category, D3D11_MESSAGE_SEVERITY Severity,
    D3D11_MESSAGE_ID ID, LPCSTR pDescription);
  HRESULT (__stdcall *AddApplicationMessage)(ID3D11InfoQueue* This, D3D11_MESSAGE_SEVERITY Severity, LPCSTR pDescription);
  HRESULT (__stdcall *SetBreakOnCategory)(ID3D11InfoQueue* This, D3D11_MESSAGE_CATEGORY Category, BOOL bEnable);
  HRESULT (__stdcall *SetBreakOnSeverity)(ID3D11InfoQueue* This, D3D11_MESSAGE_SEVERITY Severity, BOOL bEnable);
  HRESULT (__stdcall *SetBreakOnID)(ID3D11InfoQueue* This, D3D11_MESSAGE_ID ID, BOOL bEnable);
  BOOL (__stdcall *GetBreakOnCategory)(ID3D11InfoQueue* This, D3D11_MESSAGE_CATEGORY Category);
  BOOL (__stdcall *GetBreakOnSeverity)(ID3D11InfoQueue* This, D3D11_MESSAGE_SEVERITY Severity);
  BOOL (__stdcall *GetBreakOnID)(ID3D11InfoQueue* This, D3D11_MESSAGE_ID ID);
  void (__stdcall *SetMuteDebugOutput)(ID3D11InfoQueue* This, BOOL bMute);
  BOOL (__stdcall *GetMuteDebugOutput)(ID3D11InfoQueue* This);
} ID3D11InfoQueueVtbl;

struct _ID3D11InfoQueue {
  const ID3D11InfoQueueVtbl* lpVtbl;
};
]])
------------------------------------------------------------------------------
--
-- ID3D11InfoQueue
--
------------------------------------------------------------------------------
local ID3D11InfoQueue = {}
setmetatable(ID3D11InfoQueue, D3D.IUnknownMT)

ID3D11InfoQueue.GUID = ffi.new('GUID', {
  0x6543dbb6, 0x1b48, 0x42f5,
  {0xab, 0x82, 0xe9, 0x7e, 0xc7, 0x43, 0x26, 0xf6}
})

function ID3D11InfoQueue:setBreakOnSeverity(severity, bool)
  local hres = self.lpVtbl.SetBreakOnSeverity(self, severity, bool)
  if hres ~= 0 then
    error(string.format("DX11Debug: failed to set break on severity [%s]", D3D.GetError(hres)))
  end
end

local ID3D11InfoQueueMT = {__index = ID3D11InfoQueue}
ffi.metatype('ID3D11InfoQueue', ID3D11InfoQueueMT)
------------------------------------------------------------------------------
--
-- ID3D11Debug
--
------------------------------------------------------------------------------
local ID3D11Debug = {}
setmetatable(ID3D11Debug, D3D.IUnknownMT)

ID3D11Debug.GUID = ffi.new('GUID', {
  0x79cf2233, 0x7536, 0x4948,
  {0x9d, 0x36, 0x1e, 0x46, 0x92, 0xdc, 0x57, 0x60}
})

function ID3D11Debug:getInfoQueue()
  return self:queryInterface('ID3D11InfoQueue*', ID3D11InfoQueue.GUID)
end

function ID3D11Debug:validateContext(ctx)
  self.lpVtbl.ValidateContext(self, ctx)
  if ctx:getType() == "deferred" then
    self.lpVtbl.ValidateContextForDispatch(self, ctx)
  end
end

function ID3D11Debug:reportStats()
  self.lpVtbl.ReportLiveDeviceObjects(self, bit.bor(D3D.RLDO_SUMMARY, D3D.RLDO_DETAIL))
end

local ID3D11DebugMT = {__index = ID3D11Debug}
return ffi.metatype('ID3D11Debug', ID3D11DebugMT)