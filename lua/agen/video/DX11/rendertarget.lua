----------------------------------------------------------------------------------
--
-- DX11 render target implementation
-- will use a texture 2D as a backend
--
----------------------------------------------------------------------------------
local bit = require('bit')
local string = require('string')
local D3D = require('agen.video.DX11.lib')
local DX11Texture = require('agen.video.DX11.texture')
local ffi = D3D.ffi

local DX11RenderTarget = {}
local DX11RenderTargetMT = {__index = DX11RenderTarget}
-- TODO: init from device caps
local _MAX_RENDER_TARGETS = 8
-- default clear color values
local colorBlack = {
  GUID  = ffi.new('GUID', {
    0x1b2d2bf8, 0xb7a9, 0x434d, {0xb8, 0xd1, 0x69, 0xc8, 0xa3, 0x23, 0x8e, 0xdf}}),
  value = ffi.new('float[4]', {0, 0, 0, 1}),
}
local depthZero  = {
  GUID = ffi.new('GUID', {
    0x3a526580, 0x250d, 0x4899, {0xa7, 0xaa, 0xc1, 0x50, 0x2f, 0xe0, 0x79, 0xc5}}),
  value = ffi.new('float[1]'),
}

local mapPurpose = {
  color = {
    support = D3D.FORMAT_SUPPORT_RENDER_TARGET,
    bindFlags = bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_RENDER_TARGET),
  },
  depth = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = D3D.BIND_DEPTH_STENCIL,
  },
  depth_res = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL),
  },
  stencil = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = D3D.BIND_DEPTH_STENCIL,
  },
  stencil_res = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL),
  },
  depth_stencil = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = D3D.BIND_DEPTH_STENCIL,
  },
  depth_stencil_res = {
    support = D3D.FORMAT_SUPPORT_DEPTH_STENCIL,
    bindFlags = bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL),
		texFormat = {
			[tonumber(D3D.FORMAT_D32_FLOAT)]         = D3D.FORMAT_R32_TYPELESS,
			[tonumber(D3D.FORMAT_D24_UNORM_S8_UINT)] = D3D.FORMAT_R24G8_TYPELESS,
			[tonumber(D3D.FORMAT_D16_UNORM)]         = D3D.FORMAT_R16_TYPELESS,
			-- TODO: 8bit debth?
		}
  },
}

local function createTextureWithPurpose(device, opts, purpose)
  assert(opts.format, "Missing required paramter .format")
  assert(purpose, "Missing required paramter purpose")
  assert(mapPurpose[purpose], "Unsupported texture purpose [" .. purpose .. "]")

  local dxgiFormat = D3D.FORMAT_UNKNOWN
  if bit.band(bit.rshift(opts.format, 28), 0xf) == 1 then
    -- SDL format
    error("DXGI format not expected!")
  end
	--
	-- depth/stencil shader resource texture formats must be typeless
	--
	local bindFlags = mapPurpose[purpose].bindFlags
	if bindFlags == bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL) then
		agen.log.verbose('video', 'changing depth/stencil texture format with purpose [' .. purpose .. '] to typeless')
		dxgiFormat = mapPurpose[purpose].texFormat[opts.format]
	else
		dxgiFormat = opts.format
	end

  if not device:checkFormatSupport(dxgiFormat, mapPurpose[purpose].support) then
    agen.log.error('video', "FIXME: brokern for depth formats! Unsupported [" .. purpose .. "] format [" .. dxgiFormat .. "]")
  end
  --
	-- FIXME: use device:createTexture()
  -- create texture to use as color, depth or depth/stencil buffer (same size as the render target texture)
  --
  return DX11Texture.create2D(device, {
    width   = opts.width,
    height  = opts.height,
    samples = opts.samples,
    dxgi_format  = dxgiFormat,
    levels  = 1,
    usage   = D3D.USAGE_DEFAULT,
    flags   = bindFlags,
  })
end

--- Create Render target from texture
-- @param DX11 device
-- @param table of color/depth buffer descriptors
-- @return DX11RenderTarget
function DX11RenderTarget.create(device, renderDesc)
  assert(device, "Invalid argument #1, ID3D11Device expected, got nil")
  assert(ffi.istype('ID3D11Device', device), "Invalid argument #1, ID3D11Device expected")
  assert(renderDesc, "Invalid argument #2, RenderTarget Description expected, got nil")
  local self = {
    numViews = 0,
    renderTargetViews = ffi.gc(
      ffi.new('ID3D11RenderTargetView*[?]', _MAX_RENDER_TARGETS),
      function(views) 
        for i = 0, _MAX_RENDER_TARGETS - 1, 1 do
          if views[i] ~= nil then views[i]:release() end
        end
      end),
    viewports = ffi.new('D3D11_VIEWPORT[?]', _MAX_RENDER_TARGETS),
    depthStencilView = nil,
    textures = {},
  }
  setmetatable(self, DX11RenderTargetMT)

  for name, value in pairs(renderDesc) do
    local viewIndex = string.match(name, 'color(%d)')
    if viewIndex ~= nil then
      -- viewIndex is string, convert to number to use as cdata index array
      viewIndex = tonumber(viewIndex)
      -----------------------------------------------------------
      --
      -- creating color buffer attachment
      --
      -----------------------------------------------------------
      if self.numViews == _MAX_RENDER_TARGETS then
        error("Maximum number of color buffers exceeded: " .. _MAX_RENDER_TARGETS)
      end
      -----------------------------------------------------------
      --
      -- Create render target view
      --
      -----------------------------------------------------------
      local viewDesc = ffi.new('D3D11_RENDER_TARGET_VIEW_DESC', {
        Format = D3D.FORMAT_UNKNOWN,
        ViewDimension = D3D.RTV_DIMENSION_TEXTURE2D,
        Texture2D = ffi.new('D3D11_TEX2D_RTV', {
          MipSlice = 0,
        }),
      })
      if value.texture ~= nil then
        ------------------------------------------------------------
        -- 
        -- use existing texture
        --
        ------------------------------------------------------------
        self.textures[viewIndex] = value.texture
        viewDesc.Format = value.texture:getDesc().Format
        device:createRenderTargetViewNoGC(value.texture, viewDesc, self.renderTargetViews + viewIndex)
        local w, h = value.texture:getSize(0)
        self.viewports[viewIndex].Width  = w
        self.viewports[viewIndex].Height = h
        self.viewports[viewIndex].MaxDepth = 1
      elseif value.format then
        ------------------------------------------------------------
        -- 
        -- create new texture
        --
        ------------------------------------------------------------
        local colorBufferTex = createTextureWithPurpose(device, value, 'color')
        self.textures[viewIndex] = colorBufferTex
        viewDesc.Format = colorBufferTex:getDesc().Format
        self.renderTargetViews[viewIndex] = device:createRenderTargetView(colorBufferTex, viewDesc)
        self.viewports[viewIndex].Width  = value.width
        self.viewports[viewIndex].Height = value.height
        self.viewports[viewIndex].MaxDepth = 1
      else
        error("Cannot create Render target: neither texture nor format specified for buffer [" .. name .. "]") 
      end
      --
      -- self is storing a pointer, increase ref count
      --
      self.renderTargetViews[viewIndex]:ref()
      --
      -- store clearColor as private data
      --
      self.renderTargetViews[viewIndex]:setPrivateData(colorBlack.GUID, ffi.sizeof('float[4]'), value.clearColor or colorBlack.value)
      self.numViews = self.numViews + 1

    ------------------------------------------------------------------------------------------
    --
    -- Depth/Stencil View can be a new or existing depth buffer/texture
    --
    ------------------------------------------------------------------------------------------
    elseif name == 'depth' or
           name == 'depth_stencil' or
           name == 'stencil' then
      --
      -- Create the depth stencil view
      --
      local descDSV = ffi.new('D3D11_DEPTH_STENCIL_VIEW_DESC', {
        Format		  = D3D.FORMAT_UNKNOWN,
        ViewDimension = D3D.DSV_DIMENSION_TEXTURE2D,
        Flags         = 0,
        Texture2D     = ffi.new('D3D11_TEX2D_DSV', {
          MipSlice = 0
        })
      })
 
      if value.texture then
        descDSV.Format = value.texture:getDesc().Format
        self.depthStencilView = device:createDepthStencilView(value.texture, descDSV)
        self.textures.depth_stencil = value.texture
      elseif value.format then
        --
        -- create texture to use as depth/stencil buffer (same size as the render target texture)
        --
        local dex = createTextureWithPurpose(device, value, 'depth_stencil' .. (value.shaderResource and '_res' or ''))
				-- FIXME: SDL formats?
        descDSV.Format = value.format
        self.depthStencilView = device:createDepthStencilView(dex, descDSV)
        self.textures.depth_stencil = dex
      else
        error("Cannot create Depth/Stencil target: neither texture nor format specified for buffer [" .. name .. "]")
      end
      -- store clear color
      if value.clearColor then
        depthZero.value[0] = value.clearColor[0]
      end
      self.depthStencilView:setPrivateData(depthZero.GUID, ffi.sizeof(depthZero.value), depthZero.value)
    else
      error("Unsupported buffer type [" .. name .. "]")
	  end
  end
  agen.log.debug('video', "Created RenderTarget with [%d] color buffers and %s depth/stencil buffer",
    self.numViews,
    self.depthStencilView and "" or "no")
  return self
end

function DX11RenderTarget:setActive(ctx)
  ctx.lpVtbl.OMSetRenderTargets(ctx, self.numViews, self.renderTargetViews, self.depthStencilView)
  -----------------------------------------------------------------------------------------------------------
  --
  -- Even though you specify float values to the members of the D3D11_VIEWPORT structure for the 
  -- pViewports array in a call to ID3D11DeviceContext::RSSetViewports for feature levels 9_x, RSSetViewports 
  -- uses DWORDs internally. Because of this behavior, when you use a negative top left corner for the 
  -- viewport, the call to RSSetViewports for feature levels 9_x fails. This failure occurs because 
  -- RSSetViewports for 9_x casts the floating point values into unsigned integers without validation, which 
  -- results in integer overflow. 
  -- FIXME: not here
  -----------------------------------------------------------------------------------------------------------
  ctx.lpVtbl.RSSetViewports(ctx, self.numViews, self.viewports)
end

function DX11RenderTarget:setClearColor(index, bgcolor)
  assert(bgcolor, "Invalid argument #1, color cannot be nil")
  assert(ffi.istype('float[4]', bgcolor), "Invalid argument #1, float[4] cdata expected")
  self.renderTargetViews[index]:setPrivateData(colorBlack.GUID, ffi.sizeof(colorBlack.value), bgcolor)
end

function DX11RenderTarget:getClearColor(index)
  local bgcolor = ffi.new('float[4]')
  local size = ffi.new('UINT[1]')
  self.renderTargetViews[index]:getPrivateData(colorBlack.GUID, size, bgcolor)
end
--- Clear all views with stored clear color value
-- FIXME: use no GC implementation
function DX11RenderTarget:clear(ctx)
  local bgcolor = ffi.new('float[4]')
  local size = ffi.new('UINT[1]')

  size[0] = ffi.sizeof(colorBlack.value)
  for i = 0, self.numViews - 1, 1 do
    self.renderTargetViews[i]:getPrivateData(colorBlack.GUID, size, bgcolor)
    ctx.lpVtbl.ClearRenderTargetView(ctx, self.renderTargetViews[i], bgcolor)
  end
end

function DX11RenderTarget:clearDepthView(val, ctx)
  assert(self.depthStencilView, "no depth / stencil attachment set")
  if val == nil then
    val = ffi.new('float[1]')
    local size = ffi.new('UINT[1]', {
      [0] = ffi.sizeof('float')
    })
    self.depthStencilView:getPrivateData(depthZero.GUID, size, val)
    val = val[0]
  end
  ctx.lpVtbl.ClearDepthStencilView(ctx, self.depthStencilView, bit.bor(D3D.CLEAR_DEPTH, D3D.CLEAR_STENCIL), val, 0)
end

function DX11RenderTarget:getColorAttachment(index)
  return self.textures[index]
end

function DX11RenderTarget:getDepthAttachment()
  return self.textures.depth_stencil
end

function DX11RenderTarget:setViewVolume(index, l, b, w, h, n, f)
  self.viewports[index].TopLeftX = l
  self.viewports[index].TopLeftY = b
  self.viewports[index].Width    = w
  self.viewports[index].Height   = h
  self.viewports[index].MinDepth = n
  self.viewports[index].MaxDepth = f
end

function DX11RenderTarget:getViewVolume(index)
  return self.viewports[index].Width,
         self.viewports[index].Height
end

return DX11RenderTarget
