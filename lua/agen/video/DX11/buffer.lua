local string = require('string')
local bit = require('bit')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_USAGE {	
  USAGE_DEFAULT	  = 0x00000000,
  USAGE_IMMUTABLE	= 0x00000001,
  USAGE_DYNAMIC	  = 0x00000002,
  USAGE_STAGING	  = 0x00000003
} D3D11_USAGE;

typedef enum _D3D11_CPU_ACCESS_FLAG {
  CPU_ACCESS_WRITE	= 0x00010000,
  CPU_ACCESS_READ	  = 0x00020000
} D3D11_CPU_ACCESS_FLAG;

typedef enum _D3D11_RESOURCE_MISC_FLAG { 
  RESOURCE_MISC_GENERATE_MIPS                    = 0x1L,
  RESOURCE_MISC_SHARED                           = 0x2L,
  RESOURCE_MISC_TEXTURECUBE                      = 0x4L,
  RESOURCE_MISC_DRAWINDIRECT_ARGS                = 0x10L,
  RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS           = 0x20L,
  RESOURCE_MISC_BUFFER_STRUCTURED                = 0x40L,
  RESOURCE_MISC_RESOURCE_CLAMP                   = 0x80L,
  RESOURCE_MISC_SHARED_KEYEDMUTEX                = 0x100L,
  RESOURCE_MISC_GDI_COMPATIBLE                   = 0x200L,
  RESOURCE_MISC_SHARED_NTHANDLE                  = 0x800L,
  RESOURCE_MISC_RESTRICTED_CONTENT               = 0x1000L,
  RESOURCE_MISC_RESTRICT_SHARED_RESOURCE         = 0x2000L,
  RESOURCE_MISC_RESTRICT_SHARED_RESOURCE_DRIVER  = 0x4000L,
  RESOURCE_MISC_GUARDED                          = 0x8000L,
  RESOURCE_MISC_TILE_POOL                        = 0x20000L,
  RESOURCE_MISC_TILED                            = 0x00040000
} D3D11_RESOURCE_MISC_FLAG;

typedef struct _D3D11_BUFFER_DESC {
  UINT        ByteWidth;
  D3D11_USAGE Usage;
  UINT        BindFlags;
  UINT        CPUAccessFlags;
  UINT        MiscFlags;
  UINT        StructureByteStride;
} D3D11_BUFFER_DESC;

typedef struct _ID3D11Buffer ID3D11Buffer;
typedef struct _ID3D11BufferVtbl {
	HRESULT (__stdcall *QueryInterface)(ID3D11Buffer* This, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Buffer* This);
  ULONG (__stdcall *Release)(ID3D11Buffer* This);
  void (__stdcall *GetDevice)(ID3D11Buffer* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Buffer* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Buffer* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Buffer* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetType)(ID3D11Buffer* This, D3D11_RESOURCE_DIMENSION* pResourceDimension);
  void (__stdcall *SetEvictionPriority)(ID3D11Buffer* This, UINT EvictionPriority);
  UINT (__stdcall *GetEvictionPriority)(ID3D11Buffer* This);
	void (__stdcall *GetDesc)(ID3D11Buffer* This, D3D11_BUFFER_DESC *pDesc);
} ID3D11BufferVtbl;

struct _ID3D11Buffer {
  const ID3D11BufferVtbl* lpVtbl;
};		
]])

local ID3D11Buffer = require('agen.interface.video.buffer')
ID3D11Buffer.GUID = ffi.new('GUID', {
  0x48570b85,
  0xd1ee, 0x4fcd,
  {0xa2, 0x50, 0xeb, 0x35, 0x07, 0x22, 0xb0, 0x37}
})
setmetatable(ID3D11Buffer, D3D.ID3D11ResourceMT)

local desc = ffi.new('D3D11_BUFFER_DESC')

local ID3D11BufferMT = {
	__index  = ID3D11Buffer,
	__len		 = function(self)
    self.lpVtbl.GetDesc(self, desc)
    return desc.ByteWidth
  end,
}
-- https://www.guidgenerator.com/online-guid-generator.aspx
local GUID_BufferType = ffi.new('GUID', {
  0x6dade14a,
  0xc596, 0x45db, 
  {0x95, 0xa3, 0x44, 0x28, 0x6d, 0xa3, 0xe6, 0x62}
})

local mapDX11BufferPurpose = {
  vertex   = D3D.BIND_VERTEX_BUFFER,
  data     = D3D.BIND_VERTEX_BUFFER,
  index    = D3D.BIND_INDEX_BUFFER,
  -- uniform data updated per frame
  uniform  = D3D.BIND_CONSTANT_BUFFER,
}

local mapDX11BufferMapping = {
  read       = D3D.MAP_READ,
  write      = D3D.MAP_WRITE,
  read_write = D3D.MAP_READ_WRITE,
  write_discard      = D3D.MAP_WRITE_DISCARD,
  write_no_overwrite = D3D.MAP_WRITE_NO_OVERWRITE,
}

local mapDX11BufferUsage = {
  -- A resource that is accessible by both the GPU (read only) and the CPU (write only).
  -- A dynamic resource is a good choice for a resource that will be updated by the CPU
  -- at least once per frame. To update a dynamic resource, use a Map method. BindFlags > 0
	dynamic	 = D3D.USAGE_DYNAMIC,
  -- A resource that can only be read by the GPU. It cannot be written by the GPU, 
  -- and cannot be accessed at all by the CPU. This type of resource must be initialized 
  -- when it is created, since it cannot be changed after creation.
  constant = D3D.USAGE_IMMUTABLE,
  -- A resource that requires read and write access by the GPU.
	static	 = D3D.USAGE_DEFAULT,
  -- A resource that supports data transfer (copy) from the CPU to the GPU or vice versa
  -- BindFlags must be 0
  staging  = D3D.USAGE_STAGING,
}

function ID3D11Buffer.create(device, options)
  assert(device ~= nil,   "Invalid argument #1, ID3D11Device device expected, got nil")
  assert(ffi.istype('ID3D11Device', device),
                          "Invalid argument #1, ID3D11Device expected")
  assert(mapDX11BufferPurpose[options.purpose],
                          string.format("Unsupported buffer purpose [%s]", options.purpose))

  local bindFlags = mapDX11BufferPurpose[options.purpose]
  local usage = mapDX11BufferUsage[options.usage]
  local cpuFlags = 0
  local data = nil

  if options.usage == 'constant' then
    -- For a constant buffer (BindFlags of D3D11_BUFFER_DESC set to D3D11_BIND_CONSTANT_BUFFER),
    -- you must set the ByteWidth value of D3D11_BUFFER_DESC in multiples of 16, and less than
    -- or equal to D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT.
    assert((options.size % 16) == 0, "Constant buffer size must be 16 bytes aligned")
		data = ffi.new('D3D11_SUBRESOURCE_DATA', {
			pSysMem = options.data,
			SysMemPitch = 0,
			SysMemSlicePitch = 0
		})
  elseif options.usage == 'static' then
    -- Note: USAGE_DEFAULT cannot have any CPUAccessFlags set
    cpuFlags = 0
  elseif options.usage == 'dynamic' then
    -- no initial data, will update from host memory via mapping
    cpuFlags = D3D.CPU_ACCESS_WRITE
  elseif options.usage == 'staging' then
    cpuFlags = D3D.CPU_ACCESS_WRITE
    bindFlags = 0x00000000
  end

  local bufferDesc = ffi.new('D3D11_BUFFER_DESC', {
		ByteWidth      = options.size,
		Usage          = usage,
		BindFlags      = bindFlags,
    -- D3D.CPU_ACCESS_WRITE requires D3D11_USAGE_DYNAMIC or D3D11_USAGE_STAGING
    -- mappable buffers require CPU_ACCESS_READ or CPU_ACCESS_WRITE
    -- D3D.CPU_ACCESS_READ  requires D3D11_USAGE_STAGING
		CPUAccessFlags = cpuFlags,
		MiscFlags      = 0,
		StructureByteStride = ffi.sizeof(options.ctype),
  })
  --
  -- Now create buffer
  --
  local self = device:_createBuffer(bufferDesc, data)
  --
  -- Set ctype as private data
  --
  self:setPrivateData(GUID_BufferType, string.len(options.ctype), options.ctype)
  --
  -- Optional: set buffer name
  --
  if options.name then
    self:setPrivateData(D3D.GUID_DebugObjectName, string.len(options.name), options.name)
  end
  return self
end
--- Asynchronously update buffer region
function ID3D11Buffer:updateFromBuffer(offset, srcBuff)
  assert(offset >= 0,                         'offset not 0 or positive')
  assert(srcBuff ~= nil,                      'source is nil')
  assert(ffi.istype('ID3D11Buffer', srcBuff), 'source not a buffer')
  -- For textures see D3D11CalcSubresource function
	local ctx = self:getImmediateContext()
  -- A buffer is an unstructured resource and is therefore defined as containing a single subresource.
  -- APIs that take buffers do not need a subresource index
  ctx.lpVtbl.CopySubresourceRegion(ctx, self, 0, offset, 0, 0, srcBuff, 0, nil)
end
--- Map whole buffer. May be called per frame.
-- @param mapping flags
-- @param context
function ID3D11Buffer:_map(flags)
  local mapFlags = mapDX11BufferMapping[flags]
  assert(mapFlags ~= nil, 'unsupported mapping flags [' ..  flags .. ']')

	local ctx			 = self:getImmediateContext()
  local mapping  = ffi.new('D3D11_MAPPED_SUBRESOURCE')
  ------------------------------------------------------------------------
  --
  -- If you call Map on a deferred context, you can only pass D3D11_MAP_WRITE_DISCARD, 
  -- D3D11_MAP_WRITE_NO_OVERWRITE, or both to the MapType parameter. Other D3D11_MAP-typed 
  -- values are not supported for a deferred context.
  --
  ------------------------------------------------------------------------
  local hres = ctx.lpVtbl.Map(ctx, self, 0, mapFlags,
    0, --D3D.MAP_FLAG_DO_NOT_WAIT,
    mapping)
  if hres ~= 0 then
    error(string.format("Failed to map buffer: [%s]. Static buffers cannot be mapped, use update()", D3D.GetError(hres)))
  end
  -- For D3D_FEATURE_LEVEL_10_0 and higher, the pointer is aligned to 16 bytes.
  -- For lower than D3D_FEATURE_LEVEL_10_0, the pointer is aligned to 4 bytes.
  return mapping.pData
end

function ID3D11Buffer:_getCtype(ptr)
  local size = 16
  local ctypeC = ffi.new('char[?]', size)
  local ctypeSize = ffi.new('UINT[1]', {[0] = size})

  self:getPrivateData(GUID_BufferType, ctypeSize, ctypeC)
  ----------------------------------------------------------------------------
  -- If the optional argument len is missing, ptr is converted to a "char *" 
  -- and the data is assumed to be zero-terminated. The length of the string 
  -- is computed with strlen().
  -- Otherwise ptr is converted to a "void *" and len gives the length of the 
  -- data. The data may contain embedded zeros and need not be byte-oriented 
  -- (though this may cause endianess issues).
  ----------------------------------------------------------------------------
  local ret = ffi.string(ctypeC, ctypeSize[0])
  if ptr then
    ret = ret .. '*'
  end
  return ret
end

local indexBufferType = {
  [2] = D3D.FORMAT_R16_UINT,
  [4] = D3D.FORMAT_R32_UINT,
}
function ID3D11Buffer:getIndexBufferFormat()
	local sizeof = ffi.sizeof(self:_getCtype(false))
  local bufferFormat = indexBufferType[sizeof]
  assert(bufferFormat ~= nil, "Unsupported index buffer format")
  return bufferFormat
end

function ID3D11Buffer:_unmap()
	local ctx	= self:getImmediateContext()
  ctx:unmap(self, 0)
end

return ffi.metatype('ID3D11Buffer', ID3D11BufferMT)
