local bit     = require('bit')
local string  = require('string')
local D3D     = require('agen.video.DX11.lib')
local sdl     = require('sdl2')

local DX11Buffer  = require('agen.video.DX11.buffer')
local DX11Texture = require('agen.video.DX11.texture')
local DX11Sampler = require('agen.video.DX11.sampler')
local DX11SwapChain = require('agen.video.DX11.swapchain')
local RenderState = require('agen.video.DX11.renderstate')
local DX11Counter = require('agen.video.DX11.counter')
local DX11Context = require('agen.video.DX11.context')
local DX11RenderTarget = require('agen.video.DX11.rendertarget')
local DX11Debug   = require('agen.video.DX11.debug')

local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D_DRIVER_TYPE { 
  DRIVER_TYPE_UNKNOWN    = 0,
  DRIVER_TYPE_HARDWARE   = 1,
  DRIVER_TYPE_REFERENCE  = 2,
  DRIVER_TYPE_NULL       = 3,
  DRIVER_TYPE_SOFTWARE   = 4,
  DRIVER_TYPE_WARP       = 5
} D3D_DRIVER_TYPE;

typedef enum _D3D11_CREATE_DEVICE_FLAG { 
  CREATE_DEVICE_SINGLETHREADED                                 = 0x1,
  CREATE_DEVICE_DEBUG                                          = 0x2,
  CREATE_DEVICE_SWITCH_TO_REF                                  = 0x4,
  CREATE_DEVICE_PREVENT_INTERNAL_THREADING_OPTIMIZATIONS       = 0x8,
  CREATE_DEVICE_BGRA_SUPPORT                                   = 0x20,
  CREATE_DEVICE_DEBUGGABLE                                     = 0x40,
  CREATE_DEVICE_PREVENT_ALTERING_LAYER_SETTINGS_FROM_REGISTRY  = 0x80,
  CREATE_DEVICE_DISABLE_GPU_TIMEOUT                            = 0x100,
  CREATE_DEVICE_VIDEO_SUPPORT                                  = 0x800
} CREATE_DEVICE_FLAG;

typedef enum _D3D11_FEATURE { 
  FEATURE_THREADING                       = 0,
  FEATURE_DOUBLES                         = 1,
  FEATURE_FORMAT_SUPPORT                  = 2,
  FEATURE_FORMAT_SUPPORT2                 = 3,
  FEATURE_D3D10_X_HARDWARE_OPTIONS        = 4,
  FEATURE_D3D11_OPTIONS                   = 5,
  FEATURE_ARCHITECTURE_INFO               = 6,
  FEATURE_D3D9_OPTIONS                    = 7,
  FEATURE_SHADER_MIN_PRECISION_SUPPORT    = 8,
  FEATURE_D3D9_SHADOW_SUPPORT             = 9,
  FEATURE_D3D11_OPTIONS1                  = 10,
  FEATURE_D3D9_SIMPLE_INSTANCING_SUPPORT  = 11,
  FEATURE_MARKER_SUPPORT                  = 12,
  FEATURE_D3D9_OPTIONS1                   = 13,
  FEATURE_D3D11_OPTIONS2                  = 14,
  FEATURE_D3D11_OPTIONS3                  = 15,
  FEATURE_GPU_VIRTUAL_ADDRESS_SUPPORT     = 16
} D3D11_FEATURE;

typedef enum _D3D11_STANDARD_MULTISAMPLE_QUALITY_LEVELS { 
	STANDARD_MULTISAMPLE_PATTERN  = 0xffffffff,
	CENTER_MULTISAMPLE_PATTERN    = 0xfffffffe
} D3D11_STANDARD_MULTISAMPLE_QUALITY_LEVELS;

typedef struct _D3D11_FEATURE_DATA_THREADING {
  BOOL DriverConcurrentCreates;
  BOOL DriverCommandLists;
} D3D11_FEATURE_DATA_THREADING;

typedef struct _D3D11_FEATURE_DATA_FORMAT_SUPPORT {
  DXGI_FORMAT InFormat;
  UINT        OutFormatSupport;
} D3D11_FEATURE_DATA_FORMAT_SUPPORT;

typedef struct _D3D11_SO_DECLARATION_ENTRY D3D11_SO_DECLARATION_ENTRY;

typedef struct _ID3D11DeviceVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11Device* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Device* This);
  ULONG (__stdcall *Release)(ID3D11Device* This);
  HRESULT (__stdcall *CreateBuffer)(ID3D11Device* This,const D3D11_BUFFER_DESC *pDesc,const D3D11_SUBRESOURCE_DATA *pInitialData,ID3D11Buffer **ppBuffer);
  HRESULT (__stdcall *CreateTexture1D)(ID3D11Device* This, const D3D11_TEXTURE1D_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Texture** ppTexture1D);
  HRESULT (__stdcall *CreateTexture2D)(ID3D11Device* This, const D3D11_TEXTURE2D_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Texture** ppTexture2D);
  HRESULT (__stdcall *CreateTexture3D)(ID3D11Device* This, const D3D11_TEXTURE3D_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Texture** ppTexture3D);
  HRESULT (__stdcall *CreateShaderResourceView)(ID3D11Device* This, void* pResource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc, ID3D11ShaderResourceView** ppSRView);
  HRESULT (__stdcall *CreateUnorderedAccessView)(ID3D11Device* This, void* pResource, const D3D11_UNORDERED_ACCESS_VIEW_DESC *pDesc, ID3D11UnorderedAccessView** ppUAView);
  HRESULT (__stdcall *CreateRenderTargetView)(ID3D11Device* This, void* pResource, const D3D11_RENDER_TARGET_VIEW_DESC* pDesc, ID3D11RenderTargetView** ppRTView);
  HRESULT (__stdcall *CreateDepthStencilView)(ID3D11Device* This, void* pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC* pDesc, ID3D11DepthStencilView** ppDepthStencilView);
  HRESULT (__stdcall *CreateInputLayout)(ID3D11Device* This,const D3D11_INPUT_ELEMENT_DESC *pInputElementDescs,UINT NumElements,const void* pShaderBytecodeWithInputSignature, size_t BytecodeLength, ID3D11InputLayout** ppInputLayout);
  HRESULT (__stdcall *CreateVertexShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppVertexShader);
  HRESULT (__stdcall *CreateGeometryShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppGeometryShader);
  HRESULT (__stdcall *CreateGeometryShaderWithStreamOutput)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, const D3D11_SO_DECLARATION_ENTRY *pSODeclaration, UINT NumEntries, const UINT* pBufferStrides, UINT NumStrides, UINT RasterizedStream, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppGeometryShader);
  HRESULT (__stdcall *CreatePixelShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppPixelShader);
  HRESULT (__stdcall *CreateHullShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppHullShader);
  HRESULT (__stdcall *CreateDomainShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppDomainShader);
  HRESULT (__stdcall *CreateComputeShader)(ID3D11Device* This, const void* pShaderBytecode, size_t BytecodeLength, ID3D11ClassLinkage* pClassLinkage, ID3D11Shader** ppComputeShader);
  HRESULT (__stdcall *CreateClassLinkage)(ID3D11Device* This, ID3D11ClassLinkage **ppLinkage);
  HRESULT (__stdcall *CreateBlendState)(ID3D11Device* This,const D3D11_BLEND_DESC *pBlendStateDesc,ID3D11BlendState **ppBlendState);
  HRESULT (__stdcall *CreateDepthStencilState)(ID3D11Device* This,const D3D11_DEPTH_STENCIL_DESC *pDepthStencilDesc,ID3D11DepthStencilState **ppDepthStencilState);
  HRESULT (__stdcall *CreateRasterizerState)(ID3D11Device* This,const D3D11_RASTERIZER_DESC *pRasterizerDesc,ID3D11RasterizerState **ppRasterizerState);
  HRESULT (__stdcall *CreateSamplerState)(ID3D11Device* This,const D3D11_SAMPLER_DESC *pSamplerDesc,ID3D11SamplerState **ppSamplerState);
  HRESULT (__stdcall *CreateQuery)(ID3D11Device* This,const D3D11_QUERY_DESC *pQueryDesc,ID3D11Query **ppQuery);
  HRESULT (__stdcall *CreatePredicate)(ID3D11Device* This,const D3D11_QUERY_DESC *pPredicateDesc,ID3D11Predicate **ppPredicate);
  HRESULT (__stdcall *CreateCounter)(ID3D11Device* This,const D3D11_COUNTER_DESC *pCounterDesc,ID3D11Counter **ppCounter);
  HRESULT (__stdcall *CreateDeferredContext)(ID3D11Device* This, UINT ContextFlags, ID3D11DeviceContext** ppDeferredContext);
  HRESULT (__stdcall *OpenSharedResource)(ID3D11Device* This,HANDLE hResource,REFIID ReturnedInterface,void **ppResource);
  HRESULT (__stdcall *CheckFormatSupport)(ID3D11Device* This,DXGI_FORMAT Format,UINT *pFormatSupport);
  HRESULT (__stdcall *CheckMultisampleQualityLevels)(ID3D11Device* This,DXGI_FORMAT Format,UINT SampleCount,UINT *pNumQualityLevels);
  void (__stdcall *CheckCounterInfo)(ID3D11Device* This,D3D11_COUNTER_INFO *pCounterInfo);
  HRESULT (__stdcall *CheckCounter)(ID3D11Device* This, const D3D11_COUNTER_DESC* pDesc, D3D11_COUNTER_TYPE* pType,UINT* pActiveCounters, LPSTR szName, UINT* pNameLength, LPSTR szUnits, UINT* pUnitsLength, LPSTR szDescription, UINT* pDescriptionLength);
  HRESULT (__stdcall *CheckFeatureSupport)(ID3D11Device* This,D3D11_FEATURE Feature,void* pFeatureSupportData,UINT FeatureSupportDataSize);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Device* This,REFGUID guid,UINT *pDataSize,void* pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Device* This,REFGUID guid,UINT DataSize,const void* pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Device* This,REFGUID guid,const IUnknown* pData);
  D3D_FEATURE_LEVEL (__stdcall *GetFeatureLevel)(ID3D11Device* This);
  UINT (__stdcall *GetCreationFlags)(ID3D11Device* This);
  HRESULT (__stdcall *GetDeviceRemovedReason)(ID3D11Device* This);
  void (__stdcall *GetImmediateContext)(ID3D11Device* This,ID3D11DeviceContext** ppImmediateContext);
  HRESULT (__stdcall *SetExceptionMode)(ID3D11Device* This,UINT RaiseFlags);
  UINT (__stdcall *GetExceptionMode)(ID3D11Device* This);
} ID3D11DeviceVtbl;

struct _ID3D11Device {
  const ID3D11DeviceVtbl* lpVtbl;
};

HRESULT __stdcall D3D11CreateDevice(
  void* pAdapter,
  UINT DriverType,
  HMODULE Software,
  UINT Flags,
  const D3D_FEATURE_LEVEL* pFeatureLevels,
  UINT FeatureLevels,
  UINT SDKVersion,
  ID3D11Device** ppDevice,
  D3D_FEATURE_LEVEL* pFeatureLevel,
  ID3D11DeviceContext** ppImmediateContext
);
/*
 * Notes for Windows Store apps
 * The D3D11CreateDeviceAndSwapChain function does not exist for Windows Store apps. 
 * Instead, Windows Store apps use the D3D11CreateDevice function and then use the 
 * IDXGIFactory2::CreateSwapChainForCoreWindow method.
 */
HRESULT __stdcall D3D11CreateDeviceAndSwapChain(
	void* pAdapter,
	UINT DriverType,
	HMODULE Software,
	UINT Flags,
	const D3D_FEATURE_LEVEL* pFeatureLevels,
	UINT FeatureLevels,
	UINT SDKVersion,
	const DXGI_SWAP_CHAIN_DESC* pSwapChainDesc,
	IDXGISwapChain** ppSwapChain,
	ID3D11Device** ppDevice,
	D3D_FEATURE_LEVEL* pFeatureLevel,
	ID3D11DeviceContext** ppImmediateContext);
]])

local colorBitsToFormat = {
  [32] = D3D.FORMAT_R8G8B8A8_UNORM,
  [24] = D3D.FORMAT_R8G8B8A8_UNORM,
  [16] = D3D.FORMAT_B4G4R4A4_UNORM,
   [8] = D3D.FORMAT_A8_UNORM,
}
-- note: for shader access use typeless formats
local depthBitsToFormat = {
  [32] = D3D.FORMAT_D32_FLOAT,
  [24] = D3D.FORMAT_D24_UNORM_S8_UINT,
  [16] = D3D.FORMAT_D16_UNORM,
   [8] = nil,
}

local mapSDLFormat2DXFormat = {
  -- 8 bit
  [tonumber(sdl.PIXELFORMAT_UNKNOWN)]   = D3D.FORMAT_UNKNOWN,
  [tonumber(sdl.PIXELFORMAT_A8)]        = D3D.FORMAT_A8_UNORM,
  -- 16 bit
  [tonumber(sdl.PIXELFORMAT_ARGB4444)]	= D3D.FORMAT_B4G4R4A4_UNORM,
  [tonumber(sdl.PIXELFORMAT_ARGB1555)]	= D3D.FORMAT_B5G5R5A1_UNORM,
  [tonumber(sdl.PIXELFORMAT_RGB565)]	  = D3D.FORMAT_B5G6R5_UNORM,
  -- 24 bit formats need expanding to 32
  [tonumber(sdl.PIXELFORMAT_RGB888)]    = D3D.FORMAT_B8G8R8X8_UNORM,
  -- 32 bit
  [tonumber(sdl.PIXELFORMAT_RGBX8888)]  = D3D.FORMAT_B8G8R8X8_UNORM,
  [tonumber(sdl.PIXELFORMAT_ARGB8888)]  = D3D.FORMAT_B8G8R8A8_UNORM,
  [tonumber(sdl.PIXELFORMAT_ABGR8888)]  = D3D.FORMAT_R8G8B8A8_UNORM,
  -- FOURCC
  DXT1 = D3D.FORMAT_BC1_UNORM,
  DXT2 = D3D.FORMAT_BC2_UNORM,
  DXT3 = D3D.FORMAT_BC2_UNORM,
  DXT4 = D3D.FORMAT_BC3_UNORM,
  DXT5 = D3D.FORMAT_BC3_UNORM,
  ATI1 = D3D.FORMAT_BC4_UNORM,
  ATI2 = D3D.FORMAT_BC5_UNORM,
  -- DEPTH/STENCIL
  [tonumber(sdl.PIXELFORMAT_D32)]   = D3D.FORMAT_D32_FLOAT,
  [tonumber(sdl.PIXELFORMAT_D24S8)] = D3D.FORMAT_D24_UNORM_S8_UINT,
  [tonumber(sdl.PIXELFORMAT_D16)]   = D3D.FORMAT_D16_UNORM,
  [tonumber(sdl.PIXELFORMAT_D8)]    = D3D.FORMAT_UNKNOWN,
}
--------------------------------------------------------------------
--
-- ID3D11Device
--
--------------------------------------------------------------------
local ID3D11Device = {}
setmetatable(ID3D11Device, D3D.IUnknownMT)

ID3D11Device.GUID = ffi.new('GUID', {
  0xdb6f6ddb,
  0xac77, 0x4e88,
  {0x82, 0x53, 0x81, 0x9d, 0xf9, 0xbb, 0xf1, 0x40}
})
------------------------------------------------------------------------------------
--
-- since each thread has its own lua state these are automatically per-private
--
------------------------------------------------------------------------------------
local _swapChain
--
-- An immediate context renders directly to the driver. Each device has one and only 
-- one immediate context which can retrieve data from the GPU. An immediate context 
-- can be used to immediately render (or play back) a command list.
--
local _context
local _defaultRenderTarget
local _vsync = 1
local _infoQueue
local _debug
local _caps  = {
  [tonumber(D3D.FEATURE_THREADING)] = 0,
  [tonumber(D3D.FEATURE_FORMAT_SUPPORT)] = 0,
  [tonumber(D3D.FEATURE_D3D10_X_HARDWARE_OPTIONS)] = 0,
}

function ID3D11Device.create(window, config)
  assert(config, "Missing required parameter #2, lua table expected got nil")
  assert(config.color_buffer, "Missing required configuration option .color_buffer")
  assert(colorBitsToFormat[config.color_buffer], "Unsupported color buffer depth")
  -------------------------------------------------------------------------------
  --
  -- Setup swapchain descriptor
  --
  -------------------------------------------------------------------------------
  local SwapChainDesc = ffi.new('DXGI_SWAP_CHAIN_DESC')
  SwapChainDesc.BufferDesc.Width   = config.width
  SwapChainDesc.BufferDesc.Height  = config.height
  SwapChainDesc.BufferDesc.Format  = colorBitsToFormat[config.color_buffer] or D3D.FORMAT_R8G8B8A8_UNORM
  SwapChainDesc.BufferDesc.ScanlineOrdering = D3D.MODE_SCANLINE_ORDER_PROGRESSIVE
  SwapChainDesc.BufferDesc.Scaling = D3D.MODE_SCALING_UNSPECIFIED
  SwapChainDesc.BufferDesc.RefreshRate.Numerator = 0
  SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1
  ------------------------------------------------------------------------------
  --
  -- The default sampler mode, with no anti-aliasing, has 
  -- a count of 1 and a quality level of 0.
  --
  -- If multi-sample antialiasing is being used, all bound 
  -- render targets and depth buffers must have the same sample 
  -- counts and quality levels.
  --
  -------------------------------------------------------------------------------
	if config.samples ~= nil and config.samples > 1 then
    SwapChainDesc.SampleDesc.Count   = config.samples
    SwapChainDesc.SampleDesc.Quality = D3D.STANDARD_MULTISAMPLE_PATTERN
	else
    SwapChainDesc.SampleDesc.Count   = 1
    SwapChainDesc.SampleDesc.Quality = 0 --D3D.STANDARD_MULTISAMPLE_PATTERN
	end
  SwapChainDesc.BufferUsage  = D3D.USAGE_RENDER_TARGET_OUTPUT
  SwapChainDesc.BufferCount  = 2
  SwapChainDesc.OutputWindow = window:getNativeWindow()
  -- TODO: check if new is needed
  SwapChainDesc.Windowed     = ffi.new('BOOL', config.windowed)
  -- SwapChainDesc.Windowed     = config.windowed and 1 or 0
  -- FLIP effect requires at least double buffering (BufferCount = 2+)
  SwapChainDesc.SwapEffect   = D3D.SWAP_EFFECT_FLIP_DISCARD
  SwapChainDesc.Flags        = 0

  assert(D3D.IsWindow(SwapChainDesc.OutputWindow) > 0, "Failed to get native window handle from SDL window")  
  --------------------------------------------------------------------------------
  --
  -- feature levels
  --
  --------------------------------------------------------------------------------
  local featureLevels = ffi.new('D3D_FEATURE_LEVEL[4]', {
    -- Needs ID3D11Device1 object
    --D3D.FEATURE_LEVEL_11_1,
    [0] = D3D.FEATURE_LEVEL_11_0,
    [1] = D3D.FEATURE_LEVEL_10_1,
    [2] = D3D.FEATURE_LEVEL_10_0,
    --
    -- min required feature level for instanced rendering is DX9 Shader model 3.0
    --
    [3] = D3D.FEATURE_LEVEL_9_3
  })
  local level = ffi.new('D3D_FEATURE_LEVEL[1]')
  local deviceFlags = 0 --bit.bor(D3D.CREATE_DEVICE_BGRA_SUPPORT)
  if config.debug then
    deviceFlags = bit.bor(deviceFlags, D3D.CREATE_DEVICE_DEBUG)
  end

  local device    = ffi.new('ID3D11Device*[1]')
  local swapChain = ffi.new('IDXGISwapChain*[1]')
  local context   = ffi.new('ID3D11DeviceContext*[1]')
  local hres = D3D.D3D11CreateDeviceAndSwapChain(nil,
    D3D.DRIVER_TYPE_HARDWARE,
    nil,
    deviceFlags,
    featureLevels, ffi.sizeof(featureLevels) / ffi.sizeof('D3D_FEATURE_LEVEL'),
    D3D.SDK_VERSION,
    SwapChainDesc,
    swapChain,
    device,
    level,
    context)
  if hres ~= 0 then
    error(string.format("Failed to create DX11 device: %s", D3D.GetError(hres)))
  end

  agen.log.debug('video', "Created DX11 device feature level [0x%x]", level[0])
  if level[0] == D3D.FEATURE_LEVEL_11_1 then
    -- TODO
    error("ID3D11Device1 not implemented")
  end

  local self = ffi.gc(device[0], D3D.Finalize)

  _swapChain = ffi.gc(swapChain[0], D3D.Finalize)
  _context = ffi.gc(context[0], D3D.Finalize)
  _vsync = config.vsync
  ---------------------------------------------------------------------------------------------
  --
  -- setup debug interface
  --
  ---------------------------------------------------------------------------------------------
  if config.debug then
    _debug = self:queryInterface('ID3D11Debug*', DX11Debug.GUID)
    _infoQueue = _debug:getInfoQueue()
    _infoQueue:setBreakOnSeverity(D3D.MESSAGE_SEVERITY_CORRUPTION, true)
    _infoQueue:setBreakOnSeverity(D3D.MESSAGE_SEVERITY_ERROR, true)
    _infoQueue:setBreakOnSeverity(D3D.MESSAGE_SEVERITY_WARNING, true)
    _infoQueue:setBreakOnSeverity(D3D.MESSAGE_SEVERITY_INFO, true)
  end

  local dataThreading = ffi.new('D3D11_FEATURE_DATA_THREADING[1]')
  _caps.FEATURE_DATA_THREADING = self:checkFeatureSupport(
    D3D.FEATURE_THREADING,
    dataThreading,
    ffi.sizeof('D3D11_FEATURE_DATA_THREADING'))

  agen.log.info('video', "Instantiated DX11 device [%s]", tostring(self))
  return self
end
-- TODO: store the depth buffer format instead of passing value
function ID3D11Device:init(config)
  --
  -- Setup default render target from back buffer
  --
  local tex = _swapChain:getBuffer(0, 'texture2D')
  local texW, texH = tex:getSize()
  ------------------------------------------------------------------
  --
  -- Create render target from backbuffer texture2D
  --
  ------------------------------------------------------------------
  _defaultRenderTarget = self:createRenderTarget({
    color0 = {
      texture = tex,
      clearColor = ffi.new('float[4]', {0, 0, 0, 1}),
    },
    depth_stencil = {
      texture = nil,
      -- requested DXGI format
      format  = depthBitsToFormat[config.depth_buffer],
      width   = texW,
      height  = texH,
      clearColor = ffi.new('float[1]', {1}),
    }
  })
  _defaultRenderTarget:setActive(_context)
end

function ID3D11Device:getName()
  return 'directx11'
end

function ID3D11Device:getDefaultRenderTarget()
  return _defaultRenderTarget
end
------------------------------------------------------------------------------------------
--
-- Render aka pipeline states
--
------------------------------------------------------------------------------------------
function ID3D11Device:createRenderState(opts)
  return RenderState:new(self, opts)
end
--- Create new vertex/index buffer from parameters
-- @param parameters
-- @return new buffer object
function ID3D11Device:createBuffer(options)
  return DX11Buffer.create(self, options)
end
--- Create new empty texture from parameters
-- @param options
--  width
--  height
--  levels mip levels
--  format texture format guaranteed to be compatible with the backbuffer
--  usage D3D usage
--  samples AA samples
-- @return new texture object
function ID3D11Device:createTexture(options)
  assert(options.format ~= nil, "Texture format not specified")

  agen.log.debug('video', "Mapping SDL image format to DX11 format...")
  options.dxgi_format = self:toNativeFormat(options.format)

  if not options.usage then
    options.usage = D3D.USAGE_DEFAULT
  end

  if not options.flags then
		options.flags = D3D.BIND_SHADER_RESOURCE
  end

	return DX11Texture.create2D(self, options)
end

function ID3D11Device:toNativeFormat(sdlFormat)
  -- RGB vs FOURCC
  local index = nil
  if bit.band(bit.rshift(sdlFormat, 28), 0xff) == 1 then
    -- SDL format, convert to native
    index = tonumber(sdlFormat)
    agen.log.debug('video', "SDL format [0x%x] to convert to DXGI format", index)
  elseif sdlFormat < 256 then
    -- DXGI format, already native
    return sdlFormat
  else
    -- FOURCC, convert to string
    index = string.char(bit.band(bit.rshift(sdlFormat,  0), 0xff)) ..
            string.char(bit.band(bit.rshift(sdlFormat,  8), 0xff)) ..
            string.char(bit.band(bit.rshift(sdlFormat, 16), 0xff)) ..
            string.char(bit.band(bit.rshift(sdlFormat, 24), 0xff))
    agen.log.debug('video', "4CC format converted to DXGI format [%s]", index)
  end

  assert(index, string.format("Invalid texture format [0x%x]", tonumber(sdlFormat)))
  assert(mapSDLFormat2DXFormat[index], string.format("Unsupported texture format [%s]", tostring(index)))
  return mapSDLFormat2DXFormat[index]
end
--- Check if sdlFormat can be used as a texture format
-- @param SDL_Format
-- @return true|false
function ID3D11Device:isSupportedTextureFormat(sdlFormat, flags)
  local fmt = self:toNativeFormat(sdlFormat)
  local ret = self:checkFormatSupport(fmt, flags)

  if ret then
    agen.log.debug('video', "DXGI texture format [%d] is supported", fmt)
  else
    agen.log.debug('video', "SDL texture format [0x%x] needs conversion", sdlFormat)
  end
  return ret
end
--- Return SDL pixel format that can be directly used by DX11. This is
-- the format the SDL surface will be converted to in order to be used
-- as texture data source.
-- @param source SDL format
-- @return dest SDL format
function ID3D11Device:getBestTextureFormat(sdlFormat)
  if bit.band(bit.rshift(sdlFormat, 28), 0xff) == 1 then
    --
    -- SDL format
    --
    local bytesPP = bit.band(bit.rshift(sdlFormat, 0), 0xff)
    local bitsPP  = bit.band(bit.rshift(sdlFormat, 8), 0xff)

    if bytesPP == 4 and bitsPP == 32 and 
      self:isSupportedTextureFormat(sdl.PIXELFORMAT_ABGR8888, D3D.FORMAT_SUPPORT_TEXTURE2D) then
      return sdl.PIXELFORMAT_ABGR8888
    elseif bytesPP == 4 and bitsPP == 24 and 
      self:isSupportedTextureFormat(sdl.PIXELFORMAT_RGBX8888, D3D.FORMAT_SUPPORT_TEXTURE2D) then
      return sdl.PIXELFORMAT_RGBX8888
    elseif bytesPP == 3 and bitsPP == 24 and
      self:isSupportedTextureFormat(sdl.PIXELFORMAT_RGBX8888, D3D.FORMAT_SUPPORT_TEXTURE2D) then
      return sdl.PIXELFORMAT_RGBX8888
    elseif bytesPP == 2 and bitsPP == 16 then
      if sdlFormat == sdl.PIXELFORMAT_ARGB4444 or
         sdlFormat == sdl.PIXELFORMAT_RGBA4444 or
         sdlFormat == sdl.PIXELFORMAT_ABGR4444 or
         sdlFormat == sdl.PIXELFORMAT_BGRA4444 then
         if self:isSupportedTextureFormat(sdl.PIXELFORMAT_ARGB4444, D3D.FORMAT_SUPPORT_TEXTURE2D) then
           return sdl.PIXELFORMAT_ARGB4444
         end 
      elseif sdlFormat == sdl.PIXELFORMAT_ARGB1555 or
         sdlFormat == sdl.PIXELFORMAT_RGBA5551 or
         sdlFormat == sdl.PIXELFORMAT_ABGR1555 or
         sdlFormat == sdl.PIXELFORMAT_BGRA5551 then
         if self:isSupportedTextureFormat(sdl.PIXELFORMAT_ARGB1555, D3D.FORMAT_SUPPORT_TEXTURE2D) then
           return sdl.PIXELFORMAT_ARGB1555
         end
      elseif sdlFormat == sdl.PIXELFORMAT_RGB565 or
        sdlFormat == sdl.PIXELFORMAT_BGR565 then
        if self:isSupportedTextureFormat(sdl.PIXELFORMAT_RGB565, D3D.FORMAT_SUPPORT_TEXTURE2D) then
          return sdl.PIXELFORMAT_RGB565
        end
      end
    elseif bytesPP == 1 and bitsPP == 8 and 
      self:isSupportedTextureFormat(sdl.PIXELFORMAT_A8, D3D.FORMAT_SUPPORT_TEXTURE2D) then
      return sdl.PIXELFORMAT_A8
    elseif self:isSupportedTextureFormat(sdl.PIXELFORMAT_ABGR8888, D3D.FORMAT_SUPPORT_TEXTURE2D) then
      --
      -- "catch all" format
      --
      return sdl.PIXELFORMAT_ABGR8888
    else
      error(string.format('unexpected SDL format [0x%x]', sdlFormat))
    end
  elseif sdlFormat < 256 then
    --
    -- DXGI format
    --
    return sdlFormat
  else
    --
    -- FOURCC format: supported DXGI format
    --
    return sdlFormat
  end
end
-------------------------------------------------------------------------
--
-- Object attached to a specific texture unit in the rendering pipeline.
-- When a texture is attached to the same texture unit the sampler applies
-- to that texture.
--
-------------------------------------------------------------------------
function ID3D11Device:createSampler(options)
  return DX11Sampler.create(self, options)
end

function ID3D11Device:present()
  --
  -- Specifying DXGI_PRESENT_TEST in the Flags parameter is analogous to IDirect3DDevice9::TestCooperativeLevel in Direct3D 9.
  --
  -- Possible return values include: 
  -- S_OK, DXGI_ERROR_DEVICE_RESET or DXGI_ERROR_DEVICE_REMOVED (see DXGI_ERROR), DXGI_STATUS_OCCLUDED (see DXGI_STATUS), or D3DDDIERR_DEVICEREMOVED.
  --
  _swapChain:present(_vsync, D3D.PRESENT_DEFAULT)
end

--- Reset device on resize backBuffers
function ID3D11Device:reset(window)
  _defaultRenderTarget = nil
  collectgarbage('collect')
  --
  -- ensure that all references are released
  --
  _context:clearState()
end
--- getImmediateContext - in DX11 this is the only context that can submit
-- rendering commands to the driver.
-- @param none
-- @return ID3D11DeviceContext object
function ID3D11Device:getImmediateContext()
  return _context
end

--- create new render target
-- @param color/depth/stencil buffers descriptor
function ID3D11Device:createRenderTarget(renderTargetDesc)
  for name, buffer in pairs(renderTargetDesc) do
    if not buffer.texture and bit.band(bit.rshift(buffer.format, 28), 0xf) == 1 then
      if string.match(name, 'color(%d)') then
        -- convert SDL image format to DXGI
        buffer.format = self:toNativeFormat(self:getBestTextureFormat(buffer.format))
      else
        -- map SDL depth format to DXGI
        buffer.format = self:toNativeFormat(buffer.format)
      end
    end
  end
  return DX11RenderTarget.create(self, renderTargetDesc)
end
--- scale backbuffer to fit window. D3D11 default handler does it automatically.
-- @param SDL window
function ID3D11Device:backBufferScale(window)
  agen.log.debug('video', "scale back buffer")
end
--- resize backbuffer to fit window
-- @param SDL window
function ID3D11Device:backBufferResize(window)
  local w, h = window:getSize()
  agen.log.debug('video', "TODO: resize back buffer to [%dx%d]", w, h)
  --[[
  -- You can't resize a swap chain unless you release all outstanding references to its 
  -- back buffers. You must release all of its direct and indirect references on the back 
  -- buffers in order for ResizeBuffers to succeed.
  -- Direct references are held by the application after it calls AddRef on a resource.
  -- Indirect references are held by views to a resource, binding a view of the resource to a 
  -- device context, a command list that used the resource, a command list that used a view to 
  -- that resource, a command list that executed another command list that used the resource, 
  -- and so on.
  -- Before you call ResizeBuffers, ensure that the application releases all references (by 
  -- calling the appropriate number of Release invocations) on the resources, any views to the 
  -- resource, and any command lists that use either the resources or views, and ensure that
  -- neither the resource nor a view is still bound to a device context. You can use 
  -- ID3D11DeviceContext::ClearState to ensure that all references are released. If a view 
  -- is bound to a deferred context, you must discard the partially built command list as 
  -- well (by calling ClearState, ID3D11DeviceContext::FinishCommandList, then Release on the 
  -- command list). After you call ResizeBuffers, you can re-query interfaces via IDXGISwapChain::GetBuffer.
  self:reset(window)
  -- _defaultRenderTarget now nil
  _swapChain:resizeBuffers(1, w, h, D3D.FORMAT_UNKNOWN)
  _defaultRenderTarget:setViewVolume(0, 0, 0, w, h, 0, 1)
  ]]
end

function ID3D11Device:reportStats()
  if _debug then
    _debug:reportStats()
  end
end

function ID3D11Device:createRasterizerState(rasterDesc)
  local rasterState = ffi.new('ID3D11RasterizerState*[1]')
  local hres = self.lpVtbl.CreateRasterizerState(self, rasterDesc, rasterState)
  if hres ~= 0 or rasterState[0] == nil then
    error(string.format("Failed to create rasterizer state: %s", D3D.GetError(hres)))
  end
  return ffi.gc(rasterState[0], D3D.Finalize)
end

function ID3D11Device:createDepthStencilState(depthStencilDesc)
  local dsState = ffi.new('ID3D11DepthStencilState*[1]')
  local hres = self.lpVtbl.CreateDepthStencilState(self, depthStencilDesc, dsState)
  if hres ~= 0 or dsState[0] == nil then
    error(string.format("Failed to create depth/stencil state: %s", D3D.GetError(hres)))
  end
  return ffi.gc(dsState[0], D3D.Finalize)
end

function ID3D11Device:createBlendState(blendDesc)
  local blendState = ffi.new('ID3D11BlendState*[1]')
  local hres = self.lpVtbl.CreateBlendState(self, blendDesc, blendState)
  if hres ~= 0 or blendState[0] == nil then
    error(string.format("Failed to create blend state: %s", D3D.GetError(hres)))
  end
  return ffi.gc(blendState[0], D3D.Finalize)
end

function ID3D11Device:createShaderResourceView(resource, resourceDesc)
	local resView = ffi.new('ID3D11ShaderResourceView*[1]')
	local hres = self.lpVtbl.CreateShaderResourceView(self, resource, resourceDesc, resView)
	if hres ~= 0 or resView[0] == nil then
	  error(string.format("Failed to create shader resource view from format [%d]: %s", tonumber(resourceDesc.Format), D3D.GetError(hres)))
	end
	return ffi.gc(resView[0], D3D.Finalize)
end

function ID3D11Device:createVertexShader(bufferPtr, bufferSize)
  local shader = ffi.new('ID3D11Shader*[1]')
  local hres = self.lpVtbl.CreateVertexShader(self, bufferPtr, bufferSize, nil, shader)
  if hres ~= 0 or shader[0] == nil then
    error(string.format("CreateVertexShader failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(shader[0], D3D.Finalize)
end

function ID3D11Device:createPixelShader(bufferPtr, bufferSize)
  local shader = ffi.new('ID3D11Shader*[1]')
  local hres = self.lpVtbl.CreatePixelShader(self, bufferPtr, bufferSize, nil, shader)
  if hres ~= 0 or shader[0] == nil then
    error(string.format("CreatePixelShader failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(shader[0], D3D.Finalize)
end
--- Requires D3D.FEATURE_LEVEL_10_0
function ID3D11Device:createGeometryShader(bufferPtr, bufferSize)
  local shader = ffi.new('ID3D11Shader*[1]')
  local hres = self.lpVtbl.CreateGeometryShader(self, bufferPtr, bufferSize, nil, shader)
  if hres ~= 0 or shader[0] == nil then
    error(string.format("CreateGeometryShader failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(shader[0], D3D.Finalize)
end

function ID3D11Device:createInputLayout(inputDesc, numElements, shaderCode, shaderCodeLen)
	local inputLayout = ffi.new('ID3D11InputLayout*[1]')
  local hres = self.lpVtbl.CreateInputLayout(self, inputDesc, numElements, shaderCode, shaderCodeLen, inputLayout)
  if hres ~= 0 or inputLayout[0] == nil then
    error(string.format("Failed to create input layout: %s", D3D.GetError(hres)))
  end
  return ffi.gc(inputLayout[0], D3D.Finalize)
end

function ID3D11Device:createSamplerState(samplerStateDesc)
	local samplerState = ffi.new('ID3D11SamplerState*[1]')
	local hres = self.lpVtbl.CreateSamplerState(self, samplerStateDesc, samplerState)
	if hres ~= 0 or samplerState[0] == nil then
		error(string.format("Failed to create sampler: %s", D3D.GetError(hres)))
	end
	return ffi.gc(samplerState[0], D3D.Finalize)
end

function ID3D11Device:createRenderTargetView(resource, viewDesc)
  local renderTargetView = ffi.new('ID3D11RenderTargetView*[1]')
  self:createRenderTargetViewNoGC(resource, viewDesc, renderTargetView)
  return ffi.gc(renderTargetView[0], D3D.Finalize)
end

function ID3D11Device:createRenderTargetViewNoGC(resource, viewDesc, renderTargetView)
  local hres = self.lpVtbl.CreateRenderTargetView(self, resource, viewDesc, renderTargetView)
  if hres ~= 0 or renderTargetView[0] == nil then
    error(string.format("CreateRenderTargetView failed: %s", D3D.GetError(hres)))
  end
end

function ID3D11Device:createDepthStencilView(resource, viewDesc)
  local depthBufferView = ffi.new('ID3D11DepthStencilView*[1]')
  self:createDepthStencilViewNoGC(resource, viewDesc, depthBufferView)
  return ffi.gc(depthBufferView[0], D3D.Finalize)
end

function ID3D11Device:createDepthStencilViewNoGC(resource, viewDesc, depthBufferView)
  local hres = self.lpVtbl.CreateDepthStencilView(self, resource, viewDesc, depthBufferView)
  if hres ~= 0 or depthBufferView[0] == nil then
	  error(string.format("CreateDepthStencilView with format [%d] failed: %s", tonumber(viewDesc.Format), D3D.GetError(hres)))
  end
end

function ID3D11Device:createCounter(desc)
  local ret = ffi.new('ID3D11Counter*[1]')
  local hres = self.lpVtbl.CreateCounter(self, desc, ret)
  if hres ~= 0 or ret[0] == nil then
    error(string.format("Failed to create counter: %s", D3D.GetError(hres)))
  end
  return ffi.gc(ret[0], D3D.Finalize)
end

function ID3D11Device:createQuery(desc)
  local ret = ffi.new('ID3D11Query*[1]')
  local hres = self.lpVtbl.CreateQuery(self, desc, ret)
  if hres ~= 0 or ret[0] == nil then
    error(string.format("Failed to create query: %s", D3D.GetError(hres)))
  end
  return ffi.gc(ret[0], D3D.Finalize)
end
--- Check if all bits in expect are present in the supported format options
function ID3D11Device:checkFormatSupport(format, expect)
  local flags = ffi.new('UINT[1]')
  agen.log.debug('video', "Checking format [%d]", format)
  local hres = self.lpVtbl.CheckFormatSupport(self, format, flags)
  if hres ~= 0 then
    error(string.format("CheckFormatSupport failed: %s", D3D.GetError(hres)))
  end
  agen.log.verbose('video', "format [%d] supported for [0x%x]", format, flags[0])
  if bit.band(flags[0], expect) ~= expect then
    agen.log.debug('video', "Format [%d] is not supported for flags [0x%x]", format, expect)
    return false
  end
  return true
end

function ID3D11Device:checkFeatureSupport(feature, data, dataLen)
  local hres = self.lpVtbl.CheckFeatureSupport(self, feature, data, dataLen)
  if hres ~= 0 then
    error(string.format("Failed to query device feature support: %s", D3D.GetError(hres)))
  end
  return data
end

function ID3D11Device:_createBuffer(bufferDesc, data)
  local buffer = ffi.new('ID3D11Buffer*[1]')
  local hres = self.lpVtbl.CreateBuffer(self, bufferDesc, data, buffer)
  if hres ~= 0 or buffer[0] == nil then
    error(string.format("Failed to create buffer: %s", D3D.GetError(hres)))
  end
	return ffi.gc(buffer[0], D3D.Finalize)
end

function ID3D11Device:createTexture2D(texDesc, data)
  local tex = ffi.new('ID3D11Texture*[1]')
  local hres = self.lpVtbl.CreateTexture2D(self, texDesc, data, tex)
  if hres ~= 0 or tex[0] == nil then
    error(string.format("Failed to create texture2D with format [%d]: %s", tonumber(texDesc.Format), D3D.GetError(hres)))
  end
  return ffi.gc(tex[0], D3D.Finalize)
end
--- createDeferredContext - create DX11 context to use in a separate thread
-- If you use the D3D.CREATE_DEVICE_SINGLETHREADED value to create the device that is 
-- represented by ID3D11Device, the CreateDeferredContext method will fail, and you will 
-- not be able to create a deferred context.
-- @param none
-- @return new ID3D11DeviceContext
function ID3D11Device:createDeferredContext()
	local ctx = ffi.new('ID3D11DeviceContext*[1]')
	local hres = self.lpVtbl.CreateDeferredContext(self, 0, ctx)
	if hres ~= 0 then
		error(string.format("Failed to create deferred context: %s", D3D.GetError(hres)))
	end
	return ffi.gc(ctx[0], D3D.Finalize)
end

function ID3D11Device:finalize()
  agen.log.error('video', 'FIXME: render states not released!')
--[[
  if _renderStates then
    agen.log.verbose('video', "releasing render states...")
    for i, _ in pairs(_renderStates) do
      _renderStates[i] = nil
    end
    _renderStates = nil
  end
]]
  --agen.log.verbose('video', "reporting stats")
  --self:reportStats()

  if _context then
    _context:clearState()
    _context = nil
  end

  _defaultRenderTarget = nil
  _swapChain = nil
  _infoQueue = nil
  _debug = nil
end

local ID3D11DeviceMT = {__index = ID3D11Device}
return ffi.metatype('ID3D11Device', ID3D11DeviceMT)
