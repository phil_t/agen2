local bit = require('bit')
local string = require('string')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi
local compiler = require('agen.video.DX11.reflection')

ffi.cdef([[
typedef enum _D3D11_UAV_DIMENSION { 
  UAV_DIMENSION_UNKNOWN         = 0,
  UAV_DIMENSION_BUFFER          = 1,
  UAV_DIMENSION_TEXTURE1D       = 2,
  UAV_DIMENSION_TEXTURE1DARRAY  = 3,
  UAV_DIMENSION_TEXTURE2D       = 4,
  UAV_DIMENSION_TEXTURE2DARRAY  = 5,
  UAV_DIMENSION_TEXTURE3D       = 8
} D3D11_UAV_DIMENSION;

typedef struct _D3D11_BUFFER_UAV {
  UINT FirstElement;
  UINT NumElements;
  UINT Flags;
} D3D11_BUFFER_UAV;

typedef struct _D3D11_TEX1D_UAV {
  UINT MipSlice;
} D3D11_TEX1D_UAV;

typedef struct _D3D11_TEX1D_ARRAY_UAV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX1D_ARRAY_UAV;

typedef struct _D3D11_TEX2D_UAV {
  UINT MipSlice;
} D3D11_TEX2D_UAV;

typedef struct _D3D11_TEX2D_ARRAY_UAV {
  UINT MipSlice;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2D_ARRAY_UAV;

typedef struct _D3D11_TEX3D_UAV {
  UINT MipSlice;
  UINT FirstWSlice;
  UINT WSize;
} D3D11_TEX3D_UAV;

typedef struct _D3D11_UNORDERED_ACCESS_VIEW_DESC {
  DXGI_FORMAT         Format;
  D3D11_UAV_DIMENSION ViewDimension;
  union {
    D3D11_BUFFER_UAV      Buffer;
    D3D11_TEX1D_UAV       Texture1D;
    D3D11_TEX1D_ARRAY_UAV Texture1DArray;
    D3D11_TEX2D_UAV       Texture2D;
    D3D11_TEX2D_ARRAY_UAV Texture2DArray;
    D3D11_TEX3D_UAV       Texture3D;
  };
} D3D11_UNORDERED_ACCESS_VIEW_DESC;

typedef struct _ID3D11ShaderResourceView ID3D11ShaderResourceView;
typedef struct _ID3D11ShaderResourceViewVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11ShaderResourceView* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11ShaderResourceView* This);
  ULONG (__stdcall *Release)(ID3D11ShaderResourceView* This);
  void (__stdcall *GetDevice)(ID3D11ShaderResourceView* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11ShaderResourceView* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11ShaderResourceView* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11ShaderResourceView* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetResource)(ID3D11ShaderResourceView* This, ID3D11Resource **ppResource);
  void (__stdcall *GetDesc)(ID3D11ShaderResourceView* This, D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc);
} ID3D11ShaderResourceViewVtbl;

struct _ID3D11ShaderResourceView {
  const ID3D11ShaderResourceViewVtbl* lpVtbl;
};
/*
 * An unordered access resource (which includes buffers, textures, and texture arrays - without 
 * multisampling), allows temporally unordered read/write access from multiple threads. This 
 * means that this resource type can be read/written simultaneously by multiple threads without 
 * generating memory conflicts through the use of Atomic Functions.
 * 
 * Create an unordered access buffer or texture by calling a function such as ID3D11Device::CreateBuffer 
 * or ID3D11Device::CreateTexture2D and passing in the D3D11_BIND_UNORDERED_ACCESS flag from the 
 * D3D11_BIND_FLAG enumeration.
 * Unordered access resources can only be bound to pixel shaders and compute shaders. During 
 * execution, pixel shaders or compute shaders running in parallel have the same unordered access
 * resources bound.
 */
typedef struct _ID3D11UnorderedAccessView ID3D11UnorderedAccessView;
typedef struct _ID3D11UnorderedAccessViewVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11UnorderedAccessView* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11UnorderedAccessView* This);
  ULONG (__stdcall *Release)(ID3D11UnorderedAccessView* This);
  void (__stdcall *GetDevice)(ID3D11UnorderedAccessView* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11UnorderedAccessView* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11UnorderedAccessView* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11UnorderedAccessView* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetResource)(ID3D11UnorderedAccessView* This, ID3D11Resource **ppResource);
  void (__stdcall *GetDesc)(ID3D11UnorderedAccessView* This, D3D11_UNORDERED_ACCESS_VIEW_DESC *pDesc);
} ID3D11UnorderedAccessViewVtbl;

struct _ID3D11UnorderedAccessView {
  const ID3D11UnorderedAccessViewVtbl* lpVtbl;
};

typedef struct _ID3D11ClassLinkage ID3D11ClassLinkage;
typedef struct _ID3D11ClassLinkageVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11ClassLinkage* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11ClassLinkage* This);
  ULONG (__stdcall *Release)(ID3D11ClassLinkage* This);
  void (__stdcall *GetDevice)(ID3D11ClassLinkage* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11ClassLinkage* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11ClassLinkage* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11ClassLinkage* This, REFGUID guid, const IUnknown *pData);
} ID3D11ClassLinkageVtbl;

struct _ID3D11ClassLinkage {
  const ID3D11ClassLinkageVtbl* lpVtbl;
};

typedef struct _ID3D11Shader ID3D11Shader;
typedef struct _ID3D11ShaderVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11Shader* This, REFIID riid, void** ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11Shader* This);
  ULONG (__stdcall *Release)(ID3D11Shader* This);
  void (__stdcall *GetDevice)(ID3D11Shader* This, ID3D11Device** ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11Shader* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11Shader* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Shader* This, REFGUID guid, const IUnknown *pData);
} ID3D11ShaderVtbl;

typedef struct _ID3D11Shader {
  const ID3D11ShaderVtbl* lpVtbl;
};
]])
----------------------------------------------------------------------
--
-- ID3D11ClassLinkage
--
----------------------------------------------------------------------
local ID3D11ClassLinkage = {}
setmetatable(ID3D11ClassLinkage, D3D.ID3D11DeviceChildMT)

local ID3D11ClassLinkageMT = {__index = ID3D11ClassLinkage}
ffi.metatype('ID3D11ClassLinkage', ID3D11ClassLinkageMT)
----------------------------------------------------------------------
--
-- ID3D11Shader
--
----------------------------------------------------------------------
local ID3D11Shader = {}
setmetatable(ID3D11Shader, D3D.ID3D11DeviceChildMT)

local ID3D11ShaderMT = {__index = ID3D11Shader}
ffi.metatype('ID3D11Shader', ID3D11ShaderMT)
----------------------------------------------------------------------
--
-- ID3D11ShaderResourceView
--
----------------------------------------------------------------------
local ID3D11ShaderResourceView = {}
setmetatable(ID3D11ShaderResourceView, D3D.ID3D11ViewMT)

local ID3D11ShaderResourceViewMT = {__index = ID3D11ShaderResourceView}
ffi.metatype('ID3D11ShaderResourceView', ID3D11ShaderResourceViewMT)
-----------------------------------------------------------------------
--
-- ID3D11UnorderedAccessView
--
-----------------------------------------------------------------------
local ID3D11UnorderedAccessView = {}
setmetatable(ID3D11UnorderedAccessView, D3D.ID3D11ViewMT)

local ID3D11UnorderedAccessViewMT = {__index = ID3D11UnorderedAccessView}
ffi.metatype('ID3D11UnorderedAccessView', ID3D11UnorderedAccessViewMT)
-------------------------------------------------------------------------
--
-- Agen2 shader program
--
-------------------------------------------------------------------------
local DX11ShaderProgram = {}
local DX11ShaderProgramMT = {__index = DX11ShaderProgram}

function DX11ShaderProgram.create(device, shaders)
  assert(device, "Invalid argument #1, ID3D11Device device expected, got nil")
  assert(ffi.istype('ID3D11Device', device), "Invalid argument #1, ID3D11Device expected")

  local self = {
    featureLevel = tonumber(device.lpVtbl.GetFeatureLevel(device)),
    vertex = {
      shader = nil,
      blob   = nil,
      reflection = nil,
    },
    pixel = {
      shader = nil,
      blob = nil,
      reflection = nil,
    },
    geometry = {
      shader = nil,
      blob = nil,
      reflection = nil,
    }
  }
  setmetatable(self, DX11ShaderProgramMT)
  --
  -- will compile
  --
  self:addVertexShader(device, shaders.vertex)
  self:addPixelShader(device, shaders.pixel)
  self:addGeometryShader(device, shaders.geometry)

  return self
end

local mapFeatureLevelToShaderProfile = {
  [tonumber(D3D.FEATURE_LEVEL_9_3)]  = '4_0_level_9_3',
  [tonumber(D3D.FEATURE_LEVEL_10_0)] = '4_0',
  [tonumber(D3D.FEATURE_LEVEL_10_1)] = '4_1',
  [tonumber(D3D.FEATURE_LEVEL_11_0)] = '5_0',
  [tonumber(D3D.FEATURE_LEVEL_11_1)] = '5_1',
}

function DX11ShaderProgram:getShaderProfile(prefix)
  assert(self.featureLevel, "Unknown device feature level")
  assert(mapFeatureLevelToShaderProfile[self.featureLevel], "Unsupported device feature level")
  local shaderProfile = prefix .. mapFeatureLevelToShaderProfile[self.featureLevel]
  agen.log.debug('video', "DX11: using shader profile [%s]", shaderProfile)
  return shaderProfile
end

function DX11ShaderProgram:setActive(ctx)
  ctx.lpVtbl.VSSetShader(ctx, self.vertex.shader, nil, 0)
  ctx.lpVtbl.PSSetShader(ctx, self.pixel.shader, nil, 0)
  -- can be nil
  ctx.lpVtbl.GSSetShader(ctx, self.geometry.shader, nil, 0)
end

function DX11ShaderProgram:addVertexShader(device, code)
  local bytecode, reflect = self:compileVertexShader(code)
  self.vertex.shader = device:createVertexShader(bytecode:getBuffer())
  self.vertex.blob = bytecode
  self.vertex.reflection = reflect
end

function DX11ShaderProgram:addPixelShader(device, code)
  local bytecode, reflect = self:compilePixelShader(code)
  self.pixel.shader = device:createPixelShader(bytecode:getBuffer())
  self.pixel.blob = bytecode
  self.pixel.reflection = reflect
end
-- optional
function DX11ShaderProgram:addGeometryShader(device, code)
  if not code then return end
  local bytecode, reflect = self:compileGeometryShader(code)
  self.geometry.shader = device:createGeometryShader(bytecode:getBuffer())
  self.geometry.blob = bytecode
  self.geometry.reflection = reflect
end

function DX11ShaderProgram:getFirstShaderCode()
  local blob = self.vertex.blob or self.geometry.blob or self.pixel.blob
  if not blob then
    return nil, 0
  end
  return blob:getBuffer()
end

function DX11ShaderProgram:cleanup()
  self.vertex.blob = nil
  self.geometry.blob = nil
  self.pixel.blob = nil
end

function DX11ShaderProgram:compileVertexShader(code)
  assert(code, "No shader code")
  return self:_compileShader("VShader", code, self:getShaderProfile('vs_'))
end

function DX11ShaderProgram:compilePixelShader(code)
  assert(code, "No shader code")
  return self:_compileShader("PShader", code, self:getShaderProfile('ps_'))
end

function DX11ShaderProgram:compileGeometryShader(code)
  return self:_compileShader("GShader", code, self:getShaderProfile('gs_'))
end
--- Compile a shader from a string
-- @param name shader name
-- @param code shader hlsl code
-- @param target shader profile (vs4_0)
-- @return managed blob and reflection interface
function DX11ShaderProgram:_compileShader(name, code, target)
  local bytecode = compiler.compile(code, name, "main", target)
  return bytecode, compiler.reflect(bytecode)
end
--- Lookup shader resource binding index by name
-- @param - shader resource name
-- @return - shader resource index
function DX11ShaderProgram:getVSResourceBindingIndex(name)
  local desc = self.vertex.reflection:getResourceBindingIndex(name)
  return desc.BindPoint
end
--- Lookup shader resource binding index by name
-- @param - shader resource name
-- @return - shader resource index
function DX11ShaderProgram:getGSResourceBindingIndex(name)
  local desc = self.geometry.reflection:getResourceBindingIndex(name)
  return desc.BindPoint
end
--- Lookup shader resource binding index by name
-- @param - shader resource name
-- @return - shader resource index
function DX11ShaderProgram:getPSResourceBindingIndex(name)
  local desc = self.pixel.reflection:getResourceBindingIndex(name)
  return desc.BindPoint
end

function DX11ShaderProgram:getSamplerIndex(name)
  local desc = self.pixel.reflection:getResourceBindingIndex(name)
  assert(desc.Type == compiler.SIT_TEXTURE, "shader resource not a texture")
  return desc.BindPoint
end

return DX11ShaderProgram
