local bit = require('bit')
local string = require('string')

local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi
local compiler
local reflectGUID

do
  local ok = false
  -- Windows 8.1
  ok, compiler = pcall(ffi.load, 'D3DCompiler_47')
  if ok then
    reflectGUID = ffi.new('GUID', {
      0x8d536ca1,
      0x0cca, 0x4956,
      {0xa8, 0x37, 0x78, 0x69, 0x63, 0x75, 0x55, 0x84}
    })
  else
    -- Windows 8.0
    ok, compiler = pcall(ffi.load, 'D3DCompiler_46')
    if ok then
      reflectGUID = ffi.new('GUID', {
        0x8d536ca1,
        0x0cca, 0x4956,
        {0xa8, 0x37, 0x78, 0x69, 0x63, 0x75, 0x55, 0x84}
      })
    else
      -- Windows 7
      ok, compiler = pcall(ffi.load, 'D3DCompiler_43')
      if ok then
        reflectGUID = ffi.new('GUID', {
          0x0a233719,
          0x3960, 0x4578,
          {0x9d, 0x7c, 0x20, 0x3b, 0x8b, 0x1d, 0x9c, 0xc1}
        })
      end
    end
  end
end

ffi.cdef([[
typedef enum _D3D11_NAME {
  NAME_UNDEFINED = 0x00000000,
} D3D11_NAME;

typedef enum _D3D11_REGISTER_COMPONENT_TYPE { 
  REGISTER_COMPONENT_UNKNOWN    = 0x00000000,
  REGISTER_COMPONENT_UINT32     = 1,
  REGISTER_COMPONENT_SINT32     = 2,
  REGISTER_COMPONENT_FLOAT32    = 3,
} D3D11_REGISTER_COMPONENT_TYPE;

typedef enum _D3D11_SHADER_VARIABLE_CLASS { 
  SVC_SCALAR               = 0,
  SVC_VECTOR               = 1,
  SVC_MATRIX_ROWS          = 2,
  SVC_MATRIX_COLUMNS       = 3,
  SVC_OBJECT               = 4,
  SVC_STRUCT               = 5,
  SVC_INTERFACE_CLASS      = 6,
  SVC_INTERFACE_POINTER    = 7,
  SVC_FORCE_DWORD      = 0x7fffffff
} D3D11_SHADER_VARIABLE_CLASS;

typedef enum _D3D11_SHADER_VARIABLE_TYPE { 
  SVT_VOID                         = 0,
  SVT_BOOL                         = 1,
  SVT_INT                          = 2,
  SVT_FLOAT                        = 3,
  SVT_STRING                       = 4,
  SVT_TEXTURE                      = 5,
  SVT_TEXTURE1D                    = 6,
  SVT_TEXTURE2D                    = 7,
  SVT_TEXTURE3D                    = 8,
  SVT_TEXTURECUBE                  = 9,
  SVT_SAMPLER                      = 10,
  SVT_SAMPLER1D                    = 11,
  SVT_SAMPLER2D                    = 12,
  SVT_SAMPLER3D                    = 13,
  SVT_SAMPLERCUBE                  = 14,
  SVT_PIXELSHADER                  = 15,
  SVT_VERTEXSHADER                 = 16,
  SVT_PIXELFRAGMENT                = 17,
  SVT_VERTEXFRAGMENT               = 18,
  SVT_UINT                         = 19,
  SVT_UINT8                        = 20,
  SVT_GEOMETRYSHADER               = 21,
  SVT_RASTERIZER                   = 22,
  SVT_DEPTHSTENCIL                 = 23,
  SVT_BLEND                        = 24,
  SVT_BUFFER                       = 25,
  SVT_CBUFFER                      = 26,
  SVT_TBUFFER                      = 27,
  SVT_TEXTURE1DARRAY               = 28,
  SVT_TEXTURE2DARRAY               = 29,
  SVT_RENDERTARGETVIEW             = 30,
  SVT_DEPTHSTENCILVIEW             = 31,
  SVT_TEXTURE2DMS                  = 32,
  SVT_TEXTURE2DMSARRAY             = 33,
  SVT_TEXTURECUBEARRAY             = 34,
  SVT_HULLSHADER                   = 35,
  SVT_DOMAINSHADER                 = 36,
  SVT_INTERFACE_POINTER            = 37,
  SVT_COMPUTESHADER                = 38,
  SVT_DOUBLE                       = 39,
  SVT_RWTEXTURE1D                  = 40,
  SVT_RWTEXTURE1DARRAY             = 41,
  SVT_RWTEXTURE2D                  = 42,
  SVT_RWTEXTURE2DARRAY             = 43,
  SVT_RWTEXTURE3D                  = 44,
  SVT_RWBUFFER                     = 45,
  SVT_BYTEADDRESS_BUFFER           = 46,
  SVT_RWBYTEADDRESS_BUFFER         = 47,
  SVT_STRUCTURED_BUFFER            = 48,
  SVT_RWSTRUCTURED_BUFFER          = 49,
  SVT_APPEND_STRUCTURED_BUFFER     = 50,
  SVT_CONSUME_STRUCTURED_BUFFER    = 51,
  SVT_MIN8FLOAT                    = 52,
  SVT_MIN10FLOAT                   = 53,
  SVT_MIN16FLOAT                   = 54,
  SVT_MIN12INT                     = 55,
  SVT_MIN16INT                     = 56,
  SVT_MIN16UINT                    = 57,
  SVT_FORCE_DWORD              = 0x7fffffff
} D3D11_SHADER_VARIABLE_TYPE;

typedef enum _D3D11_CBUFFER_TYPE { 
  CT_CBUFFER               = 0,
  CT_TBUFFER               = 1,
  CT_INTERFACE_POINTERS    = 2,
  CT_RESOURCE_BIND_INFO    = 3,
} D3D11_CBUFFER_TYPE;

typedef enum _D3D11_SHADER_VERSION_TYPE {
  SHVER_PIXEL_SHADER    = 0,
  SHVER_VERTEX_SHADER   = 1,
  SHVER_GEOMETRY_SHADER = 2,
  // D3D11 Shaders
  SHVER_HULL_SHADER     = 3,
  SHVER_DOMAIN_SHADER   = 4,
  SHVER_COMPUTE_SHADER  = 5,
} D3D11_SHADER_VERSION_TYPE;

typedef enum _D3D11_PRIMITIVE { 
  PRIMITIVE_UNDEFINED                 = 0,
  PRIMITIVE_POINT                     = 1,
  PRIMITIVE_LINE                      = 2,
  PRIMITIVE_TRIANGLE                  = 3,
  PRIMITIVE_LINE_ADJ                  = 6,
  PRIMITIVE_TRIANGLE_ADJ              = 7,
  PRIMITIVE_1_CONTROL_POINT_PATCH     = 8,
  // PRIMITIVE_n_CONTROL_POINT_PATCH
  PRIMITIVE_32_CONTROL_POINT_PATCH    = 39,
} D3D11_PRIMITIVE;

typedef enum _D3D11_TESSELLATOR_OUTPUT_PRIMITIVE { 
  TESSELLATOR_OUTPUT_UNDEFINED     = 0,
  TESSELLATOR_OUTPUT_POINT         = 1,
  TESSELLATOR_OUTPUT_LINE          = 2,
  TESSELLATOR_OUTPUT_TRIANGLE_CW   = 3,
  TESSELLATOR_OUTPUT_TRIANGLE_CCW  = 4
} D3D11_TESSELLATOR_OUTPUT_PRIMITIVE;

typedef enum _D3D11_TESSELLATOR_PARTITIONING { 
  TESSELLATOR_PARTITIONING_UNDEFINED        = 0,
  TESSELLATOR_PARTITIONING_INTEGER          = 1,
  TESSELLATOR_PARTITIONING_POW2             = 2,
  TESSELLATOR_PARTITIONING_FRACTIONAL_ODD   = 3,
  TESSELLATOR_PARTITIONING_FRACTIONAL_EVEN  = 4
} D3D11_TESSELLATOR_PARTITIONING;

typedef enum _D3D11_TESSELLATOR_DOMAIN { 
  TESSELLATOR_DOMAIN_UNDEFINED  = 0,
  TESSELLATOR_DOMAIN_ISOLINE    = 1,
  TESSELLATOR_DOMAIN_TRI        = 2,
  TESSELLATOR_DOMAIN_QUAD       = 3
} D3D11_TESSELLATOR_DOMAIN;

typedef enum _D3D11_SHADER_INPUT_TYPE { 
  SIT_CBUFFER                          = 0,
  SIT_TBUFFER                          = 1,
  SIT_TEXTURE                          = 2,
  SIT_SAMPLER                          = 3,
  SIT_UAV_RWTYPED                      = 4,
  SIT_STRUCTURED                       = 5,
  SIT_UAV_RWSTRUCTURED                 = 6,
  SIT_BYTEADDRESS                      = 7,
  SIT_UAV_RWBYTEADDRESS                = 8,
  SIT_UAV_APPEND_STRUCTURED            = 9,
  SIT_UAV_CONSUME_STRUCTURED           = 10,
  SIT_UAV_RWSTRUCTURED_WITH_COUNTER    = 11,
} D3D11_SHADER_INPUT_TYPE;

typedef enum _D3D11_RESOURCE_RETURN_TYPE { 
  RETURN_TYPE_UNORM      = 1,
  RETURN_TYPE_SNORM      = 2,
  RETURN_TYPE_SINT       = 3,
  RETURN_TYPE_UINT       = 4,
  RETURN_TYPE_FLOAT      = 5,
  RETURN_TYPE_MIXED      = 6,
  RETURN_TYPE_DOUBLE     = 7,
  RETURN_TYPE_CONTINUED  = 8
} D3D11_RESOURCE_RETURN_TYPE;

typedef struct _D3D11_SIGNATURE_PARAMETER_DESC {
  const char*                 SemanticName;   // Name of the semantic
  UINT                        SemanticIndex;  // Index of the semantic
  UINT                        Register;       // Number of member variables
  D3D11_NAME                    SystemValueType;// A predefined system value, or D3D_NAME_UNDEFINED if not applicable
  D3D11_REGISTER_COMPONENT_TYPE ComponentType;  // Scalar type (e.g. uint, float, etc.)
  int8_t                      Mask;           // Mask to indicate which components of the register
                                              // are used (combination of D3D10_COMPONENT_MASK values)
  int8_t                      ReadWriteMask;  // Mask to indicate whether a given component is 
                                              // never written (if this is an output signature) or
                                              // always read (if this is an input signature).
                                              // (combination of D3D10_COMPONENT_MASK values)
  UINT Stream;                                // Stream index
} D3D11_SIGNATURE_PARAMETER_DESC;

typedef struct _D3D11_SHADER_VARIABLE_DESC {
  const char* Name;           // Name of the variable
  UINT        StartOffset;    // Offset in constant buffer's backing store
  UINT        Size;           // Size of variable (in bytes)
  UINT        uFlags;         // Variable flags
  void*       DefaultValue;   // Raw pointer to default value
  UINT        StartTexture;   // First texture index (or -1 if no textures used)
  UINT        TextureSize;    // Number of texture slots possibly used.
  UINT        StartSampler;   // First sampler index (or -1 if no textures used)
  UINT        SamplerSize;    // Number of sampler slots possibly used.
} D3D11_SHADER_VARIABLE_DESC;

typedef struct _D3D11_SHADER_TYPE_DESC {
  D3D11_SHADER_VARIABLE_CLASS   Class;    // Variable class (e.g. object, matrix, etc.)
  D3D11_SHADER_VARIABLE_TYPE    Type;     // Variable type (e.g. float, sampler, etc.)
  UINT                        Rows;     // Number of rows (for matrices, 1 for other numeric, 0 if not applicable)
  UINT                        Columns;  // Number of columns (for vectors & matrices, 1 for other numeric, 0 if not applicable)
  UINT                        Elements; // Number of elements (0 if not an array)
  UINT                        Members;  // Number of members (0 if not a structure)
  UINT                        Offset;   // Offset from the start of structure (0 if not a structure member)
  const char*                 Name;     // Name of type, can be NULL
} D3D11_SHADER_TYPE_DESC;

typedef struct _D3D11_SHADER_BUFFER_DESC {
  const char*       Name;           // Name of the constant buffer
  D3D11_CBUFFER_TYPE  Type;           // Indicates type of buffer content
  UINT              Variables;      // Number of member variables
  UINT              Size;           // Size of CB (in bytes)
  UINT              uFlags;         // Buffer description flags
} D3D11_SHADER_BUFFER_DESC;

typedef struct _D3D11_SHADER_DESC {
  UINT          Version;                     // Shader version
  const char*   Creator;                     // Creator string
  UINT          Flags;                       // Shader compilation/parse flags
  
  UINT          ConstantBuffers;             // Number of constant buffers
  UINT          BoundResources;              // Number of bound resources
  UINT          InputParameters;             // Number of parameters in the input signature
  UINT          OutputParameters;            // Number of parameters in the output signature

  UINT          InstructionCount;            // Number of emitted instructions
  UINT          TempRegisterCount;           // Number of temporary registers used 
  UINT          TempArrayCount;              // Number of temporary arrays used
  UINT          DefCount;                    // Number of constant defines 
  UINT          DclCount;                    // Number of declarations (input + output)
  UINT          TextureNormalInstructions;   // Number of non-categorized texture instructions
  UINT          TextureLoadInstructions;     // Number of texture load instructions
  UINT          TextureCompInstructions;     // Number of texture comparison instructions
  UINT          TextureBiasInstructions;     // Number of texture bias instructions
  UINT          TextureGradientInstructions; // Number of texture gradient instructions
  UINT          FloatInstructionCount;       // Number of floating point arithmetic instructions used
  UINT          IntInstructionCount;         // Number of signed integer arithmetic instructions used
  UINT          UintInstructionCount;        // Number of unsigned integer arithmetic instructions used
  UINT          StaticFlowControlCount;      // Number of static flow control instructions used
  UINT          DynamicFlowControlCount;     // Number of dynamic flow control instructions used
  UINT          MacroInstructionCount;       // Number of macro instructions used
  UINT          ArrayInstructionCount;       // Number of array instructions used
  UINT          CutInstructionCount;         // Number of cut instructions used
  UINT          EmitInstructionCount;        // Number of emit instructions used
  D3D11_PRIMITIVE_TOPOLOGY   GSOutputTopology; // Geometry shader output topology
  UINT          GSMaxOutputVertexCount;      // Geometry shader maximum output vertex count
  D3D11_PRIMITIVE InputPrimitive;              // GS/HS input primitive
  UINT          PatchConstantParameters;     // Number of parameters in the patch constant signature
  UINT          cGSInstanceCount;            // Number of Geometry shader instances
  UINT          cControlPoints;              // Number of control points in the HS->DS stage
  D3D11_TESSELLATOR_OUTPUT_PRIMITIVE HSOutputPrimitive;  // Primitive output by the tessellator
  D3D11_TESSELLATOR_PARTITIONING HSPartitioning;         // Partitioning mode of the tessellator
  D3D11_TESSELLATOR_DOMAIN  TessellatorDomain; // Domain of the tessellator (quad, tri, isoline)
                                             // instruction counts
  UINT cBarrierInstructions;                 // Number of barrier instructions in a compute shader
  UINT cInterlockedInstructions;             // Number of interlocked instructions
  UINT cTextureStoreInstructions;            // Number of texture writes
} D3D11_SHADER_DESC;

typedef struct _D3D11_SHADER_INPUT_BIND_DESC {
  const char*               Name;           // Name of the resource
  D3D11_SHADER_INPUT_TYPE     Type;           // Type of resource (e.g. texture, cbuffer, etc.)
  UINT                      BindPoint;      // Starting bind point
  UINT                      BindCount;      // Number of contiguous bind points (for arrays)
                      
  UINT                      uFlags;         // Input binding flags
  D3D11_RESOURCE_RETURN_TYPE  ReturnType;     // Return type (if texture)
  D3D11_SRV_DIMENSION         Dimension;      // Dimension (if texture)
  UINT                      NumSamples;     // Number of samples (0 if not MS texture)
} D3D11_SHADER_INPUT_BIND_DESC;

typedef struct _D3D_SHADER_MACRO {
  const char* Name;
  const char* Definition;
} D3D_SHADER_MACRO;

typedef struct _ID3D11ShaderReflectionType ID3D11ShaderReflectionType;
typedef struct _ID3D11ShaderReflectionConstantBuffer ID3D11ShaderReflectionConstantBuffer;
typedef struct _ID3D11ShaderReflectionVariable ID3D11ShaderReflectionVariable;
typedef struct _ID3D11ShaderReflection ID3D11ShaderReflection;

typedef struct _ID3D11ShaderReflectionTypeVtbl {
  HRESULT (__stdcall* QueryInterface)(ID3D11ShaderReflectionType* self, REFIID iid, void** ppv);
  ULONG (__stdcall *AddRef)(ID3D11ShaderReflectionType* self);
  ULONG (__stdcall *Release)(ID3D11ShaderReflectionType* self);
  HRESULT (__stdcall *GetDesc)(ID3D11ShaderReflectionType* self, D3D11_SHADER_TYPE_DESC *pDesc);
  ID3D11ShaderReflectionType* (__stdcall *GetMemberTypeByIndex)(ID3D11ShaderReflectionType* self, UINT Index);
  ID3D11ShaderReflectionType* (__stdcall *GetMemberTypeByName)(ID3D11ShaderReflectionType* self, const char* Name);
  const char* (__stdcall *GetMemberTypeName)(ID3D11ShaderReflectionType* self, UINT Index);
  HRESULT (__stdcall *IsEqual)(ID3D11ShaderReflectionType* self, ID3D11ShaderReflectionType* pType);
  ID3D11ShaderReflectionType* (__stdcall *GetSubType)(ID3D11ShaderReflectionType* self);
  ID3D11ShaderReflectionType* (__stdcall *GetBaseClass)(ID3D11ShaderReflectionType* self);
  UINT (__stdcall *GetNumInterfaces)(ID3D11ShaderReflectionType* self);
  ID3D11ShaderReflectionType* (__stdcall *GetInterfaceByIndex)(ID3D11ShaderReflectionType* self, UINT uIndex);
  HRESULT (__stdcall *IsOfType)(ID3D11ShaderReflectionType* self, ID3D11ShaderReflectionType* pType);
  HRESULT (__stdcall *ImplementsInterface)(ID3D11ShaderReflectionType* self, ID3D11ShaderReflectionType* pBase);
} ID3D11ShaderReflectionTypeVtbl;

struct _ID3D11ShaderReflectionType {
  const ID3D11ShaderReflectionTypeVtbl* lpVtbl;
};

typedef struct _ID3D11ShaderReflectionVariableVtbl {
  HRESULT (__stdcall* QueryInterface)(ID3D11ShaderReflectionVariable* self, REFIID iid, void** ppv);
  ULONG (__stdcall *AddRef)(ID3D11ShaderReflectionVariable* self);
  ULONG (__stdcall *Release)(ID3D11ShaderReflectionVariable* self);
  HRESULT (__stdcall *GetDesc)(ID3D11ShaderReflectionVariable* self, D3D11_SHADER_VARIABLE_DESC *pDesc);
  ID3D11ShaderReflectionType* (__stdcall *GetType)(ID3D11ShaderReflectionVariable* self);
  ID3D11ShaderReflectionConstantBuffer* (__stdcall *GetBuffer)(ID3D11ShaderReflectionVariable* self);
  UINT (__stdcall *GetInterfaceSlot)(ID3D11ShaderReflectionVariable* self, UINT uArrayIndex);
} ID3D11ShaderReflectionVariableVtbl;

struct __ID3D11ShaderReflectionVariable {
  const ID3D11ShaderReflectionVariableVtbl* lpVtbl;
};

typedef struct _ID3D11ShaderReflectionConstantBufferVtbl {
  HRESULT (__stdcall* QueryInterface)(ID3D11ShaderReflectionConstantBuffer* self, REFIID iid, void** ppv);
  ULONG (__stdcall *AddRef)(ID3D11ShaderReflectionConstantBuffer* self);
  ULONG (__stdcall *Release)(ID3D11ShaderReflectionConstantBuffer* self);
  HRESULT (__stdcall *GetDesc)(ID3D11ShaderReflectionConstantBuffer* self, D3D11_SHADER_BUFFER_DESC *pDesc);
  ID3D11ShaderReflectionVariable* (__stdcall *GetVariableByIndex)(ID3D11ShaderReflectionConstantBuffer* self, UINT Index);
  ID3D11ShaderReflectionVariable* (__stdcall *GetVariableByName)(ID3D11ShaderReflectionConstantBuffer* self, const char* Name);
} ID3D11ShaderReflectionConstantBufferVtbl;

struct _ID3D11ShaderReflectionConstantBuffer {
  const ID3D11ShaderReflectionConstantBufferVtbl* lpVtbl;
};

typedef struct _ID3D11ShaderReflectionVtbl {
  HRESULT (__stdcall* QueryInterface)(ID3D11ShaderReflection* self, REFIID iid, void** ppv);
  ULONG (__stdcall *AddRef)(ID3D11ShaderReflection* self);
  ULONG (__stdcall *Release)(ID3D11ShaderReflection* self);
  HRESULT (__stdcall* GetDesc)(ID3D11ShaderReflection* self, D3D11_SHADER_DESC *pDesc);
  ID3D11ShaderReflectionConstantBuffer* (__stdcall* GetConstantBufferByIndex)(ID3D11ShaderReflection* self, UINT Index);
  ID3D11ShaderReflectionConstantBuffer* (__stdcall* GetConstantBufferByName)(ID3D11ShaderReflection* self, const char* Name);
  HRESULT (__stdcall* GetResourceBindingDesc)(ID3D11ShaderReflection* self, UINT ResourceIndex, D3D11_SHADER_INPUT_BIND_DESC *pDesc);
  HRESULT (__stdcall* GetInputParameterDesc)(ID3D11ShaderReflection* self, UINT ParameterIndex, D3D11_SIGNATURE_PARAMETER_DESC *pDesc);
  HRESULT (__stdcall* GetOutputParameterDesc)(ID3D11ShaderReflection* self,UINT ParameterIndex, D3D11_SIGNATURE_PARAMETER_DESC *pDesc);
  HRESULT (__stdcall* GetPatchConstantParameterDesc)(ID3D11ShaderReflection* self, UINT ParameterIndex, D3D11_SIGNATURE_PARAMETER_DESC *pDesc);
  ID3D11ShaderReflectionVariable* (__stdcall* GetVariableByName)(ID3D11ShaderReflection* self, const char* Name);
  HRESULT (__stdcall *GetResourceBindingDescByName)(ID3D11ShaderReflection* self, const char* Name, D3D11_SHADER_INPUT_BIND_DESC *pDesc);
  UINT (__stdcall* GetMovInstructionCount)(ID3D11ShaderReflection* self);
  UINT (__stdcall* GetMovcInstructionCount)(ID3D11ShaderReflection* self);
  UINT (__stdcall* GetConversionInstructionCount)(ID3D11ShaderReflection* self);
  UINT (__stdcall* GetBitwiseInstructionCount)(ID3D11ShaderReflection* self);
  D3D11_PRIMITIVE (__stdcall* GetGSInputPrimitive)(ID3D11ShaderReflection* self);
  BOOL (__stdcall* IsSampleFrequencyShader)(ID3D11ShaderReflection* self);
  UINT (__stdcall* GetNumInterfaceSlots)(ID3D11ShaderReflection* self);
  HRESULT (__stdcall* GetMinFeatureLevel)(ID3D11ShaderReflection* self, enum D3D_FEATURE_LEVEL* pLevel);
  UINT (__stdcall* GetThreadGroupSize)(ID3D11ShaderReflection* self, UINT* pSizeX, UINT* pSizeY, UINT* pSizeZ);
} ID3D11ShaderReflectionVtbl;

struct _ID3D11ShaderReflection {
  const ID3D11ShaderReflectionVtbl* lpVtbl;
};

typedef struct _ID3DBlob ID3DBlob;
typedef struct _ID3DBlobVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3DBlob* this, REFIID riid, void** ppvObject); 
  ULONG (__stdcall *AddRef)(ID3DBlob* this);
  ULONG (__stdcall *Release)(ID3DBlob* this);
  void* (__stdcall *GetBufferPointer)(ID3DBlob* this);
  size_t (__stdcall *GetBufferSize)(ID3DBlob* this);
} ID3DBlobVtbl;

struct _ID3DBlob {
  const ID3DBlobVtbl* lpVtbl;
};

typedef enum _D3DCOMPILE_FLAGS {
  COMPILE_DEBUG                  = 0x00000001,
  SKIP_VALIDATION                = 0x00000002,
  PACK_MATRIX_COLUMN_MAJOR       = (1 << 4),
  ENABLE_BACKWARDS_COMPATIBILITY = (1 << 12),
  PACK_MATRIX_ROW_MAJOR          = (1 << 13),
  OPTIMIZATION_LEVEL0            = (1 << 14),
  //OPTIMIZATION_LEVEL2          = ((1 << 14)|(1 << 15)),
  OPTIMIZATION_LEVEL3            = (1 << 15),
} D3DCOMPILE_FLAGS;

HRESULT __stdcall D3DCompile(const char* pSrcData, size_t SrcDataSize,
  const void* pSourceName,
  const D3D_SHADER_MACRO* pDefines,
  // ID3DInclude 
  void* pInclude,
  const char* pEntrypoint,
  const char* pTarget,
  D3DCOMPILE_FLAGS Flags1,
  UINT Flags2,
  ID3DBlob** ppCode,
  ID3DBlob** ppErrorMsgs
);

HRESULT __stdcall D3DReflect(const void* srcData, size_t srcDataSize, REFIID interface, ID3D11ShaderReflection** Reflector);
]])
---------------------------------------------------------------------
--
-- Blob
--
---------------------------------------------------------------------
local ID3DBlob = {}
setmetatable(ID3DBlob, D3D.IUnknownMT)

function ID3DBlob:getBuffer()
  local ptr  = self.lpVtbl.GetBufferPointer(self)
  local size = self.lpVtbl.GetBufferSize(self)
  return ptr, size
end

local ID3DBlobMT = {__index = ID3DBlob}
ffi.metatype('ID3DBlob', ID3DBlobMT)
--------------------------------------------------------------------
--
-- ID3D11ShaderReflectionType
--
--------------------------------------------------------------------
local ID3D11ShaderReflectionType = {}
setmetatable(ID3D11ShaderReflectionType, D3D.IUnknownMT)

ID3D11ShaderReflectionType.GUID = ffi.new('GUID', {
  0x6e6ffa6a, 
  0x9bae, 0x4613, 
  {0xa5, 0x1e, 0x91, 0x65, 0x2d, 0x50, 0x8c, 0x21}
})

function ID3D11ShaderReflectionType:getDesc()
  local ret = ffi.new('D3D11_SHADER_TYPE_DESC')
  local hres = self.lpVtbl.GetDesc(self, ret)
  if hres ~= 0 then
    error(string.format("Failed to get Shader Reflection Type description %s", D3D.GetError(hres)))
  end  
  return ret
end

local ID3D11ShaderReflectionTypeMT = {
  __index = ID3D11ShaderReflectionType,
  __eq = function(self, pType)
    return self.lpVtbl.IsEqual(self, pType) == 0
  end,
}
ffi.metatype('ID3D11ShaderReflectionType', ID3D11ShaderReflectionTypeMT)
--------------------------------------------------------------------
--
-- ID3D11ShaderReflectionVariable
--
--------------------------------------------------------------------
local ID3D11ShaderReflectionVariable = {}
setmetatable(ID3D11ShaderReflectionVariable, D3D.IUnknownMT)

ID3D11ShaderReflectionVariable.GUID = ffi.new('GUID', { 
  0x51f23923, 
  0xf3e5, 0x4bd1, 
  {0x91, 0xcb, 0x60, 0x61, 0x77, 0xd8, 0xdb, 0x4c}
})

function ID3D11ShaderReflectionVariable:getDesc()
  local ret = ffi.new('D3D11_SHADER_VARIABLE_DESC')
  local hres = self.lpVtbl.GetDesc(self. ret)
  if hres ~= 0 then
    error(string.format("Failed to get Shader variable description %s", D3D.GetError(hres)))
  end
  return ret
end

function ID3D11ShaderReflectionVariable:getType()
  local ret = self.lpVtbl.GetType(self)
  if ret == nil then
    error("Failed to get Shader variable type")
  end
  return ret
end

local ID3D11ShaderReflectionVariableMT = {__index = ID3D11ShaderReflectionVariable}
ffi.metatype('ID3D11ShaderReflectionVariable', ID3D11ShaderReflectionVariableMT)
--------------------------------------------------------------------
--
-- ID3D11ShaderReflectionConstantBuffer
--
--------------------------------------------------------------------
local ID3D11ShaderReflectionConstantBuffer = {}
setmetatable(ID3D11ShaderReflectionConstantBuffer, D3D.IUnknownMT)

ID3D11ShaderReflectionConstantBuffer.GUID = ffi.new('GUID', {
  0xeb62d63d, 
  0x93dd, 0x4318, 
  {0x8a, 0xe8, 0xc6, 0xf8, 0x3a, 0xd3, 0x71, 0xb8}
})

local ID3D11ShaderReflectionConstantBufferMT = {__index = ID3D11ShaderReflectionConstantBuffer}
ffi.metatype('ID3D11ShaderReflectionConstantBuffer', ID3D11ShaderReflectionConstantBufferMT)
--------------------------------------------------------------------
--
-- ID3D11ShaderReflection
--
--------------------------------------------------------------------
local ID3D11ShaderReflection = {}
setmetatable(ID3D11ShaderReflection, D3D.IUnknownMT)

ID3D11ShaderReflection.GUID = reflectGUID

function ID3D11ShaderReflection:getDesc()
  local ret = ffi.new('D3D11_SHADER_DESC')
  local hres = self.lpVtbl.GetDesc(self, ret)
  if hres ~= 0 then
    error(string.format("Failed to get reflection description: %s", D3D.GetError(hres)))
  end
  return ret
end

function ID3D11ShaderReflection:getVar(name)
  local ret = self.lpVtbl.GetVariableByName(self, name)
  if ret == nil then
    error(string.format("No variable [%s] in shader", name))
  end
  return ffi.gc(ret, D3D.Finalize)
end

function ID3D11ShaderReflection:getResourceBindingIndex(name)
  assert(name and name ~= "", "Invalid argument #2: resource name cannot be empty")
  local desc = ffi.new('D3D11_SHADER_INPUT_BIND_DESC')
  local hres = self.lpVtbl.GetResourceBindingDescByName(self, name, desc) 
  if hres ~= 0 then
    error(string.format("Failed to get resource [%s] binding description: [%s] Note: the resource might have been optimized away by the shader compiler if not referenced in the shader code.", 
      name,
      D3D.GetError(hres)))
  end
  return desc
end

local ID3D11ShaderReflectionMT = {__index = ID3D11ShaderReflection}
ffi.metatype('ID3D11ShaderReflection', ID3D11ShaderReflectionMT)
----------------------------------------------------------------------
--
-- D3D Shader Compiler interface
--
----------------------------------------------------------------------
local D3DCompiler = {}
setmetatable(D3DCompiler, {__index = compiler})

function D3DCompiler.reflect(code)
  local ref  = ffi.new('ID3D11ShaderReflection*[1]')
  local buffer, size = code:getBuffer()
  local hres = compiler.D3DReflect(buffer, size, ID3D11ShaderReflection.GUID, ref)
  if hres ~= 0 then
    error(string.format("Failed to create shader reflector:[%s]", D3D.GetError(hres)))
  end
  return ffi.gc(ref[0], D3D.Finalize)
end

function D3DCompiler.compile(code, name, entryPoint, target)
  local bytecode = ffi.new('ID3DBlob*[1]')
  local err      = ffi.new('ID3DBlob*[1]')
  local hres     = compiler.D3DCompile(code, #code, name, nil, nil, entryPoint, target,
    compiler.OPTIMIZATION_LEVEL3,
    0, bytecode, err)

  if bytecode[0] ~= nil then
    bytecode = ffi.gc(bytecode[0], D3D.Finalize)
  end

  if err[0] ~= nil then
    err = ffi.gc(err[0], D3D.Finalize)
  end

  if hres ~= 0 or bytecode[0] == nil then
    error(string.format("Failed to compile shader [%s]: [%s]", target, ffi.string(err:getBuffer())))
  end

  if err[0] ~= nil then
    agen.log.debug('video', "Shader compile error:[%s]", ffi.string(err:getBuffer()))
  end

  return bytecode
end

return D3DCompiler
