local string = require('string')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_TEXTURE_ADDRESS_MODE {
  TEXTURE_ADDRESS_WRAP	= 1,
  TEXTURE_ADDRESS_MIRROR	= 2,
  TEXTURE_ADDRESS_CLAMP	= 3,
  TEXTURE_ADDRESS_BORDER	= 4,
  TEXTURE_ADDRESS_MIRROR_ONCE	= 5,
} D3D11_TEXTURE_ADDRESS_MODE;

typedef enum _D3D11_FILTER {
  FILTER_MIN_MAG_MIP_POINT	= 0x00000000,
  FILTER_MIN_MAG_POINT_MIP_LINEAR	= 0x1,
  FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT	= 0x4,
  FILTER_MIN_POINT_MAG_MIP_LINEAR	= 0x5,
  FILTER_MIN_LINEAR_MAG_MIP_POINT	= 0x10,
  FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR	= 0x11,
  FILTER_MIN_MAG_LINEAR_MIP_POINT	= 0x14,
  FILTER_MIN_MAG_MIP_LINEAR	= 0x15,
  FILTER_ANISOTROPIC	= 0x55,
  FILTER_COMPARISON_MIN_MAG_MIP_POINT	= 0x80,
  FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR	= 0x81,
  FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT	= 0x84,
  FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR	= 0x85,
  FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT	= 0x90,
  FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR	= 0x91,
  FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT	= 0x94,
  FILTER_COMPARISON_MIN_MAG_MIP_LINEAR	= 0x95,
  FILTER_COMPARISON_ANISOTROPIC	= 0xd5,
  FILTER_MINIMUM_MIN_MAG_MIP_POINT	= 0x100,
  FILTER_MINIMUM_MIN_MAG_POINT_MIP_LINEAR	= 0x101,
  FILTER_MINIMUM_MIN_POINT_MAG_LINEAR_MIP_POINT	= 0x104,
  FILTER_MINIMUM_MIN_POINT_MAG_MIP_LINEAR	= 0x105,
  FILTER_MINIMUM_MIN_LINEAR_MAG_MIP_POINT	= 0x110,
  FILTER_MINIMUM_MIN_LINEAR_MAG_POINT_MIP_LINEAR	= 0x111,
  FILTER_MINIMUM_MIN_MAG_LINEAR_MIP_POINT	= 0x114,
  FILTER_MINIMUM_MIN_MAG_MIP_LINEAR	= 0x115,
  FILTER_MINIMUM_ANISOTROPIC	= 0x155,
  FILTER_MAXIMUM_MIN_MAG_MIP_POINT	= 0x180,
  FILTER_MAXIMUM_MIN_MAG_POINT_MIP_LINEAR	= 0x181,
  FILTER_MAXIMUM_MIN_POINT_MAG_LINEAR_MIP_POINT	= 0x184,
  FILTER_MAXIMUM_MIN_POINT_MAG_MIP_LINEAR	= 0x185,
  FILTER_MAXIMUM_MIN_LINEAR_MAG_MIP_POINT	= 0x190,
  FILTER_MAXIMUM_MIN_LINEAR_MAG_POINT_MIP_LINEAR	= 0x191,
  FILTER_MAXIMUM_MIN_MAG_LINEAR_MIP_POINT	= 0x194,
  FILTER_MAXIMUM_MIN_MAG_MIP_LINEAR	= 0x195,
  FILTER_MAXIMUM_ANISOTROPIC	= 0x1d5
} D3D11_FILTER;

typedef struct _D3D11_SAMPLER_DESC {
  D3D11_FILTER Filter;
  D3D11_TEXTURE_ADDRESS_MODE AddressU;
  D3D11_TEXTURE_ADDRESS_MODE AddressV;
  D3D11_TEXTURE_ADDRESS_MODE AddressW;
  float MipLODBias;
  UINT MaxAnisotropy;
  D3D11_COMPARISON_FUNC ComparisonFunc;
  float BorderColor[4];
  float MinLOD;
  float MaxLOD;
} D3D11_SAMPLER_DESC;

typedef struct _ID3D11SamplerState ID3D11SamplerState;
typedef struct _ID3D11SamplerStateVtbl {
  HRESULT (__stdcall *QueryInterface)(ID3D11SamplerState* This, REFIID riid, void **ppvObject);
  ULONG (__stdcall *AddRef)(ID3D11SamplerState* This);
  ULONG (__stdcall *Release)(ID3D11SamplerState* This);
  void (__stdcall *GetDevice)(ID3D11SamplerState* This, ID3D11Device **ppDevice);
  HRESULT (__stdcall *GetPrivateData)(ID3D11SamplerState* This, REFGUID guid, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *SetPrivateData)(ID3D11SamplerState* This, REFGUID guid, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11SamplerState* This, REFGUID guid, const IUnknown *pData);
  void (__stdcall *GetDesc)(ID3D11SamplerState* This, D3D11_SAMPLER_DESC *pDesc);
} ID3D11SamplerStateVtbl;

struct _ID3D11SamplerState {
  const ID3D11SamplerStateVtbl* lpVtbl;
};
]])

local function samplerFilterFromOptions(options)
  if not options then
    options = {}
  end

  if options.min == nil then
    options.min = 'LINEAR'
  end

  if options.mag == nil then
    options.mag = 'LINEAR'
  end

  if options.mip == nil then
    options.mip = 'LINEAR'
  end

  if options.min == options.mag and options.mag == options.mip then
    return string.format("FILTER_MIN_MAG_MIP_%s", options.min)
  elseif options.min == options.mag and options.mag ~= options.mip then
    return string.format("FILTER_MIN_MAG_%s_MIP_%s", options.min, options.mip)
  elseif options.min ~= options.mag and options.mag == options.mip then
    return string.format("FILTER_MIN_%s_MAG_MIP_%s", options.min, options.mag)
  elseif options.min ~= options.mag and options.mag ~= options.mip then
    return string.format("FILTER_MIN_%s_MAG_%s_MIP_%s", options.min, options.mag, options.mag)
  end
  return "FILTER_MIN_MAG_MIP_POINT"
end

local samplerWrapOptions = {
  wrap   = D3D.TEXTURE_ADDRESS_WRAP,
  mirror = D3D.TEXTURE_ADDRESS_MIRROR,
  clamp  = D3D.TEXTURE_ADDRESS_CLAMP,
  border = D3D.TEXTURE_ADDRESS_BORDER,
  mirrorOnce = D3D.TEXTURE_ADDRESS_MIRROR_ONCE,
}
  
local ID3D11SamplerState = {}
setmetatable(ID3D11SamplerState, D3D.ID3D11DeviceChildMT)
--
-- At feature levels 9_1, 9_2 and 9_3, the display device supports the use of 2-D textures with dimensions that are 
-- not powers of two under two conditions. First, only one MIP-map level for each texture can be created, and second, 
-- no wrap sampler modes for textures are allowed (that is, the AddressU, AddressV, and AddressW members of D3D11_SAMPLER_DESC 
-- cannot be set to D3D11_TEXTURE_ADDRESS_WRAP).
--
function ID3D11SamplerState.create(device, options)  
	local samplerStateDesc = ffi.new('D3D11_SAMPLER_DESC', {
		Filter					= D3D[samplerFilterFromOptions(options)],
		AddressU				= options and samplerWrapOptions[options.address] or D3D.TEXTURE_ADDRESS_CLAMP,
		AddressV				= options and samplerWrapOptions[options.address] or D3D.TEXTURE_ADDRESS_CLAMP,
		AddressW				= options and samplerWrapOptions[options.address] or D3D.TEXTURE_ADDRESS_CLAMP,
		MipLODBias			= 0,
    --
    --  Clamping value used if D3D11_FILTER_ANISOTROPIC or D3D11_FILTER_COMPARISON_ANISOTROPIC 
    -- is specified in Filter. Valid values are between 1 and 16.
    --
		MaxAnisotropy		= 1,
    --
    -- Comparison filters only work with textures that have the following DXGI formats: 
    -- R32_FLOAT_X8X24_TYPELESS, R32_FLOAT, R24_UNORM_X8_TYPELESS, R16_UNORM.
    --
		ComparisonFunc	= D3D.COMPARISON_NEVER,
    --
    -- Border color to use if D3D11_TEXTURE_ADDRESS_BORDER is specified for 
    -- AddressU, AddressV, or AddressW. Range must be between 0.0 and 1.0 inclusive.
    --
		BorderColor			= {1, 1, 1, 1},
		MinLOD					= -3.402823466e+38,
    -- To have no upper limit on LOD set this to a large value such as D3D11_FLOAT32_MAX.
		MaxLOD					= 3.402823466e+38,
	})
	return device:createSamplerState(samplerStateDesc)
end

function ID3D11SamplerState:getDesc()
  local desc = ffi.new('D3D11_SAMPLER_DESC')
  self.lpVtbl.GetDesc(self, desc)
  return desc
end

local ID3D11SamplerStateMT = {__index = ID3D11SamplerState}
return ffi.metatype('ID3D11SamplerState', ID3D11SamplerStateMT)
