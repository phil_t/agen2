local string = require('string')
local bit = require('bit')

local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _D3D11_SRV_DIMENSION { 
  SRV_DIMENSION_UNKNOWN           = 0x00000000,
  SRV_DIMENSION_BUFFER            = 1,
  SRV_DIMENSION_TEXTURE1D         = 2,
  SRV_DIMENSION_TEXTURE1DARRAY    = 3,
  SRV_DIMENSION_TEXTURE2D         = 4,
  SRV_DIMENSION_TEXTURE2DARRAY    = 5,
  SRV_DIMENSION_TEXTURE2DMS       = 6,
  SRV_DIMENSION_TEXTURE2DMSARRAY  = 7,
  SRV_DIMENSION_TEXTURE3D         = 8,
  SRV_DIMENSION_TEXTURECUBE       = 9,
  SRV_DIMENSION_TEXTURECUBEARRAY  = 10,
  SRV_DIMENSION_BUFFEREX          = 11
} D3D11_SRV_DIMENSION;

typedef struct _D3D11_TEXTURE1D_DESC {
  UINT Width;
  UINT MipLevels;
  UINT ArraySize;
  DXGI_FORMAT Format;
  D3D11_USAGE Usage;
  UINT BindFlags;
  UINT CPUAccessFlags;
  UINT MiscFlags;
} D3D11_TEXTURE1D_DESC;

typedef struct _D3D11_TEXTURE2D_DESC {
  UINT Width;
  UINT Height;
  UINT MipLevels;
  UINT ArraySize;
  DXGI_FORMAT Format;
  DXGI_SAMPLE_DESC SampleDesc;
  D3D11_USAGE Usage;
  UINT BindFlags;
  UINT CPUAccessFlags;
  UINT MiscFlags;
} D3D11_TEXTURE2D_DESC;

typedef struct _D3D11_TEXTURE3D_DESC {
  UINT Width;
  UINT Height;
  UINT Depth;
  UINT MipLevels;
  DXGI_FORMAT Format;
  D3D11_USAGE Usage;
  UINT BindFlags;
  UINT CPUAccessFlags;
  UINT MiscFlags;
} D3D11_TEXTURE3D_DESC;

typedef struct _D3D11_BUFFER_SRV {
  union {
    UINT FirstElement;
    UINT ElementOffset;
  };
  union {
    UINT NumElements;
    UINT ElementWidth;
  };
} D3D11_BUFFER_SRV;

typedef struct _D3D11_TEX1D_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
} D3D11_TEX1D_SRV;

typedef struct _D3D11_TEX1D_ARRAY_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX1D_ARRAY_SRV;

typedef struct _D3D11_TEX2D_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
} D3D11_TEX2D_SRV;

typedef struct _D3D11_TEX2D_ARRAY_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2D_ARRAY_SRV;

typedef struct _D3D11_TEX2DMS_SRV {
  UINT UnusedField_NothingToDefine;
} D3D11_TEX2DMS_SRV;

typedef struct _D3D11_TEX2DMS_ARRAY_SRV {
  UINT FirstArraySlice;
  UINT ArraySize;
} D3D11_TEX2DMS_ARRAY_SRV;

typedef struct _D3D11_TEX3D_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
} D3D11_TEX3D_SRV;

typedef struct _D3D11_TEXCUBE_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
} D3D11_TEXCUBE_SRV;

typedef struct _D3D11_TEXCUBE_ARRAY_SRV {
  UINT MostDetailedMip;
  UINT MipLevels;
  UINT First2DArrayFace;
  UINT NumCubes;
} D3D11_TEXCUBE_ARRAY_SRV;

typedef struct _D3D11_BUFFEREX_SRV {
  UINT FirstElement;
  UINT NumElements;
  UINT Flags;
} D3D11_BUFFEREX_SRV;
	
typedef struct _D3D11_SHADER_RESOURCE_VIEW_DESC {
  DXGI_FORMAT         Format;
  D3D11_SRV_DIMENSION ViewDimension;
  union {
    D3D11_BUFFER_SRV        Buffer;
    D3D11_TEX1D_SRV         Texture1D;
    D3D11_TEX1D_ARRAY_SRV   Texture1DArray;
    D3D11_TEX2D_SRV         Texture2D;
    D3D11_TEX2D_ARRAY_SRV   Texture2DArray;
    D3D11_TEX2DMS_SRV       Texture2DMS;
    D3D11_TEX2DMS_ARRAY_SRV Texture2DMSArray;
    D3D11_TEX3D_SRV         Texture3D;
    D3D11_TEXCUBE_SRV       TextureCube;
    D3D11_TEXCUBE_ARRAY_SRV TextureCubeArray;
    D3D11_BUFFEREX_SRV      BufferEx;
  };
} D3D11_SHADER_RESOURCE_VIEW_DESC;

typedef struct _ID3D11Texture ID3D11Texture;
typedef struct _ID3D11TextureVtbl {
	HRESULT (__stdcall *QueryInterface)(ID3D11Texture* this, REFIID riid, void** ppvObj);
  ULONG   (__stdcall *AddRef)(ID3D11Texture* this);
  ULONG   (__stdcall *Release)(ID3D11Texture* this);
	void (__stdcall *GetDevice)(ID3D11Texture* This, ID3D11Device **ppDevice);
	HRESULT (__stdcall *GetPrivateData)(ID3D11Texture* This, REFGUID guid, UINT *pDataSize, void *pData);
	HRESULT (__stdcall *SetPrivateData)(ID3D11Texture* This, REFGUID guid, UINT DataSize, const void *pData);
	HRESULT (__stdcall *SetPrivateDataInterface)(ID3D11Texture* This, REFGUID guid, const IUnknown *pData);
	void (__stdcall *GetType)(ID3D11Texture* This, D3D11_RESOURCE_DIMENSION *pResourceDimension);
	void (__stdcall *SetEvictionPriority)(ID3D11Texture* This, UINT EvictionPriority);
	UINT (__stdcall *GetEvictionPriority)(ID3D11Texture* This);
	void (__stdcall *GetDesc)(ID3D11Texture* This, void* pDesc);
} ID3D11TextureVtbl;

struct _ID3D11Texture {
  const ID3D11TextureVtbl* lpVtbl;
};
]])

local ID3D11Texture = require('agen.interface.video.texture')
setmetatable(ID3D11Texture, D3D.ID3D11ResourceMT)

function ID3D11Texture.create2D(device, options)
  assert(ffi.istype('ID3D11Device', device), "Invalid argument #1, ID3D11Device expected")
  assert(options,        "Missing required parameter #2")
  assert(options.dxgi_format > D3D.FORMAT_UNKNOWN, "Format cannot be unknown")
  assert(options.usage,  "Missing required parameter usage")
  assert(options.flags,  "Missing required parameter flags")
  assert(options.width  > 0, "Invalid texture width")
  assert(options.height > 0, "Invalid texture height")
  -- can't specify NULL for pInitialData when creating IMMUTABLE resources (see D3D11_USAGE).
  if options.usage == D3D.USAGE_IMMUTABLE then
    assert(options.data ~= nil, "immutable texture must have initial data")
  end
	if bit.band(options.flags, bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL)) == 
	   bit.bor(D3D.BIND_SHADER_RESOURCE, D3D.BIND_DEPTH_STENCIL) then
		assert(options.dxgi_format == D3D.FORMAT_R32_TYPELESS   or
					 options.dxgi_format == D3D.FORMAT_R24G8_TYPELESS or
					 options.dxgi_format == D3D.FORMAT_R16_TYPELESS, 'depth/stencil shader resource texture must be typless format') 
	end

  local samples = options.samples
  if samples == nil or samples == 0 then
    samples = 1
  end

	local descTex = ffi.new('D3D11_TEXTURE2D_DESC')
	descTex.Width               = options.width
	descTex.Height              = options.height
	descTex.MipLevels           = options.levels or 1
	descTex.ArraySize           = 1
	descTex.Format              = options.dxgi_format
	descTex.SampleDesc.Count    = samples
	descTex.SampleDesc.Quality  = 0
	descTex.Usage               = options.usage
	descTex.BindFlags           = options.flags
	descTex.CPUAccessFlags      = 0
	descTex.MiscFlags           = 0

  agen.log.debug('video', "Will create texture [%dx%d@%d] fmt %d", descTex.Width, descTex.Height, descTex.MipLevels, descTex.Format)
  --
  -- multisampled resources cannot be initialized with data
  --
  local initialData = nil
  if options.usage == D3D.USAGE_IMMUTABLE then
    initialData                  = ffi.new('D3D11_SUBRESOURCE_DATA')
    initialData.pSysMem 	       = options.data
    initialData.SysMemPitch      = options.pitch or 0
    initialData.SysMemSlicePitch = 0
  end
  return device:createTexture2D(descTex, initialData)
end

function ID3D11Texture:getDesc()
  -- FIXME: assumes TEXTURE2D
	local desc = ffi.new('D3D11_TEXTURE2D_DESC')
	self.lpVtbl.GetDesc(self, desc)
  return desc
end
--- Get texture width and height
-- @param none
-- @return width, height
function ID3D11Texture:_getSize()
	local desc = self:getDesc()
  return desc.Width, desc.Height
end
--- Get texture type. Can be 1D, 2D, 2D with multisampling, 3D, 3D with multisampling,
--- array1D, array2D, array2D with multisampling, array3D, array3D with multisampling.
function ID3D11Texture:getType()
  local dim = ffi.new('D3D11_RESOURCE_DIMENSION[1]')
  self.lpVtbl.GetType(self, dim)
  return dim[0]
end

local shaderResourceViewFormats = {
	[tonumber(D3D.FORMAT_R32_TYPELESS)]   = D3D.FORMAT_R32_TYPELESS,
  [tonumber(D3D.FORMAT_R24G8_TYPELESS)] = D3D.FORMAT_R24_UNORM_X8_TYPLESS,
  [tonumber(D3D.FORMAT_R16_TYPELESS)]   = D3D.FORMAT_R16_TYPELESS,
}
--- Create shader resource object for accessing texture from shader
-- @return new lua managed ShaderResourceView object
function ID3D11Texture:getShaderResourceView()
	--
	-- Set resource description to NULL to create a view that accesses the entire 
  -- resource (using the format the resource was created with).
	--
	--local resDesc = nil
	local texDesc = self:getDesc()
	assert(self:getType() == D3D.RESOURCE_DIMENSION_TEXTURE2D, "Only 2D textures supported")

	local resDesc = ffi.new('D3D11_SHADER_RESOURCE_VIEW_DESC')
  -- depth/stencil formats need to be bound as typeless
  resDesc.Format = shaderResourceViewFormats[tonumber(texDesc.Format)] or texDesc.Format
	resDesc.ViewDimension = D3D.SRV_DIMENSION_TEXTURE2D
  resDesc.Texture2D.MostDetailedMip = 0
  resDesc.Texture2D.MipLevels = texDesc.MipLevels
	return self:getDevice():createShaderResourceView(self, resDesc)
end

function ID3D11Texture:map(ctx)
  if not ctx then ctx = self:getImmediateContext() end
  local ret = ffi.new('D3D11_MAPPED_SUBRESOURCE')
	error('not implemented')
--[[
HRESULT (__stdcall *Map)(ID3D11DeviceContext* This, void *pResource, UINT Subresource, D3D11_MAP MapType, D3D11_MAP_FLAG MapFlags, D3D11_MAPPED_SUBRESOURCE* pMappedResource);
]]
  return ret.pData, ret.RowPitch, ret.DepthPitch
end

function ID3D11Texture:unmap(ctx)
  if not ctx then ctx = self:getImmediateContext() end
	error('not implemented')
--[[
void (__stdcall *Unmap)(ID3D11DeviceContext* This, void *pResource, UINT Subresource);
]]
end

--- synchronously update texture region / mip level
-- dataLen is required only for compressed formats
function ID3D11Texture:_updateRect(level, destRect, data, dataLen, dataPitch)
  assert(data,                        "Invalid parameter #3, source data cannot be nil")
  assert(dataPitch and dataPitch > 0, "Invalid parameter #5, source data pitch cannot be unknown") 

  local ctx = self:getImmediateContext()
  local destBox = nil
--[[
  TODO: why this doesn't work?
  if destRect then
    --
    -- Coordinates are in bytes for buffers and in texels for textures. 
    --
    destBox = ffi.new('D3D11_BOX')
    -- The x position of the left hand side of the box
    destBox.left = destRect.x
    -- The y position of the top of the box
    destBox.top  = destRect.y
    -- The z position of the front of the box
    destBox.front = 0
    -- The x position of the right hand side of the box
    destBox.right  = destRect.w
    -- The y position of the bottom of the box
    destBox.bottom = destRect.h
    -- The z position of the back of the box
    -- by having front equal to back, the box is technically empty
    destBox.back   = 1
    agen.log.debug('video', "Updating texture level [%d], data size [%d] bytes, pitch [%d] bytes, size [%dx%d]", level, dataLen, dataPitch, destBox.right, destBox.bottom)
  end
]]
  ctx.lpVtbl.UpdateSubresource(ctx, self, level, destBox, data, dataPitch, 0)
  -- TODO: 
  -- ctx.lpVtbl.CopySubresourceRegion(ctx, self, 0, offset, 0, 0, srcBuff, 0, nil)
  return 0, nil
end

function ID3D11Texture:generateMipLevels(ctx)
  if not ctx then ctx = self:getImmediateContext() end
	error('not implemented')
  --[[
  void (__stdcall *GenerateMips)(ID3D11DeviceContext* This,ID3D11ShaderResourceView *pShaderResourceView);
  ]]
end

local ID3D11TextureMT = {__index = ID3D11Texture}
return ffi.metatype('ID3D11Texture', ID3D11TextureMT)
