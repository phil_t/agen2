local string = require('string')
local D3D    = require('agen.video.DX11.lib')
local ffi    = D3D.ffi
local class  = require('middleclass')
local IRenderPass = require('agen.interface.video.renderPass')

ffi.cdef([[
typedef struct _DX11Range {
  UINT location;
  UINT length;
} DX11Range;
]])

local _MAX_ATTRIB_SLOTS  = 8
local _MAX_SAMPLER_SLOTS = 8
local _MAX_TEXTURE_SLOTS = 8
local _MAX_UNIFORM_SLOTS = 8

local renderPass = class('renderPass')
renderPass:include(IRenderPass)

function renderPass:initialize(rState)
  self.owner = rState
  self.ctx = rState:getDevice():getImmediateContext()
  self.renderTarget = nil
  self.vertexBufferSlots = {
    numSlots = 0,
    buffers = ffi.gc(ffi.new('ID3D11Buffer*[?]', _MAX_ATTRIB_SLOTS), function(buffs)
      for i = 0, _MAX_ATTRIB_SLOTS - 1, 1 do
        if buffs[i] ~= nil then buffs[i]:release() end
      end
    end),
    offsets = ffi.new('UINT[?]', _MAX_ATTRIB_SLOTS),
    strides = ffi.new('UINT[?]', _MAX_ATTRIB_SLOTS),
  }
  self.textureSlots = {
    -- textureViews elements
    numSlots = 0,
    -- shader resource views
    textures = {},
    textureViews = ffi.new('ID3D11ShaderResourceView*[?]', _MAX_TEXTURE_SLOTS),
  }
  self.samplerSlots = {
    numSlots = 0,
    samplers = {},
    samplerStates = ffi.new('ID3D11SamplerState*[?]', _MAX_SAMPLER_SLOTS),
  }
  self.uniformBufferSlots = {
    numSlots = 0,
    vertexRange   = ffi.new('DX11Range', {0, 0}),
    geometryRange = ffi.new('DX11Range', {0, 0}),
    fragmentRange = ffi.new('DX11Range', {0, 0}),
    buffers = ffi.gc(ffi.new('ID3D11Buffer*[?]', _MAX_UNIFORM_SLOTS), function(buffs)
      for i = 0, _MAX_UNIFORM_SLOTS - 1, 1 do
        if buffs[i] ~= nil then buffs[i]:release() end
      end
    end), 
    offsets = ffi.new('UINT[?]', _MAX_UNIFORM_SLOTS),
    names = {},
  }
  self.indexBuffer = nil

  return self
end
--- set custom render target replacing the default
-- @param Render Target object
function renderPass:setRenderTarget(target)
  self.renderTarget = target
end

function renderPass:_init(opts)
  if opts.context ~= nil then
    self.ctx = opts.context
  end

  if opts.renderTarget ~= nil then
    self:setRenderTarget(opts.renderTarget)
  elseif self.renderTarget == nil then
    local defaultRenderTarget = self.owner:getDevice():getDefaultRenderTarget()
    self:setRenderTarget(defaultRenderTarget)
  end

  if opts.vertexBuffers then
    assert(#opts.vertexBuffers < _MAX_ATTRIB_SLOTS, "Number of vertex buffers > _MAX_ATTRIB_SLOTS")
    -- buffers' strides are in the renderState vertex declaration, offsets are always 0 for now
    local decl = self.owner:getVertexDeclaration()
    for i, buffer in pairs(opts.vertexBuffers) do
      self:setVertexBuffer(i, buffer, decl.strides[i], 0)
    end
  end

  if opts.indexBuffer then
    self:setIndexBuffer(opts.indexBuffer)
  end

  if opts.uniforms then
    assert(#opts.uniforms < _MAX_UNIFORM_SLOTS, "Number of uniform buffers > _MAX_UNIFORM_SLOTS")
    local uniformIndex = 0
    if opts.uniforms.vertex then
      for i, data in pairs(opts.uniforms.vertex) do
        self:setUniformBuffer('vertex', uniformIndex, data.name, data.data, data.offset or 0)
        uniformIndex = uniformIndex + i + 1
      end
    end

    if opts.uniforms.geometry then
      for i, data in pairs(opts.uniforms.geometry) do
        self:setUniformBuffer('geometry', uniformIndex, data.name, data.data, data.offset or 0)
        uniformIndex = uniformIndex + i + 1
      end
    end

    if opts.uniforms.fragment then
      for i, data in pairs(opts.uniforms.fragment) do
        self:setUniformBuffer('pixel', uniformIndex, data.name, data.data, data.offset or 0)
        uniformIndex = uniformIndex + i + 1
      end
    end
  end

  if opts.textures then
    for i, tex in pairs(opts.textures) do
      self.textureSlots.textures[i] = tex:getShaderResourceView()
    end
  end

  if opts.samplers then
    for i, samp in pairs(opts.samplers) do
      self.samplerSlots.numSlots = self.samplerSlots.numSlots + 1
      self.samplerSlots.samplers[i] = samp
      self.samplerSlots.samplerStates[i] = samp
    end
  end
end

---- fill currently referenced color, depth and stencil buffers
function renderPass:_clear()
  self.renderTarget:clear(self.ctx)
end

function renderPass:_clearDepth()
  self.renderTarget:clearDepthView(nil, self.ctx)
end

function renderPass:_clearDepthStencil()
  self.renderTarget:clearDepthView(nil, self.ctx)
end

function renderPass:setVertexBuffer(index, buffer, stride, offset)
  if self.vertexBufferSlots.numSlots < index + 1 then
    self.vertexBufferSlots.numSlots = index + 1
  end
	self.vertexBufferSlots.buffers[index] = buffer
  self.vertexBufferSlots.buffers[index]:ref()
  self.vertexBufferSlots.strides[index] = stride
  self.vertexBufferSlots.offsets[index] = offset
end
-- TODO: set in batches per type
function renderPass:setUniformBuffer(shader, index, name, buffer, offset)
  if shader == 'vertex' then
    self.uniformBufferSlots.vertexRange.location = 0
    self.uniformBufferSlots.vertexRange.length   = self.uniformBufferSlots.vertexRange.length + 1
  elseif shader == 'geometry' then
    self.uniformBufferSlots.geometryRange.location = self.uniformBufferSlots.vertexRange.location + 1
    self.uniformBufferSlots.geometryRange.length   = self.uniformBufferSlots.geometryRange.length + 1
  elseif shader == 'pixel' then
    self.uniformBufferSlots.fragmentRange.location = self.uniformBufferSlots.geometryRange.location + 1
    self.uniformBufferSlots.fragmentRange.length   = self.uniformBufferSlots.fragmentRange.length + 1
  else
    error("Unexpected stage [" .. shader .. "]")
  end

  agen.log.debug('video', "Using constant buffer [%s] at index [%d]", name, index)

  self.uniformBufferSlots.buffers[index] = buffer
  self.uniformBufferSlots.buffers[index]:ref()
  self.uniformBufferSlots.offsets[index] = offset
  self.uniformBufferSlots.names[name] = index
end

function renderPass:getVertexBuffer(index)
  assert(index < _MAX_ATTRIB_SLOTS, "Invalid vertex buffer index requested")
  assert(self.vertexBufferSlots.buffers[index], "non-existing vertex buffer index requested")
  return self.vertexBufferSlots.buffers[index]
end

function renderPass:getIndexBuffer()
  return self.indexBuffer.buffer
end

function renderPass:_getUniformBuffer(name)
  local index = self.uniformBufferSlots.names[name]
  assert(index, "No uniform buffer [" .. name .. "] found")
  assert(index < _MAX_UNIFORM_SLOTS, "Invalid uniform buffer index requested")
  assert(self.uniformBufferSlots.buffers[index], "non-existing uniform buffer index requested")
  return self.uniformBufferSlots.buffers[index]
end

function renderPass:setIndexBuffer(buffer)
  self.indexBuffer = {
    buffer = buffer,
    format = buffer:getIndexBufferFormat(),
    offset = 0,
  }
end
--- update the texture+sampler shader resource bindings for the draw calls that will follow
--  @param slotIndex sampler/texture "slot" to update. "Seen" as sampler/texture register index in the shader
--  @param samplerIndex index in the samplers array pointing to the sampler being assigned to slotIndex
--  @param textureIndex index in the textures array pointing to the texture being assigned to slotIndex
function renderPass:_setTexture(slotIndex, samplerIndex, textureIndex)
  assert(slotIndex < _MAX_TEXTURE_SLOTS, "Slot index > _MAX_TEXTURE_SLOTS")
  assert(slotIndex < _MAX_SAMPLER_SLOTS, "Slot index > _MAX_SAMPLER_SLOTS")
  local ctx = self.ctx

  local ts = self.textureSlots
  if ts.numSlots < slotIndex + 1 then
    ts.numSlots = slotIndex + 1
  end
  ts.textureViews[slotIndex]  = ts.textures[textureIndex]  
  ctx.lpVtbl.PSSetShaderResources(ctx, slotIndex, 1, ts.textureViews)  

  local ss = self.samplerSlots
  if ss.numSlots < slotIndex + 1 then
    ss.numSlots = slotIndex + 1
  end
  ss.samplerStates[slotIndex] = ss.samplers[samplerIndex]
  ctx.lpVtbl.PSSetSamplers(ctx, slotIndex, 1, ss.samplerStates)
end

function renderPass:setViewport(l, t, w, h, n, f)
  assert(w > 0, "viewport width is positive integer")
  assert(h > 0, "viewport height is positive integer")
  --assert(l + w <= D3D.VIEWPORT_BOUNDS_MAX, "size in bounds")
  --assert(t + h <= D3D.VIEWPORT_BOUNDS_MAX, "size in bounds")
  assert(n >= 0 and n <= 1, "viewport near is between 0 and 1")
  assert(f >= 0 and f <= 1, "viewport far is between 0 and 1")
  local viewport = ffi.new('D3D11_VIEWPORT')
  viewport.TopLeftX = l
  viewport.TopLeftY = t
  viewport.Width    = w
  viewport.Height   = h
  viewport.MinDepth = n
  viewport.MaxDepth = f
  self.ctx:setViewports(1, viewport)
end
--- The scissor rectangles will only be used if ScissorEnable is set to true in the rasterizer state. Which
-- scissor rectangle to use is determined by the SV_ViewportArrayIndex semantic output by a geometry shader 
-- (see shader semantic syntax). If a geometry shader does not make use of the SV_ViewportArrayIndex semantic 
-- then Direct3D will use the first scissor rectangle in the array.
function renderPass:setScissor(x, y, w, h)
  local clip = ffi.new('D3D11_RECT')
  clip.left   = x
  clip.right  = x + w
  clip.top    = y
  clip.bottom = y + h
  self.ctx:setScissors(1, clip)
end
------------------------------------------------------------------------------
--
-- Drawing
--
------------------------------------------------------------------------------
function renderPass:_begin()
  local ctx = self.ctx
  if self.renderTarget then
    self.renderTarget:setActive(ctx)
  end
  -- set vertex buffers
  local vbs = self.vertexBufferSlots
  ctx.lpVtbl.IASetVertexBuffers(ctx, 0, vbs.numSlots, vbs.buffers, vbs.strides, vbs.offsets)
  -- set index buffer
  if self.indexBuffer ~= nil then
    local ib = self.indexBuffer
    ctx.lpVtbl.IASetIndexBuffer(ctx, ib.buffer, ib.format, ib.offset)
  end
  -- set vertex shader uniforms
  local ub = self.uniformBufferSlots
  ctx.lpVtbl.VSSetConstantBuffers(ctx, 0, ub.vertexRange.length, ub.buffers + ub.vertexRange.location)
  -- set geo shader unifroms
  ctx.lpVtbl.GSSetConstantBuffers(ctx, 0, ub.geometryRange.length, ub.buffers + ub.geometryRange.location)
  -- set fragment shader uniforms
  ctx.lpVtbl.PSSetConstantBuffers(ctx, 0, ub.fragmentRange.length, ub.buffers + ub.fragmentRange.location)
  -- set samplers
  local ss = self.samplerSlots
  ctx.lpVtbl.PSSetSamplers(ctx, 0, ss.numSlots, ss.samplerStates)
  -- set textures
  local ts = self.textureSlots
  ctx.lpVtbl.PSSetShaderResources(ctx, 0, ts.numSlots, ts.textureViews)
end

--- Issue drawing command from buffer at index.
-- @param offset offset in vertices from the begining of buffer
-- @param num number of vertices
function renderPass:_drawPrimitive(offset, num)
  self.ctx.lpVtbl.Draw(self.ctx, num, offset)
end
--- Issue drawing command from indexed buffer at index.
-- @param offset from the beginning of the vertex buffer
-- @param num number of indices to render
-- @param ioffset offset from the beginning of the index buffer
function renderPass:_drawIndexedPrimitive(offset, num, ioffset)
  self.ctx.lpVtbl.DrawIndexed(self.ctx, num, ioffset, offset)
end
--- Issue instanced drawing command from indexed buffer at index.
-- @param offset from the beginning of vertex buffer
-- @param num number of indices
-- @param ioffset offset in index buffer
-- @param numInsts number of instances to draw
function renderPass:_drawInstancedPrimitive(offset, num, ioffset, numInsts)
  self.ctx.lpVtbl.DrawIndexedInstanced(self.ctx, num, numInsts, ioffset, offset, 0)
end
--- Issue a drawing command with parameters in buffer at index <bufferIndex>
-- @param bufferIndex Parameters buffer index
-- @param offset from the beginning of parameters buffer
function renderPass:_drawPrimitiveIndirect(bufferIndex, offset)
  self.ctx.lpVtbl.DrawInstancedIndirect(self.ctx, self.vertexBufferSlots.buffers[bufferIndex], offset)
end
--- Issue an indexed drawing command with parameters in buffer at index <bufferIndex>
-- @param bufferIndex Parameters buffer index
-- @param offset from the beginning of parameters buffer
function renderPass:_drawIndexedPrimitiveIndirect(bufferIndex, offset)
  self.ctx.lpVtbl.DrawIndexedInstancedIndirect(self.ctx, self.vertexBufferSlots.buffers[bufferIndex], offset)
end
--- Submit commands to GPU
function renderPass:submit()
end

return renderPass
