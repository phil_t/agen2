local string = require('string')
local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

ffi.cdef([[
typedef enum _DXGI_SWAP_EFFECT {
	SWAP_EFFECT_DISCARD          = 0x00000000,
	SWAP_EFFECT_SEQUENTIAL       = 0x00000001,
	SWAP_EFFECT_FLIP_SEQUENTIAL  = 0x00000003,
  SWAP_EFFECT_FLIP_DISCARD     = 0x00000004
} DXGI_SWAP_EFFECT;

typedef enum _DXGI_MODE_SCANLINE_ORDER { 
	MODE_SCANLINE_ORDER_UNSPECIFIED        = 0,
	MODE_SCANLINE_ORDER_PROGRESSIVE        = 1,
	MODE_SCANLINE_ORDER_UPPER_FIELD_FIRST  = 2,
	MODE_SCANLINE_ORDER_LOWER_FIELD_FIRST  = 3
} DXGI_MODE_SCANLINE_ORDER;

typedef enum _DXGI_MODE_SCALING { 
	MODE_SCALING_UNSPECIFIED  = 0,
	MODE_SCALING_CENTERED     = 1,
	MODE_SCALING_STRETCHED    = 2
} DXGI_MODE_SCALING;

typedef enum _DXGI_SWAP_CHAIN_FLAG { 
  SWAP_CHAIN_FLAG_NONPREROTATED                    = 1,
  SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH                = 2,
  SWAP_CHAIN_FLAG_GDI_COMPATIBLE                   = 4,
  SWAP_CHAIN_FLAG_RESTRICTED_CONTENT               = 8,
  SWAP_CHAIN_FLAG_RESTRICT_SHARED_RESOURCE_DRIVER  = 16,
  SWAP_CHAIN_FLAG_DISPLAY_ONLY                     = 32,
  SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT    = 64,
  SWAP_CHAIN_FLAG_FOREGROUND_LAYER                 = 128
} DXGI_SWAP_CHAIN_FLAG;

typedef struct _DXGI_MODE_DESC {
	UINT                     Width;
	UINT                     Height;
	DXGI_RATIONAL            RefreshRate;
	DXGI_FORMAT              Format;
	DXGI_MODE_SCANLINE_ORDER ScanlineOrdering;
	DXGI_MODE_SCALING        Scaling;
} DXGI_MODE_DESC;

typedef struct DXGI_SWAP_CHAIN_DESC {
	DXGI_MODE_DESC   BufferDesc;
	DXGI_SAMPLE_DESC SampleDesc;
	DXGI_USAGE       BufferUsage;
	UINT             BufferCount;
	HWND             OutputWindow;
	BOOL             Windowed;
	DXGI_SWAP_EFFECT SwapEffect;
	UINT             Flags;
} DXGI_SWAP_CHAIN_DESC;

typedef struct _DXGI_FRAME_STATISTICS DXGI_FRAME_STATISTICS;
/*********************************************************************
 *
 * IDXGIObject
 *
 *********************************************************************/
typedef struct _IDXGIObject IDXGIObject;
typedef struct _IDXGIObjectVtbl {
  HRESULT (__stdcall *QueryInterface)(IDXGIObject* This, REFIID riid,void **ppvObject);
  ULONG (__stdcall *AddRef)(IDXGIObject* This);
  ULONG (__stdcall *Release)(IDXGIObject* This);
  HRESULT (__stdcall *SetPrivateData)(IDXGIObject* This, REFGUID Name, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(IDXGIObject* This, REFGUID Name, const IUnknown *pUnknown);
  HRESULT (__stdcall *GetPrivateData)(IDXGIObject* This, REFGUID Name, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *GetParent)(IDXGIObject* This, REFIID riid, void **ppParent);
} IDXGIObjectVtbl;

struct _IDXGIObject {
  const IDXGIObjectVtbl* lpVtbl;
};
/**********************************************************************
 *
 * IDXGIOutput (monitor)
 *
 **********************************************************************/
typedef struct _IDXGIOutput IDXGIOutput;
/***********************************************************************
 *
 * IDXGISwapChain
 *
 ***********************************************************************/
typedef struct _IDXGISwapChain IDXGISwapChain;
typedef struct _IDXGISwapChainVtbl {
  HRESULT (__stdcall *QueryInterface)(IDXGISwapChain* This, REFIID riid,void **ppvObject);
  ULONG (__stdcall *AddRef)(IDXGISwapChain* This);
  ULONG (__stdcall *Release)(IDXGISwapChain* This);
  HRESULT (__stdcall *SetPrivateData)(IDXGISwapChain * This, REFGUID Name, UINT DataSize, const void *pData);
  HRESULT (__stdcall *SetPrivateDataInterface)(IDXGISwapChain * This, REFGUID Name, const IUnknown *pUnknown);
  HRESULT (__stdcall *GetPrivateData)(IDXGISwapChain * This, REFGUID Name, UINT *pDataSize, void *pData);
  HRESULT (__stdcall *GetParent)(IDXGISwapChain * This, REFIID riid, void **ppParent);
  HRESULT (__stdcall *GetDevice)(IDXGISwapChain* This, REFIID riid, void** ppDevice);
  HRESULT (__stdcall *Present)(IDXGISwapChain* This, UINT SyncInterval, DXGI_PRESENT Flags);
  HRESULT (__stdcall *GetBuffer)(IDXGISwapChain* This, UINT Buffer, REFIID riid, void **ppSurface);
  HRESULT (__stdcall *SetFullscreenState)(IDXGISwapChain* This, BOOL Fullscreen, IDXGIOutput *pTarget);
  HRESULT (__stdcall *GetFullscreenState)(IDXGISwapChain* This, BOOL *pFullscreen, IDXGIOutput **ppTarget);
  HRESULT (__stdcall *GetDesc)(IDXGISwapChain* This, DXGI_SWAP_CHAIN_DESC *pDesc);
  HRESULT (__stdcall *ResizeBuffers)(IDXGISwapChain* This, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);
  HRESULT (__stdcall *ResizeTarget)(IDXGISwapChain* This, const DXGI_MODE_DESC *pNewTargetParameters);
  HRESULT (__stdcall *GetContainingOutput)(IDXGISwapChain* This, IDXGIOutput **ppOutput);
  HRESULT (__stdcall *GetFrameStatistics)(IDXGISwapChain* This, DXGI_FRAME_STATISTICS *pStats);
  HRESULT (__stdcall *GetLastPresentCount)(IDXGISwapChain* This, UINT *pLastPresentCount);  
} IDXGISwapChainVtbl;

struct _IDXGISwapChain {
  const IDXGISwapChainVtbl* lpVtbl;
};
// from dxgi.dll:
//HRESULT CreateDXGIFactory(REFIID riid, void** ppFactory);
//HRESULT CreateDXGIFactory1(REFIID riid, void** ppFactory);
// 8.1+
//HRESULT CreateDXGIFactory2(UINT Flags, REFIID riid, void** ppFactory);
]])

local IDXGIObject = {}
setmetatable(IDXGIObject, D3D.IUnknownMT)

function IDXGIObject:getParent(guid, ctype)
  local parent = ffi.new('void*[1]')
  local hres = self.lpVtbl.GetParent(self, guid, parent)
  if hres ~= 0 then
    error(string.format("GetParent failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(ffi.cast(ctype, parent[0]), D3D.Finalize)
end

local IDXGIObjectMT = {__index = IDXGIObject}

local IDXGIDeviceSubObject = {}
setmetatable(IDXGIDeviceSubObject, IDXGIObjectMT)

function IDXGIDeviceSubObject:getDevice(guid, ctype)
  local dev = ffi.new('void*[1]')
  local hres = self.lpVtbl.GetDevice(self, guid, dev)
  if hres ~= 0 then
    error(string.format("GetDevice failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(ffi.cast(ctype, dev[0]), D3D.Finalize)
end

local IDXGIDeviceSubObjectMT = {__index = IDXGIDeviceSubObject}
---------------------------------------------------------------------
--
-- constants
--
---------------------------------------------------------------------
local IID_ID3D11Texture2D = ffi.new('GUID', {
  0x6f15aaf2,
	0xd208, 0x4e89,
	{0x9a,0xb4,0x48,0x95,0x35,0xd3,0x4f,0x9c}
})

local bufferObjects = {
  texture2D = {
    ctype = 'ID3D11Texture*',
    GUID  = IID_ID3D11Texture2D,
  },
}
---------------------------------------------------------------------
--
-- IDXGISwapChain
--
---------------------------------------------------------------------
local IDXGISwapChain = {}
setmetatable(IDXGISwapChain, IDXGIDeviceSubObjectMT)

function IDXGISwapChain:resizeBuffers(numBuffers, width, height, format)
  agen.log.debug('application', 'resize backbuffers [%d] [%dx%d@%p]', numBuffers, width, height, format)
  local hres = self.lpVtbl.ResizeBuffers(self, numBuffers, width, height, format, 0)
  if hres ~= 0 then
    error(string.format("Failed to resize backbuffer(s): %s", D3D.GetError(hres)))
  end
end

function IDXGISwapChain:present(sync, flags)
  local hres = self.lpVtbl.Present(self, sync, flags)
  if hres ~= 0 then
    error(string.format("Present failed: %s", D3D.GetError(hres)))
  end
end
--- Accesses one of the swap-chain's back buffers.
-- @param buffer index
-- @param object REFIID
-- @return managed object
function IDXGISwapChain:getBuffer(bufferIndex, objectType)
  assert(bufferIndex, "Buffer index cannot be nil")
  assert(objectType, "Object type cannot be nil")
  assert(bufferObjects[objectType], "Unexpected buffer type")

  local obj = ffi.new('void*[1]')
  local hres = self.lpVtbl.GetBuffer(self, bufferIndex, bufferObjects[objectType].GUID, obj)
  if hres ~= 0 or obj[0] == nil then
    error(string.format("GetBuffer failed: %s", D3D.GetError(hres)))
  end
  return ffi.gc(
    ffi.cast(bufferObjects[objectType].ctype, obj[0]),
    D3D.Finalize)
end

function IDXGISwapChain:getDesc()
  local desc = ffi.new('DXGI_SWAP_CHAIN_DESC')
  local hres = self.lpVtbl.GetDesc(self, desc)
  if hres ~= 0 then
    error(string.format("GetDesc failed: %s", D3D.GetError(hres)))
  end
  return desc
end

local IDXGISwapChainMT = {__index = IDXGISwapChain}
return ffi.metatype('IDXGISwapChain', IDXGISwapChainMT)
