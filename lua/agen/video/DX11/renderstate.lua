local D3D = require('agen.video.DX11.lib')
local ffi = D3D.ffi

local DX11Shader = require('agen.video.DX11.shader')
local vertexDecl = require('agen.video.DX11.declaration')
local RenderPass = require('agen.video.DX11.renderpass')
local mat4       = require('agen.util.math.mat4')
local class      = require('middleclass')
local IRenderState  = require('agen.interface.video.renderState')

local RenderState   = class('RenderState')
RenderState:include(IRenderState)

local ptypeTopology = {
  triangles = D3D.PRIMITIVE_TOPOLOGY_TRIANGLELIST,
  strip     = D3D.PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
  lines     = D3D.PRIMITIVE_TOPOLOGY_LINELIST,
  linestrip = D3D.PRIMITIVE_TOPOLOGY_LINESTRIP,
  points    = D3D.PRIMITIVE_TOPOLOGY_POINTLIST,
}

local mapCullMode = {
  off   = D3D.CULL_NONE,
  back  = D3D.CULL_BACK,
  front = D3D.CULL_FRONT,
}

function RenderState:initialize(device, opts)
  self.owner = device
  self.vdecl = nil
  self.program = DX11Shader.create(device, opts.shaders)
  self.xform   = {
    projection = mat4.identity(),
    model      = mat4.identity(),
    view       = mat4.identity(),
    mvp        = mat4.create(),
  }
  self.topology = ptypeTopology[opts.topology]
  self.state = {
    blender = nil,
    raster  = nil,
    depth   = nil,
  }
  self.vdecl = vertexDecl.create(device, opts.vertexBuffersAttr, self.program)
  -- FIXME: refac
  self.program:cleanup()
  --
  -- create default rasterizer state
  -- TODO: pass from shared code, should be consistent across backends
  --
  local rasterDesc = ffi.new('D3D11_RASTERIZER_DESC', {
    FillMode  = D3D.FILL_SOLID,
    CullMode  = D3D.CULL_BACK,
    FrontCounterClockwise = 1,
    DepthBias = 0,
    DepthBiasClamp = 0,
    SlopeScaledDepthBias = 0,
    DepthClipEnable = 0,
    ScissorEnable = 0,
    MultisampleEnable = 0,
    AntialiasedLineEnable = 0,
  })
  self.state.raster = device:createRasterizerState(rasterDesc)
  --
  -- create default depth/stencil state
  -- TODO: pass from shared code, should be consistent across backends
  --
  local dsDesc = ffi.new('D3D11_DEPTH_STENCIL_DESC', {
    DepthEnable    = 1,
    DepthWriteMask = D3D.DEPTH_WRITE_MASK_ALL,
    DepthFunc      = D3D.COMPARISON_LESS,
    -- Stencil test parameters
    StencilEnable    = 0,
    StencilReadMask  = 0xFF,
    StencilWriteMask = 0xFF,
    --
    -- Stencil operations if pixel is front-facing
    --
    FrontFace = ffi.new('D3D11_DEPTH_STENCILOP_DESC', {
      StencilFailOp      = D3D.STENCIL_OP_KEEP,
      StencilDepthFailOp = D3D.STENCIL_OP_INCR,
      StencilPassOp      = D3D.STENCIL_OP_KEEP,
      StencilFunc        = D3D.COMPARISON_ALWAYS,
    }),
    --
    -- Stencil operations if pixel is back-facing
    --
    BackFace = ffi.new('D3D11_DEPTH_STENCILOP_DESC', {
      StencilFailOp      = D3D.STENCIL_OP_KEEP,
      StencilDepthFailOp = D3D.STENCIL_OP_DECR,
      StencilPassOp      = D3D.STENCIL_OP_KEEP,
      StencilFunc        = D3D.COMPARISON_ALWAYS,
    })
  })
  self.state.depth = device:createDepthStencilState(dsDesc)
  --
  -- create default blending state
  -- TODO: pass from shared code, should be consistent across backends
  --
  local blendDesc = ffi.new('D3D11_BLEND_DESC', {
    AlphaToCoverageEnable = 0,
    IndependentBlendEnable = 0,
    --
    -- An array of D3D11_RENDER_TARGET_BLEND_DESC structures that describe 
    -- the blend states for render targets; these correspond to the eight 
    -- render targets that can be bound to the output-merger stage at one time.
    --
    RenderTarget = ffi.new('D3D11_RENDER_TARGET_BLEND_DESC[8]', {[0] = {
      BlendEnable = 0,
      SrcBlend  = D3D.BLEND_ONE,
      DestBlend = D3D.BLEND_ZERO,
      BlendOp   = D3D.BLEND_OP_ADD,
      SrcBlendAlpha = D3D.BLEND_ONE,
      DestBlendAlpha = D3D.BLEND_ZERO,
      BlendOpAlpha = D3D.BLEND_OP_ADD,
      RenderTargetWriteMask = D3D.COLOR_WRITE_ENABLE_ALL
    }})
  })
  self.state.blender = device:createBlendState(blendDesc)
  --
  -- in IRenderState
  --
  self:init()

  return self
end

function RenderState:setActive()
  local ctx = self:getDevice():getImmediateContext()
  -- set shaders
  self.program:setActive(ctx)
  self.vdecl:setActive(ctx)
  -- set render topology
  ctx.lpVtbl.IASetPrimitiveTopology(ctx, self.topology)
  -- set fixed state
  local state = self.state
  ctx:setRasterizerState(state.raster)
  ctx:setDepthStencilState(state.depth, 1)
  ctx:setBlenderState(state.blender, nil, 0xffffffff)
end

function RenderState:_createRenderPass()
  return RenderPass:new(self)
end

function RenderState:_setDepthFunc(name)
  local device = self:getDevice()
  local depthDesc  = self.state.depth:getDesc()

  if name == 'off' then
    depthDesc.DepthEnable = 0
  elseif name == 'less' then
    depthDesc.DepthEnable = 1
    depthDesc.DepthFunc   = D3D.COMPARISON_LESS
  elseif name == 'greater' then
    depthDesc.DepthEnable = 1
    depthDesc.DepthFunc   = D3D.COMPARISON_GREATER
  elseif name == 'less_equal' then
    depthDesc.DepthEnable = 1
    depthDesc.DepthFunc   = D3D.COMPARISON_LESS_EQUAL
  elseif name == 'greater_equal' then
    depthDesc.DepthEnable = 1
    depthDesc.DepthFunc   = D3D.COMPARISON_GREATER_EQUAL
  else
    error("depth function [" .. name .. "] not implemented")
  end
  self.state.depth = device:createDepthStencilState(depthDesc)
end

function RenderState:_setBlendFunc(name)
  local device = self:getDevice()
  local blendDesc = self.state.blender:getDesc()

  if name == 'off' then
    blendDesc.RenderTarget[0].BlendEnable = 0
  elseif name == 'alpha' then
    blendDesc.RenderTarget[0].BlendEnable = 1
    blendDesc.RenderTarget[0].SrcBlend  = D3D.BLEND_SRC_ALPHA
    blendDesc.RenderTarget[0].DestBlend = D3D.BLEND_INV_SRC_ALPHA
    blendDesc.RenderTarget[0].BlendOp   = D3D.BLEND_OP_ADD
  elseif name == 'additive' then
    blendDesc.RenderTarget[0].BlendEnable = 1
    blendDesc.RenderTarget[0].SrcBlend  = D3D.BLEND_SRC_ALPHA
    blendDesc.RenderTarget[0].DestBlend = D3D.BLEND_ONE
    blendDesc.RenderTarget[0].BlendOp   = D3D.BLEND_OP_ADD
  else
    error("FIXME: blend function [" .. name .. "] not implemented")
  end
  self.state.blender = device:createBlendState(blendDesc)
end
------------------------------------------------------------------------
--
-- update rasterizer state
--
------------------------------------------------------------------------
function RenderState:_setCullFunc(name)
  assert(mapCullMode[name], "Invalid cull function")
  local rasterDesc = self.state.raster:getDesc()
  rasterDesc.CullMode = mapCullMode[name]
  self.state.raster = self:getDevice():createRasterizerState(rasterDesc)
end

function RenderState:_setWireFrame(flag)
  local rasterDesc = self.state.raster:getDesc()
  rasterDesc.FillMode = flag and D3D.FILL_WIREFRAME or D3D.FILL_SOLID
  self.state.raster = self:getDevice():createRasterizerState(rasterDesc)
end

function RenderState:_setScissorTest(flag)
  local rasterDesc = self.state.raster:getDesc()
  rasterDesc.ScissorEnable = flag and 1 or 0
  self.state.raster = self:getDevice():createRasterizerState(rasterDesc)
end

return RenderState
