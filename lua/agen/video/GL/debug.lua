local string = require('string')
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t DEBUG_OUTPUT_SYNCHRONOUS    = 0x8242;
static const uint16_t DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
static const uint16_t DEBUG_CALLBACK_FUNCTION     = 0x8244;
static const uint16_t DEBUG_CALLBACK_USER_PARAM   = 0x8245;
static const uint16_t DEBUG_SOURCE_API            = 0x8246;
static const uint16_t DEBUG_SOURCE_WINDOW_SYSTEM  = 0x8247;
static const uint16_t DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
static const uint16_t DEBUG_SOURCE_THIRD_PARTY  = 0x8249;
static const uint16_t DEBUG_SOURCE_APPLICATION  = 0x824A;
static const uint16_t DEBUG_SOURCE_OTHER        = 0x824B;
static const uint16_t DEBUG_TYPE_ERROR                = 0x824C;
static const uint16_t DEBUG_TYPE_DEPRECATED_BEHAVIOR  = 0x824D;
static const uint16_t DEBUG_TYPE_UNDEFINED_BEHAVIOR   = 0x824E;
static const uint16_t DEBUG_TYPE_PORTABILITY          = 0x824F;
static const uint16_t DEBUG_TYPE_PERFORMANCE          = 0x8250;
static const uint16_t DEBUG_TYPE_OTHER                = 0x8251;
static const uint16_t MAX_DEBUG_MESSAGE_LENGTH  = 0x9143;
static const uint16_t MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
static const uint16_t DEBUG_LOGGED_MESSAGES     = 0x9145;
static const uint16_t DEBUG_SEVERITY_HIGH       = 0x9146;
static const uint16_t DEBUG_SEVERITY_MEDIUM     = 0x9147;
static const uint16_t DEBUG_SEVERITY_LOW        = 0x9148;

typedef void (*PFNGLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam);
typedef void (*PFNGLDEBUGMESSAGECONTROLPROC)(GLenum source, GLenum type, GLenum severity,
                                             GLsizei count, const GLuint* ids, GLboolean enabled);
typedef void (*PFNGLDEBUGMESSAGEINSERTPROC)(GLenum source, GLenum type, GLuint id,
                                            GLenum severity, GLsizei length, const GLchar* buf);
typedef void (*PFNGLDEBUGMESSAGECALLBACKPROC)(PFNGLDEBUGPROC callback, const GLvoid* userParam);
typedef GLuint (*PFNGLGETDEBUGMESSAGELOGPROC)(GLuint count, GLsizei bufSize, GLenum* sources,
                                              GLenum* types, GLuint* ids, GLenum* severities,
                                              GLsizei* lengths, GLchar* messageLog);
typedef void (*PFNGLGETPOINTERVPROC)(GLenum pname, GLvoid** params);
]])

local mapSev = {
  [tonumber(gl.DEBUG_SEVERITY_HIGH)]   = 'error',
  [tonumber(gl.DEBUG_SEVERITY_MEDIUM)] = 'info',
  [tonumber(gl.DEBUG_SEVERITY_LOW)]    = 'debug',
  [tonumber(gl.DEBUG_SEVERITY_NOTIFICATION)] = 'verbose',
  [gl.DEBUG_SEVERITY_NOTIFICATION] = 'verbose',
}

local mapType = {
  [tonumber(gl.DEBUG_TYPE_ERROR)] = "GL error",
  [tonumber(gl.DEBUG_TYPE_DEPRECATED_BEHAVIOR)] = "GL deprecated",
  [tonumber(gl.DEBUG_TYPE_UNDEFINED_BEHAVIOR)]  = "GL undefined",
  [tonumber(gl.DEBUG_TYPE_PORTABILITY)] = "GL non-portable",
  [tonumber(gl.DEBUG_TYPE_PERFORMANCE)] = "GL performance",
  [tonumber(gl.DEBUG_TYPE_OTHER)] = "GL info",
}

local function debug_log(source, t, id, severity, length, message, userParam)
  local errstr = message and ffi.string(message, length) or ""
  agen.log.cat(mapSev[tonumber(severity)] or 'verbose', 'video', "[%d] %s: %s", id, mapType[tonumber(t)] or 'unknown', errstr)
end

local dbg = {
  symtab = {
    DebugMessageCallback = 'PFNGLDEBUGMESSAGECALLBACKPROC',
    GetPointerv = 'PFNGLGETPOINTERVPROC',
  },
  callback = nil,
}

function dbg.setup(ctx)
  if not ctx:isSupported("GL_ARB_debug_output") then
    agen.log.info('video', "context debug option set, but ARB_debug_output not available, ignoring...")
    return
  end

  agen.log.info('video', "using ARB_debug_output for synchronous logging")

  for n, k in pairs(dbg.symtab) do
    gl[n] = k
  end

  dbg.callback = ffi.cast('PFNGLDEBUGPROC', debug_log)

  gl.Enable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
  gl.DebugMessageCallback(dbg.callback, nil)
end

function dbg.quit()
  if dbg.callback then
    gl.Disable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
    gl.DebugMessageCallback(nil, nil)
    dbg.callback:free()
    dbg.callback = nil
  end
end

return dbg
