local string = require('string')
local bit = require('bit')
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef enum _GLPrimitives {
  POINTS         = 0x0000,
  LINES          = 0x0001,
  LINE_LOOP      = 0x0002,
  LINE_STRIP     = 0x0003,
  TRIANGLES      = 0x0004,
  TRIANGLE_STRIP = 0x0005,
  TRIANGLE_FAN   = 0x0006,
  QUADS          = 0x0007,
  QUAD_STRIP     = 0x0008,
  POLYGON        = 0x0009,
} GLPrimitives;

static const uint16_t BUFFER_SIZE          = 0x8764;
static const uint16_t BUFFER_USAGE         = 0x8765;
static const uint16_t READ_ONLY            = 0x88B8;
static const uint16_t WRITE_ONLY           = 0x88B9;
static const uint16_t READ_WRITE           = 0x88BA;
static const uint16_t STREAM_DRAW          = 0x88E0;
static const uint16_t STREAM_READ          = 0x88E1;
static const uint16_t STREAM_COPY          = 0x88E2;
static const uint16_t STATIC_DRAW          = 0x88E4;
static const uint16_t STATIC_READ          = 0x88E5;
static const uint16_t STATIC_COPY          = 0x88E6;
static const uint16_t DYNAMIC_DRAW         = 0x88E8;
static const uint16_t DYNAMIC_READ         = 0x88E9;
static const uint16_t DYNAMIC_COPY         = 0x88EA;

static const uint16_t PARAMETER_BUFFER     = 0x80EE;
static const uint16_t ARRAY_BUFFER         = 0x8892;
static const uint16_t ELEMENT_ARRAY_BUFFER = 0x8893;
static const uint16_t PIXEL_PACK_BUFFER    = 0x88EB;
static const uint16_t PIXEL_UNPACK_BUFFER  = 0x88EC;
static const uint16_t UNIFORM_BUFFER       = 0x8A11;
static const uint16_t TEXTURE_BUFFER            = 0x8C2A;
static const uint16_t TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
static const uint16_t COPY_READ_BUFFER     = 0x8F36;
static const uint16_t COPY_WRITE_BUFFER    = 0x8F37;

static const uint16_t MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
static const uint16_t MAX_ELEMENT_INDEX      = 0x8D6B;

typedef void (*PFNGLBINDBUFFERPROC)(GLenum target, GLuint buffer);
typedef void (*PFNGLDELETEBUFFERSPROC)(GLsizei n, const GLuint* buffers);
typedef void (*PFNGLGENBUFFERSPROC)(GLsizei n, GLuint* buffers);
typedef GLboolean (*PFNGLISBUFFERPROC)(GLuint buffer);
typedef void (*PFNGLBUFFERSUBDATAPROC)(GLenum target, GLintptr offset, GLsizeiptr size, const void *data);
typedef void (*PFNGLGETBUFFERSUBDATAPROC)(GLenum target, GLintptr offset, GLsizeiptr size, void *data);
// MapBufferRange has more detailed mapping options
// typedef uint8_t* (*PFNGLMAPBUFFERPROC)(GLenum target, GLuint access);
typedef void (*PFNGLBUFFERDATAPROC)(GLenum target, GLsizeiptr size, const void* data, GLenum usage);
typedef void (*PFNGLGETBUFFERPARAMETERIVPROC)(GLenum target, GLenum pname, GLint* params);
typedef void (*PFNGLGETBUFFERPOINTERVPROC)(GLenum target, GLenum pname, void** params);
typedef GLboolean (*PFNGLUNMAPBUFFERPROC)(GLenum target);
//
// GL 3.0
//
typedef void (*PFNGLBINDBUFFERRANGEPROC)(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size);
typedef void (*PFNGLBINDBUFFERBASEPROC)(GLenum target, GLuint index, GLuint buffer);
//
// GL 3.1
//
typedef void (*PFNGLCOPYBUFFERSUBDATAPROC)(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);
//
// Core 3.2 or GL_ARB_map_buffer_range
//
typedef enum _GLMapFlags {
  MAP_READ_BIT              = 0x00000001,
  MAP_WRITE_BIT             = 0x00000002,
  MAP_INVALIDATE_RANGE_BIT  = 0x00000004,
  MAP_INVALIDATE_BUFFER_BIT = 0x00000008, // GL is free to allocate a new buffer to avoid synchronization
  MAP_FLUSH_EXPLICIT_BIT    = 0x00000010, // explicit synchronization when MAP_UNSYNCHRONIZED_BIT is set
  MAP_UNSYNCHRONIZED_BIT    = 0x00000020  // implicit synchronization in GL disabled - up to client
} GLMapFlags;

typedef uint8_t* (*PFNGLMAPBUFFERRANGEPROC)(GLenum target, GLintptr offset, GLsizeiptr length, GLuint access);
typedef void (*PFNGLFLUSHMAPPEDBUFFERRANGEPROC)(GLenum target, GLintptr offset, GLsizeiptr length);

typedef struct _glBuffer_t {
  GLuint id[1];
  GLenum target;
  GLenum ctype;
	GLsizei size;
} glBuffer_t;
]])

local mapBufferPurpose = {
  vertex    = gl.ARRAY_BUFFER,
	data      = gl.ARRAY_BUFFER,
  index     = gl.ELEMENT_ARRAY_BUFFER,
  upload    = gl.PIXEL_PACK_BUFFER,
  download  = gl.PIXEL_UNPACK_BUFFER,
  uniform   = gl.UNIFORM_BUFFER,
}
---------------------------------------------------------
--
-- Vertex Buffer Object
--
---------------------------------------------------------
local symtab = {
  -- core since 1.5 or GL_ARB_vertex_buffer_object
  BindBuffer       = 'PFNGLBINDBUFFERPROC',
  DeleteBuffers    = 'PFNGLDELETEBUFFERSPROC',
  GenBuffers       = 'PFNGLGENBUFFERSPROC',
  IsBuffer         = 'PFNGLISBUFFERPROC',
  BufferData       = 'PFNGLBUFFERDATAPROC',
  -- not used
  -- BufferSubData    = 'PFNGLBUFFERSUBDATAPROC',
  -- GetBufferSubData = 'PFNGLGETBUFFERSUBDATAPROC',
  -- GetBufferParameteriv = 'PFNGLGETBUFFERPARAMETERIVPROC',
  -- GetBufferPointerv    = 'PFNGLGETBUFFERPOINTERVPROC',
  -- MapBuffer        = 'PFNGLMAPBUFFERPROC',
  UnmapBuffer      = 'PFNGLUNMAPBUFFERPROC',
  -- core 3.0
  BindBufferBase   = 'PFNGLBINDBUFFERBASEPROC',
  -- BindBufferRange  = 'PFNGLBINDBUFFERRANGEPROC',
  -- core 3.1
  CopyBufferSubData = 'PFNGLCOPYBUFFERSUBDATAPROC',
  -- core 3.3
  MapBufferRange    = 'PFNGLMAPBUFFERRANGEPROC',
  -- FlushMappedBufferRange = 'PFNGLFLUSHMAPPEDBUFFERRANGEPROC',
}

local IBufferGL32 = require('agen.interface.video.buffer')
local IBufferGL32MT = {
	__index = IBufferGL32,
  __gc    = function(self) gl.DeleteBuffers(1, self.id) end,
	__len   = function(self) return self.size end,
	__eq    = function(self, other)
    if other == nil -- comparing to nil
       or
       not ffi.istype('glBuffer_t', other) then -- comparing to non-buffer
      return false
    end
    return self.id[0] == other.id[0]
  end,
	__tostring = function(self)
    return string.format("GL buffer <0x%x:%d>", tonumber(self.target), tonumber(self.id[0]))
  end,
}

local _MAX_ELEMENT_VERTICES   = 0
local _MAX_ELEMENT_INDICES    = 0
local _MAX_ELEMENT_INDEX      = 0
local _MAX_UNIFORM_BLOCK_SIZE = 0

IBufferGL32.mapBufferUsage = {
  constant = gl.STATIC_DRAW, -- IMMUTABLE in ARB_buffer_storage
  static   = gl.STATIC_DRAW,
  dynamic  = gl.DYNAMIC_DRAW,
  staging  = gl.DYNAMIC_DRAW,
}
--------------------------------------------------------------
--
-- One-time init, needs valid context.
--
--------------------------------------------------------------
function IBufferGL32.setup(ctx)
  local ver = ctx.getVersion()

  for k, v in pairs(symtab) do
    gl[k] = v
  end

  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    gl.import(IBufferGL32, 'buffer', 'ARB_direct_state_access')
  elseif ver >= 4.4 or ctx:isSupported("GL_ARB_buffer_storage") then
    gl.import(IBufferGL32, 'buffer', 'ARB_buffer_storage')
  end

  if ctx:isSupported("GL_NV_shader_buffer_load") then
    gl.import(IBufferGL32, 'buffer', 'NV_shader_buffer_load')
  end

  _MAX_ELEMENT_INDEX      = math.min(2^24 - 1, gl.getCap(gl.MAX_ELEMENT_INDEX))
  _MAX_UNIFORM_BLOCK_SIZE = math.min(2^14,     gl.getCap(gl.MAX_UNIFORM_BLOCK_SIZE))
end

local mapIndexDataType = {
  [1] = gl.UNSIGNED_BYTE,
  [2] = gl.UNSIGNED_SHORT,
  [4] = gl.UNSIGNED_INT
}

function IBufferGL32.new(device, options)
  assert(mapBufferPurpose[options.purpose], "purpose [" .. options.purpose .. '] not implemented')
  --
  -- index buffers need 'ctype'
  --
  if options.purpose == 'index' then
    assert(mapIndexDataType[ffi.sizeof(options.ctype)], "Invalid value for option `ctype'")
  end

  local self = ffi.new('glBuffer_t')
  self.target = mapBufferPurpose[options.purpose]
	self.size   = options.size
  if options.purpose == 'index' then
    self.ctype = mapIndexDataType[ffi.sizeof(options.ctype)]
  end

  self:_alloc():_init(options)
  local err, msg = gl.GetError()
  if err ~= 0 then
    error("create buffer failed " .. msg)
  end
  return self
end

function IBufferGL32:_alloc()
  gl.GenBuffers(1, self.id)
  self:setActive()
  return self
end

function IBufferGL32:_init(options)
  assert(self.mapBufferUsage[options.usage], "Invalid buffer usage " .. options.usage)
  -- allocate size, copy the data for constant buffers
  gl.BufferData(self.target, self.size, options.usage == 'constant' and options.data or nil, self.mapBufferUsage[options.usage])
end

function IBufferGL32:getDevice()
  return agen.video.getDevice()
end
--- Synchronously update buffer
-- @param offset from the begining in bytes
-- @param data   new data pointer
-- @param size   data size in bytes
function IBufferGL32:updateFromBuffer(offset, srcBuffer)
  assert(offset >= 0,                         'offset not 0 or positive')
  assert(srcBuffer ~= nil,                    'source is nil')
  assert(ffi.istype('glBuffer_t', srcBuffer)
         and
         gl.IsBuffer(srcBuffer:getID()),      'source not a buffer')

  gl.BindBuffer(gl.COPY_WRITE_BUFFER, self:getID())
  gl.BindBuffer(gl.COPY_READ_BUFFER, srcBuffer:getID())
  gl.CopyBufferSubData(gl.COPY_READ_BUFFER, gl.COPY_WRITE_BUFFER, 0, offset, #srcBuffer)
  local err, msg = gl.GetError()
  if err ~= 0 then
    error("update buffer failed " .. msg)
  end
end

IBufferGL32.mappingFlags = {
  read               = gl.MAP_READ_BIT,
  write              = gl.MAP_WRITE_BIT,
  read_write         = bit.bor(gl.MAP_READ_BIT, gl.MAP_WRITE_BIT),
  write_discard      = bit.bor(gl.MAP_WRITE_BIT, gl.MAP_INVALIDATE_BUFFER_BIT),
  write_no_overwrite = gl.MAP_WRITE_BIT,
}

--- Map buffer flags
function IBufferGL32:_getMappingFlags(flag)
  assert(self.mappingFlags[flag], "Unsupported mapping flag " .. flag)
  return self.mappingFlags[flag]
end

--- Map whole buffer
function IBufferGL32:_map(flag)
  self:setActive()
  local flags = self:_getMappingFlags(flag)
  local ptr = gl.MapBufferRange(self.target, 0, self.size, flags)
  if ptr == nil then
    local err, msg = gl.GetError()
    error("mapping buffer for [" .. flag .. "] failed " .. msg)
  end
  return ptr
end

function IBufferGL32:_unmap()
  self:setActive()
  -- if offset was set in map() should be 0 here
  --gl.FlushMappedBufferRange(self.target, offset, size)
  if gl.UnmapBuffer(self.target) == 0 then
    local err, msg = gl.GetError()
    if err ~= gl.NO_ERROR then
      --error(string.format("Unmap buffer [%s] failed: %s ", tostring(self), msg))
      agen.log.debug('video', "Unmap buffer [%s] failed: %s", tostring(self), msg)
    else
      agen.log.debug('video', "Unmap buffer failed: unknown error")
    end
  end
end

function IBufferGL32:setActive()
  gl.BindBuffer(self.target, self:getID())
end

function IBufferGL32:type()
  for i, v in pairs(mapBufferPurpose) do
    if self.target == v then
      return i
    end
  end
  error(string.format("Unknown resource type [0x%x]", self.target))
end

--- Bind a buffer object to an indexed buffer target
function IBufferGL32:setBindingIndex(index)
  assert(self.target == gl.UNIFORM_BUFFER, "Invalid buffer target")
  --
  -- Calling glBindBufferBase is equivalent to calling glBindBufferRange() with offset
  -- zero and size equal to the size of the buffer.
  --
  gl.BindBufferBase(self.target, index, self.id[0])
end

function IBufferGL32:getID()
  return self.id[0]
end

function IBufferGL32:getTarget()
  return self.target
end
--- index buffers only: GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT or GL_UNSIGNED_INT
function IBufferGL32:getGLType()
  return self.ctype
end

local mapGLType2Size = {
  [tonumber(gl.UNSIGNED_BYTE)]  = 1,
  [tonumber(gl.UNSIGNED_SHORT)] = 2,
  [tonumber(gl.UNSIGNED_INT)]   = 4,
}
function IBufferGL32:getIndexTypeSize()
  assert(self.ctype > 0, 'ctype not set!')
  assert(mapGLType2Size[tonumber(self.ctype)], 'unknown ctype')
  return mapGLType2Size[tonumber(self.ctype)]
end

return ffi.metatype('glBuffer_t', IBufferGL32MT)
