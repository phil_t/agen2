local string = require('string')
local bit    = require('bit')
local math   = require('math')
local sdl    = require('sdl2')
local gl  = require('agen.video.GL.lib')
local ctx = require('agen.video.GL.context')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t MAX_TEXTURE_SIZE   = 0x0D33;
static const uint16_t UNPACK_ALIGNMENT   = 0x0CF5;
static const uint16_t UNPACK_LSB_FIRST   = 0x0CF1;
static const uint16_t UNPACK_ROW_LENGTH  = 0x0CF2;
static const uint16_t UNPACK_SKIP_PIXELS = 0x0CF4;
static const uint16_t UNPACK_SKIP_ROWS   = 0x0CF3;
static const uint16_t UNPACK_SWAP_BYTES  = 0x0CF0;
static const uint16_t TEXTURE_2D = 0x0DE1;
static const uint16_t TEXTURE_INTERNAL_FORMAT = 0x1003;
static const uint16_t TEXTURE_BORDER_COLOR    = 0x1004;
static const uint16_t COMPRESSED_RGB_S3TC_DXT1  = 0x83F0;
static const uint16_t COMPRESSED_RGBA_S3TC_DXT1 = 0x83F1;
static const uint16_t COMPRESSED_RGBA_S3TC_DXT3 = 0x83F2;
static const uint16_t COMPRESSED_RGBA_S3TC_DXT5 = 0x83F3;
static const uint16_t TEXTURE0       = 0x84C0;
static const uint16_t TEXTURE1       = 0x84C1;
static const uint16_t TEXTURE2       = 0x84C2;
static const uint16_t TEXTURE3       = 0x84C3;
static const uint16_t ACTIVE_TEXTURE = 0x84E0;
static const uint16_t UNSIGNED_INT_24_8 = 0x84FA;
static const uint16_t DEPTH24_STENCIL8  = 0x88F0;
static const uint16_t MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
static const uint16_t TEXTURE_2D_ARRAY = 0x8C1A;
static const uint16_t COMPRESSED_RED_RGTC1        = 0x8DBB;
static const uint16_t COMPRESSED_SIGNED_RED_RGTC1 = 0x8DBC;
static const uint16_t COMPRESSED_RG_RGTC2         = 0x8DBD;
static const uint16_t COMPRESSED_SIGNED_RG_RGTC2  = 0x8DBE;
static const uint16_t TEXTURE_SWIZZLE_R    = 0x8E42;
static const uint16_t TEXTURE_SWIZZLE_G    = 0x8E43;
static const uint16_t TEXTURE_SWIZZLE_B    = 0x8E44;
static const uint16_t TEXTURE_SWIZZLE_A    = 0x8E45;
static const uint16_t TEXTURE_SWIZZLE_RGBA = 0x8E46;
static const uint16_t COMPRESSED_RGBA_BPTC_UNORM_ARB         = 0x8E8C;
static const uint16_t COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB   = 0x8E8D;
static const uint16_t COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB   = 0x8E8E;
static const uint16_t COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB = 0x8E8F;
static const uint16_t TEXTURE_2D_MULTISAMPLE       = 0x9100;
static const uint16_t TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
static const uint16_t UNPACK_COMPRESSED_BLOCK_WIDTH  = 0x9127;
static const uint16_t UNPACK_COMPRESSED_BLOCK_HEIGHT = 0x9128;
static const uint16_t UNPACK_COMPRESSED_BLOCK_DEPTH  = 0x9129;
static const uint16_t UNPACK_COMPRESSED_BLOCK_SIZE   = 0x912A;

typedef struct _glTexture_t {
  GLuint  id[1];
  GLint   width, height;
  GLenum  target, format;
} glTexture_t;

typedef void (*PFNGLACTIVETEXTUREPROC)(GLenum texture);
typedef void (*PFNGLTEXPARAMETERIPROC)(GLenum target, GLenum pname, const GLint params);
typedef void (*PFNGLTEXPARAMETERIVPROC)(GLenum target, GLenum pname, const GLint *params);
typedef void (*PFNGLGETTEXLEVELPARAMETERIVPROC)(GLenum target, GLint level, GLenum pname, GLint *params);
typedef void (*PFNGLGENTEXTURESPROC)(GLsizei n, GLuint* textures);
typedef void (*PFNGLBINDTEXTUREPROC)(GLenum target, GLuint texture);
typedef void (*PFNGLDELETETEXTURESPROC)(GLsizei n, const GLuint* textures);
typedef void (*PFNGLPIXELSTOREIPROC)(GLenum pname, GLint param);
typedef GLboolean (*PFNGLISTEXTUREPROC)(GLuint texture);
typedef void (*PFNGLTEXSUBIMAGE2DPROC)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (*PFNGLTEXSUBIMAGE3DPROC)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);
typedef void (*PFNGLGENERATEMIPMAPPROC)(GLenum mode);
typedef void (*PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid *data);
typedef void (*PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data);
]])

local GLTexture = require('agen.interface.video.texture')
local GLTextureMT = {
	__index = GLTexture,
	__tostring = function(self)
		return string.format("texture<%d> [%dx%d]", self.id[0], self.width, self.height)
	end,
  __gc = function(self) gl.DeleteTextures(1, self.id) end,
}

local glTexFormats = {
  color = {
	  [8] = gl.RED,
    [16] = gl.RG,
	  [24] = gl.RGB,
	  [32] = gl.RGBA,
    DXT1 = gl.COMPRESSED_RGBA_S3TC_DXT1,
    DXT3 = gl.COMPRESSED_RGBA_S3TC_DXT3,
    DXT5 = gl.COMPRESSED_RGBA_S3TC_DXT5,
    ATI1 = gl.COMPRESSED_RED_RGTC1,
    ATI2 = gl.COMPRESSED_RG_RGTC2,
    -- DXGI compressed formats
    [70] = gl.COMPRESSED_RGBA_S3TC_DXT1,
    [71] = gl.COMPRESSED_RGBA_S3TC_DXT1,
    [72] = gl.COMPRESSED_RGBA_S3TC_DXT1,
    [73] = gl.COMPRESSED_RGBA_S3TC_DXT3,
    [74] = gl.COMPRESSED_RGBA_S3TC_DXT3,
    [75] = gl.COMPRESSED_RGBA_S3TC_DXT3,
    [76] = gl.COMPRESSED_RGBA_S3TC_DXT5,
    [77] = gl.COMPRESSED_RGBA_S3TC_DXT5,
    [78] = gl.COMPRESSED_RGBA_S3TC_DXT5,
    [94] = gl.COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB,
    [95] = gl.COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB,
    [96] = gl.COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB,
    [97] = gl.COMPRESSED_RGBA_BPTC_UNORM_ARB,
    [98] = gl.COMPRESSED_RGBA_BPTC_UNORM_ARB,
    [99] = gl.COMPRESSED_RGBA_BPTC_UNORM_ARB,
  },
  depth = {
    [16] = gl.DEPTH_COMPONENT16,
    [24] = gl.DEPTH_COMPONENT24,
    [32] = gl.DEPTH_COMPONENT32,
  },
  --
  -- Stencil formats can only be used for Textures if OpenGL 4.4 or
  -- ARB_texture_stencil8 is available.
  --
  stencil = {
    [1] = 0, --gl.STENCIL_INDEX1,
    [4] = 0, --gl.STENCIL_INDEX4,
    [8] = 0, --gl.STENCIL_INDEX8,
    [16] = 0, --gl.STENCIL_INDEX16,
  },
  --
  -- Core 3.0 or EXT_packed_depth_stencil
  --
  depth_stencil = {
    [32] = gl.DEPTH24_STENCIL8,
  },
}

local symtab = {
  ActiveTexture  = 'PFNGLACTIVETEXTUREPROC',
	GenTextures		 = 'PFNGLGENTEXTURESPROC',
	DeleteTextures = 'PFNGLDELETETEXTURESPROC',
	BindTexture		 = 'PFNGLBINDTEXTUREPROC',
	IsTexture			 = 'PFNGLISTEXTUREPROC',
	PixelStorei		 = 'PFNGLPIXELSTOREIPROC',
	TexSubImage2D	 = 'PFNGLTEXSUBIMAGE2DPROC',
	-- TexSubImage3D	 = 'PFNGLTEXSUBIMAGE3DPROC',
	-- TexParameteri  = 'PFNGLTEXPARAMETERIPROC',
	TexParameteriv = 'PFNGLTEXPARAMETERIVPROC',
  GetTexLevelParameteriv = 'PFNGLGETTEXLEVELPARAMETERIVPROC',
	GenerateMipmap = 'PFNGLGENERATEMIPMAPPROC',
  CompressedTexSubImage2D = 'PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC',
  -- CompressedTexSubImage3D = 'PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC',
}

local function roundDownPowerOfTwo(v)
  assert(v > 0, "Invalid value, positive non-0 expected")
  while v > 0 do
    if bit.band(v, (v - 1)) == 0 and v <= 8 then
      agen.log.debug('video', "UNPACK_ALIGNMENT rounded down to [%d]", v)
      return v
    end
    v = bit.rshift(v, 1)
  end
  error("shouldn't be here")
end

function GLTexture.swizzleFromSurfaceFormat(fmt)
  assert(ffi.istype('SDL_PixelFormat', fmt), "Invalid argument #1, SDL_PixelFormat expected")
  local ret = ffi.new('GLint[4]', {gl.ONE, gl.ONE, gl.ONE, gl.ONE})
  if fmt.BytesPerPixel == 4 then
    ret[fmt.Rshift / 8] = gl.RED
    ret[fmt.Gshift / 8] = gl.GREEN
    ret[fmt.Bshift / 8] = gl.BLUE
    if fmt.Ashift ~= 0 then
      ret[fmt.Ashift / 8] = gl.ALPHA
    end
  elseif fmt.BytesPerPixel == 3 then
    ret[fmt.Rshift / 8] = gl.RED
    ret[fmt.Gshift / 8] = gl.GREEN
    ret[fmt.Bshift / 8] = gl.BLUE
  elseif fmt.BytesPerPixel == 1 then
    ret[3] = gl.RED
  else
    agen.log.info('video', "Unexpected surface format: bpp = %d", fmt.BytesPerPixel)
    return nil
  end
  return ret
end

local _MAX_TEXTURE_SIZE = 0
local _MAX_ARRAY_TEXTURE_LAYERS = 0

function GLTexture.setup(context)
  for k, v in pairs(symtab) do
    gl[k] = v
  end

  local ver = context.getVersion()
  --
  -- core 3.2 or GL_ARB_texture_swizzle required
  --
  if ver < 3.3 and not context:isSupported("GL_ARB_texture_swizzle") then
    error(string.format("Unsupported GL version [%f]", ver))
  end

  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    gl.import(GLTexture, 'texture', 'ARB_direct_state_access')
  elseif ver >= 4.2 or context:isSupported("GL_ARB_texture_storage") then
    gl.import(GLTexture, 'texture', 'ARB_texture_storage')
  else
    gl.import(GLTexture, 'texture', 'EXT_texture')
  end

  if ver >= 4.3 or ctx:isSupported('GL_ARB_internalformatquery2') then
    gl.import(GLTexture, 'texture', 'ARB_internalformat_query2')
  end

  _MAX_TEXTURE_SIZE         = math.min(4096, gl.getCap(gl.MAX_TEXTURE_SIZE))
  -- maximum number of layers allowed in an array texture
  _MAX_ARRAY_TEXTURE_LAYERS = math.min( 256, gl.getCap(gl.MAX_ARRAY_TEXTURE_LAYERS))

  agen.log.debug('video', 'using MAX_TEXTURE_SIZE [%d]', _MAX_TEXTURE_SIZE)
end

local newGLTexture = ffi.typeof('glTexture_t')
--- Create new 2D texture
-- @param options lua table.
-- Required:
--  width, height
--  format - SDL format
-- Optional:
--  bytesPerPixel
--  pitch
--  levels
--  swizzle
--  samples (not implemented)
--  purpose - color, depth, stencil, data
function GLTexture.new(options)
  assert(options.format, "Missing required parameter format")
  assert(options.width,  "Missing required parameter width")
  assert(options.width >= 0 and
         options.width <= _MAX_TEXTURE_SIZE, 'texture width not supported')
  assert(options.height, "Missing required parameter height")
  assert(options.height >= 0 and
         options.height <= _MAX_TEXTURE_SIZE, 'texture height not supported')

  local self = newGLTexture()

  local target = gl.TEXTURE_2D
  if options.samples and options.samples > 1 then
    target = gl.TEXTURE_2D_MULTISAMPLE
  end
  self.target = target

  gl.GetError()
  self:_alloc()

  if options.bytesPerPixel then
    agen.log.verbose('video', "bytes per pixel in memory = [%d]", options.bytesPerPixel)
    --
    -- Specifies the alignment requirements for the start of each pixel row in memory. 
    -- The allowable values are 1 (byte-alignment), 2 (rows aligned to even-numbered bytes), 
    -- 4 (word-alignment), and 8 (rows start on double-word boundaries).
    --
	  gl.PixelStorei(gl.UNPACK_ALIGNMENT, roundDownPowerOfTwo(options.bytesPerPixel))
	  -- row length in pixels
    if options.pitch then
      local row_length = math.floor(options.pitch / options.bytesPerPixel)
      agen.log.debug('video', "UNPACK_ROW_LENGTH = [%d] pixels", row_length)
	    gl.PixelStorei(gl.UNPACK_ROW_LENGTH, row_length)
    end
  end

	self.width  = options.width
	self.height = options.height
  self.format = self:_glFormatFromSDLFormat(options.format, options.purpose or 'color')

  if options.sdlformat then
    local swizzle = GLTexture.swizzleFromSurfaceFormat(options.sdlformat)
    agen.log.debug('video', "GL texture swizzle [%x][%x][%x][%x]",
      swizzle[0],
      swizzle[1],
      swizzle[2],
      swizzle[3])
    self:swizzle(swizzle)
  --
  -- ugly hack: determine swizzle from options.format
  --
  elseif options.bgra then
    local s = ffi.new('GLint[4]', {gl.BLUE, gl.GREEN, gl.RED, gl.ALPHA})
    self:swizzle(s)
  elseif options.format == sdl.PIXELFORMAT_A8 then
    local s = ffi.new('GLint[4]', {gl.ONE, gl.ONE, gl.ONE, gl.RED})
		self:swizzle(s)
	end

  if not options.levels or options.levels == 0 then
    options.levels = 1
  end

  self:_init(options)

  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("failed to create texture [%s]", msg))
  end

  return self
end

function GLTexture:_alloc()
  assert(self.target, 'texture target not set')
  gl.GenTextures(1, self.id)
  gl.BindTexture(self.target, self:getID())
  return self
end

function GLTexture:swizzle(val)
  gl.TexParameteriv(self.target, gl.TEXTURE_SWIZZLE_RGBA, val)
end
--- Update part of texture on a specific level
-- @param level - texture level to update
-- @param destRect - destinaton SDL_Rect or nil for whole dest texture
-- @param data
-- @param dataLen length
-- @param dataPitch pitch
function GLTexture:_updateRect(level, destRect, data, dataLen, dataPitch)
  local x, y, w, h
  if destRect == nil then
    x, y, w, h = 0, 0, self:getSize()
  else
    x, y, w, h = destRect.x, destRect.y, destRect.w, destRect.h
  end

  if self.format > 0x8000 then
    assert(dataLen > 0, "data length is required for compressed formats")
    gl.CompressedTexSubImage2D(self.target, level, x, y, w, h, self.format, dataLen, data);
  else
    gl.TexSubImage2D(self.target, level, x, y, w, h, self.format, gl.UNSIGNED_BYTE, data)
  end

  return gl.GetError()
end

function GLTexture:getID()
  return self.id[0]
end

function GLTexture:type()
  if self.target == gl.TEXTURE_2D or 
     self.target == gl.TEXTURE_2D_MULTISAMPLE then
    return 'texture2D'
  end
  error("Not implemented")
end

function GLTexture:setActive(texUnitID, samplerObj)
  assert(texUnitID ~= nil and texUnitID >= 0, 'invalid argument #1, texture unit expected')
  assert(samplerObj ~= nil and ffi.istype('glSampler_t', samplerObj), 'invalid argument #2, sampler object expected')
  gl.ActiveTexture(gl.TEXTURE0 + texUnitID)
  gl.BindTexture(self.target, self.id[0])
  samplerObj:setActive(texUnitID)
end

function GLTexture:_getSize()
  return self.width, self.height
end

function GLTexture:generateMipLevels()
  gl.GenerateMipmap(self.format)
  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("failed to create texture [%s]", msg))
  end
end

function GLTexture:_glFormatFromSDLFormat(fmt, purpose)
  if bit.band(bit.rshift(fmt, 28), 0xf) == 1 then
    -- SDL format
    local bpp = tonumber(bit.band(fmt, 0x000000ff) * 8)
    assert(glTexFormats[purpose][bpp], "Unsupported format depth [" .. purpose .. ':' .. bpp .. "]")
    return glTexFormats[purpose][bpp]
  elseif fmt < 256 then
    -- DXGI format
    agen.log.debug('video', "using DXGI format [%d]", tonumber(fmt))
    -- GL 4.2
    if fmt >= 94 and fmt <= 99 and not ctx:isSupported("GL_ARB_texture_compression_bptc") then
      error("BPTC texture compression not supported")
    end
    return glTexFormats[purpose][tonumber(fmt)]
  else
    local fourcc = string.char(bit.band(bit.rshift(fmt,  0), 0xff)) .. 
                   string.char(bit.band(bit.rshift(fmt,  8), 0xff)) .. 
                   string.char(bit.band(bit.rshift(fmt, 16), 0xff)) .. 
                   string.char(bit.band(bit.rshift(fmt, 24), 0xff))
    agen.log.debug('video', "using FOURCC format [%s]", fourcc)
    assert(glTexFormats[purpose][fourcc], "Unsupported FOURCC format [" .. fourcc .. "]")

    if string.match(fourcc, 'DXT') and not ctx:isSupported('GL_EXT_texture_compression_s3tc') then
      error("S3 texture compression not supported")
    elseif string.match(fourcc, 'ATI') and 
      not ctx:isSupported('GL_ARB_texture_compression_rgtc') and
      not ctx:isSupported('GL_EXT_texture_compression_rgtc') then
      error("ATIx texture compression not supported")
    end
    return glTexFormats[purpose][fourcc]
  end
end

return ffi.metatype('glTexture_t', GLTextureMT)
