-------------------------------------------------------------------------
--
-- Core 4.4 or ARB_buffer_storage
--
-------------------------------------------------------------------------
local bit = require('bit')
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi
local bor = bit.bor

ffi.cdef([[
// Accepted by the <pname> parameter of GetBufferParameter{i|i64}v:
static const uint16_t BUFFER_IMMUTABLE_STORAGE = 0x821F;
static const uint16_t BUFFER_STORAGE_FLAGS     = 0x8220;
// Also accepted as part of the <access> parameter to MapBufferRange:
static const uint32_t MAP_PERSISTENT_BIT  = 0x00000040; // keep the buffer mapped the whole time
static const uint32_t MAP_COHERENT_BIT    = 0x00000080; // guarantee GL will "see" your changes - may wait
// The contents of the data store may be updated after creation through calls to BufferSubData. If this bit 
// is not set, the buffer content may not be directly updated by the client. The <data> argument may be used 
// to specify the initial content of the buffer's data store regardless of the presence of the DYNAMIC_STORAGE_BIT.
static const uint32_t DYNAMIC_STORAGE_BIT = 0x00000100;
// When all other criteria for the buffer storage allocation are met, this bit may be used by an implementation 
// to determine whether to use storage that is local to the server or to the client to serve as the backing store 
// for the buffer (application will manage the buffer memory).
static const uint32_t CLIENT_STORAGE_BIT  = 0x00000200;
// Accepted by the <barriers> parameter of MemoryBarrier:
static const uint32_t CLIENT_MAPPED_BUFFER_BARRIER_BIT = 0x00004000;

typedef void (*PFNGLBUFFERSTORAGEPROC)(GLenum target, GLsizeiptr size, const GLvoid* data, GLbitfield flags);
]])

local VBS = {}

VBS.mappingFlags = {
  read          = gl.MAP_READ_BIT,
  write         = gl.MAP_WRITE_BIT,
  read_write    = bor(gl.MAP_READ_BIT, gl.MAP_WRITE_BIT),
  write_discard	= bor(gl.MAP_WRITE_BIT, gl.MAP_INVALIDATE_BUFFER_BIT, gl.MAP_COHERENT_BIT, gl.MAP_PERSISTENT_BIT),
  write_no_overwrite = bor(gl.MAP_WRITE_BIT, gl.MAP_COHERENT_BIT, gl.MAP_PERSISTENT_BIT)
}

VBS.symtab = {
  BufferStorage = 'PFNGLBUFFERSTORAGEPROC',
}

VBS.mapBufferUsage = {
  constant = 0,
	-- can only update via staging buffer
  static   = 0,
  -- GL_DYNAMIC_STORAGE_BIT:
  -- The contents of the data store may be updated after creation through calls to glBufferSubData. 
  -- If this bit is not set, the buffer content may not be directly updated by the client. The data 
  -- argument may be used to specify the initial content of the buffer's data store regardless of the 
  -- presence of the GL_DYNAMIC_STORAGE_BIT. Regardless of the presence of this bit, buffers may always 
  -- be updated with server-side calls such as glCopyBufferSubData glClearBufferSubData.
  -- MAP_PERSISTENT_BIT - always mapped
  -- MAP_COHERENT_BIT   - make changes visible to GPU without explicit flush & fence
	-- can map and update from memory buffer
  dynamic  = bor(gl.MAP_WRITE_BIT, gl.MAP_COHERENT_BIT, gl.MAP_PERSISTENT_BIT),
  -- host can write and change is visible to GPU without explicit flush
  staging  = bor(gl.MAP_WRITE_BIT),
}

--- initialize buffer
function VBS:_init(options)
  assert(options, 'invalid argument #1, buffer options cannot be nil')
  assert(type(options) == 'table', 'invalid argument #1, table expected')
  assert(self.mapBufferUsage[options.usage], "Invalid buffer usage " .. options.usage)
  -- data can be nil
  gl.BufferStorage(self.target, self.size, options.usage == 'constant' and options.data or nil, self.mapBufferUsage[options.usage])
end

return VBS
