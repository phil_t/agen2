--------------------------------------------------------------------
--
-- GL_NV_shader_buffer_load - refer to buffer via its GPU address.
--
--------------------------------------------------------------------
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t BUFFER_GPU_ADDRESS_NV           = 0x8F1D;
static const uint16_t GPU_ADDRESS_NV                  = 0x8F34;
static const uint16_t MAX_SHADER_BUFFER_ADDRESS_NV    = 0x8F35;

typedef uint64_t GLuint64EXT;
//typedef void (*PFNGLMAKEBUFFERRESIDENTNVPROC)(GLenum target, GLenum access);
//typedef void (*PFNGLMAKEBUFFERNONRESIDENTNVPROC)(GLenum target);
//typedef GLboolean (*PFNGLISBUFFERRESIDENTNVPROC)(GLenum target);
typedef void (*PFNGLMAKENAMEDBUFFERRESIDENTNVPROC)(GLuint buffer, GLenum access);
typedef void (*PFNGLMAKENAMEDBUFFERNONRESIDENTNVPROC)(GLuint buffer);
typedef GLboolean (*PFNGLISNAMEDBUFFERRESIDENTNVPROC)(GLuint buffer);
typedef void (*PFNGLGETBUFFERPARAMETERUI64VNVPROC)(GLenum target, GLenum pname, GLuint64EXT *params);
typedef void (*PFNGLGETNAMEDBUFFERPARAMETERUI64VNVPROC)(GLuint buffer, GLenum pname, GLuint64EXT *params);
typedef void (*PFNGLGETINTEGERUI64VNVPROC)(GLenum value, GLuint64EXT *result);
//typedef void (*PFNGLUNIFORMUI64NVPROC)(GLint location, GLuint64EXT value);
//typedef void (*PFNGLUNIFORMUI64VNVPROC)(GLint location, GLsizei count, const GLuint64EXT *value);
//typedef void (*PFNGLPROGRAMUNIFORMUI64NVPROC)(GLuint program, GLint location, GLuint64EXT value);
//typedef void (*PFNGLPROGRAMUNIFORMUI64VNVPROC)(GLuint program, GLint location, GLsizei count, const GLuint64EXT *value);
typedef void (*PFNGLGETINTEGERUI64I_VNVPROC)(GLenum value, GLuint index, GLuint64EXT *result);
]])

local NSBL = {}

NSBL.symtab = {
  MakeNamedBufferResidentNV     = 'PFNGLMAKENAMEDBUFFERRESIDENTNVPROC',
  MakeNamedBufferNonResidentNV  = 'PFNGLMAKENAMEDBUFFERNONRESIDENTNVPROC',
  IsNamedBufferResidentNV       = 'PFNGLISNAMEDBUFFERRESIDENTNVPROC',
  GetNamedBufferParameterui64vNV = 'PFNGLGETNAMEDBUFFERPARAMETERUI64VNVPROC',
  --GetIntegerui64i_vNV           = 'PFNGLGETINTEGERUI64I_VNVPROC',
}

function NSBL:getGPUAddress()
  local bufferAddr = ffi.new('GLuint64EXT[1]')
  local bufferID = self:getID()

  if gl.IsNamedBufferResidentNV(bufferID) ~= gl.TRUE then
    gl.MakeNamedBufferResidentNV(bufferID, gl.READ_ONLY)
  end
  gl.GetNamedBufferParameterui64vNV(bufferID, gl.BUFFER_GPU_ADDRESS_NV, bufferAddr)
  gl.MakeNamedBufferNonResidentNV(bufferID)

  agen.log.debug('video', "Vertex buffer [%s] GPU address [%s]",
    tostring(self),
    tostring(bufferAddr[0]))
  return bufferAddr[0]
end

return NSBL
