-------------------------------------------------------------------------
--
-- GL_ARB_direct_state_access
--
-------------------------------------------------------------------------
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi
-- on top of ARB_buffer_storage
local DSA = require('agen.video.GL.buffer.ARB_buffer_storage')

ffi.cdef([[
typedef GLvoid (*PFNGLCREATEBUFFERSPROC)(GLsizei n, GLuint* buffers);
typedef GLvoid (*PFNGLNAMEDBUFFERSTORAGEPROC)(GLuint buffer, GLsizeiptr size, const GLvoid *data, GLbitfield flags);
// a white lie to avoid casting
// typedef uint8_t* (*PFNGLMAPNAMEDBUFFERPROC)(GLuint buffer, GLenum access);
typedef uint8_t* (*PFNGLMAPNAMEDBUFFERRANGEPROC)(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access);
typedef GLboolean (*PFNGLUNMAPNAMEDBUFFERPROC)(GLuint buffer);
typedef GLvoid (*PFNGLCOPYNAMEDBUFFRSUBDATAPROC)(GLuint readBuffer, GLuint writeBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);
]])

DSA.symtab = {
  CreateBuffers       = 'PFNGLCREATEBUFFERSPROC',
  NamedBufferStorage  = 'PFNGLNAMEDBUFFERSTORAGEPROC',
  -- MapNamedBuffer      = 'PFNGLMAPNAMEDBUFFERPROC',
  MapNamedBufferRange = 'PFNGLMAPNAMEDBUFFERRANGEPROC',
  UnmapNamedBuffer    = 'PFNGLUNMAPNAMEDBUFFERPROC',
  CopyNamedBufferSubData = 'PFNGLCOPYNAMEDBUFFRSUBDATAPROC',
}

function DSA:_alloc()
  gl.CreateBuffers(1, self.id)
  return self
end

function DSA:_init(options)
  assert(options, 'invalid argument #1, buffer options cannot be nil')
  assert(type(options) == 'table', 'invalid argument #1, table expected')
  assert(self.mapBufferUsage[options.usage], "Invalid buffer usage " .. options.usage)
  -- data can be nil
  gl.NamedBufferStorage(self:getID(), self.size, options.usage == 'constant' and options.data or nil, self.mapBufferUsage[options.usage])
end

function DSA:setActive()
  -- no op
end

function DSA:updateFromBuffer(offset, srcBuffer)
  gl.CopyNamedBufferSubData(srcBuffer:getID(), self:getID(), 0, offset, srcBuffer.size)
end

function DSA:_map(flag)
  local flags = self:_getMappingFlags(flag)
  local ptr = gl.MapNamedBufferRange(self:getID(), 0, self.size, flags)
  if ptr == nil then
    local err, msg = gl.GetError()
    error("glMapNamedBufferRange failed " .. msg)
  end
  return ptr
end

function DSA:_unmap()
  if gl.UnmapNamedBuffer(self:getID()) == 0 then
    local err, msg = gl.GetError()
    if err ~= gl.NO_ERROR then
      agen.log.error('video', "Unmap buffer [%s] failed: %s", tostring(self), msg)
    else
      agen.log.debug('video', "Unmap buffer failed: unknown error")
    end
  end
end

return DSA
