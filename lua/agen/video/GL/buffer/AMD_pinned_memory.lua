---------------------------------------------------------------------------
--
-- GL_AMD_pinned_memory
-- It allows an existing page of system memory allocated by the application
-- to be used as memory directly accessible to the graphics processor.
--
---------------------------------------------------------------------------
local ffi = require('ffi')

ffi.cdef([[
/*
 * Accepted by the <target> parameters of BindBuffer, BufferData,
 * BufferSubData, MapBuffer, UnmapBuffer, GetBufferSubData,
 * GetBufferParameteriv, GetBufferPointerv, MapBufferRange:
 */
static const uint16_t EXTERNAL_VIRTUAL_MEMORY_BUFFER_AMD = 0x9160;
]]);

local APM = {}
APM.symtab = {}
--
-- If <target> is EXTERNAL_VIRTUAL_MEMORY_BUFFER_AMD, then the client's memory is used directly by
-- the GL for all subsequent operations on the buffer object's data store. In this case, the application
-- must guarantee the existence of the buffer for the lifetime of the buffer object, or until its data store
-- is re-specified by another call to BufferData.
--

return APM
