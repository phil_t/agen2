--------------------------------------------------------------------------
--
-- NV_bindless_multi_draw_indirect
-- use NV bindless buffer for drawing commands
--
--------------------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef struct _BindlessPtrNV {
  GLuint   index;
  GLuint   reserved; 
  GLuint64 address;
  GLuint64 length;
} BindlessPtrNV; 
                                                             
typedef struct _DrawArraysIndirectBindlessCommandNV {
  DrawArraysIndirectCommand   cmd;
  BindlessPtrNV               vertexBuffers[];
} DrawArraysIndirectBindlessCommandNV;

typedef void (*PFNGLMULTIDRAWARRAYSINDIRECTBINDLESSNVPROC)(GLenum mode, const void *indirect, GLsizei drawCount, GLsizei stride, GLint vertexBufferCount);
typedef void (*PFNGLMULTIDRAWELEMENTSINDIRECTBINDLESSNVPROC) (GLenum mode, GLenum type, const void *indirect, GLsizei drawCount, GLsizei stride, GLint vertexBufferCount);
]])

local BMDI = {}

local symtab = {
  MultiDrawArraysIndirectBindless = 'PFNGLMULTIDRAWARRAYSINDIRECTBINDLESSNVPROC',
  MultiDrawElementsIndirectBindless = 'PFNGLMULTIDRAWELEMENTSINDIRECTBINDLESSNVPROC',
}

function BMDI:included(klass)
	agen.log.info('application', 'renderPass: using NV_bindless_multi_draw_indirect')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

return BMDI
