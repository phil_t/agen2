local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef GLvoid (*PFNGLCREATEVERTEXARRAYSPROC)(GLsizei n, GLuint *arrays);
typedef GLvoid (*PFNGLENABLEVERTEXARRAYATTRIBPROC)(GLuint vaobj, GLuint index);
typedef GLvoid (*PFNGLVERTEXARRAYVERTEXBUFFERPROC)(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
typedef GLvoid (*PFNGLVERTEXARRAYATTRIBFORMATPROC)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
typedef GLvoid (*PFNGLVERTEXARRAYATTRIBBINDIGPROC)(GLuint vaobj, GLuint attribindex, GLuint bindingindex);
typedef GLvoid (*PFNGLVERTEXARRAYBINDINGDIVISORPROC)(GLuint vaobj, GLuint bindingindex, GLuint divisor);
typedef GLvoid (*PFNGLVERTEXARRAYELEMENTBUFFERPROC)(GLuint vaobj, GLuint buffer);
]])

local DSA = {}

local symtab = {
    CreateVertexArrays = 'PFNGLCREATEVERTEXARRAYSPROC',
    EnableVertexArrayAttrib = 'PFNGLENABLEVERTEXARRAYATTRIBPROC',
    VertexArrayVertexBuffer = 'PFNGLVERTEXARRAYVERTEXBUFFERPROC',
    VertexArrayAttribFormat = 'PFNGLVERTEXARRAYATTRIBFORMATPROC',
    VertexArrayAttribBinding = 'PFNGLVERTEXARRAYATTRIBBINDIGPROC',
    VertexArrayBindingDivisor = 'PFNGLVERTEXARRAYBINDINGDIVISORPROC',
    VertexArrayElementBuffer = 'PFNGLVERTEXARRAYELEMENTBUFFERPROC'
}

function DSA:included(klass)
  agen.log.info('application', 'renderPass: using ARB_direct_state_access')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function DSA:initVAO(ids)
  gl.CreateVertexArrays(1, ids)
  return ids
end

function DSA:bindVAO(id)
    -- no op
end

function DSA:enableVAO(attr)
    gl.EnableVertexArrayAttrib(self.id[0], attr)
end

function DSA:onIndexBuffer()
    gl.VertexArrayElementBuffer(self:getID(), self.indexSlot:getID())
end

function DSA:onVertexAttribute(attr, v)
  assert(self.class.declType[v.type] ~= nil, 'Invalid vertex declaration')

  local buffer = self.vertexSlots.buffers[v.index]
  gl.VertexArrayVertexBuffer(self:getID(), v.index, buffer:getID(), 0, v.stride)

  local l, t = unpack(self.class.declType[v.type])
  gl.VertexArrayAttribFormat(self:getID(), attr, l, t, v.norm and gl.TRUE or gl.FALSE, v.offset)
  -- set the association between a vertex attribute and 
  -- the vertex buffer binding used by that attribute
  gl.VertexArrayAttribBinding(self:getID(), attr, v.index)
  gl.VertexArrayBindingDivisor(self:getID(), v.index, v.instanced and 1 or 0)
end

return DSA
