--------------------------------------------------------------------
--
-- AMD_multi_draw_indirect
-- Reduces number of draw calls for ARB_draw_indirect
--
--------------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef void (PFNGLMULTIDRAWARRAYSINDIRECTAMDPROC)(GLenum mode, const void *indirect, GLsizei primcount, GLsizei stride);
typedef void (PFNGLMULTIDRAWELEMENTSINDIRECTAMDPROC)(GLenum mode, GLenum type, const void *indirect, GLsizei primcount, GLsizei stride);
]])

local AMDI = {}

local symtab = {
  MultiDrawArraysIndirectAMD   = 'PFNGLMULTIDRAWARRAYSINDIRECTAMDPROC',
  MultiDrawElementsIndirectAMD = 'PFNGLMULTIDRAWELEMENTSINDIRECTAMDPROC',
}

function AMDI:included(klass)
	agen.log.info('application', 'renderPass: using AMD_multi_draw_indirect')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

return AMDI
