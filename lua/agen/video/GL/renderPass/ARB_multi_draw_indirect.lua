--------------------------------------------------------------------
--
-- ARB_multi_draw_indirect / Core 4.3
-- Reduces number of draw calls: commands are added to a buffer and
-- executed in one call from there.
--
--------------------------------------------------------------------
local gl  = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef void (*PFNGLMULTIDRAWARRAYSINDIRECTPROC)(GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride);
typedef void (*PFNGLMULTIDRAWELEMENTSINDIRECTPROC)(GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride);
]])

local MDI = {}

local symtab = {
  MultiDrawArraysIndirect   = 'PFNGLMULTIDRAWARRAYSINDIRECTPROC',
  MultiDrawElementsIndirect = 'PFNGLMULTIDRAWELEMENTSINDIRECTPROC',
}

function MDI:included(klass)
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function MDI:_drawPrimitive(offset, size)
  -- TODO: add to command buffer
end

function MDI:_drawIndexedPrimitive(offset, size, ioffset)
  -- TODO: add to command buffer
end

function MDI:_drawInstancedPrimitive(offset, size, ioffset, count)
  -- TODO: add to command buffer
end

function MDI:_submit()
  -- TODO: MultiDrawElementsIndirect
end

return MDI
