--------------------------------------------------------------
--
-- GL_ARB_multi_bind / Core 4.4
--
--------------------------------------------------------------
local gl  = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef void (*PFNGLBINDBUFFERSBASEPROC)(GLenum target, GLuint first, GLsizei count, const GLuint *buffers);
typedef void (*PFNGLBINDBUFFERSRANGEPROC)(GLenum target, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizeiptr *sizes);
typedef void (*PFNGLBINDTEXTURESPROC)(GLuint first, GLsizei count, const GLuint *textures);
typedef void (*PFNGLBINDSAMPLERSPROC)(GLuint first, GLsizei count, const GLuint *samplers);
typedef void (*PFNGLBINDIMAGETEXTURESPROC)(GLuint first, GLsizei count, const GLuint *textures);
typedef void (*PFNGLBINDVERTEXBUFFERSPROC)(GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides);
]])

local AMB = {}

local symtab = {
  BindBuffersBase = 'PFNGLBINDBUFFERSBASEPROC',
  BindBuffersRange = 'PFNGLBINDBUFFERSRANGEPROC',
  BindTextures = 'PFNGLBINDTEXTURESPROC',
  BindSamplers = 'PFNGLBINDSAMPLERSPROC',
  BindImageTextures = 'PFNGLBINDIMAGETEXTURESPROC',
  BindVertexBuffers = 'PFNGLBINDVERTEXBUFFERSPROC',
}

function AMB:included(klass)
	agen.log.info('application', 'renderPass: using ARB_multi_bind')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function AMB:bindResources()
  -- bind textures
  gl.BindTextures(0, self.textureSlots.numSlots, self.textureSlots.textureIDs)
  -- bind samplers
  gl.BindSamplers(0, self.samplerSlots.numSlots, self.samplerSlots.samplerIDs)
  -- bind uniform buffers
  --gl.BindBuffersBase(gl.UNIFORM_BUFFER, 0, self.uniformSlots.numSlots, self.uniformSlots.bufferIDs)
end

return AMB
