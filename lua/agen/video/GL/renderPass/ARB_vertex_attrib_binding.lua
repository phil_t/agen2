----------------------------------------------------------------
--
-- GL_ARB_vertex_attrib_binding / core 4.3
--
----------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
static const uint16_t VERTEX_ATTRIB_BINDING             = 0x82D4;
static const uint16_t VERTEX_ATTRIB_RELATIVE_OFFSET     = 0x82D5;
static const uint16_t VERTEX_BINDING_DIVISOR            = 0x82D6;
static const uint16_t VERTEX_BINDING_OFFSET             = 0x82D7;
static const uint16_t VERTEX_BINDING_STRIDE             = 0x82D8;
static const uint16_t MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
static const uint16_t MAX_VERTEX_ATTRIB_BINDINGS        = 0x82DA;
static const uint16_t VERTEX_BINDING_BUFFER             = 0x8F4F;

typedef void (*PFNGLBINDVERTEXBUFFERPROC)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride);
typedef void (*PFNGLVERTEXATTRIBFORMATPROC)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset);
typedef void (*PFNGLVERTEXATTRIBIFORMATPROC)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset);
typedef void (*PFNGLVERTEXATTRIBLFORMATPROC)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset);
typedef void (*PFNGLVERTEXATTRIBBINDINGPROC)(GLuint attribindex, GLuint bindingindex);
typedef void (*PFNGLVERTEXBINDINGDIVISORPROC)(GLuint bindingindex, GLuint divisor);
]])

local VAB = {}

local symtab = {  
  BindVertexBuffer     = 'PFNGLBINDVERTEXBUFFERPROC',
  VertexAttribFormat   = 'PFNGLVERTEXATTRIBFORMATPROC',
  VertexAttribBinding  = 'PFNGLVERTEXATTRIBBINDINGPROC',
  VertexBindingDivisor = 'PFNGLVERTEXBINDINGDIVISORPROC',
}

function VAB:included(klass)
	agen.log.info('application', 'renderPass: using ARB_vertex_attrib_binding')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function VAB:onVertexAttribute(attr, v)
	assert(self.class.declType[v.type] ~= nil, 'Invalid vertex declaration')

  local buffer = self.vertexSlots.buffers[v.index]
  -- bind a buffer <buffer> to the vertex buffer bind point 
  -- <bindingindex>, and sets the <stride> between elements 
  -- and <offset> (in basic machine units) of the first element in the buffer.
  gl.BindVertexBuffer(v.index, buffer:getID(), 0, v.stride)

  local l, t = unpack(self.class.declType[v.type])
  gl.VertexAttribFormat(attr, l, t, v.norm and gl.TRUE or gl.FALSE, v.offset)
  -- set the association between a vertex attribute and 
  -- the vertex buffer binding used by that attribute
  gl.VertexAttribBinding(attr, v.index)
  --
  -- Note: 
  -- glVertexBindingDivisor uses currently bound vertex array object, 
  -- whereas glVertexArrayBindingDivisor updates state of the vertex 
  -- array object with ID vaobj.
  --
  gl.VertexBindingDivisor(v.index, v.instanced and 1 or 0)
end

return VAB
