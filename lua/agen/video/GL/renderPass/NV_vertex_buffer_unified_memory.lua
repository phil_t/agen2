--------------------------------------------------------------------
--
-- GL_NV_vertex_buffer_unified_memory
-- This extension provides a mechanism to specify vertex attrib and
-- element array locations using GPU addresses.
--
--------------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
static const uint16_t VERTEX_ATTRIB_ARRAY_UNIFIED_NV  = 0x8F1E;
static const uint16_t ELEMENT_ARRAY_UNIFIED_NV        = 0x8F1F;
static const uint16_t VERTEX_ATTRIB_ARRAY_ADDRESS_NV  = 0x8F20;
static const uint16_t ELEMENT_ARRAY_ADDRESS_NV        = 0x8F29;
static const uint16_t VERTEX_ATTRIB_ARRAY_LENGTH_NV   = 0x8F2A;
static const uint16_t ELEMENT_ARRAY_LENGTH_NV         = 0x8F33;

typedef void (*PFNGLGETBUFFERPARAMETERUI64VNVPROC)(GLenum target, GLenum pname, GLuint64EXT *params);
typedef void (*PFNGLGETNAMEDBUFFERPARAMETERUI64VNVPROC)(GLuint buffer, GLenum pname, GLuint64EXT *params);
typedef void (*PFNGLGETINTEGERUI64VNVPROC)(GLenum value, GLuint64EXT *result);
//typedef void (*PFNGLUNIFORMUI64NVPROC)(GLint location, GLuint64EXT value);
//typedef void (*PFNGLUNIFORMUI64VNVPROC)(GLint location, GLsizei count, const GLuint64EXT *value);
//typedef void (*PFNGLPROGRAMUNIFORMUI64NVPROC)(GLuint program, GLint location, GLuint64EXT value);
//typedef void (*PFNGLPROGRAMUNIFORMUI64VNVPROC)(GLuint program, GLint location, GLsizei count, const GLuint64EXT *value);
typedef void (*PFNGLBUFFERADDRESSRANGENVPROC)(GLenum pname, GLuint index, GLuint64EXT address, GLsizeiptr length);
typedef void (*PFNGLVERTEXATTRIBFORMATNVPROC)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride);
typedef void (*PFNGLVERTEXATTRIBIFORMATNVPROC)(GLuint index, GLint size, GLenum type, GLsizei stride);
typedef void (*PFNGLGETINTEGERUI64I_VNVPROC)(GLenum value, GLuint index, GLuint64EXT *result);
]])

local NV = {}

local symtab = {
  BufferAddressRangeNV          = 'PFNGLBUFFERADDRESSRANGENVPROC',
  VertexAttribFormatNV          = 'PFNGLVERTEXATTRIBFORMATNVPROC',
  --VertexAttribIFormatNV         = 'PFNGLVERTEXATTRIBIFORMATNVPROC',
  --GetIntegerui64i_vNV           = 'PFNGLGETINTEGERUI64I_VNVPROC',
}

function NV:included(klass)
	agen.log.info('application', 'renderPass: using NV_vertex_buffer_unified_memory')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function NV:onVertexAttribute(attr, v)
  assert(v.index, "Required attribute index not set")
  local buffer = self.vertexSlots.buffers[v.index]
  assert(buffer, "Invalid vertex buffer index specified") 
  local bufferAddr = buffer:getGPUAddress()
  assert(bufferAddr, string.format("Buffer at [%d] GPU address not set!", v.index))
  assert(self.class.declType[v.type] ~= nil, 'Invalid vertex declaration')
  
  local l, t = unpack(self.class.declType[v.type])
  gl.VertexAttribFormatNV(attr, l, t, v.norm and gl.TRUE or gl.FALSE, v.stride)
  
  gl.BufferAddressRangeNV(gl.VERTEX_ATTRIB_ARRAY_ADDRESS_NV, attr, bufferAddr + v.offset, #buffer)
  gl.VertexAttribDivisor(attr, v.instanced and 1 or 0)
end

function NV:onIndexBuffer()
  local indexBuffer = self.indexSlot
  local bufferAddr = indexBuffer:getGPUAddress()
  assert(bufferAddr ~= nil, "Index buffer address unknown")
  assert(#indexBuffer > 0, "Index buffer size invalid")
  gl.BufferAddressRangeNV(gl.ELEMENT_ARRAY_ADDRESS_NV, 0, bufferAddr, #indexBuffer)
end

function NV:setActive()
  --
  -- While VERTEX_ATTRIB_ARRAY_UNIFIED_NV is enabled, the rendering 
  -- commands ArrayElement, DrawArrays, DrawElements, DrawRangeElements, 
  -- MultiDrawArrays, DrawArraysInstanced and DrawElementsInstanced 
  -- operate as previously defined, except that data for enabled vertex and
  -- attrib arrays are sourced from GPU addresses specified by the command:
  -- void BufferAddressRangeNV(enum pname, uint index, uint64EXT address, sizeiptr length);
  --  
  gl.EnableClientState(gl.VERTEX_ATTRIB_ARRAY_UNIFIED_NV)
  for i = 0, self.vertexSlots.numSlots - 1, 1 do
    gl.MakeNamedBufferResidentNV(self.vertexSlots.bufferIDs[i], gl.READ_ONLY)
  end
  
  if self.indexSlot then
    --
    -- While ELEMENT_ARRAY_UNIFIED_NV is enabled, DrawElements, 
    -- DrawElementsRange, MultiDrawElements, and DrawElementsInstanced source
    -- their indices from the address specified by the command 
    -- BufferAddressRangeNV where <pname> is ELEMENT_ARRAY_ADDRESS_NV and 
    -- <index> is zero, added to the <indices> parameter.
    --    
    gl.EnableClientState(gl.ELEMENT_ARRAY_UNIFIED_NV)
    gl.MakeNamedBufferResidentNV(self.indexSlot:getID(), gl.READ_ONLY)
  end

  self:bindResources()
end

function NV:_submit()
  gl.DisableClientState(gl.VERTEX_ATTRIB_ARRAY_UNIFIED_NV)
  for i = 0, self.vertexSlots.numSlots - 1, 1 do
    gl.MakeNamedBufferNonResidentNV(self.vertexSlots.bufferIDs[i])
  end

  if self.indexSlot then
    gl.DisableClientState(gl.ELEMENT_ARRAY_UNIFIED_NV)
    gl.MakeNamedBufferNonResidentNV(self.indexSlot:getID())
  end  
end

return NV
