----------------------------------------------------------------
--
-- GL_ARB_base_instance / Core 4.2
--
----------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef void (*PFNGLDRAWELEMENTSINSTANCEDVASEVERTEXBASEINSTANCEPROC)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex, GLuint baseinstance);
]])

local ABI = {}

local symtab = {
  DrawElementsInstancedBaseVertexBaseInstance =
    'PFNGLDRAWELEMENTSINSTANCEDVASEVERTEXBASEINSTANCEPROC',
}

function ABI:included(klass)
	agen.log.info('application', 'renderPass: using ARB_base_instance')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function ABI:drawInstancedPrimitive(offset, size, ioffset, count)
  local indexType = self.indexSlot:getGLType()
  local iOffsetBytes = ioffset * self.indexSlot:getIndexTypeSize()
  gl.DrawElementsInstancedBaseVertexBaseInstance(self.owner.topology, size, indexType, ioffset, count, offset, 0)
end

return ABI
