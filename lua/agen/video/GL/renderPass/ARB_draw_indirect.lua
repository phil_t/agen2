---------------------------------------------------------------
--
-- GL_ARB_draw_indirect / Core 4.0
--
---------------------------------------------------------------
local gl = require('agen.video.GL.lib')

gl.ffi.cdef([[
typedef struct _DrawArraysIndirectCommand {
  GLuint count;
  GLuint primCount;
  GLuint first;
  GLuint reservedMustBeZero;
} DrawArraysIndirectCommand;

typedef struct _DrawElementsIndirectCommand {
  GLuint count;
  GLuint primCount;
  GLuint firstIndex;
  GLint  baseVertex;
  GLuint reservedMustBeZero;
} DrawElementsIndirectCommand;
// buffer targets
static const uint16_t DRAW_INDIRECT_BUFFER         = 0x8F3F;
static const uint16_t DRAW_INDIRECT_UNIFIED_NV     = 0x8F40;
static const uint16_t DRAW_INDIRECT_ADDRESS_NV     = 0x8F41;
static const uint16_t DRAW_INDIRECT_LENGTH_NV      = 0x8F42;
static const uint16_t DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;

typedef void (*PFNGLDRAWARRAYSINDIRECTPROC)(GLenum mode, const DrawArraysIndirectCommand* indirect);
typedef void (*PFNGLDRAWELEMENTSINDIRECTPROC)(GLenum mode, GLenum type, const DrawElementsIndirectCommand*indirect);
]])

local ADI = {}

local symtab = {
  DrawArraysIndirect   = 'PFNGLDRAWARRAYSINDIRECTPROC',
  DrawElementsIndirect = 'PFNGLDRAWELEMENTSINDIRECTPROC',
}

function ADI:included(klass)
	agen.log.info('application', 'renderPass: using ARB_draw_indirect')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function ADI:drawPrimitiveIndirect(bufferIndex, offset)
  error("not implemented")
end

function ADI:drawIndexedPrimitiveIndirect(bufferIndex, offset)
  error("not implemented")
end

return ADI
