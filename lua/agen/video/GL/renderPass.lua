local class = require('middleclass')
local bit   = require('bit')
local math  = require('math')
local IRenderPass = require('agen.interface.video.renderPass')
local gl    = require('agen.video.GL.lib')
local ctx   = require('agen.video.GL.context')

local ffi  = gl.ffi
local bor  = bit.bor
local band = bit.band

ffi.cdef([[
static const uint16_t MAX_ELEMENTS_VERTICES   = 0x80E8;
static const uint16_t MAX_ELEMENTS_INDICES    = 0x80E9;
static const uint16_t MAX_TEXTURE_IMAGE_UNITS = 0x8872;

typedef void (*PFNGLVIEWPORTPROC)(GLint x, GLint y, GLsizei width, GLsizei height);
typedef void (*PFNGLSCISSORPROC)(GLint x, GLint y, GLsizei width, GLsizei height);

typedef void (*PFNGLDISABLEVERTEXATTRIBARRAYPROC)(GLuint index);
typedef void (*PFNGLENABLEVERTEXATTRIBARRAYPROC)(GLuint index);
// GL_ARB_vertex_array_object / Core 3.1
typedef void (*PFNGLBINDVERTEXARRAYPROC)(GLuint array);
typedef void (*PFNGLDELETEVERTEXARRAYSPROC)(GLsizei n, const GLuint* arrays);
typedef void (*PFNGLGENVERTEXARRAYSPROC)(GLsizei n, GLuint* arrays);
typedef GLboolean (*PFNGLISVERTEXARRAYPROC)(GLuint array);
typedef void (*PFNGLVERTEXATTRIBPOINTERPROC)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, uintptr_t pointer);
// GL_EXT_vertex_array or Compat 1.1
typedef void (*PFNGLDRAWARRAYSPROC)(GLenum mode, GLint first, GLsizei count);
// Core 3.2
typedef void (*PFNGLDRAWELEMENTSBASEVERTEXPROC)(GLenum mode, GLsizei count, GLenum type, uintptr_t indices, GLint basevertex);
//typedef void (*PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, uintptr_t indices, GLint basevertex);
typedef void (*PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC)(GLenum mode, GLsizei count, GLenum type, uintptr_t indices, GLsizei instancecount, GLint basevertex);
// GL_ARB_instanced_arrays / Core 3.3
typedef void (*PFNGLVERTEXATTRIBDIVISORPROC)(GLuint index, GLuint divisor);
]])

-- max recommended
local _MAX_ELEMENTS_VERTICES   = 0
local _MAX_ELEMENTS_INDICES    = 0

local _MAX_VERTEX_BUFFER_SLOTS = 8
local _MAX_TEXTURE_UNIT_SLOTS  = 8
local _MAX_UNIFORM_SLOTS       = 8
local _MAX_TEXTURE_IMAGE_UNITS = 0  -- max accessible from fragment shader

local renderPass = class('renderPass')
renderPass:include(IRenderPass)

renderPass.static.declType = {
  float1   = {1, gl.FLOAT},
  float2   = {2, gl.FLOAT},
  float3   = {3, gl.FLOAT},
  float4   = {4, gl.FLOAT},
  vector4  = {4, gl.FLOAT},
  d3dcolor = {4, gl.UNSIGNED_BYTE},
  ubyte4   = {4, gl.UNSIGNED_BYTE},
  none	   = {0, 0},
}

renderPass.static.symtab = {
  Viewport = 'PFNGLVIEWPORTPROC',
  Scissor  = 'PFNGLSCISSORPROC',
  BindVertexArray     = 'PFNGLBINDVERTEXARRAYPROC',
  DeleteVertexArrays  = 'PFNGLDELETEVERTEXARRAYSPROC',
  GenVertexArrays     = 'PFNGLGENVERTEXARRAYSPROC',
  IsVertexArray       = 'PFNGLISVERTEXARRAYPROC',
  EnableVertexAttribArray  = 'PFNGLENABLEVERTEXATTRIBARRAYPROC',
  DisableVertexAttribArray = 'PFNGLDISABLEVERTEXATTRIBARRAYPROC',
  VertexAttribPointer = 'PFNGLVERTEXATTRIBPOINTERPROC',
  VertexAttribDivisor = 'PFNGLVERTEXATTRIBDIVISORPROC',
  DrawArrays            = 'PFNGLDRAWARRAYSPROC',
  DrawElementsBaseVertex = 'PFNGLDRAWELEMENTSBASEVERTEXPROC',
  --DrawRangeElementsBaseVertex = 'PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC',
  DrawElementsInstancedBaseVertex = 'PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC',
}

function renderPass.static:setup(context)
  local ver = context:getVersion()
  --
  -- Vertex attribute definitions
  --
  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    local directStateAccess = require('agen.video.GL.renderPass.ARB_direct_state_access')
    renderPass:include(directStateAccess)
  elseif ver >= 4.3 or (context:isSupported("GL_ARB_vertex_attrib_binding") and
                    context:isSupported("GL_ARB_explicit_attrib_location")) then
    local attribBinding = require('agen.video.GL.renderPass.ARB_vertex_attrib_binding')
    renderPass:include(attribBinding)
  elseif ver >= 3.3 or (context:isSupported("GL_ARB_instanced_arrays") and
                        context:isSupported("GL_ARB_explicit_attrib_location")) then
    agen.log.info('application', 'renderPass: using ARB_vertex_array_object')
  else
    error("Unsupported GL context version")
  end
  --
  -- Indirect draw calls
  --
  if ver >= 4.0 or context:isSupported("GL_ARB_draw_indirect") then
    local drawIndirrect = require('agen.video.GL.renderPass.ARB_draw_indirect')
    renderPass:include(drawIndirrect)
  end
  --
  -- NV bindless vertex buffers
  --
  if context:isSupported("GL_NV_shader_buffer_load") and
     context:isSupported("GL_NV_vertex_buffer_unified_memory") then
    local vertexBufferNV = require('agen.video.GL.renderPass.NV_vertex_buffer_unified_memory')
    renderPass:include(vertexBufferNV)
  end
  --
  -- Bind resources: not implemented
  --
  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    -- no binding needed
  elseif false and (ver >= 4.4 or context:isSupported('GL_ARB_multi_bind')) then
    -- not implemented
    local multiBind = require('agen.video.GL.renderPass.ARB_multi_bind')
    renderPass:include(multiBind)
  end
  --
  -- now import symbols in symtab
  --
  for k, v in pairs(renderPass.symtab) do
    gl[k] = v
  end

  _MAX_TEXTURE_IMAGE_UNITS = math.min(16, gl.getCap(gl.MAX_TEXTURE_IMAGE_UNITS))
  agen.log.debug('video', 'using MAX_TEXTURE_IMAGE_UNITS of [%d]', _MAX_TEXTURE_IMAGE_UNITS)
end

function renderPass:initialize(rState)
  self.owner = rState
  self.id    = nil
  self.vertexSlots  = {
    numSlots  = 0,
    buffers   = {},
    bufferIDs = ffi.new('GLuint[?]', _MAX_VERTEX_BUFFER_SLOTS),
  }
  -- zero-indexed lua table of textures referenced by render pass
  self.textures = {}
  -- zero-indexed lua table of samplers referenced by render pass
  self.samplers = {}
  -- zero-indexed lua table of texture+sampler units
  self.textureUnitSlots = {
    units    = {},
    numSlots = 0,
  }
  self.indexSlot = nil
  self.uniformSlots = {
    numSlots = 0,
    buffers = {},
    bufferIDs = ffi.new('GLuint[?]', _MAX_UNIFORM_SLOTS),
  }
  self.renderTarget = nil
end

function renderPass:_init(buffs)
  for i, b in pairs(buffs.vertexBuffers) do
    self:setVertexBuffer(i, b)
  end

  if buffs.indexBuffer then
    self.indexSlot = buffs.indexBuffer
  end

  if buffs.uniforms then
    -- valid indices: vertex, geometry, fragment
    if buffs.uniforms.vertex then
      for _, data in pairs(buffs.uniforms.vertex) do
        self:setUniformBuffer(nil, data.name, data.data, data.offset or 0)
      end
    end

    if buffs.uniforms.geometry then
      for _, data in pairs(buffs.uniforms.geometry) do
        self:setUniformBuffer(nil, data.name, data.data, data.offset or 0)
      end
    end

    if buffs.uniforms.fragment then
      for _, data in pairs(buffs.uniforms.fragment) do
        self:setUniformBuffer(nil, data.name, data.data, data.offset or 0)
      end
    end
  end

  if buffs.samplers then
    self.samplers = buffs.samplers
  end

  if buffs.textures then
    self.textures = buffs.textures
  end

  if buffs.renderTarget then
    self:setRenderTarget(buffs.renderTarget)
  else
    local defaultRenderTarget = self.owner:getDevice():getDefaultRenderTarget()
    self:setRenderTarget(defaultRenderTarget)
  end
  ----------------------------------------------------------------------------
  --
  -- Setup VAO:
  -- will capture the whole state and restore it on bind.
  -- Enable all the buffers / attributes you need here
  --
  ----------------------------------------------------------------------------
  local ids = ffi.new('GLuint[1]')
  ids = self:initVAO(ids)
  self.id = ffi.gc(ids, function(id)
    gl.DeleteVertexArrays(1, id)
  end)
  self:bindVAO(self:getID())
  --
  -- Process attributes
  --
  local attr
  local lastIndex = -1
  local layout = self.owner:getVertexDeclaration()

  for i, v in pairs(layout) do
    -- OpenGL 3.3+: attribute locations set explicitly
    attr = i - 1
    agen.log.debug('video', "usage [%s] attribute [%d]", v.usage, attr)

    if lastIndex ~= v.index then
      assert(self.vertexSlots.buffers[v.index], "buffer attributes layout: no vertex buffer set for index")
      lastIndex = v.index
    end

    self:enableVAO(attr)
    self:onVertexAttribute(attr, v)
  end

  if self.indexSlot then
    self:onIndexBuffer()
  end

  self:bindVAO(0)

  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("create VAO failed [%s]", msg))
  end
end

function renderPass:initVAO(ids)
  gl.GenVertexArrays(1, ids)
  return ids
end

function renderPass:enableVAO(attr)
  gl.EnableVertexAttribArray(attr)
end

function renderPass:bindVAO(id)
  gl.BindVertexArray(id)
end

function renderPass:onVertexAttribute(attr, v)
  -- bind VAO
  self.vertexSlots.buffers[v.index]:setActive()
  local l, t = unpack(renderPass.declType[v.type])
  --
  -- VAO will remember the currently bound buffer, no need to bind it at draw time
  --
  gl.VertexAttribPointer(attr, l, t, v.norm and gl.TRUE or gl.FALSE, v.stride, v.offset)
  gl.VertexAttribDivisor(attr, v.instanced and 1 or 0)
end

function renderPass:onIndexBuffer()
  -- will fail if no VAO is bound in Core 3.2+ context
  self.indexSlot:setActive()
end

function renderPass:setVertexBuffer(index, buffer)
  assert(index < _MAX_VERTEX_BUFFER_SLOTS, "Too many vertex buffers used")

  if index >= self.vertexSlots.numSlots then
    self.vertexSlots.numSlots = index + 1
  end

  self.vertexSlots.bufferIDs[index] = buffer:getID()
  self.vertexSlots.buffers[index]   = buffer
end

function renderPass:getVertexBuffer(index)
  return self.vertexSlots.buffers[index]
end

function renderPass:getIndexBuffer()
  return self.indexSlot
end
--- Associate texture with texture unit stage / sampler
-- @param samplerIndex sampler object index in samplerIDs array
-- @param sampler sampler object index in samplers array
-- @param texture object index in textures array
function renderPass:_setTexture(samplerIndex, sampler, texture)
  assert(samplerIndex < _MAX_TEXTURE_UNIT_SLOTS, 'sampler index exceeds maxumim supported [' .. _MAX_TEXTURE_UNIT_SLOTS .. ']')
  assert(texture < _MAX_TEXTURE_IMAGE_UNITS, 'texture index exceeds maximum supported [' .. _MAX_TEXTURE_IMAGE_UNITS .. ']')
  assert(self.samplers[sampler], "No sampler at index [" .. sampler .. "]")
  assert(self.textures[texture], "No texture at index [" .. texture .. "]")

  if self.textureUnitSlots.numSlots <= samplerIndex then
    self.textureUnitSlots.numSlots = samplerIndex + 1
  end
  -- store as unit
  self.textureUnitSlots.units[samplerIndex] = {
    texture = self.textures[texture],
    sampler = self.samplers[sampler],
  }
  -- has to bind now. why?
  self.textures[texture]:setActive(samplerIndex, self.samplers[sampler])
end

function renderPass:setUniformBuffer(shader, name, buffer, offset)
  assert(name, "uniform buffer name cannot be nil")
  local prog = self.owner:getProgram()
  local blockIndex   = prog:getUniformBlockIndex(name)
  if blockIndex >= self.uniformSlots.numSlots then
    self.uniformSlots.numSlots = blockIndex + 1
  end

  local bindingIndex = ctx:getBufferBindingIndex()
  --
  -- set binding index for block
  --
  prog:setBindingIndex(blockIndex, bindingIndex)
  --
  -- bind UBO to binding index
  --
  buffer:setBindingIndex(bindingIndex)
  --
  -- store
  --
  self.uniformSlots.buffers[name] = buffer
  self.uniformSlots.bufferIDs[blockIndex] = buffer:getID()
end

function renderPass:_getUniformBuffer(name)
  assert(self.uniformSlots.buffers[name], "non-existing uniform buffer name")
  return self.uniformSlots.buffers[name]
end

function renderPass:setRenderTarget(target)
  self.renderTarget = target
end

function renderPass:bindResources()
  -- bind textures and samplers
  local unit
  for i = 0, self.textureUnitSlots.numSlots - 1, 1 do
    unit = self.textureUnitSlots.units[i]
    unit.texture:setActive(i, unit.sampler)
  end
  -- TODO: bind UBOs
end

function renderPass:getID()
  return self.id[0]
end

function renderPass:setActive()
  self:bindResources()
  if self.renderTarget ~= nil then
    self.renderTarget:setActive()
  end
end
--- set viewport
-- @param x x-coordinate of lower left corner
-- @param y y-coordinate of lower left corner
-- @param w width
-- @param h height
-- @param n near coordinate of view volume
-- @param f far coordinate of view volume
function renderPass:setViewport(l, b, w, h, n, f)
  assert(w > 0, 'viewport width > 0')
  assert(h > 0, 'viewport height > 0')
  gl.Viewport(l, b, w, h)
end
--- set scissor rect
-- @param x x-coordinate of lower left corner
-- @param y y-coordinate of lower left corner
-- @param w width
-- @param h height
function renderPass:setScissor(x, y, w, h)
  assert(w > 0, 'scissor width > 0')
  assert(h > 0, 'scissor height > 0')
  gl.Scissor(x, y, w, h)
end
---------------------------------------------------------------------------
--
-- drawing API
--
---------------------------------------------------------------------------
function renderPass:_begin()
  gl.BindVertexArray(self.id[0])
  self:setActive()
end

function renderPass:_clear()
  self.renderTarget:clear()
end

function renderPass:_clearDepth()
  self.renderTarget:clearDepth()
end

function renderPass:_clearDepthStencil()
  self.renderTarget:clearDepthStencil()
end

function renderPass:_drawPrimitive(offset, size)
  gl.DrawArrays(self.owner.topology, offset, size)
end

function renderPass:_drawIndexedPrimitive(offset, size, ioffset)
  local indexType = self.indexSlot:getGLType()
  local iOffsetBytes = ioffset * self.indexSlot:getIndexTypeSize()
  gl.DrawElementsBaseVertex(self.owner.topology, size, indexType, iOffsetBytes, offset)
end

function renderPass:_drawInstancedPrimitive(offset, size, ioffset, count)
  local indexType = self.indexSlot:getGLType()
  local iOffsetBytes = ioffset * self.indexSlot:getIndexTypeSize()
  gl.DrawElementsInstancedBaseVertex(self.owner.topology, size, indexType, iOffsetBytes, count, offset)
end
--
-- via extensions
--
function renderPass:_drawPrimitiveIndirect(bufferIndex, offset)
  error("drawPrimitiveIndirect: not supported")
end

function renderPass:_drawIndexedPrimitiveIndirect(bufferIndex, offset)
  error("drawIndexedPrimitiveIndirect: not supported")
end
--- submit commands list to device for execution
function renderPass:_submit()
  -- no op
end

return renderPass
