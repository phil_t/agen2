-----------------------------------------------------------------
--
-- context object is for per-thread data
--
-----------------------------------------------------------------
local ffi = require('ffi')
local bit = require('bit')
local string = require('string')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _GL_attr {
  GL_RED_SIZE = 0x00000000,
  GL_GREEN_SIZE,
  GL_BLUE_SIZE,
  GL_ALPHA_SIZE,
  GL_BUFFER_SIZE,
  GL_DOUBLEBUFFER,
  GL_DEPTH_SIZE,
  GL_STENCIL_SIZE,
  GL_ACCUM_RED_SIZE,
  GL_ACCUM_GREEN_SIZE,
  GL_ACCUM_BLUE_SIZE,
  GL_ACCUM_ALPHA_SIZE,
  GL_STEREO,
  GL_MULTISAMPLEBUFFERS,
  GL_MULTISAMPLESAMPLES,
  GL_ACCELERATED_VISUAL,
  GL_RETAINED_BACKING,
  GL_CONTEXT_MAJOR_VERSION,
  GL_CONTEXT_MINOR_VERSION,
  GL_CONTEXT_EGL,
  GL_CONTEXT_FLAGS,
  GL_CONTEXT_PROFILE_MASK,
  GL_SHARE_WITH_CURRENT_CONTEXT,
  GL_FRAMEBUFFER_SRGB_CAPABLE,
  GL_CONTEXT_RELEASE_BEHAVIOR,
  GL_CONTEXT_RESET_NOTIFICATION,
  GL_CONTEXT_NO_ERROR
} SDL_GLattr;

typedef enum _SDL_GLcontextFlag {
  GL_CONTEXT_DEBUG_FLAG              = 0x0001,
  GL_CONTEXT_FORWARD_COMPATIBLE_FLAG = 0x0002,
  GL_CONTEXT_ROBUST_ACCESS_FLAG      = 0x0004,
  GL_CONTEXT_RESET_ISOLATION_FLAG    = 0x0008
} SDL_GLcontextFlag;
//
// see ARB_context_flush_control
//
typedef enum _SDL_GLcontextReleaseFlag {
  GL_CONTEXT_RELEASE_BEHAVIOR_NONE   = 0x0000,
  GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH  = 0x0001
} SDL_GLcontextReleaseFlag;
//
// see ARB_robustness
//
typedef enum _SDL_GLContextResetNotification {
  GL_CONTEXT_RESET_NO_NOTIFICATION = 0x0000,
  GL_CONTEXT_RESET_LOSE_CONTEXT    = 0x0001
} SDL_GLContextResetNotification;

typedef enum _SDL_GLprofile {
  GL_CONTEXT_PROFILE_CORE           = 0x0001,
  GL_CONTEXT_PROFILE_COMPATIBILITY  = 0x0002,
  GL_CONTEXT_PROFILE_ES             = 0x0004 /* GLX_CONTEXT_ES2_PROFILE_BIT_EXT */
} SDL_GLprofile;

typedef struct _SDL_GLContext SDL_GLContext;

SDL_GLContext* SDL_GL_CreateContext(SDL_Window* window);
SDL_GLContext* SDL_GL_GetCurrentContext(void);
void SDL_GL_DeleteContext(SDL_GLContext* context);
int SDL_GL_MakeCurrent(SDL_Window* window, SDL_GLContext* context);
void SDL_GL_SwapWindow(SDL_Window* window);

void SDL_GL_ResetAttributes(void);
int SDL_GL_SetAttribute(SDL_GLattr attr, int value);
int SDL_GL_GetAttribute(SDL_GLattr attr, int* value);
int SDL_GL_SetSwapInterval(int interval);
int SDL_GL_GetSwapInterval(void);
SDL_bool SDL_GL_ExtensionSupported(const char* extension);
]])
-------------------------------------------------------------------
--
-- TODO: binding locations are finite amount, handle intelligently
--
-------------------------------------------------------------------
local _gUniformBufferBindingIndex = 0

local GLContext = {}
local GLContextMT = {__index = GLContext}

local glContexts = {
  -- lenovo win10 laptop
  {4, 6, sdl.GL_CONTEXT_PROFILE_CORE},
	-- home win10 desktop and nouveau on linux is 4.5
	{4, 5, sdl.GL_CONTEXT_PROFILE_CORE},
  -- compute shader added in 4.3
  {4, 3, sdl.GL_CONTEXT_PROFILE_CORE},
  -- macOS is at 4.1
  {4, 1, sdl.GL_CONTEXT_PROFILE_CORE},
	-- min required 3.3
	{3, 3, sdl.GL_CONTEXT_PROFILE_CORE},
}
--- Setup global context attributes once to use for creating contexts
-- @param config agen config table
function GLContext.setAttributes(config)
  assert(config, "no configuration provided")

  sdl.SDL_GL_ResetAttributes()

  sdl.SDL_GL_SetAttribute(sdl.GL_BUFFER_SIZE, config.color_buffer)
  if config.color_buffer == 32 then
    sdl.SDL_GL_SetAttribute(sdl.GL_RED_SIZE,   8)
    sdl.SDL_GL_SetAttribute(sdl.GL_GREEN_SIZE, 8)
    sdl.SDL_GL_SetAttribute(sdl.GL_BLUE_SIZE,  8)
    sdl.SDL_GL_SetAttribute(sdl.GL_ALPHA_SIZE, 8)
  elseif config.color_buffer == 24 then
    sdl.SDL_GL_SetAttribute(sdl.GL_RED_SIZE,   8)
    sdl.SDL_GL_SetAttribute(sdl.GL_GREEN_SIZE, 8)
    sdl.SDL_GL_SetAttribute(sdl.GL_BLUE_SIZE,  8)
    sdl.SDL_GL_SetAttribute(sdl.GL_ALPHA_SIZE, 0)
  elseif config.color_buffer == 16 then
    sdl.SDL_GL_SetAttribute(sdl.GL_RED_SIZE,   5)
    sdl.SDL_GL_SetAttribute(sdl.GL_GREEN_SIZE, 5)
    sdl.SDL_GL_SetAttribute(sdl.GL_BLUE_SIZE,  5)
    sdl.SDL_GL_SetAttribute(sdl.GL_ALPHA_SIZE, 1)
  elseif config.color_buffer == 15 then
    sdl.SDL_GL_SetAttribute(sdl.GL_RED_SIZE,   5)
    sdl.SDL_GL_SetAttribute(sdl.GL_GREEN_SIZE, 5)
    sdl.SDL_GL_SetAttribute(sdl.GL_BLUE_SIZE,  5)
    sdl.SDL_GL_SetAttribute(sdl.GL_ALPHA_SIZE, 0)
  end

  if config.depth_buffer and config.depth_buffer > 0 then
    sdl.SDL_GL_SetAttribute(sdl.GL_DEPTH_SIZE, config.depth_buffer) 
    sdl.SDL_GL_SetAttribute(sdl.GL_STENCIL_SIZE, 8)
  else
    sdl.SDL_GL_SetAttribute(sdl.GL_DEPTH_SIZE, 0)
    sdl.SDL_GL_SetAttribute(sdl.GL_STENCIL_SIZE, 0)
  end
  sdl.SDL_GL_SetAttribute(sdl.GL_DOUBLEBUFFER, 1)
  sdl.SDL_GL_SetAttribute(sdl.GL_ACCELERATED_VISUAL, 1)
  sdl.SDL_GL_SetAttribute(sdl.GL_RETAINED_BACKING, 0)
  sdl.SDL_GL_SetAttribute(sdl.GL_SHARE_WITH_CURRENT_CONTEXT, 1)

  if config.samples and config.samples > 0 then
    sdl.SDL_GL_SetAttribute(sdl.GL_MULTISAMPLEBUFFERS, 1)
    sdl.SDL_GL_SetAttribute(sdl.GL_MULTISAMPLESAMPLES, config.samples)
  else
    sdl.SDL_GL_SetAttribute(sdl.GL_MULTISAMPLEBUFFERS, 0)
  end
  --
  -- TODO: if ARB_create_context_robustness
  --
  if config.debug then
    sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_FLAGS, 
      bit.bor(sdl.GL_CONTEXT_FORWARD_COMPATIBLE_FLAG, sdl.GL_CONTEXT_ROBUST_ACCESS_FLAG, sdl.GL_CONTEXT_DEBUG_FLAG))
  else
    sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_FLAGS, 
      bit.bor(sdl.GL_CONTEXT_FORWARD_COMPATIBLE_FLAG, sdl.GL_CONTEXT_ROBUST_ACCESS_FLAG))
  end
  --
  -- a graphics reset will result in the loss of all context state, requiring the recreation of
  -- all associated objects.
  --
  sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_RESET_NOTIFICATION, sdl.GL_CONTEXT_RESET_LOSE_CONTEXT)
  --
  -- no implicit flush on context switch
  --
  sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_RELEASE_BEHAVIOR, sdl.GL_CONTEXT_RELEASE_BEHAVIOR_NONE)
end
--- Create openGL context with window 
-- @param SDL window
-- @return lua-managed SDL_GLContext
function GLContext.create(win)
  local self = nil
	--
	-- GL attribute version not set yet (first call)
	--
	for _, v in pairs(glContexts) do
		sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_MAJOR_VERSION, v[1])
		sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_MINOR_VERSION, v[2])
		sdl.SDL_GL_SetAttribute(sdl.GL_CONTEXT_PROFILE_MASK,  v[3])

		self = sdl.SDL_GL_CreateContext(win)
		if self ~= nil then
			break
		else
			agen.log.debug('video', "GL context [%d.%d]: %s", v[1], v[2], sdl.GetError())
		end
	end

  if self == nil then
    error('No supported GL context found')
  end
  return ffi.gc(self, sdl.SDL_GL_DeleteContext)
end
--- make a context current for caller thread
function GLContext:makeCurrent(window)
  if sdl.SDL_GL_MakeCurrent(window, self) ~= 0 then
    error(string.format("failed to make context current: %s", sdl.GetError()))
  end  
end
--- get context that was made current for caller thread
function GLContext.getCurrent()
  local self = sdl.SDL_GL_GetCurrentContext()
  if self == nil then
    error(string.format("failed to get current context: %s", sdl.GetError()))
  end
  return self
end
--- query the current context version
function GLContext:getVersion()
  local maj = ffi.new('int[1]')
  local min = ffi.new('int[1]')

  if sdl.SDL_GL_GetAttribute(sdl.GL_CONTEXT_MAJOR_VERSION, maj) ~= 0 then
    agen.log.debug('video', "failed to get context attribute: %s", sdl.GetError())
    return 0.0
  end

  if sdl.SDL_GL_GetAttribute(sdl.GL_CONTEXT_MINOR_VERSION, min) ~= 0 then
    agen.log.debug('video', "failed to get context attribute: %s", sdl.GetError())
    return 0.0
  end

  return tonumber(maj[0]) + tonumber(min[0]) / 10
end

function GLContext:isDebug()
  local flags = ffi.new('int[1]')

  if sdl.SDL_GL_GetAttribute(sdl.GL_CONTEXT_FLAGS, flags) ~= 0 then
    agen.log.debug('video', "failed to get context attribute: %s", sdl.GetError())
    return false
  end

  return bit.band(flags[0], sdl.GL_CONTEXT_DEBUG_FLAG) ~= 0 and true or false
end

function GLContext:isSupported(ext)
  return sdl.SDL_GL_ExtensionSupported(ext) == sdl.TRUE
end

function GLContext:getBufferBindingIndex()
  local ret = _gUniformBufferBindingIndex
  _gUniformBufferBindingIndex = _gUniformBufferBindingIndex + 1
  return _gUniformBufferBindingIndex
end

return ffi.metatype('SDL_GLContext', GLContextMT)
