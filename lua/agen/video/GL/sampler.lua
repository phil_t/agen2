local string = require('string')
local gl = require('agen.video.GL.lib')
local ffi = gl.ffi
-------------------------------------------------------------------------------
--
-- Core since 3.3
-- Extension GL_ARB_sampler_objects
--
-------------------------------------------------------------------------------
ffi.cdef([[
static const uint16_t TEXTURE_ENV_MODE = 0x2200;
static const uint16_t TEXTURE_ENV      = 0x2300;
/* TextureMagFilter */
static const uint16_t NEAREST = 0x2600;
static const uint16_t LINEAR  = 0x2601;
/* TextureMinFilter */
static const uint16_t NEAREST_MIPMAP_NEAREST = 0x2700;
static const uint16_t NEAREST_MIPMAP_LINEAR  = 0x2702;
static const uint16_t LINEAR_MIPMAP_NEAREST  = 0x2701;
static const uint16_t LINEAR_MIPMAP_LINEAR   = 0x2703;
/* TextureParameterName */
static const uint16_t TEXTURE_MAG_FILTER  = 0x2800;
static const uint16_t TEXTURE_MIN_FILTER  = 0x2801;
static const uint16_t TEXTURE_WRAP_S      = 0x2802;
static const uint16_t TEXTURE_WRAP_T      = 0x2803;
static const uint16_t TEXTURE_WRAP_R      = 0x8072;
static const uint16_t REPEAT              = 0x2901;
static const uint16_t CLAMP               = 0x2900;

static const uint16_t CLAMP_TO_EDGE        = 0x812F;
static const uint16_t TEXTURE_MIN_LOD      = 0x813A;
static const uint16_t TEXTURE_MAX_LOD      = 0x813B;
static const uint16_t TEXTURE_BASE_LEVEL   = 0x813C;
static const uint16_t TEXTURE_MAX_LEVEL    = 0x813D;
static const uint16_t MIRRORED_REPEAT      = 0x8370;
static const uint16_t MIRROR_CLAMP_TO_EDGE = 0x8743;
static const uint16_t MIRROR_CLAMP_TO_BORDER = 0x8912;

typedef void (*PFNGLGENSAMPLERSPROC)(GLsizei count, GLuint *samplers);
typedef void (*PFNGLDELETESAMPLERSPROC)(GLsizei count, const GLuint *samplers);
typedef GLboolean (*PFNGLISSAMPLERPROC)(GLuint sampler);
typedef void (*PFNGLBINDSAMPLERPROC)(GLuint unit, GLuint sampler);
typedef void (*PFNGLSAMPLERPARAMETERIPROC)(GLuint sampler, GLenum pname, GLint param);
typedef void (*PFNGLSAMPLERPARAMETERIVPROC)(GLuint sampler, GLenum pname, const GLint *param);
typedef void (*PFNGLSAMPLERPARAMETERFPROC)(GLuint sampler, GLenum pname, GLfloat param);
typedef void (*PFNGLSAMPLERPARAMETERFVPROC)(GLuint sampler, GLenum pname, const GLfloat *param);
typedef void (*PFNGLSAMPLERPARAMETERIIVPROC)(GLuint sampler, GLenum pname, const GLint *param);
typedef void (*PFNGLSAMPLERPARAMETERIUIVPROC)(GLuint sampler, GLenum pname, const GLuint *param);
typedef void (*PFNGLGETSAMPLERPARAMETERIVPROC)(GLuint sampler, GLenum pname, GLint *params);
typedef void (*PFNGLGETSAMPLERPARAMETERIIVPROC)(GLuint sampler, GLenum pname, GLint *params);
typedef void (*PFNGLGETSAMPLERPARAMETERFVPROC)(GLuint sampler, GLenum pname, GLfloat *params);
typedef void (*PFNGLGETSAMPLERPARAMETERIUIVPROC)(GLuint sampler, GLenum pname, GLuint *params);

typedef struct _glSampler_t {
  GLuint id[1];
} glSampler_t;
]])

local symtab = {
  GenSamplers       = 'PFNGLGENSAMPLERSPROC',
  DeleteSamplers      = 'PFNGLDELETESAMPLERSPROC',
  IsSampler       = 'PFNGLISSAMPLERPROC',
  BindSampler       = 'PFNGLBINDSAMPLERPROC',
  SamplerParameteri   = 'PFNGLSAMPLERPARAMETERIPROC',
  SamplerParameteriv    = 'PFNGLSAMPLERPARAMETERIVPROC',
  SamplerParameterf   = 'PFNGLSAMPLERPARAMETERFPROC',
  SamplerParameterfv    = 'PFNGLSAMPLERPARAMETERFVPROC',
  GetSamplerParameteriv = 'PFNGLGETSAMPLERPARAMETERIVPROC',
  GetSamplerParameterfv = 'PFNGLGETSAMPLERPARAMETERFVPROC',
}

local ISamplerGL = {}
local ISamplerGLMT = {
  __index = ISamplerGL,
  __gc = function(self) gl.DeleteSamplers(1, self.id) end,
  __eq = function(self, other)
    if other == nil or not ffi.istype('glSampler_t', other) then
      return false
    end
    return self.id[0] == other.id[0]
  end,
  __tostring = function(self) return string.format("sampler<%d>", self.id[0]) end,
}

function ISamplerGL.setup(ctx)
  local ver = ctx.getVersion()

  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    gl.import(ISamplerGL, 'sampler', 'ARB_direct_state_access')
  elseif ver >= 3.3 or ctx:isSupported("GL_ARB_sampler_objects") then
     agen.log.info('video', "sampler: using ARB_sampler_objects")
  else
    error(string.format("Unsupported GL version [%f]", ver))
  end

  for k, v in pairs(symtab) do
    gl[k] = v
  end
end

local samplerValues = {
  [tonumber(gl.TEXTURE_MIN_FILTER)] = gl.LINEAR,
  [tonumber(gl.TEXTURE_MAG_FILTER)] = gl.LINEAR,
  [tonumber(gl.TEXTURE_WRAP_S)]     = gl.REPEAT,
  [tonumber(gl.TEXTURE_WRAP_T)]     = gl.REPEAT,
  [tonumber(gl.TEXTURE_WRAP_R)]     = gl.REPEAT,
  [tonumber(gl.TEXTURE_MAX_ANISOTROPY_EXT)] = 1.0,
  [tonumber(gl.TEXTURE_LOD_BIAS)]   = 0,
--  [tonumber(gl.TEXTURE_BORDER_COLOR)] = 0x00000000,
  [tonumber(gl.TEXTURE_MIN_LOD)]    = -1000,
  [tonumber(gl.TEXTURE_MAX_LOD)]    = 1000,
}

local optionToValue = {
  POINT  = gl.NEAREST,
  LINEAR = gl.LINEAR,
}

local optionToAddress = {
  wrap   = gl.REPEAT,
  mirror = gl.MIRRORED_REPEAT,
  clamp  = gl.CLAMP,
  border = gl.MIRROR_CLAMP_TO_BORDER,
  mirrorOnce = gl.MIRROR_CLAMP_TO_EDGE,
}

function ISamplerGL.new(options)
  local self = ffi.new('glSampler_t')
  self:_alloc()
  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("create sampler failed [%s]", msg))
  end
  return self:init(options)
end

function ISamplerGL:_alloc()
  gl.GenSamplers(1, self.id)
  return self
end

function ISamplerGL:init(options)
  --
  -- TODO: from options
  --[[
  for k, v in pairs(samplerValues) do
    if ffi.istype('GLint[4]', v) then
      gl.SamplerParameteriv(self:getID(), k, v)
    else
      gl.SamplerParameteri(self:getID(), k, v)
    end
  end
  ]]
  agen.log.warn('application', "TODO: GL sampler init values not implemented")
  return self
end

function ISamplerGL:getID()
  return self.id[0]
end

function ISamplerGL:setActive(texUnitID)
  gl.BindSampler(texUnitID, self:getID())
end

return ffi.metatype('glSampler_t', ISamplerGLMT)
