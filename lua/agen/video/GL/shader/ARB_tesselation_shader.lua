local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t TESS_EVALUATION_SHADER         = 0x8E87;
static const uint16_t TESS_CONTROL_SHADER            = 0x8E88;
static const uint16_t PATCH_VERTICES                 = 0x8E72;
static const uint16_t PATCH_DEFAULT_INNER_LEVEL      = 0x8E73;
static const uint16_t PATCH_DEFAULT_OUTER_LEVEL      = 0x8E74;
static const uint16_t TESS_CONTROL_OUTPUT_VERTICES   = 0x8E75;
static const uint16_t TESS_GEN_MODE                  = 0x8E76;
static const uint16_t TESS_GEN_SPACING               = 0x8E77;
static const uint16_t TESS_GEN_VERTEX_ORDER          = 0x8E78;
static const uint16_t TESS_GEN_POINT_MODE            = 0x8E79;
static const uint16_t ISOLINES                       = 0x8E7A;
static const uint16_t FRACTIONAL_ODD                 = 0x8E7B;
static const uint16_t FRACTIONAL_EVEN                = 0x8E7C;
static const uint16_t MAX_PATCH_VERTICES             = 0x8E7D;
static const uint16_t MAX_TESS_GEN_LEVEL             = 0x8E7E;
static const uint16_t MAX_TESS_CONTROL_UNIFORM_COMPONENTS     = 0x8E7F;
static const uint16_t MAX_TESS_EVALUATION_UNIFORM_COMPONENTS  = 0x8E80;
static const uint16_t MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS    = 0x8E81;
static const uint16_t MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
static const uint16_t MAX_TESS_CONTROL_OUTPUT_COMPONENTS      = 0x8E83;
static const uint16_t MAX_TESS_PATCH_COMPONENTS               = 0x8E84;
static const uint16_t MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
static const uint16_t MAX_TESS_EVALUATION_OUTPUT_COMPONENTS   = 0x8E86;
static const uint16_t MAX_TESS_CONTROL_UNIFORM_BLOCKS         = 0x8E89;
static const uint16_t MAX_TESS_EVALUATION_UNIFORM_BLOCKS      = 0x8E8A;
static const uint16_t MAX_TESS_CONTROL_INPUT_COMPONENTS       = 0x886C;
static const uint16_t MAX_TESS_EVALUATION_INPUT_COMPONENTS    = 0x886D;
]])

return {}
