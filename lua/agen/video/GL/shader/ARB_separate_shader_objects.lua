-----------------------------------------------------------------------------------
--
-- Core 4.1 or ARB_separate_shader_objects
--
-----------------------------------------------------------------------------------
local ffi = require('ffi')
ffi.cdef([[
// Accepted by <stages> parameter to UseProgramStages:
static const uint32_t VERTEX_SHADER_BIT          = 0x00000001;
static const uint32_t FRAGMENT_SHADER_BIT        = 0x00000002;
static const uint32_t GEOMETRY_SHADER_BIT        = 0x00000004;
static const uint32_t TESS_CONTROL_SHADER_BIT    = 0x00000008;
static const uint32_t TESS_EVALUATION_SHADER_BIT = 0x00000010;
static const uint32_t ALL_SHADER_BITS            = 0xFFFFFFFF;

static const uint16_t PROGRAM_SEPARABLE        = 0x8258;
static const uint16_t ACTIVE_PROGRAM           = 0x8259;
static const uint16_t PROGRAM_PIPELINE_BINDING = 0x825A;

typedef void (*PFNGLPROGRAMPARAMETERIPROC)(GLuint program, GLenum pname, GLint value);
typedef void (*PFNGLUSEPROGRAMSTAGESPROC)(GLuint pipeline, GLbitfield stages, GLuint program);
typedef void (*PFNGLACTIVESHADERPROGRAMPROC)(GLuint pipeline, GLuint program);
typedef GLuint (*PFNGLCREATESHADERPROGRAMVPROC)(GLenum type, GLsizei count, const GLchar *const *strings);
typedef void (*PFNGLBINDPROGRAMPIPELINEPROC)(GLuint pipeline);
typedef void (*PFNGLDELETEPROGRAMPIPELINESPROC)(GLsizei n, const GLuint *pipelines);
typedef void (*PFNGLGENPROGRAMPIPELINESPROC)(GLsizei n, GLuint *pipelines);
typedef GLboolean (*PFNGLISPROGRAMPIPELINEPROC)(GLuint pipeline);
typedef void (*PFNGLGETPROGRAMPIPELINEIVPROC)(GLuint pipeline, GLenum pname, GLint *params);
]])

local ASSO = {}

local symtab = {
  ProgramParameteri = 'PFNGLPROGRAMPARAMETERIPROC',
  UseProgramStages  = 'PFNGLUSEPROGRAMSTAGESPROC',
  ActiveShaderProgram = 'PFNGLCREATESHADERPROGRAMVPROC',
  BindProgramPipeline = 'PFNGLBINDPROGRAMPIPELINEPROC',
  GenProgramPipelines = 'PFNGLGENPROGRAMPIPELINESPROC',
  IsProgramPipeline   = 'PFNGLISPROGRAMPIPELINEPROC',
  GetProgramPipelineiv  = 'PFNGLGETPROGRAMPIPELINEIVPROC',
}

return ASSO
