local gl = require('agen.video.GL.lib')
local ffi = gl.ffi
---------------------------------------------------------------------------------------
--
-- In GL unlike DX we don't want to compile the shaders at build time. Due to the lack
-- of common IR each GL driver implements their own shader compiler that has to be used
-- at run-time. The purpose of this extension is to cache the shader binary created the
-- first time the application ran.
--
---------------------------------------------------------------------------------------
ffi.cdef([[
typedef void (*PFNGLGETPROGRAMBINARYPROC)(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary);
typedef void (*PFNGLPROGRAMBINARYPROC)(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length);
typedef void (*PFNGLPROGRAMPARAMETERIPROC)(GLuint program, GLenum pname, GLint value);
]])

local ShaderBinary = {}

local symtab = {
  GetProgramBinary  = 'PFNGLGETPROGRAMBINARYPROC',
  ProgramBinary     = 'PFNGLPROGRAMBINARYPROC',
  ProgramParameteri = 'PFNGLPROGRAMPARAMETERIPROC',
}

function ShaderBinary:included(klass)
  agen.log.debug('video', "shader: using ARB_get_program_binary")
  for k, v in pairs(symtab) do
    klass.static.symtab[k] = v
  end
end

function ShaderBinary:getShaderBinary()
  error('not implemented')
end

function ShaderBinary:setShaderBinary(blob)
  error('not implemented')
end

return ShaderBinary
