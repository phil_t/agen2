local string = require('string')
local class  = require('middleclass')
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t FRAGMENT_SHADER = 0x8B30;
static const uint16_t VERTEX_SHADER   = 0x8B31;
static const uint16_t GEOMETRY_SHADER = 0x8DD9;

static const uint16_t DELETE_STATUS       = 0x8B80;
static const uint16_t COMPILE_STATUS      = 0x8B81;
static const uint16_t LINK_STATUS         = 0x8B82;
static const uint16_t VALIDATE_STATUS     = 0x8B83;
static const uint16_t INFO_LOG_LENGTH     = 0x8B84;
static const uint16_t ATTACHED_SHADERS    = 0x8B85;
static const uint16_t ACTIVE_UNIFORMS     = 0x8B86;
static const uint16_t ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
static const uint16_t SHADER_SOURCE_LENGTH      = 0x8B88;
static const uint16_t ACTIVE_ATTRIBUTES         = 0x8B89;
static const uint16_t ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A;
static const uint16_t FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
static const uint16_t SHADING_LANGUAGE_VERSION  = 0x8B8C;
static const uint16_t CURRENT_PROGRAM           = 0x8B8D;

typedef struct _gl_program_t {
  GLuint id;
} gl_program_t;

typedef void (*PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (*PFNGLBINDATTRIBLOCATIONPROC) (GLuint program, GLuint index, const GLchar *name);
typedef void (*PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (*PFNGLCREATEPROGRAMPROC) (void);
typedef GLuint (*PFNGLCREATESHADERPROC) (GLenum type);
typedef void (*PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (*PFNGLDELETESHADERPROC) (GLuint shader);
typedef void (*PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (*PFNGLDISABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (*PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (*PFNGLGETACTIVEATTRIBPROC) (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (*PFNGLGETACTIVEUNIFORMPROC) (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (*PFNGLGETATTACHEDSHADERSPROC) (GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders);
typedef GLint (*PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (*PFNGLGETPROGRAMIVPROC) (GLuint program, GLenum pname, GLint *params);
typedef void (*PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (*PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint *params);
typedef void (*PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (*PFNGLGETSHADERSOURCEPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source);
typedef GLint (*PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (*PFNGLGETUNIFORMFVPROC) (GLuint program, GLint location, GLfloat *params);
typedef void (*PFNGLGETUNIFORMIVPROC) (GLuint program, GLint location, GLint *params);
typedef void (*PFNGLGETVERTEXATTRIBDVPROC) (GLuint index, GLenum pname, GLdouble *params);
typedef void (*PFNGLGETVERTEXATTRIBFVPROC) (GLuint index, GLenum pname, GLfloat *params);
typedef void (*PFNGLGETVERTEXATTRIBIVPROC) (GLuint index, GLenum pname, GLint *params);
typedef void (*PFNGLGETVERTEXATTRIBPOINTERVPROC) (GLuint index, GLenum pname, void **pointer);
typedef GLboolean (*PFNGLISPROGRAMPROC) (GLuint program);
typedef GLboolean (*PFNGLISSHADERPROC) (GLuint shader);
typedef void (*PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (*PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (*PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (*PFNGLUNIFORM1FPROC) (GLint location, GLfloat v0);
typedef void (*PFNGLUNIFORM2FPROC) (GLint location, GLfloat v0, GLfloat v1);
typedef void (*PFNGLUNIFORM3FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef void (*PFNGLUNIFORM4FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
typedef void (*PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
typedef void (*PFNGLUNIFORM2IPROC) (GLint location, GLint v0, GLint v1);
typedef void (*PFNGLUNIFORM3IPROC) (GLint location, GLint v0, GLint v1, GLint v2);
typedef void (*PFNGLUNIFORM4IPROC) (GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
typedef void (*PFNGLUNIFORM1FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (*PFNGLUNIFORM2FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (*PFNGLUNIFORM3FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (*PFNGLUNIFORM4FVPROC) (GLint location, GLsizei count, const GLfloat *value);
typedef void (*PFNGLUNIFORM1IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (*PFNGLUNIFORM2IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (*PFNGLUNIFORM3IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (*PFNGLUNIFORM4IVPROC) (GLint location, GLsizei count, const GLint *value);
typedef void (*PFNGLUNIFORMMATRIX2FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (*PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (*PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

typedef void (*PFNGLBINDFRAGDATALOCATIONPROC) (GLuint program, GLuint color, const GLchar *name);
typedef GLint (*PFNGLGETFRAGDATALOCATIONPROC) (GLuint program, const GLchar *name);

typedef void (*PFNGLVERTEXATTRIB4FVPROC) (GLuint index, const GLfloat *v);
typedef GLboolean (*PFNGLVALIDATEPROGRAMPROC)(GLuint program);
//
// uniform buffers
//
typedef void (*PFNGLGETACTIVEUNIFORMNAMEPROC)(GLuint program, GLuint uniformIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformName);
typedef GLuint (*PFNGLGETUNIFORMBLOCKINDEXPROC)(GLuint program, const GLchar *uniformBlockName);
typedef void (*PFNGLGETACTIVEUNIFORMBLOCKIVPROC)(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params);
typedef void (*PFNGLUNIFORMBLOCKBINDINGPROC)(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding);

static const GLuint INVALID_INDEX = 0xFFFFFFFFu;
]])

local IProgramGL = class('IProgramGL')

IProgramGL.static.symtab = {
--  BindFragDataLocation = 'PFNGLBINDFRAGDATALOCATIONPROC',
--  VertexAttrib4fv = 'PFNGLVERTEXATTRIB4FVPROC',
	CreateShader = 'PFNGLCREATESHADERPROC',
	ShaderSource = 'PFNGLSHADERSOURCEPROC',
	CompileShader = 'PFNGLCOMPILESHADERPROC',
	CreateProgram = 'PFNGLCREATEPROGRAMPROC',
	AttachShader = 'PFNGLATTACHSHADERPROC',
	DeleteShader = 'PFNGLDELETESHADERPROC',
--	BindAttribLocation = 'PFNGLBINDATTRIBLOCATIONPROC',
	LinkProgram = 'PFNGLLINKPROGRAMPROC',
	UseProgram = 'PFNGLUSEPROGRAMPROC',
	DeleteProgram = 'PFNGLDELETEPROGRAMPROC',
	GetShaderiv = 'PFNGLGETSHADERIVPROC',
	GetShaderInfoLog = 'PFNGLGETSHADERINFOLOGPROC',
	Uniform1i = 'PFNGLUNIFORM1IPROC',
--	Uniform4fv = 'PFNGLUNIFORM4FVPROC',
	GetUniformLocation = 'PFNGLGETUNIFORMLOCATIONPROC',
--	UniformMatrix4fv = 'PFNGLUNIFORMMATRIX4FVPROC',
--	GetAttribLocation = 'PFNGLGETATTRIBLOCATIONPROC',
	GetProgramiv = 'PFNGLGETPROGRAMIVPROC',
	GetProgramInfoLog = 'PFNGLGETPROGRAMINFOLOGPROC',
	IsProgram = 'PFNGLISPROGRAMPROC',
	ValidateProgram = 'PFNGLVALIDATEPROGRAMPROC',
  -- core since 3.1 or GL_ARB_uniform_buffer_object
  GetActiveUniformName = 'PFNGLGETACTIVEUNIFORMNAMEPROC',
  GetActiveUniformBlockiv = 'PFNGLGETACTIVEUNIFORMBLOCKIVPROC',
  GetUniformBlockIndex = 'PFNGLGETUNIFORMBLOCKINDEXPROC',
  UniformBlockBinding = 'PFNGLUNIFORMBLOCKBINDINGPROC',
}

function IProgramGL:__tostring()
  return string.format("program <%d>", self.program.id)
end

function IProgramGL:__eq(other)
  if other == nil then
    return false
  end
  return self.program.id == other.program.id
end

function IProgramGL.static:setup(ctx)
  if ctx:isSupported('GL_ARB_get_program_binary') then
    local programBinary = require('agen.video.GL.shader.ARB_get_program_binary')
    IProgramGL:include(programBinary)
  end

  for k, v in pairs(IProgramGL.symtab) do
    gl[k] = v
  end
end

function IProgramGL:initialize(shaderDesc)
  local prog = ffi.new('gl_program_t')
  prog.id = gl.CreateProgram()

  self.program = ffi.gc(prog, function(p)
    gl.DeleteProgram(p.id)
  end)
  self.samplerSlots = {}
  self.uniformsCache = {}

  self:setShaders(shaderDesc)
end

function IProgramGL:setActive()
  gl.UseProgram(self.program.id)
end

function IProgramGL:setShaders(shaders)
  --
  -- required shaders
  --
  self:addVertexShader(shaders.vertex)
  self:addPixelShader(shaders.pixel)
  --
  -- optional shaders
  --
  self:addGeometryShader(shaders.geometry)
  --
  -- link into one program
  --
  self:_linkShader()
  --
  -- required to access shader vars
  --
  self:setActive()
end

function IProgramGL:addVertexShader(code)
  local shader = self:_compileShader(gl.VERTEX_SHADER, code) 
  gl.AttachShader(self.program.id, shader)
  gl.DeleteShader(shader)
	self.uniformsCache = {}
end

function IProgramGL:addPixelShader(code)
  local shader = self:_compileShader(gl.FRAGMENT_SHADER, code) 
  gl.AttachShader(self.program.id, shader)
  gl.DeleteShader(shader)
	self.uniformsCache = {}
end
-- optional
function IProgramGL:addGeometryShader(code)
  if code == nil then
    return
  end
  local shader = self:_compileShader(gl.GEOMETRY_SHADER, code) 
  gl.AttachShader(self.program.id, shader)
  gl.DeleteShader(shader)
end

function IProgramGL:_compileShader(kind, code)
  assert(code, "Shader code cannot be nil")
	local shader = gl.CreateShader(kind)
  local text = ffi.new('GLchar[?]', #code + 1, code)
  local text_ptr = ffi.new('const GLchar*[1]', text)
  local len  = ffi.new('GLint[1]', #code)

	gl.ShaderSource(shader, 1, text_ptr, len)
	gl.CompileShader(shader)
    
	local status = ffi.new('GLint[1]', 0)
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, status)

	if status[0] ~= gl.TRUE then
    local err = 'Unknown'
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, status)
		if status[0] > 0 then
			local log = ffi.new('GLchar[?]', status[0] + 1)
			gl.GetShaderInfoLog(shader, status[0], status, log)
      err = ffi.string(log, status[0])
    end
    gl.DeleteShader(shader)
		error(err)
  end

	return shader
end

function IProgramGL:_linkShader()
  gl.LinkProgram(self.program.id)
  --
  -- We must check and make sure that it linked. If it fails, it would 
  -- mean either there is a mismatch between the vertex and fragment 
  -- shaders. It might be that you have surpassed your GPU's abilities. 
  -- Perhaps too many ALU operations or too many texel fetch instructions 
  -- or too many interpolators or dynamic loops.
  --
	local int = ffi.new('GLint[1]')

  gl.GetProgramiv(self.program.id, gl.LINK_STATUS, int)
  if int[0] ~= gl.TRUE then
		-- 
    -- Noticed that glGetProgramiv is used to get the length for 
    -- a shader program, not glGetShaderiv.
    --
		gl.GetProgramiv(self.program.id, gl.INFO_LOG_LENGTH, int)
    --
		-- The maxLength includes the NULL character
    --
		local progInfoLog = ffi.new('char[?]', int[0])
 
		gl.GetProgramInfoLog(self.program.id, int[0], int, progInfoLog)
		error(ffi.string(progInfoLog, int[0]))
  else
    agen.log.debug('video', "Program linked")
  end
end

function IProgramGL:getUniform(name)
	local ret = self.uniformsCache[name]
	if ret == nil then
		ret = gl.GetUniformLocation(self.program.id, name)
    if ret < 0 then
		  local err, msg = gl.GetError()
		  if err ~= 0 then
			  error(string.format("Invalid uniform name [%s]: [%s]", name, msg))
		  end
    end
		self.uniformsCache[name] = ret
	end
  return ret
end
--- Get index for sampler uniform location in the program. Texture unit (index) is assigned to it (with texture and sampler bound to the unit)
-- @param name sampler unifrom name
-- @return sampler index
function IProgramGL:getSamplerIndex(name)
  if self.samplerSlots[name] == nil then
    local loc = self:getUniform(name)
    local index = #self.samplerSlots
    gl.Uniform1i(loc, index)
    agen.log.verbose('video', "Shader var [%s] at index [%d] = [%d]", name, loc, index)
    self.samplerSlots[name] = index
  end
  return self.samplerSlots[name]
end

--- Get uniform block index from shader.
function IProgramGL:getUniformBlockIndex(name)
  assert(name and name ~= "", "Invalid argument #1, name not set")
  local blockIndex = gl.GetUniformBlockIndex(self.program.id, name)
  -- uniform buffer found in shader?
  if blockIndex == gl.INVALID_INDEX then
    error(string.format("program [%s]: uniform buffer [%s] not found. Note that it may have been optimized away if not referenced in the shader code.", tostring(self), name))
  end
  return blockIndex
end

function IProgramGL:setBindingIndex(blockIndex, bindingIndex)
  gl.UniformBlockBinding(self.program.id, blockIndex, bindingIndex)
end

return IProgramGL
