local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef GLvoid (*PFNGLCREATESAMPLERSPROC)(GLsizei n, GLuint *samplers);
]])

local DSA = {}

DSA.symtab = {
    CreateSamplers = 'PFNGLCREATESAMPLERSPROC',
}

function DSA:_alloc()
    gl.CreateSamplers(1, self.id)
    return self
end

function DSA:setActive(texUnitID)
    -- no op
end

return DSA
