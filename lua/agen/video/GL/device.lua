local string  = require('string')
local sdl     = require('sdl2.lib')
local gl      = require('agen.video.GL.lib')
local debug   = require('agen.video.GL.debug')
local ctx     = require('agen.video.GL.context')
local buffer  = require('agen.video.GL.buffer')
local texture = require('agen.video.GL.texture')
local sampler = require('agen.video.GL.sampler')
local renderTarget = require('agen.video.GL.renderTarget')
local shader  = require('agen.video.GL.shader')
local renderState  = require('agen.video.GL.renderState')
local fence   = require('agen.video.GL.fence')
local ffi     = gl.ffi

ffi.cdef([[
typedef const GLubyte* (*PFNGLGETSTRINGPROC)(GLenum name);
typedef void (*PFNGLENABLEPROC)(GLenum mode);
typedef void (*PFNGLDISABLEPROC)(GLenum mode);
typedef void (*PFNGLHINTPROC)(GLenum target, GLenum mode);
typedef void (*PFNGLENABLECLIENTSTATEPROC)(GLenum target);
typedef void (*PFNGLDISABLECLIENTSTATEPROC)(GLenum target);
typedef void (*PFNGLSAMPLECOVERAGE)(GLclampf value, GLboolean invert);

typedef struct _GLDevice {
  SDL_Window* window;
} GLDevice;
]])

local symtab = {
  GetString      = 'PFNGLGETSTRINGPROC',
  Enable         = 'PFNGLENABLEPROC',
  Disable        = 'PFNGLDISABLEPROC',
  Hint           = 'PFNGLHINTPROC',
  EnableClientState  = 'PFNGLENABLECLIENTSTATEPROC',
  DisableClientState = 'PFNGLDISABLECLIENTSTATEPROC',
--  SampleCoverage = 'PFNGLSAMPLECOVERAGE',
}

local GLDevice = {}
local _context = nil
local _defaultRenderTarget = nil
--- Create video device
-- @param window SDL_Window
-- @config configuration table
-- @return device object
function GLDevice.create(window, config)
  ctx.setAttributes(config)
  _context = ctx.create(window)

  local self = ffi.new('GLDevice')
  -- store ref to avoid creating new cdata object per frame.
  self.window = window

  _context:makeCurrent(window)
  --
  -- GL context is now valid
  --
  for k, v in pairs(symtab) do
    gl[k] = v
  end

  agen.log.info('video', "GL_VENDOR:%s", gl.GetString(gl.VENDOR))
  agen.log.info('video', "GL_VERSION:%s", gl.GetString(gl.VERSION))
  agen.log.info('video', "GL_SHADING_LANGUAGE_VERSION:%s", gl.GetString(gl.SHADING_LANGUAGE_VERSION))
	--
	-- set vsync
	--
  if sdl.SDL_GL_SetSwapInterval(config.vsync) < 0 then
		agen.log.warn('video', "SetSwapInterval not supported: %s", sdl.GetError())
  end
  agen.log.verbose('video', "Swap interval = %d", tonumber(sdl.SDL_GL_GetSwapInterval()))

  agen.log.info('video', "OpenGL [%.1f] context created", _context:getVersion())
  --
  -- init extensions
  --
  if _context:isDebug() then debug.setup(_context) end
  buffer.setup(_context)
  renderState:setup(_context)
  sampler.setup(_context)
  texture.setup(_context)
  renderTarget:setup(_context)
  shader:setup(_context)
  fence.setup(_context)

  gl.GetError()
  return self
end

function GLDevice:init(config)
  --
  -- FIXME: mutisampling in RenderState
  --
  local attr = ffi.new('int[1]')
  if sdl.SDL_GL_GetAttribute(sdl.GL_MULTISAMPLEBUFFERS, attr) == 0 and attr[0] > 0 then
    agen.log.info('application', 'multisampled buffers enabled')
    --
    -- TODO: check for GL_ARB_multisample
    --
    gl.Enable(gl.MULTISAMPLE)
  else
    gl.Disable(gl.MULTISAMPLE)
  end

  --gl.Hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST)
  local w, h = self:_get_drawable_size()
  _defaultRenderTarget = renderTarget:createDefault(w, h)
end

function GLDevice:_get_drawable_size()
  local width  = ffi.new('int[1]')
  local height = ffi.new('int[1]')
  sdl.SDL_GL_GetDrawableSize(self.window, width, height)
  return width[0], height[0]
end

function GLDevice:getDefaultRenderTarget()
  return _defaultRenderTarget
end

function GLDevice:createRenderState(options)
  return renderState:new(self, options)
end

function GLDevice:createBuffer(options)
  return buffer.new(self, options)
end

function GLDevice:present()
  sdl.SDL_GL_SwapWindow(self.window)
end

function GLDevice:backBufferScale(window)
  local w, h = self:_get_drawable_size()
  agen.log.info('video', "scale: drawable resized to [%dx%d]", w, h)
  --
  -- map drawable size to view
  --
  _defaultRenderTarget:setViewVolume(0, 0, 0, w, h, 0, 1)
end

function GLDevice:backBufferResize(window)
  local w, h = self:_get_drawable_size()
  agen.log.info('video', "resize not implemented: drawable resized to [%dx%d]", w, h)
  -- self:init()
end

function GLDevice:getName()
  return string.format("opengl%d", _context.getVersion() * 10)
end
--- Create new empty texture with specified properties
-- @return new texture object
function GLDevice:createTexture(textureDesc)
  --
  -- create new texture
  --
  return texture.new(textureDesc)
end

function GLDevice:getBestTextureFormat(sdl_format)
  -- with swizzle we can read any texture layout
  return sdl_format
end

function GLDevice:createSampler(options)
  return sampler.new(options)
end

function GLDevice:createShader(options)
  return shader:new(options)
end
--- Use on the main thread to create a new context and pass it to other threads.
-- @return GL context sharing resources with current context
function GLDevice:createDeferredContext()
	return ctx.create(self.window)
end

function GLDevice:createRenderTarget(renderTargetDesc)
  return renderTarget:createWithDesc(renderTargetDesc)
end

function GLDevice:finalize()
  if _context:isDebug() then
    debug.quit()
  end
  _defaultRenderTarget = nil
  _context = nil
end

local GLDeviceMT = {__index = GLDevice}
return ffi.metatype('GLDevice', GLDeviceMT)
