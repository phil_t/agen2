local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef GLvoid (*PFNGLCREATEFRAMEBUFFERSPROC)(GLsizei n, GLuint *framebuffers);
typedef GLvoid (*PFNGLNAMEDFRAMEBUFFERTEXTUREPROC)(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level);
typedef GLenum (*PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC)(GLuint framebuffer, GLenum target);
typedef GLvoid (*PFNGLCLEARNAMEDFRAMEBUFFERFVPROC)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLfloat *value);
typedef GLvoid (*PFNCLEARNAMEDFRAMEBUFFERFIPROC)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil);
]])

local DSA = {}

local symtab = {
  CreateFramebuffers = 'PFNGLCREATEFRAMEBUFFERSPROC',
  NamedFramebufferTexture = 'PFNGLNAMEDFRAMEBUFFERTEXTUREPROC',
  CheckNamedFramebufferStatus = 'PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC',
  ClearNamedFramebufferfv = 'PFNGLCLEARNAMEDFRAMEBUFFERFVPROC',
  ClearNamedFramebufferfi = 'PFNCLEARNAMEDFRAMEBUFFERFIPROC',
}

function DSA:included(klass)
  agen.log.info('application', 'renderTarget: using ARB_direct_state_access')
  for k, v in pairs(symtab) do
    klass.symtab[k] = v
  end
end

function DSA:newFramebuffers(num)
  local fbo = ffi.new('GLuint[?]', num)
  gl.CreateFramebuffers(num, fbo)
  return fbo
end

function DSA:_checkStatus()
  return gl.CheckNamedFramebufferStatus(self:getID(), gl.FRAMEBUFFER)
end

function DSA:framebufferTexture(attachment, id)
  gl.NamedFramebufferTexture(self:getID(), attachment, id, 0)
end

function DSA:clearFV(enum, index, attachment)
  gl.ClearNamedFramebufferfv(self:getID(), enum, index, attachment)
end

function DSA:clearFI(enum, index, attachment, val)
  gl.ClearNamedFramebufferfi(self:getID(), enum, index, attachment, val)
end
--[[
function DSA:setActive()
  gl.ViewportArrayv(0, 8, self.viewports)
  --gl.ScissorArrayv(0, self._MAX_VIEWPORTS, self.scissors)
end
]]
return DSA
