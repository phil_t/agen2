-------------------------------------------------------------------
--
-- RenderTarget - color buffer or a texture that can be rendered to
--
-------------------------------------------------------------------
local string = require('string')
local math   = require('math')
local class  = require('middleclass')
local bit = require('bit')
local gl = require('agen.video.GL.lib')
local GLTexture = require('agen.video.GL.texture')
local ffi = gl.ffi
--------------------------------------------------------------------------
--
-- Core 4.1
--    or
-- GL_ARB_framebuffer_object + GL_ARB_draw_buffers + GL_ARB_viewport_array
--
--------------------------------------------------------------------------
ffi.cdef([[
static const uint16_t FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
static const uint16_t FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
static const uint16_t FRAMEBUFFER_ATTACHMENT_RED_SIZE     = 0x8212;
static const uint16_t FRAMEBUFFER_ATTACHMENT_GREEN_SIZE   = 0x8213;
static const uint16_t FRAMEBUFFER_ATTACHMENT_BLUE_SIZE    = 0x8214;
static const uint16_t FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE   = 0x8215;
static const uint16_t FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE   = 0x8216;
static const uint16_t FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
static const uint16_t FRAMEBUFFER_DEFAULT      = 0x8218;
static const uint16_t FRAMEBUFFER_UNDEFINED    = 0x8219;
static const uint16_t DEPTH_STENCIL_ATTACHMENT = 0x821A;
static const uint16_t MAX_VIEWPORTS            = 0x825B;

static const uint16_t MAX_DRAW_BUFFERS         = 0x8824;
static const uint16_t DRAW_BUFFER0             = 0x8825;
// DRAW_BUFFERn
static const uint16_t DRAW_BUFFER15            = 0x8834;

static const uint16_t FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
static const uint16_t FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
static const uint16_t FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
static const uint16_t FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
static const uint16_t FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
static const uint16_t FRAMEBUFFER_COMPLETE = 0x8CD5;
static const uint16_t FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
static const uint16_t FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
static const uint16_t FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = 0x8CDB;
static const uint16_t FRAMEBUFFER_INCOMPLETE_READ_BUFFER = 0x8CDC;
static const uint16_t FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
static const uint16_t MAX_COLOR_ATTACHMENTS   = 0x8CDF;
static const uint16_t COLOR_ATTACHMENT0       = 0x8CE0;
static const uint16_t COLOR_ATTACHMENT1       = 0x8CE1;
static const uint16_t COLOR_ATTACHMENT2       = 0x8CE2;
static const uint16_t COLOR_ATTACHMENT31  = 0x8CFF;
static const uint16_t DEPTH_ATTACHMENT    = 0x8D00;
static const uint16_t STENCIL_ATTACHMENT  = 0x8D20;
static const uint16_t FRAMEBUFFER         = 0x8D40;
static const uint16_t RENDERBUFFER        = 0x8D41;
static const uint16_t RENDERBUFFER_WIDTH  = 0x8D42;
static const uint16_t RENDERBUFFER_HEIGHT = 0x8D43;
static const uint16_t RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
static const uint16_t STENCIL_INDEX1  = 0x8D46;
static const uint16_t STENCIL_INDEX4  = 0x8D47;
static const uint16_t STENCIL_INDEX8  = 0x8D48;
static const uint16_t STENCIL_INDEX16 = 0x8D49;

typedef GLboolean (*PFNGLISRENDERBUFFERPROC)(GLuint renderbuffer);
typedef void (*PFNGLBINDRENDERBUFFERPROC)(GLenum target, GLuint renderbuffer);
typedef void (*PFNGLDELETERENDERBUFFERSPROC)(GLsizei n, const GLuint *renderbuffers);
typedef void (*PFNGLGENRENDERBUFFERSPROC)(GLsizei n, GLuint *renderbuffers);
typedef void (*PFNGLRENDERBUFFERSTORAGEPROC)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (*PFNGLGETRENDERBUFFERPARAMETERIVPROC)(GLenum target, GLenum pname, GLint *params);
typedef GLboolean (*PFNGLISFRAMEBUFFERPROC)(GLuint framebuffer);
typedef void (*PFNGLBINDFRAMEBUFFERPROC)(GLenum target, GLuint framebuffer);
typedef void (*PFNGLDELETEFRAMEBUFFERSPROC)(GLsizei n, const GLuint *framebuffers);
typedef void (*PFNGLGENFRAMEBUFFERSPROC)(GLsizei n, GLuint *framebuffers);
typedef GLenum (*PFNGLCHECKFRAMEBUFFERSTATUSPROC)(GLenum target);
typedef void (*PFNGLFRAMEBUFFERTEXTURE1DPROC)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (*PFNGLFRAMEBUFFERTEXTURE2DPROC)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (*PFNGLFRAMEBUFFERTEXTURE3DPROC)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset);
typedef void (*PFNGLFRAMEBUFFERRENDERBUFFERPROC)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
typedef void (*PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC)(GLenum target, GLenum attachment, GLenum pname, GLint *params);
typedef void (*PFNGLGENERATEMIPMAPPROC)(GLenum target);
typedef void (*PFNGLBLITFRAMEBUFFERPROC)(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
typedef void (*PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (*PFNGLFRAMEBUFFERTEXTURELAYERPROC)(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer);

typedef void (*PFNGLDRAWBUFFERSPROC)(GLsizei n, const GLenum *bufs);
typedef void (*PFNGLCLEARBUFFERIVPROC)(GLenum buffer, GLint drawbuffer, const GLint *value);
typedef void (*PFNGLCLEARBUFFERUIVPROC)(GLenum buffer, GLint drawbuffer, const GLuint *value);
typedef void (*PFNGLCLEARBUFFERFVPROC)(GLenum buffer, GLint drawbuffer, const GLfloat *value);
typedef void (*PFNGLCLEARBUFFERFIPROC)(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil);
typedef void (*PFNGLVIEWPORTARRAYVPROC)(GLuint first, GLsizei count, const GLfloat *v);
typedef void (*PFNGLVIEWPORTINDEXEDFVPROC)(GLuint index, const GLfloat *v);
typedef void (*PFNGLSCISSORARRAYVPROC)(GLuint first, GLsizei count, const GLint *v);
typedef void (*PFNGLSCISSORINDEXEDVPROC)(GLuint index, const GLint *v);
]])

local renderTarget = class('renderTarget')

renderTarget.static.symtab = {
  GenFramebuffers    = 'PFNGLGENFRAMEBUFFERSPROC',
  DeleteFramebuffers = 'PFNGLDELETEFRAMEBUFFERSPROC',
  BindFramebuffer    = 'PFNGLBINDFRAMEBUFFERPROC',
  IsFramebuffer      = 'PFNGLISFRAMEBUFFERPROC',
  FramebufferTexture2D    = 'PFNGLFRAMEBUFFERTEXTURE2DPROC',
  FramebufferRenderbuffer = 'PFNGLFRAMEBUFFERRENDERBUFFERPROC',
  GetRenderbufferParameteriv = 'PFNGLGETRENDERBUFFERPARAMETERIVPROC',
  CheckFramebufferStatus  = 'PFNGLCHECKFRAMEBUFFERSTATUSPROC',
  GenRenderbuffers    = 'PFNGLGENRENDERBUFFERSPROC',
  DeleteRenderbuffers = 'PFNGLDELETERENDERBUFFERSPROC',
  BindRenderbuffer    = 'PFNGLBINDRENDERBUFFERPROC',
  IsRenderbuffer      = 'PFNGLISRENDERBUFFERPROC',
  RenderbufferStorage = 'PFNGLRENDERBUFFERSTORAGEPROC',
  RenderbufferStorageMultisample = 'PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC',
  -- Core 2.0 or GL_ARB_draw_buffers
  DrawBuffers           = 'PFNGLDRAWBUFFERSPROC',
  -- clear buffer
  ClearBufferfv         = 'PFNGLCLEARBUFFERFVPROC',
  ClearBufferfi         = 'PFNGLCLEARBUFFERFIPROC',
  -- Core 4.1 or GL_ARB_viewport_array
  ViewportArrayv        = 'PFNGLVIEWPORTARRAYVPROC',
  ViewportIndexedfv     = 'PFNGLVIEWPORTINDEXEDFVPROC',
  ScissorArrayv         = 'PFNGLSCISSORARRAYVPROC',
  ScissorIndexedv       = 'PFNGLSCISSORINDEXEDVPROC',
}
--
-- set at setup()
--
local _MAX_COLOR_ATTACHMENTS = 0
local _MAX_VIEWPORTS         = 0
local _MAX_DRAW_BUFFERS      = 0

local fbCompletionError = {
  [tonumber(gl.FRAMEBUFFER_COMPLETE)] = "none",
  [tonumber(gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT)] = "Attachment incomplete",
  [tonumber(gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)] = "Attachment missing",
  [tonumber(gl.FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER)] = "Draw buffer incomplete",
  [tonumber(gl.FRAMEBUFFER_INCOMPLETE_READ_BUFFER)] = "Read buffer incomplete",
  [tonumber(gl.FRAMEBUFFER_UNSUPPORTED)] = "Not supported",
}

local colorBlack = ffi.new('float[4]', {0, 0, 0, 1})
local depthOne   = ffi.new('float[1]', {1})
local stencilOne = ffi.new('float[1]', {1})

--- import framebuffer GL symbols
function renderTarget.static:setup(ctx)
  local ver = ctx.getVersion()

  if ver >= 4.5 or ctx:isSupported("GL_ARB_direct_state_access") then
    local directStateAccess = require('agen.video.GL.renderTarget.ARB_direct_state_access')
    renderTarget:include(directStateAccess)
  elseif ver < 4.1 and not (ctx:isSupported('GL_ARB_framebuffer_object') and
                        ctx:isSupported('GL_ARB_draw_buffers') and 
                        ctx:isSupported('GL_ARB_viewport_array')) then
    error("Missing required functionality")
  end

  for k, v in pairs(renderTarget.symtab) do
    gl[k] = v
  end

  _MAX_COLOR_ATTACHMENTS = math.min(8, gl.getCap(gl.MAX_COLOR_ATTACHMENTS))
  _MAX_VIEWPORTS = math.min(8, gl.getCap(gl.MAX_VIEWPORTS))
  -- Note: there's no GL_MAX_SCISSORS, must match GL_MAX_VIEWPORTS
  _MAX_DRAW_BUFFERS      = math.min(8, gl.getCap(gl.MAX_DRAW_BUFFERS))
end

local function createTextureFor(purpose, textureDesc)
  local bpp = tonumber(bit.band(textureDesc.format, 0x000000ff))
  assert(bpp > 0, "Invalid bytes per pixel for format [" .. textureDesc.format .. "]")

  return GLTexture.new({
    purpose	  = purpose,
    width	  = textureDesc.width,
    height	  = textureDesc.height,
    format	  = textureDesc.format,
    bytesPerPixel = bpp,
    pitch	  = (textureDesc.pitch or textureDesc.width) * bpp,
    data	  = nil,
    samples   	  = textureDesc.samples or 1,
  })
end

--- Default render target is the back buffer (framebuffer ID 0)
function renderTarget.static:createDefault(w, h)
  return self:new(w, h)
end
--- Create render target with texture(s) backing
-- @param renderTargetDesc lua table describing the render target structure
-- @return self new render target object
function renderTarget.static:createWithDesc(renderTargetDesc)
  assert(renderTargetDesc ~= nil, 'invalid argument #1, render target description not set')
  assert(not(renderTargetDesc.depth_stencil and (renderTargetDesc.depth or renderTargetDesc.stencil)),
    "Cannot specify both depth_stencil and depth or stencil")
  return self:new(renderTargetDesc)
end

function renderTarget:newFramebuffers(num)
  local fbo = ffi.new('GLuint[?]', num)
  gl.GenFramebuffers(num, fbo)
  return fbo
end

function renderTarget:initializeWithDesc(renderTargetDesc)
  local fbo = self:newFramebuffers(1)

  self.fbo = ffi.gc(fbo, function(ids)
    gl.DeleteFramebuffers(1, ids)
  end)
  self.attachments = {
    color = {},
    depth_stencil = nil,
    depth = nil,
    stencil = nil,
  }
  self.viewports = ffi.new('float[?]', 4 * _MAX_VIEWPORTS)
  self.scissors  = ffi.new('int[?]',   4 * _MAX_VIEWPORTS)

  gl.BindFramebuffer(gl.FRAMEBUFFER, self:getID())
  self:_init(renderTargetDesc)
  gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
end

function renderTarget:initialize(w, h)
  if h == nil then
    self:initializeWithDesc(w)
  else
    self:initializeDefault(w, h)
  end
end

function renderTarget:initializeDefault(w, h)
  self.fbo = ffi.new('GLuint[1]')
  self.fbo[0] = 0 -- framebuffer ID 0
  self.attachments = {
    color = {
      [0] = {
        texture = nil,
        clearColor = colorBlack,
      },
    },
    depth_stencil = {
      texture = nil,
      clearColor = depthOne,
    },
    depth = {
      texture = nil,
      clearColor = depthOne,
    },
    stencil = nil,
  }
  self.viewports = ffi.new('float[?]', 4 * _MAX_VIEWPORTS)
  self.scissors  = ffi.new('int[?]',   4 * _MAX_VIEWPORTS)

  self.viewports[0] = 0
  self.viewports[1] = 0
  self.viewports[2] = w
  self.viewports[3] = h

  self.scissors[0] = 0
  self.scissors[1] = 0
  self.scissors[2] = w
  self.scissors[3] = h
end

function renderTarget:_init(renderTargetDesc)
  local numColorAttachments = 0

  for name, value in pairs(renderTargetDesc) do
    local colorBufferIndex = string.match(name, 'color(%d)')
    if colorBufferIndex ~= nil then
      colorBufferIndex = tonumber(colorBufferIndex)
      --
      -- don't exceed MAX_COLOR_ATTACHMENTS
      --
      assert(colorBufferIndex < _MAX_COLOR_ATTACHMENTS, 'color buffer index out of range ' .. colorBufferIndex)
      --
      -- cannot specify the same index twice
      --
      assert(self.attachments.color[colorBufferIndex] == nil, "Render target attachment at index [" .. colorBufferIndex .. "] already exists!")
      --
      -- add color attachment
      --
      agen.log.verbose('video', "Adding color attachemnt [%d]", colorBufferIndex)
      self.attachments.color[colorBufferIndex] = {
        texture = value.tex or createTextureFor('color', value),
        clearColor = value.clearColor or colorBlack,
      }
      numColorAttachments = numColorAttachments + 1
      local w, h = self.attachments.color[colorBufferIndex].texture:getSize(0)
      self.viewports[colorBufferIndex * 4 + 2] = w
      self.viewports[colorBufferIndex * 4 + 3] = h

      self.scissors[colorBufferIndex * 4 + 2] = w
      self.scissors[colorBufferIndex * 4 + 3] = h
      agen.log.debug('video', "Render target [%d] size [%dx%d]", colorBufferIndex, w, h)
    elseif name == 'depth' then
      --
      -- add depth attachment
      --
      self.attachments.depth = {
        texture = value.tex or createTextureFor('depth', value),
        clearColor = value.clearColor or depthOne,
      }
    elseif name == 'stencil' then
      --
      -- add stencil attachment
      --
      self.attachments.stencil = {
        texture = value.tex or createTextureFor('stencil', value),
        clearColor = value.clearColor or stencilOne,
      }
    elseif name == 'depth_stencil' then
      --
      -- add depth_stencil attachment
      --
      self.attachments.depth_stencil = {
        texture = value.tex or createTextureFor('depth_stencil', value),
        clearColor = value.clearColor or depthOne,
      }
    else
      --
      -- unknown attachment
      --
      error("Unsupported attachment [" .. name .. "]")
    end
  end

  if numColorAttachments > 0 then
    agen.log.verbose('video', "Will create render target with [%d] color attachments", numColorAttachments)
    local drawBuffers = ffi.new('GLenum[?]', numColorAttachments)
    local i = 0

    for _, colorAttachment in pairs(self.attachments.color) do
      self:framebufferTexture(gl.COLOR_ATTACHMENT0 + i, colorAttachment.texture:getID())
      drawBuffers[i] = gl.COLOR_ATTACHMENT0 + i
      i = i + 1
    end

    agen.log.verbose('video', "Setting up [%d] draw buffers...", numColorAttachments)
    if numColorAttachments > _MAX_DRAW_BUFFERS then
      agen.log.warning('too many color buffers, limiting to [%d]', _MAX_DRAW_BUFFERS)
      numColorAttachments = _MAX_DRAW_BUFFERS
    end
    gl.DrawBuffers(numColorAttachments, drawBuffers)
  end

  if self.attachments.depth then
    self:framebufferTexture(gl.DEPTH_ATTACHMENT, self.attachments.depth.texture:getID())
  end

  if self.attachments.stencil then
    self:framebufferTexture(gl.STENCIL_ATTACHMENT, self.attachments.stencil.texture:getID())
  end

  if self.attachments.depth_stencil then
    self:framebufferTexture(gl.DEPTH_STENCIL_ATTACHMENT, self.attachments.depth_stencil.texture:getID())
  end

  local err, msg = gl.GetError()
  if err ~= 0 then
    error("create framebuffer failed:" .. msg)
  end

  self:checkStatus()

  return self
end

function renderTarget:getID()
  return self.fbo[0]
end

function renderTarget:framebufferTexture(attachment, id)
  gl.FramebufferTexture2D(gl.FRAMEBUFFER, attachment, gl.TEXTURE_2D, id, 0)
end

function renderTarget:_checkStatus()
  return gl.CheckFramebufferStatus(gl.FRAMEBUFFER)
end

function renderTarget:checkStatus()
  local err = self:_checkStatus()
  if err ~= gl.FRAMEBUFFER_COMPLETE then
    error("Framebuffer not complete:" .. (fbCompletionError[tonumber(err)] or "unknown"))
  end
end

--- Select as 'current'
function renderTarget:setActive()
  gl.BindFramebuffer(gl.FRAMEBUFFER, self:getID())
  gl.ViewportArrayv(0, _MAX_VIEWPORTS, self.viewports)
  --gl.ScissorArrayv(0, _MAX_VIEWPORTS, self.scissors)
end
--- Fill all attachments with default values
function renderTarget:clear()
  for i = 0, _MAX_COLOR_ATTACHMENTS - 1, 1 do
    if self.attachments.color[i] ~= nil then
      self:clearColor(i)
    end
  end

  if self.depth_stencil then
    self:clearDepthStencil()
  elseif self.depth then
    self:clearDepth()
  end
end

function renderTarget:setClearColor(index, color)
  self.attachments.color[index].clearColor = color
end

function renderTarget:getClearColor(index)
  return self.attachments.color[index].clearColor
end

function renderTarget:clearColor(index, color)
  self:clearFV(gl.COLOR, index, color or self.attachments.color[index].clearColor)
end

function renderTarget:clearDepth()
  self:clearFV(gl.DEPTH, 0, self.attachments.depth.clearColor)
end

function renderTarget:clearFV(enum, index, attachment)
  gl.ClearBufferfv(enum, index, attachment)
end

function renderTarget:clearDepthStencil()
  local att = self.attachments
  self:clearFI(gl.DEPTH_STENCIL, 0, att.depth_stencil.clearColor[0], stencilOne[0])
end

function renderTarget:clearFI(enum, index, attachment, val)
  gl.ClearBufferfi(enum, index, attachment, val)
end

function renderTarget:setViewVolume(index, l, b, w, h, n, f)
  self.viewports[index * 4 + 0] = l
  self.viewports[index * 4 + 1] = b
  self.viewports[index * 4 + 2] = w
  self.viewports[index * 4 + 3] = h
end

function renderTarget:getViewVolume(index)
  return self.viewports[index * 4 + 2], self.viewports[index * 4 + 3]
end

function renderTarget:getColorAttachment(index)
  return self.attachments.color[index].texture
end

function renderTarget:getDepthAttachment()
  if self.attachments.depth then
    return self.attachments.depth.texture
  end
  if self.attachments.depth_stencil then
    return self.attachments.depth_stencil.texture
  end
  return nil
end

return renderTarget
