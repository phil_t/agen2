local math = require('math')
local gl   = require('agen.video.GL.lib')
local ffi  = gl.ffi

ffi.cdef([[
typedef void (*PFNGLTEXIMAGE2DPROC)(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels);
typedef void (*PFNGLTEXIMAGE3DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
typedef void (*PFNGLTEXIMAGE2DMULTISAMPLEPROC)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
typedef void (*PFNGLTEXIMAGE3DMULTISAMPLEPROC) (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations);
typedef void (*PFNGLCOMPRESSEDTEXIMAGE2DPROC)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data);
typedef void (*PFNGLCOMPRESSEDTEXIMAGE3DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data);
]])

local toInternalFormat = {
  [tonumber(gl.COMPRESSED_RGB_S3TC_DXT1)]  = gl.RGBA,
  [tonumber(gl.COMPRESSED_RGBA_S3TC_DXT1)] = gl.RGBA,
  [tonumber(gl.COMPRESSED_RGBA_S3TC_DXT3)] = gl.RGBA,
  [tonumber(gl.COMPRESSED_RGBA_S3TC_DXT5)] = gl.RGBA,
  [tonumber(gl.DEPTH_COMPONENT16)]         = gl.DEPTH_COMPONENT,
  [tonumber(gl.DEPTH_COMPONENT24)]         = gl.DEPTH_COMPONENT,
  [tonumber(gl.DEPTH_COMPONENT32)]         = gl.DEPTH_COMPONENT,
}

local TextureExt = {}

TextureExt.symtab = {
	TexImage2D = 'PFNGLTEXIMAGE2DPROC',
	TexImage3D = 'PFNGLTEXIMAGE3DPROC',
  TexImage2DMultisample = 'PFNGLTEXIMAGE2DMULTISAMPLEPROC',
  TexImage3DMultisample = 'PFNGLTEXIMAGE3DMULTISAMPLEPROC',
  CompressedTexImage2D  = 'PFNGLCOMPRESSEDTEXIMAGE2DPROC',
  CompressedTexImage3D  = 'PFNGLCOMPRESSEDTEXIMAGE3DPROC',
}
--- Allocate texture storage according to inputs
function TextureExt:_init(options)
  local w, h = self:getSize()

  if options.samples and options.samples > 1 then
    gl.TexImage2DMultisample(self.target, options.samples, self.format, w, h, gl.TRUE)
  else
    gl.TexParameteri(self.target, gl.TEXTURE_BASE_LEVEL, 0)
    gl.TexParameteri(self.target, gl.TEXTURE_MAX_LEVEL, options.levels - 1)

    for l = 0, options.levels - 1, 1 do
	    gl.TexImage2D(self.target, l, self.format, w, h, 0, toInternalFormat[tonumber(self.format)] or self.format, gl.UNSIGNED_BYTE, nil)
      w = math.max(1, math.floor(w / 2))
      h = math.max(1, math.floor(h / 2))
    end
  end
end

return TextureExt
