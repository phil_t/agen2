local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t STENCIL_INDEX = 0x1901;
]])

local TexStencil = {}

return TexStencil
