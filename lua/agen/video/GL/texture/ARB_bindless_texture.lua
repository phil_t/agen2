------------------------------------------------------------------------
--
-- Core 4.4 or ARB_bindless_texture
-- allows OpenGL applications to access texture objects in shaders without 
-- first binding each texture to one of a limited number of texture image 
-- units.
--
------------------------------------------------------------------------
local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
// Accepted by the <type> parameter of VertexAttribLPointer:
static const uint16_t UNSIGNED_INT64_ARB = 0x140F;

typedef GLuint64 (*PFNGLGETTEXTUREHANDLEARBPROC)(GLuint texture);
typedef GLuint64 (*PFNGLGETTEXTURESAMPLERHANDLEARBPROC)(GLuint texture, GLuint sampler);
typedef void (*PFNGLMAKETEXTUREHANDLERESIDENTARBPROC)(GLuint64 handle);
typedef void (*PFNGLMAKETEXTUREHANDLENONRESIDENTARBPROC)(GLuint64 handle);
typedef GLuint64 (*PFNGLGETIMAGEHANDLEARBPROC)(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
typedef void (*PFNGLMAKEIMAGEHANDLERESIDENTARBPROC)(GLuint64 handle, GLenum access);
typedef void (*PFNGLMAKEIMAGEHANDLENONRESIDENTARBPROC)(GLuint64 handle);
typedef void (*PFNGLUNIFORMHANDLEUI64ARBPROC)(GLint location, GLuint64 value);
typedef void (*PFNGLUNIFORMHANDLEUI64VARBPROC)(GLint location, GLsizei count, const GLuint64 *value);
typedef void (*PFNGLPROGRAMUNIFORMHANDLEUI64ARBPROC)(GLuint program, GLint location, GLuint64 value);
typedef void (*PFNGLPROGRAMUNIFORMHANDLEUI64VARBPROC)(GLuint program, GLint location, GLsizei count, const GLuint64 *values);
typedef GLboolean (*PFNGLISTEXTUREHANDLERESIDENTARBPROC)(GLuint64 handle);
typedef GLboolean (*PFNGLISIMAGEHANDLERESIDENTARBPROC)(GLuint64 handle);
]])

local ABT = {}

ABT.symtab = {
  GetTextureHandleARB = 'PFNGLGETTEXTUREHANDLEARBPROC',
  GetTextureSamplerHandleARB = 'PFNGLGETTEXTURESAMPLERHANDLEARBPROC',
  IsTextureHanderResidentARB = 'PFNGLISTEXTUREHANDLERESIDENTARBPROC',
  MakeTextureHandleResidentARB = 'PFNGLMAKETEXTUREHANDLERESIDENTARBPROC',
  MakeTextureHandleNonResidentARB = 'PFNGLMAKETEXTUREHANDLENONRESIDENTARBPROC',
  IsImageHandleResidentARB = 'PFNGLISIMAGEHANDLERESIDENTARBPROC',
  GetImageHandleARB = 'PFNGLGETIMAGEHANDLEARBPROC',
  MakeImageHandleResidentARB = 'PFNGLMAKEIMAGEHANDLERESIDENTARBPROC',
  MakeImageHandleNonResidentARB = 'PFNGLMAKEIMAGEHANDLENONRESIDENTARBPROC',
  UniformHandleui64ARB = 'PFNGLUNIFORMHANDLEUI64ARBPROC',
  UniformHandleui64vARB = 'PFNGLUNIFORMHANDLEUI64VARBPROC',
  ProgramUniformHandleui64ARB = 'PFNGLPROGRAMUNIFORMHANDLEUI64ARBPROC',
  ProgramUniformHandleui64vARB = 'PFNGLPROGRAMUNIFORMHANDLEUI64VARBPROC',
}

return ABT
