------------------------------------------------------------------------
--
-- NV_bindless_texture
-- allows OpenGL applications to access texture objects in shaders without 
-- first binding each texture to one of a limited number of texture image 
-- units.
--
------------------------------------------------------------------------
local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef GLuint64 (*PFNGLGETTEXTUREHANDLENVPROC)(GLuint texture);
typedef GLuint64 (*PFNGLGETTEXTURESAMPLERHANDLENVPROC)(GLuint texture, GLuint sampler);
typedef void (*PFNGLMAKETEXTUREHANDLERESIDENTNVPROC)(GLuint64 handle);
typedef void (*PFNGLMAKETEXTUREHANDLENONRESIDENTNVPROC)(GLuint64 handle);
typedef GLuint64 (*PFNGLGETIMAGEHANDLENVPROC)(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
typedef void (*PFNGLMAKEIMAGEHANDLERESIDENTNVPROC)(GLuint64 handle, GLenum access);
typedef void (*PFNGLMAKEIMAGEHANDLENONRESIDENTNVPROC)(GLuint64 handle);
typedef void (*PFNGLUNIFORMHANDLEUI64NVPROC)(GLint location, GLuint64 value);
typedef void (*PFNGLUNIFORMHANDLEUI64VNVPROC)(GLint location, GLsizei count, const GLuint64 *value);
typedef void (*PFNGLPROGRAMUNIFORMHANDLEUI64NVPROC)(GLuint program, GLint location, GLuint64 value);
typedef void (*PFNGLPROGRAMUNIFORMHANDLEUI64VNVPROC)(GLuint program, GLint location, GLsizei count, const GLuint64 *values);
typedef GLboolean (*PFNGLISTEXTUREHANDLERESIDENTNVPROC)(GLuint64 handle);
typedef GLboolean (*PFNGLISIMAGEHANDLERESIDENTNVPROC)(GLuint64 handle);
]])

local NBT = {}

NBT.symtab = {
  -- will create a texture handle using the current state of the texture named <texture>, 
  -- including any embedded sampler state.
  GetTextureHandleNV = 'PFNGLGETTEXTUREHANDLENVPROC',
  -- will create a texture handle using the current non-sampler state of the texture named <texture>
  -- and the sampler state from the sampler object <sampler>
  GetTextureSamplerHandleNV = 'PFNGLGETTEXTURESAMPLERHANDLENVPROC',
  IsTextureHanderResidentNV = 'PFNGLISTEXTUREHANDLERESIDENTNVPROC',
  MakeTextureHandleResidentNV = 'PFNGLMAKETEXTUREHANDLERESIDENTNVPROC',
  MakeTextureHandleNonResidentNV = 'PFNGLMAKETEXTUREHANDLENONRESIDENTNVPROC',
  IsImageHandleResidentNV = 'PFNGLISIMAGEHANDLERESIDENTNVPROC',
  GetImageHandleNV = 'PFNGLGETIMAGEHANDLENVPROC',
  MakeImageHandleResidentNV = 'PFNGLMAKEIMAGEHANDLERESIDENTNVPROC',
  MakeImageHandleNonResidentNV = 'PFNGLMAKEIMAGEHANDLENONRESIDENTNVPROC',
  UniformHandleui64NV = 'PFNGLUNIFORMHANDLEUI64NVPROC',
  UniformHandleui64vNV = 'PFNGLUNIFORMHANDLEUI64VNVPROC',
  ProgramUniformHandleui64NV = 'PFNGLPROGRAMUNIFORMHANDLEUI64NVPROC',
  ProgramUniformHandleui64vNV = 'PFNGLPROGRAMUNIFORMHANDLEUI64VNVPROC',
}
-- The handle for each texture or texture/sampler pair is unique; the same
-- handle will be returned if GetTextureHandleNV is called multiple times for
-- the same texture or if GetTextureSamplerHandleNV is called multple times
-- for the same texture/sampler pair.
function NBT:getGPUAddress()
  local addr = gl.GetTextureHandleNV(self.id[0])
  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("failed to get texture handle [%s]", msg))
  end
  return addr
end

function NBT:setActive(texUnitID, samplerObj)
  --gl.ActiveTexture(gl.TEXTURE0 + texUnitID)
  gl.MakeTextureHandleResidentNV(self:getGPUAddress())
  samplerObj:setActive(texUnitID)
end

return NBT
