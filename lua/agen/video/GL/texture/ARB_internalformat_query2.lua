local gl = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t SAMPLES                       = 0x80A9; 
static const uint16_t NUM_SAMPLE_COUNTS             = 0x9380;
static const uint16_t INTERNALFORMAT_SUPPORTED      = 0x826F;
static const uint16_t INTERNALFORMAT_PREFERRED      = 0x8270;
static const uint16_t INTERNALFORMAT_RED_SIZE       = 0x8271;
static const uint16_t INTERNALFORMAT_GREEN_SIZE     = 0x8272;
static const uint16_t INTERNALFORMAT_ALPHA_SIZE     = 0x8274;
static const uint16_t INTERNALFORMAT_DEPTH_SIZE     = 0x8275;
static const uint16_t INTERNALFORMAT_STENCIL_SIZE   = 0x8276;
static const uint16_t INTERNALFORMAT_SHARED_SIZE    = 0x8277;
static const uint16_t INTERNALFORMAT_RED_TYPE       = 0x8278;
static const uint16_t INTERNALFORMAT_GREEN_TYPE     = 0x8279;
static const uint16_t INTERNALFORMAT_BLUE_TYPE      = 0x827A;
static const uint16_t INTERNALFORMAT_ALPHA_TYPE     = 0x827B;
static const uint16_t INTERNALFORMAT_DEPTH_TYPE     = 0x827C;
static const uint16_t INTERNALFORMAT_STENCIL_TYPE   = 0x827D;
static const uint16_t MAX_WIDTH                     = 0x827E;
static const uint16_t MAX_HEIGHT                    = 0x827F;
static const uint16_t MAX_DEPTH                     = 0x8280;
static const uint16_t MAX_LAYERS                    = 0x8281;

typedef GLvoid (*PFNGETINTERNALFORMATIVPROC)(GLenum target, GLenum internalformat, GLenum pname,
                                             GLsizei bufSize, GLint *params);
typedef GLvoid (*PFNGETINTERNALFORMATI64VPROC)(GLenum target, GLenum internalformat, GLenum pname,
                                               GLsizei bufSize, GLint64 *params);
]])

local IFQ = {}

IFQ.symtab = {
    GetInternalFormativ   = 'PFNGETINTERNALFORMATIVPROC',
    GetInternalFormati64v = 'PFNGETINTERNALFORMATI64VPROC',
}

return IFQ