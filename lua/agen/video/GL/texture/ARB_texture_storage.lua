------------------------------------------------------------------
--
-- GL_ARB_texture_storage             or Core 4.2
-- GL_ARB_texture_storage_multisample or Core 4.3
--
------------------------------------------------------------------
local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
typedef void (*PFNGLTEXSTORAGE1DPROC)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width);
typedef void (*PFNGLTEXSTORAGE2DPROC)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (*PFNGLTEXSTORAGE3DPROC)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
typedef void (*PFNGLTEXSTORAGE2DMULTISAMPLEPROC)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
typedef void (*PFNGLTEXSTORAGE3DMULTISAMPLEPROC)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations);
]])

local TexStorage = {}

TexStorage.toInternalFormat = {
  [tonumber(gl.RGBA)] = gl.RGBA8,
  [tonumber(gl.RGB)]  = gl.RGB8,
  [tonumber(gl.RG)]   = gl.RG8,
  [tonumber(gl.RED)]  = gl.R8,
}

TexStorage.symtab = {
  --TexStorage1D = 'PFNGLTEXSTORAGE1DPROC',
  TexStorage2D = 'PFNGLTEXSTORAGE2DPROC',
  TexStorage3D = 'PFNGLTEXSTORAGE3DPROC',
  --TexStorage2DMultisample = 'PFNGLTEXSTORAGE2DMULTISAMPLEPROC',
  --TexStorage3DMultisample = 'PFNGLTEXSTORAGE3DMULTISAMPLEPROC',
}

--- Allocate texture storage according to inputs
function TexStorage:_init(options)
  local fmt = self.toInternalFormat[tonumber(self.format)] or self.format
  local w, h = self:getSize()
  if options.samples ~= nil and options.samples > 1 then
    error("TODO: multisample textures support")
  end
  agen.log.debug('video', "TexStorage2D using internal format [0x%x]", fmt)
	gl.TexStorage2D(self.target, options.levels or 1, fmt, w, h)
end

return TexStorage
