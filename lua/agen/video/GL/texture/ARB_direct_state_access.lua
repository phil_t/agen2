local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi

ffi.cdef([[
static const uint16_t TEXTURE_TARGET = 0x1006;
static const uint16_t QUERY_TARGET   = 0x82EA;

typedef GLvoid (*PFNGLCREATETEXTURESPROC)(GLenum target, GLsizei n, GLuint *textures);
typedef GLvoid (*PFNGLTEXTURESTORAGE2DPROC)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
typedef GLvoid (*PFNGLTEXTUREPARAMETERIVPROC)(GLuint texture, GLenum pname, const GLint *param);
typedef GLvoid (*PFNGLGENERATETEXTUREMIPMAPPROC)(GLuint texture);
typedef GLvoid (*PFNGLBINDTEXTUREUNITPROC)(GLuint unit, GLuint texture);
typedef GLvoid (*PFNGLTEXTURESUBIMAGE2DPROC)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type,
const GLvoid *pixels);
typedef GLvoid (*PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height,
                                     GLenum format,
                                     GLsizei imageSize, const GLvoid *data);
]])

local DSA = require('agen.video.GL.texture.ARB_texture_storage')

DSA.symtab = {
    CreateTextures = 'PFNGLCREATETEXTURESPROC',
    TextureStorage2D = 'PFNGLTEXTURESTORAGE2DPROC',
    TextureParameteriv = 'PFNGLTEXTUREPARAMETERIVPROC',
    GenerateTextureMipmap = 'PFNGLGENERATETEXTUREMIPMAPPROC',
    BindTextureUnit = 'PFNGLBINDTEXTUREUNITPROC',
    TextureSubImage2D = 'PFNGLTEXTURESUBIMAGE2DPROC',
    CompressedTextureSubImage2D = 'PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC'
}

function DSA:_alloc()
  gl.CreateTextures(self.target, 1, self.id)
end

function DSA:_init(options)
    local fmt = self.toInternalFormat[tonumber(self.format)] or self.format
    local w, h = self:getSize()
    if options.samples ~= nil and options.samples > 1 then
      error("TODO: multisample textures support")
    end
    agen.log.debug('video', "TextureStorage2D using internal format [0x%x]", fmt)
    gl.TextureStorage2D(self:getID(), options.levels or 1, fmt, w, h)
end

function DSA:swizzle(val)
    gl.TextureParameteriv(self:getID(), gl.TEXTURE_SWIZZLE_RGBA, val)
end

function DSA:generateMipLevels()
    gl.GenerateTextureMipmap(self:getID())
    local err, msg = gl.GetError()
    if err ~= 0 then
      error(string.format("failed to create texture [%s]", msg))
    end
end

function DSA:setActive(texUnitID, samplerObj)
  gl.BindTextureUnit(texUnitID, self:getID())
  samplerObj:setActive(texUnitID)
end

function DSA:updateRect(level, destRect, data, dataLen, dataPitch)
  assert(data, "Cannot update texture region with no data")
  local x, y, w, h
  if destRect == nil then
    x, y, w, h = 0, 0, self:getSize()
  else
    x, y, w, h = destRect.x, destRect.y, destRect.w, destRect.h
  end

  if self.format > 0x8000 then
     assert(dataLen > 0, "data length is required for compressed formats")
    --
    -- fetch actual format
    --
    --local fmt = ffi.new('GLint[1]')
    --gl.GetTexLevelParameteriv(self.target, 0, gl.TEXTURE_INTERNAL_FORMAT, fmt)
    --agen.log.debug('video', "updating level [%d] data size [%d], [%dx%d]", level, dataLen, w, h)
    gl.CompressedTextureSubImage2D(self:getID(), level, x, y, w, h, self.format, dataLen, data);
  else
    gl.TextureSubImage2D(self:getID(), level, x, y, w, h, self.format, gl.UNSIGNED_BYTE, data)
  end
  local err, msg = gl.GetError()
  if err ~= 0 then
    error(string.format("failed to update texture: [%s]", msg))
  end
end

return DSA
