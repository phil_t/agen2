local gl  = require('agen.video.GL.lib')
local ffi = gl.ffi
------------------------------------------------------------------------
--
-- GL_ARB_sync or Core 3.2
--
------------------------------------------------------------------------
ffi.cdef([[
typedef struct _GLsync {void* ptr;} GLsync;

static const uint16_t MAX_SERVER_WAIT_TIMEOUT = 0x9111;
static const uint16_t OBJECT_TYPE             = 0x9112;
static const uint16_t SYNC_CONDITION          = 0x9113;
static const uint16_t SYNC_STATUS             = 0x9114;
static const uint16_t SYNC_FLAGS              = 0x9115;
static const uint16_t SYNC_FENCE              = 0x9116;
static const uint16_t SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
static const uint16_t UNSIGNALED              = 0x9118;
static const uint16_t SIGNALED                = 0x9119;
static const uint16_t ALREADY_SIGNALED        = 0x911A;
static const uint16_t TIMEOUT_EXPIRED         = 0x911B;
static const uint16_t CONDITION_SATISFIED     = 0x911C;
static const uint16_t WAIT_FAILED             = 0x911D;

typedef GLsync (*PFNGLFENCESYNCPROC)(GLenum condition, GLbitfield flags);
typedef GLboolean (*PFNGLISSYNCPROC)(GLsync sync);
typedef void (*PFNGLDELETESYNCPROC)(GLsync sync);
typedef GLenum (*PFNGLCLIENTWAITSYNCPROC)(GLsync sync, GLbitfield flags, GLuint64 timeout);
typedef void (*PFNGLWAITSYNCPROC)(GLsync sync, GLbitfield flags, GLuint64 timeout);
typedef void (*PFNGLGETINTEGER64VPROC)(GLenum pname, GLint64 *data);
typedef void (*PFNGLGETSYNCIVPROC)(GLsync sync, GLenum pname, GLsizei bufSize, GLsizei *length, GLint *values);
]])

local GLFence = {}
local GLFenceMT = {__index = GLFence}

local symtab = {
  FenceSync     = 'PFNGLFENCESYNCPROC',
  IsSync        = 'PFNGLISSYNCPROC',
  DeleteSync    = 'PFNGLDELETESYNCPROC',
  ClientWaitSync = 'PFNGLCLIENTWAITSYNCPROC',
  WaitSync      = 'PFNGLWAITSYNCPROC',
  GetInteger64v = 'PFNGLGETINTEGER64VPROC',
  GetSynciv     = 'PFNGLGETSYNCIVPROC',
}

GLFence.MAX_WAIT = 0

function GLFence.setup(ctx)
  for k, v in pairs(symtab) do
    gl[k] = v
  end

  local max_tmout = ffi.new('GLint64[1]')
  gl.GetInteger64v(gl.MAX_SERVER_WAIT_TIMEOUT, max_tmout)
  GLFence.MAX_WAIT = max_tmout[0]
  agen.log.verbose('application', "MAX_SERVER_WAIT_TIMEOUT = %s", tostring(GLFence.MAX_WAIT))
end
--- Will create and insert fence in the commands list
function GLFence.new()
  local self = gl.FenceSync(gl.SYNC_GPU_COMMANDS_COMPLETE, 0)
  assert(self and gl.IsSync(self) == gl.TRUE, "Failed to create sync object")
  return ffi.gc(self, function(f)
    if gl.IsSync(f) then
      gl.DeleteSync(f)
    end
  end)
end

function GLFence:getStatus()
  local sz = ffi.new('GLsizei[1]')
  local val = ffi.new('GLint[1]')

  gl.GetSynciv(self, gl.SYNC_STATUS, ffi.sizeof('GLint'), sz, val)
  if sz[0] ~= 1 then
    error("failed to get fence status")
  end

  if val[0] == gl.SIGNALED then
    return 'signaled'
  end
  return 'unsignaled'
end

function GLFence:waitClient()
  error('not implemented')
end

return ffi.metatype('GLsync', GLFenceMT)
