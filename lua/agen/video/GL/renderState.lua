local class      = require('middleclass')
local gl				 = require('agen.video.GL.lib')
local renderPass = require('agen.video.GL.renderPass')
local mat4       = require('agen.util.math.mat4')
local ffi        = gl.ffi

ffi.cdef([[
typedef void (*PFNGLPOLYGONMODEPROC)(GLenum face, GLenum mode);
typedef void (*PFNGLCULLFACEPROC)(GLenum mode);
typedef void (*PFNGLBLENDFUNCPROC)(GLenum srcFactor, GLenum destFactor);
typedef void (*PFNGLBLENDEQUATIONPROC)(GLenum mode);
typedef void (*PFNGLDEPTHFUNCPROC)(GLenum mode);
typedef void (*PFNGLDEPTHMASKPROC)(GLboolean flag);
typedef void (*PFNGLSHADEMODELPROC)(GLenum model);
typedef void (*PFNGLFRONTFACEPROC)(GLenum mode);
typedef void (*PFNGLDEPTHRANGEFPROC)(GLfloat nearVal, GLfloat farVal);
]])

local RenderState = class('RenderState')
local IRenderState = require('agen.interface.video.renderState')
RenderState:include(IRenderState)

local symtab = {
  ShadeModel     = 'PFNGLSHADEMODELPROC',
  FrontFace      = 'PFNGLFRONTFACEPROC',
  PolygonMode    = 'PFNGLPOLYGONMODEPROC',
  CullFace       = 'PFNGLCULLFACEPROC',
  DepthFunc      = 'PFNGLDEPTHFUNCPROC',
  DepthMask      = 'PFNGLDEPTHMASKPROC',
  DepthRangef    = 'PFNGLDEPTHRANGEFPROC',
	BlendFunc			 = 'PFNGLBLENDFUNCPROC',
	BlendEquation  = 'PFNGLBLENDEQUATIONPROC',
}

RenderState.static.mapPrimitives = {
  points    = gl.POINTS,
  lines     = gl.LINES,
  loop      = gl.LINE_LOOP,
  linestrip = gl.LINE_STRIP,
  triangles = gl.TRIANGLES,
  strip     = gl.TRIANGLE_STRIP,
  fan       = gl.TRIANGLE_FAN,
  quads     = gl.QUADS,
  quadstrip = gl.QUAD_STRIP,
  polygon   = gl.POLYGON,
}

function RenderState.static:setup(context)
  for k, v in pairs(symtab) do
    gl[k] = v
  end
  renderPass:setup(context)
end

function RenderState:initialize(device, opts)
  assert(RenderState.mapPrimitives[opts.topology] ~= nil, 'unsupported topology ' .. opts.topology)

  self.owner   = device
  self.program = device:createShader(opts.shaders)
  self.vdecl   = opts.vertexBuffersAttr
  self.xform   = {
    projection = mat4.identity(),
    model      = mat4.identity(),
    view       = mat4.identity(),
    mvp        = mat4.create(),
  }
  self.topology = RenderState.mapPrimitives[opts.topology]
	self.state   = {
    blend_mode = 'off',
		fill_mode	 = 'solid',
		light			 = 'off',
    cull       = 'off',
    depth      = 'off',
    scissor    = 'off',
	}
  -- in IRenderState
  self:init()
end

function RenderState:setActive()
  -- apply shader
  self.program:setActive()
  -- apply state: no (i)pairs in hotpath (luajit doesn't support compiling it)
  self:setState('blend_mode', self.state.blend_mode)
  self:setState('fill_mode', self.state.fill_mode)
  self:setState('light', self.state.light)
  self:setState('cull', self.state.cull)
  self:setState('depth', self.state.depth)
  self:setState('scissor', self.state.scissor)
end

function RenderState:_createRenderPass()
  return renderPass:new(self)
end
-----------------------------------------------------------------------------------------------
--
-- state setters
--
-----------------------------------------------------------------------------------------------
function RenderState:_setWireFrame(enable)
	if enable then
		self.state.fill_mode = 'line'
	else
		self.state.fill_mode = 'solid'
	end
end

function RenderState:_setScissorTest(value)
  self.state.scissor = value and 'on' or 'off'
end

function RenderState:_setDepthFunc(name)
  self.state.depth = name
end

function RenderState:_setBlendFunc(name)
  self.state.blend_mode = name
end

function RenderState:_setCullFunc(name)
  self.state.cull = name
end

function RenderState:setState(name, value)
  --agen.log.verbose('application', "[%s] = [%s]", name, value)
  gl.FrontFace(gl.CCW)
	if name == 'blend_mode' then
		self:setBlendMode(value)
	elseif name == 'fill_mode' then
    if value == 'line' then
      gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
    else
      gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
    end
  elseif name == 'cull' then
    if value == 'off' then
      gl.Disable(gl.CULL_FACE)
    elseif value == 'front' then
      gl.Enable(gl.CULL_FACE)
      gl.CullFace(gl.FRONT)
    elseif value == 'back' then
      gl.Enable(gl.CULL_FACE)
      gl.CullFace(gl.BACK)
    end
	elseif name == 'light' then
    -- spammer
    --agen.log.warn('appliation', 'state "light" not implemented')
  elseif name == 'depth' then
    if value == 'off' then
      gl.Disable(gl.DEPTH_TEST)
    elseif value == 'less' then
      gl.Enable(gl.DEPTH_TEST)
      gl.DepthFunc(gl.LESS)
    elseif value == 'greater' then
      gl.Enable(gl.DEPTH_TEST)
      gl.DepthFunc(gl.GREATER)
    elseif value == 'less_equal' then
      gl.Enable(gl.DEPTH_TEST)
      gl.DepthFunc(gl.LEQUAL)
    elseif value == 'greater_equal' then
      gl.Enable(gl.DEPTH_TEST)
      gl.DepthFunc(gl.GEQUAL)
    else
      agen.log.info('application', "FIXME: unsupported depth func")
    end
  elseif name == 'scissor' then
    if value == 'off' then
      gl.Disable(gl.SCISSOR_TEST)
    elseif value == 'on' then
      gl.Enable(gl.SCISSOR_TEST)
    else
      error('unexpected scissor test value [' .. value .. ']')
    end
  else
    error('unknown render state [' .. name .. ']')
  end
end

function RenderState:setBlendMode(mode)
  if mode == 'off' then
    gl.Disable(gl.BLEND)
    return
  end

  gl.Enable(gl.BLEND)

  if mode == 'zero' then
    gl.BlendEquation(gl.FUNC_ADD)
    gl.BlendFunc(gl.ZERO, gl.ONE)
  elseif mode == 'one' then
    gl.BlendEquation(gl.FUNC_ADD)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ZERO)
  elseif mode == 'alpha' then
    gl.BlendEquation(gl.FUNC_ADD)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
  elseif mode == 'add' then
    gl.BlendEquation(gl.FUNC_ADD)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
  elseif mode == 'sub' then
    gl.BlendEquation(gl.FUNC_SUBTRACT)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
  elseif mode == 'revsub' then
    gl.BlendEquation(gl.FUNC_REVERSE_SUBTRACT)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
  elseif mode == 'min' then
    gl.BlendEquation(gl.MIN)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
  elseif mode == 'max' then
    gl.BlendEquation(gl.MAX)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
  else
    error("Unsupported blending mode " .. mode)
  end
end

return RenderState
