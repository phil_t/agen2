local ffi = require('ffi')
local sdl = require('sdl2.lib')
-- macos/x11: openGL dlopen-ed by SDL when creating the window
-- local lib = ffi.C
-- win32 / wayland: load opengl
local lib = ffi.os == 'Linux' and ffi.load('GL') or ffi.os == 'Windows' and ffi.load('opengl32') or ffi.C
ffi.cdef([[
  typedef uint8_t  GLubyte;
  typedef uint32_t GLuint;
  typedef int32_t  GLint;
  typedef int32_t  GLsizei;
  typedef int64_t  GLint64;
  typedef uint64_t GLuint64;
  typedef void GLvoid;
  typedef ptrdiff_t GLsizeiptr;
  typedef ptrdiff_t GLintptr;
  typedef float  GLfloat;
  typedef double GLdouble;
  typedef float  GLclampf;
  typedef int8_t GLchar;
  // 1-byte unsigned
  typedef bool GLboolean;
  // 4-byte unsigned
  typedef enum _GLbitfield {
    DEPTH_BUFFER_BIT   = 0x00000100,
    STENCIL_BUFFER_BIT = 0x00000400,
    COLOR_BUFFER_BIT   = 0x00004000,
    ALL_ATTRIB_BITS    = 0x000FFFFF
  } GLbitfield;

  static const int8_t ZERO = 0x00;
  static const int8_t ONE  = 0x01;

  typedef enum _GLenum {
    NO_ERROR             = 0x0000,

    NEVER                = 0x0200,
    LESS                 = 0x0201,
    EQUAL                = 0x0202,
    LEQUAL               = 0x0203,
    GREATER              = 0x0204,
    NOTEQUAL             = 0x0205,
    GEQUAL               = 0x0206,
    ALWAYS               = 0x0207,

    SRC_COLOR            = 0x0300,
    ONE_MINUS_SRC_COLOR  = 0x0301,
    SRC_ALPHA            = 0x0302,
    ONE_MINUS_SRC_ALPHA  = 0x0303,
    DST_ALPHA            = 0x0304,
    ONE_MINUS_DST_ALPHA  = 0x0305,
    DST_COLOR            = 0x0306,
    ONE_MINUS_DST_COLOR  = 0x0307,
    SRC_ALPHA_SATURATE   = 0x0308,

		FRONT_AND_BACK      = 0x0408,
    // errors
    INVALID_VALUE       = 0x0501,
    INVALID_ENUM        = 0x0500,
    INVALID_OPERATION   = 0x0502,
    STACK_OVERFLOW      = 0x0503,
    STACK_UNDERFLOW     = 0x0504,
    OUT_OF_MEMORY       = 0x0505,

		SHADE_MODEL         = 0x0B54,
    BLEND               = 0x0BE2,
    BLEND_SRC           = 0x0BE1,
    BLEND_DST           = 0x0BE0,
    // depth test
    DEPTH_TEST          = 0x0B71,
    DEPTH_CLEAR_VALUE   = 0x0B73,
    DEPTH_FUNC          = 0x0B74,
    DEPTH_RANGE         = 0x0B70,
    DEPTH_WRITEMASK     = 0x0B72,

    STENCIL_TEST        = 0x0B90,
    VIEWPORT            = 0x0BA2,

    SCISSOR_TEST        = 0x0C11,

    PERSPECTIVE_CORRECTION_HINT = 0x0C50,

    MAX_LIGHTS          = 0x0D31,
    DEPTH_BITS          = 0x0D56,
    DONT_CARE           = 0x1100,
    FASTEST             = 0x1101,
    NICEST              = 0x1102,

    /* data types */
    BYTE                = 0x1400,
    UNSIGNED_BYTE       = 0x1401,
    SHORT               = 0x1402,
    UNSIGNED_SHORT      = 0x1403,
    INT                 = 0x1404,
    UNSIGNED_INT        = 0x1405,
    FLOAT               = 0x1406,
    DOUBLE              = 0x140A,
    UNSIGNED_INT64      = 0x140F,

    COLOR               = 0x1800,
    DEPTH               = 0x1801,
    STENCIL             = 0x1802,

    COLOR_INDEX				  = 0x1900,
    DEPTH_COMPONENT     = 0x1902,
    RED					        = 0x1903,
    GREEN				        = 0x1904,
    BLUE					      = 0x1905,
    ALPHA				        = 0x1906,
    RGB                 = 0x1907,
    RGBA                = 0x1908,

		FLAT                = 0x1D00,
		SMOOTH              = 0x1D01,

    VENDOR              = 0x1F00,
    RENDERER            = 0x1F01,
    VERSION             = 0x1F02,
    EXTENSIONS          = 0x1F03,
    /* textures */
    MODULATE            = 0x2100,
    DECAL               = 0x2101,

		/* Polygons */
		POINT               = 0x1B00,
		LINE                = 0x1B01,
		FILL                = 0x1B02,
		CW                  = 0x0900,
		CCW                 = 0x0901,
		FRONT               = 0x0404,
		BACK                = 0x0405,
		POLYGON_MODE        = 0x0B40,
		POLYGON_STIPPLE     = 0x0B42,
    CULL_FACE           = 0x0B44,
    CULL_FACE_MODE      = 0x0B45,
    FRONT_FACE          = 0x0B46,

		/* Lighting */
		LIGHTING            = 0x0B50,
		LIGHT0              = 0x4000,
		// ... LIGHT0 + 1
		LIGHT7              = 0x4007,
    /* Blending */
    CONSTANT_COLOR       = 0x8001,
    ONE_MINUS_CONSTANT_COLOR = 0x8002,
    CONSTANT_ALPHA       = 0x8003,
    ONE_MINUS_CONSTANT_ALPHA = 0x8004,
		BLEND_COLOR          = 0x8005,
		FUNC_ADD             = 0x8006,
		MIN                  = 0x8007,
		MAX                  = 0x8008,
		BLEND_EQUATION       = 0x8009,
		FUNC_SUBTRACT        = 0x800A,
		FUNC_REVERSE_SUBTRACT = 0x800B,

    RGB2  = 0x804E,
    RGB4  = 0x804F,
    RGB5  = 0x8050,
    RGB8  = 0x8051,
    RGB10 = 0x8052,
    RGB12 = 0x8053,
    RGB16 = 0x8054,
    RGBA2 = 0x8055,
    RGBA4 = 0x8056,
    RGB5_A1  = 0x8057,
    RGBA8    = 0x8058,
    RGB10_A2 = 0x8059,
    RGBA12   = 0x805A,
    RGBA16   = 0x805B,

    UNPACK_SKIP_IMAGES   = 0x806D,
    UNPACK_IMAGE_HEIGHT  = 0x806E,

    MULTISAMPLE          = 0x809D,
    SAMPLE_ALPHA_TO_COVERAGE = 0x809E,
    SAMPLE_ALPHA_TO_ONE      = 0x809F,
    SAMPLE_COVERAGE          = 0x80A0,

    BGR                  = 0x80E0,
    BGRA                 = 0x80E1,

    PARAMETER_BUFFER_BINDING = 0x80EF,

    POINT_SIZE_MIN              = 0x8126,
    POINT_SIZE_MAX              = 0x8127,
    POINT_FADE_THRESHOLD_SIZE   = 0x8128,
    POINT_DISTANCE_ATTENUATION  = 0x8129,
    
    GENERATE_MIPMAP      = 0x8191,
    DEPTH_COMPONENT16    = 0x81A5,
    DEPTH_COMPONENT24    = 0x81A6,
    DEPTH_COMPONENT32    = 0x81A7,
    
    COMPRESSED_RED = 0x8225,
    COMPRESSED_RG  = 0x8226,
    RG     = 0x8227,
    RG_INTEGER = 0x8228,
    R8     = 0x8229,
    R16    = 0x822A,
    RG8    = 0x822B,
    RG16   = 0x822C,
    R16F   = 0x822D,
    R32F   = 0x822E,
    RG16F  = 0x822F,
    RG32F  = 0x8230,
    R8I    = 0x8231,
    R8UI   = 0x8232,
    R16I   = 0x8233,
    R16UI  = 0x8234,
    R32I   = 0x8235,
    R32UI  = 0x8236,
    RG8I   = 0x8237,
    RG8UI  = 0x8238,
    RG16I  = 0x8239,
    RG16UI = 0x823A,
    RG32I  = 0x823B,
    RG32UI = 0x823C,

    VIEWPORT_SUBPIXEL_BITS = 0x825C,
    VIEWPORT_BOUNDS_RANGE  = 0x825D,
    LAYER_PROVOKING_VERTEX = 0x825E,
    VIEWPORT_INDEX_PROVOKING_VERTEX = 0x825F,
    UNDEFINED_VERTEX       = 0x8260,

    DEBUG_SEVERITY_NOTIFICATION = 0x826B,

    COLOR_COMPONENTS       = 0x8283,
    DEPTH_COMPONENTS       = 0x8284,
    STENCIL_COMPONENTS     = 0x8285,

    MAX_RENDERBUFFER_SIZE    = 0x84E8,
    COMPRESSED_ALPHA         = 0x84E9,
    COMPRESSED_LUMINANCE     = 0x84EA,
    COMPRESSED_LUMINANCE_ALPHA = 0x84EB,
    COMPRESSED_INTENSITY     = 0x84EC,
    COMPRESSED_RGB           = 0x84ED,
    COMPRESSED_RGBA          = 0x84EE,
    TEXTURE_COMPRESSION_HINT = 0x84EF,
    UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER    = 0x84F0,
    UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x84F1,
    DEPTH_STENCIL                  = 0x84F9,
    MAX_TEXTURE_LOD_BIAS           = 0x84FD,
    TEXTURE_MAX_ANISOTROPY_EXT     = 0x84FE,
    MAX_TEXTURE_MAX_ANISOTROPY_EXT = 0x84FF,
    TEXTURE_LOD_BIAS               = 0x8501,
    TEXTURE_COMPRESSED_IMAGE_SIZE  = 0x86A0,
    TEXTURE_COMPRESSED             = 0x86A1,
    NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2,
    COMPRESSED_TEXTURE_FORMATS     = 0x86A3,

	  BLEND_EQUATION_ALPHA           = 0x883D,
    POINT_SPRITE                   = 0x8861,
    COORD_REPLACE                  = 0x8862,
	  MAX_VERTEX_ATTRIBS             = 0x8869,
	  VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A,

    GEOMETRY_VERTICES_OUT  = 0x8916,
    GEOMETRY_INPUT_TYPE    = 0x8917,
    GEOMETRY_OUTPUT_TYPE   = 0x8918,

  /* uniform buffers */
  MAX_VERTEX_UNIFORM_BLOCKS                = 0x8A2B,
  MAX_GEOMETRY_UNIFORM_BLOCKS              = 0x8A2C,
  MAX_FRAGMENT_UNIFORM_BLOCKS              = 0x8A2D,
  MAX_COMBINED_UNIFORM_BLOCKS              = 0x8A2E,
  MAX_UNIFORM_BUFFER_BINDINGS              = 0x8A2F,
  MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS   = 0x8A31,
  MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0x8A32,
  MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33,
  UNIFORM_BUFFER_OFFSET_ALIGNMENT          = 0x8A34,
  ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH     = 0x8A35,
  ACTIVE_UNIFORM_BLOCKS                    = 0x8A36,
  UNIFORM_TYPE                             = 0x8A37,
  UNIFORM_SIZE                             = 0x8A38,
  UNIFORM_NAME_LENGTH                      = 0x8A39,
  UNIFORM_BLOCK_INDEX                      = 0x8A3A,
  UNIFORM_OFFSET                           = 0x8A3B,
  UNIFORM_ARRAY_STRIDE                     = 0x8A3C,
  UNIFORM_MATRIX_STRIDE                    = 0x8A3D,
  UNIFORM_IS_ROW_MAJOR                     = 0x8A3E,
  UNIFORM_BLOCK_BINDING                    = 0x8A3F,
  UNIFORM_BLOCK_DATA_SIZE                  = 0x8A40,
  UNIFORM_BLOCK_NAME_LENGTH                = 0x8A41,
  UNIFORM_BLOCK_ACTIVE_UNIFORMS            = 0x8A42,
  UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES     = 0x8A43,
  UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER   = 0x8A44,
  UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER = 0x8A45,
  UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46,
  // framebuffers
  FRAMEBUFFER_BINDING       = 0x8CA6,
  DRAW_FRAMEBUFFER_BINDING  = 0x8CA6,
  RENDERBUFFER_BINDING      = 0x8CA7,
  READ_FRAMEBUFFER          = 0x8CA8,
  DRAW_FRAMEBUFFER          = 0x8CA9,
  READ_FRAMEBUFFER_BINDING  = 0x8CAA,
  RENDERBUFFER_SAMPLES      = 0x8CAB,
  DEPTH_COMPONENT32F        = 0x8CAC,
  DEPTH32F_STENCIL8         = 0x8CAD,

  MAX_SAMPLES             = 0x8D57,

    MAX_GEOMETRY_UNIFORM_COMPONENTS      = 0x8DDF,
    MAX_GEOMETRY_OUTPUT_VERTICES         = 0x8DE0,
    MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0x8DE1,

    PRIMITIVE_RESTART               = 0x8F9D,
    PRIMITIVE_RESTART_INDEX         = 0x8F9E,

    MAX_VERTEX_OUTPUT_COMPONENTS   = 0x9122,
    MAX_GEOMETRY_INPUT_COMPONENTS  = 0x9123,
    MAX_GEOMETRY_OUTPUT_COMPONENTS = 0x9124,
    MAX_FRAGMENT_INPUT_COMPONENTS  = 0x9125,
    PACK_COMPRESSED_BLOCK_WIDTH    = 0x912B,
    PACK_COMPRESSED_BLOCK_HEIGHT   = 0x912C,
    PACK_COMPRESSED_BLOCK_DEPTH    = 0x912D,
    PACK_COMPRESSED_BLOCK_SIZE     = 0x912E,
    TEXTURE_IMMUTABLE_FORMAT       = 0x912F,

    UNIFORM_BUFFER_UNIFIED_NV      = 0x936E,
    UNIFORM_BUFFER_ADDRESS_NV      = 0x936F,
    UNIFORM_BUFFER_LENGTH_NV       = 0x9370,
} GLenum;
GLenum glGetError(void);
GLvoid glGetIntegerv(GLenum mode, GLint* value);
/*
 * from SDL2
 */
void* SDL_GL_GetProcAddress(const char* proc);
void SDL_GL_GetDrawableSize(SDL_Window* window, int* w, int* h);
SDL_Window* SDL_GL_GetCurrentWindow(void);
]])

local glErrors = {
	[tonumber(lib.NO_ERROR)]	        = "",
  [tonumber(lib.INVALID_VALUE)]     = "Argument value invalid",
  [tonumber(lib.INVALID_ENUM)]      = "Argument enum invalid",
  [tonumber(lib.INVALID_OPERATION)] = "Operation not allowed",
  [tonumber(lib.STACK_OVERFLOW)]    = "Stack overflow",
  [tonumber(lib.STACK_UNDERFLOW)]   = "Stack underflow",
  [tonumber(lib.OUT_OF_MEMORY)]     = "Out of memory",
}

local GL = {}
function GL.GetError()
	local err = tonumber(lib.glGetError())
	local msg = glErrors[err]
	if msg == nil then
		error(string.format("Inexpected error code [0x%x]", err))
	end
	return err, msg
end

function GL.import(mt, base, ext)
  agen.log.debug('video', "%s: using %s", base, ext)
  local t = require(string.format('agen.video.GL.%s.%s', base, ext))
  for k, v in pairs(t) do
    if k == 'symtab' and type(v) == 'table' then
      -- import symbols from symtab
      for k, v in pairs(t.symtab) do
        GL[k] = v
      end
    else
      -- import symbols from ext into base
      if mt[k] then
        agen.log.verbose('video', "%s: overriding method [%s]", base, k)
      end
      mt[k] = v
    end
  end
  return mt
end

function GL.getCap(cap)
  assert(cap, "Invalid argument #1, cap cannot be nil")

  local _cap = ffi.new('GLint[1]')
  GL.glGetIntegerv(cap, _cap)
  return _cap[0]
end

GL.ffi = ffi

local GLMT = {
  __index = lib,
  -- bind a new C symbol at run-time (GL extensions)
  __newindex = function(gl, k, v)
    assert(k, "index cannot be nil")
    assert(v, "symbol C-type cannot be nil")
    local sym = 
      sdl.SDL_GL_GetProcAddress('gl' .. k)
      or
      sdl.SDL_GL_GetProcAddress('gl' .. k .. 'ARB')
      or
      nil
		if sym == nil then
      error("Undefined symbol [" .. v .. ' ' .. k .. "]")
    end
    --agen.log.verbose('video', "Resolved symbol [gl%s] at [%s]", k, tostring(sym))
	  rawset(gl, k, ffi.cast(v, sym))
  end,
  __tostring = "Agen2 OpenGL FFI binding v1.0",
}

return setmetatable(GL, GLMT)
