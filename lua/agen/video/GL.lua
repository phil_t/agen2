local GLdevice = require('agen.video.GL.device')
-----------------------------------------------------------
--
-- public API
--
-----------------------------------------------------------
local API = {
  enumerateDevices = function()
    error('not implemented')
  end,
  createDevice = function(win, config)
    return GLdevice.create(win, config)
  end,
}

return API
