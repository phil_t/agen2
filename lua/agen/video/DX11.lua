local ID3D11Device = require('agen.video.DX11.device')

return {
  enumerateDevices = function()
  end,
  createDevice = function(window, config)
    return ID3D11Device.create(window, config)
  end,
}
