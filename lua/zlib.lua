local ffi  = require('ffi')
local lib = ffi.load(ffi.os == 'Windows' and 'zlib1' or 'z')

ffi.cdef([[
/*
 * looks like zlib was developed with 16/32 bit apps in mind, types are not 64-bit firendly:
 * unsinged int  - at least 16 bits
 * unsigned long - at least 32 bits
 */
typedef void* (*alloc_func)(void* opaque, uint32_t items, uint32_t size);
typedef void  (*free_func) (void* opaque, void* address);

typedef struct z_stream_s {
    const uint8_t*  next_in;  /* next input byte */
    unsigned int    avail_in; /* number of bytes available at next_in */
    unsigned long   total_in; /* total number of input bytes read so far */

    uint8_t*        next_out; /* next output byte should be put there */
    unsigned int    avail_out; /* remaining free space at next_out */
    unsigned long   total_out; /* total number of bytes output so far */

    const           char *msg;  /* last error message, NULL if no error */
    void*           state;      /* not visible by applications */

    alloc_func      zalloc;  /* used to allocate the internal state */
    free_func       zfree;   /* used to free the internal state */
    void*           opaque;  /* private data object passed to zalloc and zfree */

    int             data_type;  /* best guess about the data type: binary or text */
    unsigned long   adler;      /* adler32 value of the uncompressed data */
    unsigned long   reserved;   /* reserved for future use */
} z_stream;

const char* zlibVersion(void);
unsigned long zlibCompileFlags(void);
const char* zError(int);
/*
 * inflate
 */
int inflateInit2_(z_stream* strm, int windowBits, const char *version, int stream_size);
int inflateBackInit_(z_stream* strm, int windowBits, unsigned char* window, const char *version, int stream_size);
int inflate(z_stream* strm, int flush);
int inflateEnd(z_stream* strm);
int inflateSetDictionary(z_stream* strm, const uint8_t* dictionary, unsigned int dictLength);
int inflateSync(z_stream* strm);
/*
 * utils
 */                                             
unsigned long compressBound(unsigned long sourceLen);
int compress2 (uint8_t *dest, unsigned long *destLen, const uint8_t *source, unsigned long sourceLen, int level);
int uncompress(uint8_t *dest, unsigned long *destLen, const uint8_t *source, unsigned long sourceLen);
]])

local function zgetError(code)
  assert(code, "zlib error code cannot be nil")
  return ffi.string(lib.zError(code))
end

local zlibStream = {}
local zlibStreamMT = {__index = zlibStream}

function zlibStream.create(bits, version, size)
  local self = ffi.new('z_stream')
  local err = lib.inflateInit2_(self, bits, version, size)
  if err ~= 0 then
    error(zgetError(err))
  end
  return self
end

function zlibStream:sync()
  local err = lib.inflateSync(self)
  if err ~= 0 then
    error(zgetError(err))
  end
end

ffi.metatype('z_stream', zlibStreamMT)
-------------------------------------------------------------------------
--
-- zlib
--
-------------------------------------------------------------------------
local zlib = {}
local zlibMT = {__index = lib}
setmetatable(zlib, zlibMT)

function zlib.getVersion()
  return ffi.string(lib.zlibVersion())
end

function zlib.getError(code)
  return zgetError(code)
end

return zlib
