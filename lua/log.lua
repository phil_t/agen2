local ffi = require("ffi")
local sdl = require("sdl2.log")

local priorities = {
  crit    = sdl.LOG_PRIORITY_CRITICAL,
  error   = sdl.LOG_PRIORITY_ERROR,
  warn    = sdl.LOG_PRIORITY_WARN,
  info    = sdl.LOG_PRIORITY_INFO,
  debug   = sdl.LOG_PRIORITY_DEBUG,
  verbose = sdl.LOG_PRIORITY_VERBOSE,
}

local cats = {
  application = sdl.LOG_CATEGORY_APPLICATION,
  error   = sdl.LOG_CATEGORY_ERROR,
  assert  = sdl.LOG_CATEGORY_ASSERT,
  system  = sdl.LOG_CATEGORY_SYSTEM,
  audio   = sdl.LOG_CATEGORY_AUDIO,
  video   = sdl.LOG_CATEGORY_VIDEO,
  render  = sdl.LOG_CATEGORY_RENDER,
  input   = sdl.LOG_CATEGORY_INPUT,
  test    = sdl.LOG_CATEGORY_TEST
}

local log = {}

--- Similar to "string.format" except that it casts "cdata" objects.
-- @param sz Format string
-- @param ... List of values
-- @return Formatted string
function log.format(sz, ...)
  -- largely untested
  local args = { ... }
  local n = 1
  -- itearate format characters
  for t in string.gmatch(sz, "%%([%a])") do
    -- cast each argument
    local a = args[n]
    if a == nil then
      error("Invalid format argument (nil): #" .. n)
    end
    if type(a) == "cdata" then
      if t == "c" or t == "s" or t == "S" then
        args[n] = ffi.string(a)
      else
        args[n] = tonumber(a)
      end
    end
    n = n + 1
  end
  return string.format(sz, unpack(args))
end

-- internal
local _file = nil
local _filename = nil

--- Opens a log file.
-- @param fn File name or nil
function log.file(fn)
  if _filename == fn then
    return
  end
  if _file then
    _file:close()
    _file = nil
    _filename = nil
  end
  if fn then
    _file = io.open(fn, "w")
    _filename = fn
  end
end

--- Logs a string.
-- @param format Format string
-- @param ... List of values
function log.out(sz, ...)
  local out = log.format(sz, ...)
  sdl.SDL_Log(out)
  if _file then
    _file:write(out)
    _file:write("\n")
    _file:flush()
  end
end

--- Logs a string, given a function and category.
-- @param func Log function. Could be "info", "error", "warn", "debug" or "verbose"
-- @param cat Log category. Could be "application", "error", "assert", "system", "audio", "video", "render", "input" or "test".
-- @param format Format string
-- @param ... List of values
function log.cat(func, cat, sz, ...)
  local c = cats[cat]        or sdl.LOG_CATEGORY_APPLICATION
  local p = priorities[func] or sdl.LOG_PRIORITY_INFO
  local out = log.format(sz, ...)
  sdl.SDL_LogMessage(c, p, out)
  if _file then
    --log._file:write(cat)
    --log._file:write(": ")
    _file:write(func)
    _file:write(": ")
    _file:write(out)
    _file:write("\n")
    _file:flush()
  end
end

function log.info(cat, sz, ...)
  log.cat("info", cat, sz, ...)
end

function log.error(cat, sz, ...)
  log.cat("error", cat, sz, ...)
end

function log.warn(cat, sz, ...)
  log.cat("warn", cat, sz, ...)
end

function log.debug(cat, sz, ...)
  log.cat("debug", cat, sz, ...)
end

function log.verbose(cat, sz, ...)
  log.cat("verbose", cat, sz, ...)
end
--- set logging priority for a specific log category
function log.set_category_priority(cat, pri)
  local c = cats[cat] or sdl.LOG_CATEGORY_APPLICATION
  local p = priorities[pri] or sdl.LOG_PRIORITY_INFO
  sdl.SDL_LogSetPriority(c, p)
end
--- set logging priority for all log categories
function log.set_priority(pri)
  local p = priorities[pri] or sdl.LOG_PRIORITY_INFO
  sdl.SDL_LogSetAllPriority(p)
end

return log
