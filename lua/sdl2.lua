local lib = require('sdl2.lib')

local sdl = {
	log		  = require('sdl2.log'),
	window  = require('sdl2.window'),
	surface = require('sdl2.surface'),
}
setmetatable(sdl, {__index = lib})

return sdl
