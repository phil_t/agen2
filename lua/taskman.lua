local ffi   = require('ffi')
local sdl   = require('sdl2.lib')
local lib   = require('agen.util.mpmc')
local queue = require("agen.util.queue")
local group = require("agen.util.taskgroup")

-- @class Task manager
-- @description Manages system threads on which tasks can be scheduled for execution. 
-- Each thread has its own Lua state that is reused for each task and an internal queue of pending tasks.

local taskman = {}

taskman.pool = nil
taskman.queue = {
  [tonumber(sdl.THREAD_PRIORITY_LOW)   ] = nil,
  [tonumber(sdl.THREAD_PRIORITY_NORMAL)] = nil,
  [tonumber(sdl.THREAD_PRIORITY_HIGH)  ] = nil,
}
taskman.maxthreads = sdl.SDL_GetCPUCount()

--- Starts the task manager.
-- @param mt Number of threads
function taskman.start(mt)
  assert(taskman.pool == nil, "Task manager already started")
  if mt ~= nil and mt > 0 then
    taskman.maxthreads = mt
  end

  taskman.pool = ffi.gc(lib.agen_taskman_ref(), lib.agen_taskman_unref)
  lib.agen_taskman_start(taskman.pool, taskman.maxthreads)
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_LOW)   ] = lib.agen_get_queue(taskman.pool, sdl.THREAD_PRIORITY_LOW)
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_NORMAL)] = lib.agen_get_queue(taskman.pool, sdl.THREAD_PRIORITY_NORMAL)
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_HIGH)  ] = lib.agen_get_queue(taskman.pool, sdl.THREAD_PRIORITY_HIGH)
end

--- Stops the task manager and all running tasks.
function taskman.stop()
  if taskman.pool ~= nil then
    lib.agen_taskman_stop(taskman.pool)
    taskman.pool = nil
  end
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_LOW)   ] = nil
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_NORMAL)] = nil
  taskman.queue[tonumber(sdl.THREAD_PRIORITY_HIGH)  ] = nil
end

--- Checks the status of the task manager.
-- @return True if the task manager is running
function taskman.running()
  return lib.agen_taskman_is_running(taskman.pool)
end

--- Creates a group of tasks that will be executed in parallel.
-- @return Task group object
function taskman.newgroup()
  return group:create()
end

--- Creates a queue used for communication between threads.
-- @param s Size of the queue
-- @return Queue object
function taskman.newqueue(s)
  return queue:create(s)
end

--- Creates a new task.
-- @param p SDL_ThreadPriority
-- @param g Task group object
-- @param f Function name
-- @param q Function f parameter
-- @return Task object
function taskman.newtask(p, g, f, q)
  return g:add(taskman.queue[p], f, q)
end

return taskman
