local ffi = require('ffi')
agen = require('agen')
--- Called before creating the window. 
-- @param arg lua table with command line parameters
-- @return lua table with configuration values (see conf.lua)
function _agen_init(arg)
  local video, audio
  if ffi.os == "OSX" then
    video = 'GL'
    audio = 'AL'
  elseif ffi.os == "Windows" then
    video = 'DX11'
    audio = 'XA2'
  elseif ffi.os == "Linux" then
    video = 'GL'
    audio = 'SDL'
  else
    video = 'GL'
    audio = 'SDL'
  end
  agen.conf.video.driver = video
  agen.conf.audio.driver = audio
  agen.arg = arg
  return agen.conf
end

