---------------------------------------------------------------------------------------------
--
-- SDL_Surface represents 2D image data in system memory (image)
--
---------------------------------------------------------------------------------------------
local ffi = require('ffi')
local bit = require('bit')
local string = require('string')

local sdl = require('sdl2.lib')
local SDL_Rect = require('sdl2.rect')

ffi.cdef([[
typedef enum _SDL_PIXELFORMAT {
  PIXELFORMAT_UNKNOWN   = 0x00000000,
  PIXELFORMAT_INDEX1LSB = 0x11100100,
  PIXELFORMAT_INDEX1MSB = 0x11200100,
  PIXELFORMAT_INDEX8		= 0x13000801,
  // NOTE: A8 doesn't exist in SDL
  PIXELFORMAT_A8        = 0x17000801,
  PIXELFORMAT_RGB332		= 0x14110801,

  PIXELFORMAT_RGB444		= 0x15120c02,
  PIXELFORMAT_RGB555		= 0x15130f02,
  PIXELFORMAT_BGR555		= 0x15530f02,
  // 16 bit
  PIXELFORMAT_ARGB4444	= 0x15321002,
  PIXELFORMAT_RGBA4444	= 0x15421002,
  PIXELFORMAT_ABGR4444	= 0x15721002,
  PIXELFORMAT_BGRA4444	= 0x15821002,
  PIXELFORMAT_ARGB1555	= 0x15331002,
  PIXELFORMAT_RGBA5551	= 0x15441002,
  PIXELFORMAT_ABGR1555	= 0x15731002,
  PIXELFORMAT_BGRA5551	= 0x15841002,
  PIXELFORMAT_RGB565		= 0x15151002,
  PIXELFORMAT_BGR565		= 0x15551002,
  // 24 bit
  PIXELFORMAT_RGB24     = 0x17101803,
  PIXELFORMAT_BGR24     = 0x17401803,
  PIXELFORMAT_RGB888    = 0x16161804,
  PIXELFORMAT_RGBX8888  = 0x16261804,
  PIXELFORMAT_BGR888    = 0x16561804,
  PIXELFORMAT_BGRX8888  = 0x16661804,
  // 32 bit
  PIXELFORMAT_ARGB8888  = 0x16362004,
  PIXELFORMAT_RGBA8888  = 0x16462004,
  PIXELFORMAT_ABGR8888  = 0x16762004,
  PIXELFORMAT_BGRA8888  = 0x16862004,
  PIXELFORMAT_ARGB2101010 = 0x63702004,
  // Depth / stencil formats
  PIXELFORMAT_D32       = 0x16192004,
  PIXELFORMAT_D24S8     = 0x161a2004,
  PIXELFORMAT_D16       = 0x15191002,
  PIXELFORMAT_D8        = 0x14190801,
} SDL_PIXELFORMAT;

typedef struct _SDL_Color {
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 a;
} SDL_Color;

typedef struct _SDL_Palette {
    int ncolors;
    SDL_Color *colors;
    Uint32 version;
    int refcount;
} SDL_Palette;

typedef struct _SDL_BlitMap SDL_BlitMap;

typedef struct _SDL_PixelFormat SDL_PixelFormat;
struct _SDL_PixelFormat {
    SDL_PIXELFORMAT format;
    SDL_Palette *palette;
    Uint8 BitsPerPixel;
    Uint8 BytesPerPixel;
    Uint8 padding[2];
    Uint32 Rmask;
    Uint32 Gmask;
    Uint32 Bmask;
    Uint32 Amask;
    Uint8 Rloss;
    Uint8 Gloss;
    Uint8 Bloss;
    Uint8 Aloss;
    Uint8 Rshift;
    Uint8 Gshift;
    Uint8 Bshift;
    Uint8 Ashift;
    int refcount;
    struct _SDL_PixelFormat *next;
};

struct _SDL_Surface {
    Uint32 flags;               /**< Read-only */
    SDL_PixelFormat *format;    /**< Read-only */
    int w, h;                   /**< Read-only */
    int pitch;                  /**< Read-only */
    uint8_t *pixels;            /**< Read-write */

    /** Application data associated with the surface */
    void *userdata;             /**< Read-write */

    /** information needed for surfaces requiring locks */
    int locked;                 /**< Read-only */
    void *lock_data;            /**< Read-only */

    /** clipping information */
    SDL_Rect clip_rect;         /**< Read-only */

    /** info for fast blit mapping to other surfaces */
    SDL_BlitMap *map;    /**< Private */

    /** Reference count -- used when freeing surface */
    int refcount;               /**< Read-mostly */
};

/** If depth is 4 or 8 bits, an empty palette is allocated for the surface. If depth is greater than 8 bits, the pixel format is set using the [RGBA]mask parameters. **/
SDL_Surface* SDL_CreateRGBSurface(Uint32 flags, int width, int height, int depth,
                                  Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
SDL_Surface* SDL_CreateRGBSurfaceFrom(void *pixels,
                                      int width, int height, int depth, int pitch,
                                      Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
void SDL_FreeSurface(SDL_Surface* surface);
SDL_Surface* SDL_ConvertSurfaceFormat(SDL_Surface* src,
                                      Uint32       pixel_format,
                                      Uint32       flags);
SDL_Surface* SDL_ConvertSurface(SDL_Surface* src, const SDL_PixelFormat* fmt, Uint32 flags);
int SDL_LockSurface(SDL_Surface* surface);
void SDL_UnlockSurface(SDL_Surface* surface);
const char* SDL_GetPixelFormatName(Uint32 format);
]])

local SDL_SurfaceMT = {
	__index = {},
	__tostring = nil,
}

SDL_SurfaceMT.__tostring = function(self)
	return string.format("SDL_Surface [%dx%d] format [%s]", self.w, self.h, 
		ffi.string(sdl.SDL_GetPixelFormatName(self.format.format)))
end
--- Create new surface
SDL_SurfaceMT.__index.create = function(w, h, d, r, g, b, a)
  -- the flags are unused and should be set to 0
	local surf = sdl.SDL_CreateRGBSurface(0, w, h, d, r, g, b, a)
	if surf == nil then
		error(string.format("Failed to create surface: [%s]", sdl.GetError()))
	end
  return ffi.gc(surf, sdl.SDL_FreeSurface)
end
--- Create new surface from existing one with format conversion
SDL_SurfaceMT.__index.createFromSurface = function(surface, destFormat)
  --
  -- we handle A8 format manually
  --
  if destFormat == sdl.PIXELFORMAT_A8 then
    if surface.format.format ~= sdl.PIXELFORMAT_INDEX8 then
      surface = sdl.SDL_ConvertSurfaceFormat(surface, sdl.PIXELFORMAT_INDEX8, 0)
      if surface == nil then
        error(string.format("Failed to convert surface to INDEX8 format: [%s]", sdl.GetError()))
      end
      agen.log.debug('application', "Converted to [%s]", surface:getFormatName())
    end
    return surface:convertToAlpha()
  end

  local surf = sdl.SDL_ConvertSurfaceFormat(surface, destFormat, 0)
  if not surf then
    error(string.format("Failed to create surface: [%s]", sdl.GetError()))
  end
  return ffi.gc(surf, sdl.SDL_FreeSurface)
end
--- Lock surface before accessing pixels directly.
SDL_SurfaceMT.__index.lock = function(self)
  if sdl.SDL_LockSurface(self) ~= 0 then
    error(string.format("Failed to lock surface [%s]", sdl.GetError()))
  end
end

SDL_SurfaceMT.__index.unlock = function(self)
  sdl.SDL_UnlockSurface(self)
end

SDL_SurfaceMT.__index.getFormatName = function(self)
	local name = sdl.SDL_GetPixelFormatName(self.format.format)
  return ffi.string(name, sdl.SDL_strlen(name))
end

SDL_SurfaceMT.__index.toSDLFormat = function(type, order, layout, bits, bytes)
  return bit.bor(bit.lshift(1,      28), 
                 bit.lshift(type,   24), 
                 bit.lshift(order,  20), 
                 bit.lshift(layout, 16), 
                 bit.lshift(bits,    8), 
                 bit.lshift(bytes,   0))
end
     
--- Convert to A8 by replacing the indices with actual 8-bit grey scale because SDL doesn't provide
-- A8 format. Currently only conversion from INDEX8 is supported.
SDL_SurfaceMT.__index.convertToAlpha = function(self) 
  assert(self.format.format == sdl.PIXELFORMAT_INDEX8, 
    string.format("convert to alpha: unsupported source format %s", self:getFormatName()))

  local dest = self.create(self.w, self.h, 8, 0, 0, 0, 0)
  if dest == nil then
    error(string.format("Failed to create surface: %s", sdl.GetError()))
  end
  
  dest:lock()
  self:lock()

  local colors = self.format.palette.colors
  local color  = 0
  local pixels = dest.pixels
  local pixel  = 0

  agen.log.info('applicaton', 'pallete version %d', self.format.palette.version)

  for h = 0, self.h - 1, 1 do
    for w = 0, self.w - 1, 1 do
      color = colors[self.pixels[self.w * h + w]]
      if color.a == 0 then
        pixel = 0x0
      else
        pixel = color.r * 0.2989 + color.g * 0.5870 + color.b * 0.1140
      end
      pixels[dest.w * h + w] = pixel
    end
  end
  
  self:unlock()
  dest:unlock()
  
  dest.format.format = sdl.PIXELFORMAT_A8

  return dest
end

return ffi.metatype('SDL_Surface', SDL_SurfaceMT)
