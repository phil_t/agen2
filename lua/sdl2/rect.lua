local ffi = require('ffi')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef struct _SDL_Point {
    int x;
    int y;
} SDL_Point;

struct _SDL_Rect {
  int x, y;
  int w, h;
};
/**
 *  \brief Determine whether two rectangles intersect.
 *
 *  \return SDL_TRUE if there is an intersection, SDL_FALSE otherwise.
 */
SDL_bool SDL_HasIntersection(const SDL_Rect* A, const SDL_Rect* B);
/**
 *  \brief Calculate the intersection of two rectangles.
 *
 *  \return SDL_TRUE if there is an intersection, SDL_FALSE otherwise.
 */
SDL_bool SDL_IntersectRect(const SDL_Rect* A, const SDL_Rect* B, SDL_Rect* result);
/**
 *  \brief Calculate the union of two rectangles.
 */
void SDL_UnionRect(const SDL_Rect* A, const SDL_Rect* B, SDL_Rect* result);
/**
 *  \brief Calculate a minimal rectangle enclosing a set of points
 *
 *  \return SDL_TRUE if any points were within the clipping rect
 */
SDL_bool SDL_EnclosePoints(const SDL_Point* points, int count, const SDL_Rect* clip, SDL_Rect* result);
/**
 *  \brief Calculate the intersection of a rectangle and line segment.
 *
 *  \return SDL_TRUE if there is an intersection, SDL_FALSE otherwise.
 */
SDL_bool SDL_IntersectRectAndLine(const SDL_Rect* rect, int *X1, int *Y1, int *X2, int *Y2);
]])

local SDL_RectMT = {__index = {}}
SDL_RectMT.__eq = function(r1, r2)
    return r1 and r2 and 
					(r1.x == r2.x) and (r1.y == r2.y) and
          (r1.w == r2.w) and (r1.h == r2.h)
end
--- operator+ returns rect union
SDL_RectMT.__add = function(r1, r2)
	local ret = ffi.new('SDL_Rect')
	sdl.SDL_UnionRect(r1, r2, ret)
	return ret
end

SDL_RectMT.__index.isEmpty = function(self)
	return not self or self.w <= 0 or self.h <= 0
end

SDL_RectMT.__index.doIntersect = sdl.SDL_HasIntersection
SDL_RectMT.__index.intersect = function(r1, r2)
	local ret = ffi.new('SDL_Rect')
	if sdl.SDL_IntersectRect(r1, r2, ret) == sdl.TRUE then
		return ret
	else
		return nil
	end
end

SDL_RectMT.__tostring = function(self)
	return string.format("SDL_Rect %dx%d %dx%d", tonumber(self.x), tonumber(self.y),
																							 tonumber(self.w), tonumber(self.h))
end

return ffi.metatype('SDL_Rect', SDL_RectMT)
