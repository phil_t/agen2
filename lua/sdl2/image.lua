----------------------------------------------------------------
--
-- SDL2_image binding allowing decoding of various image formats
--
----------------------------------------------------------------
local ffi = require('ffi')
local bit = require('bit')
local lib = ffi.os == 'OSX' and ffi.load('SDL2_image.framework/SDL2_image') or ffi.load('SDL2_image')
local sdl = require('sdl2.lib')
local rwops = require('sdl2.rwops')

ffi.cdef([[
typedef enum _IMG_InitFlags {
  IMG_INIT_JPG  = 0x00000001,
  IMG_INIT_PNG  = 0x00000002,
  IMG_INIT_TIF  = 0x00000004,
  IMG_INIT_WEBP = 0x00000008,
  IMG_INIT_JXL  = 0x00000010,
  IMG_INIT_AVIF = 0x00000020
} IMG_InitFlags;

int IMG_Init(IMG_InitFlags flags);
void IMG_Quit(void);

SDL_Surface* IMG_LoadTyped_RW(SDL_RWops *src, int freesrc, const char *type);
SDL_Surface* IMG_Load(const char* file);
SDL_Surface* IMG_Load_RW(SDL_RWops *src, int freesrc);
int IMG_SavePNG(SDL_Surface* surface, const char* file);
]])

do
  local formats = lib.IMG_Init(bit.bor(lib.IMG_INIT_JPG, lib.IMG_INIT_PNG, lib.IMG_INIT_TIF))
  if formats == 0 then
    agen.log.debug('application', "Only BMP format is supported")
  else
	  if bit.band(formats, lib.IMG_INIT_JPG) == 0 then
      agen.log.debug('application', "SDL_image: JPEG decoder dll not found")
	  end
	  
	  if bit.band(formats, lib.IMG_INIT_PNG) == 0 then
      agen.log.debug('application', "SDL_image: PNG decoder dll not found")
	  end
	  
	  if bit.band(formats, lib.IMG_INIT_TIF) == 0 then
      agen.log.debug('application', "SDL_image: TIF decoder dll not found")
	  end
  end
end

local Image = {}

function Image.createFromPath(file)
  assert(file, "Invalid argument: file path cannot be nil")
  local ops = rwops.createFromPath(file, 'rb')
  return Image.createFromRWops(ops, file)
end

function Image.createFromRWops(ops, name)
  assert(ops, "Invalid argument #1, rwops cannot be nil")
  assert(ffi.istype('SDL_RWops', ops), "Invalid argument #1, rwops expected")
  -- GC will free rwops
  local surface = lib.IMG_Load_RW(ops, 0)
  if surface == nil then
		error(string.format("Failed to load [%s]: [%s]", name or 'asset', sdl.GetError()))
  end
  return ffi.gc(surface, sdl.SDL_FreeSurface)
end

return Image
