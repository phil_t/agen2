local ffi = require('ffi')
local string = require('string')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef unsigned long SDL_threadID;
/* Thread local storage ID, 0 is the invalid ID */
typedef unsigned int SDL_TLSID;
typedef void (*TLS_finalizer)(void*);

SDL_threadID SDL_ThreadID(void);
//SDL_threadID SDL_GetThreadID(SDL_Thread* thread);
int SDL_SetThreadPriority(SDL_ThreadPriority priority);

SDL_TLSID SDL_TLSCreate(void);
void* SDL_TLSGet(SDL_TLSID id);
int SDL_TLSSet(SDL_TLSID id, const void *value, TLS_finalizer func);
]])

local Thread = {}

function Thread.createTLS(data, finalizer)
  local id = sdl.SDL_TLSCreate()
  if id == 0 then
    error(string.format("Failed to create thread-local storage: %s", sdl.GetError()))
  end
  if data then
    if sdl.SDL_TLSSet(id, data, finalizer) ~= 0 then
      error(string.format("Failed to set thread-local storage: %s", sdl.GetError()))
    end
  end
  return id
end

function Thread.getTLS(id, ctype)
  return ffi.cast(ctype, sdl.SDL_TLSGet(id))
end

return Thread
