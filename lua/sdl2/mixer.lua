local ffi = require('ffi')
local lib = ffi.os == 'OSX' and ffi.load('SDL2_mixer.framework/SDL2_mixer') or ffi.load('SDL2_mixer')

ffi.cdef([[
typedef enum _MIX_InitFlags {
    MIX_INIT_FLAC        = 0x00000001,
    MIX_INIT_MOD         = 0x00000002,
    MIX_INIT_MODPLUG     = 0x00000004,
    MIX_INIT_MP3         = 0x00000008,
    MIX_INIT_OGG         = 0x00000010,
    MIX_INIT_FLUIDSYNTH  = 0x00000020
} MIX_InitFlags;

/* Loads dynamic libraries and prepares them for use.  Flags should be
   one or more flags from MIX_InitFlags OR'd together.
   It returns the flags successfully initialized, or 0 on failure.
 */
int Mix_Init(int flags);
/* Unloads libraries loaded with Mix_Init */
void Mix_Quit(void);
]])

return lib

