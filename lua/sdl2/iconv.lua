local ffi = require('ffi')
local sdl = require('sdl2.lib')
local string = require('string')

ffi.cdef([[
/* The SDL implementation of iconv() returns these error codes */
typedef enum _SDL_Iconv_Error {
	ICONV_OK     = 0,
	ICONV_ERROR  = -1,
	ICONV_E2BIG  = -2,
	ICONV_EILSEQ = -3,
	ICONV_EINVAL = -4,
} SDL_Iconv_Error;

/* SDL_iconv_* are now always real symbols/types, not macros or inlined. */
typedef struct _SDL_iconv_t SDL_iconv_t;
SDL_iconv_t* SDL_iconv_open(const char *tocode, const char *fromcode);
int SDL_iconv_close(SDL_iconv_t* cd);
/*
function converts the multibyte sequence starting at *inbuf to a multibyte sequence 
starting at *outbuf.  At most  *inbytesleft  bytes, starting at *inbuf, will be read.  
At most *outbytesleft bytes, starting at *outbuf, will be written.
*/
SDL_Iconv_Error SDL_iconv(SDL_iconv_t* cd, const void** inbuf, size_t* inbytesleft, 
																					 char **outbuf,      size_t* outbytesleft);
/**
 *  This function converts a string between encodings in one pass, returning a
 *  string that must be freed with SDL_free() or NULL on error.
 */
char* SDL_iconv_string(const char *tocode, const char *fromcode, 
											 const void *inbuf, size_t inbytesleft);
/*
// UTF-8 to
#define SDL_iconv_utf8_locale(S)    SDL_iconv_string("", "UTF-8", S, SDL_strlen(S)+1)
// UTF-8 to UTF-16
#define SDL_iconv_utf8_ucs2(S)      (Uint16 *)SDL_iconv_string("UCS-2-INTERNAL", "UTF-8", S, SDL_strlen(S)+1)
// UTF-8 to UTF-32
#define SDL_iconv_utf8_ucs4(S)      (Uint32 *)SDL_iconv_string("UCS-4-INTERNAL", "UTF-8", S, SDL_strlen(S)+1)
*/
]])

local SDL_iconvMT = {__index = {}}

SDL_iconvMT.__index.create = function(to, from)
	local self = sdl.SDL_iconv_open(to, from)
	if self == nil then
		error("Failed to create convertor")
	end

	return ffi.gc(self, sdl.SDL_iconv_close)
end
--- converts a string from one encoding to another
-- @param srcString source string, cdata or lua string
-- @param srcStringLen source string length in bytes
-- @return new string, new string length in characters
SDL_iconvMT.__index.convert = function(self, srcString, srcStringLen)
	assert(srcString, "Source string cannot be nil")
	local srcStringAddr = ffi.new('const char*[1]', {[0] = srcString})
	local srcStringLenAddr = ffi.new('size_t[1]', {[0] = srcStringLen})
	local destString = ffi.new('char[?][1]', srcStringLen)
	local destStringLen = ffi.new('size_t[1]', {[0] = srcStringLen})
	local err = sdl.SDL_iconv(self, srcStringAddr, srcStringLenAddr, destString, destStringLen)
	--
	-- TODO: retry with bigger dest buffer
	--
	if err ~= 0 then
		error(string.format("failed to convert string [%d]", tonumber(err)))
	end

	return destString[0], destStringLen[0]
end
--- converts a string between encodings in one pass, returning a string that must be 
-- freed with SDL_free() or NULL on error.
-- @param to destination encoding
-- @param from source encoding
-- @param data source string
-- @param elems source string length in bytes
-- @return new string
SDL_iconvMT.__index.convertStatic = function(to, from, data, bytes)
	local out = sdl.SDL_iconv_string(to, from, data, bytes)
	if out == nil then
		return nil
	end
	return ffi.gc(out, sdl.SDL_free)
end

SDL_iconvMT.__index.UTF16_TO_UTF8 = function(srcString, srcStringLen)
	local srcStringBytes = srcStringLen or (sdl.SDL_wcslen(srcString) * ffi.sizeof('wchar_t'))
	local out = sdl.SDL_iconv_string("UTF-8", "UCS-2-INTERNAL", srcString, srcStringBytes)
	if out == nil then
		error("Failed to convert string")
	end
	-- will copy (include \0)
	local ret = ffi.string(out, sdl.SDL_strlen(out) + 1)
	sdl.SDL_free(out)
	return ret
end

SDL_iconvMT.__index.UTF8_TO_UTF16 = function(luaString)
	local out = sdl.SDL_iconv_string("UCS-2-INTERNAL", "UTF-8", luaString, string.len(luaString))
	if out == nil then
		error(string.format("Failed to convert utf8 string to utf16: [%s]", sdl.GetError()))
	end
	return ffi.gc(out, sdl.SDL_free)
end

return ffi.metatype('SDL_iconv_t', SDL_iconvMT)
