local ffi = require('ffi')

local lib = ffi.load(ffi.os == 'OSX' and '@executable_path/../Frameworks/cimgui.dylib' or 'cimgui')
local sdl = require('sdl2.lib')
local mouse = require('sdl2.input.mouse')
local kbd   = require('sdl2.input.keyboard')

ffi.cdef([[
typedef uint32_t  ImU32;
typedef uint16_t  ImWchar;     // character for keyboard input/display

// Configuration flags stored in io.ConfigFlags. Set by user/application.
typedef enum _ImGuiConfigFlags {
    ImGuiConfigFlags_NavEnableKeyboard      = 0x01,   // Master keyboard navigation enable flag. NewFrame() will automatically fill io.NavInputs[] based on io.KeyDown[].
    ImGuiConfigFlags_NavEnableGamepad       = 0x02,   // Master gamepad navigation enable flag. This is mostly to instruct your imgui back-end to fill io.NavInputs[]. Back-end also needs to set ImGuiBackendFlags_HasGamepad.
    ImGuiConfigFlags_NavEnableSetMousePos   = 0x04,   // Instruct navigation to move the mouse cursor. May be useful on TV/console systems where moving a virtual mouse is awkward. Will update io.MousePos and set io.WantSetMousePos=true. If enabled you MUST honor io.WantSetMousePos requests in your binding, otherwise ImGui will react as if the mouse is jumping around back and forth.
    ImGuiConfigFlags_NavNoCaptureKeyboard   = 0x08,   // Instruct navigation to not set the io.WantCaptureKeyboard flag with io.NavActive is set.
    ImGuiConfigFlags_NoMouse                = 0x10,   // Instruct imgui to clear mouse position/buttons in NewFrame(). This allows ignoring the mouse information back-end
    ImGuiConfigFlags_NoMouseCursorChange    = 0x20,   // Instruct back-end to not alter mouse cursor shape and visibility.
    // User storage (to allow your back-end/engine to communicate to code that may be shared between multiple projects. Those flags are not used by core ImGui)
    ImGuiConfigFlags_IsSRGB                 = 0x200000,  // Application is SRGB-aware.
    ImGuiConfigFlags_IsTouchScreen          = 0x400000,  // Application is using a touch screen instead of a mouse.
} ImGuiConfigFlags;

// Back-end capabilities flags stored in io.BackendFlags. Set by imgui_impl_xxx or custom back-end.
typedef enum _ImGuiBackendFlags {
    ImGuiBackendFlags_HasGamepad            = 1 << 0,   // Back-end has a connected gamepad.
    ImGuiBackendFlags_HasMouseCursors       = 1 << 1,   // Back-end can honor GetMouseCursor() values and change the OS cursor shape.
    ImGuiBackendFlags_HasSetMousePos        = 1 << 2    // Back-end can honor io.WantSetMousePos and reposition the mouse (only used if ImGuiConfigFlags_NavEnableSetMousePos is set).
} ImGuiBackendFlags;

typedef enum _ImGuiKey {
  ImGuiKey_Tab,
  ImGuiKey_LeftArrow,
  ImGuiKey_RightArrow,
  ImGuiKey_UpArrow,
  ImGuiKey_DownArrow,
  ImGuiKey_PageUp,
  ImGuiKey_PageDown,
  ImGuiKey_Home,
  ImGuiKey_End,
  ImGuiKey_Insert,
  ImGuiKey_Delete,
  ImGuiKey_Backspace,
  ImGuiKey_Space,
  ImGuiKey_Enter,
  ImGuiKey_Escape,
  ImGuiKey_A,         // for text edit CTRL+A: select all
  ImGuiKey_C,         // for text edit CTRL+C: copy
  ImGuiKey_V,         // for text edit CTRL+V: paste
  ImGuiKey_X,         // for text edit CTRL+X: cut
  ImGuiKey_Y,         // for text edit CTRL+Y: redo
  ImGuiKey_Z,         // for text edit CTRL+Z: undo
  ImGuiKey_COUNT
} ImGuiKey;

// Read instructions in imgui.cpp for more details. Download PNG/PSD at goo.gl/9LgVZW.
typedef enum _ImGuiNavInput {
    // Gamepad Mapping
    ImGuiNavInput_Activate,      // activate / open / toggle / tweak value       // e.g. Cross  (PS4), A (Xbox), A (Switch), Space (Keyboard)
    ImGuiNavInput_Cancel,        // cancel / close / exit                        // e.g. Circle (PS4), B (Xbox), B (Switch), Escape (Keyboard)
    ImGuiNavInput_Input,         // text input / on-screen keyboard              // e.g. Triang.(PS4), Y (Xbox), X (Switch), Return (Keyboard)
    ImGuiNavInput_Menu,          // tap: toggle menu / hold: focus, move, resize // e.g. Square (PS4), X (Xbox), Y (Switch), Alt (Keyboard)
    ImGuiNavInput_DpadLeft,      // move / tweak / resize window (w/ PadMenu)    // e.g. D-pad Left/Right/Up/Down (Gamepads), Arrow keys (Keyboard)
    ImGuiNavInput_DpadRight,     //
    ImGuiNavInput_DpadUp,        //
    ImGuiNavInput_DpadDown,      //
    ImGuiNavInput_LStickLeft,    // scroll / move window (w/ PadMenu)            // e.g. Left Analog Stick Left/Right/Up/Down
    ImGuiNavInput_LStickRight,   //
    ImGuiNavInput_LStickUp,      //
    ImGuiNavInput_LStickDown,    //
    ImGuiNavInput_FocusPrev,     // next window (w/ PadMenu)                     // e.g. L1 or L2 (PS4), LB or LT (Xbox), L or ZL (Switch)
    ImGuiNavInput_FocusNext,     // prev window (w/ PadMenu)                     // e.g. R1 or R2 (PS4), RB or RT (Xbox), R or ZL (Switch) 
    ImGuiNavInput_TweakSlow,     // slower tweaks                                // e.g. L1 or L2 (PS4), LB or LT (Xbox), L or ZL (Switch)
    ImGuiNavInput_TweakFast,     // faster tweaks                                // e.g. R1 or R2 (PS4), RB or RT (Xbox), R or ZL (Switch)
    // [Internal] Don't use directly! This is used internally to differentiate keyboard from gamepad inputs for behaviors that require to differentiate them.
    // Keyboard behavior that have no corresponding gamepad mapping (e.g. CTRL+TAB) will be directly reading from io.KeyDown[] instead of io.NavInputs[].
    ImGuiNavInput_KeyMenu_,      // toggle menu                                  // = io.KeyAlt
    ImGuiNavInput_KeyLeft_,      // move left                                    // = Arrow keys
    ImGuiNavInput_KeyRight_,     // move right
    ImGuiNavInput_KeyUp_,        // move up
    ImGuiNavInput_KeyDown_,      // move down
} ImGuiNavInput;

static const uint32_t ImGuiNavInput_COUNT = 22;

// Enumeration for PushStyleColor() / PopStyleColor()
typedef enum _ImGuiCol {
  Col_Text,
  Col_TextDisabled,
  Col_WindowBg,              // Background of normal windows
  Col_ChildWindowBg,         // Background of child windows
  Col_PopupBg,               // Background of popups, menus, tooltips windows
  Col_Border,
  Col_BorderShadow,
  Col_FrameBg,               // Background of checkbox, radio button, plot, slider, text input
  Col_FrameBgHovered,
  Col_FrameBgActive,
  Col_TitleBg,
  Col_TitleBgCollapsed,
  Col_TitleBgActive,
  Col_MenuBarBg,
  Col_ScrollbarBg,
  Col_ScrollbarGrab,
  Col_ScrollbarGrabHovered,
  Col_ScrollbarGrabActive,
  Col_ComboBg,
  Col_CheckMark,
  Col_SliderGrab,
  Col_SliderGrabActive,
  Col_Button,
  Col_ButtonHovered,
  Col_ButtonActive,
  Col_Header,
  Col_HeaderHovered,
  Col_HeaderActive,
  Col_Column,
  Col_ColumnHovered,
  Col_ColumnActive,
  Col_ResizeGrip,
  Col_ResizeGripHovered,
  Col_ResizeGripActive,
  Col_CloseButton,
  Col_CloseButtonHovered,
  Col_CloseButtonActive,
  Col_PlotLines,
  Col_PlotLinesHovered,
  Col_PlotHistogram,
  Col_PlotHistogramHovered,
  Col_TextSelectedBg,
  Col_ModalWindowDarkening,  // darken entire screen when a modal window is active
  Col_COUNT,
  Col_32BIT = 0x1fffffff
} ImGuiCol;

// Enumeration for PushStyleVar() / PopStyleVar()
// NB: the enum only refers to fields of ImGuiStyle() which makes sense to be pushed/poped in UI code. Feel free to add others.
typedef enum _ImGuiStyleVar {
  StyleVar_Alpha,               // float
  StyleVar_WindowPadding,       // ImVec2
  StyleVar_WindowRounding,      // float
  StyleVar_WindowMinSize,       // ImVec2
  StyleVar_ChildWindowRounding, // float
  StyleVar_FramePadding,        // ImVec2
  StyleVar_FrameRounding,       // float
  StyleVar_ItemSpacing,         // ImVec2
  StyleVar_ItemInnerSpacing,    // ImVec2
  StyleVar_IndentSpacing,       // float
  StyleVar_GrabMinSize,         // float
  StyleVar_32BIT = 0x1fffffff
} ImGuiStyleVar;

// FIXME-OBSOLETE: Will be replaced by future color/picker api
typedef enum _ImGuiColorEditMode {
  ColorEdit_UserSelect = -2,
  ColorEdit_UserSelectShowButton = -1,
  ColorEdit_RGB = 0,
  ColorEdit_HSV = 1,
  ColorEdit_HEX = 2,
  ColorEdit_32BIT = 0x1fffffff
} ImGuiColorEditMode;

typedef struct _ImVec2 {
  float x, y;
} ImVec2;

typedef struct _ImVec4 {
  float x, y, z, w;
} ImVec4;

typedef struct _ImFontAtlas ImFontAtlas;
typedef struct _ImGuiStyle ImGuiStyle;
typedef struct _ImDrawList ImDrawList;
typedef struct _ImFont ImFont;

typedef struct _ImDrawData {
  bool Valid;        // Only valid after Render() is called and before the next NewFrame() is called.
  ImDrawList** CmdLists;
  int CmdListsCount;
  int TotalVtxCount; // For convenience, sum of all cmd_lists vtx_buffer.Size
  int TotalIdxCount; // For convenience, sum of all cmd_lists idx_buffer.Size
} ImDrawData;

typedef struct _ImGuiIO {
    ImGuiConfigFlags   ConfigFlags;         // = 0                  // See ImGuiConfigFlags_ enum. Set by user/application. Gamepad/keyboard navigation options, etc.
    ImGuiBackendFlags  BackendFlags;        // = 0                  // Set ImGuiBackendFlags_ enum. Set by imgui_impl_xxx files or custom back-end.
    ImVec2        DisplaySize;              // <unset>              // Display size, in pixels. For clamping windows positions.
    float         DeltaTime;                // = 1.0f/60.0f         // Time elapsed since last frame, in seconds.
    float         IniSavingRate;            // = 5.0f               // Maximum time between saving positions/sizes to .ini file, in seconds.
    const char*   IniFilename;              // = "imgui.ini"        // Path to .ini file. NULL to disable .ini saving.
    const char*   LogFilename;              // = "imgui_log.txt"    // Path to .log file (default parameter to ImGui::LogToFile when no file is specified).
    float         MouseDoubleClickTime;     // = 0.30f              // Time for a double-click, in seconds.
    float         MouseDoubleClickMaxDist;  // = 6.0f               // Distance threshold to stay in to validate a double-click, in pixels.
    float         MouseDragThreshold;       // = 6.0f               // Distance threshold before considering we are dragging.
    int           KeyMap[21];               // <unset>              // Map of indices into the KeysDown[512] entries array which represent your "native" keyboard state.
    float         KeyRepeatDelay;           // = 0.250f             // When holding a key/button, time before it starts repeating, in seconds (for buttons in Repeat mode, etc.).
    float         KeyRepeatRate;            // = 0.050f             // When holding a key/button, rate at which it repeats, in seconds.
    void*         UserData;                 // = NULL               // Store your own data for retrieval by callbacks.
    ImFontAtlas*  Fonts;                    // <auto>               // Load and assemble one or more fonts into a single tightly packed texture. Output to Fonts array.
    float         FontGlobalScale;          // = 1.0f               // Global scale all fonts
    bool          FontAllowUserScaling;     // = false              // Allow user scaling text of individual window with CTRL+Wheel.
    ImFont*       FontDefault;              // = NULL               // Font to use on NewFrame(). Use NULL to uses Fonts->Fonts[0].
    ImVec2        DisplayFramebufferScale;  // = (1.0f,1.0f)        // For retina display or other situations where window coordinates are different from framebuffer coordinates. User storage only, presently not used by ImGui.
    ImVec2        DisplayVisibleMin;        // <unset> (0.0f,0.0f)  // If you use DisplaySize as a virtual space larger than your screen, set DisplayVisibleMin/Max to the visible area.
    ImVec2        DisplayVisibleMax;        // <unset> (0.0f,0.0f)  // If the values are the same, we defaults to Min=(0.0f) and Max=DisplaySize
    // Advanced/subtle behaviors
    bool          OptMacOSXBehaviors;       // = defined(__APPLE__) // OS X style: Text editing cursor movement using Alt instead of Ctrl, Shortcuts using Cmd/Super instead of Ctrl, Line/Text Start and End using Cmd+Arrows instead of Home/End, Double click selects by word instead of selecting whole text, Multi-selection in lists uses Cmd/Super instead of Ctrl
    bool          OptCursorBlink;           // = true               // Enable blinking cursor, for users who consider it annoying.
    //------------------------------------------------------------------
    // Settings (User Functions)
    //------------------------------------------------------------------
    // Optional: access OS clipboard
    // (default to use native Win32 clipboard on Windows, otherwise uses a private clipboard. Override to access OS clipboard on other architectures)
    const char* (*GetClipboardTextFn)(void* user_data);
    void        (*SetClipboardTextFn)(void* user_data, const char* text);
    void*       ClipboardUserData;
    // Optional: notify OS Input Method Editor of the screen position of your cursor for text input position (e.g. when using Japanese/Chinese IME in Windows)
    // (default to use native imm32 api on Windows)
    void        (*ImeSetInputScreenPosFn)(int x, int y);
    void*       ImeWindowHandle;            // (Windows) Set this to your HWND to get automatic IME cursor positioning.
//#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
    // [OBSOLETE] Rendering function, will be automatically called in Render(). Please call your rendering function yourself now! You can obtain the ImDrawData* by calling ImGui::GetDrawData() after Render().
    // See example applications if you are unsure of how to implement this.
    void        (*RenderDrawListsFn)(ImDrawData* data);
//#endif
    //------------------------------------------------------------------
    // Input - Fill before calling NewFrame()
    //------------------------------------------------------------------
    ImVec2      MousePos;                       // Mouse position, in pixels. Set to ImVec2(-FLT_MAX,-FLT_MAX) if mouse is unavailable (on another screen, etc.)
    bool        MouseDown[5];                   // Mouse buttons: left, right, middle + extras. ImGui itself mostly only uses left button (BeginPopupContext** are using right button). Others buttons allows us to track if the mouse is being used by your application + available to user as a convenience via IsMouse** API.
    float       MouseWheel;                     // Mouse wheel: 1 unit scrolls about 5 lines text. 
    float       MouseWheelH;                    // Mouse wheel (Horizontal). Most users don't have a mouse with an horizontal wheel, may not be filled by all back-ends.
    bool        MouseDrawCursor;                // Request ImGui to draw a mouse cursor for you (if you are on a platform without a mouse cursor).
    bool        KeyCtrl;                        // Keyboard modifier pressed: Control
    bool        KeyShift;                       // Keyboard modifier pressed: Shift
    bool        KeyAlt;                         // Keyboard modifier pressed: Alt
    bool        KeySuper;                       // Keyboard modifier pressed: Cmd/Super/Windows
    bool        KeysDown[512];                  // Keyboard keys that are pressed (ideally left in the "native" order your engine has access to keyboard keys, so you can use your own defines/enums for keys).
    ImWchar     InputCharacters[16+1];          // List of characters input (translated by user from keypress+keyboard state). Fill using AddInputCharacter() helper.
    float       NavInputs[ImGuiNavInput_COUNT]; // Gamepad inputs (keyboard keys will be auto-mapped and be written here by ImGui::NewFrame, all values will be cleared back to zero in ImGui::EndFrame)
    //------------------------------------------------------------------
    // Output - Retrieve after calling NewFrame()
    //------------------------------------------------------------------
    bool        WantCaptureMouse;           // When io.WantCaptureMouse is true, imgui will use the mouse inputs, do not dispatch them to your main game/application (in both cases, always pass on mouse inputs to imgui). (e.g. unclicked mouse is hovering over an imgui window, widget is active, mouse was clicked over an imgui window, etc.). 
    bool        WantCaptureKeyboard;        // When io.WantCaptureKeyboard is true, imgui will use the keyboard inputs, do not dispatch them to your main game/application (in both cases, always pass keyboard inputs to imgui). (e.g. InputText active, or an imgui window is focused and navigation is enabled, etc.).
    bool        WantTextInput;              // Mobile/console: when io.WantTextInput is true, you may display an on-screen keyboard. This is set by ImGui when it wants textual keyboard input to happen (e.g. when a InputText widget is active).
    bool        WantSetMousePos;            // MousePos has been altered, back-end should reposition mouse on next frame. Set only when ImGuiConfigFlags_NavEnableSetMousePos flag is enabled.
    bool        NavActive;                  // Directional navigation is currently allowed (will handle ImGuiKey_NavXXX events) = a window is focused and it doesn't use the ImGuiWindowFlags_NoNavInputs flag.
    bool        NavVisible;                 // Directional navigation is visible and allowed (will handle ImGuiKey_NavXXX events).
    float       Framerate;                  // Application framerate estimation, in frame per second. Solely for convenience. Rolling average estimation based on IO.DeltaTime over 120 frames
    int         MetricsRenderVertices;      // Vertices output during last call to Render()
    int         MetricsRenderIndices;       // Indices output during last call to Render() = number of triangles * 3
    int         MetricsActiveWindows;       // Number of visible root windows (exclude child windows)
    ImVec2      MouseDelta;                 // Mouse delta. Note that this is zero if either current or previous position are invalid (-FLT_MAX,-FLT_MAX), so a disappearing/reappearing mouse won't have a huge delta.
    //------------------------------------------------------------------
    // [Internal] ImGui will maintain those fields. Forward compatibility not guaranteed!
    //------------------------------------------------------------------
    ImVec2      MousePosPrev;               // Previous mouse position temporary storage (nb: not for public use, set to MousePos in NewFrame())
    ImVec2      MouseClickedPos[5];         // Position at time of clicking
    float       MouseClickedTime[5];        // Time of last click (used to figure out double-click)
    bool        MouseClicked[5];            // Mouse button went from !Down to Down
    bool        MouseDoubleClicked[5];      // Has mouse button been double-clicked?
    bool        MouseReleased[5];           // Mouse button went from Down to !Down
    bool        MouseDownOwned[5];          // Track if button was clicked inside a window. We don't request mouse capture from the application if click started outside ImGui bounds.
    float       MouseDownDuration[5];       // Duration the mouse button has been down (0.0f == just clicked)
    float       MouseDownDurationPrev[5];   // Previous time the mouse button has been down
    ImVec2      MouseDragMaxDistanceAbs[5]; // Maximum distance, absolute, on each axis, of how much mouse has traveled from the clicking point
    float       MouseDragMaxDistanceSqr[5]; // Squared maximum distance of how much mouse has traveled from the clicking point
    float       KeysDownDuration[512];      // Duration the keyboard key has been down (0.0f == just pressed)
    float       KeysDownDurationPrev[512];  // Previous duration the key has been down
    float       NavInputsDownDuration[ImGuiNavInput_COUNT];
    float       NavInputsDownDurationPrev[ImGuiNavInput_COUNT];
} ImGuiIO;

// Internal state access - if you want to share ImGui state between modules (e.g. DLL) or allocate it yourself
typedef struct _ImGuiContext ImGuiContext;
const char* igGetVersion();
ImGuiContext* igCreateContext(ImFontAtlas* shared_font_atlas);
void igDestroyContext(ImGuiContext* ctx);
ImGuiContext* igGetCurrentContext();
void igSetCurrentContext(ImGuiContext* ctx);

ImGuiIO* igGetIO();
ImGuiStyle* igGetStyle();
ImDrawData* igGetDrawData();
void igNewFrame();
void igRender();
void igShowUserGuide();
void igShowStyleEditor(ImGuiStyle* ref);
void igShowDemoWindow(bool* opened);
void igShowMetricsWindow(bool* opened);

void ImGuiIO_AddInputCharacter(unsigned short c);
void ImGuiIO_AddInputCharactersUTF8(const char* utf8_chars);
void ImGuiIO_ClearInputCharacters();

typedef struct _ImFontConfig {
  void*           FontData;                   //          // TTF data
  int             FontDataSize;               //          // TTF data size
  bool            FontDataOwnedByAtlas;       // true     // TTF data ownership taken by the container ImFontAtlas (will delete memory itself). Set to true
  int             FontNo;                     // 0        // Index of font within TTF file
  float           SizePixels;                 //          // Size in pixels for rasterizer
  int             OversampleH, OversampleV;   // 3, 1     // Rasterize at higher quality for sub-pixel positioning. We don't use sub-pixel positions on the Y axis.
  bool            PixelSnapH;                 // false    // Align every character to pixel boundary (if enabled, set OversampleH/V to 1)
  ImVec2          GlyphExtraSpacing;          // 0, 0     // Extra spacing (in pixels) between glyphs
  ImVec2          GlyphOffset;                // 0, 0     // Offset all glyphs from this font input
  const ImWchar*  GlyphRanges;                //          // Pointer to a user-provided list of Unicode range (2 value per range, values are inclusive, zero-terminated list). THE ARRAY DATA NEEDS TO PERSIST AS LONG AS THE FONT IS ALIVE.
  bool            MergeMode;                  // false    // Merge into previous ImFont, so you can combine multiple inputs font into one ImFont (e.g. ASCII font + icons + Japanese glyphs).
  unsigned int    RasterizerFlags;            // 0x00000000 // Settings for custom font rasterizer (e.g. ImGuiFreeType). Leave as zero if you aren't using one.
  float           RasterizerMultiply;         // 1.0f     // Brighten (>1.0f) or darken (<1.0f) font output. Brightening small fonts may be a good workaround to make them more readable.
  // [Internal]
  char            Name[32];                               // Name (strictly for debugging)
  ImFont*         DstFont;
} ImFontConfig;
 
/*
 * ImFontAtlas
 */
void ImFontAtlas_GetTexDataAsRGBA32(ImFontAtlas* atlas, unsigned char** out_pixels, int* out_width, int* out_height, int* out_bytes_per_pixel);
void ImFontAtlas_GetTexDataAsAlpha8(ImFontAtlas* atlas, unsigned char** out_pixels, int* out_width, int* out_height, int* out_bytes_per_pixel);
void ImFontAtlas_SetTexID(ImFontAtlas* atlas, void* tex);
ImFont* ImFontAtlas_AddFont(ImFontAtlas* atlas, const ImFontConfig* font_cfg);
ImFont* ImFontAtlas_AddFontDefault(ImFontAtlas* atlas, const ImFontConfig* font_cfg);
ImFont* ImFontAtlas_AddFontFromFileTTF(ImFontAtlas* atlas, const char* filename, float size_pixels, const ImFontConfig* font_cfg, const ImWchar* glyph_ranges);
ImFont* ImFontAtlas_AddFontFromMemoryTTF(ImFontAtlas* atlas, void* ttf_data, int ttf_size, float size_pixels, const ImFontConfig* font_cfg, const ImWchar* glyph_ranges);
ImFont* ImFontAtlas_AddFontFromMemoryCompressedTTF(ImFontAtlas* atlas, const void* compressed_ttf_data, int compressed_ttf_size, float size_pixels, const ImFontConfig* font_cfg, const ImWchar* glyph_ranges);
ImFont* ImFontAtlas_AddFontFromMemoryCompressedBase85TTF(ImFontAtlas* atlas, const char* compressed_ttf_data_base85, float size_pixels, const ImFontConfig* font_cfg, const ImWchar* glyph_ranges);
void ImFontAtlas_ClearTexData(ImFontAtlas* atlas);
void ImFontAtlas_Clear(ImFontAtlas* atlas);
/*
 * Interleaved vertex
 */
#pragma pack(1);
typedef struct _ImDrawVert {
  ImVec2 pos;
  ImVec2 uv;
  ImU32  col;
} ImDrawVert;
#pragma pack();
/*
 * built with 16-bit indices
 */
typedef uint16_t ImDrawIdx;
/*
 * user data to identify a texture (this is whatever to you 
 * want it to be! read the FAQ about ImTextureID in imgui.cpp)
 */
typedef void* ImTextureID;
/*
 * Draw callbacks for advanced uses.
 * NB- You most likely do NOT need to use draw callbacks just to create your own widget or 
 * customized UI rendering (you can poke into the draw list for that)
 * Draw callback may be useful for example, 
 * A) Change your GPU render state, 
 * B) render a complex 3D scene inside a UI element (without an intermediate texture/render target), 
 * C) etc.
 * The expected behavior from your rendering function is 
 * if (cmd.UserCallback != NULL) cmd.UserCallback(parent_list, cmd); else RenderTriangles()
 */
typedef struct _ImDrawCmd ImDrawCmd;
typedef void (*ImDrawCallback)(const ImDrawList* parent_list, const ImDrawCmd* cmd);
/*
 * Typically, 1 command = 1 gpu draw call (unless command is a callback)
 */
struct _ImDrawCmd {
  unsigned int ElemCount;       // Number of indices (multiple of 3) to be rendered as triangles. Vertices are stored in the callee ImDrawList\'s vtx_buffer[] array, indices in idx_buffer[].
  ImVec4 ClipRect;              // Clipping rectangle (x1, y1, x2, y2)
  ImTextureID TextureId;        // User-provided texture ID. Set by user in ImfontAtlas::SetTexID() for fonts or passed to Image*() functions. Ignore if never using images or multiple fonts atlas.
  ImDrawCallback UserCallback;  // If != NULL, call the function instead of rendering the vertices. clip_rect and texture_id will be set normally.
  void* UserCallbackData;       // The draw callback code can access this.
};

void ImDrawData_ScaleClipRects(ImDrawData* drawData, const ImVec2& sc);
void ImDrawData_DeIndexAllBuffers(ImDrawData* drawData);
/*
 * ImDrawList
 */
int ImDrawList_GetVertexBufferSize(ImDrawList* list);
ImDrawVert* ImDrawList_GetVertexPtr(ImDrawList* list, int n);
int ImDrawList_GetIndexBufferSize(ImDrawList* list);
ImDrawIdx* ImDrawList_GetIndexPtr(ImDrawList* list, int n);
int ImDrawList_GetCmdSize(ImDrawList* list);
ImDrawCmd* ImDrawList_GetCmdPtr(ImDrawList* list, int n);
void ImDrawList_Clear(ImDrawList* list);
void ImDrawList_ClearFreeMemory(ImDrawList* list);
void ImDrawList_PushClipRect(ImDrawList* list, ImVec2 clip_rect_min, ImVec2 clip_rect_max, bool intersect_with_current_clip_rect);
void ImDrawList_PushClipRectFullScreen(ImDrawList* list);
void ImDrawList_PopClipRect(ImDrawList* list);
void ImDrawList_PushTextureID(ImDrawList* list, const ImTextureID texture_id);
void ImDrawList_PopTextureID(ImDrawList* list);
/*
 * Text widget
 */
void igText(const char* fmt, ...);
void igTextV(const char* fmt, va_list args);
void igTextColored(const ImVec4 col, const char* fmt, ...);
void igTextColoredV(const ImVec4 col, const char* fmt, va_list args);
void igTextDisabled(const char* fmt, ...);
void igTextDisabledV(const char* fmt, va_list args);
void igTextWrapped(const char* fmt, ...);
void igTextWrappedV(const char* fmt, va_list args);
void igTextUnformatted(const char* text, const char* text_end);
/*
 * Logging: all text output from interface is redirected to tty/file/clipboard. Tree nodes are automatically opened.
 */
void igLogToTTY(int max_depth);
void igLogToFile(int max_depth, const char* filename);
void igLogToClipboard(int max_depth);
void igLogFinish();
void igLogButtons();
void igLogText(const char* fmt, ...);
// Parameters stacks (shared)
void igPushFont(ImFont* font);
void igPopFont();
void igPushStyleColor(ImGuiCol idx, const ImVec4 col);
void igPopStyleColor(int count);
void igPushStyleVar(ImGuiStyleVar idx, float val);
void igPushStyleVarVec(ImGuiStyleVar idx, const ImVec2 val);
void igPopStyleVar(int count);
ImFont* igGetFont();
float igGetFontSize();
void igGetFontTexUvWhitePixel(ImVec2* pOut);
ImU32 igGetColorU32(ImGuiCol idx, float alpha_mul);
ImU32 igGetColorU32Vec(const ImVec4* col);
// Parameters stacks (current window)
void igPushItemWidth(float item_width);
void igPopItemWidth();
float igCalcItemWidth();
void igPushTextWrapPos(float wrap_pos_x);
void igPopTextWrapPos();
void igPushAllowKeyboardFocus(bool v);
void igPopAllowKeyboardFocus();
void igPushButtonRepeat(bool repeat);
void igPopButtonRepeat();
// Layout
void igSeparator();
void igSameLine(float pos_x, float spacing_w);
void igNewLine();
void igSpacing();
void igDummy(const ImVec2* size);
void igIndent(float indent_w);
void igUnindent(float indent_w);
void igBeginGroup();
void igEndGroup();
void igGetCursorPos(ImVec2* pOut);
float igGetCursorPosX();
float igGetCursorPosY();
void igSetCursorPos(const ImVec2 local_pos);
void igSetCursorPosX(float x);
void igSetCursorPosY(float y);
void igGetCursorStartPos(ImVec2* pOut);
void igGetCursorScreenPos(ImVec2* pOut);
void igSetCursorScreenPos(const ImVec2 pos);
void igAlignFirstTextHeightToWidgets();
float igGetTextLineHeight();
float igGetTextLineHeightWithSpacing();
float igGetItemsLineHeightWithSpacing();
// Widgets
void igText(const char* fmt, ...);
void igTextV(const char* fmt, va_list args);
void igTextColored(const ImVec4 col, const char* fmt, ...);
void igTextColoredV(const ImVec4 col, const char* fmt, va_list args);
void igTextDisabled(const char* fmt, ...);
void igTextDisabledV(const char* fmt, va_list args);
void igTextWrapped(const char* fmt, ...);
void igTextWrappedV(const char* fmt, va_list args);
void igTextUnformatted(const char* text, const char* text_end);
void igLabelText(const char* label, const char* fmt, ...);
void igLabelTextV(const char* label, const char* fmt, va_list args);
void igBullet();
void igBulletText(const char* fmt, ...);
void igBulletTextV(const char* fmt, va_list args);
bool igButton(const char* label, const ImVec2 size);
bool igSmallButton(const char* label);
bool igInvisibleButton(const char* str_id, const ImVec2 size);
void igImage(ImTextureID user_texture_id, const ImVec2 size, const ImVec2 uv0, const ImVec2 uv1, const ImVec4 tint_col, const ImVec4 border_col);
bool igImageButton(ImTextureID user_texture_id, const ImVec2 size, const ImVec2 uv0, const ImVec2 uv1, int frame_padding, const ImVec4 bg_col, const ImVec4 tint_col);
bool igCheckbox(const char* label, bool* v);
bool igCheckboxFlags(const char* label, unsigned int* flags, unsigned int flags_value);
bool igRadioButtonBool(const char* label, bool active);
bool igRadioButton(const char* label, int* v, int v_button);
bool igCombo(const char* label, int* current_item, const char** items, int items_count, int height_in_items);
bool igCombo2(const char* label, int* current_item, const char* items_separated_by_zeros, int height_in_items);
bool igCombo3(const char* label, int* current_item, bool(*items_getter)(void* data, int idx, const char** out_text), void* data, int items_count, int height_in_items);
bool igColorButton(const ImVec4 col, bool small_height, bool outline_border);
bool igColorEdit3(const char* label, float col[3]);
bool igColorEdit4(const char* label, float col[4], bool show_alpha);
void igColorEditMode(ImGuiColorEditMode mode);
void igPlotLines(const char* label, const float* values, int values_count, int values_offset, const char* overlay_text, float scale_min, float scale_max, ImVec2 graph_size, int stride);
void igPlotLines2(const char* label, float(*values_getter)(void* data, int idx), void* data, int values_count, int values_offset, const char* overlay_text, float scale_min, float scale_max, ImVec2 graph_size);
void igPlotHistogram(const char* label, const float* values, int values_count, int values_offset, const char* overlay_text, float scale_min, float scale_max, ImVec2 graph_size, int stride);
void igPlotHistogram2(const char* label, float(*values_getter)(void* data, int idx), void* data, int values_count, int values_offset, const char* overlay_text, float scale_min, float scale_max, ImVec2 graph_size);
void igProgressBar(float fraction, const ImVec2* size_arg, const char* overlay);
]])
-------------------------------------------------------------
--
-- Fonts
--
-------------------------------------------------------------
local ImFontAtlas = {}
local ImFontAtlasMT = {__index = ImFontAtlas}

function ImFontAtlas:getTexDataRGBA32()
  local data = ffi.new('uint8_t*[1]')
  local w = ffi.new('int[1]')
  local h = ffi.new('int[1]')
  local bpp = ffi.new('int[1]')
  lib.ImFontAtlas_GetTexDataAsRGBA32(self, data, w, h, bpp)
  return data[0], w[0], h[0], bpp[0]
end

function ImFontAtlas:getTexDataA8()
  local data = ffi.new('uint8_t*[1]')
  local w = ffi.new('int[1]')
  local h = ffi.new('int[1]')
  local bpp = ffi.new('int[1]')
  lib.ImFontAtlas_GetTexDataAsAlpha8(self, data, w, h, bpp)
  return data[0], w[0], h[0], bpp[0]
end

ffi.metatype('ImFontAtlas', ImFontAtlasMT)
-----------------------------------------------------------------
--
-- ImDrawList
--
-----------------------------------------------------------------
local ImDrawList = {}
local ImDrawListMT = {__index = ImDrawList}
--- get size of vertex buffer in bytes
function ImDrawList:getVtxBufferSize()
  return lib.ImDrawList_GetVertexBufferSize(self)
end
--- get a pointer to the vertices data
-- @param offset optional offset into the data in vertices
-- @return ImDrawVert pointer
function ImDrawList:getVtxBufferPtr(offset)
  return lib.ImDrawList_GetVertexPtr(self, offset or 0)
end
--- get size of index buffer in bytes
function ImDrawList:getIdxBufferSize()
  return lib.ImDrawList_GetIndexBufferSize(self)
end

function ImDrawList:getIdxBufferPtr(offset)
  return lib.ImDrawList_GetIndexPtr(self, offset or 0)
end

function ImDrawList:getCmdSize()
  return lib.ImDrawList_GetCmdSize(self)
end

function ImDrawList:getCmdPtr(index)
  assert(index, 'invalid argument #1, index cannot be nil')
  assert(index >= 0 and index < lib.ImDrawList_GetCmdSize(self),
                'invalid argument #1, index out of range')
  return lib.ImDrawList_GetCmdPtr(self, index)
end

function ImDrawList:clear()
  lib.ImDrawList_Clear(self)
end

ffi.metatype('ImDrawList', ImDrawListMT)
-----------------------------------------------------------------
--
-- ImDrawCmd
--
-----------------------------------------------------------------
local ImDrawData = {}
local ImDrawDataMT = {__index = ImDrawData}

function ImDrawData:scaleClipRects(sc)
  lib.ImDrawData_ScaleClipRects(self, sc)
end

function ImDrawData:deindex()
  lib.ImDrawData_DeIndexAllBuffers(self)
end

function ImDrawData:updateBuffers(vtxBuffer, idxBuffer)
  assert((self.TotalVtxCount * ffi.sizeof('ImDrawVert')) <= #vtxBuffer, 
    "Insufficient vertex buffer size")
  assert((self.TotalIdxCount * ffi.sizeof('ImDrawIdx')) <= #idxBuffer, 
    "Insufficient index buffer size")
  assert(vtxBuffer, 'invalid argument #2, vertex buffer is nil')
  assert(idxBuffer, 'invalid argument #3, index buffer is nil')

  local cmdList
  local vtxBytes, idxBytes

  local vdest = vtxBuffer:map('write_discard')
  local idest = idxBuffer:map('write_discard')

  for i = 0, self.CmdListsCount - 1, 1 do
    cmdList = self.CmdLists[i]
    --
    -- update vertex buffer
    --
    vtxBytes = cmdList:getVtxBufferSize() * ffi.sizeof('ImDrawVert')
    ffi.copy(vdest, cmdList:getVtxBufferPtr(0), vtxBytes)
    vdest = vdest + vtxBytes
    --
    -- update index buffer
    --
    idxBytes = cmdList:getIdxBufferSize() * ffi.sizeof('ImDrawIdx')
    ffi.copy(idest, cmdList:getIdxBufferPtr(0), idxBytes)
    idest = idest + idxBytes
  end

  vtxBuffer:unmap()
  idxBuffer:unmap()
end

ffi.metatype('ImDrawData', ImDrawDataMT)
-----------------------------------------------------------------
--
-- cImGUI
--
-----------------------------------------------------------------
local cimgui = {}
local ctx = nil

function cimgui.init(window)
  assert(window, "Invalid paramter #1, window cannot be nil")
  assert(ffi.istype('SDL_Window', window), "Invalid paramter #1, not SDL_Window")

  ctx = ffi.gc(lib.igCreateContext(nil), lib.igDestroyContext)

  local io = lib.igGetIO()
  --
  -- GUI uses window coordinates
  --
  local w, h = window:getSize()
  io.DisplaySize.x = w
  io.DisplaySize.y = h

  io.DisplayFramebufferScale.x = 1
  io.DisplayFramebufferScale.y = 1

  io.DeltaTime = 0
  io.IniFilename = 'imgui.ini'
  io.LogFilename = 'imgui.log'
  -- don't push rendering on me, agen will poll for it
  io.RenderDrawListsFn = nil
  io.ImeWindowHandle = window:getNativeWindow()
  io.MouseDrawCursor = false

  return io
end

local last = 0
function cimgui.update()
  local now = sdl.SDL_GetTicks()
  local io = lib.igGetIO()
  -- in secs
  io.DeltaTime = (now - last) / 1000
  last = now
  --
  -- update mouse state: in window coords
  --
  local mask, x, y = mouse:getStateWindow()
  io.MousePos.x = x * io.DisplayFramebufferScale.x
  io.MousePos.y = y * io.DisplayFramebufferScale.y

  io.MouseDown[0] = mouse:buttonPressed(mask, sdl.MOUSE_BUTTON_LEFT)
  io.MouseDown[1] = mouse:buttonPressed(mask, sdl.MOUSE_BUTTON_RIGHT)
  io.MouseDown[2] = mouse:buttonPressed(mask, sdl.MOUSE_BUTTON_MIDDLE)
  --
  -- TODO: upate keyboard state
  --
  return io
end

function cimgui.shutdown()
  ctx = nil
  collectgarbage('collect')
end

local cimguiMT = {
  __index = lib,
  __tostring = 'imgui C-binding 1.60',
}
return setmetatable(cimgui, cimguiMT)
