local ffi = require('ffi')
local ttf = ffi.load(ffi.os == 'OSX' and 'SDL2_ttf.framework/SDL2_ttf' or 'SDL2_ttf')
local string = require('string')
local table  = require('table')

local sdl = require('sdl2.lib')
local iconv = require('sdl2.iconv')

ffi.cdef([[
typedef enum _TTF_HINTING {
  HINTING_NORMAL    = 0,
  HINTING_LIGHT     = 1,
  HINTING_MONO      = 2,
  HINTING_NONE      = 3,
} TTF_HINTING;

typedef struct _TTF_Font TTF_Font;

typedef struct _TTF_Glyph_Metrics {
	Uint16	code;
	int			minX, maxX;
	int			minY, maxY;
	int			advance;
} TTF_Glyph_Metrics;

int TTF_Init(void);
void TTF_Quit(void);

TTF_Font* TTF_OpenFont(const char *file, int ptsize);
TTF_Font* TTF_OpenFontIndex(const char *file, int ptsize, long index);
TTF_Font* TTF_OpenFontRW(SDL_RWops *src, int freesrc, int ptsize);
TTF_Font* TTF_OpenFontIndexRW(SDL_RWops *src, int freesrc, int ptsize, long index);
void TTF_CloseFont(TTF_Font *font);

TTF_HINTING TTF_GetFontHinting(const TTF_Font *font);
void TTF_SetFontHinting(TTF_Font *font, TTF_HINTING hinting);

/* Get the total height of the font - usually equal to point size */
int TTF_FontHeight(const TTF_Font *font);

/* Get the offset from the baseline to the top of the font
   This is a positive value, relative to the baseline.
 */
int TTF_FontAscent(const TTF_Font *font);

/* Get the offset from the baseline to the bottom of the font
   This is a negative value, relative to the baseline.
 */
int TTF_FontDescent(const TTF_Font *font);

/* Get the recommended spacing between lines of text for this font */
int TTF_FontLineSkip(const TTF_Font *font);

/* Get/Set whether or not kerning is allowed for this font */
int TTF_GetFontKerning(const TTF_Font *font);
void TTF_SetFontKerning(TTF_Font *font, int allowed);

/* Get the number of faces of the font */
long TTF_FontFaces(const TTF_Font* font);

/* Get the font face attributes, if any */
int TTF_FontFaceIsFixedWidth(const TTF_Font *font);
char* TTF_FontFaceFamilyName(const TTF_Font *font);
char* TTF_FontFaceStyleName(const TTF_Font *font);

/* Check wether a glyph is provided by the font or not */
int TTF_GlyphIsProvided(const TTF_Font* font, Uint16 ch);

/* Get the metrics (dimensions) of a glyph
   To understand what these metrics mean, here is a useful link:
    http://freetype.sourceforge.net/freetype2/docs/tutorial/step2.html
 */
int TTF_GlyphMetrics(TTF_Font* font, Uint16 ch, int* minx, int* maxx, int* miny, int* maxy, int* advance);

/* Create an 8-bit palettized surface and render the given glyph at
   high quality with the given font and colors.  The 0 pixel is background,
   while other pixels have varying degrees of the foreground color.
   The glyph is rendered without any padding or centering in the X
   direction, and aligned normally in the Y direction.
   This function returns the new surface, or NULL if there was an error.
*/
SDL_Surface* TTF_RenderGlyph_Shaded(TTF_Font* font, Uint16 ch, SDL_Color fg, SDL_Color bg);
SDL_Surface* TTF_RenderGlyph_Blended(TTF_Font *font, Uint16 ch, SDL_Color fg);
]])

do
  local err = ttf.TTF_Init()
  if err ~= 0 then
    error(string.format("Failed to initialize SDL2_ttf [%d]", err))
  end
end

local TTFont = {}
local TTFontMT = {
  __index = TTFont,
  __tostring = function(self)
    return string.format("%s - %s - [%d]",
      ffi.string(ttf.TTF_FontFaceFamilyName(self.font)),
      ffi.string(ttf.TTF_FontFaceStyleName(self.font)),
      ttf.TTF_FontHeight(self.font))
  end,
}

--- Point size to load font as. This basically translates to pixel height at 72DPI. 
function TTFont.createFromPath(filename, ptsize)
  assert(filename, "invalid argument #1, filename expected, got nil")
  assert(ptsize and ptsize > 0, "invalid argument #2, positive integer expected")

	local font = ttf.TTF_OpenFont(filename, ptsize)
  if not font then 
    error(string.format("Failed to create font object from [%s] [%s]", filename, sdl.GetError()))
  end

  local self = {
    font     = ffi.gc(font, ttf.TTF_CloseFont),
    size     = ptsize,
    surface  = nil,
		metrics	 = {},
  }
  setmetatable(self, TTFontMT)
  return self
end

function TTFont.createFromRWops(ops, ptsize)
  assert(ops, "Invalid argument #1, rwops cannot be nil")
  assert(ffi.istype('SDL_RWops', ops), "Invalid argument #1, rwops expected")
  assert(ptsize and ptsize > 0, "invalid argument #2, positive integer expected")

	local font = ttf.TTF_OpenFontRW(ops, 0, ptsize)
  if not font then 
    error(string.format("Failed to create font object [%s]", sdl.GetError()))
  end

  local self = {
    font     = ffi.gc(font, ttf.TTF_CloseFont),
    size     = ptsize,
    surface  = nil,
		metrics	 = {},
  }
  setmetatable(self, TTFontMT)
  return self
end
--- Get the total height of the font - usually equal to point size
function TTFont:getHeight()
  return ttf.TTF_FontHeight(self.font)
end

function TTFont:faceFamilyName()
  return ffi.string(ttf.TTF_FontFaceFamilyName(self.font))
end

function TTFont:faceStyleName()
  return ffi.string(ttf.TTF_FontFaceStyleName(self.font))
end
--- render glyphs stored in utf8 string into a surface
-- @param utf8string glyphs string
function TTFont:preloadGlyphs(utf8string, stringLen)
	local len = stringLen or #utf8string
	--
	-- Convert string of glyphs to UTF-16 for freetype
	--
	local utf16string = iconv.convertStatic("UCS-2-INTERNAL", "UTF-8", utf8string, len)
	local glyphs = ffi.cast('Uint16*', utf16string)
	--
	-- render glyphs
	--
	for i = 0, len - 1, 1 do
		if ttf.TTF_GlyphIsProvided(self.font, glyphs[i]) == 0 then
			agen.log.warn('application', "glyph [0x%x] not in the font", glyphs[i])
		end
	end
end
--- render glyphs from a table ot UTF16 codes
-- @param utf16codes character codes table (UTF16)
function TTFont:preloadGlyphsRange(utf16codes)
	--
	-- render glyphs
	--
	for _, code in ipairs(utf16codes) do
		if ttf.TTF_GlyphIsProvided(self.font, code) == 0 then
			agen.log.warn('application', "glyph [0x%x] not in the font", code)
		end
	end
end

return TTFont
