local ffi = require("ffi")
local sdl = require("sdl2.lib")

ffi.cdef([[
typedef enum {
  FIRSTEVENT     = 0x0000,     /**< Unused (do not remove) */
  QUIT           = 0x0100,
  APP_TERMINATING,
  APP_LOWMEMORY,
  APP_WILLENTERBACKGROUND,
  APP_DIDENTERBACKGROUND,
  APP_WILLENTERFOREGROUND,
  APP_DIDENTERFOREGROUND,
  DISPLAYEVENT   = 0x0150,  /**< Display state change */
  WINDOWEVENT    = 0x0200, /**< Window state change */
  SYSWMEVENT,             /**< System specific event */
  KEYDOWN        = 0x0300, /**< Key pressed */
  KEYUP,                  /**< Key released */
  TEXTEDITING,            /**< Keyboard text editing (composition) */
  TEXTINPUT,              /**< Keyboard text input */
  MOUSEMOTION    = 0x0400, /**< Mouse moved */
  MOUSEBUTTONDOWN,        /**< Mouse button pressed */
  MOUSEBUTTONUP,          /**< Mouse button released */
  MOUSEWHEEL,             /**< Mouse wheel motion */
  JOYAXISMOTION  = 0x0600, /**< Joystick axis motion */
  JOYBALLMOTION,          /**< Joystick trackball motion */
  JOYHATMOTION,           /**< Joystick hat position change */
  JOYBUTTONDOWN,          /**< Joystick button pressed */
  JOYBUTTONUP,            /**< Joystick button released */
  JOYDEVICEADDED,         /**< A new joystick has been inserted into the system */
  JOYDEVICEREMOVED,       /**< An opened joystick has been removed */
  CONTROLLERAXISMOTION  = 0x0650, /**< Game controller axis motion */
  CONTROLLERBUTTONDOWN,          /**< Game controller button pressed */
  CONTROLLERBUTTONUP,            /**< Game controller button released */
  CONTROLLERDEVICEADDED,         /**< A new Game controller has been inserted into the system */
  CONTROLLERDEVICEREMOVED,       /**< An opened Game controller has been removed */
  CONTROLLERDEVICEREMAPPED,      /**< The controller mapping was updated */
  FINGERDOWN      = 0x0700,
  FINGERUP,
  FINGERMOTION,
  DOLLARGESTURE   = 0x0800,
  DOLLARRECORD,
  MULTIGESTURE,
  CLIPBOARDUPDATE = 0x0900, /**< The clipboard changed */
  DROPFILE        = 0x1000, /**< The system requests a file open */
  RENDER_TARGETS_RESET = 0x2000, /**< The render targets have been reset */
  /* Events ::SDL_USEREVENT through ::SDL_LASTEVENT are for your use, and should be allocated with SDL_RegisterEvents() */
  USEREVENT    = 0x8000,
  LASTEVENT    = 0xFFFF
} SDL_EventType;

// Uint8
typedef enum {
  WINDOWEVENT_NONE = 0x00, /**< Never used */
  WINDOWEVENT_SHOWN,          /**< Window has been shown */
  WINDOWEVENT_HIDDEN,         /**< Window has been hidden */
  WINDOWEVENT_EXPOSED,        /**< Window has been exposed and should be redrawn */
  WINDOWEVENT_MOVED,          /**< Window has been moved to data1, data2 */
  WINDOWEVENT_RESIZED,        /**< Window has been resized to data1xdata2 */
  WINDOWEVENT_SIZE_CHANGED,   /**< The window size has changed, either as a result of an API call or through the system or user changing the window size. */
  WINDOWEVENT_MINIMIZED,      /**< Window has been minimized */
  WINDOWEVENT_MAXIMIZED,      /**< Window has been maximized */
  WINDOWEVENT_RESTORED,       /**< Window has been restored to normal size and position */
  WINDOWEVENT_ENTER,          /**< Window has gained mouse focus */
  WINDOWEVENT_LEAVE,          /**< Window has lost mouse focus */
  WINDOWEVENT_FOCUS_GAINED,   /**< Window has gained keyboard focus */
  WINDOWEVENT_FOCUS_LOST,     /**< Window has lost keyboard focus */
  WINDOWEVENT_CLOSE,          /**< The window manager requests that the window be closed */
  WINDOWEVENT_TAKE_FOCUS,     /**< Window is being offered a focus (should SetWindowInputFocus() on itself or a subwindow, or ignore) */
  WINDOWEVENT_HIT_TEST        /**< Window had a hit test that wasn't SDL_HITTEST_NORMAL. */
} WindowEventID;

typedef enum _SDL_DisplayEventID {
  DISPLAYEVENT_NONE,          /**< Never used */
  DISPLAYEVENT_ORIENTATION    /**< Display orientation has changed to data1 */
} SDL_DisplayEventID;

typedef enum _SDL_eventaction {
  ADDEVENT,
  PEEKEVENT,
  GETEVENT
} SDL_eventaction;

typedef enum _SDL_Scancode {
  SCANCODE_UNKNOWN = 0,
} SDL_Scancode;

typedef enum _SDL_Keycode {
  KEYCODE_UNKNOWN = 0xffffffff,
} SDL_Keycode;

typedef struct _SDL_Keysym {
    SDL_Scancode scancode;      /**< SDL physical key code - see ::SDL_Scancode for details */
    SDL_Keycode sym;            /**< SDL virtual key code - see ::SDL_Keycode for details */
    Uint16 mod;                 /**< current key modifiers */
    Uint32 unused;
} SDL_Keysym;

typedef struct _SDL_QuitEvent {
    Uint32 type;
    Uint32 timestamp;
} SDL_QuitEvent;

/**
 *  Display state change event data (event.display.*)
 */
typedef struct _SDL_DisplayEvent {
    Uint32 type;        /**< ::SDL_DISPLAYEVENT */
    Uint32 timestamp;   /**< In milliseconds, populated using SDL_GetTicks() */
    Uint32 display;     /**< The associated display index */
    Uint8 event;        /**< ::SDL_DisplayEventID */
    Uint8 padding1;
    Uint8 padding2;
    Uint8 padding3;
    Sint32 data1;       /**< event dependent data */
} SDL_DisplayEvent;

typedef struct _SDL_WindowEvent {
  Uint32 type;
  Uint32 timestamp;
  Uint32 windowID;    /**< The associated window */
  Uint8 event;        /**< ::SDL_WindowEventID */
  Uint8 padding1;
  Uint8 padding2;
  Uint8 padding3;
  Sint32 data1;       /**< event dependent data */
  Sint32 data2;       /**< event dependent data */
} SDL_WindowEvent;

typedef struct _SDL_KeyboardEvent {
    Uint32 type;        /**< ::SDL_KEYDOWN or ::SDL_KEYUP */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with keyboard focus, if any */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    Uint8 repeat;       /**< Non-zero if this is a key repeat */
    Uint8 padding2;
    Uint8 padding3;
    SDL_Keysym keysym;  /**< The key that was pressed or released */
} SDL_KeyboardEvent;

typedef struct _SDL_MouseMotionEvent {
    Uint32 type;        /**< ::SDL_MOUSEMOTION */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Uint32 state;       /**< The current button state */
    Sint32 x;           /**< X coordinate, relative to window */
    Sint32 y;           /**< Y coordinate, relative to window */
    Sint32 xrel;        /**< The relative motion in the X direction */
    Sint32 yrel;        /**< The relative motion in the Y direction */
} SDL_MouseMotionEvent;

typedef struct _SDL_MouseButtonEvent {
    Uint32 type;        /**< ::SDL_MOUSEBUTTONDOWN or ::SDL_MOUSEBUTTONUP */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Uint8 button;       /**< The mouse button index */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    Uint8 clicks;       /**< 1 for single-click, 2 for double-click, etc. */
    Uint8 padding1;
    Sint32 x;           /**< X coordinate, relative to window */
    Sint32 y;           /**< Y coordinate, relative to window */
} SDL_MouseButtonEvent;

typedef struct _SDL_MouseWheelEvent {
    Uint32 type;        /**< ::SDL_MOUSEWHEEL */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Sint32 x;           /**< The amount scrolled horizontally, positive to the right and negative to the left */
    Sint32 y;           /**< The amount scrolled vertically, positive away from the user and negative toward the user */
    Uint32 direction;   /**< Set to one of the SDL_MOUSEWHEEL_* defines. When FLIPPED the values in X and Y will be opposite. Multiply by -1 to change them back */
    float preciseX;     /**< The amount scrolled horizontally, positive to the right and negative to the left, with float precision (added in 2.0.18) */
    float preciseY;     /**< The amount scrolled vertically, positive away from the user and negative toward the user, with float precision (added in 2.0.18) */
    Sint32 mouseX;      /**< X coordinate, relative to window (added in 2.26.0) */
    Sint32 mouseY;      /**< Y coordinate, relative to window (added in 2.26.0) */
} SDL_MouseWheelEvent;

typedef struct _SDL_UserEvent {
    Uint32 type;        /**< ::SDL_USEREVENT through ::SDL_LASTEVENT-1 */
    Uint32 timestamp;
    Uint32 windowID;    /**< The associated window if any */
    Sint32 code;        /**< User defined event code */
    void *data1;        /**< User defined data pointer */
    void *data2;        /**< User defined data pointer */
} SDL_UserEvent;

// TODO: add all events here
typedef union _SDL_Event {
  Uint32 type;
  SDL_QuitEvent quit;
  SDL_DisplayEvent display;
  SDL_WindowEvent window;
  SDL_KeyboardEvent key;
  SDL_MouseMotionEvent motion;
  SDL_MouseButtonEvent button;
  SDL_MouseWheelEvent wheel;
  SDL_UserEvent user;
  Uint8 padding[56];
} SDL_Event;
// events

void SDL_PumpEvents();
Uint8 SDL_EventState(Uint32 type, int state);
SDL_bool SDL_HasEvents(Uint32 minType, Uint32 maxType);
int SDL_PeepEvents(SDL_Event* events, 
           int numevents, 
           SDL_eventaction action, 
           Uint32 minType,
           Uint32 maxType);
int SDL_PushEvent(SDL_Event* event);
void SDL_FlushEvent(Uint32 type);
void SDL_FlushEvents(Uint32 minType, Uint32 maxType);
Uint32 SDL_RegisterEvents(int numevents);
typedef int (__cdecl *SDL_EventFilter)(void* usedata, SDL_Event* e);
void SDL_SetEventFilter(SDL_EventFilter filter, void* userdata);

SDL_Window* SDL_GetWindowFromID(Uint32 id);
]])

-- todo: sdl.EVENT is not very useful from Lua, we could just use hex or a string
-- i'll leave the table for checking if an event exits

local events = {
  [tonumber(sdl.FIRSTEVENT)] = "firstevent",
  [tonumber(sdl.QUIT)] = "quit",
  [tonumber(sdl.APP_TERMINATING)] = "terminating",
  [tonumber(sdl.APP_LOWMEMORY)] = "lowmemory",
  [tonumber(sdl.APP_WILLENTERBACKGROUND)] = "enteringbg",
  [tonumber(sdl.APP_DIDENTERBACKGROUND)] = "enteredbg",
  [tonumber(sdl.APP_WILLENTERFOREGROUND)] = "enteringfg",
  [tonumber(sdl.APP_DIDENTERFOREGROUND)] = "enteredfg",
  [tonumber(sdl.DISPLAYEVENT)] = "displayevent",
  [tonumber(sdl.WINDOWEVENT)] = "winevent",
  [tonumber(sdl.SYSWMEVENT)] = "sysevent",
  [tonumber(sdl.KEYDOWN)] = "keydown",
  [tonumber(sdl.KEYUP)] = "keyup",
  [tonumber(sdl.TEXTEDITING)] = "textedit",
  [tonumber(sdl.TEXTINPUT)] = "textinput",
  [tonumber(sdl.MOUSEMOTION)] = "mousemove",
  [tonumber(sdl.MOUSEBUTTONDOWN)] = "mousedown",
  [tonumber(sdl.MOUSEBUTTONUP)] = "mouseup",
  [tonumber(sdl.MOUSEWHEEL)] = "mousewheel",
  [tonumber(sdl.JOYAXISMOTION)] = "joystickmove",
  [tonumber(sdl.JOYBALLMOTION)] = "joystickbmove",
  [tonumber(sdl.JOYHATMOTION)] = "joystickhmove",
  [tonumber(sdl.JOYBUTTONDOWN)] = "joystickdown",
  [tonumber(sdl.JOYBUTTONUP)] = "joystickup",
  [tonumber(sdl.JOYDEVICEADDED)] = "joystickadd",
  [tonumber(sdl.JOYDEVICEREMOVED)] = "joystickremove",
  [tonumber(sdl.CONTROLLERAXISMOTION)] = "controllermove",
  [tonumber(sdl.CONTROLLERBUTTONDOWN)] = "controllerdown",
  [tonumber(sdl.CONTROLLERBUTTONUP)] = "controllerup",
  [tonumber(sdl.CONTROLLERDEVICEADDED)] = "controlleradd",
  [tonumber(sdl.CONTROLLERDEVICEREMOVED)] = "controllerremove",
  [tonumber(sdl.CONTROLLERDEVICEREMAPPED)] = "controllermap",
  [tonumber(sdl.FINGERDOWN)] = "fingerdown",
  [tonumber(sdl.FINGERUP)] = "fingerup",
  [tonumber(sdl.FINGERMOTION)] = "fingermove",
  [tonumber(sdl.DOLLARGESTURE)] = "dollargesture",
  [tonumber(sdl.DOLLARRECORD)] = "dollarrecord",
  [tonumber(sdl.MULTIGESTURE)] = "multigesture",
  [tonumber(sdl.CLIPBOARDUPDATE)] = "clipboard",
  [tonumber(sdl.DROPFILE)] = "dropfile",
  [tonumber(sdl.RENDER_TARGETS_RESET)] = "renderreset",
  [tonumber(sdl.USEREVENT)] = "userevent",
  [tonumber(sdl.LASTEVENT)] = "lastevent"
}

local winevents =
{
  [tonumber(sdl.WINDOWEVENT_NONE)] = "none",
  [tonumber(sdl.WINDOWEVENT_SHOWN)] = "shown",
  [tonumber(sdl.WINDOWEVENT_HIDDEN)] = "hidden",
  [tonumber(sdl.WINDOWEVENT_EXPOSED)] = "exposed",
  [tonumber(sdl.WINDOWEVENT_MOVED)] = "moved",
  [tonumber(sdl.WINDOWEVENT_RESIZED)] = "resized",
  [tonumber(sdl.WINDOWEVENT_SIZE_CHANGED)] = "sizechanged",
  [tonumber(sdl.WINDOWEVENT_MINIMIZED)] = "minimized",
  [tonumber(sdl.WINDOWEVENT_MAXIMIZED)] = "maximized",
  [tonumber(sdl.WINDOWEVENT_RESTORED)] = "restored",
  [tonumber(sdl.WINDOWEVENT_ENTER)] = "enter",
  [tonumber(sdl.WINDOWEVENT_LEAVE)] = "leave",
  [tonumber(sdl.WINDOWEVENT_FOCUS_GAINED)] = "focusgained",
  [tonumber(sdl.WINDOWEVENT_FOCUS_LOST)] = "focuslost",
  [tonumber(sdl.WINDOWEVENT_CLOSE)] = "close",
  [tonumber(sdl.WINDOWEVENT_TAKE_FOCUS)] = 'focusoffered',
  [tonumber(sdl.WINDOWEVENT_HIT_TEST)] = 'hittestother',
}
---------------------------------------------------------------
--
-- event base class
--
---------------------------------------------------------------
local SDLEvent = {}
local SDLEventMT = {__index = SDLEvent}
--- return event type as string
function SDLEvent:event_type()
  return events[self.type]
end
ffi.metatype('SDL_Event', SDLEventMT)

local SDLDisplayEvent = {}
setmetatable(SDLDisplayEvent, SDLEventMT)
local SDLDisplayEventMT = {__index = SDLDisplayEvent}

ffi.metatype('SDL_DisplayEvent', SDLDisplayEventMT)

local SDLWinEvent = {}
setmetatable(SDLWinEvent, SDLEventMT)
local SDLWinEventMT = {__index = SDLWinEvent}
--- return window event subtype as string
function SDLWinEvent:winevent()
  return winevents[self.event]
end
--- get SDL Window ptr from event
function SDLWinEvent:window()
  return sdl.SDL_GetWindowFromID(self.windowID)
end
ffi.metatype('SDL_WindowEvent', SDLWinEventMT)

local keystate = {
  [0] = 'released',
  [1] = 'pressed',
}

local SDLKeyEvent = {}
setmetatable(SDLKeyEvent, SDLEventMT)
local SDLKeyEventMT = {__index = SDLKeyEvent}

function SDLKeyEvent:window() 
  return sdl.SDL_GetWindowFromID(self.windowID)
end

function SDLKeyEvent:get_state()
  return keystate[self.state]
end

ffi.metatype('SDL_KeyboardEvent', SDLKeyEventMT)

local SDLQuitEvent = {}
setmetatable(SDLQuitEvent, SDLEventMT)
local SDLQuitEventMT = {__index = SDLQuitEvent}
ffi.metatype('SDL_QuitEvent', SDLEventMT)

--ffi.metatype('SDL_UserEvent', SDLEventMT)

local event = {}

--- Adds an event to the event queue.
-- @param e Event type
-- @param p1 First argument
-- @param p2 Second argument
-- @param p3 Third argument
function event.push(e, p1, p2, p3)
  error("Not implemented")
end

--- Removes the latest event from the event queue.
-- @return Event
function event.pop()
  error("Not implemented")
end

--- Clears the event queue.
function event.clear()
  sdl.SDL_FlushEvents(sdl.FIRSTEVENT, sdl.LASTEVENT)
end
--- Returns an iterator for messages in the event queue.
-- @return Iterator function
local e = ffi.new('SDL_Event')
function event.poll()
  return function()
    if sdl.SDL_PeepEvents(e, 1, sdl.GETEVENT, sdl.FIRSTEVENT, sdl.LASTEVENT) > 0 then
      local et = e.type
      -- TODO: add all types or use lookup table
      -- if we want to use "raw" event objects, why not return "e" right away?
      if et == sdl.WINDOWEVENT then
        return e.window
      elseif et == sdl.QUIT then
        return e.quit
      elseif et == sdl.USEREVENT then
        return e.user
      else
        return e
      end
    end
  end
end

local callbacks = {}
local immediate = {}

--- Sets an event callback
-- @param et SDL event type (might be "prettier" if we used strings)
-- @param f Callback function
-- @param i Immediate flag (optional)
function event.setCallback(et, f, i)
	et = tonumber(et)
  callbacks[et] = f
  immediate[et] = i
end

--- Event filter function (internal)
-- @param e event
-- @return 0 if immediate or 1 if not
local function _eventFilter(e)
  local et = tonumber(e.type)
  -- string version:
  -- et = events[et] or winevents[et]
  -- is it an immediate event?
  if immediate[et] then
    -- process handler
    callbacks[et](e)
    -- don't add to queue
    return 0
  end
  -- add to queue for "polling"
  return 1
end

--- Enable or disable the event filter
-- @b Enabled flag
function event.setFilter(b)
  if b == true then
    -- enable the filter if b is true
    if event.filter == nil then
      event.filter = ffi.cast('SDL_EventFilter', _eventFilter)
      sdl.SDL_SetEventFilter(event.filter, nil)
    end
  else
    -- disable the filter in all other cases
    if event.filter then
      event.filter:free()
      event.filter = nil
    end
  end
end

--- Processes all events
-- @return false if a "quit" event was captured or true otherwise
local e = ffi.new('SDL_Event')
function event.process()
  --
	-- pumping happens on the main thread
	--
  sdl.SDL_PumpEvents()

  while sdl.SDL_PeepEvents(e, 1, sdl.GETEVENT, sdl.FIRSTEVENT, sdl.LASTEVENT) > 0 do
    local et = e:event_type()
    -- note: intercept in EventWatcher if needed
    local arg, func = e, nil
    if et == 'quit' then
      return false
    elseif et == 'winevent' then
      arg = e.window
      func = callbacks[e.window.event]
    elseif et == 'keyup' or et == 'keydown' then
      arg = e.key
      func = callbacks[e.type]
    elseif et == 'mousemove' then
      arg = e.motion
      func = callbacks[e.type]
    elseif et == 'mousedown' or et == 'mouseup' then
      arg = e.button
      func = callbacks[e.type]
    elseif et == 'user' then
      arg = e.user
      func = callbacks[e.type]
    else
      func = callbacks[e.type]
    end
    -- does this event have a handler?
    if func then
      func(arg)
    end
  end
  -- todo: should we flush the events at this point?
  return true
end

return event
