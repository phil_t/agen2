local ffi = require('ffi')
local string = require('string')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _SDL_RWops_Type {
  RWOPS_UNKNOWN   = 0x00000000,
  RWOPS_WINFILE   = 1,
  RWOPS_STDFILE   = 2,  /* Stdio file */
  RWOPS_JNIFILE   = 3,  /* Android asset */
  RWOPS_MEMORY    = 4,  /* Memory stream */
  RWOPS_MEMORY_RO = 5,  /* Read-Only memory stream */
} SDL_RWops_Type;

/**
 * This is the read/write operation structure -- very basic.
 */
struct _SDL_RWops {
    Sint64 (*_size)(SDL_RWops* context);
    Sint64 (*_seek)(SDL_RWops* context, Sint64 offset, int whence);
    size_t (*_read)(SDL_RWops* context, void *ptr, size_t size, size_t maxnum);
    size_t (*_write)(SDL_RWops* context, const void *ptr, size_t size, size_t num);
    int (*_close)(SDL_RWops* context);
    Uint32 type;
    union {
      // __ANDROID__
        struct {
            void *fileNameRef;
            void *inputStreamRef;
            void *readableByteChannelRef;
            void *readMethod;
            void *assetFileDescriptorRef;
            long position;
            long size;
            long offset;
            int fd;
        } androidio;
      // _WIN32
        struct {
            SDL_bool append;
            void *h;
            struct {
                void *data;
                size_t size;
                size_t left;
            } buffer;
        } windowsio;
      // STDIO
        struct {
            SDL_bool autoclose;
            void *fp;
        } stdio;
        struct {
            Uint8 *base;
            Uint8 *here;
            Uint8 *stop;
        } mem;
        struct {
            void *data1;
            void *data2;
        } unknown;
    } hidden;
};

/**
 *  \name RWFrom functions
 *
 *  Functions to create SDL_RWops structures from various data streams.
 */
SDL_RWops *SDL_RWFromFile(const char *file, const char *mode);
SDL_RWops *SDL_RWFromFP(void* fp, SDL_bool autoclose);
SDL_RWops *SDL_RWFromMem(void *mem, int size);
SDL_RWops *SDL_RWFromConstMem(const void *mem, int size);

SDL_RWops *SDL_AllocRW(void);
void SDL_FreeRW(SDL_RWops * area);

static const int RW_SEEK_SET = 0;       /**< Seek from the beginning of data */
static const int RW_SEEK_CUR = 1;       /**< Seek relative to current read point */
static const int RW_SEEK_END = 2;       /**< Seek relative to the end of data */

Uint8 SDL_ReadU8(SDL_RWops* src);
Uint16 SDL_ReadLE16(SDL_RWops* src);
Uint16 SDL_ReadBE16(SDL_RWops* src);
Uint32 SDL_ReadLE32(SDL_RWops* src);
Uint32 SDL_ReadBE32(SDL_RWops* src);
Uint64 SDL_ReadLE64(SDL_RWops* src);
Uint64 SDL_ReadBE64(SDL_RWops* src);

size_t SDL_WriteU8(SDL_RWops* dst, Uint8 value);
size_t SDL_WriteLE16(SDL_RWops* dst, Uint16 value);
size_t SDL_WriteBE16(SDL_RWops* dst, Uint16 value);
size_t SDL_WriteLE32(SDL_RWops* dst, Uint32 value);
size_t SDL_WriteBE32(SDL_RWops* dst, Uint32 value);
size_t SDL_WriteLE64(SDL_RWops* dst, Uint64 value);
size_t SDL_WriteBE64(SDL_RWops* dst, Uint64 value);
]])

local RWops = {}

function RWops.createFromPathNoGC(path, mode)
  local self = sdl.SDL_RWFromFile(path, mode)
  if self == nil then
    error(string.format("Failed to create rwops from file [%s]: [%s]", path, sdl.GetError()))
  end
  return self
end

function RWops.createFromPath(path, mode)
  return ffi.gc(RWops.createFromPathNoGC(path, mode), function(ops)
    ops._close(ops)
  end)
end

function RWops:size()
  local ret = self._size(self)
  if ret < 0 then
    error(string.format("Failed to seek in stream: [%s]", sdl.GetError()))
  end
  return ret
end

function RWops:seek(offset, whence)
  local ret = self._seek(self, offset, whence)
  if ret < 0 then
    error(string.format("Failed to seek in stream: [%s]", sdl.GetError()))
  end
  return ret
end

function RWops:tell()
  return self._seek(self, 0, sdl.RW_SEEK_CUR)
end

function RWops:read(ptr, size, n)
  return self._read(self, ptr, size, n)
end

function RWops:readLE16()
  return sdl.SDL_ReadLE16(self)
end

function RWops:readLE32()
  return sdl.SDL_ReadLE32(self)
end

function RWops:write(ptr, size, n)
  return self._write(self, ptr, size, n)
end

function RWops:close()
  if self._close(self) ~= 0 then
    agen.log.warn('application', "Failed to flush stream on close")
  end
end

local RWopsMT = {__index = RWops}
return ffi.metatype('SDL_RWops', RWopsMT)
