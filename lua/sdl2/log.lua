local ffi = require("ffi")
local sdl = require("sdl2.lib")

ffi.cdef([[
  typedef enum _SDL_LogCategory {
    LOG_CATEGORY_APPLICATION = 0x00000000,
    LOG_CATEGORY_ERROR,
    LOG_CATEGORY_ASSERT,
    LOG_CATEGORY_SYSTEM,
    LOG_CATEGORY_AUDIO,
    LOG_CATEGORY_VIDEO,
    LOG_CATEGORY_RENDER,
    LOG_CATEGORY_INPUT,
    LOG_CATEGORY_TEST,
	} SDL_LogCategory;

  typedef enum _SDL_LogPriority {
    LOG_PRIORITY_VERBOSE = 0x00000001,
    LOG_PRIORITY_DEBUG,
    LOG_PRIORITY_INFO,
    LOG_PRIORITY_WARN,
    LOG_PRIORITY_ERROR,
    LOG_PRIORITY_CRITICAL,
    NUM_LOG_PRIORITIES
  } SDL_LogPriority;

  void SDL_LogMessage(SDL_LogCategory category, SDL_LogPriority priority, const char *fmt, ...);
	void SDL_Log(const char* fmt, ...);
	void SDL_LogInfo(SDL_LogCategory category, const char* fmt, ...);
	void SDL_LogError(SDL_LogCategory category, const char* fmt, ...);
	void SDL_LogWarn(SDL_LogCategory category, const char* fmt, ...);
	void SDL_LogDebug(SDL_LogCategory category, const char* fmt, ...);
	void SDL_LogVerbose(SDL_LogCategory category, const char* fmt, ...);

  typedef void (*SDL_LogOutputFunction)(void *userdata, int category, SDL_LogPriority priority, const char *message);
  void SDL_LogSetOutputFunction(SDL_LogOutputFunction callback, void *userdata);
  void SDL_LogSetAllPriority(SDL_LogPriority priority);
  void SDL_LogSetPriority(int category, SDL_LogPriority priority);
]])

return sdl
