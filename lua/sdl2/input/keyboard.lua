local ffi = require('ffi')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _SDL_Keymod {
  KMOD_NONE = 0x0000,
  KMOD_LSHIFT = 0x0001,
  KMOD_RSHIFT = 0x0002,
  KMOD_LCTRL = 0x0040,
  KMOD_RCTRL = 0x0080,
  KMOD_LALT = 0x0100,
  KMOD_RALT = 0x0200,
  KMOD_LGUI = 0x0400,
  KMOD_RGUI = 0x0800,
  KMOD_NUM = 0x1000,
  KMOD_CAPS = 0x2000,
  KMOD_MODE = 0x4000,
  KMOD_RESERVED = 0x8000
} SDL_Keymod;

const Uint8* SDL_GetKeyboardState(int* numkeys);
SDL_Keymod SDL_GetModState(void);
SDL_Keycode SDL_GetKeyFromScancode(SDL_Scancode scancode);
SDL_Scancode SDL_GetScancodeFromKey(SDL_Keycode key);

const char* SDL_GetKeyName(SDL_Keycode key);
const char* SDL_GetScancodeName(SDL_Scancode scancode);

SDL_Keycode SDL_GetKeyFromName(const char* name);
SDL_Scancode SDL_GetScancodeFromName(const char* name);
]])

local SDLKeyboard = {
	state   = nil,
	numKeys = ffi.new('int[1]'),
}

local SDLKeyboardMT = {
	__index = SDLKeyboard,
	__len   = function(self) return self.numKeys[0] end,
}
setmetatable(SDLKeyboard, SDLKeyboardMT)
--- Enable reporting keyboard state changes via keyboard events (for GUI)
function SDLKeyboard:allowEvents(yes, event)
	if yes then
		sdl.SDL_EventState(event, sdl.ENABLE)
	else
		sdl.SDL_EventState(event, sdl.DISABLE)
	end
end
--- Update internal state storage
function SDLKeyboard:updateState()
	self.state = sdl.SDL_GetKeyboardState(self.numKeys)
end
--- Accessor
function SDLKeyboard:getState()
	return self.numKeys[0], self.state
end
--- Key name to lua string
function SDLKeyboard:getKeyName(key)
  return ffi.string(sdl.SDL_GetKeyName(key))
end

return SDLKeyboard
