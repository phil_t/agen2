local ffi = require('ffi')
local bit = require('bit')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _SDL_MOUSE_BUTTON {
  MOUSE_BUTTON_LEFT   = 1,
  MOUSE_BUTTON_MIDDLE = 2,
  MOUSE_BUTTON_RIGHT  = 3,
  MOUSE_BUTTON_X1     = 4,
  MOUSE_BUTTON_X2     = 5,
} SDL_MOUSE_BUTTON;

typedef enum _SDL_SystemCursor {
  SYSTEM_CURSOR_ARROW,     /**< Arrow */
  SYSTEM_CURSOR_IBEAM,     /**< I-beam */
  SYSTEM_CURSOR_WAIT,      /**< Wait */
  SYSTEM_CURSOR_CROSSHAIR, /**< Crosshair */
  SYSTEM_CURSOR_WAITARROW, /**< Small wait cursor (or Wait if not available) */
  SYSTEM_CURSOR_SIZENWSE,  /**< Double arrow pointing northwest and southeast */
  SYSTEM_CURSOR_SIZENESW,  /**< Double arrow pointing northeast and southwest */
  SYSTEM_CURSOR_SIZEWE,    /**< Double arrow pointing west and east */
  SYSTEM_CURSOR_SIZENS,    /**< Double arrow pointing north and south */
  SYSTEM_CURSOR_SIZEALL,   /**< Four pointed arrow pointing north, south, east, and west */
  SYSTEM_CURSOR_NO,        /**< Slashed circle or crossbones */
  SYSTEM_CURSOR_HAND,      /**< Hand */
  NUM_SYSTEM_CURSORS
} SDL_SystemCursor;

SDL_Window* SDL_GetMouseFocus(void);
Uint32 SDL_GetMouseState(int* x, int* y);
Uint32 SDL_GetGlobalMouseState(int* x, int* y);
Uint32 SDL_GetRelativeMouseState(int* x, int* y);
void SDL_WarpMouseInWindow(SDL_Window* window, int x, int y);
int SDL_WarpMouseGlobal(int x, int y);
int SDL_SetRelativeMouseMode(SDL_bool enabled);
int SDL_CaptureMouse(SDL_bool enabled);
SDL_bool SDL_GetRelativeMouseMode(void);

/* Implementation dependent */
typedef struct _SDL_Cursor SDL_Cursor;
SDL_Cursor* SDL_CreateCursor(const Uint8* data, const Uint8* mask,
                             int w, int h, int hot_x, int hot_y);
SDL_Cursor* SDL_CreateColorCursor(SDL_Surface* surface, int hot_x, int hot_y);
SDL_Cursor* SDL_CreateSystemCursor(SDL_SystemCursor id);
void SDL_SetCursor(SDL_Cursor* cursor);
SDL_Cursor* SDL_GetCursor(void);
SDL_Cursor* SDL_GetDefaultCursor(void);
void SDL_FreeCursor(SDL_Cursor* cursor);
int SDL_ShowCursor(int toggle);
]])
--------------------------------------------------------------
--
-- Mouse cursor
--
--------------------------------------------------------------
local SDLMouseCursor = {}
local SDLMouseCursorMT = {__index = SDLMouseCursor}

function SDLMouseCursor.createFromSurface(surf, hotx, hoty)
  return ffi.gc(sdl.SDL_CreateColorCursor(surf, hotx, hoty), sdl.SDL_FreeCursor)
end

function SDLMouseCursor:setActive()
  sdl.SDL_SetCursor(self)
end

function SDLMouseCursor:show(toggle)
  sdl.SDL_ShowCursor(toggle)
end

ffi.metatype('SDL_Cursor', SDLMouseCursorMT)
--------------------------------------------------------------
--
-- Mouse singleton
--
--------------------------------------------------------------
local SDLMouse = {}
local SDLMouseMT = {__index = SDLMouse}
setmetatable(SDLMouse, SDLMouseMT)

local _buttonMasks = {
  [tonumber(sdl.MOUSE_BUTTON_LEFT)]   =  1,
  [tonumber(sdl.MOUSE_BUTTON_MIDDLE)] =  2,
  [tonumber(sdl.MOUSE_BUTTON_RIGHT)]  =  4,
  [tonumber(sdl.MOUSE_BUTTON_X1)]     =  8,
  [tonumber(sdl.MOUSE_BUTTON_X2)]     = 16,
}

local pos = ffi.new('int[2]')
local function _getState(func)
  local buttons = sdl[func](pos, pos + 1)
  return buttons, pos[0], pos[1]
end

--[[
  MOUSEMOTION     /**< Mouse moved */
  MOUSEBUTTONDOWN,/**< Mouse button pressed */
  MOUSEBUTTONUP,  /**< Mouse button released */
  MOUSEWHEEL,     /**< Mouse wheel motion */
 ]]
function SDLMouse:allowEvents(yes, event)
	if yes then
		sdl.SDL_EventState(event, sdl.ENABLE)
	else
		sdl.SDL_EventState(event, sdl.DISABLE)
	end
end
--- in window coordinates
function SDLMouse:getStateWindow()
  return _getState('SDL_GetMouseState')
end
--- in desktop coordinates
function SDLMouse:getStateGlobal()
  return _getState('SDL_GetGlobalMouseState')
end
--- deltas only
function SDLMouse:getStateRelative()
  return _getState('SDL_GetRelativeMouseState')
end

function SDLMouse:buttonPressed(state, button)
  return bit.band(state, _buttonMasks[tonumber(button)]) ~= 0
end

function SDLMouse:isPressed(button)
  local mask = sdl.SDL_GetMouseState(nil, nil)
  return self:buttonPressed(mask, button)
end

return SDLMouse
