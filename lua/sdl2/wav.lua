local string = require('string')
local bit    = require('bit')
local sdl    = require('sdl2.lib')
local rwops  = require('sdl2.rwops')
local ffi    = sdl.ffi

ffi.cdef([[
/** 
 * SDL uses raw Uint8* and Uint32 pair, make it a struct for ffi.gc 
 */
typedef struct _SDL_AudioBuffer {
  Uint8* data[1];
  Uint32 len[1];
  int freq;                   /**< DSP frequency -- samples per second */
  SDL_AudioFormat format;     /**< Audio data format */
  Uint8 channels;             /**< Number of channels: 1 mono, 2 stereo */
  Uint16 samples;             /**< Audio buffer size in samples (power of 2) */
} SDL_AudioBuffer;
/**
 *  This function loads a WAVE from the data source, automatically freeing
 *  that source if \c freesrc is non-zero.  For example, to load a WAVE file,
 *  you could do:
 *  \code
 *      SDL_LoadWAV_RW(SDL_RWFromFile("sample.wav", "rb"), 1, ...);
 *  \endcode
 *
 *  If this function succeeds, it returns the given SDL_AudioSpec,
 *  filled with the audio data format of the wave data, and sets
 *  \c *audio_buf to a malloc()'d buffer containing the audio data,
 *  and sets \c *audio_len to the length of that audio buffer, in bytes.
 *  You need to free the audio buffer with SDL_FreeWAV() when you are
 *  done with it.
 *
 *  This function returns NULL and sets the SDL error message if the
 *  wave file cannot be opened, uses an unknown data format, or is
 *  corrupt.  Currently raw and MS-ADPCM WAVE files are supported.
 */
SDL_AudioSpec* SDL_LoadWAV_RW(SDL_RWops* src,
                              int freesrc,
                              SDL_AudioSpec* spec,
                              Uint8** audio_buf,
                              Uint32* audio_len);
/**
 *  This function frees data previously allocated with SDL_LoadWAV_RW()
 */
void SDL_FreeWAV(Uint8* audio_buf);
]])

local SDL_AudioBuffer = {}
local SDL_AudioBufferMT = {
  __index = SDL_AudioBuffer,
  __len   = function(self)
    return self.len[0]
  end,
  __gc    = function(self)
    if self.len[0] > 0 then
      sdl.SDL_FreeWAV(self.data[0])
      self.data[0] = nil
      self.len[0]  = 0
    end
  end,
}

function SDL_AudioBuffer.createFromPath(path)
  assert(path, "Invalid argument: file path cannot be nil")

  local ops = rwops.createFromPath(path, 'rb')
  return SDL_AudioBuffer.createFromRWops(ops, path)
end

function SDL_AudioBuffer.createFromRWops(ops, name)
  assert(ops, "Invalid argument #1, rwops cannot be nil")
  assert(ffi.istype('SDL_RWops', ops), "Invalid argument #1, rwops expected")

  local self = ffi.new('SDL_AudioBuffer')
  local spec = ffi.new('SDL_AudioSpec')
  if sdl.SDL_LoadWAV_RW(ops, 0, spec, self.data, self.len) == nil then
		error(string.format("Failed to load [%s]: [%s]", name or 'asset', sdl.GetError()))
  end

  self.channels = spec.channels
  self.freq     = spec.freq
  self.format   = spec.format
  self.samples  = spec.samples

  agen.log.info('audio', "Loaded [%s]: channels [%d], sampling rate [%d]Hz, bits per sample [%d]",
    name, self.channels,
          self.freq,
          bit.band(self.format, sdl.AUDIO_MASK_BITSIZE))
  return self
end

function SDL_AudioBuffer:getData()
  return self.data[0]
end

return ffi.metatype('SDL_AudioBuffer', SDL_AudioBufferMT)
