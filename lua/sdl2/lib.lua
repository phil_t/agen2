local ffi = require('ffi')
local lib = ffi.os == 'Windows' and ffi.load('SDL2') or ffi.C

ffi.cdef([[
	typedef uint8_t  Uint8;
  typedef uint16_t Uint16;
	typedef uint32_t Uint32;
	typedef int32_t	 Sint32;
	typedef uint64_t Uint64;
	typedef int64_t  Sint64;

  typedef enum _SDL_InitFlags {
    INIT_AUDIO = 0x00000010,
    INIT_JOYSTICK = 0x00000200,  /**< SDL_INIT_JOYSTICK implies SDL_INIT_EVENTS */
    INIT_HAPTIC = 0x00001000,
    INIT_GAMECONTROLLER = 0x00002000, /**< SDL_INIT_GAMECONTROLLER implies SDL_INIT_JOYSTICK */
  } SDL_InitFlags;

	typedef struct _SDL_Window  SDL_Window;
  typedef struct _SDL_RWops   SDL_RWops;
	typedef struct _SDL_Rect    SDL_Rect;
  typedef struct _SDL_Surface SDL_Surface;

	typedef enum {
		FALSE = 0x00000000,
		TRUE  = 0x00000001
	} SDL_bool;
	
  typedef enum _SDL_EventFilter {
    QUERY   = -1,
    IGNORE  = 0,
    DISABLE = 0,
    ENABLE  = 1
  } SDL_EventFilter;

  typedef enum _SDL_ThreadPriority {
    THREAD_PRIORITY_LOW,
    THREAD_PRIORITY_NORMAL,
    THREAD_PRIORITY_HIGH
  } SDL_ThreadPriority;

  int SDL_InitSubSystem(Uint32 flags);
  void SDL_QuitSubSystem(Uint32 flags);

  const char* SDL_GetError(void);
  // filesystem
  char* SDL_GetPrefPath(const char* org, const char* app);
  char* SDL_GetBasePath(void);
  // stdinc
  void* SDL_malloc(size_t size);
  void SDL_free(void* ptr);
  // utf16 strlen
  size_t SDL_wcslen(const wchar_t* string);
  // utf8 strlen
  size_t SDL_strlen(const char *str);
  // strcmp
  int SDL_strcmp(const char *str1, const char *str2);
  int SDL_strncmp(const char *str1, const char *str2, size_t maxlen);
  int SDL_strcasecmp(const char *str1, const char *str2);
  int SDL_strncasecmp(const char *str1, const char *str2, size_t len);
  // misc
  Uint32 SDL_GetMouseState(int* x, int* y);
  // ticks
  Uint64 SDL_GetPerformanceCounter(void);
  // ticks per sec
  Uint64 SDL_GetPerformanceFrequency(void);
  // msecs
  Uint32 SDL_GetTicks(void);

  void SDL_Delay(Uint32);
  int SDL_GetCPUCount(void);
  SDL_bool SDL_HasSSE2(void);
  SDL_bool SDL_HasAVX(void);
  SDL_bool SDL_HasAVX2(void);

  // audio
  const char* SDL_GetCurrentAudioDriver(void);
  const char* SDL_GetAudioDeviceName(int index, int iscapture);
  int SDL_GetNumAudioDevices(int iscapture);

  typedef Uint16 SDL_AudioFormat;
  static const Uint16 AUDIO_MASK_BITSIZE  = 0x00ff;
  static const Uint16 AUDIO_MASK_DATATYPE = 0x0100;
  static const Uint16 AUDIO_MASK_ENDIAN   = 0x1000;
  static const Uint16 AUDIO_MASK_SIGNED   = 0x8000;
  // 8-bit
  static const SDL_AudioFormat AUDIO_S8     = 0x8008; /**< Signed 8-bit samples */
  static const SDL_AudioFormat AUDIO_U8     = 0x0008; /**< Unsigned 8-bit samples */
  // 16 bit
  static const SDL_AudioFormat AUDIO_S16LSB = 0x8010; /**< Signed 16-bit samples */
  static const SDL_AudioFormat AUDIO_S16MSB = 0x9010; /**< As above, but big-endian byte order */
  static const SDL_AudioFormat AUDIO_U16LSB = 0x0010; /**< Unsigned 16-bit samples */
  static const SDL_AudioFormat AUDIO_U16MSB = 0x1010; /**< As above, but big-endian byte order */
  // 32-bit int
  static const SDL_AudioFormat AUDIO_S32LSB = 0x8020; /**< 32-bit integer samples */
  static const SDL_AudioFormat AUDIO_S32MSB = 0x9020; /**< As above, but big-endian byte order */
  // 32-bit float
  static const SDL_AudioFormat AUDIO_F32LSB = 0x8120; /**< 32-bit floating point samples */
  static const SDL_AudioFormat AUDIO_F32MSB = 0x9120; /**< As above, but big-endian byte order */

  typedef void (__cdecl *SDL_AudioCallback)(void *userdata, Uint8 *stream, int len);
  typedef struct _SDL_AudioSpec {
    int freq;                   /**< DSP frequency -- samples per second */
    SDL_AudioFormat format;     /**< Audio data format */
    Uint8 channels;             /**< Number of channels: 1 mono, 2 stereo */
    Uint8 silence;              /**< Audio buffer silence value (calculated) */
    Uint16 samples;             /**< Audio buffer size in samples (power of 2) */
    Uint16 padding;             /**< Necessary for some compile environments */
    Uint32 size;                /**< Audio buffer size in bytes (calculated) */
    SDL_AudioCallback callback;
    void *userdata;
  } SDL_AudioSpec;

  typedef struct _SDL_AudioCVT SDL_AudioCVT;
  typedef void (__cdecl *SDL_AudioFilter)(struct SDL_AudioCVT* cvt, SDL_AudioFormat format);

#pragma pack(1)
  struct _SDL_AudioCVT {
    int needed;                 /**< Set to 1 if conversion possible */
    SDL_AudioFormat src_format; /**< Source audio format */
    SDL_AudioFormat dst_format; /**< Target audio format */
    double rate_incr;           /**< Rate conversion increment */
    Uint8 *buf;                 /**< Buffer to hold entire audio data */
    int len;                    /**< Length of original audio buffer */
    int len_cvt;                /**< Length of converted audio buffer */
    int len_mult;               /**< buffer must be len*len_mult big */
    double len_ratio;           /**< Given len, final size is len*len_ratio */
    SDL_AudioFilter filters[10];        /**< Filter list */
    int filter_index;           /**< Current audio conversion function */
  };
#pragma pack()

  int SDL_BuildAudioCVT(SDL_AudioCVT* cvt,
                        SDL_AudioFormat src_format,
                        Uint8 src_channels,
                        int src_rate,
                        SDL_AudioFormat dst_format,
                        Uint8 dst_channels,
                        int dst_rate);
  /**
   *  Once you have initialized the \c cvt structure using SDL_BuildAudioCVT(),
   *  created an audio buffer \c cvt->buf, and filled it with \c cvt->len bytes of
   *  audio data in the source format, this function will convert it in-place
   *  to the desired format.
   *
   *  The data conversion may expand the size of the audio data, so the buffer
   *  \c cvt->buf should be allocated after the \c cvt structure is initialized by
   *  SDL_BuildAudioCVT(), and should be \c cvt->len*cvt->len_mult bytes long.
   */
  int SDL_ConvertAudio(SDL_AudioCVT* cvt);

  // misc
  Uint32 SDL_MasksToPixelFormatEnum(int bpp, Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
  SDL_bool SDL_PixelFormatEnumToMasks(Uint32 format, int* bpp, Uint32* Rmask, Uint32* Gmask, Uint32* Bmask, Uint32* Amask);
]])

local sdl = {}

sdl.ffi = ffi
sdl.GetError = function()
  return ffi.string(lib.SDL_GetError()) 
end

--- Get private directory where application is allowed to write
-- @param org Company name (string)
-- @param app App name (sintrg)
-- @return path (lua string)
sdl.GetPrivatePath = function(org, app)
  local ptr = lib.SDL_GetPrefPath(org, app)
  if ptr == nil then
    -- not implemented for this platform?
    error("SDL_GetPrefPath:" .. sdl.GetError())
  end
  -- will copy to lua string
  local ret = ffi.string(ptr, lib.SDL_strlen(ptr))
  -- caller needs to free ptr
  lib.SDL_free(ptr)
  return ret
end

--- Get executable directory
sdl.GetBasePath = function()
  local ptr = lib.SDL_GetBasePath()
  if ptr == nil then
    error("SDL_GetBasePath:" .. sdl.GetError())
  end
  -- copy to lua string
  local ret = ffi.string(ptr, lib.SDL_strlen(ptr))
  lib.SDL_free(ptr)
  return ret
end

local sdlMT = {__index = lib}
return setmetatable(sdl, sdlMT)
