local ffi = require('ffi')
local bit = require('bit')
local string = require('string')
local sdl = require('sdl2.lib')

ffi.cdef([[
typedef enum _SDL_WindowFlags {
  WINDOW_FULLSCREEN = 0x00000001,         /**< fullscreen window */
  WINDOW_OPENGL = 0x00000002,             /**< window usable with OpenGL context */
  WINDOW_SHOWN = 0x00000004,              /**< window is visible */
  WINDOW_HIDDEN = 0x00000008,             /**< window is not visible */
  WINDOW_BORDERLESS = 0x00000010,         /**< no window decoration */
  WINDOW_RESIZABLE = 0x00000020,          /**< window can be resized */
  WINDOW_MINIMIZED = 0x00000040,          /**< window is minimized */
  WINDOW_MAXIMIZED = 0x00000080,          /**< window is maximized */
  WINDOW_INPUT_GRABBED = 0x00000100,      /**< window has grabbed input focus */
  WINDOW_INPUT_FOCUS = 0x00000200,        /**< window has input focus */
  WINDOW_MOUSE_FOCUS = 0x00000400,        /**< window has mouse focus */
  WINDOW_FOREIGN = 0x00000800,            /**< window not created by SDL */
  WINDOW_FULLSCREEN_DESKTOP = 0x00001001,
  WINDOW_ALLOW_HIGHDPI = 0x00002000,      /**< window should be created in high-DPI mode if supported */
  WINDOW_MOUSE_CAPTURE = 0x00004000,      /**< window has mouse captured (unrelated to INPUT_GRABBED) */
  WINDOW_ALWAYS_ON_TOP = 0x00008000,      /**< window should always be above others */
  WINDOW_SKIP_TASKBAR  = 0x00010000,      /**< window should not be added to the taskbar */
  WINDOW_UTILITY       = 0x00020000,      /**< window should be treated as a utility window */
  WINDOW_TOOLTIP       = 0x00040000,      /**< window should be treated as a tooltip */
  WINDOW_POPUP_MENU    = 0x00080000,      /**< window should be treated as a popup menu */
  WINDOW_VULKAN        = 0x10000000       /**< window usable for Vulkan surface */
} SDL_WindowFlags;

typedef enum _SDL_SYSWM_TYPE {
	SYSWM_UNKNOWN = 0x00000000,
	SYSWM_WINDOWS,
	SYSWM_X11,
	SYSWM_DIRECTFB,
	SYSWM_COCOA,
	SYSWM_UIKIT,
	SYSWM_WAYLAND,
	SYSWM_MIR,
	SYSWM_WINRT,
	SYSWM_ANDROID,
  SYSWM_VIVANTE,
  SYSWM_OS2
} SDL_SYSWM_TYPE;

#pragma pack(1)
typedef struct {
	Uint8 major;
	Uint8 minor;
	Uint8 patch;
} SDL_version;
#pragma pack()

typedef struct _SDL_SysWMinfo {
  SDL_version version;
  SDL_SYSWM_TYPE subsystem;
  union {
    struct {
      // HWND
      void* window;
      void* hdc;
      void* hinstance;
    } win;
    struct {
      void* window; // IInspectable
    } winrt;
    struct {
      // NSWindow
      void* window;
    } cocoa;
    struct {
      void* window; // UIWindow
      uint32_t framebuffer; /**< The GL view's Framebuffer Object. It must be bound when rendering to the screen using GL. */
      uint32_t colorbuffer; /**< The GL view's color Renderbuffer Object. It must be bound when SDL_GL_SwapWindow is called. */
      uint32_t resolveFramebuffer; /**< The Framebuffer Object which holds the resolve color Renderbuffer, when MSAA is used. */
    } uikit;
    struct {
    	void* display; // Display - the X11 display
      void* window;  // Window  - the X11 window
    } x11;
    struct {
    	void /*struct wl_display*/ 	*display;          /**< Wayland display */
    	void /*struct wl_surface*/ 	*surface;          /**< Wayland surface */
    	void 				*shell_surface;    /**< DEPRECATED Wayland shell_surface (window manager handle) */
    	void /*struct wl_egl_window*/ 	*egl_window;       /**< Wayland EGL window (native window) */
    	void /*struct xdg_surface*/ 	*xdg_surface;      /**< Wayland xdg surface (window manager handle) */
    	void /*struct xdg_toplevel*/	*xdg_toplevel;     /**< Wayland xdg toplevel role */
    } wl;
    struct {
      void* window;   // ANativeWindow*
      void* surface;  // EGLSurface surface;
    } android;
    Uint8 dummy[64];
  } info;
} SDL_SysWMinfo;

void SDL_GetVersion(SDL_version* ver);

SDL_Window* SDL_CreateWindow(const char *title, int x, int y, int w, int h, Uint32 flags);
SDL_Window* SDL_GetWindowFromID(Uint32 id);
void SDL_DestroyWindow(SDL_Window* window);
void SDL_SetWindowTitle(SDL_Window* window, const char* title);
const char* SDL_GetWindowTitle(SDL_Window* window);
int SDL_SetWindowFullscreen(SDL_Window* window, Uint32 flags);
void SDL_ShowWindow(SDL_Window* window);
void SDL_HideWindow(SDL_Window* window);

Uint32 SDL_GetWindowPixelFormat(SDL_Window* window);
void SDL_GetWindowSize(SDL_Window* window, int* w, int* h);
SDL_bool SDL_GetWindowWMInfo(SDL_Window* window, SDL_SysWMinfo* info);
void SDL_SetWindowBordered(SDL_Window* window, SDL_bool bordered);
Uint32 SDL_GetWindowID(SDL_Window* window);
SDL_WindowFlags SDL_GetWindowFlags(SDL_Window* window);
/* in 2.0.26 */
void SDL_GetWindowSizeInPixels(SDL_Window * window, int *w, int *h);
]])

local SDL_Window = {}
--- Create a new lua-managed window
function SDL_Window.create(title, x, y, w, h, flags)
	return ffi.gc(sdl.SDL_CreateWindow(title, x, y, w, h, flags),
								sdl.SDL_DestroyWindow)
end

function SDL_Window.fromID(id)
  assert(id ~= nil and id ~= 0, 'invalid arg #1, Uint32 expected')
  return sdl.SDL_GetWindowFromID(id)
end

function SDL_Window:getSize()
	local w = ffi.new('int[1]')
	local h = ffi.new('int[1]')

	sdl.SDL_GetWindowSize(self, w, h)
	return w[0], h[0]
end

function SDL_Window:getSizePixels()
  local w = ffi.new('int[1]')
  local h = ffi.new('int[1]')

  sdl.SDL_GetWindowSizeInPixels(self, w, h)
  return w[0], h[0]
end
--- Get underlying native window
function SDL_Window:getNativeWindow()
  --
  -- The info structure must be initialized with the SDL version
  --
  local info = ffi.new('SDL_SysWMinfo')
  sdl.SDL_GetVersion(info.version)

  if sdl.SDL_GetWindowWMInfo(self, info) ~= sdl.TRUE then
    error(string.format("Failed to get WMInfo: [%s]", sdl.GetError()))
  end

  if info.subsystem == sdl.SYSWM_WINDOWS then
    return info.info.win.window
  elseif info.subsystem == sdl.SYSWM_COCOA then
    return info.info.cocoa.window
  elseif info.subsystem == sdl.SYSWM_WINRT then
    return info.info.winrt.window
  elseif info.subsystem == sdl.SYSWM_UIKIT then
    return info.info.uikit.window
  elseif info.subsystem == sdl.SYSWM_X11 then
    return info.info.x11.window
  elseif info.subsystem == sdl.SYSWM_WAYLAND then
    return info.info.wl.egl_window
  else
    error(string.format("Unexpected subsystem [%d]", tonumber(info.subsystem)))
  end
end

SDL_Window.setTitle       = sdl.SDL_SetWindowTitle
SDL_Window.show           = sdl.SDL_ShowWindow
SDL_Window.hide           = sdl.SDL_HideWindow
SDL_Window.getPixelFormat = sdl.SDL_GetWindowPixelFormat
SDL_Window.getFlags       = sdl.SDL_GetWindowFlags
--- Get current window title as lua string
function SDL_Window:getTitle()
	local title = sdl.SDL_GetWindowTitle(self)
	if title ~= nil then
		return ffi.string(title, sdl.SDL_strlen(title))
	end
  return ''
end

function SDL_Window:bitsPerPixel()
  local pf = self:getPixelFormat()
  -- #define SDL_BITSPERPIXEL(X) (((X) >> 8) & 0xFF)
  return bit.band(bit.rshift(pf, 8), 0xff)
end
--- Set/unset window as fullscreen
-- @param flags
--	SDL_WINDOW_FULLSCREEN, for "real" fullscreen with a videomode change
--  SDL_WINDOW_FULLSCREEN_DESKTOP for "fake" fullscreen that takes the size of the desktop
--  0 for windowed mode
function SDL_Window:setFullscreen(flags)
	if sdl.SDL_SetWindowFullscreen(self, flags) ~= 0 then
		error(string.format("SDL_SetWindowFullscreen failed: %s", sdl.GetError()))
	end
end
--- Enable/disable window border
-- @param true|false
SDL_Window.setBorder = sdl.SDL_SetWindowBordered

local SDL_WindowMT = {
  __index = SDL_Window,
  --- Compare windows
  __eq = function(self, window)
    return sdl.SDL_GetWindowID(self) == sdl.SDL_GetWindowID(window)
  end,
  __tostring = function(self)
    return string.format("SDL_WindowID %d", tonumber(sdl.SDL_GetWindowID(self)))
  end,
}

return ffi.metatype('SDL_Window', SDL_WindowMT)
