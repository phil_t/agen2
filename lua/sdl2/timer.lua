local ffi = require('ffi')
local sdl = require('sdl2.lib')

ffi.cdef([[
Uint64 SDL_GetPerformanceCounter(void);
Uint64 SDL_GetPerformanceFrequency(void);

typedef int SDL_TimerID;
typedef struct _SDL_Timer {
  SDL_TimerID id;
} SDL_Timer;
typedef Uint32 (__cdecl *SDL_TimerCallback)(Uint32 interval, void* param);
// NOTE: the timer runs on a separate thread
SDL_TimerID SDL_AddTimer(Uint32 interval, SDL_TimerCallback callback, void* param);
SDL_bool SDL_RemoveTimer(SDL_TimerID id);
]])
--
-- TODO: 1. init SDL_TIMER explicitly to get a factory interface for creatine Timer objects
--       2. add agen_timer_create() API to publish the completion SDL event from C
--
local SDL_Timer = {}
local SDL_TimerMT = {
  __index = SDL_Timer,
  __eq = function(a, b) return a.id == b.id end,
  __gc = function(self) 
    self:remove()
  end,
}

function SDL_Timer:add(interval, func, cdata)
  assert(interval >= 0, "Invalid argument #1, interval cannot be negative")
  assert(func, "Invalid argument #2, c/lua function expected, got nil")
  self.id = sdl.SDL_AddTimer(interval, func, cdata)
  if self.id == 0 then
    error("Failed to create SDL timer " .. sdl.GetError())
  end
end

function SDL_Timer:remove()
  if self.id == 0 then return end
  if sdl.SDL_RemoveTimer(self.id) ~= sdl.TRUE then
    error("Failed to remove SDL timer " .. sdl.GetError())
  end
  self.id = 0
end

return ffi.metatype('SDL_Timer', SDL_TimerMT)
