local ffi = require('ffi')
local bit = require('bit')
local lib = require('sdl2.cimgui')

ffi.cdef([[
typedef struct _ImGuiStorage ImGuiStorage;
typedef struct _ImGuiSizeConstraintCallbackData ImGuiSizeConstraintCallbackData;
typedef void (*ImGuiSizeConstraintCallback)(ImGuiSizeConstraintCallbackData* data);

typedef enum _ImGuiWindowFlags {
  // Default: 0
  NoTitleBar             = 0x00000001, // Disable title-bar
  NoResize               = 0x00000002, // Disable user resizing with the lower-right grip
  NoMove                 = 0x00000004, // Disable user moving the window
  NoScrollbar            = 0x00000008, // Disable scrollbars (window can still scroll with mouse or programatically)
  NoScrollWithMouse      = 0x00000010, // Disable user vertically scrolling with mouse wheel
  NoCollapse             = 0x00000020, // Disable user collapsing window by double-clicking on it
  AlwaysAutoResize       = 0x00000040, // Resize every window to its content every frame
  ShowBorders            = 0x00000080, // Show borders around windows and items
  NoSavedSettings        = 0x00000100, // Never load/save settings in .ini file
  NoInputs               = 0x00000200, // Disable catching mouse or keyboard inputs
  MenuBar                = 0x00000400, // Has a menu-bar
  HorizontalScrollbar    = 0x00000800, // Allow horizontal scrollbar to appear (off by default). You may use SetNextWindowContentSize(ImVec2(width,0.0f)); prior to calling Begin() to specify width. Read code in imgui_demo in the "Horizontal Scrolling" section.
  NoFocusOnAppearing     = 0x00001000, // Disable taking focus when transitioning from hidden to visible state
  NoBringToFrontOnFocus  = 0x00002000, // Disable bringing window to front when taking focus (e.g. clicking on it or programatically giving it focus)
  AlwaysVerticalScrollbar = 0x00004000, // Always show vertical scrollbar (even if ContentSize.y < Size.y)
  AlwaysHorizontalScrollbar = 0x00008000, // Always show horizontal scrollbar (even if ContentSize.x < Size.x)
  AlwaysUseWindowPadding = 0x00010000, // Ensure child windows without border uses style.WindowPadding (ignored by default for non-bordered child windows, because more convenient)
  // [Internal]
  ChildWindow            = 0x00100000, // Don't use! For internal use by BeginChild()
  ChildWindowAutoFitX    = 0x00200000, // Don't use! For internal use by BeginChild()
  ChildWindowAutoFitY    = 0x00400000, // Don't use! For internal use by BeginChild()
  ComboBox               = 0x00800000, // Don't use! For internal use by ComboBox()
  Tooltip                = 0x01000000, // Don't use! For internal use by BeginTooltip()
  Popup                  = 0x02000000, // Don't use! For internal use by BeginPopup()
  Modal                  = 0x04000000, // Don't use! For internal use by BeginPopupModal()
  ChildMenu              = 0x08000000, // Don't use! For internal use by BeginMenu()
} ImGuiWindowFlags;

bool igBegin(const char* name, bool* p_open, ImGuiWindowFlags flags);
bool igBegin2(const char* name, bool* p_open, const ImVec2 size_on_first_use, float bg_alpha, ImGuiWindowFlags flags);
void igEnd();
bool igBeginChild(const char* str_id, const ImVec2 size, bool border, ImGuiWindowFlags extra_flags);
bool igBeginChildEx(ImGuiID id, const ImVec2 size, bool border, ImGuiWindowFlags extra_flags);
void igEndChild();
void igGetContentRegionMax(ImVec2* out);
void igGetContentRegionAvail(ImVec2* out);
float igGetContentRegionAvailWidth();
void igGetWindowContentRegionMin(ImVec2* out);
void igGetWindowContentRegionMax(ImVec2* out);
float igGetWindowContentRegionWidth();
ImDrawList* igGetWindowDrawList();
void igGetWindowPos(ImVec2* out);
void igGetWindowSize(ImVec2* out);
float igGetWindowWidth();
float igGetWindowHeight();
bool igIsWindowCollapsed();
void igSetWindowFontScale(float scale);
void igSetNextWindowPos(const ImVec2 pos, ImGuiSetCond cond);
void igSetNextWindowPosCenter(ImGuiSetCond cond);
void igSetNextWindowSize(const ImVec2 size, ImGuiSetCond cond);
void igSetNextWindowSizeConstraints(const ImVec2 size_min, const ImVec2 size_max, ImGuiSizeConstraintCallback custom_callback, void* custom_callback_data);
void igSetNextWindowContentSize(const ImVec2 size);
void igSetNextWindowContentWidth(float width);
void igSetNextWindowCollapsed(bool collapsed, ImGuiSetCond cond);
void igSetNextWindowFocus();
void igSetWindowPos(const ImVec2 pos, ImGuiSetCond cond);
void igSetWindowSize(const ImVec2 size, ImGuiSetCond cond);
void igSetWindowCollapsed(bool collapsed, ImGuiSetCond cond);
void igSetWindowFocus();
void igSetWindowPosByName(const char* name, const ImVec2 pos, ImGuiSetCond cond);
void igSetWindowSize2(const char* name, const ImVec2 size, ImGuiSetCond cond);
void igSetWindowCollapsed2(const char* name, bool collapsed, ImGuiSetCond cond);
void igSetWindowFocus2(const char* name);

float igGetScrollX();
float igGetScrollY();
float igGetScrollMaxX();
float igGetScrollMaxY();
void igSetScrollX(float scroll_x);
void igSetScrollY(float scroll_y);
void igSetScrollHere(float center_y_ratio);
void igSetScrollFromPosY(float pos_y, float center_y_ratio);
void igSetKeyboardFocusHere(int offset);
void igSetStateStorage(ImGuiStorage* tree);
ImGuiStorage* igGetStateStorage();
]])

local igWindow = {}
local vecZero = ffi.new('ImVec2')

function igWindow.showOverlay(title, text, x, y)
  local pos = ffi.new('ImVec2')
  pos.x = x
  pos.y = y
  lib.SetNextWindowPos(pos)

  local open = ffi.new('bool[1]')
  open[0] = true
  if lib.igBegin(title, open, vecZero, 0.3, 
    bit.bor(lib.NoTitleBar, lib.NoResize, lib.NoMove, lib.NoSavedSettings)) == true then
    lib.igText("Simple overlay\non the top-left side of the screen.")
    lib.igSeparator()
    lib.igText(text)
  end
  lib.igEnd()
end

return igWindow

