use std::path::Path;
use std::env;

// https://doc.rust-lang.org/cargo/reference/build-scripts.html

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    // println!("cargo:rustc-link-search=/usr/local/lib");
    // println!("cargo:rustc-link-lib=SDL2");
    _platform_specific_setup();
}

#[cfg(target_os = "windows")]
fn _platform_specific_setup() {
    println!("cargo:rustc-link-search=G:\\Devel\\agen2\\bin\\win32\\x64");
    println!("cargo:rustc-link-lib=lua51");
}

#[cfg(target_os = "macos")]
fn _platform_specific_setup() {
    let home = env::var("HOME").unwrap();
//    let proj = env::var("CARGO_MANIFEST_DIR").unwrap();
    
    println!("cargo:rustc-env=MACOSX_DEPLOYMENT_TARGET=10.12");	
//    println!("cargo:rustc-link-search={}", Path::new(&proj)
//        .join("bin")
//        .join("macOS")
//        .display());
    // libs relative to exe
    // println!("cargo:rustc-link-arg=-Wl,-rpath,@loader_path/../Frameworks");
    println!("cargo:rustc-link-arg=-Wl,-rpath,@executable_path/../Frameworks");
    println!("cargo:rustc-link-search=framework={}", Path::new(&home)
        .join("Library")
        .join("Frameworks")
        .display());
}

#[cfg(target_os = "linux")]
fn _platform_specific_setup() {
    // libs relavive to exe
    println!("cargo:rustc-link-arg=-Wl,-rpath,$ORIGIN/../lib");
}
