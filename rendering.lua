local ffi   = require('ffi')
local table = require('table')
local math3D = require('agen.util.math')
local sdl   = require('sdl2')
local font  = require('sdl2.font')
local image = require('sdl2.image')
agen.task   = require('taskman')

local bgcolor = ffi.new('float[4]', {0xae / 0xff, 0xae / 0xff, 0xae / 0xff, 1})
----------------------------------------------------------------------
function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
end
--
-- Note: vertex layout is application-specific
--
ffi.cdef([[
typedef struct _vertex_t {
  float x, y, z;	// position
  float u, v;			// texture mapping
} vertex_t;

typedef struct _instdata_t {
  float mat[16];	// model matrix (could be 3x3 for 2D)
} instdata_t __attribute__((aligned(16)));

typedef struct _griddata {
	float u, v;		// texture mapping
} griddata_t;
]])
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets = {}
  assets.images = {}
  assets.fonts  = {}
  --
  -- Load shaders from files
  --
  assets.shaders = {
    vertex = {},
    pixel  = {},
  }
  do
    local vShader = {
	opengl33 = [[
#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (std140) uniform xform {
  mat4 modelViewProj;
};

out vec2 v_texCoord;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   v_texCoord  = texcoord;
}
  ]],
	opengl41 = [[
#version 410
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (std140) uniform xform {
  mat4 modelViewProj;
};

out vec2 v_texCoord;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   v_texCoord  = texcoord;
}
  ]],
	opengl45 = [[
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;

layout (binding = 0, std140) uniform xform {
  mat4 modelViewProj;
};

layout (location = 0) out vec2 v_texCoord;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   v_texCoord  = texcoord;
}
  ]],
  directx11 = [[
cbuffer xform : register(b0)
{
    float4x4 modelViewProj; 
};

struct VOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

VOut main(float3 position : POSITION, float2 texcoord : TEXCOORD0) {
    VOut output;

    output.position = mul(modelViewProj, float4(position, 1.0f));
    output.texcoord = texcoord;

    return output;
}  
  ]]}
    table.insert(assets.shaders.vertex, vShader[device:getName()])
	
    local pShader = {
    opengl33 = [[
#version 330
in vec2 v_texCoord;
out vec4 fragColor;
uniform sampler2D tex0;

void main (void) {
  fragColor = texture(tex0, v_texCoord);
}
  ]],
  opengl41 = [[
#version 410
in vec2 v_texCoord;
out vec4 fragColor;
uniform sampler2D tex0;

void main (void) {
  fragColor = texture(tex0, v_texCoord);
}
  ]],
  opengl45 = [[
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 v_texCoord;
layout (location = 0) out vec4 fragColor;
layout (binding = 0) uniform sampler2D tex0;

void main (void) {
  fragColor = texture(tex0, v_texCoord);
}
  ]],
  directx11 = [[
Texture2D tex0 : register(t0);
sampler samplr : register(s0);

float4 main(float4 position : SV_POSITION, float2 texcoord : TEXCOORD) : SV_TARGET
{
    return tex0.Sample(samplr, texcoord);
}
  ]]}
    table.insert(assets.shaders.pixel, pShader[device:getName()])    
    --
    -- Instanced rendering shaders
    -- Note: instanced rendering requires shader model 3 (vs3_0)
    --
    local ivShader = {
      opengl33 = [[
#version 330
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in mat4 model;

out vec2 v_texCoord;

uniform mat4 viewProj;

void main(void) {
  mat4 modelViewProj = viewProj * model;
  gl_Position = modelViewProj * vec4(position, 1.0f);
  v_texCoord  = texcoord;
}
]],
  directx11 = [[
cbuffer ModelViewProjectionCB : register(b0) {
    matrix modelViewProj; 
};

cbuffer ViewProjectionCB : register(b1) {
    matrix viewProj; 
};

struct VOut {
    float4 position  : POSITION;
    float2 texcoord : TEXCOORD0;
};

VOut main(float3 position : POSITION, float2 texcoord0 : TEXCOORD0) {
    VOut output;

    output.position = mul(viewProj, float4(position, 1.0f));
    output.texcoord = texcoord0;

    return output;
}    
  ]]}
    table.insert(assets.shaders.vertex, ivShader[device:getName()])
		--
		-- Grid rendering shader
		--
		local gvShader = {
opengl33 = [[
#version 330
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec2 texoffset;

out vec2 v_texCoord;

uniform int  mapWidth;
uniform mat4 viewProj;

void main(void) {
	float x = (gl_InstanceID % mapWidth) * 110;
	float y = floor(gl_InstanceID / mapWidth) * 60;
  gl_Position = viewProj * (vec4(position, 0.0f, 1.0f) + vec4(x, y, 0.0f, 1.0f));
  v_texCoord  = texcoord + texoffset;
}
]],
  directx11 = [[
  ]]}
    table.insert(assets.shaders.vertex, gvShader[device:getName()])
  end
  --
  -- Setup buffers
  --
  assets.buffers = {}
  do
    -- 
    -- TODO: Asset manager: load from file and convert into device-specific data
    --
    local triangle = ffi.new('vertex_t[4]', {
      -- If you don't need per-vertex color set it as uniform in the vertex shader.
			{-100,  -100, 0,  0, 1},
			{ 100,  -100, 0,  1, 1},
			{-100,   100, 0,  0, 0},
      { 100,   100, 0,  1, 0},
    })

    local len  = ffi.sizeof('vertex_t[?]', 4)
    local vbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = len,
      ctype   = 'vertex_t',
      data	  = triangle,
    })
    table.insert(assets.buffers, vbuffer)
  end
  --
  -- Add index buffer
  --
  do
    local strip = ffi.new('uint16_t[4]', {0, 1, 2, 3})
    local len = ffi.sizeof('uint16_t[?]', 4)
    local ibuffer = device:createBuffer({
      purpose = 'index',
      usage   = 'static',
      size    = len,
      -- 16 or 32 bit value
      ctype   = 'uint16_t',
      data    = strip,
    })
	
    table.insert(assets.buffers, ibuffer)
  end
  --
  -- Add per-instance buffer
  --
  do
    local len = ffi.sizeof('instdata_t[?]', 30)
    local mbuffer = device:createBuffer({
      purpose = 'data',
      -- update per frame (particles)
      -- or static tile map?
      --usage   = 'static_write',
      usage   = 'dynamic',
      size    = len,
			ctype   = 'instdata_t',
    })
    
    local data = ffi.cast('instdata_t*', mbuffer:map('write_discard'))
		for i = 1, len / ffi.sizeof('instdata_t') do
			local pos = math3D.mat4.identity()
			pos:translate(math3D.vec3.create(
				math3D.rand.linear(-500, 500), 
				math3D.rand.linear(-200, 200), 
				0))
			ffi.copy(data[i].mat, pos:ptr(), ffi.sizeof('float[16]'))
		end
    mbuffer:unmap(0, len)
	
    table.insert(assets.buffers, mbuffer)
  end
  --[[
	do
    --
    -- tile texture coordinates are constant
    --
    local data = ffi.new('griddata_t[?]', 400)
    local len  = ffi.sizeof('griddata_t[?]', 400)
    for i = 1, 400 do
			data[i].u = 0
			data[i].v = 0
		end
    
    local gbuffer = device:createBuffer({
      purpose = 'data',
      usage   = 'constant',
      size    = len,
			ctype   = 'griddata_t',
      data    = data,
    })
    
    table.insert(assets.buffers, gbuffer)
	end
  ]]
	--
  -- load image
  --
  do
    local img = image.createFromPath('assets/images/img_test.png')    
    local tex = device:createTextureFromSurface(img)
    assets.images[0] = tex
  end
  --
  -- load font
  --
  do
    local vera18 = font.createFromPath('assets/fonts/Vera.ttf', 18)
		vera18:preloadGlyphs(" .,!?0123456789abcdefghijklmnopqrstuvwxyz", 41)
    table.insert(assets.fonts, vera18)
  end
  --
  -- add uniform buffers
  --
  do
    local cbuffer = nil
    cbuffer = device:createBuffer({
      purpose = 'uniform',
      usage   = 'dynamic',
      size    = ffi.sizeof('float[16]'),
      ctype   = 'float',
    })
    table.insert(assets.buffers, cbuffer)
  end
  return assets
end
--- requires thread pool module
-- Will run multiple thread of hello_world() from worker.lua in parallel.
-- Each thread will push 'Hello World' to the queue and the caller will
-- receive them.
local function processParallel()
  --
  -- Create a group of tasks.
  -- Note: don't reuse groups once they were waited on
  --
  local g = agen.task.newgroup()
  --
  -- Create a queue with maxthreads slots. Each thread will push results there.
  --
  local q = agen.task.newqueue(agen.task.maxthreads)
  --
  -- Add task of running 'hello_world' in worker.lua to the group
  --
  for i = 1, agen.task.maxthreads do
    agen.task.newtask(sdl.THREAD_PRIORITY_NORMAL, g, "hello_world", q)
  end
  --
  -- wait for tasks to complete
  --
  g:wait()
  --
  -- fetch results from queue
  --
  while true do
    --
    -- "items" are cdata allocated on another thread and must be alive
    -- until pop-ed by the caller (here). That's why the caller is responsible
    -- for releasing them. 
    -- Reference counting would automate that process for the caller.
    --
    local item = q:pop()
    if item == nil then break end
    agen.log.info("application", "Queue pop: %s", item)
    sdl.SDL_free(item)
  end
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
function agen.render(device)

  processParallel()

  do
    local rState = device:getRenderState('default')
    rState:setActive()
    --
    -- 1. Get RenderPass object. References target color/depth/stencil buffers, Render State (shaders and vertex layout) and Fixed-function state like viewport/blending mode/scissor test/depth test func.
    --
    local pass = rState:getRenderPass('default')
    --
    --  2. Issue drawing commands via the RenderPass object.
    --
    pass:begin()
    pass:clear()
    pass:drawIndexedPrimitive(0, 4, 0)
    --
    --  3. Submit to GPU for execution.
    --
    pass:submit()
  end
end

function agen.init()
  local device = agen.video.getDevice()
  --
  -- Start task manager
  --
  agen.task.start()

  local assets = loadAssets(device)
  local rTarget = device:getDefaultRenderTarget()
  local w, h = rTarget:getViewVolume(0)
  agen.log.debug('application', "Viewport size [%dx%d]", w, h)
  --
  -- 1. Setup render states
  --
  do
	  local rState = device:createRenderState({
      label = 'default',
	    shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
      },
	    vertexBuffersAttr = {
        {index = 0, name = 'position', offset = ffi.offsetof('vertex_t', 'x'), type = 'float3', usage = 'position', stride = ffi.sizeof('vertex_t')},
        {index = 0, name = 'texcoord', offset = ffi.offsetof('vertex_t', 'u'), type = 'float2', usage = 'texcoord', stride = ffi.sizeof('vertex_t')},
	    },
      topology = 'strip',
    })
	  local program = rState:getProgram()
	  --
	  -- Setup program inputs. 
		-- Model / view / projection remain the same per draw call.
		-- NOTE: transformation matrix multiplication order is: projection * view * model
	  --
		local xform = math3D.mat4.identity()
		-- ortho projection
		local proj = math3D.mat4.ortho(-w / 2, w / 2, -h / 2, h / 2, 0.1, 1)
		xform:multiply(proj)
		-- view is camera
		local view = math3D.mat4.lookAt(
			math3D.vec3.create(0, 0, 1),
			math3D.vec3.create(0, 0, 0),
			math3D.vec3.create(0, 1, 0))
		xform:multiply(view)
		-- model is identity
    local cbuffer = assets.buffers[4]
    local modelViewProj = cbuffer:map('write_discard')
    ffi.copy(modelViewProj, xform:ptr(), ffi.sizeof('float[16]'))
    cbuffer:unmap()

    local pass = rState:getRenderPass('default'):init({
      vertexBuffers = {
	      [0] = assets.buffers[1],
      },
      uniforms = {
        vertex = {
          [0] = {name = 'xform', data = assets.buffers[4]},
        },
      },
      indexBuffer = assets.buffers[2],
      samplers = {
        [0] = device:createSampler(),
      },
      textures = assets.images,
    })
	  --
	  -- associate texture and sampler with active texture unit in program
	  --
	  local tex0 = program:getSamplerIndex('tex0')
	  pass:setTexture(tex0, 0, 0)

    rState:setDepthFunc('off')
    rState:setBlendFunc('alpha')
  end

  rTarget:setClearColor(0, bgcolor)
end


