local ffi    = require('ffi')
local table  = require('table')
local math   = require('math')
local rStateFactory = require('agen.renderState.factory')
local dds    = require('agen.util.dds')
local sdl    = require('sdl2')
local image  = require('sdl2.image')
local rwops  = require('sdl2.rwops')
local keyboard = require('sdl2.input.keyboard')
----------------------------------------------------------------------
function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
  collectgarbage('collect')
end
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets = {
    buffers = {},
    images  = {},
    fonts   = {},
    shaders = {
      vertex = {},
      pixel  = {},
    },
  }

  do
    local dir = "assets/shaders/" .. device:getName()
    assets.shaders.vertex[1] = agen.io.contents(dir .. "/image.vert")
    assets.shaders.pixel[1] = agen.io.contents(dir .. "/image.frag")
  end
  --
  -- Setup buffers
  --
  do
    local w, h = 512, 512
    local quad = ffi.new('vertex2D[4]', {
      -- If you don't need per-vertex color set it as uniform in the vertex shader.
			{-w,  -h, 0, 1},
			{ w,  -h, 1, 1},
			{-w,   h, 0, 0},
      { w,   h, 1, 0},
    })

    local len  = ffi.sizeof('vertex2D[?]', 4)
    local vbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = len,
      ctype   = 'vertex2D',
      data	  = quad,
    })
    table.insert(assets.buffers, vbuffer)
  end
  --
  -- Add index buffer
  --
  do
    local strip = ffi.new('uint16_t[4]', {0, 1, 2, 3})
    local len = ffi.sizeof('uint16_t[?]', 4)
    local ibuffer = device:createBuffer({
      purpose = 'index',
      usage   = 'static',
      size    = len,
      -- 16 or 32 bit value
      ctype   = 'uint16_t',
      data    = strip,
    })
	
    table.insert(assets.buffers, ibuffer)
  end
  --
  -- add uniform buffers
  --
  local cbuffer = device:createBuffer({
    purpose = 'uniform',
    usage   = 'dynamic',
    size    = ffi.sizeof('float[16]'),
    ctype   = 'float',
  })
  table.insert(assets.buffers, cbuffer)
	--
  -- load images
  --
  local imagesArray = {
    -- ok
    '32x32_map_tile.png',
    '32x32_map_tile.dds',
    -- same payload, different header
    '32x32_map_tile_dxt5.dds',
    '32x32_map_tile_dxt5_dx10.dds',
    '32x32_map_tile_dxt1.dds',
    '32x32_map_tile_dxt3.dds',
    -- ok
    '32x32_map_tile_rgb.dds',
    -- GL_ARB_texture_compression_bptc
    --'32x32_map_tile_bc5.dds',
    --'32x32_map_tile_bc7.dds',
    'img_mars.jpg',
    'img_test.png',    
    'lena.png',
    'level1_8888.dds',
    'lena_rgb.dds',
  }

  local fourcc = ffi.new('uint8_t[4]')
  for i, filename in pairs(imagesArray) do
    local tex
    local ops = rwops.createFromPath('assets/images/' .. filename, 'rb')
    if ops:read(fourcc, 4, 1) ~= 1 then
      error("Failed to read from file")
    end
    agen.log.debug('application', "[%c][%c][%c][%d]", fourcc[0], fourcc[1], fourcc[2], fourcc[3])
    if dds.isDDS(fourcc) then
      local details = dds.getDetails(ops)
      tex = device:createTexture(details)
      -- fill with data
      local destRect = ffi.new('SDL_Rect', {
        x = 0,
        y = 0,
        w = details.width,
        h = details.height})
      local buffer = ffi.new('uint8_t[?]', details.size)
      for level = 1, details.levels, 1 do
        local len, pitch = dds.readMipLevel(ops, details, level - 1, buffer, details.size)
        tex:updateRect(level - 1, destRect, buffer, len, pitch)
        destRect.w = math.max(math.floor(destRect.w / 2), 1)
        destRect.h = math.max(math.floor(destRect.h / 2), 1)
      end
    else
      if ops:seek(0, 0) ~= 0 then
        error("Failed to reset stream")
      end
      local img = image.createFromRWops(ops, filename)
      tex = device:createTextureFromSurface(img)
    end
    assets.images[i - 1] = tex
  end

  return assets
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
function agen.render(device)
  do
    local rState = device:getRenderState('default')
    -- make state setting active
    rState:setActive()

    local pass = rState:getRenderPass('default')
    -- make render pass assets active
    pass:begin()
    -- clear render target
    pass:clear()
    -- issue render commands
    pass:drawIndexedPrimitive(0, 4, 0)
    -- submit render commands for execution
    pass:submit()
  end
  device:terminalWrite("Press <space> to cycle through images")
end

function agen.init()
  local device = agen.video.getDevice()
  -- preload all assets
  local assets = loadAssets(device)
  --
  -- create ortho 2D render state with a default render pass
  --
  local rState = rStateFactory.create2D(device, {
    center  = 'middle',
    -- in window coords
    size = {agen.window:getSize()},
    depthTest = false,
    shaders = {
      vertex = assets.shaders.vertex[1],
      pixel  = assets.shaders.pixel[1],
    },
    topology = 'strip',
    vertexBuffers = {
      [0] = assets.buffers[1],
    },
    indexBuffer   = assets.buffers[2],
    uniformBuffer = assets.buffers[3],
    textures      = assets.images,
    bgcolor       = {0xae / 0xff, 0xae / 0xff, 0xae / 0xff, 1},
  })
	--
	-- associate texture and sampler with active texture unit in program
	--
	local program = rState:getProgram()
	local tex0 = program:getSamplerIndex('tex0')
  local pass = rState:getRenderPass('default')
  local imageIndex = 0
	pass:setTexture(tex0, 0, imageIndex)
  --
  -- setup keyboard input
  --
  keyboard:allowEvents(true, sdl.KEYDOWN)

  agen.event.setCallback(sdl.KEYDOWN, function(e)
    if e.keysym.sym == string.byte(' ') then
      -- update image index
      imageIndex = (imageIndex + 1) % (#assets.images + 1)
	    pass:setTexture(tex0, 0, imageIndex)
    end
  end)
end
