#version 330
layout(location = 0) out vec4 fragColor;

in vec4 v_Color;

void main(void) {
	fragColor = v_Color;
}
