#version 330
in vec2 v_texCoord;
in vec4 v_Color;

out vec4 fragColor;
uniform sampler2D tex0;

void main(void) {
  /*
   * alpha-only font rendering
   */
  fragColor = v_Color;
  fragColor.a *= texture(tex0, v_texCoord).a;
}
