#version 330
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec4 color;

layout (std140) uniform xform {
  mat4 proj;
} XForm;

out vec2 Frag_UV;
out vec4 Frag_Color;

void main() {
  Frag_UV = texcoord;
  Frag_Color = color;
  gl_Position = XForm.proj * vec4(position.xy, 0, 1);
}

