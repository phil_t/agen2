#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (std140) uniform xform {
  mat4 modelViewProj;
} XForm;

out VertexData {
  vec4 normal;
  vec4 vertColor;
} VertexOut;

void main(void) {
   gl_Position = XForm.modelViewProj * vec4(position, 1.0f);
   vec4 normal = vec4(normal, 1.0f);
   VertexOut.normal    = normal;
   //VertexOut.vertColor = XForm.modelViewProj * normal;
   VertexOut.vertColor = normal;
}

