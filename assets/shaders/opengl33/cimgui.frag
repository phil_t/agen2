#version 330
uniform sampler2D Texture;
in vec2 Frag_UV;
in vec4 Frag_Color;
out vec4 Out_Color;

void main() {
  Out_Color = Frag_Color;
  Out_Color.a *= texture(Texture, Frag_UV).a;
}

