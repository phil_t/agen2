#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (std140) uniform xform {
  mat4 modelViewProj;
  vec4 fontColor;
} XForm;

out vec2 v_texCoord;
out vec4 v_Color;

void main(void) {
   gl_Position = XForm.modelViewProj * vec4(position, 0.0f, 1.0f);
   v_texCoord  = texcoord;
	 v_Color		 = XForm.fontColor;
}
