#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoord;

layout (std140) uniform xform {
  mat4 modelViewProj;
};

out Data {
  vec4 vertColor;
  vec2 texCoord;
} DataOut;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   DataOut.vertColor = modelViewProj * vec4(normal, 1.0f);
   DataOut.texCoord  = texcoord;
}
