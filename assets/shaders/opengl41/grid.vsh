#version 410
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec2 texoffset;
layout (std140) uniform xform {
  mat4 viewProj;
  int  mapWidth;
  int  tileWidth;
};

out vec2 v_texCoord;

void main(void) {
  // x wraps at mapWidth
	int x = ((gl_InstanceID % mapWidth) * tileWidth) - 300;
	int y = ((gl_InstanceID / mapWidth) * tileWidth) - 300;
  
  vec4 modelPos = vec4(position.x + x, position.y + y, 0, 1);
  gl_Position = viewProj * modelPos;
  v_texCoord  = texcoord + texoffset;
}
