#version 410

layout(location = 0) out vec4 fragColor;

layout (std140) uniform material {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  vec4 ambient;
};

in VertexData {
  vec4 normal;
  vec4 color;
} VertexIn;

void main (void) {
  fragColor = VertexIn.color * diffuse;
}

