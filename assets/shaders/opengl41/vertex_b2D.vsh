#version 330
layout (location = 0) in vec2 position;
out vec4 v_Color;

uniform mat4 modelViewProj;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 0.0f, 1.0f);
	 v_Color		 = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}
