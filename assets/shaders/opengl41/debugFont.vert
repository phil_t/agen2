#version 410
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (std140) uniform xform {
  mat4 modelViewProj;
  vec4 fontColor;
};

out VertexData {
  vec2 texCoord;
  vec4 Color;
} VertexOut;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 0, 1);
   VertexOut.texCoord  = texcoord;
	 VertexOut.Color		 = fontColor;
}
