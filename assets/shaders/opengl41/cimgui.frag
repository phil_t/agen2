#version 410
uniform sampler2D Texture;
in vData {
  vec2 uv;
  vec4 color;
} vDataIn;

out vec4 fragColor;

void main() {
  fragColor = vDataIn.color;
  fragColor.a *= texture(Texture, vDataIn.uv).a;
}

