#version 410
layout(location = 0) out vec4 fragColor;

layout (std140) uniform material {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  vec4 ambient;
};

uniform sampler2D tex;

in Data {
  vec4 normal;
  vec2 texCoord;
} DataIn;

void main() {
  fragColor = texture(tex, DataIn.texCoord) * diffuse;
}
