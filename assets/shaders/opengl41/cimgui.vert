#version 410
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec4 color;

layout (std140) uniform xform {
  mat4 proj;
} XForm;

out vData {
  vec2 uv;
  vec4 color;
} vDataOut;

void main() {
  gl_Position = XForm.proj * vec4(position.xy, 0, 1);
  vDataOut.uv = texcoord;
  vDataOut.color = color;
}

