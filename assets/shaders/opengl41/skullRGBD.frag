#version 410
layout(location = 0) out vec4 fragColorR;
layout(location = 1) out vec4 fragColorG;
layout(location = 2) out vec4 fragColorB;

in VertexData {
  vec4 normal;
  vec4 color;
} VertexIn;

void main(void) {
  fragColorR = vec4(VertexIn.color.r, 0, 0, VertexIn.color.a);
  fragColorG = vec4(0, VertexIn.color.g, 0, VertexIn.color.a);
  fragColorB = vec4(0, 0, VertexIn.color.b, VertexIn.color.a);
}

