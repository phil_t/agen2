#version 410
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec4 color;

layout (std140) uniform xform {
  mat4 modelViewProj;
  vec4 colorFactor;
};

out vec2 v_texCoord;
out vec4 v_color;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   v_texCoord  = texcoord;
   v_color     = color * colorFactor;
}

