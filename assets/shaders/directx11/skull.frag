///////////////////////////////////////////////////////////////
//
// Pixel shader
//  Calculates pixel color taking into account color modulation
//  or lighting.
//
///////////////////////////////////////////////////////////////
//
// pixel shader input
//
struct Pin {
  float4 position : SV_POSITION;
  float4 normal   : NORMAL;
  float4 color    : COLOR;
};

cbuffer material : register(b0) {
  float4 diffuse;
  float4 specular;
  float4 emissive;
  float4 ambient;
};

float4 main(Pin input) : SV_TARGET {
    return input.color * diffuse;
}

