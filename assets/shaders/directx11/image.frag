Texture2D tex0 : register(t0);
sampler samplr : register(s0);

float4 main(float4 position : SV_POSITION, float2 texcoord : TEXCOORD) : SV_TARGET {
    return tex0.Sample(samplr, texcoord);
}

