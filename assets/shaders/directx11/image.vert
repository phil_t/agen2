cbuffer xform : register(b0) {
    float4x4 modelViewProj; 
};

struct VOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

VOut main(float3 position : POSITION, float2 texcoord : TEXCOORD0) {
    VOut output;

    output.position = mul(modelViewProj, float4(position, 1.0f));
    output.texcoord = texcoord;

    return output;
}  

