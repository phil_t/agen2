cbuffer xform : register(b0) {
    float4x4 modelViewProj; 
    float4   color;
};

struct VOut {
    float4 position : SV_POSITION;
    float4 color    : COLOR;
    float2 texcoord : TEXCOORD;
};

VOut main(float2 position : POSITION, float2 texcoord : TEXCOORD0) {
    VOut output;

    output.position = mul(modelViewProj, float4(position, 0.0f, 1.0f));
    output.color    = color;
    output.texcoord = texcoord;

    return output;
}  

