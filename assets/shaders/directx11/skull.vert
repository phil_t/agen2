//////////////////////////////////////////////////////////////
//
// Vertex shader
//  Transforms vertices from model to world space and sets
//  vertex color.
//
//////////////////////////////////////////////////////////////
cbuffer xform : register(b0) {
    float4x4 modelViewProj; 
};
//
// vertex shader input
//
struct Vin {
  float3 position : POSITION;
  float3 normal   : NORMAL;
};
//
// geometry shader input
//
struct Gin {
  float4 position : SV_POSITION;
  float4 normal   : NORMAL;
  float4 color    : COLOR;
};

Gin main(Vin input) {
    Gin output;
    float4 normal   = float4(input.normal, 1.0f);

    output.position = mul(modelViewProj, float4(input.position, 1.0f));
    output.normal   = normal;
    output.color    = mul(modelViewProj, normal);
    return output;
}

