/////////////////////////////////////////////////////////////////
//
// Geometry shader
//  Runs after the vertex shader. Used to generate vertices from 
//  existing vertices. For example generate sprite quad from top-left, 
//  width-height or patricles from AABB to avoid uploading large
//  amount of data from cpu to gpu.
//  Can output points, lines or triangles.
//
/////////////////////////////////////////////////////////////////
//
// geometry shader input
//
struct Gin {
  float4 position : SV_POSITION;
  float4 normal   : NORMAL;
  float4 color    : COLOR;
};
//
// pixel shader input
//
struct Pin {
  float4 position : SV_POSITION;
  float4 normal : NORMAL;
  float4 color  : COLOR;
};
//
// max number of vertices to output
//
[maxvertexcount(3)]
//
// just a pass-through, triangles in, same triangles out
//
void main(triangle Gin poly[3][1], inout TriangleStream<Pin> ptStream) {
  Pin v1, v2, v3;

  v1.position = poly[0][0].position;
  v1.normal   = poly[0][0].normal;
  v1.color    = poly[0][0].color;
  ptStream.Append(v1);
  
  v2.position = poly[1][0].position;
  v2.normal   = poly[1][0].normal;
  v2.color    = poly[1][0].color;
  ptStream.Append(v2);

  v3.position = poly[2][0].position;
  v3.normal   = poly[2][0].normal;
  v3.color    = poly[2][0].color;
  ptStream.Append(v3);  
}
