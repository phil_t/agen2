Texture2D tex0 : register(t0);
sampler samplr : register(s0);

struct Pin {
  float4 position : SV_POSITION;
  float4 color    : COLOR;
  float2 texcoord : TEXCOORD;
};

float4 main(Pin input) : SV_TARGET {
    return input.color; // * tex0.Sample(samplr, input.texcoord);
}

