struct Pin {
  float4 position : SV_POSITION;
  float2 texcoord : TEXCOORD;
};

Texture2D tex0 : register(t0);
sampler SampleType : register(s0);

float4 main(Pin pin) : SV_TARGET {
    return tex0.Sample(SampleType, pin.texcoord);
}
