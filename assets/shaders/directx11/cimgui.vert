struct a2v {
 float2 position : POSITION;
 float2 texcoord : TEXCOORD0;
 float4 color    : COLOR0;
};

struct v2p {
 float4 Position  : SV_Position;
 float2 UV        : TEXCOORD;
 float4 Color	  : COLOR;
};

cbuffer xform : register(b0) {
  matrix mvp;
};

v2p main(a2v input) {
  v2p output;
  
  output.Position = mul(mvp, float4(input.position, 0, 1));
  output.Color    = input.color;
  output.UV       = input.texcoord;
  
  return output;
}
