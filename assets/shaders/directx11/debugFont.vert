struct a2v {
 float2 Position : POSITION;
 float2 Texcoord : TEXCOORD;
};

struct v2p {
 float4 Position  : SV_Position;
 float4 Color	    : COLOR;
 float2 Texcoord  : TEXCOORD;
};

cbuffer xform : register(b0) {
  matrix mvp;
	float4 fontColor;
};

v2p main(a2v input) {
  v2p output;
  
  output.Position = mul(mvp, float4(input.Position, 0, 1));
  output.Color    = fontColor;
  output.Texcoord = input.Texcoord;
  
  return output;
}
