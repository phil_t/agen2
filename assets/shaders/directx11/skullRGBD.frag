//
// pixel shader input
//
struct Pin {
  float4 normal : NORMAL;
  float4 color  : COLOR;
};
//
// pixel shader output
//
struct PSout {
  float4 ColorR : SV_Target0;
  float4 ColorG : SV_Target1;
  float4 ColorB : SV_Target2;
  //float4 depth  : SV_Target3;
};

PSout main(Pin input) {
  PSout pout;
  pout.ColorR = float4(input.color.r, 0, 0, input.color.a);
  pout.ColorG = float4(0, input.color.g, 0, input.color.a);
  pout.ColorB = float4(0, 0, input.color.b, input.color.a);
  return pout;
}

