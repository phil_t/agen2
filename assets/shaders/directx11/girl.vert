cbuffer xform : register(b0) {
  float4x4 modelViewProj; 
};

struct Vin {
  float3 pos    : POSITION;
  float3 normal : NORMAL;
  float2 texcoord : TEXCOORD0;
};

struct Pin {
  float4 position : SV_POSITION;
  float3 normal   : NORMAL;
  float2 texcoord : TEXCOORD;
};

Pin main(Vin input) {
  Pin output;
	// modelViewProj is column-major
  output.position = mul(modelViewProj, float4(input.pos, 1.0f));
  output.normal   = input.normal;
  output.texcoord = input.texcoord;
  return output;
}
