struct v2p {
  float4 Position  : SV_Position;
  float4 Color     : COLOR;
  float2 Texcoord  : TEXCOORD;
};

Texture2D tex0 : register(t0);
sampler samplr : register(s0);
//
// font texture is assumed to be 8-bit alpha texture affecting only font color's alpha value
//
float4 main(v2p IN) : SV_Target {
  float4 Color = IN.Color;
  Color.a *= tex0.Sample(samplr, IN.Texcoord).a;
  return Color;
}
