struct v2p {
  float4 Position  : SV_Position;
  float2 UV        : TEXCOORD;
  float4 Color     : COLOR;
};

Texture2D Texture : register(t0);
sampler samplr : register(s0);
//
// font texture is assumed to be 8-bit alpha texture affecting only font color's alpha value
//
float4 main(v2p IN) : SV_Target {
  float4 Color = IN.Color;
  Color.a *= Texture.Sample(samplr, IN.UV).a;
  return Color;
}
