cbuffer material : register(b0) {
  float4 diffuse;
  float4 specular;
  float4 emissive;
  float4 ambient;
};

Texture2D tex  : register(t0);
sampler samplr : register(s0);

struct Pin {
  float4 position : SV_POSITION;
  float3 normal   : NORMAL;
  float2 texcoord : TEXCOORD;
};

float4 main(Pin input) : SV_TARGET {
  return tex.Sample(samplr, input.texcoord) * diffuse;
}
