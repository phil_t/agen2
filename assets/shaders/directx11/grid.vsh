cbuffer xform : register(b0) {
  matrix viewProj;
  int mapWidth;
  int tileWidth;
};

struct Vin {
  float2 position  : POSITION;
  float2 texcoord  : TEXCOORD;
  float2 texoffset : TEXOFFSET;
};

struct Pin {
  float4 position : SV_POSITION;
  float2 texcoord : TEXCOORD;
};

Pin main(Vin vin, uint instanceID : SV_InstanceID) {
  Pin output;

  int x = ((instanceID % mapWidth) * tileWidth) - 300;
	int y = ((instanceID / mapWidth) * tileWidth) - 300;
  
  float4 worldPos = float4(vin.position.x + x, vin.position.y + y, 0.0f, 1.0f); 
  output.position = mul(viewProj, worldPos);  
  output.texcoord = vin.texcoord + vin.texoffset;

  return output;
}
