#version 430 core
// pass-through shader
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VertexData {
  vec4 normal;
  vec4 vertColor;
} VertexIn[];
         
out VertexData {
  vec4 normal;
  vec4 color;
} VertexOut;

void main(void) {
  int i;

  for (i = 0; i < gl_in.length(); i++) {
    gl_Position = gl_in[i].gl_Position;
    VertexOut.normal = VertexIn[i].normal;
    VertexOut.color  = VertexIn[i].vertColor;
    EmitVertex();
  }
  EndPrimitive();
}
