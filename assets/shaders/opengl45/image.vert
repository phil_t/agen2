#version 450
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (std140) uniform xform {
  mat4 modelViewProj;
};

out vec2 v_texCoord;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   v_texCoord  = texcoord;
}

