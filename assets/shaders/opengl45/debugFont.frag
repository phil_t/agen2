#version 450
uniform sampler2D tex0;

in VertexData {
  vec2 texCoord;
  vec4 Color;
} VertexIn;

out vec4 fragColor;

void main (void) {
	fragColor = VertexIn.Color;
  fragColor.a *= texture(tex0, VertexIn.texCoord).a;
}
