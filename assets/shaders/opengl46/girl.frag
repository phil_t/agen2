#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 fragColor;

layout (binding = 0, std140) uniform material {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  vec4 ambient;
};

layout (binding = 1) uniform sampler2D tex;

layout (location = 0) in Data {
  vec2 texCoord;
} DataIn;

void main() {
  fragColor = texture(tex, DataIn.texCoord) * diffuse;
}

