#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoord;

layout (binding = 0, std140) uniform xform {
  mat4 modelViewProj;
};

layout (location = 0) out Data {
  vec2 texCoord;
} DataOut;

void main(void) {
   gl_Position = modelViewProj * vec4(position, 1.0f);
   DataOut.texCoord = texcoord;
}

