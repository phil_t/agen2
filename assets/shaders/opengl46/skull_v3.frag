#version 450
layout(location = 0) out vec4 fragColor;

layout (binding = 0, std140) uniform material {
  vec4 diffuse;
  vec4 specular;
  vec4 emissive;
  vec4 ambient;
};

layout (binding = 1) uniform sampler2D tex;

layout (location = 0) in VertexData {
  vec2 texCoord;
} VertexIn;

void main (void) {
  fragColor = texture(tex, VertexIn.texCoord) * diffuse;
}

