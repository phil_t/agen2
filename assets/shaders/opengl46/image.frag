#version 450
in vec2 v_texCoord;
out vec4 fragColor;
uniform sampler2D tex0;

void main (void) {
  fragColor = texture(tex0, v_texCoord);
}

