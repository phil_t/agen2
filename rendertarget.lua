local ffi   = require('ffi')
local bit   = require('bit')
local table = require('table')
local rStateFactory = require('agen.renderState.factory')
local math3D   = require('agen.util.math')
local assimp   = require('agen.scene.assimp')
local sdl      = require('sdl2')
local keyboard = require('sdl2.input.keyboard')
-- local mouse    = require('sdl2.input.mouse')

local texWidth, texHeight = 256, 256

local bgcolor = ffi.new('float[4]', {0x2e / 0xff, 0x2e / 0xff, 0x2e / 0xff, 1})
local rtcolor = ffi.new('float[4]', {0.88, 0.88, 0.88, 1})
----------------------------------------------------------------------
function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
end
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets  = {
		images = {},
		models = {},
		buffers = {},
    shaders = {
      vertex = {},
      pixel  = {},
      geometry = {},
    }
	}
  --
  -- load shaders
  --
  local dir = "assets/shaders/" .. device:getName()
  table.insert(assets.shaders.vertex,   agen.io.contents(dir .. "/skull.vert"))
  table.insert(assets.shaders.geometry, agen.io.contents(dir .. "/skull.geo"))
  table.insert(assets.shaders.pixel,    agen.io.contents(dir .. "/skullRGBD.frag"))

  table.insert(assets.shaders.vertex,   agen.io.contents(dir .. "/image.vert"))
  table.insert(assets.shaders.pixel,    agen.io.contents(dir .. "/image.frag"))
  --
  -- Setup buffers
  --
	do
    -- setup assimp logging via SDL
		assimp.EnableLogging(function(msg, user)
      local luastring = ffi.string(msg)
      local cat, text = luastring:match("([^,]+),%s*([^,]+)")
			agen.log.debug(cat, "%s", text)
		end, 0)

		local flags = bit.bor(
			-- requires index buffer
			assimp.JoinIdenticalVertices,
			-- convert to triangles
			assimp.Triangulate,
			-- normals
			assimp.GenNormals,
      assimp.FixInfacingNormals,
			-- no scene graph
			assimp.PreTransformVertices,
			assimp.ImproveCacheLocality,
			assimp.RemoveRedundantMaterials,
			assimp.SortByPType,
			-- no textures
			--assimp.GenUVCoords,
			--assimp.TransformUVCoords,
			assimp.OptimizeMeshes)

		local scene = assimp.scene.newFromFile('assets/models/skull/skull.obj', flags)		
		agen.log.debug('application', "Loaded num meshes [%d] cameras [%d]", scene.mNumMeshes, scene.mNumCameras)

    if bit.band(scene.mFlags, assimp.INCOMPLETE) ~= 0 then
      agen.log.error('application', 'scene incomplete')
    end

		local mesh = scene.mMeshes[0]
		agen.log.debug('application', "Processing mesh [%s] of [%d] vertices in [%d] faces", mesh:getName(), mesh.mNumVertices, mesh.mNumFaces)
		local len = mesh.mNumVertices * ffi.sizeof('aiVector3D')

		local vbuffer = device:createBuffer({
			purpose = 'vertex',
			usage   = 'static',
			size    = len,
			ctype   = 'aiVector3D',
      data    = mesh.mVertices,
		})
    --
    -- normals buffer
    --
    local nbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = len,
      ctype   = 'aiVector3D',
      data    = mesh.mNormals
    })
    --
    -- model xform
    --
		local xform = math3D.mat4.createFromData(scene.mRootNode.mTransformation.m)
    --
    -- create index buffer for the faces
    --
    local indices = 0
    for j = 0, mesh.mNumFaces - 1 do
      indices = indices + #mesh.mFaces[j]
    end

    agen.log.debug('application', "Total indices: %d", indices)
    --
    -- index buffer
    --
    local ibuffer = device:createBuffer({
			purpose = 'index',
			usage   = 'static',
			size    = indices * ffi.sizeof('uint16_t'),
			ctype   = 'uint16_t',
		})
    agen.log.debug("application", "populating index buffer...")
    local idata = ffi.new('uint16_t[?]', indices)
    local index = 0
    local face = nil

    for j = 0, mesh.mNumFaces - 1, 1 do
      face = mesh.mFaces[j]
      --
      -- cannot ffi.copy directly, face indices are 32-bit
      --
      for k, v in ipairs(face) do
        idata[index + k] = v
      end
      index = index + #face
    end
    ibuffer:update(0, idata, indices * ffi.sizeof('uint16_t'))
    agen.log.debug("application", "populating index buffer complete.")
    --
    -- create constant buffer for uniforms
    --
    local cbuffer = device:createBuffer({
      purpose = 'uniform',
      usage   = 'dynamic',
      size    = ffi.sizeof('float[16]'),
      ctype   = 'float',
    })
    --
    -- TODO: setup materials
    --
    table.insert(assets.models, {
			vbuffer  = vbuffer,
      nbuffer  = nbuffer,
      ibuffer  = ibuffer,
      cbuffer  = cbuffer,
			material = nil,
			xform    = xform,
		})
	end
  --
  -- 2x2 textures
  --
  do
    local w, h = texWidth, texHeight
    local triangle = ffi.new('vertex3D[4]', {
			{-w,  -h, 0.5,  0, 1},
			{ w,  -h, 0.5,  1, 1},
			{-w,   h, 0.5,  0, 0},
      { w,   h, 0.5,  1, 0},
    })

    local vbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = ffi.sizeof('vertex3D[?]', 4),
      ctype   = 'vertex3D',
      data	  = triangle,
    })

    local ibuffer = device:createBuffer({
      purpose = 'index',
      usage   = 'static',
      size    = ffi.sizeof('uint16_t[?]', 4),
      -- 16 or 32 bit value
      ctype   = 'uint16_t',
      data    = ffi.new('uint16_t[4]', {0, 1, 2, 3}),
    })

    local cbuffer = device:createBuffer({
      purpose = 'uniform',
      usage   = 'dynamic',
      size    = ffi.sizeof('float[16]'),
      ctype   = 'float',
    })

    table.insert(assets.models, {
			vbuffer  = vbuffer,
      nbuffer  = nil,
      ibuffer  = ibuffer,
      cbuffer  = cbuffer,
			material = nil,
			xform    = nil,
		})
  end

  agen.log.debug('application', "All assets loaded")
  return assets
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
local axisX = math3D.vec3.create(1, 0, 0)
local axisY = math3D.vec3.create(0, -1, 0)
local axisZ = math3D.vec3.create(0, 0, 1)

local rect1 = math3D.vec3.create(-250,  250, 0)
local rect2 = math3D.vec3.create( 250,  250, 0)
local rect3 = math3D.vec3.create(-250, -250, 0)
local rect4 = math3D.vec3.create( 250, -250, 0)

function agen.render(device)
	--
	-- render skull model
	--
  do
    local rState = device:getRenderState('skull')
    rState:setActive()
    local pass = rState:getRenderPass('default')
    pass:begin()
    pass:clear()
    pass:clearDepthStencil()
		--
		-- rotate around Y
		--
		rState.xform.model:rotate(0.03, axisY)
    --
    -- update uniform buffer
    --
		local mvp     = rState:getModelViewProj()
    local cbuffer = pass:getUniformBuffer('xform')
    local modelViewProj = cbuffer:map('write_discard')
    ffi.copy(modelViewProj, mvp:ptr(), ffi.sizeof('float[16]'))
    cbuffer:unmap()
    --
    -- render
    --
    pass:drawIndexedPrimitive(0, 28611, 0)
    pass:submit()
  end

  do
		--
		-- render the 4 textures in a grid
		--
    local rState = device:getRenderState('view')
    rState:setActive()
    local pass = rState:getRenderPass('default')
    pass:begin()
    pass:clear()
    pass:clearDepth()

    local tex0 = rState:getProgram():getSamplerIndex('tex0')
    local cbuffer = pass:getUniformBuffer('xform')
    -- red
    do
      pass:setTexture(tex0, 0, 0)
      rState.xform.model:setIdentity()
      rState.xform.model:translate(rect1)
      local mvp = cbuffer:map('write_discard')
      ffi.copy(mvp, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
      cbuffer:unmap()
      pass:drawIndexedPrimitive(0, 4, 0)
    end
    -- green
    do
      pass:setTexture(tex0, 0, 1)
      rState.xform.model:setIdentity()
      rState.xform.model:translate(rect2)
      local mvp = cbuffer:map('write_discard')
      ffi.copy(mvp, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
      cbuffer:unmap()
      pass:drawIndexedPrimitive(0, 4, 0)
    end
    -- blue
    do
      pass:setTexture(tex0, 0, 2)
      rState.xform.model:setIdentity()
      rState.xform.model:translate(rect3)
      local mvp = cbuffer:map('write_discard')
      ffi.copy(mvp, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
      cbuffer:unmap()
      pass:drawIndexedPrimitive(0, 4, 0)
    end
    -- depth
    do
      pass:setTexture(tex0, 0, 3)
      rState.xform.model:setIdentity()
      rState.xform.model:translate(rect4)
      local mvp = cbuffer:map('write_discard')
      ffi.copy(mvp, rState:getModelViewProj():ptr(), ffi.sizeof('float[16]'))
      cbuffer:unmap()
      pass:drawIndexedPrimitive(0, 4, 0)
    end
    pass:submit()
  end

end

function agen.init()
  local device = agen.video.getDevice()
  local assets = loadAssets(device)
  local rTarget = device:getDefaultRenderTarget()
  --
  -- to setup the projection matrix
  --
  local w, h = rTarget:getViewVolume(0)
  agen.log.debug('application', "Viewport size [%dx%d]", w, h)
	--
	-- 3D rendering
	--
  rTarget:setClearColor(0, bgcolor)

	do
    --
    -- Render state #1: render model into 4 textures, R, G, B, and Depth
    --
	  local rState = device:createRenderState({
      label = 'skull',
      shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
        geometry = assets.shaders.geometry[1],
      },
	    vertexBuffersAttr = {
        {index = 0, name = 'position', offset = 0, type = 'float3', usage = 'position', stride = ffi.sizeof('aiVector3D')},
        {index = 1, name = 'normal',   offset = 0, type = 'float3', usage = 'normal'  , stride = ffi.sizeof('aiVector3D')},
	    },
      topology = 'triangles',
    })
    local wireframe = false

    -- enable key down event
    keyboard:allowEvents(true, sdl.KEYDOWN)

    agen.event.setCallback(sdl.KEYDOWN, function(e)
      if e.keysym.sym == string.byte('w') then
        wireframe = not wireframe
	      rState:setWireFrame(wireframe)
      elseif e.keysym.sym == string.byte('d') then
        depthfunc = not depthfunc
        rState:setDepthFunc(depthfunc and 'less' or 'off')
      end
    end)

    local zNear  = 0.1
    local zFar   = 100
    local yViewAngle = 35
    rState.xform.projection = math3D.mat4.perspective(math.rad(yViewAngle), texWidth, texHeight, zNear, zFar)
    -- view is camera
    rState.xform.view = math3D.mat4.lookAt(
      -- camera location
      math3D.vec3.create(0, 4, 15),
      -- looking at point
      math3D.vec3.create(0, 4, 0),
      -- normalized camera "up" vector
      axisY)
    -- model
		rState.xform.model = assets.models[1].xform
    local mvp = rState:getModelViewProj()

    local cbuffer = assets.models[1].cbuffer
    local modelViewProj = cbuffer:map('write_discard')
    ffi.copy(modelViewProj, mvp:ptr(), ffi.sizeof('float[16]'))
    cbuffer:unmap()

    rState:setDepthFunc('less')
    --
    -- setup render target with 4 attachments: R, G, B and depth
    --
    local renderTarget = device:createRenderTarget({
      color0 = {
        width  = texWidth,
        height = texHeight,
        format = sdl.PIXELFORMAT_BGRA8888,
        clearColor = rtcolor,
      },
      color1 = {
        width  = texWidth,
        height = texHeight,
        format = sdl.PIXELFORMAT_BGRA8888,
        clearColor = rtcolor,
      },
      color2 = {
        width  = texWidth,
        height = texHeight,
        format = sdl.PIXELFORMAT_BGRA8888,
        clearColor = rtcolor,
      },
      depth_stencil = {
        width  = texWidth,
        height = texHeight,
        format = sdl.PIXELFORMAT_D24S8,
        clearColor = ffi.new('float[1]', 1),
        -- HACK!
        shaderResource = true,
      },
    })

    rState:getRenderPass('default'):init({
      vertexBuffers = {
        [0] = assets.models[1].vbuffer,
        [1] = assets.models[1].nbuffer,
      },
      uniforms = {
        vertex = {
          [0] = {name = 'xform', data = assets.models[1].cbuffer},
        },
      },
      indexBuffer = assets.models[1].ibuffer,
      renderTarget = renderTarget,
    })
	end
  do
    local renderTarget =
      device:getRenderState('skull'):getRenderPass('default').renderTarget
    --
    -- Render state #2: render 4 textures in 2x2 grid
    --
    local rState = rStateFactory.create2D(device, {
      label = 'view',
      -- in backbuffer size
      size = {w, h},
      depthTest = true,
      center = 'middle',
      shaders = {
        vertex = assets.shaders.vertex[2],
        pixel  = assets.shaders.pixel[2],
      },
      topology = 'strip',
      vertexBuffers = {
        [0] = assets.models[2].vbuffer,
      },
      indexBuffer = assets.models[2].ibuffer,
      uniformBuffer = assets.models[2].cbuffer,
      textures = {
        [0] = renderTarget:getColorAttachment(0),
        [1] = renderTarget:getColorAttachment(1),
        [2] = renderTarget:getColorAttachment(2),
        [3] = renderTarget:getDepthAttachment(),
      },
    })
  end
end

