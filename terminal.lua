local table = require('table')
----------------------------------------------------------------------
function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
end

function agen.init()
end  

function agen.render(device)
  local t = {}
  for i = 0, 127 do
    t[i + 1] = string.char(i)
  end
  local tsz = table.concat(t)
  local sz = tsz .. [[

Debug font rendering
Possible use:
1.to print debug messages, errors and tracebacks
2.ASCII, mono-space, bitmap fonts only!

Requirements:
1.should be able to print text at any time
2.should be fast and use little memory
3.should not depend on other code like loading TTF files
4.should not depend on the rendering system too much

DONE:
1.figure out how to get the font texture size from SDL
2.transparency? not sure if we really need it though

TODO:
1.figure out the best way to print test at any time without constantly creating buffers
2.figure out the best primitive type per character (trilists take up 6 verts, tristrips and quads only 4!)
3.tabs "\t"?

Bad usage:
Нон-утф8]]

  device:terminalWrite(sz)
end

