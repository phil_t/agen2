---------------------------------------------------------------
--
-- Sample:
--  load scene from obj file with multiple meshes and materials
--
---------------------------------------------------------------
-- JIT compiler verbose mode: look for NYI
--require('jit.v').start()
-- JIT profiler
--require('jit.p').start()
-- Ivan's profiler
local profile = require('utils.profile')
local luaprofiler = false

local ffi   = require('ffi')
local bit   = require('bit')
local math  = require('math')

local math3D   = require('agen.util.math')
local scene    = require('agen.scene')('assimp')

local sdl      = require('sdl2')
local keyboard = require('sdl2.input.keyboard')
local mouse    = require('sdl2.input.mouse')
local wav      = require('sdl2.wav')
local rStateFactory = require('agen.renderState.factory')

local renderer

profile.hookall('Lua')

function agen.release()
  agen.log.debug('application', "application: entering release")
  profile.stop()
  profile.reset()
end
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets()
  local videoDev = agen.video.getDevice()
  local audioDev = agen.audio.getDevice()
  local assets = {
    shaders = {
      vertex = {},
      pixel  = {},
    },
    buffers = {
      vertex = {},
      index = {},
      audio = {},
    },
	}
  --
  -- Load shaders
  --
  local dir = "assets/shaders/" .. videoDev:getName()
  assets.shaders.vertex[1] = agen.io.contents(dir .. "/girl.vert")
  assets.shaders.pixel[1]  = agen.io.contents(dir .. "/girl.frag")
  --
  -- Load audio buffers
  --
  local wavFile = wav.createFromPath('assets/sounds/16bit_pcm_44100_stereo.wav')
  assets.buffers.audio[1] = audioDev:createBuffer({
    format    = wavFile.format,
    freq      = wavFile.freq,
    channels  = wavFile.channels,
    samples   = wavFile.samples,
    data      = wavFile:getData(),
    len       = #wavFile,
  })

  return assets
end
---------------------------------------------------------------------
--
-- per-frame vars
--
---------------------------------------------------------------------
local axisX = math3D.vec3.create(1, 0, 0)
local axisY = math3D.vec3.create(0, 1, 0)
local axisZ = math3D.vec3.create(0, 0, 1)
local delta = 0
local frame = 0
local report = ''
  
function agen.update(dt)
  delta = dt
  frame = frame + 1

  if luaprofiler and frame % 100 == 0 then
    report = profile.report('time', 20)
    agen.log.out(report)
    profile.reset()
  end
end 
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
function agen.render(device)
  --
  -- render model
  --
  do
	  --
	  -- rotate Model matrix around Z
	  --
    local rState = device:getRenderState('fabienne')
	  rState.xform.model:rotate(delta, axisY)
    -- render model
    renderer:present(rState)
  end
  --
  -- render gui
  --
  device:terminalWrite(
    string.format("Delta:%d ms\nLua memory: %d Kb\nPress w to toggle wireframe on/off\nPress d to toggle depth testing on/off\nPress p to toggle profiling on/off",
    delta,
    collectgarbage('count')))
end
-------------------------------------------------------------------
--
-- TODO:
-- 1. Create agScene from file with current video device.
-- 2. Initialize referenced resources.
-- 3. Setup render states with passes.
--
-- Don't just block until everything is done.
--
-------------------------------------------------------------------
function agen.init()
  local assets = loadAssets()
  local device  = agen.video.getDevice()
  local modelBase = 'assets/models/fabienne_percy'
  --
  -- Create scene
  --
  local model = scene.createSceneFromPath(modelBase .. '/rp_fabienne_percy_posed_001_200k.obj')
  --
  -- Create scene renderer
  --
  renderer = scene.createRenderer(model, modelBase)
  --
  -- to setup the projection matrix
  --
	--
	-- 3D rendering
	--
	do
	  local rState = rStateFactory.create3D(device, {
      label = 'fabienne',
      shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
      },
      topology = 'triangles',
      vertexBuffers = {
        [0] = renderer.buffers.vertex,
        [1] = renderer.buffers.normal,
        [2] = renderer.buffers.texcoord,
      },
      uniformBuffers = {
        vertex = {
          [0] = {name = 'xform', data = renderer.buffers.uniform.vertex, offset = 0},
        },
        fragment = {
          [0] = {name = 'material', data = renderer.buffers.uniform.frag, offset = 0},
        },
      },
      indexBuffer = renderer.buffers.index,
      textures = renderer.textures,
      bgcolor  = {0x2e / 0xff, 0x2e / 0xff, 0x2e / 0xff, 1},
    })
    --
    -- State is now created and initialized
    --
    local wireframe = true
	  rState:setWireFrame(wireframe)

    local depthfunc = false
    rState:setDepthFunc(depthfunc and 'less' or 'off')

    -- enable keyboard events
    keyboard:allowEvents(true, sdl.KEYDOWN)
    agen.event.setCallback(sdl.KEYDOWN, function(e)
      if e.keysym.sym == string.byte('w') then
        wireframe = not wireframe
	      rState:setWireFrame(wireframe)
      elseif e.keysym.sym == string.byte('d') then
        depthfunc = not depthfunc
        rState:setDepthFunc(depthfunc and 'less' or 'off')
      elseif e.keysym.sym == string.byte('p') then
        -- enable/disable profiling
        luaprofiler = not luaprofiler
        if luaprofiler then 
          profile.start()
        else 
          profile.stop()
        end
      end
    end)

    -- enable mouse move event
    mouse:allowEvents(true, sdl.MOUSEMOTION)
    agen.event.setCallback(sdl.MOUSEMOTION, function(e)
      if mouse:buttonPressed(e.state, sdl.MOUSE_BUTTON_LEFT) then
        -- move camera
        rState.xform.view:rotate(-e.yrel / 100, axisX)
        --zoom out
        -- rState.xform.view:translate(math3D.vec3.create(0, 0, -e.yrel / 100))
      end
    end)

    -- camera steps back
    rState.xform.view:translate(math3D.vec3.create(0, -125, 50))
    -- model from scene
    rState.xform.model = math3D.mat4.createFromData(model:getRootXForm())
    rState.xform.model:translate(math3D.vec3.create(0, 0, 50))
	end
  --[[
  profile.hookall('C')
  -- FIXME: allow sdl.SDL_GetTicks as arg
  profile.setclock(function() return sdl.SDL_GetTicks() end)
  ]]
end

