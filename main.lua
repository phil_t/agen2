local ffi   = require('ffi')
local table = require('table')
local math3D = require('agen.util.math')
local sdl   = require('sdl2')
local image = require('sdl2.image')
----------------------------------------------------------------------
function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
end
--
-- Note: vertex layout is application-specific
--
ffi.cdef([[
typedef struct _vertex_t {
  float x, y, z;	// position
  float u, v;			// texture mapping
} vertex_t;
]])

local primitives = nil
-- triangle strip
-- get a "quad" for the character (4 vertices)
local function getStrip(w, h, sx, sy, sw, sh, tw, th)
  local ux, uy = sx/tw, sy/th
  local ux2, uy2 = sw/tw + ux, sh/th + uy
  local quad =
  {
    {0, 0, 0, ux, uy},
    {w, 0, 0, ux2, uy},
    {0, -h, 0, ux, uy2},
    {w, -h, 0, ux2, uy2}
  }
  --return ffi.new('vertex_t[4]', quad)
  return quad
end
-- triangle list
-- get a "triangle pair" for the character (6 vertices)
local function getTriangles(w, h, sx, sy, sw, sh, tw, th)
  local ux, uy = sx/tw, sy/th
  local ux2, uy2 = sw/tw + ux, sh/th + uy
  local quad =
  {
    {0, 0, 0, ux, uy},
    {w, 0, 0, ux2, uy},
    {0, -h, 0, ux, uy2},
    {w, 0, 0, ux2, uy},
    {0, -h, 0, ux, uy2},
    {w, -h, 0, ux2, uy2}
  }
  return quad
end

local function loadFont(device, filename, cols, rows)
  local font = {}

  -- Load texture
  local img = image.createFromPath(filename)
  if img == nil then
    error("Failed to load texture")
  end
  agen.log.debug('application', "Created %s", tostring(img))
  font.texture = device:createTextureFromSurface(img)

  -- Setup quads and triangles
  font.quads = {}
  local tw, th = font.texture:getSize()
  local cw, ch = tw/cols, th/rows
  for i = 0, cols*rows do
    local x, y = i%cols, math.floor(i/rows)
    font.quads[i] = getTriangles(cw, ch, x*cw, y*ch, cw, ch, tw, th)
  end

  -- store the character width/height
  font.cw = cw
  font.ch = ch
  
  return font
end

local function generateText(device, font, sz, ox, oy)
  ox, oy = ox or 0, oy or 0
  -- Setup buffers
  local buffer = {}

  -- get the trilist for each character
  local x, y = ox, oy
  local vx = {}
  for i = 1, string.len(sz) do
    local b = string.byte(sz, i)
    -- skip non-printable characters
    if b >= 32 and b <= 126 then
      local q = font.quads[b]
      -- copy quad vertices
      for i = 1, #q do
        table.insert(vx, { unpack(q[i]) })
      end
      -- translate
      for j = #vx - 5, #vx do
        vx[j][1] = vx[j][1] + x
        vx[j][2] = vx[j][2] + y
      end
      -- advance x
      x = x + font.cw
    end
    -- new line and carriage return
    if b == 10 or b == 13 then
      x = ox
      y = y - font.ch
    end
  end
  primitives = #vx
  
  local vb = ffi.new('vertex_t[' .. primitives .. ']', vx)
  
  local len = ffi.sizeof('vertex_t[?]', primitives)
  local vbuffer = device:createBuffer({
    purpose = 'vertex',
    usage   = 'static',
    size    = len,
    ctype   = 'vertex_t',
    data	  = vb,
  })
  table.insert(buffer, vbuffer)
  
  local s = {}
  for i = 1, primitives do
    s[i] = i - 1
  end

  -- Add index buffer
  local index = {}
  local strip = ffi.new('uint16_t[' .. primitives .. ']', s)
  local len = ffi.sizeof('uint16_t[?]', primitives)
  local ibuffer = device:createBuffer({
    purpose = 'index',
    usage   = 'static',
    size    = len,
    -- 16 or 32 bit value
    ctype   = 'uint16_t',
    data    = strip,
  })
  table.insert(index, ibuffer)

  return buffer, index
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
function agen.render(device)
  local rState = device:getRenderState('default')
  -- 1. Create Render Pass object. References target color/depth/stencil buffers, Render State (shaders and vertex layout) and Fixed-function state like viewport/blending mode/scissor test/depth test func.
  local pass = rState:getRenderPass('default')
  pass:begin()
  --  2. Add drawing commands to Render Pass object.
  pass:clear()
  -- todo: store the number of "primitives" in the plugin?
  pass:drawIndexedPrimitive(0, primitives, 0)
  --  3. Optional: repeat for multiple render passes.
  --  4. Optional: merge multiple render passes
  --  5. Submit to GPU for execution.
  pass:submit()
end

local function readFile(fn)
  local f = io.open(fn, "r")
  if f == nil then
    error("could not open file:" .. fn)
  end
  local sz = f:read("*all")
  f:close()
  return sz
end

function agen.init()
  local device = agen.video.getDevice()
  local w, h = device:getDefaultRenderTarget():getViewVolume(0)
  agen.log.debug('application', "Viewport size [%dx%d]", w, h)

  local t = {}
  for i = 0, 127 do
    t[i + 1] = string.char(i)
  end
  local tsz = table.concat(t)
  local sz = tsz .. [[

Debug font rendering
Possible use:
1.to print debug messages, errors and tracebacks
2.ASCII, mono-space, bitmap fonts only!

Requirements:
1.should be able to print text at any time
2.should be fast and use little memory
3.should not depend on other code like loading TTF files
4.should not depend on the rendering system too much

DONE:
1.figure out how to get the font texture size from SDL
2.transparency? not sure if we really need it though

TODO:
1.figure out the best way to print test at any time without constantly creating buffers
2.figure out the best primitive type per character (trilists take up 6 verts, tristrips and quads only 4!)
3.tabs "\t"?

Bad usage:
Нон-утф8]]
  local x, y = device:getDefaultRenderTarget():getViewVolume(0)
  local font = loadFont(device, "assets/fonts/font2.png", 16, 16)
  local buffers, index = generateText(device, font, sz, -x/2, y/2)
  --
  -- setup render states
  --
  do
	  local rState = device:createRenderState({
      label = 'default',
      shaders = {
        vertex = readFile(string.format("assets/shaders/%s/vertex.%s", device:getName(), agen.conf.video.driver)),
        pixel  = readFile(string.format("assets/shaders/%s/pixel.%s", device:getName(), agen.conf.video.driver)),
      },
      vertexBuffersAttr = {
	  --
	  -- Describe vertex attributes layout in the buffer(s) for the vertex shader.
	  -- GL backend:
	  --  * vertex attributes locations have to be "resolved" from the shader. Can be pre-defined via GL_ARB_explicit_attrib_location (like DX9 does)
	  --  * either usage will match the vertex attribute name in the vertex shader
	  --  * or the order will match the vertex attributes order in the vertex shader
	  -- DX9 backend:
	  --  * vertex attributes locations are pre-defined
	  --
        {index = 0, name = 'position', offset = ffi.offsetof('vertex_t', 'x'), type = 'float3',   usage = 'position', stride = ffi.sizeof('vertex_t')},
        {index = 0, name = 'texcoord', offset = ffi.offsetof('vertex_t', 'u'), type = 'float2',   usage = 'texcoord', stride = ffi.sizeof('vertex_t')},
	    },
      topology = 'triangles',
    })
	  local program = rState:getProgram()
	  --
	  -- Setup program inputs. 
		-- Model / view / projection remain the same per draw call.
		-- NOTE: transformation matrix multiplication order is: projection * view * model
	  --
		local xform = math3D.mat4.identity()
		-- ortho projection
		local proj = math3D.mat4.ortho(-w / 2, w / 2, -h / 2, h / 2, 1, 2)
		xform:multiply(proj)
		-- view is camera
		local view = math3D.mat4.lookAt(
			math3D.vec3.create(0, 0, 1),
			math3D.vec3.create(0, 0, 0),
			math3D.vec3.create(0, 1, 0))
		xform:multiply(view)
		-- model is identity
	  local modelViewProj = program:getUniform('modelViewProj')
	  program:setMatrix(modelViewProj, xform:ptr())

    rState:setActive()
    local pass = rState:getRenderPass('default'):init({
      vertexBuffers = {
	      [0] = buffers[1],
      },
	    indexBuffer = index[1],
    })
    rState:setBlendFunc('alpha')
	  --
	  -- Associate texture and sampler with active texture unit in program
	  --
	  local tex0 = program:getSamplerIndex('tex0')
	  local sampler = device:createSampler()
	  pass:setTexture(tex0, sampler, font.texture)
  end
end

