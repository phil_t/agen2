local ffi   = require('ffi')
local bit   = require('bit')
local table = require('table')
local math3D   = require('agen.util.math')
local scene    = require('agen.scene')('assimp')
local sdl      = require('sdl2')
local keyboard = require('sdl2.input.keyboard')
local mouse    = require('sdl2.input.mouse')
local rStateFactory = require('agen.renderState.factory')

local renderer
----------------------------------------------------------------------
function agen.release()
  agen.log.debug('application', "release!")
end
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets  = {
		images = {},
		models = {},
		buffers = {},
    shaders = {
      vertex = {},
      pixel  = {},
    }
	}
  --
  -- load shaders
  --
  local dir = "assets/shaders/" .. device:getName()
  assets.shaders.vertex[1]   = agen.io.contents(dir .. "/skull_v3.vert")	
  assets.shaders.pixel[1]    = agen.io.contents(dir .. "/skull_v3.frag")
  agen.log.debug('application', "All assets loaded")
  return assets
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
local ticks = sdl.SDL_GetTicks()
local axisX = math3D.vec3.create(1, 0, 0)
local axisY = math3D.vec3.create(0, 1, 0)
local axisZ = math3D.vec3.create(0, 0, 1)
local modelViewProj = nil
local delta

function agen.update(dt)
  delta = dt
end

function agen.render(device)  
  do
    local rState = device:getRenderState('skull')
		rState.xform.model:rotate(delta, axisZ)
    renderer:present(rState)
  end

  device:terminalWrite(
    string.format("Delta:%d ms\nLua memory: %d Kb\nPress w to toggle wireframe on/off\nPress d to toggle depth testing on/off",
      delta,
      collectgarbage('count')))
end

function agen.init()
  local device = agen.video.getDevice()
  local assets = loadAssets(device)
  local modelBase = 'assets/models/skull_v3'
  --
  -- Create scene
  --
  local model = scene.createSceneFromPath(modelBase .. '/12140_Skull_v3_L2.obj')
  --
  -- Create scene renderer
  --
  renderer = scene.createRenderer(model, modelBase)
	--
	-- 3D rendering
	--
	do
	  local rState = rStateFactory.create3D(device, {
      label = 'skull',
      shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
      },
      vertexBuffers = {
        [0] = renderer.buffers.vertex,
        [1] = renderer.buffers.normal,
        [2] = renderer.buffers.texcoord,
      },
      topology = 'triangles',
      uniformBuffers = {
        vertex = {
          [0] = {name = 'xform', data = renderer.buffers.uniform.vertex, offset = 0},
        },
        fragment = {
          [0] = {name = 'material', data = renderer.buffers.uniform.frag, offset = 0},
        },
      },
      indexBuffer = renderer.buffers.index,
      textures = renderer.textures,
      bgcolor = {0x2e / 0xff, 0x2e / 0xff, 0x2e / 0xff, 1},
    })
    
    local wireframe = false
    local depthfunc = true
    
    -- enable key down event
    keyboard:allowEvents(true, sdl.KEYDOWN)

    agen.event.setCallback(sdl.KEYDOWN, function(e)
      if e.keysym.sym == string.byte('w') then
        wireframe = not wireframe
	      rState:setWireFrame(wireframe)
      elseif e.keysym.sym == string.byte('d') then
        depthfunc = not depthfunc
        rState:setDepthFunc(depthfunc and 'less' or 'off')
      end
    end)
    
    -- enable mouse move event
    mouse:allowEvents(true, sdl.MOUSEMOTION)

    agen.event.setCallback(sdl.MOUSEMOTION, function(e)
      if mouse:buttonPressed(e.state, sdl.MOUSE_BUTTON_LEFT) then
        -- transform camera
        rState.xform.view:rotate(e.yrel / 100, axisX)
      end
    end)
    -- camera
    rState.xform.view:translate(math3D.vec3.create(0, -10, 40))
    -- model
    rState.xform.model = math3D.mat4.createFromData(model:getRootXForm())
    rState.xform.model:rotate(-3.14 / 2, axisX)
	end
end

