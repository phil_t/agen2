local ffi   = require('ffi')
local table = require('table')
local math3D = require('agen.util.math')
local sdl    = require('sdl2')
local font   = require('sdl2.font')
local image  = require('sdl2.image')
----------------------------------------------------------------------
--
-- globals
--
----------------------------------------------------------------------
local tileSize = 32
local tilePadding = 1
local mapWidth = 20
local numTiles = mapWidth * mapWidth

function agen.update(dt)
  --agen.log.debug('application', "update: %s", dt)
end

function agen.release()
  agen.log.debug('application', "release!")
end

ffi.cdef([[
typedef struct _vertex_t {
  float x, y;	  // position
  float u, v;	  // texture mapping
} vertex_t;

typedef struct _griddata {
	float u, v;		// texture mapping
} __attribute__((aligned(16))) griddata_t;

typedef struct _cbuffer_t {
  float viewProj[16];
  int   width;
  int   tileWidth;
  int   padding[2];
} __attribute__((aligned(16))) cbuffer_t;
]])
----------------------------------------------------------------------
--
-- Load vertex data / shaders / texutres
--
----------------------------------------------------------------------
local function loadAssets(device)
  local assets = {
    buffers = {},
    images = {},
    fonts  = {},
    shaders = {
      vertex = {},
      pixel  = {},
    }
  }
  do
    --------------------------------------------------------------------------
    --
    -- Note: instanced rendering requires shader model 3 (vs3_0)
    --
    --------------------------------------------------------------------------
    local dir = "assets/shaders/" .. device:getName()
    assets.shaders.vertex[1] = agen.io.contents(dir .. "/grid.vsh")
    assets.shaders.pixel[1]  = agen.io.contents(dir .. "/grid.fsh")
  end
  --
  -- load tilemap image
  --
  local img = image.createFromPath('assets/images/32x32_map_tile.png')
  local tex = device:createTextureFromSurface(img)
  local w, h = tex:getSize()
  assets.images[0] = tex
  --
  -- Setup buffers
  --
  assets.buffers = {}
  do
    local s = tileSize / 2
	
    local triangle = ffi.new('vertex_t[4]', {
			{-s, -s, tilePadding / w, tileSize / h},
			{ s, -s, tileSize / w,    tileSize / h},
			{-s,  s, tilePadding / w, tilePadding / h},
      { s,  s, tileSize / w,    tilePadding / h},
    })

    local len  = ffi.sizeof('vertex_t[?]', 4)
    local vbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = len,
      ctype   = 'vertex_t',
      data	  = triangle,
    })
    assets.buffers['vertex'] = vbuffer
  end
  --
  -- Add index buffer
  --
  do
    local strip = ffi.new('uint16_t[4]', {0, 1, 2, 3})
    local len = ffi.sizeof('uint16_t[?]', 4)
    local ibuffer = device:createBuffer({
      purpose = 'index',
      usage   = 'static',
      size    = len,
      -- 16 or 32 bit value
      ctype   = 'uint16_t',
      data    = strip,
    })
    assets.buffers['index'] = ibuffer
  end
  --
  -- Offset into tilemap for each tile
  --
	do
    math.randomseed(os.clock())
    local data = ffi.new('griddata_t[?]', numTiles)
    local len  = ffi.sizeof('griddata_t[?]', numTiles)
    for y = 0, mapWidth - 1 do
      for x = 0, mapWidth - 1 do 
        data[y * mapWidth + x] = {
          (math.random(mapWidth) * (tileSize + tilePadding)) / w, 
          (math.random(mapWidth) * (tileSize + tilePadding)) / h
        }
      end
    end
    local gbuffer = device:createBuffer({
      purpose = 'vertex',
      usage   = 'static',
      size    = len,
			ctype   = 'griddata_t',
      data    = data,
    })
    assets.buffers['instanced'] = gbuffer
	end
  --
  -- load font
  --
  do
    local vera18 = font.createFromPath('assets/fonts/Vera.ttf', 18)
		vera18:preloadGlyphs(" .,!?0123456789abcdefghijklmnopqrstuvwxyz", 41)
    table.insert(assets.fonts, vera18)
  end
  --
  -- add uniform buffers
  --
  do
    local cbuffer = device:createBuffer({
      purpose = 'uniform',
      usage   = 'dynamic',
      size    = ffi.sizeof('cbuffer_t'),
      ctype   = 'cbuffer_t',
    })
    assets.buffers['uniform'] = cbuffer
  end
  return assets
end
----------------------------------------------------------------------
--
-- will wait vsync if config.vsync > 0. Should be application-defined.
--
----------------------------------------------------------------------
local bgcolor = ffi.new('float[4]', {0xae / 0xff, 0xae / 0xff, 0xae / 0xff, 1})
function agen.render(device)
  local rState = device:getRenderState('grid')
  rState:setActive()
  local pass = rState:getRenderPass('default')
  pass:begin()
  pass:clear()
  pass:drawInstancedPrimitive(0, 4, 0, numTiles)
  pass:submit()
end

function agen.init()
  local device = agen.video.getDevice()
  local assets = loadAssets(device)
  local rTarget = device:getDefaultRenderTarget()
  local w, h = rTarget:getViewVolume(0)
  agen.log.debug('application', "Viewport size [%dx%d]", w, h)
	--
	-- grid: needs extra buffer for DX9 to compensate for lack of gl_InstanceID
	--
	do
	  local rState = device:createRenderState({
      label = 'grid',
	    shaders = {
        vertex = assets.shaders.vertex[1],
        pixel  = assets.shaders.pixel[1],
      },
	  --
	  -- Describe vertex attributes layout in the buffer(s) for the vertex shader.
	  -- GL backend:
	  --  * vertex attributes locations have to be "resolved" from the shader. Can be pre-defined via GL_ARB_explicit_attrib_location (like DX does)
	  --  * either name will match the vertex attribute name in the vertex shader
	  --  * or the order will match the vertex attributes order in the vertex shader
	  -- DX9 backend:
	  --  * vertex attributes locations are pre-defined
	  --
	    vertexBuffersAttr = {
			  {index = 0, name = 'position',  offset = 0,								              type = 'float2', usage = 'position',  stride = ffi.sizeof('vertex_t'), instanced = false},
			  {index = 0, name = 'texcoord',  offset = ffi.offsetof('vertex_t', 'u'), type = 'float2', usage = 'texcoord',  stride = ffi.sizeof('vertex_t'), instanced = false},
			  {index = 1, name = 'texoffset', offset = 0,								              type = 'float2', usage = 'texoffset', stride = ffi.sizeof('griddata_t'), instanced = true},
	    },
      topology = 'strip',
    })
	  local program = rState:getProgram()
	  --
	  -- setup transformation matrix
	  --
		local xform = math3D.mat4.identity()
		-- ortho projection
		local proj = math3D.mat4.ortho(-w / 2, w / 2, -h / 2, h / 2, 0.1, 1)
		xform:multiply(proj)
		-- view is camera
		local view = math3D.mat4.lookAt(
			math3D.vec3.create(0, 0, 1),
			math3D.vec3.create(0, 0, 0),
			math3D.vec3.create(0, 1, 0))
		xform:multiply(view)
		-- model matrix is calculated per instance in the vertex shader
    local uniform_data = ffi.new('cbuffer_t')
    ffi.copy(uniform_data.viewProj, xform:ptr(), ffi.sizeof('float[16]'))
    uniform_data.width = mapWidth
    uniform_data.tileWidth = tileSize
      
    local cbuffer = assets.buffers.uniform
    local buf = cbuffer:map('write_discard')
    ffi.copy(buf, uniform_data, ffi.sizeof('cbuffer_t'))
    cbuffer:unmap()
    
    local pass = rState:getRenderPass('default'):init({
      vertexBuffers = {
        [0] = assets.buffers.vertex,
	      [1] = assets.buffers.instanced,
      },
      uniforms = {
        vertex = {
          [0] = {name = 'xform', data = assets.buffers.uniform, offset = 0},
        },
      },
	    indexBuffer = assets.buffers.index,
      textures = assets.images,
      samplers = {
        [0] = device:createSampler(),
      },
    })
	  --
	  -- associate shader variable with texture
	  --
	  local tex0 = program:getSamplerIndex('tex0')
	  pass:setTexture(tex0, 0, 0)

    rState:setDepthFunc('off')
    rState:setBlendFunc('alpha')
	end
  rTarget:setClearColor(0, bgcolor)
end

